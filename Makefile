#############################
## Front Matter            ##
#############################

.PHONY: help

.DEFAULT_GOAL := help

#############################
## Targets                 ##
#############################

## build all docker images for this project
build: build-build-container-image

## install the image needed in gitlab ci for building
build-jhipster-java:
	docker build docker/jhipster-java -f docker/jhipster-java/Dockerfile -t gerbsen/climate-dashboard-sachsen/jhipster:v7.9.4

## builds the build container to build angular and java app
build-build-container-image:
	docker build -t gerbsen/climate-dashboard-sachsen/build-container-image -f docker/jhipster/Dockerfile .

## build nginx container with files to serve
build-nginx:
	docker build -t gerbsen/climate-dashboard-sachsen/dashboard-container -f docker/nginx/Dockerfile .

## build production version of frontend in ./target/classes/static
build-frontend-dev:
	docker run --rm -t -v $(shell pwd):/app build-container-image ./npmw run webapp:build:dev

## copies all favicon files for the specific state to the root dir
build-copy-favicons:
	cp src/main/webapp/content/images/favicons/$(state)/* src/main/webapp/
	cp src/main/webapp/content/images/favicons/$(state)/android-chrome-192x192.png src/main/webapp/content/images/wappen.png

# ============================================================================================================
# ============================================================================================================
# ============================================================================================================

## Build a Docker image of the application
docker-build:
	./npmw run java:docker

## Run the application using Docker in the background
docker-up:
	docker compose -f src/main/docker/app.yml up -d

## Run the application an import the mastr once
docker-up-and-import-mastr:
	APPLICATION_DOWNLOADMASTRONSTARTUP=true docker compose -f src/main/docker/app.yml up -d

## Stop the dockerized application and remove its containers
docker-down:
	docker compose -f src/main/docker/app.yml down

## Show the logs of the dockerized application
docker-logs:
	docker compose -f src/main/docker/app.yml logs -f

# ============================================================================================================
# ============================================================================================================
# ============================================================================================================

## disable matomo for local development
disable-matomo:
	cp src/main/webapp/app/config/matomo.defaults.constants.ts src/main/webapp/app/config/matomo.constants.ts
	sed -i -e 's/MATOMO_SITE_ID_INTEGER/0/g' src/main/webapp/app/config/matomo.constants.ts
	sed -i -e 's|MATOMO_URL_STRING||g' src/main/webapp/app/config/matomo.constants.ts
	sed -i -e 's/MATOMO_ENABLED_BOOLEAN/false/g' src/main/webapp/app/config/matomo.constants.ts

## configure matomo in server container build via env variables
configure-matomo:
	cp src/main/webapp/app/config/matomo.defaults.constants.ts src/main/webapp/app/config/matomo.constants.ts
	sed -i -e 's/MATOMO_SITE_ID_INTEGER/$(MATOMO_SITE_ID_INTEGER)/g' src/main/webapp/app/config/matomo.constants.ts
	sed -i -e 's|MATOMO_URL_STRING|$(MATOMO_URL_STRING)|g' src/main/webapp/app/config/matomo.constants.ts
	sed -i -e 's/MATOMO_ENABLED_BOOLEAN/true/g' src/main/webapp/app/config/matomo.constants.ts

# ============================================================================================================
# ============================================================================================================
# ============================================================================================================

## remove build files and data for clean build
clean:
	rm -rf target/classes
	rm -rf data/$(state)/rwth_aachen/* 
	rm -rf src/main/webapp/app/config/data/mastr/*
	rm -rf src/main/webapp/app/config/data/rwth_aachen/*
	rm -rf data/postgresql/*

# ============================================================================================================
# ============================================================================================================
# ============================================================================================================

## installs all dependencies for this project
dev-install:
	./npmw install

## Start the frontend for development
dev-frontend: build-copy-favicons
	STATE=$(state) ./npmw start

## Inititales the backend for development and imports MaStR data once
dev-backend-init: postgres-up
	sleep 10
	STATE=$(state) DOWNLOAD_MASTR_ON_STARTUP=true ./mvnw -P-webapp

## just starts the backend and the database no import done
dev-backend: postgres-up
	sleep 10
	STATE=$(state) DOWNLOAD_MASTR_ON_STARTUP=false ./mvnw -P-webapp

## just starts the backend (no database start and no import done)
dev-backend-no-postgres-up:
	STATE=$(state) DOWNLOAD_MASTR_ON_STARTUP=false ./mvnw -P-webapp

## Start the application for development
dev-start: postgres-up
	sleep 10
	./mvnw

# ============================================================================================================
# ============================================================================================================
# ============================================================================================================

## Start the Postgres database using Docker
postgres-up:
	STATE=$(state) docker compose -f src/main/docker/postgresql.yml up -d --remove-orphans
	sleep 10
	./mvnw liquibase:update

## Stop the dockerized Postgres database and remove its container
postgres-down:
	STATE=$(state) docker compose -f src/main/docker/postgresql.yml down

## Stop the dockerized Postgres database and remove the database folder
postgres-clean: postgres-down
	rm -rf ./data/$(state)/postgresql

# ============================================================================================================
# ============================================================================================================
# ============================================================================================================

## serve the build locally with nginx on 8007
build-and-serve-nginx:
	docker rm -f dashboard-container
	docker build -t dashboard-image -f docker/nginx/Dockerfile .
	docker run -it --rm --name dashboard-container -p 8007:80 dashboard-image

## build prod and run as nginx image
build-and-serve-nginx-prod:
	sudo -u $(SUDO_USER) ./npmw run build
	docker rm -f dashboard-container
	docker build -t dashboard-image -f docker/nginx/Dockerfile .
	docker run -d --restart=always --name dashboard-container -p 8007:80 dashboard-image

## download data, process data, build app, start container
build-download-data-dev: build download data build-frontend-dev build-nginx

# ============================================================================================================
# ============================================================================================================
# ============================================================================================================

## process all data
data: data-rwth-aachen

## generates a sql files with statistics for all destatis data (area, population)
data-destatis:
	docker run --rm -t -v $(shell pwd):/app registry.gitlab.com/gerbsen/climate-dashboard/python-processor:latest python /app/scripts/destatis/destatis-population.py

## prepare the rwth aachen data (battery storage and e-mobility)
data-rwth-aachen:
	rm -rf src/main/webapp/app/config/data/rwth_aachen/*
	docker run --rm -t -v $(shell pwd)/scripts/:/app/scripts -w=/app/scripts/rwth_aachen -v $(shell pwd)/src/main/webapp/app/config/data/:/app/data/ registry.gitlab.com/gerbsen/climate-dashboard/mastr-downloader:latest node run-battery.js $(state)
	docker run --rm -t -v $(shell pwd)/scripts/:/app/scripts -w=/app/scripts/rwth_aachen -v $(shell pwd)/src/main/webapp/app/config/data/:/app/data/ registry.gitlab.com/gerbsen/climate-dashboard/mastr-downloader:latest node run-mobility.js $(state)

# ============================================================================================================
# ============================================================================================================
# ============================================================================================================

## Run the JDL entity generator based on the current 'schema.jdl' file
jdl:
	npx jhipster jdl src/main/resources/config/jdl/schema.jdl --skip-fake-data --force

## Run unit and e2e tests (needs running application)
test-all: test-unit test-e2e

## Run backend and frontend unit tests
test-unit: clean
	./npmw test

## Run end-to-end tests (needs running application)
test-e2e:
	./npmw run e2e

#############################
## Help Target             ##
#############################

## Show this help
help:
	@printf "Available targets:\n\n"
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  %-40s%s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST) | sort
