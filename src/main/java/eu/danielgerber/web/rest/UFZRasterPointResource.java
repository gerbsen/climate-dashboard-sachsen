package eu.danielgerber.web.rest;

import eu.danielgerber.domain.UFZRasterPoint;
import eu.danielgerber.repository.UFZRasterPointRepository;
import eu.danielgerber.service.UFZRasterPointQueryService;
import eu.danielgerber.service.criteria.UFZRasterPointCriteria;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.danielgerber.domain.UFZRasterPoint}.
 */
@RestController
@RequestMapping("/api")
public class UFZRasterPointResource {

    private final Logger log = LoggerFactory.getLogger(UFZRasterPointResource.class);

    private final UFZRasterPointRepository uFZRasterPointRepository;

    private final UFZRasterPointQueryService uFZRasterPointQueryService;

    public UFZRasterPointResource(
        UFZRasterPointRepository uFZRasterPointRepository,
        UFZRasterPointQueryService uFZRasterPointQueryService
    ) {
        this.uFZRasterPointRepository = uFZRasterPointRepository;
        this.uFZRasterPointQueryService = uFZRasterPointQueryService;
    }

    /**
     * {@code GET  /ufz-raster-points} : get all the uFZRasterPoints.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of uFZRasterPoints in body.
     */
    @GetMapping("/ufz-raster-points")
    public ResponseEntity<List<UFZRasterPoint>> getAllUFZRasterPoints(UFZRasterPointCriteria criteria) {
        log.debug("REST request to get UFZRasterPoints by criteria: {}", criteria);
        List<UFZRasterPoint> entityList = uFZRasterPointQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /ufz-raster-points/count} : count all the uFZRasterPoints.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/ufz-raster-points/count")
    public ResponseEntity<Long> countUFZRasterPoints(UFZRasterPointCriteria criteria) {
        log.debug("REST request to count UFZRasterPoints by criteria: {}", criteria);
        return ResponseEntity.ok().body(uFZRasterPointQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ufz-raster-points/:id} : get the "id" uFZRasterPoint.
     *
     * @param id the id of the uFZRasterPoint to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the uFZRasterPoint, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ufz-raster-points/{id}")
    public ResponseEntity<UFZRasterPoint> getUFZRasterPoint(@PathVariable Long id) {
        log.debug("REST request to get UFZRasterPoint : {}", id);
        Optional<UFZRasterPoint> uFZRasterPoint = uFZRasterPointRepository.findByIdAndFetchMeasurementsEagerly(id);
        return ResponseUtil.wrapOrNotFound(uFZRasterPoint);
    }
}
