package eu.danielgerber.web.rest;

import eu.danielgerber.domain.Statistic;
import eu.danielgerber.service.StatisticQueryService;
import eu.danielgerber.service.StatisticService;
import eu.danielgerber.service.criteria.StatisticCriteria;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.danielgerber.domain.Statistic}.
 */
@RestController
@RequestMapping("/api")
public class StatisticResource {

    private final Logger log = LoggerFactory.getLogger(StatisticResource.class);

    private final StatisticService statisticService;

    private final StatisticQueryService statisticQueryService;

    public StatisticResource(
        StatisticService statisticService,
        StatisticQueryService statisticQueryService
    ) {
        this.statisticService = statisticService;
        this.statisticQueryService = statisticQueryService;
    }

    /**
     * {@code GET  /statistics} : get all the statistics.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of statistics in body.
     */
    @GetMapping("/statistics")
    public ResponseEntity<List<Statistic>> getAllStatistics(StatisticCriteria criteria) {
        log.debug("REST request to get Statistics by criteria: {}", criteria);
        List<Statistic> entityList = statisticQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /statistics/count} : count all the statistics.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/statistics/count")
    public ResponseEntity<Long> countStatistics(StatisticCriteria criteria) {
        log.debug("REST request to count Statistics by criteria: {}", criteria);
        return ResponseEntity.ok().body(statisticQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /statistics/:id} : get the "id" statistic.
     *
     * @param id the id of the statistic to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the statistic, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/statistics/{id}")
    public ResponseEntity<Statistic> getStatistic(@PathVariable Long id) {
        log.debug("REST request to get Statistic : {}", id);
        Optional<Statistic> statistic = statisticService.findOne(id);
        return ResponseUtil.wrapOrNotFound(statistic);
    }
}
