package eu.danielgerber.web.rest;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.squareup.moshi.Moshi;

import eu.danielgerber.domain.MastrUnit;
import eu.danielgerber.domain.MastrUnitSearchResult;
import eu.danielgerber.domain.enums.AggregationFunction;
import eu.danielgerber.domain.enums.EnergySource;
import eu.danielgerber.domain.enums.IntervalFormat;
import eu.danielgerber.domain.enums.MastrUnitProperty;
import eu.danielgerber.domain.enums.OperationState;
import eu.danielgerber.service.MastrUnitQueryService;
import eu.danielgerber.service.MastrUnitService;
import eu.danielgerber.service.criteria.MastrUnitCriteria;
import eu.danielgerber.service.dto.MastrUnitForIntervalStatisticDTO;
import eu.danielgerber.service.dto.StateEnergyStatisitcsDTO;
import eu.danielgerber.web.rest.errors.BadRequestAlertException;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.danielgerber.domain.MastrUnit}.
 */
@RestController
@RequestMapping("/api")
public class MastrUnitResource {

    private final Logger log = LoggerFactory.getLogger(MastrUnitResource.class);

    private static final String ENTITY_NAME = "mastrUnit";
    
    private final MastrUnitService mastrUnitService;
    private final MastrUnitQueryService mastrUnitQueryService;

    public MastrUnitResource(
        MastrUnitService mastrUnitService,
        MastrUnitQueryService mastrUnitQueryService
    ) {
        this.mastrUnitService = mastrUnitService;
        this.mastrUnitQueryService = mastrUnitQueryService;
    }

    /**
     * {@code GET  /mastr-units} : get all the mastrUnits.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mastrUnits in body.
     */
    @GetMapping("/mastr-units")
    public ResponseEntity<List<MastrUnit>> getAllMastrUnits(
        MastrUnitCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get MastrUnits by criteria: {}", criteria);
        Page<MastrUnit> page = mastrUnitQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * TODO JAVADOC
     * 
     * @param id
     * @return
     * @throws IOException
     */
    @GetMapping("/mastr-units/mastr-search/{id}")
    public ResponseEntity<MastrUnitSearchResult> searchMastrUnitPublicId(@PathVariable String id) throws IOException {
        log.debug("REST request to query Mastr ID search : {}", id);

        Request request = new Request.Builder()
            .url(String.format("https://www.marktstammdatenregister.de/MaStR/Schnellsuche/Schnellsuche?praefix=SEE&mastrNummerOrId=%s",
                id.replace("SEE", "")))
            .build();

        Optional<MastrUnitSearchResult> result;

        try (Response response = new OkHttpClient().newBuilder().build().newCall(request).execute()) {
            MastrUnitSearchResult res = new Moshi.Builder().build().adapter(MastrUnitSearchResult.class).fromJson(response.body().string());
            result = res.getUrl() != null ? Optional.of(res) : Optional.ofNullable(null);
        }

        return ResponseUtil.wrapOrNotFound(result);
    }

    /**
     * {@code GET  /mastr-units/{ags}/statistics-for-interval} : returns a aggragation for the given timeinterval and admin unit
     * the admin unit has to be a community, e.g. admin_ebene_ade == Gemeinde or ade == 4 respectively
     *
     * @param ags            the ags for an administration unit (gemeindeschluesselaufgefuellt)
     * @param intervalFormat the interval to group by (WEEK, MONTH, YEAR)
     * @param energySource   the energy source of the master units
     * @param property       the property of a mastr unit to group by (GROSS_POWER)
     * @param aggregateFunction the aggregate function to use to group things
     * 
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mastrUnits in body.
     */
    @GetMapping("/mastr-units/{ags}/statistics-for-interval")
    public ResponseEntity<MastrUnitForIntervalStatisticDTO> getMastrStatisticsForIntervalForCommunity(
            @PathVariable String ags,
            @RequestParam(name = "interval", required = false, defaultValue = "YEAR") IntervalFormat intervalFormat,
            @RequestParam(name = "property", required = false, defaultValue = "GROSS_POWER") MastrUnitProperty property,
            @RequestParam(name = "energySource", required = false, defaultValue = "SOLAR_POWER") EnergySource energySource,
            @RequestParam(name = "aggregateFunction", required = false, defaultValue = "SUM") AggregationFunction aggregateFunction
        ) {

        if ( !ags.matches("^\\d{8,9}$") ) {
            throw new BadRequestAlertException(
                String.format("ags need to be a number of 8 or 9 digits: actual=%s", ags), ENTITY_NAME, "bad_ags_given"
            );
        }

        return ResponseEntity.ok().body(mastrUnitQueryService.getStatisticsForInterval(
                ags, intervalFormat, property, energySource, aggregateFunction));
    }

    /**
     * {@code GET  /mastr-units/count} : count all the mastrUnits.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/mastr-units/count")
    public ResponseEntity<Long> countMastrUnits(MastrUnitCriteria criteria) {
        log.debug("REST request to count MastrUnits by criteria: {}", criteria);
        return ResponseEntity.ok().body(mastrUnitQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /mastr-units/state} : get statistics for the complete state.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the statistics in body.
     */
    @GetMapping("/mastr-units/state")
    public ResponseEntity<StateEnergyStatisitcsDTO> getStateStatistics(
        @RequestParam(name = "energySource", required = false, defaultValue = "SOLAR_POWER") EnergySource energySource,
        @RequestParam(name = "operationState", required = false, defaultValue = "ACTIVE") OperationState state
    ) {
        log.debug("REST request to get statistics for complete state by energy source: {}", energySource);
        return ResponseEntity.ok().body(mastrUnitQueryService.getStateStatistics(state, energySource));
    }

    /**
     * {@code GET  /mastr-units/state/statistics-for-interval} : returns a aggragation for the given timeinterval 
     *
     * @param intervalFormat the interval to group by (WEEK, MONTH, YEAR)
     * @param property       the property of a mastr unit to group by (GROSS_POWER)
     * @param energySource   the energy source of the master units
     * @param aggregateFunction the aggregate function to use to group things
     * 
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mastrUnits in body.
     */
    @GetMapping("/mastr-units/state/statistics-for-interval")
    public ResponseEntity<MastrUnitForIntervalStatisticDTO> getMastrStatisticsForIntervalForState(
            @RequestParam(name = "interval", required = false, defaultValue = "YEAR") IntervalFormat intervalFormat,
            @RequestParam(name = "property", required = false, defaultValue = "GROSS_POWER") MastrUnitProperty property,
            @RequestParam(name = "energySource", required = false, defaultValue = "SOLAR_POWER") EnergySource energySource,
            @RequestParam(name = "aggregateFunction", required = false, defaultValue = "SUM") AggregationFunction aggregateFunction
        ) {

        return ResponseEntity.ok().body(mastrUnitQueryService.getStatisticsForInterval(
                null, intervalFormat, property, energySource, aggregateFunction));
    }

    /**
     * {@code GET  /mastr-units/:id} : get the "id" mastrUnit.
     *
     * @param id the id of the mastrUnit to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mastrUnit, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mastr-units/{id}")
    public ResponseEntity<MastrUnit> getMastrUnit(@PathVariable Long id) {
        log.debug("REST request to get MastrUnit : {}", id);
        Optional<MastrUnit> mastrUnit = mastrUnitService.findOne(id);
        return ResponseUtil.wrapOrNotFound(mastrUnit);
    }
}
