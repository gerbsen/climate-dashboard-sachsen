package eu.danielgerber.web.rest;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import eu.danielgerber.domain.ChargingStation;
import eu.danielgerber.domain.enums.IntervalFormat;
import eu.danielgerber.service.ChargingStationQueryService;
import eu.danielgerber.service.ChargingStationService;
import eu.danielgerber.service.criteria.ChargingStationCriteria;
import eu.danielgerber.service.dto.ChargingPointOperatorsDTO;
import eu.danielgerber.service.dto.ChargingStationsStatisticsForIntervalDTO;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.danielgerber.domain.ChargingStation}.
 */
@RestController
@RequestMapping("/api")
public class ChargingStationResource {

    private final Logger log = LoggerFactory.getLogger(ChargingStationResource.class);

    private final ChargingStationService chargingStationService;
    private final ChargingStationQueryService chargingStationQueryService;

    public ChargingStationResource(
        ChargingStationService chargingStationService,
        ChargingStationQueryService chargingStationQueryService
    ) {
        this.chargingStationService = chargingStationService;
        this.chargingStationQueryService = chargingStationQueryService;
    }

    /**
     * {@code GET  /charging-stations} : get all the chargingStations.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of chargingStations in body.
     */
    @GetMapping("/charging-stations")
    public ResponseEntity<List<ChargingStation>> getAllChargingStations(
        ChargingStationCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get ChargingStations by criteria: {}", criteria);
        Page<ChargingStation> page = chargingStationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /charging-stations/count} : count all the chargingStations.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/charging-stations/count")
    public ResponseEntity<Long> countChargingStations(ChargingStationCriteria criteria) {
        log.debug("REST request to count ChargingStations by criteria: {}", criteria);
        return ResponseEntity.ok().body(chargingStationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /charging-stations/operators} : group by operators
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the group by result in body.
     */
    @GetMapping("/charging-stations/operators")
    public ResponseEntity<ChargingPointOperatorsDTO> getChargingStationOperatorsGroupBy(
        @RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date startDate,
        @RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date endDate
    ) {
        log.debug("REST request to group ChargingStations by operators.");
        return ResponseEntity.ok().body(chargingStationQueryService.getBetreiberGroupBy(startDate, endDate));
    }

    /**
     * {@code GET  /charging-stations/statistics-for-interval} : group by time interval
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the group by result in body.
     */
    @GetMapping("/charging-stations/statistics-for-interval")
    public ResponseEntity<ChargingStationsStatisticsForIntervalDTO> getChargingStationGroupByStatisticsForInterval(
        @RequestParam(name = "interval", required = false, defaultValue = "YEAR") IntervalFormat intervalFormat
    ) {
        log.debug("REST request to group ChargingStations by statistics-for-interval.");
        return ResponseEntity.ok().body(chargingStationQueryService.getChargingStationsForInterval());
    }

    /**
     * {@code GET  /charging-stations/:id} : get the "id" chargingStation.
     *
     * @param id the id of the chargingStation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the chargingStation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/charging-stations/{id}")
    public ResponseEntity<ChargingStation> getChargingStation(@PathVariable Long id) {
        log.debug("REST request to get ChargingStation : {}", id);
        Optional<ChargingStation> chargingStation = chargingStationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(chargingStation);
    }
}
