package eu.danielgerber.web.rest;

import eu.danielgerber.domain.UFZMeasurement;
import eu.danielgerber.repository.UFZMeasurementRepository;
import eu.danielgerber.service.UFZMeasurementQueryService;
import eu.danielgerber.service.UFZMeasurementService;
import eu.danielgerber.service.criteria.UFZMeasurementCriteria;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.danielgerber.domain.UFZMeasurement}.
 */
@RestController
@RequestMapping("/api")
public class UFZMeasurementResource {

    private final Logger log = LoggerFactory.getLogger(UFZMeasurementResource.class);

    private final UFZMeasurementService uFZMeasurementService;

    private final UFZMeasurementRepository uFZMeasurementRepository;

    private final UFZMeasurementQueryService uFZMeasurementQueryService;

    public UFZMeasurementResource(
        UFZMeasurementService uFZMeasurementService,
        UFZMeasurementRepository uFZMeasurementRepository,
        UFZMeasurementQueryService uFZMeasurementQueryService
    ) {
        this.uFZMeasurementService = uFZMeasurementService;
        this.uFZMeasurementRepository = uFZMeasurementRepository;
        this.uFZMeasurementQueryService = uFZMeasurementQueryService;
    }

    /**
     * {@code GET  /ufz-measurements} : get all the uFZMeasurements.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of uFZMeasurements in body.
     */
    @GetMapping("/ufz-measurements")
    public ResponseEntity<List<UFZMeasurement>> getAllUFZMeasurements(
      UFZMeasurementCriteria criteria, 
      @org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get UFZMeasurements by criteria: {} and pageable {}", criteria, pageable);
        Page<UFZMeasurement> page = uFZMeasurementQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /ufz-measurements/dates} : get all the available uFZMeasurements dates.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of uFZMeasurements dates in body.
     */
    @GetMapping("/ufz-measurements/dates")
    public ResponseEntity<List<LocalDate>> getAllUFZMeasurementDates() {
        log.debug("REST request to get all UFZMeasurements dates.");
        List<LocalDate> dates = uFZMeasurementRepository.findDistinctDates();
        return ResponseEntity.ok().body(dates);
    }

    /**
     * {@code GET  /ufz-measurements/count} : count all the uFZMeasurements.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/ufz-measurements/count")
    public ResponseEntity<Long> countUFZMeasurements(UFZMeasurementCriteria criteria) {
        log.debug("REST request to count UFZMeasurements by criteria: {}", criteria);
        return ResponseEntity.ok().body(uFZMeasurementQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /ufz-measurements/:id} : get the "id" uFZMeasurement.
     *
     * @param id the id of the uFZMeasurement to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the uFZMeasurement, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ufz-measurements/{id}")
    public ResponseEntity<UFZMeasurement> getUFZMeasurement(@PathVariable Long id) {
        log.debug("REST request to get UFZMeasurement : {}", id);
        Optional<UFZMeasurement> uFZMeasurement = uFZMeasurementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(uFZMeasurement);
    }
}
