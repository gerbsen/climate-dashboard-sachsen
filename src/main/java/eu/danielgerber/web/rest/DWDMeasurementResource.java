package eu.danielgerber.web.rest;

import eu.danielgerber.domain.DWDMeasurement;
import eu.danielgerber.domain.enums.DWDState;
import eu.danielgerber.service.DWDMeasurementQueryService;
import eu.danielgerber.service.DWDMeasurementService;
import eu.danielgerber.service.criteria.DWDMeasurementCriteria;
import eu.danielgerber.service.dto.DWDMeasurementsDTO;
import java.util.List;
import java.util.Optional;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.danielgerber.domain.DWDMeasurement}.
 */
@RestController
@RequestMapping("/api")
public class DWDMeasurementResource {

    private final Logger log = LoggerFactory.getLogger(DWDMeasurementResource.class);

    private final DWDMeasurementService dWDMeasurementService;

    private final DWDMeasurementQueryService dWDMeasurementQueryService;

    public DWDMeasurementResource(
        DWDMeasurementService dWDMeasurementService,
        DWDMeasurementQueryService dWDMeasurementQueryService
    ) {
        this.dWDMeasurementService = dWDMeasurementService;
        this.dWDMeasurementQueryService = dWDMeasurementQueryService;
    }

    /**
     * {@code GET  /dwd-measurements} : get all the dWDMeasurements.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dWDMeasurements in body.
     */
    @GetMapping("/dwd-measurements")
    public ResponseEntity<List<DWDMeasurement>> getAllDWDMeasurements(
        DWDMeasurementCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get DWDMeasurements by criteria: {}", criteria);
        Page<DWDMeasurement> page = dWDMeasurementQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /dwd-measurements/count} : count all the dWDMeasurements.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/dwd-measurements/count")
    public ResponseEntity<Long> countDWDMeasurements(DWDMeasurementCriteria criteria) {
        log.debug("REST request to count DWDMeasurements by criteria: {}", criteria);
        return ResponseEntity.ok().body(dWDMeasurementQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /dwd-measurements/:id} : get the "id" dWDMeasurement.
     *
     * @param id the id of the dWDMeasurement to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dWDMeasurement, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/dwd-measurements/{id}")
    public ResponseEntity<DWDMeasurement> getDWDMeasurement(@PathVariable Long id) {
        log.debug("REST request to get DWDMeasurement: {}", id);
        Optional<DWDMeasurement> dWDMeasurement = dWDMeasurementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dWDMeasurement);
    }

    /**
     * {@code GET  /dwd-measurements/time-series/all/:state} : get all time series data for state.
     *
     * @param state the name of the state/state group defined by dwd.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the aggregation in body.
     */
    @GetMapping("/dwd-measurements/time-series/all/{state}")
    public ResponseEntity<DWDMeasurementsDTO> getDWDMeasurementsDTO(@PathVariable DWDState state){
        log.debug("REST request to get DWDMeasurementsDTO for: {}", state);
        return ResponseEntity.ok().body(dWDMeasurementService.getDWDMeasurementsDTO(state));
    }
}
