package eu.danielgerber.web.rest;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import eu.danielgerber.domain.AdministrationUnit;
import eu.danielgerber.service.AdministrationUnitQueryService;
import eu.danielgerber.service.AdministrationUnitService;
import eu.danielgerber.service.criteria.AdministrationUnitCriteria;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.danielgerber.domain.AdministrationUnit}.
 */
@RestController
@RequestMapping("/api")
public class AdministrationUnitResource {

    private final Logger log = LoggerFactory.getLogger(AdministrationUnitResource.class);

    private final AdministrationUnitService administrationUnitService;
    private final AdministrationUnitQueryService administrationUnitQueryService;

    public AdministrationUnitResource(
        AdministrationUnitService administrationUnitService,
        AdministrationUnitQueryService administrationUnitQueryService
    ) {
        this.administrationUnitService = administrationUnitService;
        this.administrationUnitQueryService = administrationUnitQueryService;
    }

    /**
     * {@code GET  /administration-units} : get all the administrationUnits.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of administrationUnits in body.
     */
    @GetMapping("/administration-units")
    public ResponseEntity<List<AdministrationUnit>> getAllAdministrationUnits(
        AdministrationUnitCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get AdministrationUnits by criteria: {}", criteria);
        Page<AdministrationUnit> page = administrationUnitQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /administration-units/count} : count all the administrationUnits.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/administration-units/count")
    public ResponseEntity<Long> countAdministrationUnits(AdministrationUnitCriteria criteria) {
        log.debug("REST request to count AdministrationUnits by criteria: {}", criteria);
        return ResponseEntity.ok().body(administrationUnitQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /administration-units/:id} : get the "id" administrationUnit.
     *
     * @param id the id of the administrationUnit to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the administrationUnit, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/administration-units/{id}")
    public ResponseEntity<AdministrationUnit> getAdministrationUnit(@PathVariable Long id) {
        log.debug("REST request to get AdministrationUnit : {}", id);
        Optional<AdministrationUnit> administrationUnit = administrationUnitService.findOne(id);
        return ResponseUtil.wrapOrNotFound(administrationUnit);
    }
}
