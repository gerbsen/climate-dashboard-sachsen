package eu.danielgerber.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MovingAverage {

  /**
   * This method returns a list with the moving average from the given values.
   * The returned list is null, if the list is empty, null or smaller then the window
   * width. Else the moving average is calculated. The list has the same length as 
   * the given list, but for the first width - 1 elements filled with null. A typical 
   * use case would be to generate a 30 year average of temperature measurements.
   * 
   * @param list the list to average 
   * @param windowWidth the width of the window (the number of elements to average)
   * 
   * @return a list with averaged values or null
   */
  public static List<Double> createMovingAverage(List<Double> list, int windowWidth) {
    if ( list == null || list.isEmpty() || list.size() < windowWidth ) return null;

    List<Double> movingAverageList = new ArrayList<Double>();
    
    // for the first windowWidth - 1 elements we do not have a proper average value
    if (list.size() > windowWidth)
      movingAverageList.addAll(Collections.nCopies(windowWidth - 1, null));

    for ( int i = 0; i < list.size() && i + windowWidth <= list.size(); i++) {
      List<Double> subList = list.subList(i, i + windowWidth);
      movingAverageList.add(subList.stream().mapToDouble(Double::doubleValue).sum() / windowWidth);
    }

    return movingAverageList;
  }
}
