package eu.danielgerber;

public class Constants {

    public static final String STATISTIC_POPULATION_TOTAL = "population_total";
    public static final String STATISTIC_AREA = "area";
}
