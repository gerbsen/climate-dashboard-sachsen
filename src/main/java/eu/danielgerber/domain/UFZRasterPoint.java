package eu.danielgerber.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.locationtech.jts.geom.Geometry;

/**
 * A UFZRasterPoint.
 */
@Entity
@Table(name = "ufz_raster_point")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UFZRasterPoint implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private Long id;

    /**
     * the latitude of the raster centroid
     */
    @Schema(description = "the latitude of the raster centroid")
    @Column(name = "lat")
    private Double lat;

    /**
     * the longitude of the raster centroid
     */
    @Schema(description = "the longitude of the raster centroid")
    @Column(name = "lng")
    private Double lng;

    /**
     * the geometry of the raster cell
     */
    @Schema(description = "the geometry of the raster cell")
    @Column(name = "geometry")
    private Geometry geometry;

    @OneToMany(mappedBy = "ufzRasterPoint", cascade = CascadeType.ALL)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "ufzRasterPoint" }, allowSetters = true)
    private Set<UFZMeasurement> ufzMeasurements = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public UFZRasterPoint id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLat() {
        return this.lat;
    }

    public UFZRasterPoint lat(Double lat) {
        this.setLat(lat);
        return this;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return this.lng;
    }

    public UFZRasterPoint lng(Double lng) {
        this.setLng(lng);
        return this;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Geometry getGeometry() {
        return this.geometry;
    }

    public UFZRasterPoint geometry(Geometry geometry) {
        this.setGeometry(geometry);
        return this;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Set<UFZMeasurement> getUfzMeasurements() {
        return this.ufzMeasurements;
    }

    public void setUfzMeasurements(Set<UFZMeasurement> uFZMeasurements) {
        if (this.ufzMeasurements != null) {
            this.ufzMeasurements.forEach(i -> i.setUfzRasterPoint(null));
        }
        if (uFZMeasurements != null) {
            uFZMeasurements.forEach(i -> i.setUfzRasterPoint(this));
        }
        this.ufzMeasurements = uFZMeasurements;
    }

    public UFZRasterPoint ufzMeasurements(Set<UFZMeasurement> uFZMeasurements) {
        this.setUfzMeasurements(uFZMeasurements);
        return this;
    }

    public UFZRasterPoint addUfzMeasurement(UFZMeasurement uFZMeasurement) {
        this.ufzMeasurements.add(uFZMeasurement);
        uFZMeasurement.setUfzRasterPoint(this);
        return this;
    }

    public UFZRasterPoint removeUfzMeasurement(UFZMeasurement uFZMeasurement) {
        this.ufzMeasurements.remove(uFZMeasurement);
        uFZMeasurement.setUfzRasterPoint(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UFZRasterPoint)) {
            return false;
        }
        return id != null && id.equals(((UFZRasterPoint) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UFZRasterPoint{" +
            "id=" + getId() +
            ", lat=" + getLat() +
            ", lng=" + getLng() +
            ", geometry='" + getGeometry() + "'" +
            "}";
    }

    /**
     * Checks wether there is a measurement for a given date.
     * 
     * @param date - the date to check
     * @return true if there is a measurement with the given date
     */
    public boolean hasMeasurementForDate(LocalDate date) {
        for ( UFZMeasurement measurement : this.ufzMeasurements) {
            if (measurement.getDate().equals(date) ) return true;
        }
        return false;
    }
}
