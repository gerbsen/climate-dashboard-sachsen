package eu.danielgerber.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.locationtech.jts.geom.Geometry;

/**
 * Representation of an entry in the Ladesäulenregister
 */
@Schema(description = "Representation of an entry in the Ladesäulenregister")
@Entity
@Table(name = "charging_station")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ChargingStation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "betreiber")
    private String betreiber;

    @Column(name = "strasse")
    private String strasse;

    @Column(name = "hausnummer")
    private String hausnummer;

    @Column(name = "adresszusatz")
    private String adresszusatz;

    @Column(name = "postleitzahl")
    private String postleitzahl;

    @Column(name = "ort")
    private String ort;

    @Column(name = "bundesland")
    private String bundesland;

    @Column(name = "kreis_kreisfreie_stadt")
    private String kreisKreisfreieStadt;

    @Column(name = "inbetriebnahme_datum")
    private LocalDate inbetriebnahmeDatum;

    @Column(name = "nennleistung_ladeeinrichtung")
    private Double nennleistungLadeeinrichtung;

    @Column(name = "art_der_ladeeinrichtung")
    private String artDerLadeeinrichtung;

    @Column(name = "anzahl_ladepunkte")
    private Integer anzahlLadepunkte;

    @Column(name = "erster_ladepunkt_steckertypen")
    private String ersterLadepunktSteckertypen;

    @Column(name = "erster_ladepunkt_leistung")
    private Double ersterLadepunktLeistung;

    @Column(name = "zweiter_ladepunkt_steckertypen")
    private String zweiterLadepunktSteckertypen;

    @Column(name = "zweiter_ladepunkt_leistung")
    private Double zweiterLadepunktLeistung;

    @Column(name = "dritter_ladepunkt_steckertypen")
    private String dritterLadepunktSteckertypen;

    @Column(name = "dritter_ladepunkt_leistung")
    private Double dritterLadepunktLeistung;

    @Column(name = "vierter_ladepunkt_steckertypen")
    private String vierterLadepunktSteckertypen;

    @Column(name = "vierter_ladepunkt_leistung")
    private Double vierterLadepunktLeistung;

    @Column(name = "geometry")
    private Geometry geometry;

    /**
     * every charging stations belongs to an administration unit
     */
    @Schema(description = "every charging stations belongs to an administration unit")
    @ManyToOne
    @JsonIgnoreProperties(value = { "district", "mastrUnits", "chargingStations", "statistics" }, allowSetters = true)
    private AdministrationUnit administrationUnit;

    @OneToMany(mappedBy = "chargingStation")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "administrationUnit", "chargingStation" }, allowSetters = true)
    private Set<Statistic> statistics = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ChargingStation id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBetreiber() {
        return this.betreiber;
    }

    public ChargingStation betreiber(String betreiber) {
        this.setBetreiber(betreiber);
        return this;
    }

    public void setBetreiber(String betreiber) {
        this.betreiber = betreiber;
    }

    public String getStrasse() {
        return this.strasse;
    }

    public ChargingStation strasse(String strasse) {
        this.setStrasse(strasse);
        return this;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getHausnummer() {
        return this.hausnummer;
    }

    public ChargingStation hausnummer(String hausnummer) {
        this.setHausnummer(hausnummer);
        return this;
    }

    public void setHausnummer(String hausnummer) {
        this.hausnummer = hausnummer;
    }

    public String getAdresszusatz() {
        return this.adresszusatz;
    }

    public ChargingStation adresszusatz(String adresszusatz) {
        this.setAdresszusatz(adresszusatz);
        return this;
    }

    public void setAdresszusatz(String adresszusatz) {
        this.adresszusatz = adresszusatz;
    }

    public String getPostleitzahl() {
        return this.postleitzahl;
    }

    public ChargingStation postleitzahl(String postleitzahl) {
        this.setPostleitzahl(postleitzahl);
        return this;
    }

    public void setPostleitzahl(String postleitzahl) {
        this.postleitzahl = postleitzahl;
    }

    public String getOrt() {
        return this.ort;
    }

    public ChargingStation ort(String ort) {
        this.setOrt(ort);
        return this;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getBundesland() {
        return this.bundesland;
    }

    public ChargingStation bundesland(String bundesland) {
        this.setBundesland(bundesland);
        return this;
    }

    public void setBundesland(String bundesland) {
        this.bundesland = bundesland;
    }

    public String getKreisKreisfreieStadt() {
        return this.kreisKreisfreieStadt;
    }

    public ChargingStation kreisKreisfreieStadt(String kreisKreisfreieStadt) {
        this.setKreisKreisfreieStadt(kreisKreisfreieStadt);
        return this;
    }

    public void setKreisKreisfreieStadt(String kreisKreisfreieStadt) {
        this.kreisKreisfreieStadt = kreisKreisfreieStadt;
    }

    public LocalDate getInbetriebnahmeDatum() {
        return this.inbetriebnahmeDatum;
    }

    public ChargingStation inbetriebnahmeDatum(LocalDate inbetriebnahmeDatum) {
        this.setInbetriebnahmeDatum(inbetriebnahmeDatum);
        return this;
    }

    public void setInbetriebnahmeDatum(LocalDate inbetriebnahmeDatum) {
        this.inbetriebnahmeDatum = inbetriebnahmeDatum;
    }

    public Double getNennleistungLadeeinrichtung() {
        return this.nennleistungLadeeinrichtung;
    }

    public ChargingStation nennleistungLadeeinrichtung(Double nennleistungLadeeinrichtung) {
        this.setNennleistungLadeeinrichtung(nennleistungLadeeinrichtung);
        return this;
    }

    public void setNennleistungLadeeinrichtung(Double nennleistungLadeeinrichtung) {
        this.nennleistungLadeeinrichtung = nennleistungLadeeinrichtung;
    }

    public String getArtDerLadeeinrichtung() {
        return this.artDerLadeeinrichtung;
    }

    public ChargingStation artDerLadeeinrichtung(String artDerLadeeinrichtung) {
        this.setArtDerLadeeinrichtung(artDerLadeeinrichtung);
        return this;
    }

    public void setArtDerLadeeinrichtung(String artDerLadeeinrichtung) {
        this.artDerLadeeinrichtung = artDerLadeeinrichtung;
    }

    public Integer getAnzahlLadepunkte() {
        return this.anzahlLadepunkte;
    }

    public ChargingStation anzahlLadepunkte(Integer anzahlLadepunkte) {
        this.setAnzahlLadepunkte(anzahlLadepunkte);
        return this;
    }

    public void setAnzahlLadepunkte(Integer anzahlLadepunkte) {
        this.anzahlLadepunkte = anzahlLadepunkte;
    }

    public String getErsterLadepunktSteckertypen() {
        return this.ersterLadepunktSteckertypen;
    }

    public ChargingStation ersterLadepunktSteckertypen(String ersterLadepunktSteckertypen) {
        this.setErsterLadepunktSteckertypen(ersterLadepunktSteckertypen);
        return this;
    }

    public void setErsterLadepunktSteckertypen(String ersterLadepunktSteckertypen) {
        this.ersterLadepunktSteckertypen = ersterLadepunktSteckertypen;
    }

    public Double getErsterLadepunktLeistung() {
        return this.ersterLadepunktLeistung;
    }

    public ChargingStation ersterLadepunktLeistung(Double ersterLadepunktLeistung) {
        this.setErsterLadepunktLeistung(ersterLadepunktLeistung);
        return this;
    }

    public void setErsterLadepunktLeistung(Double ersterLadepunktLeistung) {
        this.ersterLadepunktLeistung = ersterLadepunktLeistung;
    }

    public String getZweiterLadepunktSteckertypen() {
        return this.zweiterLadepunktSteckertypen;
    }

    public ChargingStation zweiterLadepunktSteckertypen(String zweiterLadepunktSteckertypen) {
        this.setZweiterLadepunktSteckertypen(zweiterLadepunktSteckertypen);
        return this;
    }

    public void setZweiterLadepunktSteckertypen(String zweiterLadepunktSteckertypen) {
        this.zweiterLadepunktSteckertypen = zweiterLadepunktSteckertypen;
    }

    public Double getZweiterLadepunktLeistung() {
        return this.zweiterLadepunktLeistung;
    }

    public ChargingStation zweiterLadepunktLeistung(Double zweiterLadepunktLeistung) {
        this.setZweiterLadepunktLeistung(zweiterLadepunktLeistung);
        return this;
    }

    public void setZweiterLadepunktLeistung(Double zweiterLadepunktLeistung) {
        this.zweiterLadepunktLeistung = zweiterLadepunktLeistung;
    }

    public String getDritterLadepunktSteckertypen() {
        return this.dritterLadepunktSteckertypen;
    }

    public ChargingStation dritterLadepunktSteckertypen(String dritterLadepunktSteckertypen) {
        this.setDritterLadepunktSteckertypen(dritterLadepunktSteckertypen);
        return this;
    }

    public void setDritterLadepunktSteckertypen(String dritterLadepunktSteckertypen) {
        this.dritterLadepunktSteckertypen = dritterLadepunktSteckertypen;
    }

    public Double getDritterLadepunktLeistung() {
        return this.dritterLadepunktLeistung;
    }

    public ChargingStation dritterLadepunktLeistung(Double dritterLadepunktLeistung) {
        this.setDritterLadepunktLeistung(dritterLadepunktLeistung);
        return this;
    }

    public void setDritterLadepunktLeistung(Double dritterLadepunktLeistung) {
        this.dritterLadepunktLeistung = dritterLadepunktLeistung;
    }

    public String getVierterLadepunktSteckertypen() {
        return this.vierterLadepunktSteckertypen;
    }

    public ChargingStation vierterLadepunktSteckertypen(String vierterLadepunktSteckertypen) {
        this.setVierterLadepunktSteckertypen(vierterLadepunktSteckertypen);
        return this;
    }

    public void setVierterLadepunktSteckertypen(String vierterLadepunktSteckertypen) {
        this.vierterLadepunktSteckertypen = vierterLadepunktSteckertypen;
    }

    public Double getVierterLadepunktLeistung() {
        return this.vierterLadepunktLeistung;
    }

    public ChargingStation vierterLadepunktLeistung(Double vierterLadepunktLeistung) {
        this.setVierterLadepunktLeistung(vierterLadepunktLeistung);
        return this;
    }

    public void setVierterLadepunktLeistung(Double vierterLadepunktLeistung) {
        this.vierterLadepunktLeistung = vierterLadepunktLeistung;
    }

    public Geometry getGeometry() {
        return this.geometry;
    }

    public ChargingStation geometry(Geometry geometry) {
        this.setGeometry(geometry);
        return this;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public AdministrationUnit getAdministrationUnit() {
        return this.administrationUnit;
    }

    public void setAdministrationUnit(AdministrationUnit administrationUnit) {
        this.administrationUnit = administrationUnit;
    }

    public ChargingStation administrationUnit(AdministrationUnit administrationUnit) {
        this.setAdministrationUnit(administrationUnit);
        return this;
    }

    public Set<Statistic> getStatistics() {
        return this.statistics;
    }

    public void setStatistics(Set<Statistic> statistics) {
        if (this.statistics != null) {
            this.statistics.forEach(i -> i.setChargingStation(null));
        }
        if (statistics != null) {
            statistics.forEach(i -> i.setChargingStation(this));
        }
        this.statistics = statistics;
    }

    public ChargingStation statistics(Set<Statistic> statistics) {
        this.setStatistics(statistics);
        return this;
    }

    public ChargingStation addStatistic(Statistic statistic) {
        this.statistics.add(statistic);
        statistic.setChargingStation(this);
        return this;
    }

    public ChargingStation removeStatistic(Statistic statistic) {
        this.statistics.remove(statistic);
        statistic.setChargingStation(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChargingStation)) {
            return false;
        }
        return id != null && id.equals(((ChargingStation) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChargingStation{" +
            "id=" + getId() +
            ", betreiber='" + getBetreiber() + "'" +
            ", strasse='" + getStrasse() + "'" +
            ", hausnummer='" + getHausnummer() + "'" +
            ", adresszusatz='" + getAdresszusatz() + "'" +
            ", postleitzahl='" + getPostleitzahl() + "'" +
            ", ort='" + getOrt() + "'" +
            ", bundesland='" + getBundesland() + "'" +
            ", kreisKreisfreieStadt='" + getKreisKreisfreieStadt() + "'" +
            ", inbetriebnahmeDatum='" + getInbetriebnahmeDatum() + "'" +
            ", nennleistungLadeeinrichtung=" + getNennleistungLadeeinrichtung() +
            ", artDerLadeeinrichtung='" + getArtDerLadeeinrichtung() + "'" +
            ", anzahlLadepunkte=" + getAnzahlLadepunkte() +
            ", ersterLadepunktSteckertypen='" + getErsterLadepunktSteckertypen() + "'" +
            ", ersterLadepunktLeistung=" + getErsterLadepunktLeistung() +
            ", zweiterLadepunktSteckertypen='" + getZweiterLadepunktSteckertypen() + "'" +
            ", zweiterLadepunktLeistung=" + getZweiterLadepunktLeistung() +
            ", dritterLadepunktSteckertypen='" + getDritterLadepunktSteckertypen() + "'" +
            ", dritterLadepunktLeistung=" + getDritterLadepunktLeistung() +
            ", vierterLadepunktSteckertypen='" + getVierterLadepunktSteckertypen() + "'" +
            ", vierterLadepunktLeistung=" + getVierterLadepunktLeistung() +
            ", geometry='" + getGeometry() + "'" +
            "}";
    }
}
