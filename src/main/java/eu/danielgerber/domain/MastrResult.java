package eu.danielgerber.domain;

import com.squareup.moshi.Json;

import java.util.List;

/**
 * A class needed to parse the JSON result from the Marktstammdatenregister.
 */
public class MastrResult {
    @Json(name = "Data")
    List<MastrUnit> data;

    public List<MastrUnit> getData() {
        return this.data;
    }
    public void setData(List<MastrUnit> data) {
        this.data = data;
    }
}
