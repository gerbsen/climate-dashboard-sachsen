package eu.danielgerber.domain.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EnergySource {

    @JsonProperty(value = "OTHER_GASES")
    OTHER_GASES(2411),
    @JsonProperty(value = "MINERAL_OIL_PRODUCTS")
    MINERAL_OIL_PRODUCTS(2409),
    @JsonProperty(value = "STORAGE")
    STORAGE(2496),
    @JsonProperty(value = "WATER")
    WATER(2498),
    @JsonProperty(value = "SLUDGE")
    SLUDGE(2405),
    @JsonProperty(value = "BIOMASS")
    BIOMASS(2493),
    @JsonProperty(value = "SOLAR_POWER")
    SOLAR_POWER(2495),
    @JsonProperty(value = "NATURAL_GAS")
    NATURAL_GAS(2410),
    @JsonProperty(value = "WIND")
    WIND(2497),
    @JsonProperty(value = "GEOTHERMICS")
    GEOTHERMICS(2403),
    @JsonProperty(value = "MINE_GAS")
    MINE_GAS(2406),
    @JsonProperty(value = "SOLAR_THERMICS")
    SOLAR_THERMICS(2404),
    @JsonProperty(value = "LIGNITE_COAL")
    LIGNITE_COAL(2408),
    @JsonProperty(value = "NONE_BIOGENIC_WASTE")
    NONE_BIOGENIC_WASTE (2412),
    @JsonProperty(value = "HEAT")
    HEAT(2413);

    private final Integer id;

    EnergySource(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "EnergySource{" +
                "id='" + id + '\'' +
                '}';
    }
}
