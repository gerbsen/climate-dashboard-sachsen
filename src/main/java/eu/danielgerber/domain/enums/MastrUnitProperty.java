package eu.danielgerber.domain.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum MastrUnitProperty {
    
    @JsonProperty(value = "GROSS_POWER")
    GROSS_POWER("brutto_leistung"),
    @JsonProperty(value = "NUMBER_OF_SOLAR_MODULES")
    NUMBER_OF_SOLAR_MODULES("anzahl_solarmodule"),
    @JsonProperty(value = "HUB_HEIGHT_WINDTURBINE")
    HUB_HEIGHT_WINDTURBINE("nabenhoehe_windenergieanlage"),
    @JsonProperty(value = "ROTOR_DIAMETER_WINDTURBINE")
    ROTOR_DIAMETER_WINDTURBINE("rotor_durchmesser_windenergieanlage"),
    @JsonProperty(value = "NET_NOMINAL_CAPACITY")
    NET_NOMINAL_CAPACITY("netto_nenn_leistung"),
    @JsonProperty(value = "USEABLE_STORAGE_CAPACITY")
    USEABLE_STORAGE_CAPACITY("nutzbare_speicher_kapazitaet");

    private final String propertyName;

    MastrUnitProperty(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyName() {
        return propertyName;
    }

    @Override
    public String toString() {
        return "MastrUnitProperty{" +
                "propertyName='" + propertyName + '\'' +
                '}';
    }
}
