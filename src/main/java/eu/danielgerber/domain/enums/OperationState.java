package eu.danielgerber.domain.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum OperationState {
 
    @JsonProperty(value = "IN_PLANING") IN_PLANING(31),
    @JsonProperty(value = "ACTIVE") ACTIVE(35),
    @JsonProperty(value = "TEMPORARILY_INACTIVE") TEMPORARILY_INACTIVE(37),
    @JsonProperty(value = "PERMANENTLY_INACTIVE") PERMANENTLY_INACTIVE(38);

    private final Integer id;

    OperationState(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "OperationState{" +
                "id='" + id + '\'' +
                '}';
    }
}
