package eu.danielgerber.domain.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum IntervalFormat {
    
    @JsonProperty(value = "YEAR")
    YEAR("YYYY"),
    @JsonProperty(value = "MONTH")
    MONTH("YYYY-mm"),
    @JsonProperty(value = "WEEK")
    WEEK("IYYY-IW");

    private final String format;

    IntervalFormat(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }

    @Override
    public String toString() {
        return "IntervalFormat{" +
                "format='" + format + '\'' +
                '}';
    }
}
