package eu.danielgerber.domain.enums;

public enum DWDState {
    
    BRANDENBURG_BERLIN("brandenburg_berlin"),
    BRANDENBURG("brandenburg"),
    BADEN_WUERTTEMBERG("baden_wuerttemberg"),
    BAYERN("bayern"),
    HESSEN("hessen"),
    MECKLENBURG_VORPOMMERN("mecklenburg_vorpommern"),
    NIEDERSACHSEN("niedersachsen"),
    NIEDERSACHSEN_HAMBURG_BREMEN("niedersachsen_hamburg_bremen"),
    NORDRHEIN_WESTFALEN("nordrhein_westfalen"),
    RHEINLAND_PFALZ("rheinland_pfalz"),
    SCHLESWIG_HOLSTEIN("schleswig_holstein"),
    SAARLAND("saarland"),
    SACHSEN("sachsen"),
    SACHSEN_ANHALT("sachsen_anhalt"),
    THUERINGEN_SACHSEN_ANHALT("thueringen_sachsen_anhalt"),
    THUERINGEN("thueringen"),
    DEUTSCHLAND("deutschland");

    private final String name;

    DWDState(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
