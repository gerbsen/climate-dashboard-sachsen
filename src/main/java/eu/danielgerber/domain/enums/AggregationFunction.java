package eu.danielgerber.domain.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum AggregationFunction {
    
    @JsonProperty(value = "SUM") SUM, 
    @JsonProperty(value = "AVG") AVG, 
    @JsonProperty(value = "MIN") MIN, 
    @JsonProperty(value = "MAX") MAX;

    @Override
    public String toString() {
        return name();
    }
}
