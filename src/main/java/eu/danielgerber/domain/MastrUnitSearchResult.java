package eu.danielgerber.domain;

import com.squareup.moshi.Json;
import io.swagger.v3.oas.annotations.media.Schema;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Schema(description = "Representation of a search result from the Marktstammdatenregister (Mastr) search API")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MastrUnitSearchResult {

    @Json(name = "url")
    private String url;

    public MastrUnitSearchResult(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
