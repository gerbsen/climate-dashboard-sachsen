package eu.danielgerber.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A UFZMeasurement.
 */
@Entity
@Table(name = "ufz_measurement")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UFZMeasurement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    /**
     * the soil moisture index from the ufz for 1,8m
     */
    @Schema(description = "the soil moisture index from the ufz for 1,8m")
    @Column(name = "smi_gesamtboden")
    private Double smiGesamtboden;

    /**
     * the soil moisture index from the ufz for 0,25m
     */
    @Schema(description = "the soil moisture index from the ufz for 0,25m")
    @Column(name = "smi_oberboden")
    private Double smiOberboden;

    /**
     * for plant available water
     */
    @Schema(description = "for plant available water")
    @Column(name = "nutzbare_feldkapazitaet")
    private Double nutzbareFeldkapazitaet;

    /**
     * the date of the measurement
     */
    @Schema(description = "the date of the measurement")
    @Column(name = "date")
    private LocalDate date;

    @ManyToOne
    @JsonIgnoreProperties(value = { "ufzMeasurements" }, allowSetters = true)
    private UFZRasterPoint ufzRasterPoint;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public UFZMeasurement id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getSmiGesamtboden() {
        return this.smiGesamtboden;
    }

    public UFZMeasurement smiGesamtboden(Double smiGesamtboden) {
        this.setSmiGesamtboden(smiGesamtboden);
        return this;
    }

    public void setSmiGesamtboden(Double smiGesamtboden) {
        this.smiGesamtboden = smiGesamtboden;
    }

    public Double getSmiOberboden() {
        return this.smiOberboden;
    }

    public UFZMeasurement smiOberboden(Double smiOberboden) {
        this.setSmiOberboden(smiOberboden);
        return this;
    }

    public void setSmiOberboden(Double smiOberboden) {
        this.smiOberboden = smiOberboden;
    }

    public Double getNutzbareFeldkapazitaet() {
        return this.nutzbareFeldkapazitaet;
    }

    public UFZMeasurement nutzbareFeldkapazitaet(Double nutzbareFeldkapazitaet) {
        this.setNutzbareFeldkapazitaet(nutzbareFeldkapazitaet);
        return this;
    }

    public void setNutzbareFeldkapazitaet(Double nutzbareFeldkapazitaet) {
        this.nutzbareFeldkapazitaet = nutzbareFeldkapazitaet;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public UFZMeasurement date(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public UFZRasterPoint getUfzRasterPoint() {
        return this.ufzRasterPoint;
    }

    public void setUfzRasterPoint(UFZRasterPoint uFZRasterPoint) {
        this.ufzRasterPoint = uFZRasterPoint;
    }

    public UFZMeasurement ufzRasterPoint(UFZRasterPoint uFZRasterPoint) {
        this.setUfzRasterPoint(uFZRasterPoint);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UFZMeasurement)) {
            return false;
        }
        return id != null && id.equals(((UFZMeasurement) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UFZMeasurement{" +
            "id=" + getId() +
            ", smiGesamtboden=" + getSmiGesamtboden() +
            ", smiOberboden=" + getSmiOberboden() +
            ", nutzbareFeldkapazitaet=" + getNutzbareFeldkapazitaet() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
