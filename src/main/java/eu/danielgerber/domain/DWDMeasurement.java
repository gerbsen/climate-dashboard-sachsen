package eu.danielgerber.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A DWDMeasurement.
 */
@Entity
@Table(name = "dwd_measurement")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DWDMeasurement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    /**
     * year of the measurement
     */
    @Schema(description = "year of the measurement")
    @Column(name = "year")
    private Long year;

    /**
     * the actual measurement description
     */
    @Schema(description = "the actual measurement description")
    @Column(name = "statistic")
    private String statistic;

    @Column(name = "brandenburg_berlin")
    private Double brandenburgBerlin;

    @Column(name = "brandenburg")
    private Double brandenburg;

    @Column(name = "baden_wuerttemberg")
    private Double badenWuerttemberg;

    @Column(name = "bayern")
    private Double bayern;

    @Column(name = "hessen")
    private Double hessen;

    @Column(name = "mecklenburg_vorpommern")
    private Double mecklenburgVorpommern;

    @Column(name = "niedersachsen")
    private Double niedersachsen;

    @Column(name = "niedersachsen_hamburg_bremen")
    private Double niedersachsenHamburgBremen;

    @Column(name = "nordrhein_westfalen")
    private Double nordrheinWestfalen;

    @Column(name = "rheinland_pfalz")
    private Double rheinlandPfalz;

    @Column(name = "schleswig_holstein")
    private Double schleswigHolstein;

    @Column(name = "saarland")
    private Double saarland;

    @Column(name = "sachsen")
    private Double sachsen;

    @Column(name = "sachsen_anhalt")
    private Double sachsenAnhalt;

    @Column(name = "thueringen_sachsen_anhalt")
    private Double thueringenSachsenAnhalt;

    @Column(name = "thueringen")
    private Double thueringen;

    @Column(name = "deutschland")
    private Double deutschland;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public DWDMeasurement id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getYear() {
        return this.year;
    }

    public DWDMeasurement year(Long year) {
        this.setYear(year);
        return this;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public String getStatistic() {
        return this.statistic;
    }

    public DWDMeasurement statistic(String statistic) {
        this.setStatistic(statistic);
        return this;
    }

    public void setStatistic(String statistic) {
        this.statistic = statistic;
    }

    public Double getBrandenburgBerlin() {
        return this.brandenburgBerlin;
    }

    public DWDMeasurement brandenburgBerlin(Double brandenburgBerlin) {
        this.setBrandenburgBerlin(brandenburgBerlin);
        return this;
    }

    public void setBrandenburgBerlin(Double brandenburgBerlin) {
        this.brandenburgBerlin = brandenburgBerlin;
    }

    public Double getBrandenburg() {
        return this.brandenburg;
    }

    public DWDMeasurement brandenburg(Double brandenburg) {
        this.setBrandenburg(brandenburg);
        return this;
    }

    public void setBrandenburg(Double brandenburg) {
        this.brandenburg = brandenburg;
    }

    public Double getBadenWuerttemberg() {
        return this.badenWuerttemberg;
    }

    public DWDMeasurement badenWuerttemberg(Double badenWuerttemberg) {
        this.setBadenWuerttemberg(badenWuerttemberg);
        return this;
    }

    public void setBadenWuerttemberg(Double badenWuerttemberg) {
        this.badenWuerttemberg = badenWuerttemberg;
    }

    public Double getBayern() {
        return this.bayern;
    }

    public DWDMeasurement bayern(Double bayern) {
        this.setBayern(bayern);
        return this;
    }

    public void setBayern(Double bayern) {
        this.bayern = bayern;
    }

    public Double getHessen() {
        return this.hessen;
    }

    public DWDMeasurement hessen(Double hessen) {
        this.setHessen(hessen);
        return this;
    }

    public void setHessen(Double hessen) {
        this.hessen = hessen;
    }

    public Double getMecklenburgVorpommern() {
        return this.mecklenburgVorpommern;
    }

    public DWDMeasurement mecklenburgVorpommern(Double mecklenburgVorpommern) {
        this.setMecklenburgVorpommern(mecklenburgVorpommern);
        return this;
    }

    public void setMecklenburgVorpommern(Double mecklenburgVorpommern) {
        this.mecklenburgVorpommern = mecklenburgVorpommern;
    }

    public Double getNiedersachsen() {
        return this.niedersachsen;
    }

    public DWDMeasurement niedersachsen(Double niedersachsen) {
        this.setNiedersachsen(niedersachsen);
        return this;
    }

    public void setNiedersachsen(Double niedersachsen) {
        this.niedersachsen = niedersachsen;
    }

    public Double getNiedersachsenHamburgBremen() {
        return this.niedersachsenHamburgBremen;
    }

    public DWDMeasurement niedersachsenHamburgBremen(Double niedersachsenHamburgBremen) {
        this.setNiedersachsenHamburgBremen(niedersachsenHamburgBremen);
        return this;
    }

    public void setNiedersachsenHamburgBremen(Double niedersachsenHamburgBremen) {
        this.niedersachsenHamburgBremen = niedersachsenHamburgBremen;
    }

    public Double getNordrheinWestfalen() {
        return this.nordrheinWestfalen;
    }

    public DWDMeasurement nordrheinWestfalen(Double nordrheinWestfalen) {
        this.setNordrheinWestfalen(nordrheinWestfalen);
        return this;
    }

    public void setNordrheinWestfalen(Double nordrheinWestfalen) {
        this.nordrheinWestfalen = nordrheinWestfalen;
    }

    public Double getRheinlandPfalz() {
        return this.rheinlandPfalz;
    }

    public DWDMeasurement rheinlandPfalz(Double rheinlandPfalz) {
        this.setRheinlandPfalz(rheinlandPfalz);
        return this;
    }

    public void setRheinlandPfalz(Double rheinlandPfalz) {
        this.rheinlandPfalz = rheinlandPfalz;
    }

    public Double getSchleswigHolstein() {
        return this.schleswigHolstein;
    }

    public DWDMeasurement schleswigHolstein(Double schleswigHolstein) {
        this.setSchleswigHolstein(schleswigHolstein);
        return this;
    }

    public void setSchleswigHolstein(Double schleswigHolstein) {
        this.schleswigHolstein = schleswigHolstein;
    }

    public Double getSaarland() {
        return this.saarland;
    }

    public DWDMeasurement saarland(Double saarland) {
        this.setSaarland(saarland);
        return this;
    }

    public void setSaarland(Double saarland) {
        this.saarland = saarland;
    }

    public Double getSachsen() {
        return this.sachsen;
    }

    public DWDMeasurement sachsen(Double sachsen) {
        this.setSachsen(sachsen);
        return this;
    }

    public void setSachsen(Double sachsen) {
        this.sachsen = sachsen;
    }

    public Double getSachsenAnhalt() {
        return this.sachsenAnhalt;
    }

    public DWDMeasurement sachsenAnhalt(Double sachsenAnhalt) {
        this.setSachsenAnhalt(sachsenAnhalt);
        return this;
    }

    public void setSachsenAnhalt(Double sachsenAnhalt) {
        this.sachsenAnhalt = sachsenAnhalt;
    }

    public Double getThueringenSachsenAnhalt() {
        return this.thueringenSachsenAnhalt;
    }

    public DWDMeasurement thueringenSachsenAnhalt(Double thueringenSachsenAnhalt) {
        this.setThueringenSachsenAnhalt(thueringenSachsenAnhalt);
        return this;
    }

    public void setThueringenSachsenAnhalt(Double thueringenSachsenAnhalt) {
        this.thueringenSachsenAnhalt = thueringenSachsenAnhalt;
    }

    public Double getThueringen() {
        return this.thueringen;
    }

    public DWDMeasurement thueringen(Double thueringen) {
        this.setThueringen(thueringen);
        return this;
    }

    public void setThueringen(Double thueringen) {
        this.thueringen = thueringen;
    }

    public Double getDeutschland() {
        return this.deutschland;
    }

    public DWDMeasurement deutschland(Double deutschland) {
        this.setDeutschland(deutschland);
        return this;
    }

    public void setDeutschland(Double deutschland) {
        this.deutschland = deutschland;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DWDMeasurement)) {
            return false;
        }
        return id != null && id.equals(((DWDMeasurement) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DWDMeasurement{" +
            "id=" + getId() +
            ", year=" + getYear() +
            ", statistic='" + getStatistic() + "'" +
            ", brandenburgBerlin=" + getBrandenburgBerlin() +
            ", brandenburg=" + getBrandenburg() +
            ", badenWuerttemberg=" + getBadenWuerttemberg() +
            ", bayern=" + getBayern() +
            ", hessen=" + getHessen() +
            ", mecklenburgVorpommern=" + getMecklenburgVorpommern() +
            ", niedersachsen=" + getNiedersachsen() +
            ", niedersachsenHamburgBremen=" + getNiedersachsenHamburgBremen() +
            ", nordrheinWestfalen=" + getNordrheinWestfalen() +
            ", rheinlandPfalz=" + getRheinlandPfalz() +
            ", schleswigHolstein=" + getSchleswigHolstein() +
            ", saarland=" + getSaarland() +
            ", sachsen=" + getSachsen() +
            ", sachsenAnhalt=" + getSachsenAnhalt() +
            ", thueringenSachsenAnhalt=" + getThueringenSachsenAnhalt() +
            ", thueringen=" + getThueringen() +
            ", deutschland=" + getDeutschland() +
            "}";
    }
}
