package eu.danielgerber.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;
import org.locationtech.jts.geom.Geometry;

/**
 * Representation of a administrationUnit (Kreis/kreisfreie Stadt)
 */
@Schema(description = "Representation of a administrationUnit (Kreis/kreisfreie Stadt)")
@Entity
@Table(name = "administration_unit")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AdministrationUnit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    /**
     * unique object identificator
     */
    @Schema(description = "unique object identificator")
    @Column(name = "objektidentifikator")
    private String objektidentifikator;

    /**
     * date added to this dataset
     */
    @Schema(description = "date added to this dataset")
    @Column(name = "beginnlebenszeit")
    private LocalDate beginnlebenszeit;

    /**
     * administrational level\n1 = Staat\n2 = Land\n3 = Regierungsbezirk\n4 = Kreis\n5 = Verwaltungsgemeinschaft\n6 = Gemeinde
     */
    @Schema(
        description = "administrational level\n1 = Staat\n2 = Land\n3 = Regierungsbezirk\n4 = Kreis\n5 = Verwaltungsgemeinschaft\n6 = Gemeinde"
    )
    @Column(name = "admin_ebene_ade")
    private String adminEbeneAde;

    /**
     * administrational level key
     */
    @Schema(description = "administrational level key")
    @Column(name = "ade")
    private Integer ade;

    /**
     * The areas in which there are no further levels below the state level are indicated as \"without structure\".
     */
    @Schema(description = "The areas in which there are no further levels below the state level are indicated as \"without structure\".")
    @Column(name = "geofaktor_gf")
    private String geofaktorGf;

    /**
     * geofaktor key
     */
    @Schema(description = "geofaktor key")
    @Column(name = "gf")
    private Long gf;

    /**
     * special areas name
     */
    @Schema(description = "special areas name")
    @Column(name = "besondere_gebiete_bsg")
    private String besondereGebieteBsg;

    /**
     * special areas key
     */
    @Schema(description = "special areas key")
    @Column(name = "bsg")
    private Long bsg;

    /**
     * This key (Amtlicher Regionalschlüssel) is the statistical key. It is structured hierarchically and reflects the\ndifferent administrative levels as existing in the Federal Republic of Germany.\nThe territorial code (ARS) is broken down as follows:\n- 1st – 2st digit = identification number of the Land\n- 3rd digit = identification number of the administrative administrationUnit\n- 4th – 5th digit = identification number of the administrationUnit (county)\n- 6th – 9th digit = identification number of the administrative association\n- 10th – 12th digit = community identification number\nThe ARS is also used to link to the other information tables.
     */
    @Schema(
        description = "This key (Amtlicher Regionalschlüssel) is the statistical key. It is structured hierarchically and reflects the\ndifferent administrative levels as existing in the Federal Republic of Germany.\nThe territorial code (ARS) is broken down as follows:\n- 1st – 2st digit = identification number of the Land\n- 3rd digit = identification number of the administrative administrationUnit\n- 4th – 5th digit = identification number of the administrationUnit (county)\n- 6th – 9th digit = identification number of the administrative association\n- 10th – 12th digit = community identification number\nThe ARS is also used to link to the other information tables."
    )
    @Column(name = "regionalschluessel_ars")
    private String regionalschluesselArs;

    /**
     * The key is structured hierarchically and is derived from the ARS shortened by the key number of the administrative association.\nThe AGS is broken down as follows:\n- 1st – 2st digit = identification number of the Land\n- 3rd digit = identification number of the administrative administrationUnit\n- 4th – 5th digit = identification number of the administrationUnit (county)\n- 6th – 8th digit = community identification number
     */
    @Schema(
        description = "The key is structured hierarchically and is derived from the ARS shortened by the key number of the administrative association.\nThe AGS is broken down as follows:\n- 1st – 2st digit = identification number of the Land\n- 3rd digit = identification number of the administrative administrationUnit\n- 4th – 5th digit = identification number of the administrationUnit (county)\n- 6th – 8th digit = community identification number"
    )
    @Column(name = "gemeindeschluessel_ags")
    private String gemeindeschluesselAgs;

    /**
     * ars of the community where the administration resides
     */
    @Schema(description = "ars of the community where the administration resides")
    @Column(name = "verwaltungssitz_sdv_ars")
    private String verwaltungssitzSdvArs;

    /**
     * the name of the administrationUnit
     */
    @Schema(description = "the name of the administrationUnit")
    @Column(name = "geografischername_gen")
    private String geografischernameGen;

    /**
     * name of the administration unit
     */
    @Schema(description = "name of the administration unit")
    @Column(name = "bezeichnung")
    private String bezeichnung;

    /**
     * The identifier is a product-specific identification number for the attribute BEZ.
     */
    @Schema(description = "The identifier is a product-specific identification number for the attribute BEZ.")
    @Column(name = "identifikator_ibz")
    private Long identifikatorIbz;

    /**
     * comment for bezeichnung
     */
    @Schema(description = "comment for bezeichnung")
    @Column(name = "bemerkung")
    private String bemerkung;

    /**
     * indicates if „bezeichnung” should be used to build the correct name
     */
    @Schema(description = "indicates if „bezeichnung” should be used to build the correct name")
    @Column(name = "namensbildung_nbd")
    private String namensbildungNbd;

    /**
     * indicates if „bezeichnung” should be used to build the correct name
     */
    @Schema(description = "indicates if „bezeichnung” should be used to build the correct name")
    @Column(name = "nbd")
    private Boolean nbd;

    /**
     * identification number of the state
     */
    @Schema(description = "identification number of the state")
    @Column(name = "land")
    private String land;

    /**
     * identification number of the administrative administrationUnit
     */
    @Schema(description = "identification number of the administrative administrationUnit")
    @Column(name = "regierungsbezirk")
    private String regierungsbezirk;

    /**
     * identification number of the administrationUnit (county)
     */
    @Schema(description = "identification number of the administrationUnit (county)")
    @Column(name = "kreis")
    private String kreis;

    /**
     * part 1 administrativ association (see regionalschluesselArs)
     */
    @Schema(description = "part 1 administrativ association (see regionalschluesselArs)")
    @Column(name = "verwaltungsgemeinschaftteil_1")
    private String verwaltungsgemeinschaftteil1;

    /**
     * part 2 administrativ association (see regionalschluesselArs)
     */
    @Schema(description = "part 2 administrativ association (see regionalschluesselArs)")
    @Column(name = "verwaltungsgemeinschaftteil_2")
    private String verwaltungsgemeinschaftteil2;

    /**
     * always 000 for districts
     */
    @Schema(description = "always 000 for districts")
    @Column(name = "gemeinde")
    private String gemeinde;

    /**
     * Function of the 3rd key digit\nR = administrative administrationUnit\nK = administrationUnit\nIn the case of Länder (states) with administrative districts the attribute is\nassigned the value R. Länder without an administrative administrationUnit or 3-digit administrationUnit\nkey are also assigned the value R, and the 3rd key digit the value 0, respectively.\nIn the case of the Länder with a 3-digit administrationUnit key number the third key digit\nonly serves to unambiguously mark the Kreis (administrationUnit) level, and the attribute is\nassigned the value K. In these Länder there exist no longer any administrative\ndistricts.
     */
    @Schema(
        description = "Function of the 3rd key digit\nR = administrative administrationUnit\nK = administrationUnit\nIn the case of Länder (states) with administrative districts the attribute is\nassigned the value R. Länder without an administrative administrationUnit or 3-digit administrationUnit\nkey are also assigned the value R, and the 3rd key digit the value 0, respectively.\nIn the case of the Länder with a 3-digit administrationUnit key number the third key digit\nonly serves to unambiguously mark the Kreis (administrationUnit) level, and the attribute is\nassigned the value K. In these Länder there exist no longer any administrative\ndistricts."
    )
    @Column(name = "funk_schluesselstelle_3")
    private String funkSchluesselstelle3;

    /**
     * Function of the 3rd key digit short
     */
    @Schema(description = "Function of the 3rd key digit short")
    @Column(name = "fk_s_3")
    private String fkS3;

    /**
     * European statistics key
     */
    @Schema(description = "European statistics key")
    @Column(name = "europ_statistikschluessel_nuts")
    private String europStatistikschluesselNuts;

    /**
     * filled territorial code, basically 12-digit ARS (filled in with zeros on the right side)
     */
    @Schema(description = "filled territorial code, basically 12-digit ARS (filled in with zeros on the right side)")
    @Column(name = "regioschluesselaufgefuellt")
    private String regioschluesselaufgefuellt;

    /**
     * filled Official Municipality Key, basically 8-digit AGS (filled in with zeros on the right side)
     */
    @Schema(description = "filled Official Municipality Key, basically 8-digit AGS (filled in with zeros on the right side)")
    @Column(name = "gemeindeschluesselaufgefuellt")
    private String gemeindeschluesselaufgefuellt;

    /**
     * Effectiveness: The attribute describes the legally relevant date for the effectiveness of the change. This date is not communicated by all sources, so that there is no entitlement to completeness.
     */
    @Schema(
        description = "Effectiveness: The attribute describes the legally relevant date for the effectiveness of the change. This date is not communicated by all sources, so that there is no entitlement to completeness."
    )
    @Column(name = "wirksamkeit_wsk")
    private LocalDate wirksamkeitWsk;

    /**
     * the geometry for the administrationUnit, needs to be string since jhipster does not understand other types
     */
    @Schema(description = "the geometry for the administrationUnit, needs to be string since jhipster does not understand other types")
    @Column(name = "geometry")
    private Geometry geometry;

    @ManyToOne
    @JsonIgnoreProperties(value = { "district", "mastrUnits", "chargingStations", "statistics" }, allowSetters = true)
    private AdministrationUnit district;

    @OneToMany(mappedBy = "administrationUnit", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "administrationUnit" }, allowSetters = true)
    private Set<MastrUnit> mastrUnits = new HashSet<>();

    @OneToMany(mappedBy = "administrationUnit", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "administrationUnit", "statistics" }, allowSetters = true)
    private Set<ChargingStation> chargingStations = new HashSet<>();

    @OneToMany(mappedBy = "administrationUnit", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "administrationUnit", "chargingStation" }, allowSetters = true)
    private Set<Statistic> statistics = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public AdministrationUnit id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObjektidentifikator() {
        return this.objektidentifikator;
    }

    public AdministrationUnit objektidentifikator(String objektidentifikator) {
        this.setObjektidentifikator(objektidentifikator);
        return this;
    }

    public void setObjektidentifikator(String objektidentifikator) {
        this.objektidentifikator = objektidentifikator;
    }

    public LocalDate getBeginnlebenszeit() {
        return this.beginnlebenszeit;
    }

    public AdministrationUnit beginnlebenszeit(LocalDate beginnlebenszeit) {
        this.setBeginnlebenszeit(beginnlebenszeit);
        return this;
    }

    public void setBeginnlebenszeit(LocalDate beginnlebenszeit) {
        this.beginnlebenszeit = beginnlebenszeit;
    }

    public String getAdminEbeneAde() {
        return this.adminEbeneAde;
    }

    public AdministrationUnit adminEbeneAde(String adminEbeneAde) {
        this.setAdminEbeneAde(adminEbeneAde);
        return this;
    }

    public void setAdminEbeneAde(String adminEbeneAde) {
        this.adminEbeneAde = adminEbeneAde;
    }

    public Integer getAde() {
        return this.ade;
    }

    public AdministrationUnit ade(Integer ade) {
        this.setAde(ade);
        return this;
    }

    public void setAde(Integer ade) {
        this.ade = ade;
    }

    public String getGeofaktorGf() {
        return this.geofaktorGf;
    }

    public AdministrationUnit geofaktorGf(String geofaktorGf) {
        this.setGeofaktorGf(geofaktorGf);
        return this;
    }

    public void setGeofaktorGf(String geofaktorGf) {
        this.geofaktorGf = geofaktorGf;
    }

    public Long getGf() {
        return this.gf;
    }

    public AdministrationUnit gf(Long gf) {
        this.setGf(gf);
        return this;
    }

    public void setGf(Long gf) {
        this.gf = gf;
    }

    public String getBesondereGebieteBsg() {
        return this.besondereGebieteBsg;
    }

    public AdministrationUnit besondereGebieteBsg(String besondereGebieteBsg) {
        this.setBesondereGebieteBsg(besondereGebieteBsg);
        return this;
    }

    public void setBesondereGebieteBsg(String besondereGebieteBsg) {
        this.besondereGebieteBsg = besondereGebieteBsg;
    }

    public Long getBsg() {
        return this.bsg;
    }

    public AdministrationUnit bsg(Long bsg) {
        this.setBsg(bsg);
        return this;
    }

    public void setBsg(Long bsg) {
        this.bsg = bsg;
    }

    public String getRegionalschluesselArs() {
        return this.regionalschluesselArs;
    }

    public AdministrationUnit regionalschluesselArs(String regionalschluesselArs) {
        this.setRegionalschluesselArs(regionalschluesselArs);
        return this;
    }

    public void setRegionalschluesselArs(String regionalschluesselArs) {
        this.regionalschluesselArs = regionalschluesselArs;
    }

    public String getGemeindeschluesselAgs() {
        return this.gemeindeschluesselAgs;
    }

    public AdministrationUnit gemeindeschluesselAgs(String gemeindeschluesselAgs) {
        this.setGemeindeschluesselAgs(gemeindeschluesselAgs);
        return this;
    }

    public void setGemeindeschluesselAgs(String gemeindeschluesselAgs) {
        this.gemeindeschluesselAgs = gemeindeschluesselAgs;
    }

    public String getVerwaltungssitzSdvArs() {
        return this.verwaltungssitzSdvArs;
    }

    public AdministrationUnit verwaltungssitzSdvArs(String verwaltungssitzSdvArs) {
        this.setVerwaltungssitzSdvArs(verwaltungssitzSdvArs);
        return this;
    }

    public void setVerwaltungssitzSdvArs(String verwaltungssitzSdvArs) {
        this.verwaltungssitzSdvArs = verwaltungssitzSdvArs;
    }

    public String getGeografischernameGen() {
        return this.geografischernameGen;
    }

    public AdministrationUnit geografischernameGen(String geografischernameGen) {
        this.setGeografischernameGen(geografischernameGen);
        return this;
    }

    public void setGeografischernameGen(String geografischernameGen) {
        this.geografischernameGen = geografischernameGen;
    }

    public String getBezeichnung() {
        return this.bezeichnung;
    }

    public AdministrationUnit bezeichnung(String bezeichnung) {
        this.setBezeichnung(bezeichnung);
        return this;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public Long getIdentifikatorIbz() {
        return this.identifikatorIbz;
    }

    public AdministrationUnit identifikatorIbz(Long identifikatorIbz) {
        this.setIdentifikatorIbz(identifikatorIbz);
        return this;
    }

    public void setIdentifikatorIbz(Long identifikatorIbz) {
        this.identifikatorIbz = identifikatorIbz;
    }

    public String getBemerkung() {
        return this.bemerkung;
    }

    public AdministrationUnit bemerkung(String bemerkung) {
        this.setBemerkung(bemerkung);
        return this;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }

    public String getNamensbildungNbd() {
        return this.namensbildungNbd;
    }

    public AdministrationUnit namensbildungNbd(String namensbildungNbd) {
        this.setNamensbildungNbd(namensbildungNbd);
        return this;
    }

    public void setNamensbildungNbd(String namensbildungNbd) {
        this.namensbildungNbd = namensbildungNbd;
    }

    public Boolean getNbd() {
        return this.nbd;
    }

    public AdministrationUnit nbd(Boolean nbd) {
        this.setNbd(nbd);
        return this;
    }

    public void setNbd(Boolean nbd) {
        this.nbd = nbd;
    }

    public String getLand() {
        return this.land;
    }

    public AdministrationUnit land(String land) {
        this.setLand(land);
        return this;
    }

    public void setLand(String land) {
        this.land = land;
    }

    public String getRegierungsbezirk() {
        return this.regierungsbezirk;
    }

    public AdministrationUnit regierungsbezirk(String regierungsbezirk) {
        this.setRegierungsbezirk(regierungsbezirk);
        return this;
    }

    public void setRegierungsbezirk(String regierungsbezirk) {
        this.regierungsbezirk = regierungsbezirk;
    }

    public String getKreis() {
        return this.kreis;
    }

    public AdministrationUnit kreis(String kreis) {
        this.setKreis(kreis);
        return this;
    }

    public void setKreis(String kreis) {
        this.kreis = kreis;
    }

    public String getVerwaltungsgemeinschaftteil1() {
        return this.verwaltungsgemeinschaftteil1;
    }

    public AdministrationUnit verwaltungsgemeinschaftteil1(String verwaltungsgemeinschaftteil1) {
        this.setVerwaltungsgemeinschaftteil1(verwaltungsgemeinschaftteil1);
        return this;
    }

    public void setVerwaltungsgemeinschaftteil1(String verwaltungsgemeinschaftteil1) {
        this.verwaltungsgemeinschaftteil1 = verwaltungsgemeinschaftteil1;
    }

    public String getVerwaltungsgemeinschaftteil2() {
        return this.verwaltungsgemeinschaftteil2;
    }

    public AdministrationUnit verwaltungsgemeinschaftteil2(String verwaltungsgemeinschaftteil2) {
        this.setVerwaltungsgemeinschaftteil2(verwaltungsgemeinschaftteil2);
        return this;
    }

    public void setVerwaltungsgemeinschaftteil2(String verwaltungsgemeinschaftteil2) {
        this.verwaltungsgemeinschaftteil2 = verwaltungsgemeinschaftteil2;
    }

    public String getGemeinde() {
        return this.gemeinde;
    }

    public AdministrationUnit gemeinde(String gemeinde) {
        this.setGemeinde(gemeinde);
        return this;
    }

    public void setGemeinde(String gemeinde) {
        this.gemeinde = gemeinde;
    }

    public String getFunkSchluesselstelle3() {
        return this.funkSchluesselstelle3;
    }

    public AdministrationUnit funkSchluesselstelle3(String funkSchluesselstelle3) {
        this.setFunkSchluesselstelle3(funkSchluesselstelle3);
        return this;
    }

    public void setFunkSchluesselstelle3(String funkSchluesselstelle3) {
        this.funkSchluesselstelle3 = funkSchluesselstelle3;
    }

    public String getFkS3() {
        return this.fkS3;
    }

    public AdministrationUnit fkS3(String fkS3) {
        this.setFkS3(fkS3);
        return this;
    }

    public void setFkS3(String fkS3) {
        this.fkS3 = fkS3;
    }

    public String getEuropStatistikschluesselNuts() {
        return this.europStatistikschluesselNuts;
    }

    public AdministrationUnit europStatistikschluesselNuts(String europStatistikschluesselNuts) {
        this.setEuropStatistikschluesselNuts(europStatistikschluesselNuts);
        return this;
    }

    public void setEuropStatistikschluesselNuts(String europStatistikschluesselNuts) {
        this.europStatistikschluesselNuts = europStatistikschluesselNuts;
    }

    public String getRegioschluesselaufgefuellt() {
        return this.regioschluesselaufgefuellt;
    }

    public AdministrationUnit regioschluesselaufgefuellt(String regioschluesselaufgefuellt) {
        this.setRegioschluesselaufgefuellt(regioschluesselaufgefuellt);
        return this;
    }

    public void setRegioschluesselaufgefuellt(String regioschluesselaufgefuellt) {
        this.regioschluesselaufgefuellt = regioschluesselaufgefuellt;
    }

    public String getGemeindeschluesselaufgefuellt() {
        return this.gemeindeschluesselaufgefuellt;
    }

    public AdministrationUnit gemeindeschluesselaufgefuellt(String gemeindeschluesselaufgefuellt) {
        this.setGemeindeschluesselaufgefuellt(gemeindeschluesselaufgefuellt);
        return this;
    }

    public void setGemeindeschluesselaufgefuellt(String gemeindeschluesselaufgefuellt) {
        this.gemeindeschluesselaufgefuellt = gemeindeschluesselaufgefuellt;
    }

    public LocalDate getWirksamkeitWsk() {
        return this.wirksamkeitWsk;
    }

    public AdministrationUnit wirksamkeitWsk(LocalDate wirksamkeitWsk) {
        this.setWirksamkeitWsk(wirksamkeitWsk);
        return this;
    }

    public void setWirksamkeitWsk(LocalDate wirksamkeitWsk) {
        this.wirksamkeitWsk = wirksamkeitWsk;
    }

    public Geometry getGeometry() {
        return this.geometry;
    }

    public AdministrationUnit geometry(Geometry geometry) {
        this.setGeometry(geometry);
        return this;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public AdministrationUnit getDistrict() {
        return this.district;
    }

    public void setDistrict(AdministrationUnit administrationUnit) {
        this.district = administrationUnit;
    }

    public AdministrationUnit district(AdministrationUnit administrationUnit) {
        this.setDistrict(administrationUnit);
        return this;
    }

    public Set<MastrUnit> getMastrUnits() {
        return this.mastrUnits;
    }

    public void setMastrUnits(Set<MastrUnit> mastrUnits) {
        if (this.mastrUnits != null) {
            this.mastrUnits.forEach(i -> i.setAdministrationUnit(null));
        }
        if (mastrUnits != null) {
            mastrUnits.forEach(i -> i.setAdministrationUnit(this));
        }
        this.mastrUnits = mastrUnits;
    }

    public AdministrationUnit mastrUnits(Set<MastrUnit> mastrUnits) {
        this.setMastrUnits(mastrUnits);
        return this;
    }

    public AdministrationUnit addMastrUnit(MastrUnit mastrUnit) {
        this.mastrUnits.add(mastrUnit);
        mastrUnit.setAdministrationUnit(this);
        return this;
    }

    public AdministrationUnit removeMastrUnit(MastrUnit mastrUnit) {
        this.mastrUnits.remove(mastrUnit);
        mastrUnit.setAdministrationUnit(null);
        return this;
    }

    public Set<ChargingStation> getChargingStations() {
        return this.chargingStations;
    }

    public void setChargingStations(Set<ChargingStation> chargingStations) {
        if (this.chargingStations != null) {
            this.chargingStations.forEach(i -> i.setAdministrationUnit(null));
        }
        if (chargingStations != null) {
            chargingStations.forEach(i -> i.setAdministrationUnit(this));
        }
        this.chargingStations = chargingStations;
    }

    public AdministrationUnit chargingStations(Set<ChargingStation> chargingStations) {
        this.setChargingStations(chargingStations);
        return this;
    }

    public AdministrationUnit addChargingStation(ChargingStation chargingStation) {
        this.chargingStations.add(chargingStation);
        chargingStation.setAdministrationUnit(this);
        return this;
    }

    public AdministrationUnit removeChargingStation(ChargingStation chargingStation) {
        this.chargingStations.remove(chargingStation);
        chargingStation.setAdministrationUnit(null);
        return this;
    }

    public Set<Statistic> getStatistics() {
        return this.statistics;
    }

    public void setStatistics(Set<Statistic> statistics) {
        if (this.statistics != null) {
            this.statistics.forEach(i -> i.setAdministrationUnit(null));
        }
        if (statistics != null) {
            statistics.forEach(i -> i.setAdministrationUnit(this));
        }
        this.statistics = statistics;
    }

    public AdministrationUnit statistics(Set<Statistic> statistics) {
        this.setStatistics(statistics);
        return this;
    }

    public AdministrationUnit addStatistic(Statistic statistic) {
        this.statistics.add(statistic);
        statistic.setAdministrationUnit(this);
        return this;
    }

    public AdministrationUnit removeStatistic(Statistic statistic) {
        this.statistics.remove(statistic);
        statistic.setAdministrationUnit(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AdministrationUnit)) {
            return false;
        }
        return id != null && id.equals(((AdministrationUnit) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AdministrationUnit{" +
            "id=" + getId() +
            ", objektidentifikator='" + getObjektidentifikator() + "'" +
            ", beginnlebenszeit='" + getBeginnlebenszeit() + "'" +
            ", adminEbeneAde='" + getAdminEbeneAde() + "'" +
            ", ade=" + getAde() +
            ", geofaktorGf='" + getGeofaktorGf() + "'" +
            ", gf=" + getGf() +
            ", besondereGebieteBsg='" + getBesondereGebieteBsg() + "'" +
            ", bsg=" + getBsg() +
            ", regionalschluesselArs='" + getRegionalschluesselArs() + "'" +
            ", gemeindeschluesselAgs='" + getGemeindeschluesselAgs() + "'" +
            ", verwaltungssitzSdvArs='" + getVerwaltungssitzSdvArs() + "'" +
            ", geografischernameGen='" + getGeografischernameGen() + "'" +
            ", bezeichnung='" + getBezeichnung() + "'" +
            ", identifikatorIbz=" + getIdentifikatorIbz() +
            ", bemerkung='" + getBemerkung() + "'" +
            ", namensbildungNbd='" + getNamensbildungNbd() + "'" +
            ", nbd='" + getNbd() + "'" +
            ", land='" + getLand() + "'" +
            ", regierungsbezirk='" + getRegierungsbezirk() + "'" +
            ", kreis='" + getKreis() + "'" +
            ", verwaltungsgemeinschaftteil1='" + getVerwaltungsgemeinschaftteil1() + "'" +
            ", verwaltungsgemeinschaftteil2='" + getVerwaltungsgemeinschaftteil2() + "'" +
            ", gemeinde='" + getGemeinde() + "'" +
            ", funkSchluesselstelle3='" + getFunkSchluesselstelle3() + "'" +
            ", fkS3='" + getFkS3() + "'" +
            ", europStatistikschluesselNuts='" + getEuropStatistikschluesselNuts() + "'" +
            ", regioschluesselaufgefuellt='" + getRegioschluesselaufgefuellt() + "'" +
            ", gemeindeschluesselaufgefuellt='" + getGemeindeschluesselaufgefuellt() + "'" +
            ", wirksamkeitWsk='" + getWirksamkeitWsk() + "'" +
            ", geometry='" + getGeometry() + "'" +
            "}";
    }

    /**
     * Returns the **first** statistic for this unit that matches the 
     * given key or null if key _does_ not match.
     * 
     * @param key of the statistic
     * @return the matchin statistic of this unit or null
     */
    public Statistic getStatisticByKey(String key) {
        return this.statistics.stream().filter(s -> key.equals(s.getKey())).findFirst().orElse(null);
    }
}
