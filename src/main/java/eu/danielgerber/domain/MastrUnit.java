package eu.danielgerber.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.squareup.moshi.Json;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.locationtech.jts.geom.Geometry;

/**
 * Representation of an entry in the Marktstammdatenregister (Mastr)
 */
@Schema(description = "Representation of an entry in the Marktstammdatenregister (Mastr)")
@Entity
@Table(name = "mastr_unit")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class MastrUnit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "mastr_nummer")
    @Json(name = "MaStRNummer")
    private String mastrNummer;

    @Column(name = "anlagen_betreiber_id")
    @Json(name = "AnlagenbetreiberId")
    private Long anlagenBetreiberId;

    @Column(name = "anlagen_betreiber_personen_art")
    @Json(name = "AnlagenbetreiberPersonenArt")
    private Long anlagenBetreiberPersonenArt;

    @Column(name = "anlagen_betreiber_masked_name")
    @Json(name = "AnlagenbetreiberMaskedName")
    private String anlagenBetreiberMaskedName;

    @Column(name = "anlagen_betreiber_mastr_nummer")
    @Json(name = "AnlagenbetreiberMaStRNummer")
    private String anlagenBetreiberMastrNummer;

    @Column(name = "anlagen_betreiber_name")
    @Json(name = "AnlagenbetreiberName")
    private String anlagenBetreiberName;

    @Column(name = "betriebs_status_id")
    @Json(name = "BetriebsStatusId")
    private Integer betriebsStatusId;

    @Column(name = "betriebs_status_name")
    @Json(name = "BetriebsStatusName")
    private String betriebsStatusName;

    @Column(name = "bundesland")
    @Json(name = "Bundesland")
    private String bundesland;

    @Column(name = "bundesland_id")
    @Json(name = "BundeslandId")
    private Integer bundeslandId;

    @Column(name = "landkreis_id")
    @Json(name = "LandkreisId")
    private Integer landkreisId;

    @Column(name = "gemeinde_id")
    @Json(name = "GemeindeId")
    private Integer gemeindeId;

    @Column(name = "datum_letzte_aktualisierung")
    @Json(name = "DatumLetzteAktualisierung")
    private LocalDate datumLetzteAktualisierung;

    @Column(name = "einheit_registrierungs_datum")
    @Json(name = "EinheitRegistrierungsdatum")
    private LocalDate einheitRegistrierungsDatum;

    @Column(name = "endgueltige_stilllegung_datum")
    @Json(name = "EndgueltigeStilllegungDatum")
    private LocalDate endgueltigeStilllegungDatum;

    @Column(name = "geplantes_inbetriebsnahme_datum")
    @Json(name = "GeplantesInbetriebsnahmeDatum")
    private LocalDate geplantesInbetriebsnahmeDatum;

    @Column(name = "inbetriebnahme_datum")
    @Json(name = "InbetriebnahmeDatum")
    private LocalDate inbetriebnahmeDatum;

    @Column(name = "kwk_anlage_inbetriebnahme_datum")
    @Json(name = "KwkAnlageInbetriebnahmedatum")
    private LocalDate kwkAnlageInbetriebnahmeDatum;

    @Column(name = "kwk_anlage_registrierungs_datum")
    @Json(name = "KwkAnlageRegistrierungsdatum")
    private LocalDate kwkAnlageRegistrierungsDatum;

    @Column(name = "eeg_inbetriebnahme_datum")
    @Json(name = "EegInbetriebnahmeDatum")
    private LocalDate eegInbetriebnahmeDatum;

    @Column(name = "eeg_anlage_registrierungs_datum")
    @Json(name = "EegAnlageRegistrierungsdatum")
    private LocalDate eegAnlageRegistrierungsDatum;

    @Column(name = "genehmigung_datum")
    @Json(name = "GenehmigungDatum")
    private LocalDate genehmigungDatum;

    @Column(name = "genehmigung_registrierungs_datum")
    @Json(name = "GenehmigungRegistrierungsdatum")
    private LocalDate genehmigungRegistrierungsDatum;

    /**
     * possible values: 2954, 2955, null
     */
    @Schema(description = "possible values: 2954, 2955, null")
    @Column(name = "is_nb_pruefung_abgeschlossen")
    @Json(name = "IsNBPruefungAbgeschlossen")
    private Integer isNbPruefungAbgeschlossen;

    @Column(name = "is_anonymisiert")
    @Json(name = "IsAnonymisiert")
    private Boolean isAnonymisiert;

    @Column(name = "is_buerger_energie")
    @Json(name = "IsBuergerenergie")
    private Boolean isBuergerEnergie;

    @Column(name = "is_einheit_notstromaggregat")
    @Json(name = "IsEinheitNotstromaggregat")
    private Boolean isEinheitNotstromaggregat;

    @Column(name = "is_mieterstrom_angemeldet")
    @Json(name = "MieterstromAngemeldet")
    private Boolean isMieterstromAngemeldet;

    @Column(name = "is_wasserkraft_ertuechtigung")
    @Json(name = "WasserkraftErtuechtigung")
    private Boolean isWasserkraftErtuechtigung;

    @Column(name = "is_pilot_windanlage")
    private Boolean isPilotWindanlage;

    @Column(name = "is_prototyp_anlage")
    private Boolean isPrototypAnlage;

    @Column(name = "lat")
    @Json(name = "Breitengrad")
    private Double lat;

    @Column(name = "lng")
    @Json(name = "Laengengrad")
    private Double lng;

    @Column(name = "ort")
    @Json(name = "Ort")
    private String ort;

    @Column(name = "plz")
    @Json(name = "Plz")
    private Integer plz;

    @Column(name = "strasse")
    @Json(name = "Strasse")
    private String strasse;

    @Column(name = "hausnummer")
    @Json(name = "Hausnummer")
    private String hausnummer;

    @Column(name = "einheitname")
    @Json(name = "EinheitName")
    private String einheitname;

    @Column(name = "flurstueck")
    @Json(name = "Flurstueck")
    private String flurstueck;

    @Column(name = "gemarkung")
    @Json(name = "Gemarkung")
    private String gemarkung;

    @Column(name = "gemeinde")
    @Json(name = "Gemeinde")
    private String gemeinde;

    @Column(name = "land_id")
    @Json(name = "LandId")
    private Integer landId;

    @Column(name = "landkreis")
    @Json(name = "Landkreis")
    private String landkreis;

    @Column(name = "ags")
    @Json(name = "Gemeindeschluessel")
    private Long ags;

    @Column(name = "lokation_id")
    @Json(name = "LokationId")
    private Long lokationId;

    @Column(name = "lokation_mastr_nr")
    @Json(name = "LokationMastrNr")
    private String lokationMastrNr;

    /**
     * possible values: '1000596; 1001319' or '1001319' :facepalm
     */
    @Schema(description = "possible values: '1000596; 1001319' or '1001319' :facepalm")
    @Column(name = "netzbetreiber_id")
    @Json(name = "NetzbetreiberId")
    private String netzbetreiberId;

    @Column(name = "netzbetreiber_masked_namen")
    @Json(name = "NetzbetreiberMaskedNamen")
    private String netzbetreiberMaskedNamen;

    @Column(name = "netzbetreiber_mastr_nummer")
    @Json(name = "NetzbetreiberMaStRNummer")
    private String netzbetreiberMastrNummer;

    @Column(name = "netzbetreiber_namen")
    @Json(name = "NetzbetreiberNamen")
    private String netzbetreiberNamen;

    /**
     * possible values: '517; 517' or '517' or null :facepalm
     */
    @Schema(description = "possible values: '517; 517' or '517' or null :facepalm")
    @Column(name = "netzbetreiber_personen_art")
    @Json(name = "NetzbetreiberPersonenArt")
    private String netzbetreiberPersonenArt;

    @Column(name = "system_status_id")
    @Json(name = "SystemStatusId")
    private Integer systemStatusId;

    @Column(name = "system_status_name")
    @Json(name = "SystemStatusName")
    private String systemStatusName;

    @Column(name = "typ")
    @Json(name = "Typ")
    private Integer typ;

    @Column(name = "aktenzeichen_genehmigung")
    @Json(name = "AktenzeichenGenehmigung")
    private String aktenzeichenGenehmigung;

    @Column(name = "anzahl_solarmodule")
    @Json(name = "AnzahlSolarModule")
    private Integer anzahlSolarmodule;

    /**
     * 727, 729, 728, 732, 731, 730, null
     */
    @Schema(description = "727, 729, 728, 732, 731, 730, null")
    @Column(name = "batterie_technologie")
    @Json(name = "Batterietechnologie")
    private Integer batterieTechnologie;

    @Column(name = "brutto_leistung")
    @Json(name = "Bruttoleistung")
    private Double bruttoLeistung;

    @Column(name = "eeg_installierte_leistung")
    @Json(name = "EegInstallierteLeistung")
    private Double eegInstallierteLeistung;

    @Column(name = "eeg_anlage_mastr_nummer")
    @Json(name = "EegAnlageMastrNummer")
    private String eegAnlageMastrNummer;

    @Column(name = "eeg_anlagen_schluessel")
    @Json(name = "EegAnlagenschluessel")
    private String eegAnlagenSchluessel;

    @Column(name = "eeg_zuschlag")
    @Json(name = "EegZuschlag")
    private String eegZuschlag;

    @Column(name = "zuschlags_nummern")
    @Json(name = "Zuschlagsnummern")
    private String zuschlagsNummern;

    @Column(name = "energie_traeger_id")
    @Json(name = "EnergietraegerId")
    private Integer energieTraegerId;

    @Column(name = "energie_traeger_name")
    @Json(name = "EnergietraegerName")
    private String energieTraegerName;

    /**
     * 1450, 1449, 1448, null
     */
    @Schema(description = "1450, 1449, 1448, null")
    @Column(name = "gemeinsamer_wechselrichter")
    @Json(name = "GemeinsamerWechselrichter")
    private Integer gemeinsamerWechselrichter;

    @Column(name = "genehmigung_behoerde")
    @Json(name = "Genehmigungbehoerde")
    private String genehmigungBehoerde;

    @Column(name = "genehmigungs_mastr_nummer")
    @Json(name = "GenehmigungsMastrNummer")
    private String genehmigungsMastrNummer;

    @Column(name = "gruppierungs_objekte")
    @Json(name = "Gruppierungsobjekte")
    private String gruppierungsObjekte;

    @Column(name = "gruppierungs_objekte_ids")
    @Json(name = "GruppierungsobjekteIds")
    private String gruppierungsObjekteIds;

    @Column(name = "hat_flexibilitaets_praemie")
    @Json(name = "HatFlexibilitaetspraemie")
    private Boolean hatFlexibilitaetsPraemie;

    @Column(name = "haupt_ausrichtung_solarmodule")
    @Json(name = "HauptausrichtungSolarModule")
    private Integer hauptAusrichtungSolarmodule;

    @Column(name = "haupt_ausrichtung_solarmodule_bezeichnung")
    @Json(name = "HauptausrichtungSolarModuleBezeichnung")
    private String hauptAusrichtungSolarmoduleBezeichnung;

    @Column(name = "haupt_brennstoff_id")
    @Json(name = "HauptbrennstoffId")
    private Integer hauptBrennstoffId;

    @Column(name = "haupt_brennstoff_namen")
    @Json(name = "HauptbrennstoffNamen")
    private String hauptBrennstoffNamen;

    @Column(name = "haupt_neigungswinkel_solar_module")
    @Json(name = "HauptneigungswinkelSolarmodule")
    private Integer hauptNeigungswinkelSolarModule;

    @Column(name = "hersteller_windenergieanlage_id")
    private Integer herstellerWindenergieanlageId;

    @Column(name = "hersteller_windenergieanlage_bezeichnung")
    @Json(name = "HerstellerWindenergieanlageBezeichnung")
    private String herstellerWindenergieanlageBezeichnung;

    @Column(name = "kwk_anlage_elektrische_leistung")
    @Json(name = "KwkAnlageElektrischeLeistung")
    private Double kwkAnlageElektrischeLeistung;

    @Column(name = "kwk_anlage_mastr_nummer")
    @Json(name = "KwkAnlageMastrNummer")
    private String kwkAnlageMastrNummer;

    @Column(name = "kwk_zuschlag")
    @Json(name = "KwkZuschlag")
    private String kwkZuschlag;

    @Column(name = "lage_einheit")
    @Json(name = "LageEinheit")
    private Integer lageEinheit;

    @Column(name = "lage_einheit_bezeichnung")
    @Json(name = "LageEinheitBezeichnung")
    private String lageEinheitBezeichnung;

    @Column(name = "leistungs_begrenzung")
    @Json(name = "Leistungsbegrenzung")
    private Integer leistungsBegrenzung;

    @Column(name = "nabenhoehe_windenergieanlage")
    @Json(name = "NabenhoeheWindenergieanlage")
    private Double nabenhoeheWindenergieanlage;

    @Column(name = "rotor_durchmesser_windenergieanlage")
    @Json(name = "RotordurchmesserWindenergieanlage")
    private Double rotorDurchmesserWindenergieanlage;

    @Column(name = "netto_nenn_leistung")
    @Json(name = "Nettonennleistung")
    private Double nettoNennLeistung;

    @Column(name = "nutzbare_speicher_kapazitaet")
    @Json(name = "NutzbareSpeicherkapazitaet")
    private Double nutzbareSpeicherKapazitaet;

    @Column(name = "nutzungs_bereich_gebsa")
    @Json(name = "NutzungsbereichGebSA")
    private Integer nutzungsBereichGebsa;

    @Column(name = "standort_anonymisiert")
    @Json(name = "StandortAnonymisiert")
    private String standortAnonymisiert;

    @Column(name = "spannungs_ebenen_id")
    @Json(name = "SpannungsebenenId")
    private String spannungsEbenenId;

    @Column(name = "spannungs_ebenen_namen")
    @Json(name = "SpannungsebenenNamen")
    private String spannungsEbenenNamen;

    @Column(name = "speicher_einheit_mastr_nummer")
    @Json(name = "SpeicherEinheitMastrNummer")
    private String speicherEinheitMastrNummer;

    @Column(name = "technologie_stromerzeugung_id")
    @Json(name = "TechnologieStromerzeugungId")
    private Integer technologieStromerzeugungId;

    @Column(name = "technologie_stromerzeugung")
    @Json(name = "TechnologieStromerzeugung")
    private String technologieStromerzeugung;

    @Column(name = "thermische_nutz_leistung")
    @Json(name = "ThermischeNutzleistung")
    private Double thermischeNutzLeistung;

    @Column(name = "typen_bezeichnung")
    @Json(name = "Typenbezeichnung")
    private String typenBezeichnung;

    @Column(name = "voll_teil_einspeisung")
    @Json(name = "VollTeilEinspeisung")
    private Integer vollTeilEinspeisung;

    @Column(name = "voll_teil_einspeisung_bezeichnung")
    @Json(name = "VollTeilEinspeisungBezeichnung")
    private String vollTeilEinspeisungBezeichnung;

    @Column(name = "windpark_name")
    @Json(name = "WindparkName")
    private String windparkName;

    @Column(name = "geometry")
    private Geometry geometry;

    /**
     * every mastr unit belongs to an administration unit
     */
    @Schema(description = "every mastr unit belongs to an administration unit")
    @ManyToOne
    @JsonIgnoreProperties(value = { "district", "mastrUnits", "chargingStations", "statistics" }, allowSetters = true)
    private AdministrationUnit administrationUnit;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public MastrUnit id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMastrNummer() {
        return this.mastrNummer;
    }

    public MastrUnit mastrNummer(String mastrNummer) {
        this.setMastrNummer(mastrNummer);
        return this;
    }

    public void setMastrNummer(String mastrNummer) {
        this.mastrNummer = mastrNummer;
    }

    public Long getAnlagenBetreiberId() {
        return this.anlagenBetreiberId;
    }

    public MastrUnit anlagenBetreiberId(Long anlagenBetreiberId) {
        this.setAnlagenBetreiberId(anlagenBetreiberId);
        return this;
    }

    public void setAnlagenBetreiberId(Long anlagenBetreiberId) {
        this.anlagenBetreiberId = anlagenBetreiberId;
    }

    public Long getAnlagenBetreiberPersonenArt() {
        return this.anlagenBetreiberPersonenArt;
    }

    public MastrUnit anlagenBetreiberPersonenArt(Long anlagenBetreiberPersonenArt) {
        this.setAnlagenBetreiberPersonenArt(anlagenBetreiberPersonenArt);
        return this;
    }

    public void setAnlagenBetreiberPersonenArt(Long anlagenBetreiberPersonenArt) {
        this.anlagenBetreiberPersonenArt = anlagenBetreiberPersonenArt;
    }

    public String getAnlagenBetreiberMaskedName() {
        return this.anlagenBetreiberMaskedName;
    }

    public MastrUnit anlagenBetreiberMaskedName(String anlagenBetreiberMaskedName) {
        this.setAnlagenBetreiberMaskedName(anlagenBetreiberMaskedName);
        return this;
    }

    public void setAnlagenBetreiberMaskedName(String anlagenBetreiberMaskedName) {
        this.anlagenBetreiberMaskedName = anlagenBetreiberMaskedName;
    }

    public String getAnlagenBetreiberMastrNummer() {
        return this.anlagenBetreiberMastrNummer;
    }

    public MastrUnit anlagenBetreiberMastrNummer(String anlagenBetreiberMastrNummer) {
        this.setAnlagenBetreiberMastrNummer(anlagenBetreiberMastrNummer);
        return this;
    }

    public void setAnlagenBetreiberMastrNummer(String anlagenBetreiberMastrNummer) {
        this.anlagenBetreiberMastrNummer = anlagenBetreiberMastrNummer;
    }

    public String getAnlagenBetreiberName() {
        return this.anlagenBetreiberName;
    }

    public MastrUnit anlagenBetreiberName(String anlagenBetreiberName) {
        this.setAnlagenBetreiberName(anlagenBetreiberName);
        return this;
    }

    public void setAnlagenBetreiberName(String anlagenBetreiberName) {
        this.anlagenBetreiberName = anlagenBetreiberName;
    }

    public Integer getBetriebsStatusId() {
        return this.betriebsStatusId;
    }

    public MastrUnit betriebsStatusId(Integer betriebsStatusId) {
        this.setBetriebsStatusId(betriebsStatusId);
        return this;
    }

    public void setBetriebsStatusId(Integer betriebsStatusId) {
        this.betriebsStatusId = betriebsStatusId;
    }

    public String getBetriebsStatusName() {
        return this.betriebsStatusName;
    }

    public MastrUnit betriebsStatusName(String betriebsStatusName) {
        this.setBetriebsStatusName(betriebsStatusName);
        return this;
    }

    public void setBetriebsStatusName(String betriebsStatusName) {
        this.betriebsStatusName = betriebsStatusName;
    }

    public String getBundesland() {
        return this.bundesland;
    }

    public MastrUnit bundesland(String bundesland) {
        this.setBundesland(bundesland);
        return this;
    }

    public void setBundesland(String bundesland) {
        this.bundesland = bundesland;
    }

    public Integer getBundeslandId() {
        return this.bundeslandId;
    }

    public MastrUnit bundeslandId(Integer bundeslandId) {
        this.setBundeslandId(bundeslandId);
        return this;
    }

    public void setBundeslandId(Integer bundeslandId) {
        this.bundeslandId = bundeslandId;
    }

    public Integer getLandkreisId() {
        return this.landkreisId;
    }

    public MastrUnit landkreisId(Integer landkreisId) {
        this.setLandkreisId(landkreisId);
        return this;
    }

    public void setLandkreisId(Integer landkreisId) {
        this.landkreisId = landkreisId;
    }

    public Integer getGemeindeId() {
        return this.gemeindeId;
    }

    public MastrUnit gemeindeId(Integer gemeindeId) {
        this.setGemeindeId(gemeindeId);
        return this;
    }

    public void setGemeindeId(Integer gemeindeId) {
        this.gemeindeId = gemeindeId;
    }

    public LocalDate getDatumLetzteAktualisierung() {
        return this.datumLetzteAktualisierung;
    }

    public MastrUnit datumLetzteAktualisierung(LocalDate datumLetzteAktualisierung) {
        this.setDatumLetzteAktualisierung(datumLetzteAktualisierung);
        return this;
    }

    public void setDatumLetzteAktualisierung(LocalDate datumLetzteAktualisierung) {
        this.datumLetzteAktualisierung = datumLetzteAktualisierung;
    }

    public LocalDate getEinheitRegistrierungsDatum() {
        return this.einheitRegistrierungsDatum;
    }

    public MastrUnit einheitRegistrierungsDatum(LocalDate einheitRegistrierungsDatum) {
        this.setEinheitRegistrierungsDatum(einheitRegistrierungsDatum);
        return this;
    }

    public void setEinheitRegistrierungsDatum(LocalDate einheitRegistrierungsDatum) {
        this.einheitRegistrierungsDatum = einheitRegistrierungsDatum;
    }

    public LocalDate getEndgueltigeStilllegungDatum() {
        return this.endgueltigeStilllegungDatum;
    }

    public MastrUnit endgueltigeStilllegungDatum(LocalDate endgueltigeStilllegungDatum) {
        this.setEndgueltigeStilllegungDatum(endgueltigeStilllegungDatum);
        return this;
    }

    public void setEndgueltigeStilllegungDatum(LocalDate endgueltigeStilllegungDatum) {
        this.endgueltigeStilllegungDatum = endgueltigeStilllegungDatum;
    }

    public LocalDate getGeplantesInbetriebsnahmeDatum() {
        return this.geplantesInbetriebsnahmeDatum;
    }

    public MastrUnit geplantesInbetriebsnahmeDatum(LocalDate geplantesInbetriebsnahmeDatum) {
        this.setGeplantesInbetriebsnahmeDatum(geplantesInbetriebsnahmeDatum);
        return this;
    }

    public void setGeplantesInbetriebsnahmeDatum(LocalDate geplantesInbetriebsnahmeDatum) {
        this.geplantesInbetriebsnahmeDatum = geplantesInbetriebsnahmeDatum;
    }

    public LocalDate getInbetriebnahmeDatum() {
        return this.inbetriebnahmeDatum;
    }

    public MastrUnit inbetriebnahmeDatum(LocalDate inbetriebnahmeDatum) {
        this.setInbetriebnahmeDatum(inbetriebnahmeDatum);
        return this;
    }

    public void setInbetriebnahmeDatum(LocalDate inbetriebnahmeDatum) {
        this.inbetriebnahmeDatum = inbetriebnahmeDatum;
    }

    public LocalDate getKwkAnlageInbetriebnahmeDatum() {
        return this.kwkAnlageInbetriebnahmeDatum;
    }

    public MastrUnit kwkAnlageInbetriebnahmeDatum(LocalDate kwkAnlageInbetriebnahmeDatum) {
        this.setKwkAnlageInbetriebnahmeDatum(kwkAnlageInbetriebnahmeDatum);
        return this;
    }

    public void setKwkAnlageInbetriebnahmeDatum(LocalDate kwkAnlageInbetriebnahmeDatum) {
        this.kwkAnlageInbetriebnahmeDatum = kwkAnlageInbetriebnahmeDatum;
    }

    public LocalDate getKwkAnlageRegistrierungsDatum() {
        return this.kwkAnlageRegistrierungsDatum;
    }

    public MastrUnit kwkAnlageRegistrierungsDatum(LocalDate kwkAnlageRegistrierungsDatum) {
        this.setKwkAnlageRegistrierungsDatum(kwkAnlageRegistrierungsDatum);
        return this;
    }

    public void setKwkAnlageRegistrierungsDatum(LocalDate kwkAnlageRegistrierungsDatum) {
        this.kwkAnlageRegistrierungsDatum = kwkAnlageRegistrierungsDatum;
    }

    public LocalDate getEegInbetriebnahmeDatum() {
        return this.eegInbetriebnahmeDatum;
    }

    public MastrUnit eegInbetriebnahmeDatum(LocalDate eegInbetriebnahmeDatum) {
        this.setEegInbetriebnahmeDatum(eegInbetriebnahmeDatum);
        return this;
    }

    public void setEegInbetriebnahmeDatum(LocalDate eegInbetriebnahmeDatum) {
        this.eegInbetriebnahmeDatum = eegInbetriebnahmeDatum;
    }

    public LocalDate getEegAnlageRegistrierungsDatum() {
        return this.eegAnlageRegistrierungsDatum;
    }

    public MastrUnit eegAnlageRegistrierungsDatum(LocalDate eegAnlageRegistrierungsDatum) {
        this.setEegAnlageRegistrierungsDatum(eegAnlageRegistrierungsDatum);
        return this;
    }

    public void setEegAnlageRegistrierungsDatum(LocalDate eegAnlageRegistrierungsDatum) {
        this.eegAnlageRegistrierungsDatum = eegAnlageRegistrierungsDatum;
    }

    public LocalDate getGenehmigungDatum() {
        return this.genehmigungDatum;
    }

    public MastrUnit genehmigungDatum(LocalDate genehmigungDatum) {
        this.setGenehmigungDatum(genehmigungDatum);
        return this;
    }

    public void setGenehmigungDatum(LocalDate genehmigungDatum) {
        this.genehmigungDatum = genehmigungDatum;
    }

    public LocalDate getGenehmigungRegistrierungsDatum() {
        return this.genehmigungRegistrierungsDatum;
    }

    public MastrUnit genehmigungRegistrierungsDatum(LocalDate genehmigungRegistrierungsDatum) {
        this.setGenehmigungRegistrierungsDatum(genehmigungRegistrierungsDatum);
        return this;
    }

    public void setGenehmigungRegistrierungsDatum(LocalDate genehmigungRegistrierungsDatum) {
        this.genehmigungRegistrierungsDatum = genehmigungRegistrierungsDatum;
    }

    public Integer getIsNbPruefungAbgeschlossen() {
        return this.isNbPruefungAbgeschlossen;
    }

    public MastrUnit isNbPruefungAbgeschlossen(Integer isNbPruefungAbgeschlossen) {
        this.setIsNbPruefungAbgeschlossen(isNbPruefungAbgeschlossen);
        return this;
    }

    public void setIsNbPruefungAbgeschlossen(Integer isNbPruefungAbgeschlossen) {
        this.isNbPruefungAbgeschlossen = isNbPruefungAbgeschlossen;
    }

    public Boolean getIsAnonymisiert() {
        return this.isAnonymisiert;
    }

    public MastrUnit isAnonymisiert(Boolean isAnonymisiert) {
        this.setIsAnonymisiert(isAnonymisiert);
        return this;
    }

    public void setIsAnonymisiert(Boolean isAnonymisiert) {
        this.isAnonymisiert = isAnonymisiert;
    }

    public Boolean getIsBuergerEnergie() {
        return this.isBuergerEnergie;
    }

    public MastrUnit isBuergerEnergie(Boolean isBuergerEnergie) {
        this.setIsBuergerEnergie(isBuergerEnergie);
        return this;
    }

    public void setIsBuergerEnergie(Boolean isBuergerEnergie) {
        this.isBuergerEnergie = isBuergerEnergie;
    }

    public Boolean getIsEinheitNotstromaggregat() {
        return this.isEinheitNotstromaggregat;
    }

    public MastrUnit isEinheitNotstromaggregat(Boolean isEinheitNotstromaggregat) {
        this.setIsEinheitNotstromaggregat(isEinheitNotstromaggregat);
        return this;
    }

    public void setIsEinheitNotstromaggregat(Boolean isEinheitNotstromaggregat) {
        this.isEinheitNotstromaggregat = isEinheitNotstromaggregat;
    }

    public Boolean getIsMieterstromAngemeldet() {
        return this.isMieterstromAngemeldet;
    }

    public MastrUnit isMieterstromAngemeldet(Boolean isMieterstromAngemeldet) {
        this.setIsMieterstromAngemeldet(isMieterstromAngemeldet);
        return this;
    }

    public void setIsMieterstromAngemeldet(Boolean isMieterstromAngemeldet) {
        this.isMieterstromAngemeldet = isMieterstromAngemeldet;
    }

    public Boolean getIsWasserkraftErtuechtigung() {
        return this.isWasserkraftErtuechtigung;
    }

    public MastrUnit isWasserkraftErtuechtigung(Boolean isWasserkraftErtuechtigung) {
        this.setIsWasserkraftErtuechtigung(isWasserkraftErtuechtigung);
        return this;
    }

    public void setIsWasserkraftErtuechtigung(Boolean isWasserkraftErtuechtigung) {
        this.isWasserkraftErtuechtigung = isWasserkraftErtuechtigung;
    }

    public Boolean getIsPilotWindanlage() {
        return this.isPilotWindanlage;
    }

    public MastrUnit isPilotWindanlage(Boolean isPilotWindanlage) {
        this.setIsPilotWindanlage(isPilotWindanlage);
        return this;
    }

    public void setIsPilotWindanlage(Boolean isPilotWindanlage) {
        this.isPilotWindanlage = isPilotWindanlage;
    }

    public Boolean getIsPrototypAnlage() {
        return this.isPrototypAnlage;
    }

    public MastrUnit isPrototypAnlage(Boolean isPrototypAnlage) {
        this.setIsPrototypAnlage(isPrototypAnlage);
        return this;
    }

    public void setIsPrototypAnlage(Boolean isPrototypAnlage) {
        this.isPrototypAnlage = isPrototypAnlage;
    }

    public Double getLat() {
        return this.lat;
    }

    public MastrUnit lat(Double lat) {
        this.setLat(lat);
        return this;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return this.lng;
    }

    public MastrUnit lng(Double lng) {
        this.setLng(lng);
        return this;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getOrt() {
        return this.ort;
    }

    public MastrUnit ort(String ort) {
        this.setOrt(ort);
        return this;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public Integer getPlz() {
        return this.plz;
    }

    public MastrUnit plz(Integer plz) {
        this.setPlz(plz);
        return this;
    }

    public void setPlz(Integer plz) {
        this.plz = plz;
    }

    public String getStrasse() {
        return this.strasse;
    }

    public MastrUnit strasse(String strasse) {
        this.setStrasse(strasse);
        return this;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getHausnummer() {
        return this.hausnummer;
    }

    public MastrUnit hausnummer(String hausnummer) {
        this.setHausnummer(hausnummer);
        return this;
    }

    public void setHausnummer(String hausnummer) {
        this.hausnummer = hausnummer;
    }

    public String getEinheitname() {
        return this.einheitname;
    }

    public MastrUnit einheitname(String einheitname) {
        this.setEinheitname(einheitname);
        return this;
    }

    public void setEinheitname(String einheitname) {
        this.einheitname = einheitname;
    }

    public String getFlurstueck() {
        return this.flurstueck;
    }

    public MastrUnit flurstueck(String flurstueck) {
        this.setFlurstueck(flurstueck);
        return this;
    }

    public void setFlurstueck(String flurstueck) {
        this.flurstueck = flurstueck;
    }

    public String getGemarkung() {
        return this.gemarkung;
    }

    public MastrUnit gemarkung(String gemarkung) {
        this.setGemarkung(gemarkung);
        return this;
    }

    public void setGemarkung(String gemarkung) {
        this.gemarkung = gemarkung;
    }

    public String getGemeinde() {
        return this.gemeinde;
    }

    public MastrUnit gemeinde(String gemeinde) {
        this.setGemeinde(gemeinde);
        return this;
    }

    public void setGemeinde(String gemeinde) {
        this.gemeinde = gemeinde;
    }

    public Integer getLandId() {
        return this.landId;
    }

    public MastrUnit landId(Integer landId) {
        this.setLandId(landId);
        return this;
    }

    public void setLandId(Integer landId) {
        this.landId = landId;
    }

    public String getLandkreis() {
        return this.landkreis;
    }

    public MastrUnit landkreis(String landkreis) {
        this.setLandkreis(landkreis);
        return this;
    }

    public void setLandkreis(String landkreis) {
        this.landkreis = landkreis;
    }

    public Long getAgs() {
        return this.ags;
    }

    public MastrUnit ags(Long ags) {
        this.setAgs(ags);
        return this;
    }

    public void setAgs(Long ags) {
        this.ags = ags;
    }

    public Long getLokationId() {
        return this.lokationId;
    }

    public MastrUnit lokationId(Long lokationId) {
        this.setLokationId(lokationId);
        return this;
    }

    public void setLokationId(Long lokationId) {
        this.lokationId = lokationId;
    }

    public String getLokationMastrNr() {
        return this.lokationMastrNr;
    }

    public MastrUnit lokationMastrNr(String lokationMastrNr) {
        this.setLokationMastrNr(lokationMastrNr);
        return this;
    }

    public void setLokationMastrNr(String lokationMastrNr) {
        this.lokationMastrNr = lokationMastrNr;
    }

    public String getNetzbetreiberId() {
        return this.netzbetreiberId;
    }

    public MastrUnit netzbetreiberId(String netzbetreiberId) {
        this.setNetzbetreiberId(netzbetreiberId);
        return this;
    }

    public void setNetzbetreiberId(String netzbetreiberId) {
        this.netzbetreiberId = netzbetreiberId;
    }

    public String getNetzbetreiberMaskedNamen() {
        return this.netzbetreiberMaskedNamen;
    }

    public MastrUnit netzbetreiberMaskedNamen(String netzbetreiberMaskedNamen) {
        this.setNetzbetreiberMaskedNamen(netzbetreiberMaskedNamen);
        return this;
    }

    public void setNetzbetreiberMaskedNamen(String netzbetreiberMaskedNamen) {
        this.netzbetreiberMaskedNamen = netzbetreiberMaskedNamen;
    }

    public String getNetzbetreiberMastrNummer() {
        return this.netzbetreiberMastrNummer;
    }

    public MastrUnit netzbetreiberMastrNummer(String netzbetreiberMastrNummer) {
        this.setNetzbetreiberMastrNummer(netzbetreiberMastrNummer);
        return this;
    }

    public void setNetzbetreiberMastrNummer(String netzbetreiberMastrNummer) {
        this.netzbetreiberMastrNummer = netzbetreiberMastrNummer;
    }

    public String getNetzbetreiberNamen() {
        return this.netzbetreiberNamen;
    }

    public MastrUnit netzbetreiberNamen(String netzbetreiberNamen) {
        this.setNetzbetreiberNamen(netzbetreiberNamen);
        return this;
    }

    public void setNetzbetreiberNamen(String netzbetreiberNamen) {
        this.netzbetreiberNamen = netzbetreiberNamen;
    }

    public String getNetzbetreiberPersonenArt() {
        return this.netzbetreiberPersonenArt;
    }

    public MastrUnit netzbetreiberPersonenArt(String netzbetreiberPersonenArt) {
        this.setNetzbetreiberPersonenArt(netzbetreiberPersonenArt);
        return this;
    }

    public void setNetzbetreiberPersonenArt(String netzbetreiberPersonenArt) {
        this.netzbetreiberPersonenArt = netzbetreiberPersonenArt;
    }

    public Integer getSystemStatusId() {
        return this.systemStatusId;
    }

    public MastrUnit systemStatusId(Integer systemStatusId) {
        this.setSystemStatusId(systemStatusId);
        return this;
    }

    public void setSystemStatusId(Integer systemStatusId) {
        this.systemStatusId = systemStatusId;
    }

    public String getSystemStatusName() {
        return this.systemStatusName;
    }

    public MastrUnit systemStatusName(String systemStatusName) {
        this.setSystemStatusName(systemStatusName);
        return this;
    }

    public void setSystemStatusName(String systemStatusName) {
        this.systemStatusName = systemStatusName;
    }

    public Integer getTyp() {
        return this.typ;
    }

    public MastrUnit typ(Integer typ) {
        this.setTyp(typ);
        return this;
    }

    public void setTyp(Integer typ) {
        this.typ = typ;
    }

    public String getAktenzeichenGenehmigung() {
        return this.aktenzeichenGenehmigung;
    }

    public MastrUnit aktenzeichenGenehmigung(String aktenzeichenGenehmigung) {
        this.setAktenzeichenGenehmigung(aktenzeichenGenehmigung);
        return this;
    }

    public void setAktenzeichenGenehmigung(String aktenzeichenGenehmigung) {
        this.aktenzeichenGenehmigung = aktenzeichenGenehmigung;
    }

    public Integer getAnzahlSolarmodule() {
        return this.anzahlSolarmodule;
    }

    public MastrUnit anzahlSolarmodule(Integer anzahlSolarmodule) {
        this.setAnzahlSolarmodule(anzahlSolarmodule);
        return this;
    }

    public void setAnzahlSolarmodule(Integer anzahlSolarmodule) {
        this.anzahlSolarmodule = anzahlSolarmodule;
    }

    public Integer getBatterieTechnologie() {
        return this.batterieTechnologie;
    }

    public MastrUnit batterieTechnologie(Integer batterieTechnologie) {
        this.setBatterieTechnologie(batterieTechnologie);
        return this;
    }

    public void setBatterieTechnologie(Integer batterieTechnologie) {
        this.batterieTechnologie = batterieTechnologie;
    }

    public Double getBruttoLeistung() {
        return this.bruttoLeistung;
    }

    public MastrUnit bruttoLeistung(Double bruttoLeistung) {
        this.setBruttoLeistung(bruttoLeistung);
        return this;
    }

    public void setBruttoLeistung(Double bruttoLeistung) {
        this.bruttoLeistung = bruttoLeistung;
    }

    public Double getEegInstallierteLeistung() {
        return this.eegInstallierteLeistung;
    }

    public MastrUnit eegInstallierteLeistung(Double eegInstallierteLeistung) {
        this.setEegInstallierteLeistung(eegInstallierteLeistung);
        return this;
    }

    public void setEegInstallierteLeistung(Double eegInstallierteLeistung) {
        this.eegInstallierteLeistung = eegInstallierteLeistung;
    }

    public String getEegAnlageMastrNummer() {
        return this.eegAnlageMastrNummer;
    }

    public MastrUnit eegAnlageMastrNummer(String eegAnlageMastrNummer) {
        this.setEegAnlageMastrNummer(eegAnlageMastrNummer);
        return this;
    }

    public void setEegAnlageMastrNummer(String eegAnlageMastrNummer) {
        this.eegAnlageMastrNummer = eegAnlageMastrNummer;
    }

    public String getEegAnlagenSchluessel() {
        return this.eegAnlagenSchluessel;
    }

    public MastrUnit eegAnlagenSchluessel(String eegAnlagenSchluessel) {
        this.setEegAnlagenSchluessel(eegAnlagenSchluessel);
        return this;
    }

    public void setEegAnlagenSchluessel(String eegAnlagenSchluessel) {
        this.eegAnlagenSchluessel = eegAnlagenSchluessel;
    }

    public String getEegZuschlag() {
        return this.eegZuschlag;
    }

    public MastrUnit eegZuschlag(String eegZuschlag) {
        this.setEegZuschlag(eegZuschlag);
        return this;
    }

    public void setEegZuschlag(String eegZuschlag) {
        this.eegZuschlag = eegZuschlag;
    }

    public String getZuschlagsNummern() {
        return this.zuschlagsNummern;
    }

    public MastrUnit zuschlagsNummern(String zuschlagsNummern) {
        this.setZuschlagsNummern(zuschlagsNummern);
        return this;
    }

    public void setZuschlagsNummern(String zuschlagsNummern) {
        this.zuschlagsNummern = zuschlagsNummern;
    }

    public Integer getEnergieTraegerId() {
        return this.energieTraegerId;
    }

    public MastrUnit energieTraegerId(Integer energieTraegerId) {
        this.setEnergieTraegerId(energieTraegerId);
        return this;
    }

    public void setEnergieTraegerId(Integer energieTraegerId) {
        this.energieTraegerId = energieTraegerId;
    }

    public String getEnergieTraegerName() {
        return this.energieTraegerName;
    }

    public MastrUnit energieTraegerName(String energieTraegerName) {
        this.setEnergieTraegerName(energieTraegerName);
        return this;
    }

    public void setEnergieTraegerName(String energieTraegerName) {
        this.energieTraegerName = energieTraegerName;
    }

    public Integer getGemeinsamerWechselrichter() {
        return this.gemeinsamerWechselrichter;
    }

    public MastrUnit gemeinsamerWechselrichter(Integer gemeinsamerWechselrichter) {
        this.setGemeinsamerWechselrichter(gemeinsamerWechselrichter);
        return this;
    }

    public void setGemeinsamerWechselrichter(Integer gemeinsamerWechselrichter) {
        this.gemeinsamerWechselrichter = gemeinsamerWechselrichter;
    }

    public String getGenehmigungBehoerde() {
        return this.genehmigungBehoerde;
    }

    public MastrUnit genehmigungBehoerde(String genehmigungBehoerde) {
        this.setGenehmigungBehoerde(genehmigungBehoerde);
        return this;
    }

    public void setGenehmigungBehoerde(String genehmigungBehoerde) {
        this.genehmigungBehoerde = genehmigungBehoerde;
    }

    public String getGenehmigungsMastrNummer() {
        return this.genehmigungsMastrNummer;
    }

    public MastrUnit genehmigungsMastrNummer(String genehmigungsMastrNummer) {
        this.setGenehmigungsMastrNummer(genehmigungsMastrNummer);
        return this;
    }

    public void setGenehmigungsMastrNummer(String genehmigungsMastrNummer) {
        this.genehmigungsMastrNummer = genehmigungsMastrNummer;
    }

    public String getGruppierungsObjekte() {
        return this.gruppierungsObjekte;
    }

    public MastrUnit gruppierungsObjekte(String gruppierungsObjekte) {
        this.setGruppierungsObjekte(gruppierungsObjekte);
        return this;
    }

    public void setGruppierungsObjekte(String gruppierungsObjekte) {
        this.gruppierungsObjekte = gruppierungsObjekte;
    }

    public String getGruppierungsObjekteIds() {
        return this.gruppierungsObjekteIds;
    }

    public MastrUnit gruppierungsObjekteIds(String gruppierungsObjekteIds) {
        this.setGruppierungsObjekteIds(gruppierungsObjekteIds);
        return this;
    }

    public void setGruppierungsObjekteIds(String gruppierungsObjekteIds) {
        this.gruppierungsObjekteIds = gruppierungsObjekteIds;
    }

    public Boolean getHatFlexibilitaetsPraemie() {
        return this.hatFlexibilitaetsPraemie;
    }

    public MastrUnit hatFlexibilitaetsPraemie(Boolean hatFlexibilitaetsPraemie) {
        this.setHatFlexibilitaetsPraemie(hatFlexibilitaetsPraemie);
        return this;
    }

    public void setHatFlexibilitaetsPraemie(Boolean hatFlexibilitaetsPraemie) {
        this.hatFlexibilitaetsPraemie = hatFlexibilitaetsPraemie;
    }

    public Integer getHauptAusrichtungSolarmodule() {
        return this.hauptAusrichtungSolarmodule;
    }

    public MastrUnit hauptAusrichtungSolarmodule(Integer hauptAusrichtungSolarmodule) {
        this.setHauptAusrichtungSolarmodule(hauptAusrichtungSolarmodule);
        return this;
    }

    public void setHauptAusrichtungSolarmodule(Integer hauptAusrichtungSolarmodule) {
        this.hauptAusrichtungSolarmodule = hauptAusrichtungSolarmodule;
    }

    public String getHauptAusrichtungSolarmoduleBezeichnung() {
        return this.hauptAusrichtungSolarmoduleBezeichnung;
    }

    public MastrUnit hauptAusrichtungSolarmoduleBezeichnung(String hauptAusrichtungSolarmoduleBezeichnung) {
        this.setHauptAusrichtungSolarmoduleBezeichnung(hauptAusrichtungSolarmoduleBezeichnung);
        return this;
    }

    public void setHauptAusrichtungSolarmoduleBezeichnung(String hauptAusrichtungSolarmoduleBezeichnung) {
        this.hauptAusrichtungSolarmoduleBezeichnung = hauptAusrichtungSolarmoduleBezeichnung;
    }

    public Integer getHauptBrennstoffId() {
        return this.hauptBrennstoffId;
    }

    public MastrUnit hauptBrennstoffId(Integer hauptBrennstoffId) {
        this.setHauptBrennstoffId(hauptBrennstoffId);
        return this;
    }

    public void setHauptBrennstoffId(Integer hauptBrennstoffId) {
        this.hauptBrennstoffId = hauptBrennstoffId;
    }

    public String getHauptBrennstoffNamen() {
        return this.hauptBrennstoffNamen;
    }

    public MastrUnit hauptBrennstoffNamen(String hauptBrennstoffNamen) {
        this.setHauptBrennstoffNamen(hauptBrennstoffNamen);
        return this;
    }

    public void setHauptBrennstoffNamen(String hauptBrennstoffNamen) {
        this.hauptBrennstoffNamen = hauptBrennstoffNamen;
    }

    public Integer getHauptNeigungswinkelSolarModule() {
        return this.hauptNeigungswinkelSolarModule;
    }

    public MastrUnit hauptNeigungswinkelSolarModule(Integer hauptNeigungswinkelSolarModule) {
        this.setHauptNeigungswinkelSolarModule(hauptNeigungswinkelSolarModule);
        return this;
    }

    public void setHauptNeigungswinkelSolarModule(Integer hauptNeigungswinkelSolarModule) {
        this.hauptNeigungswinkelSolarModule = hauptNeigungswinkelSolarModule;
    }

    public Integer getHerstellerWindenergieanlageId() {
        return this.herstellerWindenergieanlageId;
    }

    public MastrUnit herstellerWindenergieanlageId(Integer herstellerWindenergieanlageId) {
        this.setHerstellerWindenergieanlageId(herstellerWindenergieanlageId);
        return this;
    }

    public void setHerstellerWindenergieanlageId(Integer herstellerWindenergieanlageId) {
        this.herstellerWindenergieanlageId = herstellerWindenergieanlageId;
    }

    public String getHerstellerWindenergieanlageBezeichnung() {
        return this.herstellerWindenergieanlageBezeichnung;
    }

    public MastrUnit herstellerWindenergieanlageBezeichnung(String herstellerWindenergieanlageBezeichnung) {
        this.setHerstellerWindenergieanlageBezeichnung(herstellerWindenergieanlageBezeichnung);
        return this;
    }

    public void setHerstellerWindenergieanlageBezeichnung(String herstellerWindenergieanlageBezeichnung) {
        this.herstellerWindenergieanlageBezeichnung = herstellerWindenergieanlageBezeichnung;
    }

    public Double getKwkAnlageElektrischeLeistung() {
        return this.kwkAnlageElektrischeLeistung;
    }

    public MastrUnit kwkAnlageElektrischeLeistung(Double kwkAnlageElektrischeLeistung) {
        this.setKwkAnlageElektrischeLeistung(kwkAnlageElektrischeLeistung);
        return this;
    }

    public void setKwkAnlageElektrischeLeistung(Double kwkAnlageElektrischeLeistung) {
        this.kwkAnlageElektrischeLeistung = kwkAnlageElektrischeLeistung;
    }

    public String getKwkAnlageMastrNummer() {
        return this.kwkAnlageMastrNummer;
    }

    public MastrUnit kwkAnlageMastrNummer(String kwkAnlageMastrNummer) {
        this.setKwkAnlageMastrNummer(kwkAnlageMastrNummer);
        return this;
    }

    public void setKwkAnlageMastrNummer(String kwkAnlageMastrNummer) {
        this.kwkAnlageMastrNummer = kwkAnlageMastrNummer;
    }

    public String getKwkZuschlag() {
        return this.kwkZuschlag;
    }

    public MastrUnit kwkZuschlag(String kwkZuschlag) {
        this.setKwkZuschlag(kwkZuschlag);
        return this;
    }

    public void setKwkZuschlag(String kwkZuschlag) {
        this.kwkZuschlag = kwkZuschlag;
    }

    public Integer getLageEinheit() {
        return this.lageEinheit;
    }

    public MastrUnit lageEinheit(Integer lageEinheit) {
        this.setLageEinheit(lageEinheit);
        return this;
    }

    public void setLageEinheit(Integer lageEinheit) {
        this.lageEinheit = lageEinheit;
    }

    public String getLageEinheitBezeichnung() {
        return this.lageEinheitBezeichnung;
    }

    public MastrUnit lageEinheitBezeichnung(String lageEinheitBezeichnung) {
        this.setLageEinheitBezeichnung(lageEinheitBezeichnung);
        return this;
    }

    public void setLageEinheitBezeichnung(String lageEinheitBezeichnung) {
        this.lageEinheitBezeichnung = lageEinheitBezeichnung;
    }

    public Integer getLeistungsBegrenzung() {
        return this.leistungsBegrenzung;
    }

    public MastrUnit leistungsBegrenzung(Integer leistungsBegrenzung) {
        this.setLeistungsBegrenzung(leistungsBegrenzung);
        return this;
    }

    public void setLeistungsBegrenzung(Integer leistungsBegrenzung) {
        this.leistungsBegrenzung = leistungsBegrenzung;
    }

    public Double getNabenhoeheWindenergieanlage() {
        return this.nabenhoeheWindenergieanlage;
    }

    public MastrUnit nabenhoeheWindenergieanlage(Double nabenhoeheWindenergieanlage) {
        this.setNabenhoeheWindenergieanlage(nabenhoeheWindenergieanlage);
        return this;
    }

    public void setNabenhoeheWindenergieanlage(Double nabenhoeheWindenergieanlage) {
        this.nabenhoeheWindenergieanlage = nabenhoeheWindenergieanlage;
    }

    public Double getRotorDurchmesserWindenergieanlage() {
        return this.rotorDurchmesserWindenergieanlage;
    }

    public MastrUnit rotorDurchmesserWindenergieanlage(Double rotorDurchmesserWindenergieanlage) {
        this.setRotorDurchmesserWindenergieanlage(rotorDurchmesserWindenergieanlage);
        return this;
    }

    public void setRotorDurchmesserWindenergieanlage(Double rotorDurchmesserWindenergieanlage) {
        this.rotorDurchmesserWindenergieanlage = rotorDurchmesserWindenergieanlage;
    }

    public Double getNettoNennLeistung() {
        return this.nettoNennLeistung;
    }

    public MastrUnit nettoNennLeistung(Double nettoNennLeistung) {
        this.setNettoNennLeistung(nettoNennLeistung);
        return this;
    }

    public void setNettoNennLeistung(Double nettoNennLeistung) {
        this.nettoNennLeistung = nettoNennLeistung;
    }

    public Double getNutzbareSpeicherKapazitaet() {
        return this.nutzbareSpeicherKapazitaet;
    }

    public MastrUnit nutzbareSpeicherKapazitaet(Double nutzbareSpeicherKapazitaet) {
        this.setNutzbareSpeicherKapazitaet(nutzbareSpeicherKapazitaet);
        return this;
    }

    public void setNutzbareSpeicherKapazitaet(Double nutzbareSpeicherKapazitaet) {
        this.nutzbareSpeicherKapazitaet = nutzbareSpeicherKapazitaet;
    }

    public Integer getNutzungsBereichGebsa() {
        return this.nutzungsBereichGebsa;
    }

    public MastrUnit nutzungsBereichGebsa(Integer nutzungsBereichGebsa) {
        this.setNutzungsBereichGebsa(nutzungsBereichGebsa);
        return this;
    }

    public void setNutzungsBereichGebsa(Integer nutzungsBereichGebsa) {
        this.nutzungsBereichGebsa = nutzungsBereichGebsa;
    }

    public String getStandortAnonymisiert() {
        return this.standortAnonymisiert;
    }

    public MastrUnit standortAnonymisiert(String standortAnonymisiert) {
        this.setStandortAnonymisiert(standortAnonymisiert);
        return this;
    }

    public void setStandortAnonymisiert(String standortAnonymisiert) {
        this.standortAnonymisiert = standortAnonymisiert;
    }

    public String getSpannungsEbenenId() {
        return this.spannungsEbenenId;
    }

    public MastrUnit spannungsEbenenId(String spannungsEbenenId) {
        this.setSpannungsEbenenId(spannungsEbenenId);
        return this;
    }

    public void setSpannungsEbenenId(String spannungsEbenenId) {
        this.spannungsEbenenId = spannungsEbenenId;
    }

    public String getSpannungsEbenenNamen() {
        return this.spannungsEbenenNamen;
    }

    public MastrUnit spannungsEbenenNamen(String spannungsEbenenNamen) {
        this.setSpannungsEbenenNamen(spannungsEbenenNamen);
        return this;
    }

    public void setSpannungsEbenenNamen(String spannungsEbenenNamen) {
        this.spannungsEbenenNamen = spannungsEbenenNamen;
    }

    public String getSpeicherEinheitMastrNummer() {
        return this.speicherEinheitMastrNummer;
    }

    public MastrUnit speicherEinheitMastrNummer(String speicherEinheitMastrNummer) {
        this.setSpeicherEinheitMastrNummer(speicherEinheitMastrNummer);
        return this;
    }

    public void setSpeicherEinheitMastrNummer(String speicherEinheitMastrNummer) {
        this.speicherEinheitMastrNummer = speicherEinheitMastrNummer;
    }

    public Integer getTechnologieStromerzeugungId() {
        return this.technologieStromerzeugungId;
    }

    public MastrUnit technologieStromerzeugungId(Integer technologieStromerzeugungId) {
        this.setTechnologieStromerzeugungId(technologieStromerzeugungId);
        return this;
    }

    public void setTechnologieStromerzeugungId(Integer technologieStromerzeugungId) {
        this.technologieStromerzeugungId = technologieStromerzeugungId;
    }

    public String getTechnologieStromerzeugung() {
        return this.technologieStromerzeugung;
    }

    public MastrUnit technologieStromerzeugung(String technologieStromerzeugung) {
        this.setTechnologieStromerzeugung(technologieStromerzeugung);
        return this;
    }

    public void setTechnologieStromerzeugung(String technologieStromerzeugung) {
        this.technologieStromerzeugung = technologieStromerzeugung;
    }

    public Double getThermischeNutzLeistung() {
        return this.thermischeNutzLeistung;
    }

    public MastrUnit thermischeNutzLeistung(Double thermischeNutzLeistung) {
        this.setThermischeNutzLeistung(thermischeNutzLeistung);
        return this;
    }

    public void setThermischeNutzLeistung(Double thermischeNutzLeistung) {
        this.thermischeNutzLeistung = thermischeNutzLeistung;
    }

    public String getTypenBezeichnung() {
        return this.typenBezeichnung;
    }

    public MastrUnit typenBezeichnung(String typenBezeichnung) {
        this.setTypenBezeichnung(typenBezeichnung);
        return this;
    }

    public void setTypenBezeichnung(String typenBezeichnung) {
        this.typenBezeichnung = typenBezeichnung;
    }

    public Integer getVollTeilEinspeisung() {
        return this.vollTeilEinspeisung;
    }

    public MastrUnit vollTeilEinspeisung(Integer vollTeilEinspeisung) {
        this.setVollTeilEinspeisung(vollTeilEinspeisung);
        return this;
    }

    public void setVollTeilEinspeisung(Integer vollTeilEinspeisung) {
        this.vollTeilEinspeisung = vollTeilEinspeisung;
    }

    public String getVollTeilEinspeisungBezeichnung() {
        return this.vollTeilEinspeisungBezeichnung;
    }

    public MastrUnit vollTeilEinspeisungBezeichnung(String vollTeilEinspeisungBezeichnung) {
        this.setVollTeilEinspeisungBezeichnung(vollTeilEinspeisungBezeichnung);
        return this;
    }

    public void setVollTeilEinspeisungBezeichnung(String vollTeilEinspeisungBezeichnung) {
        this.vollTeilEinspeisungBezeichnung = vollTeilEinspeisungBezeichnung;
    }

    public String getWindparkName() {
        return this.windparkName;
    }

    public MastrUnit windparkName(String windparkName) {
        this.setWindparkName(windparkName);
        return this;
    }

    public void setWindparkName(String windparkName) {
        this.windparkName = windparkName;
    }

    public Geometry getGeometry() {
        return this.geometry;
    }

    public MastrUnit geometry(Geometry geometry) {
        this.setGeometry(geometry);
        return this;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public AdministrationUnit getAdministrationUnit() {
        return this.administrationUnit;
    }

    public void setAdministrationUnit(AdministrationUnit administrationUnit) {
        this.administrationUnit = administrationUnit;
    }

    public MastrUnit administrationUnit(AdministrationUnit administrationUnit) {
        this.setAdministrationUnit(administrationUnit);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MastrUnit)) {
            return false;
        }
        return id != null && id.equals(((MastrUnit) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MastrUnit{" +
            "id=" + getId() +
            ", mastrNummer='" + getMastrNummer() + "'" +
            ", anlagenBetreiberId=" + getAnlagenBetreiberId() +
            ", anlagenBetreiberPersonenArt=" + getAnlagenBetreiberPersonenArt() +
            ", anlagenBetreiberMaskedName='" + getAnlagenBetreiberMaskedName() + "'" +
            ", anlagenBetreiberMastrNummer='" + getAnlagenBetreiberMastrNummer() + "'" +
            ", anlagenBetreiberName='" + getAnlagenBetreiberName() + "'" +
            ", betriebsStatusId=" + getBetriebsStatusId() +
            ", betriebsStatusName='" + getBetriebsStatusName() + "'" +
            ", bundesland='" + getBundesland() + "'" +
            ", bundeslandId=" + getBundeslandId() +
            ", landkreisId=" + getLandkreisId() +
            ", gemeindeId=" + getGemeindeId() +
            ", datumLetzteAktualisierung='" + getDatumLetzteAktualisierung() + "'" +
            ", einheitRegistrierungsDatum='" + getEinheitRegistrierungsDatum() + "'" +
            ", endgueltigeStilllegungDatum='" + getEndgueltigeStilllegungDatum() + "'" +
            ", geplantesInbetriebsnahmeDatum='" + getGeplantesInbetriebsnahmeDatum() + "'" +
            ", inbetriebnahmeDatum='" + getInbetriebnahmeDatum() + "'" +
            ", kwkAnlageInbetriebnahmeDatum='" + getKwkAnlageInbetriebnahmeDatum() + "'" +
            ", kwkAnlageRegistrierungsDatum='" + getKwkAnlageRegistrierungsDatum() + "'" +
            ", eegInbetriebnahmeDatum='" + getEegInbetriebnahmeDatum() + "'" +
            ", eegAnlageRegistrierungsDatum='" + getEegAnlageRegistrierungsDatum() + "'" +
            ", genehmigungDatum='" + getGenehmigungDatum() + "'" +
            ", genehmigungRegistrierungsDatum='" + getGenehmigungRegistrierungsDatum() + "'" +
            ", isNbPruefungAbgeschlossen=" + getIsNbPruefungAbgeschlossen() +
            ", isAnonymisiert='" + getIsAnonymisiert() + "'" +
            ", isBuergerEnergie='" + getIsBuergerEnergie() + "'" +
            ", isEinheitNotstromaggregat='" + getIsEinheitNotstromaggregat() + "'" +
            ", isMieterstromAngemeldet='" + getIsMieterstromAngemeldet() + "'" +
            ", isWasserkraftErtuechtigung='" + getIsWasserkraftErtuechtigung() + "'" +
            ", isPilotWindanlage='" + getIsPilotWindanlage() + "'" +
            ", isPrototypAnlage='" + getIsPrototypAnlage() + "'" +
            ", lat=" + getLat() +
            ", lng=" + getLng() +
            ", ort='" + getOrt() + "'" +
            ", plz=" + getPlz() +
            ", strasse='" + getStrasse() + "'" +
            ", hausnummer='" + getHausnummer() + "'" +
            ", einheitname='" + getEinheitname() + "'" +
            ", flurstueck='" + getFlurstueck() + "'" +
            ", gemarkung='" + getGemarkung() + "'" +
            ", gemeinde='" + getGemeinde() + "'" +
            ", landId=" + getLandId() +
            ", landkreis='" + getLandkreis() + "'" +
            ", ags=" + getAgs() +
            ", lokationId=" + getLokationId() +
            ", lokationMastrNr='" + getLokationMastrNr() + "'" +
            ", netzbetreiberId='" + getNetzbetreiberId() + "'" +
            ", netzbetreiberMaskedNamen='" + getNetzbetreiberMaskedNamen() + "'" +
            ", netzbetreiberMastrNummer='" + getNetzbetreiberMastrNummer() + "'" +
            ", netzbetreiberNamen='" + getNetzbetreiberNamen() + "'" +
            ", netzbetreiberPersonenArt='" + getNetzbetreiberPersonenArt() + "'" +
            ", systemStatusId=" + getSystemStatusId() +
            ", systemStatusName='" + getSystemStatusName() + "'" +
            ", typ=" + getTyp() +
            ", aktenzeichenGenehmigung='" + getAktenzeichenGenehmigung() + "'" +
            ", anzahlSolarmodule=" + getAnzahlSolarmodule() +
            ", batterieTechnologie=" + getBatterieTechnologie() +
            ", bruttoLeistung=" + getBruttoLeistung() +
            ", eegInstallierteLeistung=" + getEegInstallierteLeistung() +
            ", eegAnlageMastrNummer='" + getEegAnlageMastrNummer() + "'" +
            ", eegAnlagenSchluessel='" + getEegAnlagenSchluessel() + "'" +
            ", eegZuschlag='" + getEegZuschlag() + "'" +
            ", zuschlagsNummern='" + getZuschlagsNummern() + "'" +
            ", energieTraegerId=" + getEnergieTraegerId() +
            ", energieTraegerName='" + getEnergieTraegerName() + "'" +
            ", gemeinsamerWechselrichter=" + getGemeinsamerWechselrichter() +
            ", genehmigungBehoerde='" + getGenehmigungBehoerde() + "'" +
            ", genehmigungsMastrNummer='" + getGenehmigungsMastrNummer() + "'" +
            ", gruppierungsObjekte='" + getGruppierungsObjekte() + "'" +
            ", gruppierungsObjekteIds='" + getGruppierungsObjekteIds() + "'" +
            ", hatFlexibilitaetsPraemie='" + getHatFlexibilitaetsPraemie() + "'" +
            ", hauptAusrichtungSolarmodule=" + getHauptAusrichtungSolarmodule() +
            ", hauptAusrichtungSolarmoduleBezeichnung='" + getHauptAusrichtungSolarmoduleBezeichnung() + "'" +
            ", hauptBrennstoffId=" + getHauptBrennstoffId() +
            ", hauptBrennstoffNamen='" + getHauptBrennstoffNamen() + "'" +
            ", hauptNeigungswinkelSolarModule=" + getHauptNeigungswinkelSolarModule() +
            ", herstellerWindenergieanlageId=" + getHerstellerWindenergieanlageId() +
            ", herstellerWindenergieanlageBezeichnung='" + getHerstellerWindenergieanlageBezeichnung() + "'" +
            ", kwkAnlageElektrischeLeistung=" + getKwkAnlageElektrischeLeistung() +
            ", kwkAnlageMastrNummer='" + getKwkAnlageMastrNummer() + "'" +
            ", kwkZuschlag='" + getKwkZuschlag() + "'" +
            ", lageEinheit=" + getLageEinheit() +
            ", lageEinheitBezeichnung='" + getLageEinheitBezeichnung() + "'" +
            ", leistungsBegrenzung=" + getLeistungsBegrenzung() +
            ", nabenhoeheWindenergieanlage=" + getNabenhoeheWindenergieanlage() +
            ", rotorDurchmesserWindenergieanlage=" + getRotorDurchmesserWindenergieanlage() +
            ", nettoNennLeistung=" + getNettoNennLeistung() +
            ", nutzbareSpeicherKapazitaet=" + getNutzbareSpeicherKapazitaet() +
            ", nutzungsBereichGebsa=" + getNutzungsBereichGebsa() +
            ", standortAnonymisiert='" + getStandortAnonymisiert() + "'" +
            ", spannungsEbenenId='" + getSpannungsEbenenId() + "'" +
            ", spannungsEbenenNamen='" + getSpannungsEbenenNamen() + "'" +
            ", speicherEinheitMastrNummer='" + getSpeicherEinheitMastrNummer() + "'" +
            ", technologieStromerzeugungId=" + getTechnologieStromerzeugungId() +
            ", technologieStromerzeugung='" + getTechnologieStromerzeugung() + "'" +
            ", thermischeNutzLeistung=" + getThermischeNutzLeistung() +
            ", typenBezeichnung='" + getTypenBezeichnung() + "'" +
            ", vollTeilEinspeisung=" + getVollTeilEinspeisung() +
            ", vollTeilEinspeisungBezeichnung='" + getVollTeilEinspeisungBezeichnung() + "'" +
            ", windparkName='" + getWindparkName() + "'" +
            ", geometry='" + getGeometry() + "'" +
            "}";
    }
}
