package eu.danielgerber.domain;

public class MastrUnitForIntervalStatisticElement {

    private Integer count;
    private String date;
    private Double aggregate;
    
    public MastrUnitForIntervalStatisticElement(Integer count, String date, Double aggregate) {
      this.count = count;
      this.date = date;
      this.aggregate = aggregate;
    }
    public Integer getCount() {
        return count;
    }
    public void setCount(Integer count) {
        this.count = count;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public Double getAggregate() {
        return aggregate;
    }
    public void setAggregate(Double aggregate) {
        this.aggregate = aggregate;
    }
}
