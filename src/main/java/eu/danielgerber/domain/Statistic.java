package eu.danielgerber.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * it should be possible to save arbitrary statistics for an administrative unit
 */
@Schema(description = "it should be possible to save arbitrary statistics for an administrative unit")
@Entity
@Table(name = "statistic")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Statistic implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    /**
     * the name of the statistic
     */
    @Schema(description = "the name of the statistic")
    @Column(name = "name")
    private String name;

    /**
     * the key of the statistic
     */
    @Schema(description = "the key of the statistic")
    @Column(name = "key")
    private String key;

    /**
     * the value of the statistic
     */
    @Schema(description = "the value of the statistic")
    @Column(name = "value")
    private Double value;

    /**
     * the unit of the statistic
     */
    @Schema(description = "the unit of the statistic")
    @Column(name = "unit")
    private String unit;

    /**
     * the date of the statistic
     */
    @Schema(description = "the date of the statistic")
    @Column(name = "date")
    private LocalDate date;

    /**
     * the name of the statistic source, helps to find which tool this generated
     */
    @Schema(description = "the name of the statistic source, helps to find which tool this generated")
    @Column(name = "source")
    private String source;

    @ManyToOne
    @JsonIgnoreProperties(value = { "district", "mastrUnits", "chargingStations", "statistics" }, allowSetters = true)
    private AdministrationUnit administrationUnit;

    @ManyToOne
    @JsonIgnoreProperties(value = { "administrationUnit", "statistics" }, allowSetters = true)
    private ChargingStation chargingStation;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Statistic id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Statistic name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return this.key;
    }

    public Statistic key(String key) {
        this.setKey(key);
        return this;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Double getValue() {
        return this.value;
    }

    public Statistic value(Double value) {
        this.setValue(value);
        return this;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getUnit() {
        return this.unit;
    }

    public Statistic unit(String unit) {
        this.setUnit(unit);
        return this;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public Statistic date(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getSource() {
        return this.source;
    }

    public Statistic source(String source) {
        this.setSource(source);
        return this;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public AdministrationUnit getAdministrationUnit() {
        return this.administrationUnit;
    }

    public void setAdministrationUnit(AdministrationUnit administrationUnit) {
        this.administrationUnit = administrationUnit;
    }

    public Statistic administrationUnit(AdministrationUnit administrationUnit) {
        this.setAdministrationUnit(administrationUnit);
        return this;
    }

    public ChargingStation getChargingStation() {
        return this.chargingStation;
    }

    public void setChargingStation(ChargingStation chargingStation) {
        this.chargingStation = chargingStation;
    }

    public Statistic chargingStation(ChargingStation chargingStation) {
        this.setChargingStation(chargingStation);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Statistic)) {
            return false;
        }
        return id != null && id.equals(((Statistic) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Statistic{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", key='" + getKey() + "'" +
            ", value=" + getValue() +
            ", unit='" + getUnit() + "'" +
            ", date='" + getDate() + "'" +
            ", source='" + getSource() + "'" +
            "}";
    }
}
