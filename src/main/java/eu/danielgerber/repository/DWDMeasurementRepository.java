package eu.danielgerber.repository;

import eu.danielgerber.domain.DWDMeasurement;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the DWDMeasurement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DWDMeasurementRepository extends JpaRepository<DWDMeasurement, Long>, JpaSpecificationExecutor<DWDMeasurement> {}
