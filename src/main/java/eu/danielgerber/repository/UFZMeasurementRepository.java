package eu.danielgerber.repository;

import eu.danielgerber.domain.AdministrationUnit;
import eu.danielgerber.domain.UFZMeasurement;

import java.time.LocalDate;
import java.util.List;

import org.locationtech.jts.geom.Geometry;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the UFZMeasurement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UFZMeasurementRepository extends JpaRepository<UFZMeasurement, Long>, JpaSpecificationExecutor<UFZMeasurement> {

  @Query("SELECT DISTINCT m.date FROM UFZMeasurement m ORDER BY date desc")
  List<LocalDate> findDistinctDates();
}
