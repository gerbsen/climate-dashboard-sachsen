package eu.danielgerber.repository;

import eu.danielgerber.domain.UFZRasterPoint;

import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the UFZRasterPoint entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UFZRasterPointRepository extends JpaRepository<UFZRasterPoint, Long>, JpaSpecificationExecutor<UFZRasterPoint> {
  
  @Modifying
  @Transactional
  @Query(nativeQuery = true, value = "DELETE FROM ufz_raster_point p WHERE st_within(ST_Centroid(p.geometry), (SELECT st_union(au.geometry) FROM administration_unit au where au.land = :stateId)) = FALSE")
  void deleteAllNotInState(@Param("stateId") String stateId);

  @Query("FROM UFZRasterPoint p JOIN FETCH p.ufzMeasurements WHERE p.id = (:id)")
  public Optional<UFZRasterPoint> findByIdAndFetchMeasurementsEagerly(@Param("id") Long id);

  @Query("FROM UFZRasterPoint p JOIN FETCH p.ufzMeasurements")
  public Set<UFZRasterPoint> findAllAndFetchMeasurementsEagerly();
}
