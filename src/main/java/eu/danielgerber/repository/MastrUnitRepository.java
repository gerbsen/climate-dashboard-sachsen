package eu.danielgerber.repository;

import eu.danielgerber.domain.MastrUnit;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;


/**
 * Spring Data JPA repository for the MastrUnit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MastrUnitRepository extends JpaRepository<MastrUnit, Long>, JpaSpecificationExecutor<MastrUnit> {

  List<MastrUnit> findByBetriebsStatusNameAndAdministrationUnit_Ade(String status, Integer ade);

  long countByBetriebsStatusIdAndEnergieTraegerId(Integer status, Integer energieTreagerId);
  long countByBetriebsStatusIdAndEnergieTraegerIdAndInbetriebnahmeDatumAfter(Integer status, Integer energieTreagerId, LocalDate startDate);
  long countByBetriebsStatusIdAndEnergieTraegerIdAndInbetriebnahmeDatumBetween(Integer status, Integer energieTreagerId, LocalDate startDate, LocalDate endDate);
  
  @Query("SELECT SUM(mu.bruttoLeistung) FROM MastrUnit mu WHERE mu.betriebsStatusId = :status and mu.energieTraegerId = :energieTreagerId")
  Double sumBruttoLeistungByBetriebsStatusId(@Param("status") Integer status, @Param("energieTreagerId") Integer energieTreagerId);

  @Query("SELECT SUM(mu.bruttoLeistung) FROM MastrUnit mu WHERE mu.betriebsStatusId = :status and mu.energieTraegerId = :energieTreagerId and mu.inbetriebnahmeDatum >= :startDate")
  Double sumBruttoLeistungByBetriebsStatusIdAfterDate(@Param("status") Integer status, @Param("energieTreagerId") Integer energieTreagerId, @Param("startDate") LocalDate startDate);

  @Query("SELECT SUM(mu.nutzbareSpeicherKapazitaet) FROM MastrUnit mu WHERE mu.betriebsStatusId = :status and mu.energieTraegerId = :energieTreagerId and mu.inbetriebnahmeDatum >= :startDate")
  Double sumNutzbareSpeicherkapazitaetByBetriebsStatusIdAfterDate(@Param("status") Integer status, @Param("energieTreagerId") Integer energieTreagerId, @Param("startDate") LocalDate startDate);

  @Query("SELECT SUM(mu.bruttoLeistung) FROM MastrUnit mu WHERE mu.betriebsStatusId = :status and mu.energieTraegerId = :energieTreagerId and mu.inbetriebnahmeDatum >= :startDate and mu.inbetriebnahmeDatum < :endDate")
  Double sumBruttoLeistungByBetriebsStatusIdBetweenDates(@Param("status") Integer status, @Param("energieTreagerId") Integer energieTreagerId, @Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

  @Query("SELECT SUM(mu.nutzbareSpeicherKapazitaet) FROM MastrUnit mu WHERE mu.betriebsStatusId = :status and mu.energieTraegerId = :energieTreagerId and mu.inbetriebnahmeDatum >= :startDate and mu.inbetriebnahmeDatum < :endDate")
  Double sumNutzbareSpeicherkapazitaetByBetriebsStatusIdBetweenDates(@Param("status") Integer status, @Param("energieTreagerId") Integer energieTreagerId, @Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

  @Query("SELECT sum(mu.bruttoLeistung) FROM MastrUnit mu WHERE mu.betriebsStatusId = 35 and mu.energieTraegerId = :energieTreagerId and mu.bruttoLeistung >= :min AND mu.bruttoLeistung < :max AND mu.administrationUnit.id = :administration_unit_id")
  Double getSumBruttoleistungByAgsBetweenBruttoleistung(@Param("administration_unit_id") Long administrationUnitId, @Param("energieTreagerId") Integer energieTreagerId, @Param("min") Double min, @Param("max") Double max);

  @Query("SELECT count(*) FROM MastrUnit mu WHERE mu.betriebsStatusId = 35 and mu.energieTraegerId = :energieTreagerId and mu.bruttoLeistung >= :min AND mu.bruttoLeistung < :max AND mu.administrationUnit.id = :administration_unit_id")
  Double getCountBruttoleistungByAgsBetweenBruttoleistung(@Param("administration_unit_id") Long administrationUnitId, @Param("energieTreagerId") Integer energieTreagerId, @Param("min") Double min, @Param("max") Double max);
}
