package eu.danielgerber.repository;

import eu.danielgerber.domain.ChargingStation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ChargingStation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChargingStationRepository extends JpaRepository<ChargingStation, Long>, JpaSpecificationExecutor<ChargingStation> {}
