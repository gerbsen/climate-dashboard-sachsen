package eu.danielgerber.repository;

import eu.danielgerber.domain.AdministrationUnit;

import java.util.List;
import java.util.Optional;

import org.locationtech.jts.geom.Geometry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AdministrationUnit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdministrationUnitRepository
    extends JpaRepository<AdministrationUnit, Long>, JpaSpecificationExecutor<AdministrationUnit> {

    @Query("select administrationUnit from AdministrationUnit administrationUnit left join fetch administrationUnit.statistics where administrationUnit.id =:id")
    Optional<AdministrationUnit> findOneWithToOneRelationships(@Param("id") Long id);

    AdministrationUnit findByGemeindeschluesselAgs(String ags);

    AdministrationUnit findByGemeindeschluesselaufgefuelltAndAde(String gemeindeSchluessel, Integer ade);

    /**
     * we need to override this method in order to use a fetch join query to always
     * get all statistics in one query when querying for administration units. if not
     * applied we run into n+1 queries for all statistics for each AU.
     *
     * BUT this might cause in memory pagination which is very bad:
     * https://tech.asimio.net/2021/05/19/Fixing-Hibernate-HHH000104-firstResult-maxResults-warning-using-Spring-Data-JPA.html
     *
     * @param spec can be {@literal null}.
     * @param pageable must not be {@literal null}.
     * @return all filtered administration units.
     */
    @Override
    @EntityGraph(attributePaths = { "statistics" })
    Page<AdministrationUnit> findAll(@Nullable Specification<AdministrationUnit> spec, Pageable pageable);

    @Query("FROM AdministrationUnit au JOIN FETCH au.statistics WHERE au.ade = 6 and within(:geometry, au.geometry) = TRUE")
    AdministrationUnit findByStContainsPoint(@Param("geometry") Geometry geometry);
}
