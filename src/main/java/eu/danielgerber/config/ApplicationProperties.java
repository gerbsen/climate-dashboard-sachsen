package eu.danielgerber.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Energytransitiondashboard.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link tech.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private Boolean downloadDwdOnStartup = false;
    private Boolean downloadUfzOnStartup = false;
    private Boolean downloadMastrOnStartup = false;
    private Boolean downloadLadesaulenRegisterOnStartup = false;
    private String state = "Sachsen";
    private String ladesaeulenRegisterUrl = "";

    public Boolean getDownloadUfzOnStartup() {
        return downloadUfzOnStartup;
    }

    public void setDownloadUfzOnStartup(Boolean downloadUfzOnStartup) {
        this.downloadUfzOnStartup = downloadUfzOnStartup;
    }

    public Boolean getDownloadDwdOnStartup() {
        return downloadDwdOnStartup;
    }

    public void setDownloadDwdOnStartup(Boolean downloadDwdOnStartup) {
        this.downloadDwdOnStartup = downloadDwdOnStartup;
    }
    
    public Boolean getDownloadMastrOnStartup() {
        return downloadMastrOnStartup;
    }

    public void setDownloadMastrOnStartup(Boolean downloadMastrOnStartup) {
        this.downloadMastrOnStartup = downloadMastrOnStartup;
    }

    public Boolean getDownloadLadesaulenRegisterOnStartup() {
        return downloadLadesaulenRegisterOnStartup;
    }

    public void setDownloadLadesaulenRegisterOnStartup(Boolean downloadLadesaulenRegisterOnStartup) {
        this.downloadLadesaulenRegisterOnStartup = downloadLadesaulenRegisterOnStartup;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateId() {

        if ("Brandenburg".equals(this.state)) return "1400";
        if ("Berlin".equals(this.state)) return "1401";
        if ("Baden-Württemberg".equals(this.state)) return "1402";
        if ("Bayern".equals(this.state)) return "1403";
        if ("Bremen".equals(this.state)) return "1404";
        if ("Hessen".equals(this.state)) return "1405";
        if ("Hamburg".equals(this.state)) return "1406";
        if ("Mecklenburg-Vorpommern".equals(this.state)) return "1407";
        if ("Niedersachsen".equals(this.state)) return "1408";
        if ("Nordrhein-Westfalen".equals(this.state)) return "1409";
        if ("Rheinland-Pfalz".equals(this.state)) return "1410";
        if ("Schleswig-Holstein".equals(this.state)) return "1411";
        if ("Saarland".equals(this.state)) return "1412";
        if ("Sachsen".equals(this.state)) return "1413";
        if ("Sachsen-Anhalt".equals(this.state)) return "1414";
        if ("Thüringen".equals(this.state)) return "1415";

        return "-1";
    }

    public String getAgsStateId() {

        if ("Schleswig-Holstein".equals(this.state)) return "01";
        if ("Hamburg".equals(this.state)) return "02";
        if ("Niedersachsen".equals(this.state)) return "03";
        if ("Bremen".equals(this.state)) return "04";
        if ("Nordrhein-Westfalen".equals(this.state)) return "05";
        if ("Hessen".equals(this.state)) return "06";
        if ("Rheinland-Pfalz".equals(this.state)) return "07";
        if ("Baden-Württemberg".equals(this.state)) return "08";
        if ("Bayern".equals(this.state)) return "09";
        if ("Saarland".equals(this.state)) return "10";
        if ("Berlin".equals(this.state)) return "11";
        if ("Brandenburg".equals(this.state)) return "12";
        if ("Mecklenburg-Vorpommern".equals(this.state)) return "13";
        if ("Sachsen".equals(this.state)) return "14";
        if ("Sachsen-Anhalt".equals(this.state)) return "15";
        if ("Thüringen".equals(this.state)) return "16";

        return "-1";
    }

    public String getLadesaeulenRegisterUrl() {
        return ladesaeulenRegisterUrl;
    }

    public void setLadesaeulenRegisterUrl(String ladesaeulenRegisterUrl) {
        this.ladesaeulenRegisterUrl = ladesaeulenRegisterUrl;
    }

    // jhipster-needle-application-properties-property
    // jhipster-needle-application-properties-property-getter
    // jhipster-needle-application-properties-property-class
}
