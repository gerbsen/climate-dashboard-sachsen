package eu.danielgerber.service;

import eu.danielgerber.domain.*; // for static metamodels
import eu.danielgerber.domain.enums.IntervalFormat;
import eu.danielgerber.repository.ChargingStationRepository;
import eu.danielgerber.service.criteria.ChargingStationCriteria;
import eu.danielgerber.service.dto.ChargingPointOperatorDTO;
import eu.danielgerber.service.dto.ChargingPointOperatorsDTO;
import eu.danielgerber.service.dto.ChargingStationsStatisticsForInterval;
import eu.danielgerber.service.dto.ChargingStationsStatisticsForIntervalDTO;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ChargingStation} entities in the database.
 * The main input is a {@link ChargingStationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ChargingStation} or a {@link Page} of {@link ChargingStation} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ChargingStationQueryService extends QueryService<ChargingStation> {

    private final Logger log = LoggerFactory.getLogger(ChargingStationQueryService.class);

    private final ChargingStationRepository chargingStationRepository;
    private final EntityManager em;

    public ChargingStationQueryService(ChargingStationRepository chargingStationRepository, EntityManager em) {
        this.chargingStationRepository = chargingStationRepository;
        this.em = em;
    }

    /**
     * Return a {@link List} of {@link ChargingStation} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ChargingStation> findByCriteria(ChargingStationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ChargingStation> specification = createSpecification(criteria);
        return chargingStationRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ChargingStation} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ChargingStation> findByCriteria(ChargingStationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ChargingStation> specification = createSpecification(criteria);
        return chargingStationRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ChargingStationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ChargingStation> specification = createSpecification(criteria);
        return chargingStationRepository.count(specification);
    }

    /** 
     * TODO comment
     * @param endDate
     * @param startDate
     */
    public ChargingPointOperatorsDTO getBetreiberGroupBy(Date startDate, Date endDate){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        List<String> whereClauses = new ArrayList<>();
        if (startDate != null) whereClauses.add(String.format("inbetriebnahme_datum >= '%s'", format.format(startDate)));
        if (endDate != null) whereClauses.add(String.format("inbetriebnahme_datum <= '%s'", format.format(endDate)));

        String query = 
        " SELECT betreiber as operator, " +
        "   sum(nennleistung_ladeeinrichtung) as power, " +
        "   sum(anzahl_ladepunkte) as points, " +
        "   sum(nennleistung_ladeeinrichtung) / sum(anzahl_ladepunkte) as average" +
        " FROM charging_station " +
         (startDate == null && endDate == null ? "" : " WHERE "  + String.join(" AND ", whereClauses)) + 
        " GROUP BY operator" +
        " ORDER BY power DESC;";

        long start = System.currentTimeMillis();
        ChargingPointOperatorsDTO dto = new ChargingPointOperatorsDTO();
        @SuppressWarnings("unchecked")
        List<Object[][]> resultList = em.createNativeQuery(query).getResultList();
        for ( Object[] row : resultList ) {
            dto.addOperators(new ChargingPointOperatorDTO((String) row[0], (double) row[1], ((BigInteger) row[2]).intValue(), (double) row[3]));
        }
        dto.setMillis(System.currentTimeMillis() - start);

        return dto;
    }

    /**
     * This method groups the charging stations's operation start date by certain 
     * time intervals (i.e. year, month, week) It calculates the sum of electrical
     * power and the sum of charging points for all time intervals, as well es the
     * cumulative sum for power and charging points of the complete time series.
     * 
     * @return a statistic for all time intervals for charging stations
     */
    @SuppressWarnings("unchecked")
    public ChargingStationsStatisticsForIntervalDTO getChargingStationsForInterval() {
        
        ChargingStationsStatisticsForIntervalDTO dto = new ChargingStationsStatisticsForIntervalDTO();
        for (IntervalFormat intervalFormat : IntervalFormat.values()) { 
            
            String query = String.format(
                "SELECT " +
                    "to_char(cs.inbetriebnahme_datum, '%s') as date, " +
                    "sum(nennleistung_ladeeinrichtung) as power, " +
                    "sum(anzahl_ladepunkte) as points " + 
                "FROM charging_station cs " +
                "GROUP BY date " +
                "ORDER BY date;",
                intervalFormat.getFormat());

            dto.addIntervalStatistics(intervalFormat, ((List<Object[]>) em.createNativeQuery(query).getResultList())
                .stream()
                .map((row) -> new ChargingStationsStatisticsForInterval((String) row[0], (double) row[1], ((BigInteger) row[2]).intValue()))
                .collect(Collectors.toList()));
            
            double sumPower = 0;
            int sumPoints = 0;
            for ( ChargingStationsStatisticsForInterval row : dto.getIntervalStatistics(intervalFormat) ) {
                sumPower += row.getTimeUnitPower();
                row.setTotalPower(sumPower);
                sumPoints += row.getTimeUnitChargingPoints();
                row.setTotalChargingPoints(sumPoints);
            }
        }

        return dto.finish();
    }

    /**
     * Function to convert {@link ChargingStationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ChargingStation> createSpecification(ChargingStationCriteria criteria) {
        Specification<ChargingStation> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ChargingStation_.id));
            }
            if (criteria.getBetreiber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBetreiber(), ChargingStation_.betreiber));
            }
            if (criteria.getStrasse() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStrasse(), ChargingStation_.strasse));
            }
            if (criteria.getHausnummer() != null) {
                specification = specification.and(buildStringSpecification(criteria.getHausnummer(), ChargingStation_.hausnummer));
            }
            if (criteria.getAdresszusatz() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAdresszusatz(), ChargingStation_.adresszusatz));
            }
            if (criteria.getPostleitzahl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPostleitzahl(), ChargingStation_.postleitzahl));
            }
            if (criteria.getOrt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOrt(), ChargingStation_.ort));
            }
            if (criteria.getBundesland() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBundesland(), ChargingStation_.bundesland));
            }
            if (criteria.getKreisKreisfreieStadt() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getKreisKreisfreieStadt(), ChargingStation_.kreisKreisfreieStadt));
            }
            if (criteria.getInbetriebnahmeDatum() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getInbetriebnahmeDatum(), ChargingStation_.inbetriebnahmeDatum));
            }
            if (criteria.getNennleistungLadeeinrichtung() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getNennleistungLadeeinrichtung(), ChargingStation_.nennleistungLadeeinrichtung)
                    );
            }
            if (criteria.getArtDerLadeeinrichtung() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getArtDerLadeeinrichtung(), ChargingStation_.artDerLadeeinrichtung)
                    );
            }
            if (criteria.getAnzahlLadepunkte() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getAnzahlLadepunkte(), ChargingStation_.anzahlLadepunkte));
            }
            if (criteria.getErsterLadepunktSteckertypen() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getErsterLadepunktSteckertypen(), ChargingStation_.ersterLadepunktSteckertypen)
                    );
            }
            if (criteria.getErsterLadepunktLeistung() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getErsterLadepunktLeistung(), ChargingStation_.ersterLadepunktLeistung)
                    );
            }
            if (criteria.getZweiterLadepunktSteckertypen() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getZweiterLadepunktSteckertypen(), ChargingStation_.zweiterLadepunktSteckertypen)
                    );
            }
            if (criteria.getZweiterLadepunktLeistung() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getZweiterLadepunktLeistung(), ChargingStation_.zweiterLadepunktLeistung)
                    );
            }
            if (criteria.getDritterLadepunktSteckertypen() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getDritterLadepunktSteckertypen(), ChargingStation_.dritterLadepunktSteckertypen)
                    );
            }
            if (criteria.getDritterLadepunktLeistung() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getDritterLadepunktLeistung(), ChargingStation_.dritterLadepunktLeistung)
                    );
            }
            if (criteria.getVierterLadepunktSteckertypen() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getVierterLadepunktSteckertypen(), ChargingStation_.vierterLadepunktSteckertypen)
                    );
            }
            if (criteria.getVierterLadepunktLeistung() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getVierterLadepunktLeistung(), ChargingStation_.vierterLadepunktLeistung)
                    );
            }
            if (criteria.getAdministrationUnitId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAdministrationUnitId(),
                            root -> root.join(ChargingStation_.administrationUnit, JoinType.LEFT).get(AdministrationUnit_.id)
                        )
                    );
            }
            if (criteria.getStatisticId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getStatisticId(),
                            root -> root.join(ChargingStation_.statistics, JoinType.LEFT).get(Statistic_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
