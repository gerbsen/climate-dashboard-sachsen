package eu.danielgerber.service;

public enum AdministrativeLevel {
  COMMUNITY(6),
  DISTRICT(4);

  private Integer level;

  AdministrativeLevel(Integer level){
    this.level = level;
  }

  public Integer getLevel() { return level; }

  public String toString(){
    return this.level + "";
  }
}
