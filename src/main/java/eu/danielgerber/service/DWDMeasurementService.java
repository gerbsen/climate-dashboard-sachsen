package eu.danielgerber.service;

import eu.danielgerber.config.ApplicationProperties;
import eu.danielgerber.domain.DWDMeasurement;
import eu.danielgerber.domain.enums.DWDState;
import eu.danielgerber.repository.DWDMeasurementRepository;
import eu.danielgerber.service.dto.DWDMeasurementsDTO;
import eu.danielgerber.service.dto.DWDYearMeasurement;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;

/**
 * Service Implementation for managing {@link DWDMeasurement}.
 */
@Service
@Transactional
public class DWDMeasurementService {

    private final Logger log = LoggerFactory.getLogger(DWDMeasurementService.class);

    private final DWDMeasurementRepository dWDMeasurementRepository;

    private ApplicationProperties applicationProperties;

    private final EntityManager em;

    public DWDMeasurementService(
        ApplicationProperties applicationProperties, 
        DWDMeasurementRepository dWDMeasurementRepository,
        EntityManager em) {
        this.dWDMeasurementRepository = dWDMeasurementRepository;
        this.applicationProperties = applicationProperties;
        this.em = em;
    }

    /**
     * Get all the dWDMeasurements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<DWDMeasurement> findAll(Pageable pageable) {
        log.debug("Request to get all DWDMeasurements");
        return dWDMeasurementRepository.findAll(pageable);
    }

    /**
     * Get one dWDMeasurement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DWDMeasurement> findOne(Long id) {
        log.debug("Request to get DWDMeasurement : {}", id);
        return dWDMeasurementRepository.findById(id);
    }

    @PostConstruct
    @Transactional
    public void downloadData() throws MalformedURLException, IOException, CsvValidationException, InterruptedException {

        if ( this.applicationProperties.getDownloadDwdOnStartup() ) {
            
            // delete all previously imported data, everything is reimported
            this.dWDMeasurementRepository.deleteAll();

            List<String> downloadUrls = new ArrayList<>(Arrays.asList(
                "annual/frost_days/regional_averages_tnas_year.txt",
                "annual/air_temperature_mean/regional_averages_tm_year.txt", 
                "annual/hot_days/regional_averages_txbs_year.txt", 
                "annual/ice_days/regional_averages_txcs_year.txt", 
                "annual/precipGE10mm_days/regional_averages_rrsfs_year.txt", 
                "annual/precipGE20mm_days/regional_averages_rrsgs_year.txt", 
                "annual/precipitation/regional_averages_rr_year.txt", 
                "annual/summer_days/regional_averages_txas_year.txt", 
                "annual/sunshine_duration/regional_averages_sd_year.txt", 
                "annual/tropical_nights_tminGE20/regional_averages_tnes_year.txt"
            ));
            for ( int i = 1; i <= 12; i++ ) {
                downloadUrls.add("monthly/precipitation/regional_averages_rr_MONTH.txt".replace("MONTH", String.format("%02d", i)));
                downloadUrls.add("monthly/air_temperature_mean/regional_averages_tm_MONTH.txt".replace("MONTH", String.format("%02d", i)));
                downloadUrls.add("monthly/sunshine_duration/regional_averages_sd_MONTH.txt".replace("MONTH", String.format("%02d", i)));
            }

            for (String urlPart : downloadUrls) {
                log.info("Starting to import {} data.", urlPart);

                // download the file and store it in a temp file
                InputStream in = new URL("https://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/" + urlPart).openStream();
                Path tempFile = Files.createTempFile(null, null);
                Files.copy(in, tempFile, StandardCopyOption.REPLACE_EXISTING);

                // setup csv reader and utils
                String[] row;
                List<DWDMeasurement> measurements = new ArrayList<>();
                CSVReader reader = new CSVReaderBuilder(new FileReader(tempFile.toFile(), StandardCharsets.ISO_8859_1))
                    .withCSVParser(new CSVParserBuilder().withSeparator(';').build()).withSkipLines(2).build();

                while ((row = reader.readNext()) != null) {
                    
                    String statisticName = StringUtils.substringBetween(urlPart, "/", "/");
                    if ( urlPart.contains("monthly") )
                        statisticName += "-" + Integer.valueOf(urlPart.substring(urlPart.lastIndexOf("_")+ 1, urlPart.lastIndexOf(".txt")));

                    DWDMeasurement measurement = new DWDMeasurement().year(Long.parseLong(row[0]))
                        .statistic(statisticName)
                        .brandenburgBerlin(Double.valueOf(row[2])).brandenburg(Double.valueOf(row[3]))
                        .badenWuerttemberg(Double.valueOf(row[4])).bayern(Double.valueOf(row[5]))
                        .hessen(Double.valueOf(row[6])).mecklenburgVorpommern(Double.valueOf(row[7]))
                        .niedersachsen(Double.valueOf(row[8])).niedersachsenHamburgBremen(Double.valueOf(row[9]))
                        .nordrheinWestfalen(Double.valueOf(row[10])).rheinlandPfalz(Double.valueOf(row[11]))
                        .schleswigHolstein(Double.valueOf(row[12])).saarland(Double.valueOf(row[13]))
                        .sachsen(Double.valueOf(row[14])).sachsenAnhalt(Double.valueOf(row[15]))
                        .thueringenSachsenAnhalt(Double.valueOf(row[16])).thueringen(Double.valueOf(row[17]))
                        .deutschland(Double.valueOf(row[18]));

                    measurements.add(measurement);
                }

                this.dWDMeasurementRepository.saveAll(measurements);
            }
        }
        else {
            log.warn(this.dWDMeasurementRepository.count() == 0 ? "Alert: There are zero DWDMeasurements in the database. Please import!" : "Importing of DWD measurements disabled.");
        }
    }

    @SuppressWarnings("unchecked")
    public DWDMeasurementsDTO getDWDMeasurementsDTO(DWDState state) {

        String query = "SELECT %s from dwd_measurement where statistic = '%s' order by year asc;";
        String queryYears = "SELECT year from dwd_measurement where statistic = 'air_temperature_mean' order by year asc;";
        String queryAirTempMean = String.format(query, state, "air_temperature_mean");
        String queryTropicalNights = String.format(query, state, "tropical_nights_tminGE20");
        String querySunshireDuration = String.format(query, state, "sunshine_duration");
        String queryPercip10 = String.format(query, state, "precipGE10mm_days");
        String queryPercip20 = String.format(query, state, "precipGE20mm_days");
        String queryHotDays = String.format(query, state, "hot_days");
        String querySummerDays = String.format(query, state, "summer_days");
        String queryFrostDays = String.format(query, state, "frost_days");
        String queryIceDays = String.format(query, state, "ice_days");

        long start = System.currentTimeMillis();
        DWDMeasurementsDTO dwdMeasurementsDTO = new DWDMeasurementsDTO();
        dwdMeasurementsDTO.setState(state);
        dwdMeasurementsDTO.setYears(em.createNativeQuery(queryYears).getResultList());
        dwdMeasurementsDTO.setAirTemperatureMean(em.createNativeQuery(queryAirTempMean).getResultList());
        dwdMeasurementsDTO.setTropicalNightsTminGE20(em.createNativeQuery(queryTropicalNights).getResultList());
        dwdMeasurementsDTO.setSunshineDuration(em.createNativeQuery(querySunshireDuration).getResultList());
        dwdMeasurementsDTO.setPrecipE10mmDays(em.createNativeQuery(queryPercip10).getResultList());
        dwdMeasurementsDTO.setPrecipE20mmDays(em.createNativeQuery(queryPercip20).getResultList());
        dwdMeasurementsDTO.setHotDays(em.createNativeQuery(queryHotDays).getResultList());
        dwdMeasurementsDTO.setSummerDays(em.createNativeQuery(querySummerDays).getResultList());
        dwdMeasurementsDTO.setFrostDays(em.createNativeQuery(queryFrostDays).getResultList());
        dwdMeasurementsDTO.setIceDays(em.createNativeQuery(queryIceDays).getResultList());
        
        for ( String statistic : Arrays.asList("sunshine_duration", "air_temperature_mean", "precipitation")) {
            for ( int i = 1; i < 13; i++) {

                String yearToValueQuery = String.format("SELECT year, %s from dwd_measurement where statistic = '%s' order by year asc;", state, statistic +  "-" + i);
                List<Object[]> resultList = em.createNativeQuery(yearToValueQuery).getResultList();
                for ( int j = 0; j < resultList.size(); j++ ) {

                    Object[] row = (Object[]) resultList.get(j);

                    DWDYearMeasurement dwdMeasurement = dwdMeasurementsDTO.getDWDYearMeasurement(statistic, ((BigInteger) row[0]).intValue());
                    dwdMeasurement.setYear(((BigInteger) row[0]).intValue());
                    if ( i == 1 ) dwdMeasurement.setJanuary((Double) row[1]);
                    else if ( i == 2 ) dwdMeasurement.setFebruary((Double) row[1]);
                    else if ( i == 3 ) dwdMeasurement.setMarch((Double) row[1]);
                    else if ( i == 4 ) dwdMeasurement.setApril((Double) row[1]);
                    else if ( i == 5 ) dwdMeasurement.setMay((Double) row[1]);
                    else if ( i == 6 ) dwdMeasurement.setJune((Double) row[1]);
                    else if ( i == 7 ) dwdMeasurement.setJuly((Double) row[1]);
                    else if ( i == 8 ) dwdMeasurement.setAugust((Double) row[1]);
                    else if ( i == 9 ) dwdMeasurement.setSeptember((Double) row[1]);
                    else if ( i == 10 ) dwdMeasurement.setOctober((Double) row[1]);
                    else if ( i == 11 ) dwdMeasurement.setNovember((Double) row[1]);
                    else if ( i == 12 ) dwdMeasurement.setDecember((Double) row[1]);
                }
            }
        }
        dwdMeasurementsDTO.setMillis(System.currentTimeMillis() - start);

        return dwdMeasurementsDTO;
    }
}
