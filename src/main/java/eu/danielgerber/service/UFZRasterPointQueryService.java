package eu.danielgerber.service;

import eu.danielgerber.domain.*; // for static metamodels
import eu.danielgerber.domain.UFZRasterPoint;
import eu.danielgerber.repository.UFZRasterPointRepository;
import eu.danielgerber.service.criteria.UFZRasterPointCriteria;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link UFZRasterPoint} entities in the database.
 * The main input is a {@link UFZRasterPointCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UFZRasterPoint} or a {@link Page} of {@link UFZRasterPoint} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UFZRasterPointQueryService extends QueryService<UFZRasterPoint> {

    private final Logger log = LoggerFactory.getLogger(UFZRasterPointQueryService.class);

    private final UFZRasterPointRepository uFZRasterPointRepository;

    public UFZRasterPointQueryService(UFZRasterPointRepository uFZRasterPointRepository) {
        this.uFZRasterPointRepository = uFZRasterPointRepository;
    }

    /**
     * Return a {@link List} of {@link UFZRasterPoint} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UFZRasterPoint> findByCriteria(UFZRasterPointCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UFZRasterPoint> specification = createSpecification(criteria);
        return uFZRasterPointRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link UFZRasterPoint} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UFZRasterPoint> findByCriteria(UFZRasterPointCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UFZRasterPoint> specification = createSpecification(criteria);
        return uFZRasterPointRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UFZRasterPointCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UFZRasterPoint> specification = createSpecification(criteria);
        return uFZRasterPointRepository.count(specification);
    }

    /**
     * Function to convert {@link UFZRasterPointCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<UFZRasterPoint> createSpecification(UFZRasterPointCriteria criteria) {
        Specification<UFZRasterPoint> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), UFZRasterPoint_.id));
            }
            if (criteria.getLat() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLat(), UFZRasterPoint_.lat));
            }
            if (criteria.getLng() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLng(), UFZRasterPoint_.lng));
            }
            if (criteria.getUfzMeasurementId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getUfzMeasurementId(),
                            root -> root.join(UFZRasterPoint_.ufzMeasurements, JoinType.LEFT).get(UFZMeasurement_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
