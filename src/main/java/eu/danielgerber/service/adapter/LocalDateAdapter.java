package eu.danielgerber.service.adapter;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

public class LocalDateAdapter {

    @ToJson
    String toJson(LocalDate date) {
        return date.toString();
    }

    @FromJson
    LocalDate fromJson(String date) {
        String replace = date.replace("/Date(", "").replace(")/", "");
        return Instant.ofEpochMilli(Long.valueOf(replace)).atZone(ZoneId.systemDefault()).toLocalDate();
    }
}
