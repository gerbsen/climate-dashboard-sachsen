package eu.danielgerber.service.adapter;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;

public class GeometryAdapter {

    @ToJson
    String toJson(Geometry geometry) {
        return geometry.toText();
    }

    @FromJson
    Geometry fromJson(String geometry) {
        try {
            return new WKTReader().read(geometry);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
