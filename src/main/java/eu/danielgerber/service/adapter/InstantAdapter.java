package eu.danielgerber.service.adapter;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

import java.time.Instant;

public class InstantAdapter {

    @ToJson
    public Long toJson(Instant input) {
        return input.toEpochMilli();
    }

    @FromJson
    public Instant fromJson(Long input) {
        return Instant.ofEpochMilli(input);
    }
}
