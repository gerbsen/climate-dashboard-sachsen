package eu.danielgerber.service;

import eu.danielgerber.domain.*; // for static metamodels
import eu.danielgerber.domain.Statistic;
import eu.danielgerber.repository.StatisticRepository;
import eu.danielgerber.service.criteria.StatisticCriteria;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Statistic} entities in the database.
 * The main input is a {@link StatisticCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Statistic} or a {@link Page} of {@link Statistic} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class StatisticQueryService extends QueryService<Statistic> {

    private final Logger log = LoggerFactory.getLogger(StatisticQueryService.class);

    private final StatisticRepository statisticRepository;

    public StatisticQueryService(StatisticRepository statisticRepository) {
        this.statisticRepository = statisticRepository;
    }

    /**
     * Return a {@link List} of {@link Statistic} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Statistic> findByCriteria(StatisticCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Statistic> specification = createSpecification(criteria);
        return statisticRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Statistic} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Statistic> findByCriteria(StatisticCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Statistic> specification = createSpecification(criteria);
        return statisticRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(StatisticCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Statistic> specification = createSpecification(criteria);
        return statisticRepository.count(specification);
    }

    /**
     * Function to convert {@link StatisticCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Statistic> createSpecification(StatisticCriteria criteria) {
        Specification<Statistic> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Statistic_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Statistic_.name));
            }
            if (criteria.getKey() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKey(), Statistic_.key));
            }
            if (criteria.getValue() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValue(), Statistic_.value));
            }
            if (criteria.getUnit() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUnit(), Statistic_.unit));
            }
            if (criteria.getDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDate(), Statistic_.date));
            }
            if (criteria.getSource() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSource(), Statistic_.source));
            }
            if (criteria.getAdministrationUnitId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAdministrationUnitId(),
                            root -> root.join(Statistic_.administrationUnit, JoinType.LEFT).get(AdministrationUnit_.id)
                        )
                    );
            }
            if (criteria.getChargingStationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getChargingStationId(),
                            root -> root.join(Statistic_.chargingStation, JoinType.LEFT).get(ChargingStation_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
