package eu.danielgerber.service;

import eu.danielgerber.domain.*; // for static metamodels
import eu.danielgerber.domain.enums.AggregationFunction;
import eu.danielgerber.domain.enums.EnergySource;
import eu.danielgerber.domain.enums.IntervalFormat;
import eu.danielgerber.domain.enums.MastrUnitProperty;
import eu.danielgerber.domain.enums.OperationState;
import eu.danielgerber.repository.MastrUnitRepository;
import eu.danielgerber.service.criteria.MastrUnitCriteria;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.Year;
import java.time.temporal.ChronoField;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.JoinType;

import eu.danielgerber.service.dto.MastrUnitForIntervalStatisticDTO;
import eu.danielgerber.service.dto.StateEnergyStatisitcsDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link MastrUnit} entities in the database.
 * The main input is a {@link MastrUnitCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MastrUnit} or a {@link Page} of {@link MastrUnit} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MastrUnitQueryService extends QueryService<MastrUnit> {

    private final Logger log = LoggerFactory.getLogger(MastrUnitQueryService.class);

    private final MastrUnitRepository mastrUnitRepository;

    private final EntityManager em;

    public MastrUnitQueryService(MastrUnitRepository mastrUnitRepository, EntityManager em) {
        this.mastrUnitRepository = mastrUnitRepository;
        this.em = em;
    }

    /**
     * Return a {@link List} of {@link MastrUnit} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MastrUnit> findByCriteria(MastrUnitCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MastrUnit> specification = createSpecification(criteria);
        return mastrUnitRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link MastrUnit} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MastrUnit> findByCriteria(MastrUnitCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MastrUnit> specification = createSpecification(criteria);
        return mastrUnitRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MastrUnitCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MastrUnit> specification = createSpecification(criteria);
        return mastrUnitRepository.count(specification);
    }

    public MastrUnitForIntervalStatisticDTO getStatisticsForInterval(String ags, IntervalFormat intervalFormat, MastrUnitProperty property,
                                                                     EnergySource energySource, AggregationFunction aggregateFunction) {
        String query = 
            ("SELECT " +
                    "count(*) as count, " +
                    "to_char(mu.inbetriebnahme_datum, 'INTERVAL_FORMAT') as date, " +
                    "AGGREGATE_FUNCTION(mu.PROPERTY) as aggregate " +
            "FROM mastr_unit mu " +
            "WHERE " +
                    "mu.energie_traeger_id = ENERGY_SOURCE and " +
                    (ags != null ? "mu.ags = 'AGS' and " : "") +
                    "mu.inbetriebnahme_datum >= '2000-01-01' " +
            "GROUP BY to_char(mu.inbetriebnahme_datum, 'INTERVAL_FORMAT') " +
            "ORDER BY date;")
                .replaceAll("INTERVAL_FORMAT", intervalFormat.getFormat())
                .replaceAll("AGGREGATE_FUNCTION", aggregateFunction.name())
                .replaceAll("AGS", ags)
                .replaceAll("PROPERTY", property.getPropertyName())
                .replaceAll("ENERGY_SOURCE", energySource.getId() + "");

        long start = System.currentTimeMillis();
        MastrUnitForIntervalStatisticDTO dto = new MastrUnitForIntervalStatisticDTO();
        @SuppressWarnings("unchecked") List<Object[][]> resultList = em.createNativeQuery(query).getResultList();
        for ( Object[] row : resultList ) {
            dto.addResult(new MastrUnitForIntervalStatisticElement(((BigInteger) row[0]).intValue(), (String) row[1], (double) row[2]));
        }
        dto.setAgs(ags);
        dto.setAggregateFunction(aggregateFunction);
        dto.setIntervalFormat(intervalFormat);
        dto.setPropertyName(property);
        dto.setEnergySource(energySource);
        dto.setMillis(System.currentTimeMillis() - start);

        return dto;
    }

    /**
     * Function to convert {@link MastrUnitCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<MastrUnit> createSpecification(MastrUnitCriteria criteria) {
        Specification<MastrUnit> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), MastrUnit_.id));
            }
            if (criteria.getMastrNummer() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMastrNummer(), MastrUnit_.mastrNummer));
            }
            if (criteria.getAnlagenBetreiberId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAnlagenBetreiberId(), MastrUnit_.anlagenBetreiberId));
            }
            if (criteria.getAnlagenBetreiberPersonenArt() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getAnlagenBetreiberPersonenArt(), MastrUnit_.anlagenBetreiberPersonenArt)
                    );
            }
            if (criteria.getAnlagenBetreiberMaskedName() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getAnlagenBetreiberMaskedName(), MastrUnit_.anlagenBetreiberMaskedName)
                    );
            }
            if (criteria.getAnlagenBetreiberMastrNummer() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getAnlagenBetreiberMastrNummer(), MastrUnit_.anlagenBetreiberMastrNummer)
                    );
            }
            if (criteria.getAnlagenBetreiberName() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getAnlagenBetreiberName(), MastrUnit_.anlagenBetreiberName));
            }
            if (criteria.getBetriebsStatusId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBetriebsStatusId(), MastrUnit_.betriebsStatusId));
            }
            if (criteria.getBetriebsStatusName() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getBetriebsStatusName(), MastrUnit_.betriebsStatusName));
            }
            if (criteria.getBundesland() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBundesland(), MastrUnit_.bundesland));
            }
            if (criteria.getBundeslandId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBundeslandId(), MastrUnit_.bundeslandId));
            }
            if (criteria.getLandkreisId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLandkreisId(), MastrUnit_.landkreisId));
            }
            if (criteria.getGemeindeId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGemeindeId(), MastrUnit_.gemeindeId));
            }
            if (criteria.getDatumLetzteAktualisierung() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getDatumLetzteAktualisierung(), MastrUnit_.datumLetzteAktualisierung)
                    );
            }
            if (criteria.getEinheitRegistrierungsDatum() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getEinheitRegistrierungsDatum(), MastrUnit_.einheitRegistrierungsDatum)
                    );
            }
            if (criteria.getEndgueltigeStilllegungDatum() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getEndgueltigeStilllegungDatum(), MastrUnit_.endgueltigeStilllegungDatum)
                    );
            }
            if (criteria.getGeplantesInbetriebsnahmeDatum() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getGeplantesInbetriebsnahmeDatum(), MastrUnit_.geplantesInbetriebsnahmeDatum)
                    );
            }
            if (criteria.getInbetriebnahmeDatum() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getInbetriebnahmeDatum(), MastrUnit_.inbetriebnahmeDatum));
            }
            if (criteria.getKwkAnlageInbetriebnahmeDatum() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getKwkAnlageInbetriebnahmeDatum(), MastrUnit_.kwkAnlageInbetriebnahmeDatum)
                    );
            }
            if (criteria.getKwkAnlageRegistrierungsDatum() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getKwkAnlageRegistrierungsDatum(), MastrUnit_.kwkAnlageRegistrierungsDatum)
                    );
            }
            if (criteria.getEegInbetriebnahmeDatum() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getEegInbetriebnahmeDatum(), MastrUnit_.eegInbetriebnahmeDatum));
            }
            if (criteria.getEegAnlageRegistrierungsDatum() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getEegAnlageRegistrierungsDatum(), MastrUnit_.eegAnlageRegistrierungsDatum)
                    );
            }
            if (criteria.getGenehmigungDatum() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGenehmigungDatum(), MastrUnit_.genehmigungDatum));
            }
            if (criteria.getGenehmigungRegistrierungsDatum() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getGenehmigungRegistrierungsDatum(), MastrUnit_.genehmigungRegistrierungsDatum)
                    );
            }
            if (criteria.getIsNbPruefungAbgeschlossen() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getIsNbPruefungAbgeschlossen(), MastrUnit_.isNbPruefungAbgeschlossen)
                    );
            }
            if (criteria.getIsAnonymisiert() != null) {
                specification = specification.and(buildSpecification(criteria.getIsAnonymisiert(), MastrUnit_.isAnonymisiert));
            }
            if (criteria.getIsBuergerEnergie() != null) {
                specification = specification.and(buildSpecification(criteria.getIsBuergerEnergie(), MastrUnit_.isBuergerEnergie));
            }
            if (criteria.getIsEinheitNotstromaggregat() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getIsEinheitNotstromaggregat(), MastrUnit_.isEinheitNotstromaggregat));
            }
            if (criteria.getIsMieterstromAngemeldet() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getIsMieterstromAngemeldet(), MastrUnit_.isMieterstromAngemeldet));
            }
            if (criteria.getIsWasserkraftErtuechtigung() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getIsWasserkraftErtuechtigung(), MastrUnit_.isWasserkraftErtuechtigung));
            }
            if (criteria.getIsPilotWindanlage() != null) {
                specification = specification.and(buildSpecification(criteria.getIsPilotWindanlage(), MastrUnit_.isPilotWindanlage));
            }
            if (criteria.getIsPrototypAnlage() != null) {
                specification = specification.and(buildSpecification(criteria.getIsPrototypAnlage(), MastrUnit_.isPrototypAnlage));
            }
            if (criteria.getLat() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLat(), MastrUnit_.lat));
            }
            if (criteria.getLng() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLng(), MastrUnit_.lng));
            }
            if (criteria.getOrt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOrt(), MastrUnit_.ort));
            }
            if (criteria.getPlz() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPlz(), MastrUnit_.plz));
            }
            if (criteria.getStrasse() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStrasse(), MastrUnit_.strasse));
            }
            if (criteria.getHausnummer() != null) {
                specification = specification.and(buildStringSpecification(criteria.getHausnummer(), MastrUnit_.hausnummer));
            }
            if (criteria.getEinheitname() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEinheitname(), MastrUnit_.einheitname));
            }
            if (criteria.getFlurstueck() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFlurstueck(), MastrUnit_.flurstueck));
            }
            if (criteria.getGemarkung() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGemarkung(), MastrUnit_.gemarkung));
            }
            if (criteria.getGemeinde() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGemeinde(), MastrUnit_.gemeinde));
            }
            if (criteria.getLandId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLandId(), MastrUnit_.landId));
            }
            if (criteria.getLandkreis() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLandkreis(), MastrUnit_.landkreis));
            }
            if (criteria.getAgs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAgs(), MastrUnit_.ags));
            }
            if (criteria.getLokationId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLokationId(), MastrUnit_.lokationId));
            }
            if (criteria.getLokationMastrNr() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLokationMastrNr(), MastrUnit_.lokationMastrNr));
            }
            if (criteria.getNetzbetreiberId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNetzbetreiberId(), MastrUnit_.netzbetreiberId));
            }
            if (criteria.getNetzbetreiberMaskedNamen() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getNetzbetreiberMaskedNamen(), MastrUnit_.netzbetreiberMaskedNamen)
                    );
            }
            if (criteria.getNetzbetreiberMastrNummer() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getNetzbetreiberMastrNummer(), MastrUnit_.netzbetreiberMastrNummer)
                    );
            }
            if (criteria.getNetzbetreiberNamen() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getNetzbetreiberNamen(), MastrUnit_.netzbetreiberNamen));
            }
            if (criteria.getNetzbetreiberPersonenArt() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getNetzbetreiberPersonenArt(), MastrUnit_.netzbetreiberPersonenArt)
                    );
            }
            if (criteria.getSystemStatusId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSystemStatusId(), MastrUnit_.systemStatusId));
            }
            if (criteria.getSystemStatusName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSystemStatusName(), MastrUnit_.systemStatusName));
            }
            if (criteria.getTyp() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTyp(), MastrUnit_.typ));
            }
            if (criteria.getAktenzeichenGenehmigung() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getAktenzeichenGenehmigung(), MastrUnit_.aktenzeichenGenehmigung));
            }
            if (criteria.getAnzahlSolarmodule() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAnzahlSolarmodule(), MastrUnit_.anzahlSolarmodule));
            }
            if (criteria.getBatterieTechnologie() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getBatterieTechnologie(), MastrUnit_.batterieTechnologie));
            }
            if (criteria.getBruttoLeistung() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBruttoLeistung(), MastrUnit_.bruttoLeistung));
            }
            if (criteria.getEegInstallierteLeistung() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getEegInstallierteLeistung(), MastrUnit_.eegInstallierteLeistung));
            }
            if (criteria.getEegAnlageMastrNummer() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getEegAnlageMastrNummer(), MastrUnit_.eegAnlageMastrNummer));
            }
            if (criteria.getEegAnlagenSchluessel() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getEegAnlagenSchluessel(), MastrUnit_.eegAnlagenSchluessel));
            }
            if (criteria.getEegZuschlag() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEegZuschlag(), MastrUnit_.eegZuschlag));
            }
            if (criteria.getZuschlagsNummern() != null) {
                specification = specification.and(buildStringSpecification(criteria.getZuschlagsNummern(), MastrUnit_.zuschlagsNummern));
            }
            if (criteria.getEnergieTraegerId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEnergieTraegerId(), MastrUnit_.energieTraegerId));
            }
            if (criteria.getEnergieTraegerName() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getEnergieTraegerName(), MastrUnit_.energieTraegerName));
            }
            if (criteria.getGemeinsamerWechselrichter() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getGemeinsamerWechselrichter(), MastrUnit_.gemeinsamerWechselrichter)
                    );
            }
            if (criteria.getGenehmigungBehoerde() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getGenehmigungBehoerde(), MastrUnit_.genehmigungBehoerde));
            }
            if (criteria.getGenehmigungsMastrNummer() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getGenehmigungsMastrNummer(), MastrUnit_.genehmigungsMastrNummer));
            }
            if (criteria.getGruppierungsObjekte() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getGruppierungsObjekte(), MastrUnit_.gruppierungsObjekte));
            }
            if (criteria.getGruppierungsObjekteIds() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getGruppierungsObjekteIds(), MastrUnit_.gruppierungsObjekteIds));
            }
            if (criteria.getHatFlexibilitaetsPraemie() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getHatFlexibilitaetsPraemie(), MastrUnit_.hatFlexibilitaetsPraemie));
            }
            if (criteria.getHauptAusrichtungSolarmodule() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getHauptAusrichtungSolarmodule(), MastrUnit_.hauptAusrichtungSolarmodule)
                    );
            }
            if (criteria.getHauptAusrichtungSolarmoduleBezeichnung() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(
                            criteria.getHauptAusrichtungSolarmoduleBezeichnung(),
                            MastrUnit_.hauptAusrichtungSolarmoduleBezeichnung
                        )
                    );
            }
            if (criteria.getHauptBrennstoffId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getHauptBrennstoffId(), MastrUnit_.hauptBrennstoffId));
            }
            if (criteria.getHauptBrennstoffNamen() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getHauptBrennstoffNamen(), MastrUnit_.hauptBrennstoffNamen));
            }
            if (criteria.getHauptNeigungswinkelSolarModule() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getHauptNeigungswinkelSolarModule(), MastrUnit_.hauptNeigungswinkelSolarModule)
                    );
            }
            if (criteria.getHerstellerWindenergieanlageId() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getHerstellerWindenergieanlageId(), MastrUnit_.herstellerWindenergieanlageId)
                    );
            }
            if (criteria.getHerstellerWindenergieanlageBezeichnung() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(
                            criteria.getHerstellerWindenergieanlageBezeichnung(),
                            MastrUnit_.herstellerWindenergieanlageBezeichnung
                        )
                    );
            }
            if (criteria.getKwkAnlageElektrischeLeistung() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getKwkAnlageElektrischeLeistung(), MastrUnit_.kwkAnlageElektrischeLeistung)
                    );
            }
            if (criteria.getKwkAnlageMastrNummer() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getKwkAnlageMastrNummer(), MastrUnit_.kwkAnlageMastrNummer));
            }
            if (criteria.getKwkZuschlag() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKwkZuschlag(), MastrUnit_.kwkZuschlag));
            }
            if (criteria.getLageEinheit() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLageEinheit(), MastrUnit_.lageEinheit));
            }
            if (criteria.getLageEinheitBezeichnung() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getLageEinheitBezeichnung(), MastrUnit_.lageEinheitBezeichnung));
            }
            if (criteria.getLeistungsBegrenzung() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getLeistungsBegrenzung(), MastrUnit_.leistungsBegrenzung));
            }
            if (criteria.getNabenhoeheWindenergieanlage() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getNabenhoeheWindenergieanlage(), MastrUnit_.nabenhoeheWindenergieanlage)
                    );
            }
            if (criteria.getRotorDurchmesserWindenergieanlage() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(
                            criteria.getRotorDurchmesserWindenergieanlage(),
                            MastrUnit_.rotorDurchmesserWindenergieanlage
                        )
                    );
            }
            if (criteria.getNettoNennLeistung() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNettoNennLeistung(), MastrUnit_.nettoNennLeistung));
            }
            if (criteria.getNutzbareSpeicherKapazitaet() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getNutzbareSpeicherKapazitaet(), MastrUnit_.nutzbareSpeicherKapazitaet)
                    );
            }
            if (criteria.getNutzungsBereichGebsa() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getNutzungsBereichGebsa(), MastrUnit_.nutzungsBereichGebsa));
            }
            if (criteria.getStandortAnonymisiert() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getStandortAnonymisiert(), MastrUnit_.standortAnonymisiert));
            }
            if (criteria.getSpannungsEbenenId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpannungsEbenenId(), MastrUnit_.spannungsEbenenId));
            }
            if (criteria.getSpannungsEbenenNamen() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getSpannungsEbenenNamen(), MastrUnit_.spannungsEbenenNamen));
            }
            if (criteria.getSpeicherEinheitMastrNummer() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getSpeicherEinheitMastrNummer(), MastrUnit_.speicherEinheitMastrNummer)
                    );
            }
            if (criteria.getTechnologieStromerzeugungId() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getTechnologieStromerzeugungId(), MastrUnit_.technologieStromerzeugungId)
                    );
            }
            if (criteria.getTechnologieStromerzeugung() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getTechnologieStromerzeugung(), MastrUnit_.technologieStromerzeugung)
                    );
            }
            if (criteria.getThermischeNutzLeistung() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getThermischeNutzLeistung(), MastrUnit_.thermischeNutzLeistung));
            }
            if (criteria.getTypenBezeichnung() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTypenBezeichnung(), MastrUnit_.typenBezeichnung));
            }
            if (criteria.getVollTeilEinspeisung() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getVollTeilEinspeisung(), MastrUnit_.vollTeilEinspeisung));
            }
            if (criteria.getVollTeilEinspeisungBezeichnung() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getVollTeilEinspeisungBezeichnung(), MastrUnit_.vollTeilEinspeisungBezeichnung)
                    );
            }
            if (criteria.getWindparkName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getWindparkName(), MastrUnit_.windparkName));
            }
            if (criteria.getAdministrationUnitId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAdministrationUnitId(),
                            root -> root.join(MastrUnit_.administrationUnit, JoinType.LEFT).get(AdministrationUnit_.id)
                        )
                    );
            }
        }
        return specification;
    }

    public StateEnergyStatisitcsDTO getStateStatistics(OperationState state, EnergySource energySource) {
        
        long start = System.currentTimeMillis();
        StateEnergyStatisitcsDTO statisticState = new StateEnergyStatisitcsDTO();
        statisticState.setEnergySource(energySource);
        statisticState.setActiveUnits(
            this.mastrUnitRepository.countByBetriebsStatusIdAndEnergieTraegerId(state.getId(), energySource.getId()));
        statisticState.setActiveUnitsBruttoLeistung(
            this.mastrUnitRepository.sumBruttoLeistungByBetriebsStatusId(state.getId(), energySource.getId()));
        
        statisticState.setActiveUnitsCurrentYear(
            this.mastrUnitRepository.countByBetriebsStatusIdAndEnergieTraegerIdAndInbetriebnahmeDatumAfter(
                state.getId(), energySource.getId(), LocalDate.of(Year.now().getValue(), 1, 1)));
        statisticState.setActiveUnitsLastYear(
            this.mastrUnitRepository.countByBetriebsStatusIdAndEnergieTraegerIdAndInbetriebnahmeDatumBetween(
            state.getId(), energySource.getId(), LocalDate.of(Year.now().getValue() - 1, 1, 1), LocalDate.of(Year.now().getValue(), 1, 1)));
        statisticState.setActiveUnitsSecondLastYear(
            this.mastrUnitRepository.countByBetriebsStatusIdAndEnergieTraegerIdAndInbetriebnahmeDatumBetween(
            state.getId(), energySource.getId(), LocalDate.of(Year.now().getValue() - 2, 1, 1), LocalDate.of(Year.now().getValue() - 1, 1, 1)));
        
        statisticState.setActiveUnitsCurrentYearBruttoLeistung(
            this.mastrUnitRepository.sumBruttoLeistungByBetriebsStatusIdAfterDate(
                state.getId(), energySource.getId(), LocalDate.of(Year.now().getValue(), 1, 1)));
        statisticState.setActiveUnitsLastYearBruttoLeistung(
            this.mastrUnitRepository.sumBruttoLeistungByBetriebsStatusIdBetweenDates(
                state.getId(), energySource.getId(), LocalDate.of(Year.now().getValue() - 1, 1, 1), LocalDate.of(Year.now().getValue(), 1, 1)));
        statisticState.setActiveUnitsSecondLastYearBruttoLeistung(
            this.mastrUnitRepository.sumBruttoLeistungByBetriebsStatusIdBetweenDates(
                state.getId(), energySource.getId(), LocalDate.of(Year.now().getValue() - 2, 1, 1), LocalDate.of(Year.now().getValue() - 1, 1, 1)));

        statisticState.setAverageBruttoLeistungPerWeekCurrentYear(statisticState.getActiveUnitsCurrentYearBruttoLeistung() / LocalDate.now().get(ChronoField.ALIGNED_WEEK_OF_YEAR));
        statisticState.setAverageBruttoLeistungPerWeekLastYear(statisticState.getActiveUnitsLastYearBruttoLeistung() / 52);
        statisticState.setAverageBruttoLeistungPerWeekSecondLastYear(statisticState.getActiveUnitsSecondLastYearBruttoLeistung() / 52);

        if ( EnergySource.STORAGE.equals(energySource) ) {

            statisticState.setAverageCapacityPerWeekCurrentYear(
                this.mastrUnitRepository.sumNutzbareSpeicherkapazitaetByBetriebsStatusIdAfterDate(
                    state.getId(), energySource.getId(), LocalDate.of(Year.now().getValue(), 1, 1)) / LocalDate.now().get(ChronoField.ALIGNED_WEEK_OF_YEAR));
            statisticState.setAverageCapacityPerWeekLastYear(
                this.mastrUnitRepository.sumNutzbareSpeicherkapazitaetByBetriebsStatusIdBetweenDates(
                    state.getId(), energySource.getId(), LocalDate.of(Year.now().getValue() - 1, 1, 1), LocalDate.of(Year.now().getValue(), 1, 1)) / 52);
            statisticState.setAverageCapacityPerWeekSecondLastYear(
                this.mastrUnitRepository.sumNutzbareSpeicherkapazitaetByBetriebsStatusIdBetweenDates(
                    state.getId(), energySource.getId(), LocalDate.of(Year.now().getValue() - 2, 1, 1), LocalDate.of(Year.now().getValue() - 1, 1, 1)) / 52);
        }

        log.info("It took {}ms to query the state statistics for energy: '{}' and state: '{}'", System.currentTimeMillis() - start, energySource.name(), state.name());

        return statisticState;
    }
}
