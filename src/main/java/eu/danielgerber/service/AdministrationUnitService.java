package eu.danielgerber.service;

import eu.danielgerber.domain.AdministrationUnit;
import eu.danielgerber.repository.AdministrationUnitRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link AdministrationUnit}.
 */
@Service
@Transactional
public class AdministrationUnitService {

    private final Logger log = LoggerFactory.getLogger(AdministrationUnitService.class);

    private final AdministrationUnitRepository administrationUnitRepository;

    public AdministrationUnitService(AdministrationUnitRepository administrationUnitRepository) {
        this.administrationUnitRepository = administrationUnitRepository;
    }

    /**
     * Save a administrationUnit.
     *
     * @param administrationUnit the entity to save.
     * @return the persisted entity.
     */
    public AdministrationUnit save(AdministrationUnit administrationUnit) {
        log.debug("Request to save AdministrationUnit : {}", administrationUnit);
        return administrationUnitRepository.save(administrationUnit);
    }

    /**
     * Update a administrationUnit.
     *
     * @param administrationUnit the entity to save.
     * @return the persisted entity.
     */
    public AdministrationUnit update(AdministrationUnit administrationUnit) {
        log.debug("Request to update AdministrationUnit : {}", administrationUnit);
        return administrationUnitRepository.save(administrationUnit);
    }

    /**
     * Partially update a administrationUnit.
     *
     * @param administrationUnit the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<AdministrationUnit> partialUpdate(AdministrationUnit administrationUnit) {
        log.debug("Request to partially update AdministrationUnit : {}", administrationUnit);

        return administrationUnitRepository
            .findById(administrationUnit.getId())
            .map(existingAdministrationUnit -> {
                if (administrationUnit.getObjektidentifikator() != null) {
                    existingAdministrationUnit.setObjektidentifikator(administrationUnit.getObjektidentifikator());
                }
                if (administrationUnit.getBeginnlebenszeit() != null) {
                    existingAdministrationUnit.setBeginnlebenszeit(administrationUnit.getBeginnlebenszeit());
                }
                if (administrationUnit.getAdminEbeneAde() != null) {
                    existingAdministrationUnit.setAdminEbeneAde(administrationUnit.getAdminEbeneAde());
                }
                if (administrationUnit.getAde() != null) {
                    existingAdministrationUnit.setAde(administrationUnit.getAde());
                }
                if (administrationUnit.getGeofaktorGf() != null) {
                    existingAdministrationUnit.setGeofaktorGf(administrationUnit.getGeofaktorGf());
                }
                if (administrationUnit.getGf() != null) {
                    existingAdministrationUnit.setGf(administrationUnit.getGf());
                }
                if (administrationUnit.getBesondereGebieteBsg() != null) {
                    existingAdministrationUnit.setBesondereGebieteBsg(administrationUnit.getBesondereGebieteBsg());
                }
                if (administrationUnit.getBsg() != null) {
                    existingAdministrationUnit.setBsg(administrationUnit.getBsg());
                }
                if (administrationUnit.getRegionalschluesselArs() != null) {
                    existingAdministrationUnit.setRegionalschluesselArs(administrationUnit.getRegionalschluesselArs());
                }
                if (administrationUnit.getGemeindeschluesselAgs() != null) {
                    existingAdministrationUnit.setGemeindeschluesselAgs(administrationUnit.getGemeindeschluesselAgs());
                }
                if (administrationUnit.getVerwaltungssitzSdvArs() != null) {
                    existingAdministrationUnit.setVerwaltungssitzSdvArs(administrationUnit.getVerwaltungssitzSdvArs());
                }
                if (administrationUnit.getGeografischernameGen() != null) {
                    existingAdministrationUnit.setGeografischernameGen(administrationUnit.getGeografischernameGen());
                }
                if (administrationUnit.getBezeichnung() != null) {
                    existingAdministrationUnit.setBezeichnung(administrationUnit.getBezeichnung());
                }
                if (administrationUnit.getIdentifikatorIbz() != null) {
                    existingAdministrationUnit.setIdentifikatorIbz(administrationUnit.getIdentifikatorIbz());
                }
                if (administrationUnit.getBemerkung() != null) {
                    existingAdministrationUnit.setBemerkung(administrationUnit.getBemerkung());
                }
                if (administrationUnit.getNamensbildungNbd() != null) {
                    existingAdministrationUnit.setNamensbildungNbd(administrationUnit.getNamensbildungNbd());
                }
                if (administrationUnit.getNbd() != null) {
                    existingAdministrationUnit.setNbd(administrationUnit.getNbd());
                }
                if (administrationUnit.getLand() != null) {
                    existingAdministrationUnit.setLand(administrationUnit.getLand());
                }
                if (administrationUnit.getRegierungsbezirk() != null) {
                    existingAdministrationUnit.setRegierungsbezirk(administrationUnit.getRegierungsbezirk());
                }
                if (administrationUnit.getKreis() != null) {
                    existingAdministrationUnit.setKreis(administrationUnit.getKreis());
                }
                if (administrationUnit.getVerwaltungsgemeinschaftteil1() != null) {
                    existingAdministrationUnit.setVerwaltungsgemeinschaftteil1(administrationUnit.getVerwaltungsgemeinschaftteil1());
                }
                if (administrationUnit.getVerwaltungsgemeinschaftteil2() != null) {
                    existingAdministrationUnit.setVerwaltungsgemeinschaftteil2(administrationUnit.getVerwaltungsgemeinschaftteil2());
                }
                if (administrationUnit.getGemeinde() != null) {
                    existingAdministrationUnit.setGemeinde(administrationUnit.getGemeinde());
                }
                if (administrationUnit.getFunkSchluesselstelle3() != null) {
                    existingAdministrationUnit.setFunkSchluesselstelle3(administrationUnit.getFunkSchluesselstelle3());
                }
                if (administrationUnit.getFkS3() != null) {
                    existingAdministrationUnit.setFkS3(administrationUnit.getFkS3());
                }
                if (administrationUnit.getEuropStatistikschluesselNuts() != null) {
                    existingAdministrationUnit.setEuropStatistikschluesselNuts(administrationUnit.getEuropStatistikschluesselNuts());
                }
                if (administrationUnit.getRegioschluesselaufgefuellt() != null) {
                    existingAdministrationUnit.setRegioschluesselaufgefuellt(administrationUnit.getRegioschluesselaufgefuellt());
                }
                if (administrationUnit.getGemeindeschluesselaufgefuellt() != null) {
                    existingAdministrationUnit.setGemeindeschluesselaufgefuellt(administrationUnit.getGemeindeschluesselaufgefuellt());
                }
                if (administrationUnit.getWirksamkeitWsk() != null) {
                    existingAdministrationUnit.setWirksamkeitWsk(administrationUnit.getWirksamkeitWsk());
                }
                if (administrationUnit.getGeometry() != null) {
                    existingAdministrationUnit.setGeometry(administrationUnit.getGeometry());
                }

                return existingAdministrationUnit;
            })
            .map(administrationUnitRepository::save);
    }

    /**
     * Get all the administrationUnits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<AdministrationUnit> findAll(Pageable pageable) {
        log.debug("Request to get all AdministrationUnits");
        return administrationUnitRepository.findAll(pageable);
    }

    /**
     * Get one administrationUnit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AdministrationUnit> findOne(Long id) {
        log.debug("Request to get AdministrationUnit : {}", id);
        return administrationUnitRepository.findById(id);
    }

    /**
     * Delete the administrationUnit by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AdministrationUnit : {}", id);
        administrationUnitRepository.deleteById(id);
    }
}
