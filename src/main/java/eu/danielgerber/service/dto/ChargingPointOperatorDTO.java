package eu.danielgerber.service.dto;

public class ChargingPointOperatorDTO {
  private String operator;
  private Double power;
  private Integer points;
  private Double average;

  public ChargingPointOperatorDTO(String operator, Double power, Integer points, Double average){
    this.operator = operator;
    this.power = power;
    this.points = points;
    this.average = average;
  }

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public Double getPower() {
    return power;
  }

  public void setPower(Double power) {
    this.power = power;
  }

  public Integer getPoints() {
    return points;
  }

  public void setPoints(Integer points) {
    this.points = points;
  }

  public Double getAverage() {
    return average;
  }

  public void setAverage(Double average) {
    this.average = average;
  }

  
}
