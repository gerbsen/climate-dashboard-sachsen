package eu.danielgerber.service.dto;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import eu.danielgerber.domain.enums.EnergySource;

@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class StateEnergyStatisitcsDTO {

  private EnergySource energySource;
  private Long activeUnits;
  private Long activeUnitsCurrentYear;
  private Long activeUnitsLastYear;
  private Long activeUnitsSecondLastYear;
  private Double activeUnitsBruttoLeistung;
  private Double activeUnitsCurrentYearBruttoLeistung;
  private Double activeUnitsLastYearBruttoLeistung;
  private Double activeUnitsSecondLastYearBruttoLeistung;
  private Double averageBruttoLeistungPerWeekCurrentYear;
  private Double averageBruttoLeistungPerWeekLastYear;
  private Double averageBruttoLeistungPerWeekSecondLastYear;
  private Double averageCapacityPerWeekCurrentYear;
  private Double averageCapacityPerWeekLastYear;
  private Double averageCapacityPerWeekSecondLastYear;
  
  public Long getActiveUnits() {
    return activeUnits;
  }
  public void setActiveUnits(Long count) {
    this.activeUnits = count;
  }
  public Double getActiveUnitsBruttoLeistung() {
    return activeUnitsBruttoLeistung;
  }
  public void setActiveUnitsBruttoLeistung(Double sumBruttoLeistung) {
    this.activeUnitsBruttoLeistung = sumBruttoLeistung;
  }
  public Long getActiveUnitsCurrentYear() {
    return activeUnitsCurrentYear;
  }
  public void setActiveUnitsCurrentYear(Long activeUnitsCurrentYear) {
    this.activeUnitsCurrentYear = activeUnitsCurrentYear;
  }
  public Long getActiveUnitsLastYear() {
    return activeUnitsLastYear;
  }
  public void setActiveUnitsLastYear(Long activeUnitsLastYear) {
    this.activeUnitsLastYear = activeUnitsLastYear;
  }
  public Long getActiveUnitsSecondLastYear() {
    return activeUnitsSecondLastYear;
  }
  public void setActiveUnitsSecondLastYear(Long activeUnitsSecondLastYear) {
    this.activeUnitsSecondLastYear = activeUnitsSecondLastYear;
  }
  public Double getActiveUnitsCurrentYearBruttoLeistung() {
    return activeUnitsCurrentYearBruttoLeistung;
  }
  public void setActiveUnitsCurrentYearBruttoLeistung(Double activeUnitsCurrentYearBruttoLeistung) {
    this.activeUnitsCurrentYearBruttoLeistung = activeUnitsCurrentYearBruttoLeistung == null ? 0 : activeUnitsCurrentYearBruttoLeistung;
  }
  public Double getActiveUnitsLastYearBruttoLeistung() {
    return activeUnitsLastYearBruttoLeistung;
  }
  public void setActiveUnitsLastYearBruttoLeistung(Double activeUnitsLastYearBruttoLeistung) {
    this.activeUnitsLastYearBruttoLeistung = activeUnitsLastYearBruttoLeistung == null ? 0 : activeUnitsLastYearBruttoLeistung;
  }
  public Double getActiveUnitsSecondLastYearBruttoLeistung() {
    return activeUnitsSecondLastYearBruttoLeistung;
  }
  public void setActiveUnitsSecondLastYearBruttoLeistung(Double activeUnitsSecondYearBruttoLeistung) {
    this.activeUnitsSecondLastYearBruttoLeistung = activeUnitsSecondYearBruttoLeistung == null ? 0 : activeUnitsSecondYearBruttoLeistung;
  }
  public Double getAverageBruttoLeistungPerWeekCurrentYear() {
    return averageBruttoLeistungPerWeekCurrentYear;
  }
  public void setAverageBruttoLeistungPerWeekCurrentYear(Double averageBruttoLeistungPerWeekCurrentYear) {
    this.averageBruttoLeistungPerWeekCurrentYear = averageBruttoLeistungPerWeekCurrentYear;
  }
  public Double getAverageBruttoLeistungPerWeekLastYear() {
    return averageBruttoLeistungPerWeekLastYear;
  }
  public void setAverageBruttoLeistungPerWeekLastYear(Double averageBruttoLeistungPerWeekLastYear) {
    this.averageBruttoLeistungPerWeekLastYear = averageBruttoLeistungPerWeekLastYear;
  }
  public Double getAverageBruttoLeistungPerWeekSecondLastYear() {
    return averageBruttoLeistungPerWeekSecondLastYear;
  }
  public void setAverageBruttoLeistungPerWeekSecondLastYear(Double averageBruttoLeistungPerWeekSecondLastYear) {
    this.averageBruttoLeistungPerWeekSecondLastYear = averageBruttoLeistungPerWeekSecondLastYear;
  }
  public EnergySource getEnergySource() {
    return energySource;
  }
  public void setEnergySource(EnergySource energySource) {
    this.energySource = energySource;
  }
  public Double getAverageCapacityPerWeekCurrentYear() {
    return averageCapacityPerWeekCurrentYear;
  }
  public void setAverageCapacityPerWeekCurrentYear(Double averageCapacityPerWeekCurrentYear) {
    this.averageCapacityPerWeekCurrentYear = averageCapacityPerWeekCurrentYear;
  }
  public Double getAverageCapacityPerWeekLastYear() {
    return averageCapacityPerWeekLastYear;
  }
  public void setAverageCapacityPerWeekLastYear(Double averageCapacityPerWeekLastYear) {
    this.averageCapacityPerWeekLastYear = averageCapacityPerWeekLastYear;
  }
  public Double getAverageCapacityPerWeekSecondLastYear() {
    return averageCapacityPerWeekSecondLastYear;
  }
  public void setAverageCapacityPerWeekSecondLastYear(Double averageCapacityPerWeekSecondLastYear) {
    this.averageCapacityPerWeekSecondLastYear = averageCapacityPerWeekSecondLastYear;
  }
}
