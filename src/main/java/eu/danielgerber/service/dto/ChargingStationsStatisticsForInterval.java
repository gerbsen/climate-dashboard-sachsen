package eu.danielgerber.service.dto;

public class ChargingStationsStatisticsForInterval {

  private String timeUnit;
  private Integer timeUnitChargingPoints;
  private Double timeUnitPower;
  private Integer totalChargingPoints;
  private Double totalPower;

  public ChargingStationsStatisticsForInterval(String timeUnit, Double timeUnitPower, Integer timeUnitChargingPoints) {
    this.timeUnit = timeUnit;
    this.timeUnitChargingPoints = timeUnitChargingPoints;
    this.timeUnitPower = timeUnitPower;
  }
  
  public String getTimeUnit() {
    return timeUnit;
  }
  public void setTimeUnit(String timeUnit) {
    this.timeUnit = timeUnit;
  }
  public Integer getTimeUnitChargingPoints() {
    return timeUnitChargingPoints;
  }
  public void setTimeUnitChargingPoints(Integer timeUnitChargingPoints) {
    this.timeUnitChargingPoints = timeUnitChargingPoints;
  }
  public Double getTimeUnitPower() {
    return timeUnitPower;
  }
  public void setTimeUnitPower(Double timeUnitPower) {
    this.timeUnitPower = timeUnitPower;
  }
  public Double getTotalPower() {
    return totalPower;
  }
  public void setTotalPower(Double totalPower) {
    this.totalPower = totalPower;
  }
  public Integer getTotalChargingPoints() {
    return totalChargingPoints;
  }
  public void setTotalChargingPoints(Integer totalChargingPoints) {
    this.totalChargingPoints = totalChargingPoints;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((timeUnit == null) ? 0 : timeUnit.hashCode());
    result = prime * result + ((timeUnitChargingPoints == null) ? 0 : timeUnitChargingPoints.hashCode());
    result = prime * result + ((timeUnitPower == null) ? 0 : timeUnitPower.hashCode());
    result = prime * result + ((totalPower == null) ? 0 : totalPower.hashCode());
    result = prime * result + ((totalChargingPoints == null) ? 0 : totalChargingPoints.hashCode());
    return result;
  }
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ChargingStationsStatisticsForInterval other = (ChargingStationsStatisticsForInterval) obj;
    if (timeUnit == null) {
      if (other.timeUnit != null)
        return false;
    } else if (!timeUnit.equals(other.timeUnit))
      return false;
    if (timeUnitChargingPoints == null) {
      if (other.timeUnitChargingPoints != null)
        return false;
    } else if (!timeUnitChargingPoints.equals(other.timeUnitChargingPoints))
      return false;
    if (timeUnitPower == null) {
      if (other.timeUnitPower != null)
        return false;
    } else if (!timeUnitPower.equals(other.timeUnitPower))
      return false;
    if (totalPower == null) {
      if (other.totalPower != null)
        return false;
    } else if (!totalPower.equals(other.totalPower))
      return false;
    if (totalChargingPoints == null) {
      if (other.totalChargingPoints != null)
        return false;
    } else if (!totalChargingPoints.equals(other.totalChargingPoints))
      return false;
    return true;
  }
  @Override
  public String toString() {
    return "ChargingStationsStatisticsForIntervalDTO [timeUnit=" + timeUnit + ", timeUnitChargingPoints="
        + timeUnitChargingPoints + ", timeUnitPower=" + timeUnitPower + ", totalPower=" + totalPower
        + ", totalChargingPoints=" + totalChargingPoints + "]";
  }
}
