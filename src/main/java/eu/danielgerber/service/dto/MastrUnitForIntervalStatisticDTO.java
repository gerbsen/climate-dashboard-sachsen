package eu.danielgerber.service.dto;

import eu.danielgerber.domain.MastrUnitForIntervalStatisticElement;
import eu.danielgerber.domain.enums.AggregationFunction;
import eu.danielgerber.domain.enums.EnergySource;
import eu.danielgerber.domain.enums.IntervalFormat;
import eu.danielgerber.domain.enums.MastrUnitProperty;

import java.util.ArrayList;
import java.util.List;

public class MastrUnitForIntervalStatisticDTO {
    private List<MastrUnitForIntervalStatisticElement> results = new ArrayList<>();
    private String ags;
    private AggregationFunction aggregateFunction;
    private IntervalFormat intervalFormat;
    private MastrUnitProperty propertyName;
    private EnergySource energySource;
    private Long millis;

    public Long getMillis() {
        return millis;
    }

    public void setMillis(Long millis) {
        this.millis = millis;
    }

    public void setResults(List<MastrUnitForIntervalStatisticElement> resultList) {
        this.results = resultList;
    }

    public void setAgs(String ags) {
        this.ags = ags;
    }

    public void setAggregateFunction(AggregationFunction aggregateFunction) {
        this.aggregateFunction = aggregateFunction;
    }

    public void setIntervalFormat(IntervalFormat intervalFormat) {
        this.intervalFormat = intervalFormat;
    }

    public void setPropertyName(MastrUnitProperty propertyName) {
        this.propertyName = propertyName;
    }

    public void setEnergySource(EnergySource energySource) {
        this.energySource = energySource;
    }

    public List<MastrUnitForIntervalStatisticElement> getResults() {
        return results;
    }

    public String getAgs() {
        return ags;
    }

    public AggregationFunction getAggregateFunction() {
        return aggregateFunction;
    }

    public IntervalFormat getIntervalFormat() {
        return intervalFormat;
    }

    public MastrUnitProperty getPropertyName() {
        return propertyName;
    }

    public EnergySource getEnergySource() {
        return energySource;
    }

    public void addResult(MastrUnitForIntervalStatisticElement mastrUnitForIntervalStatisticElement) {
      this.results.add(mastrUnitForIntervalStatisticElement);
    }
}
