package eu.danielgerber.service.dto;

import java.util.ArrayList;
import java.util.List;

public class ChargingPointOperatorsDTO {

  private Long millis;
  private List<ChargingPointOperatorDTO> operators;
  
  public ChargingPointOperatorsDTO(){
    this.operators = new ArrayList<>();
  }

  public Long getMillis() {
    return millis;
  }
  public void setMillis(Long millis) {
    this.millis = millis;
  }
  public List<ChargingPointOperatorDTO> getOperators() {
    return operators;
  }
  public void setOperators(List<ChargingPointOperatorDTO> operators) {
    this.operators = operators;
  }
  public void addOperators(ChargingPointOperatorDTO operator) {
    this.operators.add(operator);
  }
}
