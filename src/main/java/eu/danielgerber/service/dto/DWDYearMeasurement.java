package eu.danielgerber.service.dto;

public class DWDYearMeasurement {

  private Integer year = null;
  private Double january = null;
  private Double february = null;
  private Double march = null;
  private Double april = null;
  private Double may = null;
  private Double june = null;
  private Double july = null;
  private Double august = null;
  private Double september = null;
  private Double october = null;
  private Double november = null;
  private Double december = null;
  
  public Integer getYear() {
    return year;
  }
  public void setYear(Integer year) {
    this.year = year;
  }
  public Double getJanuary() {
    return january;
  }
  public void setJanuary(Double january) {
    this.january = january;
  }
  public Double getFebruary() {
    return february;
  }
  public void setFebruary(Double february) {
    this.february = february;
  }
  public Double getMarch() {
    return march;
  }
  public void setMarch(Double march) {
    this.march = march;
  }
  public Double getApril() {
    return april;
  }
  public void setApril(Double april) {
    this.april = april;
  }
  public Double getMay() {
    return may;
  }
  public void setMay(Double may) {
    this.may = may;
  }
  public Double getJune() {
    return june;
  }
  public void setJune(Double june) {
    this.june = june;
  }
  public Double getJuly() {
    return july;
  }
  public void setJuly(Double july) {
    this.july = july;
  }
  public Double getAugust() {
    return august;
  }
  public void setAugust(Double august) {
    this.august = august;
  }
  public Double getSeptember() {
    return september;
  }
  public void setSeptember(Double september) {
    this.september = september;
  }
  public Double getOctober() {
    return october;
  }
  public void setOctober(Double october) {
    this.october = october;
  }
  public Double getNovember() {
    return november;
  }
  public void setNovember(Double november) {
    this.november = november;
  }
  public Double getDecember() {
    return december;
  }
  public void setDecember(Double december) {
    this.december = december;
  }

  
}
