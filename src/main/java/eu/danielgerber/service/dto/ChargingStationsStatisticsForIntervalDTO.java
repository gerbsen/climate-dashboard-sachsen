package eu.danielgerber.service.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.danielgerber.domain.enums.IntervalFormat;

public class ChargingStationsStatisticsForIntervalDTO {

  private Map<IntervalFormat, List<ChargingStationsStatisticsForInterval>> intervalStatistics;
  private Long millis;

  public ChargingStationsStatisticsForIntervalDTO() {
    this.millis = System.currentTimeMillis();
    this.intervalStatistics = new HashMap<>();
  }

  public Map<IntervalFormat, List<ChargingStationsStatisticsForInterval>> getIntervalStatistics() {
    return intervalStatistics;
  }

  public void addIntervalStatistic(IntervalFormat format, ChargingStationsStatisticsForInterval statistic){
    if ( this.intervalStatistics.get(format) == null ) this.intervalStatistics.put(format, new ArrayList<>());
    this.intervalStatistics.get(format).add(statistic);
  }

  public void addIntervalStatistics(IntervalFormat format, List<ChargingStationsStatisticsForInterval> statistic){
    this.intervalStatistics.put(format, statistic);
  }

  public void setIntervalStatistics(Map<IntervalFormat, List<ChargingStationsStatisticsForInterval>> intervalStatistics) {
    this.intervalStatistics = intervalStatistics;
  }

  public Long getMillis() {
    return millis;
  }

  public List<ChargingStationsStatisticsForInterval> getIntervalStatistics(IntervalFormat intervalFormat) {
    return this.intervalStatistics.get(intervalFormat);
  }

  public ChargingStationsStatisticsForIntervalDTO finish() {
    this.millis = System.currentTimeMillis() - this.millis;
    return this;
  }
}
