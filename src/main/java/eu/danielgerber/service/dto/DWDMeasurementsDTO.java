package eu.danielgerber.service.dto;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import eu.danielgerber.domain.enums.DWDState;
import eu.danielgerber.util.MovingAverage;

@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class DWDMeasurementsDTO {

  private Long millis;
  private DWDState state;
  private List<BigInteger> years = new ArrayList<>();
  private List<Double> airTemperatureMean = new ArrayList<>();
  private List<Double> tropicalNightsTminGE20 = new ArrayList<>();
  private List<Double> sunshineDuration = new ArrayList<>();
  private List<Double> hotDays = new ArrayList<>();
  private List<Double> summerDays = new ArrayList<>();
  private List<Double> precipE20mmDays = new ArrayList<>();
  private List<Double> precipE10mmDays = new ArrayList<>();
  private List<Double> frostDays = new ArrayList<>();
  private List<Double> iceDays = new ArrayList<>();
  private Map<Integer, DWDYearMeasurement> regionalAverageAirTemperatureMean = new HashMap<>();
  private Map<Integer, DWDYearMeasurement> regionalAveragePercipitation = new HashMap<>();
  private Map<Integer, DWDYearMeasurement> regionalAverageSunshineDuration = new HashMap<>();

  public DWDYearMeasurement getDWDYearMeasurement(String statistic, Integer year) {

    if ( statistic == "air_temperature_mean") {
      if ( this.regionalAverageAirTemperatureMean.get(year) == null ) {
        this.regionalAverageAirTemperatureMean.put(year, new DWDYearMeasurement());
      }
      return this.regionalAverageAirTemperatureMean.get(year);
    }
    else if ( statistic == "precipitation" ) {
      if ( this.regionalAveragePercipitation.get(year) == null ) {
        this.regionalAveragePercipitation.put(year, new DWDYearMeasurement());
      }
      return this.regionalAveragePercipitation.get(year);
    }
    else if ( statistic == "sunshine_duration" ) {
      if ( this.regionalAverageSunshineDuration.get(year) == null ) {
        this.regionalAverageSunshineDuration.put(year, new DWDYearMeasurement());
      }
      return this.regionalAverageSunshineDuration.get(year);
    }

    throw new RuntimeException("unknown statistic: " + statistic);
  }

  public DWDYearMeasurement getRegionalAverageAirTemperatureMean(Integer year) {
    if ( this.regionalAverageAirTemperatureMean.get(year) == null ) {
      this.regionalAverageAirTemperatureMean.put(year, new DWDYearMeasurement());
    }
    return this.regionalAverageAirTemperatureMean.get(year);
  }

  public List<Double> getIceDays() {
    return iceDays;
  }

  public void setIceDays(List<Double> iceDays) {
    this.iceDays = iceDays;
  }

  public Long getMillis() {
    return millis;
  }

  public void setMillis(Long millis) {
    this.millis = millis;
  }

  public List<BigInteger> getYears() {
    return years;
  }

  public void setYears(List<BigInteger> years) {
    this.years = years;
  }

  public List<Double> getAirTemperatureMean() {
    return airTemperatureMean;
  }

  public void setAirTemperatureMean(List<Double> airTemperatureMean) {
    this.airTemperatureMean = airTemperatureMean;
  }

  public List<Double> getTropicalNightsTminGE20() {
    return tropicalNightsTminGE20;
  }

  public void setTropicalNightsTminGE20(List<Double> tropicalNightsTminGE20) {
    this.tropicalNightsTminGE20 = tropicalNightsTminGE20;
  }

  public List<Double> getSunshineDuration() {
    return sunshineDuration;
  }

  public void setSunshineDuration(List<Double> sunshineDuration) {
    this.sunshineDuration = sunshineDuration;
  }

  public List<Double> getHotDays() {
    return hotDays;
  }

  public void setHotDays(List<Double> hotDays) {
    this.hotDays = hotDays;
  }

  public List<Double> getSummerDays() {
    return summerDays;
  }

  public void setSummerDays(List<Double> summerDays) {
    this.summerDays = summerDays;
  }

  public List<Double> getPrecipE20mmDays() {
    return precipE20mmDays;
  }

  public void setPrecipE20mmDays(List<Double> precipE20mmDays) {
    this.precipE20mmDays = precipE20mmDays;
  }

  public List<Double> getPrecipE10mmDays() {
    return precipE10mmDays;
  }

  public void setPrecipE10mmDays(List<Double> precipE10mmDays) {
    this.precipE10mmDays = precipE10mmDays;
  }

  public List<Double> getFrostDays() {
    return frostDays;
  }

  public void setFrostDays(List<Double> frostDays) {
    this.frostDays = frostDays;
  }

  public DWDState getState() {
    return state;
  }

  public void setState(DWDState state) {
    this.state = state;
  }

  public Map<Integer, DWDYearMeasurement> getRegionalAveragePercipitation() {
    return regionalAveragePercipitation;
  }

  public void setRegionalAveragePercipitation(Map<Integer, DWDYearMeasurement> regionalAveragePercipitation) {
    this.regionalAveragePercipitation = regionalAveragePercipitation;
  }

  public Map<Integer, DWDYearMeasurement> getRegionalAverageSunshineDuration() {
    return regionalAverageSunshineDuration;
  }

  public void setRegionalAverageSunshineDuration(Map<Integer, DWDYearMeasurement> regionalAverageSunshineDuration) {
    this.regionalAverageSunshineDuration = regionalAverageSunshineDuration;
  }

  public Map<Integer, DWDYearMeasurement> getRegionalAverageAirTemperatureMean() {
    return regionalAverageAirTemperatureMean;
  }

  public void setRegionalAverageAirTemperatureMean(Map<Integer, DWDYearMeasurement> regionalAverageAirTemperatureMean) {
    this.regionalAverageAirTemperatureMean = regionalAverageAirTemperatureMean;
  }

  public List<Double> getAirTemperatureMeanRolling30() {
    return MovingAverage.createMovingAverage(this.airTemperatureMean, 30);
  }

  public List<Double> getTropicalNightsTminGE20Rolling30() {
    return MovingAverage.createMovingAverage(this.tropicalNightsTminGE20, 30);
  }

  public List<Double> getSunshineDurationRolling30() {
    return MovingAverage.createMovingAverage(this.sunshineDuration, 30);
  }

  public List<Double> getHotDaysRolling30() {
    return MovingAverage.createMovingAverage(this.hotDays, 30);
  }

  public List<Double> getSummerDaysRolling30() {
    return MovingAverage.createMovingAverage(this.summerDays, 30);
  }

  public List<Double> getPrecipE20mmDaysRolling30() {
    return MovingAverage.createMovingAverage(this.precipE20mmDays, 30);
  }

  public List<Double> getPrecipE10mmDaysRolling30() {
    return MovingAverage.createMovingAverage(this.precipE10mmDays, 30);
  }

  public List<Double> getFrostDaysRolling30() {
    return MovingAverage.createMovingAverage(this.frostDays, 30);
  }

  public List<Double> getIceDaysRolling30() {
    return MovingAverage.createMovingAverage(this.iceDays, 30);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((millis == null) ? 0 : millis.hashCode());
    result = prime * result + ((state == null) ? 0 : state.hashCode());
    result = prime * result + ((years == null) ? 0 : years.hashCode());
    result = prime * result + ((airTemperatureMean == null) ? 0 : airTemperatureMean.hashCode());
    result = prime * result + ((tropicalNightsTminGE20 == null) ? 0 : tropicalNightsTminGE20.hashCode());
    result = prime * result + ((sunshineDuration == null) ? 0 : sunshineDuration.hashCode());
    result = prime * result + ((hotDays == null) ? 0 : hotDays.hashCode());
    result = prime * result + ((summerDays == null) ? 0 : summerDays.hashCode());
    result = prime * result + ((precipE20mmDays == null) ? 0 : precipE20mmDays.hashCode());
    result = prime * result + ((precipE10mmDays == null) ? 0 : precipE10mmDays.hashCode());
    result = prime * result + ((frostDays == null) ? 0 : frostDays.hashCode());
    result = prime * result + ((iceDays == null) ? 0 : iceDays.hashCode());
    result = prime * result
        + ((regionalAverageAirTemperatureMean == null) ? 0 : regionalAverageAirTemperatureMean.hashCode());
    result = prime * result + ((regionalAveragePercipitation == null) ? 0 : regionalAveragePercipitation.hashCode());
    result = prime * result
        + ((regionalAverageSunshineDuration == null) ? 0 : regionalAverageSunshineDuration.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    DWDMeasurementsDTO other = (DWDMeasurementsDTO) obj;
    if (millis == null) {
      if (other.millis != null)
        return false;
    } else if (!millis.equals(other.millis))
      return false;
    if (state != other.state)
      return false;
    if (years == null) {
      if (other.years != null)
        return false;
    } else if (!years.equals(other.years))
      return false;
    if (airTemperatureMean == null) {
      if (other.airTemperatureMean != null)
        return false;
    } else if (!airTemperatureMean.equals(other.airTemperatureMean))
      return false;
    if (tropicalNightsTminGE20 == null) {
      if (other.tropicalNightsTminGE20 != null)
        return false;
    } else if (!tropicalNightsTminGE20.equals(other.tropicalNightsTminGE20))
      return false;
    if (sunshineDuration == null) {
      if (other.sunshineDuration != null)
        return false;
    } else if (!sunshineDuration.equals(other.sunshineDuration))
      return false;
    if (hotDays == null) {
      if (other.hotDays != null)
        return false;
    } else if (!hotDays.equals(other.hotDays))
      return false;
    if (summerDays == null) {
      if (other.summerDays != null)
        return false;
    } else if (!summerDays.equals(other.summerDays))
      return false;
    if (precipE20mmDays == null) {
      if (other.precipE20mmDays != null)
        return false;
    } else if (!precipE20mmDays.equals(other.precipE20mmDays))
      return false;
    if (precipE10mmDays == null) {
      if (other.precipE10mmDays != null)
        return false;
    } else if (!precipE10mmDays.equals(other.precipE10mmDays))
      return false;
    if (frostDays == null) {
      if (other.frostDays != null)
        return false;
    } else if (!frostDays.equals(other.frostDays))
      return false;
    if (iceDays == null) {
      if (other.iceDays != null)
        return false;
    } else if (!iceDays.equals(other.iceDays))
      return false;
    if (regionalAverageAirTemperatureMean == null) {
      if (other.regionalAverageAirTemperatureMean != null)
        return false;
    } else if (!regionalAverageAirTemperatureMean.equals(other.regionalAverageAirTemperatureMean))
      return false;
    if (regionalAveragePercipitation == null) {
      if (other.regionalAveragePercipitation != null)
        return false;
    } else if (!regionalAveragePercipitation.equals(other.regionalAveragePercipitation))
      return false;
    if (regionalAverageSunshineDuration == null) {
      if (other.regionalAverageSunshineDuration != null)
        return false;
    } else if (!regionalAverageSunshineDuration.equals(other.regionalAverageSunshineDuration))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "DWDMeasurementsDTO [millis=" + millis + ", state=" + state + ", years=" + years + ", airTemperatureMean="
        + airTemperatureMean + ", tropicalNightsTminGE20=" + tropicalNightsTminGE20 + ", sunshineDuration="
        + sunshineDuration + ", hotDays=" + hotDays + ", summerDays=" + summerDays + ", precipE20mmDays="
        + precipE20mmDays + ", precipE10mmDays=" + precipE10mmDays + ", frostDays=" + frostDays + ", iceDays=" + iceDays
        + ", regionalAverageAirTemperatureMean=" + regionalAverageAirTemperatureMean + ", regionalAveragePercipitation="
        + regionalAveragePercipitation + ", regionalAverageSunshineDuration=" + regionalAverageSunshineDuration + "]";
  }
}
