package eu.danielgerber.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link eu.danielgerber.domain.ChargingStation} entity. This class is used
 * in {@link eu.danielgerber.web.rest.ChargingStationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /charging-stations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ChargingStationCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter betreiber;

    private StringFilter strasse;

    private StringFilter hausnummer;

    private StringFilter adresszusatz;

    private StringFilter postleitzahl;

    private StringFilter ort;

    private StringFilter bundesland;

    private StringFilter kreisKreisfreieStadt;

    private LocalDateFilter inbetriebnahmeDatum;

    private DoubleFilter nennleistungLadeeinrichtung;

    private StringFilter artDerLadeeinrichtung;

    private IntegerFilter anzahlLadepunkte;

    private StringFilter ersterLadepunktSteckertypen;

    private DoubleFilter ersterLadepunktLeistung;

    private StringFilter zweiterLadepunktSteckertypen;

    private DoubleFilter zweiterLadepunktLeistung;

    private StringFilter dritterLadepunktSteckertypen;

    private DoubleFilter dritterLadepunktLeistung;

    private StringFilter vierterLadepunktSteckertypen;

    private DoubleFilter vierterLadepunktLeistung;

    private StringFilter geometry;

    private LongFilter administrationUnitId;

    private LongFilter statisticId;

    private Boolean distinct;

    public ChargingStationCriteria() {}

    public ChargingStationCriteria(ChargingStationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.betreiber = other.betreiber == null ? null : other.betreiber.copy();
        this.strasse = other.strasse == null ? null : other.strasse.copy();
        this.hausnummer = other.hausnummer == null ? null : other.hausnummer.copy();
        this.adresszusatz = other.adresszusatz == null ? null : other.adresszusatz.copy();
        this.postleitzahl = other.postleitzahl == null ? null : other.postleitzahl.copy();
        this.ort = other.ort == null ? null : other.ort.copy();
        this.bundesland = other.bundesland == null ? null : other.bundesland.copy();
        this.kreisKreisfreieStadt = other.kreisKreisfreieStadt == null ? null : other.kreisKreisfreieStadt.copy();
        this.inbetriebnahmeDatum = other.inbetriebnahmeDatum == null ? null : other.inbetriebnahmeDatum.copy();
        this.nennleistungLadeeinrichtung = other.nennleistungLadeeinrichtung == null ? null : other.nennleistungLadeeinrichtung.copy();
        this.artDerLadeeinrichtung = other.artDerLadeeinrichtung == null ? null : other.artDerLadeeinrichtung.copy();
        this.anzahlLadepunkte = other.anzahlLadepunkte == null ? null : other.anzahlLadepunkte.copy();
        this.ersterLadepunktSteckertypen = other.ersterLadepunktSteckertypen == null ? null : other.ersterLadepunktSteckertypen.copy();
        this.ersterLadepunktLeistung = other.ersterLadepunktLeistung == null ? null : other.ersterLadepunktLeistung.copy();
        this.zweiterLadepunktSteckertypen = other.zweiterLadepunktSteckertypen == null ? null : other.zweiterLadepunktSteckertypen.copy();
        this.zweiterLadepunktLeistung = other.zweiterLadepunktLeistung == null ? null : other.zweiterLadepunktLeistung.copy();
        this.dritterLadepunktSteckertypen = other.dritterLadepunktSteckertypen == null ? null : other.dritterLadepunktSteckertypen.copy();
        this.dritterLadepunktLeistung = other.dritterLadepunktLeistung == null ? null : other.dritterLadepunktLeistung.copy();
        this.vierterLadepunktSteckertypen = other.vierterLadepunktSteckertypen == null ? null : other.vierterLadepunktSteckertypen.copy();
        this.vierterLadepunktLeistung = other.vierterLadepunktLeistung == null ? null : other.vierterLadepunktLeistung.copy();
        this.geometry = other.geometry == null ? null : other.geometry.copy();
        this.administrationUnitId = other.administrationUnitId == null ? null : other.administrationUnitId.copy();
        this.statisticId = other.statisticId == null ? null : other.statisticId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public ChargingStationCriteria copy() {
        return new ChargingStationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getBetreiber() {
        return betreiber;
    }

    public StringFilter betreiber() {
        if (betreiber == null) {
            betreiber = new StringFilter();
        }
        return betreiber;
    }

    public void setBetreiber(StringFilter betreiber) {
        this.betreiber = betreiber;
    }

    public StringFilter getStrasse() {
        return strasse;
    }

    public StringFilter strasse() {
        if (strasse == null) {
            strasse = new StringFilter();
        }
        return strasse;
    }

    public void setStrasse(StringFilter strasse) {
        this.strasse = strasse;
    }

    public StringFilter getHausnummer() {
        return hausnummer;
    }

    public StringFilter hausnummer() {
        if (hausnummer == null) {
            hausnummer = new StringFilter();
        }
        return hausnummer;
    }

    public void setHausnummer(StringFilter hausnummer) {
        this.hausnummer = hausnummer;
    }

    public StringFilter getAdresszusatz() {
        return adresszusatz;
    }

    public StringFilter adresszusatz() {
        if (adresszusatz == null) {
            adresszusatz = new StringFilter();
        }
        return adresszusatz;
    }

    public void setAdresszusatz(StringFilter adresszusatz) {
        this.adresszusatz = adresszusatz;
    }

    public StringFilter getPostleitzahl() {
        return postleitzahl;
    }

    public StringFilter postleitzahl() {
        if (postleitzahl == null) {
            postleitzahl = new StringFilter();
        }
        return postleitzahl;
    }

    public void setPostleitzahl(StringFilter postleitzahl) {
        this.postleitzahl = postleitzahl;
    }

    public StringFilter getOrt() {
        return ort;
    }

    public StringFilter ort() {
        if (ort == null) {
            ort = new StringFilter();
        }
        return ort;
    }

    public void setOrt(StringFilter ort) {
        this.ort = ort;
    }

    public StringFilter getBundesland() {
        return bundesland;
    }

    public StringFilter bundesland() {
        if (bundesland == null) {
            bundesland = new StringFilter();
        }
        return bundesland;
    }

    public void setBundesland(StringFilter bundesland) {
        this.bundesland = bundesland;
    }

    public StringFilter getKreisKreisfreieStadt() {
        return kreisKreisfreieStadt;
    }

    public StringFilter kreisKreisfreieStadt() {
        if (kreisKreisfreieStadt == null) {
            kreisKreisfreieStadt = new StringFilter();
        }
        return kreisKreisfreieStadt;
    }

    public void setKreisKreisfreieStadt(StringFilter kreisKreisfreieStadt) {
        this.kreisKreisfreieStadt = kreisKreisfreieStadt;
    }

    public LocalDateFilter getInbetriebnahmeDatum() {
        return inbetriebnahmeDatum;
    }

    public LocalDateFilter inbetriebnahmeDatum() {
        if (inbetriebnahmeDatum == null) {
            inbetriebnahmeDatum = new LocalDateFilter();
        }
        return inbetriebnahmeDatum;
    }

    public void setInbetriebnahmeDatum(LocalDateFilter inbetriebnahmeDatum) {
        this.inbetriebnahmeDatum = inbetriebnahmeDatum;
    }

    public DoubleFilter getNennleistungLadeeinrichtung() {
        return nennleistungLadeeinrichtung;
    }

    public DoubleFilter nennleistungLadeeinrichtung() {
        if (nennleistungLadeeinrichtung == null) {
            nennleistungLadeeinrichtung = new DoubleFilter();
        }
        return nennleistungLadeeinrichtung;
    }

    public void setNennleistungLadeeinrichtung(DoubleFilter nennleistungLadeeinrichtung) {
        this.nennleistungLadeeinrichtung = nennleistungLadeeinrichtung;
    }

    public StringFilter getArtDerLadeeinrichtung() {
        return artDerLadeeinrichtung;
    }

    public StringFilter artDerLadeeinrichtung() {
        if (artDerLadeeinrichtung == null) {
            artDerLadeeinrichtung = new StringFilter();
        }
        return artDerLadeeinrichtung;
    }

    public void setArtDerLadeeinrichtung(StringFilter artDerLadeeinrichtung) {
        this.artDerLadeeinrichtung = artDerLadeeinrichtung;
    }

    public IntegerFilter getAnzahlLadepunkte() {
        return anzahlLadepunkte;
    }

    public IntegerFilter anzahlLadepunkte() {
        if (anzahlLadepunkte == null) {
            anzahlLadepunkte = new IntegerFilter();
        }
        return anzahlLadepunkte;
    }

    public void setAnzahlLadepunkte(IntegerFilter anzahlLadepunkte) {
        this.anzahlLadepunkte = anzahlLadepunkte;
    }

    public StringFilter getErsterLadepunktSteckertypen() {
        return ersterLadepunktSteckertypen;
    }

    public StringFilter ersterLadepunktSteckertypen() {
        if (ersterLadepunktSteckertypen == null) {
            ersterLadepunktSteckertypen = new StringFilter();
        }
        return ersterLadepunktSteckertypen;
    }

    public void setErsterLadepunktSteckertypen(StringFilter ersterLadepunktSteckertypen) {
        this.ersterLadepunktSteckertypen = ersterLadepunktSteckertypen;
    }

    public DoubleFilter getErsterLadepunktLeistung() {
        return ersterLadepunktLeistung;
    }

    public DoubleFilter ersterLadepunktLeistung() {
        if (ersterLadepunktLeistung == null) {
            ersterLadepunktLeistung = new DoubleFilter();
        }
        return ersterLadepunktLeistung;
    }

    public void setErsterLadepunktLeistung(DoubleFilter ersterLadepunktLeistung) {
        this.ersterLadepunktLeistung = ersterLadepunktLeistung;
    }

    public StringFilter getZweiterLadepunktSteckertypen() {
        return zweiterLadepunktSteckertypen;
    }

    public StringFilter zweiterLadepunktSteckertypen() {
        if (zweiterLadepunktSteckertypen == null) {
            zweiterLadepunktSteckertypen = new StringFilter();
        }
        return zweiterLadepunktSteckertypen;
    }

    public void setZweiterLadepunktSteckertypen(StringFilter zweiterLadepunktSteckertypen) {
        this.zweiterLadepunktSteckertypen = zweiterLadepunktSteckertypen;
    }

    public DoubleFilter getZweiterLadepunktLeistung() {
        return zweiterLadepunktLeistung;
    }

    public DoubleFilter zweiterLadepunktLeistung() {
        if (zweiterLadepunktLeistung == null) {
            zweiterLadepunktLeistung = new DoubleFilter();
        }
        return zweiterLadepunktLeistung;
    }

    public void setZweiterLadepunktLeistung(DoubleFilter zweiterLadepunktLeistung) {
        this.zweiterLadepunktLeistung = zweiterLadepunktLeistung;
    }

    public StringFilter getDritterLadepunktSteckertypen() {
        return dritterLadepunktSteckertypen;
    }

    public StringFilter dritterLadepunktSteckertypen() {
        if (dritterLadepunktSteckertypen == null) {
            dritterLadepunktSteckertypen = new StringFilter();
        }
        return dritterLadepunktSteckertypen;
    }

    public void setDritterLadepunktSteckertypen(StringFilter dritterLadepunktSteckertypen) {
        this.dritterLadepunktSteckertypen = dritterLadepunktSteckertypen;
    }

    public DoubleFilter getDritterLadepunktLeistung() {
        return dritterLadepunktLeistung;
    }

    public DoubleFilter dritterLadepunktLeistung() {
        if (dritterLadepunktLeistung == null) {
            dritterLadepunktLeistung = new DoubleFilter();
        }
        return dritterLadepunktLeistung;
    }

    public void setDritterLadepunktLeistung(DoubleFilter dritterLadepunktLeistung) {
        this.dritterLadepunktLeistung = dritterLadepunktLeistung;
    }

    public StringFilter getVierterLadepunktSteckertypen() {
        return vierterLadepunktSteckertypen;
    }

    public StringFilter vierterLadepunktSteckertypen() {
        if (vierterLadepunktSteckertypen == null) {
            vierterLadepunktSteckertypen = new StringFilter();
        }
        return vierterLadepunktSteckertypen;
    }

    public void setVierterLadepunktSteckertypen(StringFilter vierterLadepunktSteckertypen) {
        this.vierterLadepunktSteckertypen = vierterLadepunktSteckertypen;
    }

    public DoubleFilter getVierterLadepunktLeistung() {
        return vierterLadepunktLeistung;
    }

    public DoubleFilter vierterLadepunktLeistung() {
        if (vierterLadepunktLeistung == null) {
            vierterLadepunktLeistung = new DoubleFilter();
        }
        return vierterLadepunktLeistung;
    }

    public void setVierterLadepunktLeistung(DoubleFilter vierterLadepunktLeistung) {
        this.vierterLadepunktLeistung = vierterLadepunktLeistung;
    }

    public StringFilter getGeometry() {
        return geometry;
    }

    public StringFilter geometry() {
        if (geometry == null) {
            geometry = new StringFilter();
        }
        return geometry;
    }

    public void setGeometry(StringFilter geometry) {
        this.geometry = geometry;
    }

    public LongFilter getAdministrationUnitId() {
        return administrationUnitId;
    }

    public LongFilter administrationUnitId() {
        if (administrationUnitId == null) {
            administrationUnitId = new LongFilter();
        }
        return administrationUnitId;
    }

    public void setAdministrationUnitId(LongFilter administrationUnitId) {
        this.administrationUnitId = administrationUnitId;
    }

    public LongFilter getStatisticId() {
        return statisticId;
    }

    public LongFilter statisticId() {
        if (statisticId == null) {
            statisticId = new LongFilter();
        }
        return statisticId;
    }

    public void setStatisticId(LongFilter statisticId) {
        this.statisticId = statisticId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ChargingStationCriteria that = (ChargingStationCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(betreiber, that.betreiber) &&
            Objects.equals(strasse, that.strasse) &&
            Objects.equals(hausnummer, that.hausnummer) &&
            Objects.equals(adresszusatz, that.adresszusatz) &&
            Objects.equals(postleitzahl, that.postleitzahl) &&
            Objects.equals(ort, that.ort) &&
            Objects.equals(bundesland, that.bundesland) &&
            Objects.equals(kreisKreisfreieStadt, that.kreisKreisfreieStadt) &&
            Objects.equals(inbetriebnahmeDatum, that.inbetriebnahmeDatum) &&
            Objects.equals(nennleistungLadeeinrichtung, that.nennleistungLadeeinrichtung) &&
            Objects.equals(artDerLadeeinrichtung, that.artDerLadeeinrichtung) &&
            Objects.equals(anzahlLadepunkte, that.anzahlLadepunkte) &&
            Objects.equals(ersterLadepunktSteckertypen, that.ersterLadepunktSteckertypen) &&
            Objects.equals(ersterLadepunktLeistung, that.ersterLadepunktLeistung) &&
            Objects.equals(zweiterLadepunktSteckertypen, that.zweiterLadepunktSteckertypen) &&
            Objects.equals(zweiterLadepunktLeistung, that.zweiterLadepunktLeistung) &&
            Objects.equals(dritterLadepunktSteckertypen, that.dritterLadepunktSteckertypen) &&
            Objects.equals(dritterLadepunktLeistung, that.dritterLadepunktLeistung) &&
            Objects.equals(vierterLadepunktSteckertypen, that.vierterLadepunktSteckertypen) &&
            Objects.equals(vierterLadepunktLeistung, that.vierterLadepunktLeistung) &&
            Objects.equals(geometry, that.geometry) &&
            Objects.equals(administrationUnitId, that.administrationUnitId) &&
            Objects.equals(statisticId, that.statisticId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            betreiber,
            strasse,
            hausnummer,
            adresszusatz,
            postleitzahl,
            ort,
            bundesland,
            kreisKreisfreieStadt,
            inbetriebnahmeDatum,
            nennleistungLadeeinrichtung,
            artDerLadeeinrichtung,
            anzahlLadepunkte,
            ersterLadepunktSteckertypen,
            ersterLadepunktLeistung,
            zweiterLadepunktSteckertypen,
            zweiterLadepunktLeistung,
            dritterLadepunktSteckertypen,
            dritterLadepunktLeistung,
            vierterLadepunktSteckertypen,
            vierterLadepunktLeistung,
            geometry,
            administrationUnitId,
            statisticId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChargingStationCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (betreiber != null ? "betreiber=" + betreiber + ", " : "") +
            (strasse != null ? "strasse=" + strasse + ", " : "") +
            (hausnummer != null ? "hausnummer=" + hausnummer + ", " : "") +
            (adresszusatz != null ? "adresszusatz=" + adresszusatz + ", " : "") +
            (postleitzahl != null ? "postleitzahl=" + postleitzahl + ", " : "") +
            (ort != null ? "ort=" + ort + ", " : "") +
            (bundesland != null ? "bundesland=" + bundesland + ", " : "") +
            (kreisKreisfreieStadt != null ? "kreisKreisfreieStadt=" + kreisKreisfreieStadt + ", " : "") +
            (inbetriebnahmeDatum != null ? "inbetriebnahmeDatum=" + inbetriebnahmeDatum + ", " : "") +
            (nennleistungLadeeinrichtung != null ? "nennleistungLadeeinrichtung=" + nennleistungLadeeinrichtung + ", " : "") +
            (artDerLadeeinrichtung != null ? "artDerLadeeinrichtung=" + artDerLadeeinrichtung + ", " : "") +
            (anzahlLadepunkte != null ? "anzahlLadepunkte=" + anzahlLadepunkte + ", " : "") +
            (ersterLadepunktSteckertypen != null ? "ersterLadepunktSteckertypen=" + ersterLadepunktSteckertypen + ", " : "") +
            (ersterLadepunktLeistung != null ? "ersterLadepunktLeistung=" + ersterLadepunktLeistung + ", " : "") +
            (zweiterLadepunktSteckertypen != null ? "zweiterLadepunktSteckertypen=" + zweiterLadepunktSteckertypen + ", " : "") +
            (zweiterLadepunktLeistung != null ? "zweiterLadepunktLeistung=" + zweiterLadepunktLeistung + ", " : "") +
            (dritterLadepunktSteckertypen != null ? "dritterLadepunktSteckertypen=" + dritterLadepunktSteckertypen + ", " : "") +
            (dritterLadepunktLeistung != null ? "dritterLadepunktLeistung=" + dritterLadepunktLeistung + ", " : "") +
            (vierterLadepunktSteckertypen != null ? "vierterLadepunktSteckertypen=" + vierterLadepunktSteckertypen + ", " : "") +
            (vierterLadepunktLeistung != null ? "vierterLadepunktLeistung=" + vierterLadepunktLeistung + ", " : "") +
            (geometry != null ? "geometry=" + geometry + ", " : "") +
            (administrationUnitId != null ? "administrationUnitId=" + administrationUnitId + ", " : "") +
            (statisticId != null ? "statisticId=" + statisticId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
