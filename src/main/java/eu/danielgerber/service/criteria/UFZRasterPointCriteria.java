package eu.danielgerber.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link eu.danielgerber.domain.UFZRasterPoint} entity. This class is used
 * in {@link eu.danielgerber.web.rest.UFZRasterPointResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /ufz-raster-points?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UFZRasterPointCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter lat;

    private DoubleFilter lng;

    private StringFilter geometry;

    private LongFilter ufzMeasurementId;

    private Boolean distinct;

    public UFZRasterPointCriteria() {}

    public UFZRasterPointCriteria(UFZRasterPointCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.lat = other.lat == null ? null : other.lat.copy();
        this.lng = other.lng == null ? null : other.lng.copy();
        this.geometry = other.geometry == null ? null : other.geometry.copy();
        this.ufzMeasurementId = other.ufzMeasurementId == null ? null : other.ufzMeasurementId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public UFZRasterPointCriteria copy() {
        return new UFZRasterPointCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getLat() {
        return lat;
    }

    public DoubleFilter lat() {
        if (lat == null) {
            lat = new DoubleFilter();
        }
        return lat;
    }

    public void setLat(DoubleFilter lat) {
        this.lat = lat;
    }

    public DoubleFilter getLng() {
        return lng;
    }

    public DoubleFilter lng() {
        if (lng == null) {
            lng = new DoubleFilter();
        }
        return lng;
    }

    public void setLng(DoubleFilter lng) {
        this.lng = lng;
    }

    public StringFilter getGeometry() {
        return geometry;
    }

    public StringFilter geometry() {
        if (geometry == null) {
            geometry = new StringFilter();
        }
        return geometry;
    }

    public void setGeometry(StringFilter geometry) {
        this.geometry = geometry;
    }

    public LongFilter getUfzMeasurementId() {
        return ufzMeasurementId;
    }

    public LongFilter ufzMeasurementId() {
        if (ufzMeasurementId == null) {
            ufzMeasurementId = new LongFilter();
        }
        return ufzMeasurementId;
    }

    public void setUfzMeasurementId(LongFilter ufzMeasurementId) {
        this.ufzMeasurementId = ufzMeasurementId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UFZRasterPointCriteria that = (UFZRasterPointCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(lat, that.lat) &&
            Objects.equals(lng, that.lng) &&
            Objects.equals(geometry, that.geometry) &&
            Objects.equals(ufzMeasurementId, that.ufzMeasurementId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, lat, lng, geometry, ufzMeasurementId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UFZRasterPointCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (lat != null ? "lat=" + lat + ", " : "") +
            (lng != null ? "lng=" + lng + ", " : "") +
            (geometry != null ? "geometry=" + geometry + ", " : "") +
            (ufzMeasurementId != null ? "ufzMeasurementId=" + ufzMeasurementId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
