package eu.danielgerber.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link eu.danielgerber.domain.UFZMeasurement} entity. This class is used
 * in {@link eu.danielgerber.web.rest.UFZMeasurementResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /ufz-measurements?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UFZMeasurementCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter smiGesamtboden;

    private DoubleFilter smiOberboden;

    private DoubleFilter nutzbareFeldkapazitaet;

    private LocalDateFilter date;

    private LongFilter ufzRasterPointId;

    private Boolean distinct;

    public UFZMeasurementCriteria() {}

    public UFZMeasurementCriteria(UFZMeasurementCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.smiGesamtboden = other.smiGesamtboden == null ? null : other.smiGesamtboden.copy();
        this.smiOberboden = other.smiOberboden == null ? null : other.smiOberboden.copy();
        this.nutzbareFeldkapazitaet = other.nutzbareFeldkapazitaet == null ? null : other.nutzbareFeldkapazitaet.copy();
        this.date = other.date == null ? null : other.date.copy();
        this.ufzRasterPointId = other.ufzRasterPointId == null ? null : other.ufzRasterPointId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public UFZMeasurementCriteria copy() {
        return new UFZMeasurementCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getSmiGesamtboden() {
        return smiGesamtboden;
    }

    public DoubleFilter smiGesamtboden() {
        if (smiGesamtboden == null) {
            smiGesamtboden = new DoubleFilter();
        }
        return smiGesamtboden;
    }

    public void setSmiGesamtboden(DoubleFilter smiGesamtboden) {
        this.smiGesamtboden = smiGesamtboden;
    }

    public DoubleFilter getSmiOberboden() {
        return smiOberboden;
    }

    public DoubleFilter smiOberboden() {
        if (smiOberboden == null) {
            smiOberboden = new DoubleFilter();
        }
        return smiOberboden;
    }

    public void setSmiOberboden(DoubleFilter smiOberboden) {
        this.smiOberboden = smiOberboden;
    }

    public DoubleFilter getNutzbareFeldkapazitaet() {
        return nutzbareFeldkapazitaet;
    }

    public DoubleFilter nutzbareFeldkapazitaet() {
        if (nutzbareFeldkapazitaet == null) {
            nutzbareFeldkapazitaet = new DoubleFilter();
        }
        return nutzbareFeldkapazitaet;
    }

    public void setNutzbareFeldkapazitaet(DoubleFilter nutzbareFeldkapazitaet) {
        this.nutzbareFeldkapazitaet = nutzbareFeldkapazitaet;
    }

    public LocalDateFilter getDate() {
        return date;
    }

    public LocalDateFilter date() {
        if (date == null) {
            date = new LocalDateFilter();
        }
        return date;
    }

    public void setDate(LocalDateFilter date) {
        this.date = date;
    }

    public LongFilter getUfzRasterPointId() {
        return ufzRasterPointId;
    }

    public LongFilter ufzRasterPointId() {
        if (ufzRasterPointId == null) {
            ufzRasterPointId = new LongFilter();
        }
        return ufzRasterPointId;
    }

    public void setUfzRasterPointId(LongFilter ufzRasterPointId) {
        this.ufzRasterPointId = ufzRasterPointId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UFZMeasurementCriteria that = (UFZMeasurementCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(smiGesamtboden, that.smiGesamtboden) &&
            Objects.equals(smiOberboden, that.smiOberboden) &&
            Objects.equals(nutzbareFeldkapazitaet, that.nutzbareFeldkapazitaet) &&
            Objects.equals(date, that.date) &&
            Objects.equals(ufzRasterPointId, that.ufzRasterPointId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, smiGesamtboden, smiOberboden, nutzbareFeldkapazitaet, date, ufzRasterPointId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UFZMeasurementCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (smiGesamtboden != null ? "smiGesamtboden=" + smiGesamtboden + ", " : "") +
            (smiOberboden != null ? "smiOberboden=" + smiOberboden + ", " : "") +
            (nutzbareFeldkapazitaet != null ? "nutzbareFeldkapazitaet=" + nutzbareFeldkapazitaet + ", " : "") +
            (date != null ? "date=" + date + ", " : "") +
            (ufzRasterPointId != null ? "ufzRasterPointId=" + ufzRasterPointId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
