package eu.danielgerber.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link eu.danielgerber.domain.Statistic} entity. This class is used
 * in {@link eu.danielgerber.web.rest.StatisticResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /statistics?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class StatisticCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter key;

    private DoubleFilter value;

    private StringFilter unit;

    private LocalDateFilter date;

    private StringFilter source;

    private LongFilter administrationUnitId;

    private LongFilter chargingStationId;

    private Boolean distinct;

    public StatisticCriteria() {}

    public StatisticCriteria(StatisticCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.key = other.key == null ? null : other.key.copy();
        this.value = other.value == null ? null : other.value.copy();
        this.unit = other.unit == null ? null : other.unit.copy();
        this.date = other.date == null ? null : other.date.copy();
        this.source = other.source == null ? null : other.source.copy();
        this.administrationUnitId = other.administrationUnitId == null ? null : other.administrationUnitId.copy();
        this.chargingStationId = other.chargingStationId == null ? null : other.chargingStationId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public StatisticCriteria copy() {
        return new StatisticCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getKey() {
        return key;
    }

    public StringFilter key() {
        if (key == null) {
            key = new StringFilter();
        }
        return key;
    }

    public void setKey(StringFilter key) {
        this.key = key;
    }

    public DoubleFilter getValue() {
        return value;
    }

    public DoubleFilter value() {
        if (value == null) {
            value = new DoubleFilter();
        }
        return value;
    }

    public void setValue(DoubleFilter value) {
        this.value = value;
    }

    public StringFilter getUnit() {
        return unit;
    }

    public StringFilter unit() {
        if (unit == null) {
            unit = new StringFilter();
        }
        return unit;
    }

    public void setUnit(StringFilter unit) {
        this.unit = unit;
    }

    public LocalDateFilter getDate() {
        return date;
    }

    public LocalDateFilter date() {
        if (date == null) {
            date = new LocalDateFilter();
        }
        return date;
    }

    public void setDate(LocalDateFilter date) {
        this.date = date;
    }

    public StringFilter getSource() {
        return source;
    }

    public StringFilter source() {
        if (source == null) {
            source = new StringFilter();
        }
        return source;
    }

    public void setSource(StringFilter source) {
        this.source = source;
    }

    public LongFilter getAdministrationUnitId() {
        return administrationUnitId;
    }

    public LongFilter administrationUnitId() {
        if (administrationUnitId == null) {
            administrationUnitId = new LongFilter();
        }
        return administrationUnitId;
    }

    public void setAdministrationUnitId(LongFilter administrationUnitId) {
        this.administrationUnitId = administrationUnitId;
    }

    public LongFilter getChargingStationId() {
        return chargingStationId;
    }

    public LongFilter chargingStationId() {
        if (chargingStationId == null) {
            chargingStationId = new LongFilter();
        }
        return chargingStationId;
    }

    public void setChargingStationId(LongFilter chargingStationId) {
        this.chargingStationId = chargingStationId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final StatisticCriteria that = (StatisticCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(key, that.key) &&
            Objects.equals(value, that.value) &&
            Objects.equals(unit, that.unit) &&
            Objects.equals(date, that.date) &&
            Objects.equals(source, that.source) &&
            Objects.equals(administrationUnitId, that.administrationUnitId) &&
            Objects.equals(chargingStationId, that.chargingStationId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, key, value, unit, date, source, administrationUnitId, chargingStationId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "StatisticCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (key != null ? "key=" + key + ", " : "") +
            (value != null ? "value=" + value + ", " : "") +
            (unit != null ? "unit=" + unit + ", " : "") +
            (date != null ? "date=" + date + ", " : "") +
            (source != null ? "source=" + source + ", " : "") +
            (administrationUnitId != null ? "administrationUnitId=" + administrationUnitId + ", " : "") +
            (chargingStationId != null ? "chargingStationId=" + chargingStationId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
