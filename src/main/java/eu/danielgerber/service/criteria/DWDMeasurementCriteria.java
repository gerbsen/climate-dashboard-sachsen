package eu.danielgerber.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link eu.danielgerber.domain.DWDMeasurement} entity. This class is used
 * in {@link eu.danielgerber.web.rest.DWDMeasurementResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /dwd-measurements?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DWDMeasurementCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter year;

    private StringFilter statistic;

    private DoubleFilter brandenburgBerlin;

    private DoubleFilter brandenburg;

    private DoubleFilter badenWuerttemberg;

    private DoubleFilter bayern;

    private DoubleFilter hessen;

    private DoubleFilter mecklenburgVorpommern;

    private DoubleFilter niedersachsen;

    private DoubleFilter niedersachsenHamburgBremen;

    private DoubleFilter nordrheinWestfalen;

    private DoubleFilter rheinlandPfalz;

    private DoubleFilter schleswigHolstein;

    private DoubleFilter saarland;

    private DoubleFilter sachsen;

    private DoubleFilter sachsenAnhalt;

    private DoubleFilter thueringenSachsenAnhalt;

    private DoubleFilter thueringen;

    private DoubleFilter deutschland;

    private Boolean distinct;

    public DWDMeasurementCriteria() {}

    public DWDMeasurementCriteria(DWDMeasurementCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.year = other.year == null ? null : other.year.copy();
        this.statistic = other.statistic == null ? null : other.statistic.copy();
        this.brandenburgBerlin = other.brandenburgBerlin == null ? null : other.brandenburgBerlin.copy();
        this.brandenburg = other.brandenburg == null ? null : other.brandenburg.copy();
        this.badenWuerttemberg = other.badenWuerttemberg == null ? null : other.badenWuerttemberg.copy();
        this.bayern = other.bayern == null ? null : other.bayern.copy();
        this.hessen = other.hessen == null ? null : other.hessen.copy();
        this.mecklenburgVorpommern = other.mecklenburgVorpommern == null ? null : other.mecklenburgVorpommern.copy();
        this.niedersachsen = other.niedersachsen == null ? null : other.niedersachsen.copy();
        this.niedersachsenHamburgBremen = other.niedersachsenHamburgBremen == null ? null : other.niedersachsenHamburgBremen.copy();
        this.nordrheinWestfalen = other.nordrheinWestfalen == null ? null : other.nordrheinWestfalen.copy();
        this.rheinlandPfalz = other.rheinlandPfalz == null ? null : other.rheinlandPfalz.copy();
        this.schleswigHolstein = other.schleswigHolstein == null ? null : other.schleswigHolstein.copy();
        this.saarland = other.saarland == null ? null : other.saarland.copy();
        this.sachsen = other.sachsen == null ? null : other.sachsen.copy();
        this.sachsenAnhalt = other.sachsenAnhalt == null ? null : other.sachsenAnhalt.copy();
        this.thueringenSachsenAnhalt = other.thueringenSachsenAnhalt == null ? null : other.thueringenSachsenAnhalt.copy();
        this.thueringen = other.thueringen == null ? null : other.thueringen.copy();
        this.deutschland = other.deutschland == null ? null : other.deutschland.copy();
        this.distinct = other.distinct;
    }

    @Override
    public DWDMeasurementCriteria copy() {
        return new DWDMeasurementCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getYear() {
        return year;
    }

    public LongFilter year() {
        if (year == null) {
            year = new LongFilter();
        }
        return year;
    }

    public void setYear(LongFilter year) {
        this.year = year;
    }

    public StringFilter getStatistic() {
        return statistic;
    }

    public StringFilter statistic() {
        if (statistic == null) {
            statistic = new StringFilter();
        }
        return statistic;
    }

    public void setStatistic(StringFilter statistic) {
        this.statistic = statistic;
    }

    public DoubleFilter getBrandenburgBerlin() {
        return brandenburgBerlin;
    }

    public DoubleFilter brandenburgBerlin() {
        if (brandenburgBerlin == null) {
            brandenburgBerlin = new DoubleFilter();
        }
        return brandenburgBerlin;
    }

    public void setBrandenburgBerlin(DoubleFilter brandenburgBerlin) {
        this.brandenburgBerlin = brandenburgBerlin;
    }

    public DoubleFilter getBrandenburg() {
        return brandenburg;
    }

    public DoubleFilter brandenburg() {
        if (brandenburg == null) {
            brandenburg = new DoubleFilter();
        }
        return brandenburg;
    }

    public void setBrandenburg(DoubleFilter brandenburg) {
        this.brandenburg = brandenburg;
    }

    public DoubleFilter getBadenWuerttemberg() {
        return badenWuerttemberg;
    }

    public DoubleFilter badenWuerttemberg() {
        if (badenWuerttemberg == null) {
            badenWuerttemberg = new DoubleFilter();
        }
        return badenWuerttemberg;
    }

    public void setBadenWuerttemberg(DoubleFilter badenWuerttemberg) {
        this.badenWuerttemberg = badenWuerttemberg;
    }

    public DoubleFilter getBayern() {
        return bayern;
    }

    public DoubleFilter bayern() {
        if (bayern == null) {
            bayern = new DoubleFilter();
        }
        return bayern;
    }

    public void setBayern(DoubleFilter bayern) {
        this.bayern = bayern;
    }

    public DoubleFilter getHessen() {
        return hessen;
    }

    public DoubleFilter hessen() {
        if (hessen == null) {
            hessen = new DoubleFilter();
        }
        return hessen;
    }

    public void setHessen(DoubleFilter hessen) {
        this.hessen = hessen;
    }

    public DoubleFilter getMecklenburgVorpommern() {
        return mecklenburgVorpommern;
    }

    public DoubleFilter mecklenburgVorpommern() {
        if (mecklenburgVorpommern == null) {
            mecklenburgVorpommern = new DoubleFilter();
        }
        return mecklenburgVorpommern;
    }

    public void setMecklenburgVorpommern(DoubleFilter mecklenburgVorpommern) {
        this.mecklenburgVorpommern = mecklenburgVorpommern;
    }

    public DoubleFilter getNiedersachsen() {
        return niedersachsen;
    }

    public DoubleFilter niedersachsen() {
        if (niedersachsen == null) {
            niedersachsen = new DoubleFilter();
        }
        return niedersachsen;
    }

    public void setNiedersachsen(DoubleFilter niedersachsen) {
        this.niedersachsen = niedersachsen;
    }

    public DoubleFilter getNiedersachsenHamburgBremen() {
        return niedersachsenHamburgBremen;
    }

    public DoubleFilter niedersachsenHamburgBremen() {
        if (niedersachsenHamburgBremen == null) {
            niedersachsenHamburgBremen = new DoubleFilter();
        }
        return niedersachsenHamburgBremen;
    }

    public void setNiedersachsenHamburgBremen(DoubleFilter niedersachsenHamburgBremen) {
        this.niedersachsenHamburgBremen = niedersachsenHamburgBremen;
    }

    public DoubleFilter getNordrheinWestfalen() {
        return nordrheinWestfalen;
    }

    public DoubleFilter nordrheinWestfalen() {
        if (nordrheinWestfalen == null) {
            nordrheinWestfalen = new DoubleFilter();
        }
        return nordrheinWestfalen;
    }

    public void setNordrheinWestfalen(DoubleFilter nordrheinWestfalen) {
        this.nordrheinWestfalen = nordrheinWestfalen;
    }

    public DoubleFilter getRheinlandPfalz() {
        return rheinlandPfalz;
    }

    public DoubleFilter rheinlandPfalz() {
        if (rheinlandPfalz == null) {
            rheinlandPfalz = new DoubleFilter();
        }
        return rheinlandPfalz;
    }

    public void setRheinlandPfalz(DoubleFilter rheinlandPfalz) {
        this.rheinlandPfalz = rheinlandPfalz;
    }

    public DoubleFilter getSchleswigHolstein() {
        return schleswigHolstein;
    }

    public DoubleFilter schleswigHolstein() {
        if (schleswigHolstein == null) {
            schleswigHolstein = new DoubleFilter();
        }
        return schleswigHolstein;
    }

    public void setSchleswigHolstein(DoubleFilter schleswigHolstein) {
        this.schleswigHolstein = schleswigHolstein;
    }

    public DoubleFilter getSaarland() {
        return saarland;
    }

    public DoubleFilter saarland() {
        if (saarland == null) {
            saarland = new DoubleFilter();
        }
        return saarland;
    }

    public void setSaarland(DoubleFilter saarland) {
        this.saarland = saarland;
    }

    public DoubleFilter getSachsen() {
        return sachsen;
    }

    public DoubleFilter sachsen() {
        if (sachsen == null) {
            sachsen = new DoubleFilter();
        }
        return sachsen;
    }

    public void setSachsen(DoubleFilter sachsen) {
        this.sachsen = sachsen;
    }

    public DoubleFilter getSachsenAnhalt() {
        return sachsenAnhalt;
    }

    public DoubleFilter sachsenAnhalt() {
        if (sachsenAnhalt == null) {
            sachsenAnhalt = new DoubleFilter();
        }
        return sachsenAnhalt;
    }

    public void setSachsenAnhalt(DoubleFilter sachsenAnhalt) {
        this.sachsenAnhalt = sachsenAnhalt;
    }

    public DoubleFilter getThueringenSachsenAnhalt() {
        return thueringenSachsenAnhalt;
    }

    public DoubleFilter thueringenSachsenAnhalt() {
        if (thueringenSachsenAnhalt == null) {
            thueringenSachsenAnhalt = new DoubleFilter();
        }
        return thueringenSachsenAnhalt;
    }

    public void setThueringenSachsenAnhalt(DoubleFilter thueringenSachsenAnhalt) {
        this.thueringenSachsenAnhalt = thueringenSachsenAnhalt;
    }

    public DoubleFilter getThueringen() {
        return thueringen;
    }

    public DoubleFilter thueringen() {
        if (thueringen == null) {
            thueringen = new DoubleFilter();
        }
        return thueringen;
    }

    public void setThueringen(DoubleFilter thueringen) {
        this.thueringen = thueringen;
    }

    public DoubleFilter getDeutschland() {
        return deutschland;
    }

    public DoubleFilter deutschland() {
        if (deutschland == null) {
            deutschland = new DoubleFilter();
        }
        return deutschland;
    }

    public void setDeutschland(DoubleFilter deutschland) {
        this.deutschland = deutschland;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final DWDMeasurementCriteria that = (DWDMeasurementCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(year, that.year) &&
            Objects.equals(statistic, that.statistic) &&
            Objects.equals(brandenburgBerlin, that.brandenburgBerlin) &&
            Objects.equals(brandenburg, that.brandenburg) &&
            Objects.equals(badenWuerttemberg, that.badenWuerttemberg) &&
            Objects.equals(bayern, that.bayern) &&
            Objects.equals(hessen, that.hessen) &&
            Objects.equals(mecklenburgVorpommern, that.mecklenburgVorpommern) &&
            Objects.equals(niedersachsen, that.niedersachsen) &&
            Objects.equals(niedersachsenHamburgBremen, that.niedersachsenHamburgBremen) &&
            Objects.equals(nordrheinWestfalen, that.nordrheinWestfalen) &&
            Objects.equals(rheinlandPfalz, that.rheinlandPfalz) &&
            Objects.equals(schleswigHolstein, that.schleswigHolstein) &&
            Objects.equals(saarland, that.saarland) &&
            Objects.equals(sachsen, that.sachsen) &&
            Objects.equals(sachsenAnhalt, that.sachsenAnhalt) &&
            Objects.equals(thueringenSachsenAnhalt, that.thueringenSachsenAnhalt) &&
            Objects.equals(thueringen, that.thueringen) &&
            Objects.equals(deutschland, that.deutschland) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            year,
            statistic,
            brandenburgBerlin,
            brandenburg,
            badenWuerttemberg,
            bayern,
            hessen,
            mecklenburgVorpommern,
            niedersachsen,
            niedersachsenHamburgBremen,
            nordrheinWestfalen,
            rheinlandPfalz,
            schleswigHolstein,
            saarland,
            sachsen,
            sachsenAnhalt,
            thueringenSachsenAnhalt,
            thueringen,
            deutschland,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DWDMeasurementCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (year != null ? "year=" + year + ", " : "") +
            (statistic != null ? "statistic=" + statistic + ", " : "") +
            (brandenburgBerlin != null ? "brandenburgBerlin=" + brandenburgBerlin + ", " : "") +
            (brandenburg != null ? "brandenburg=" + brandenburg + ", " : "") +
            (badenWuerttemberg != null ? "badenWuerttemberg=" + badenWuerttemberg + ", " : "") +
            (bayern != null ? "bayern=" + bayern + ", " : "") +
            (hessen != null ? "hessen=" + hessen + ", " : "") +
            (mecklenburgVorpommern != null ? "mecklenburgVorpommern=" + mecklenburgVorpommern + ", " : "") +
            (niedersachsen != null ? "niedersachsen=" + niedersachsen + ", " : "") +
            (niedersachsenHamburgBremen != null ? "niedersachsenHamburgBremen=" + niedersachsenHamburgBremen + ", " : "") +
            (nordrheinWestfalen != null ? "nordrheinWestfalen=" + nordrheinWestfalen + ", " : "") +
            (rheinlandPfalz != null ? "rheinlandPfalz=" + rheinlandPfalz + ", " : "") +
            (schleswigHolstein != null ? "schleswigHolstein=" + schleswigHolstein + ", " : "") +
            (saarland != null ? "saarland=" + saarland + ", " : "") +
            (sachsen != null ? "sachsen=" + sachsen + ", " : "") +
            (sachsenAnhalt != null ? "sachsenAnhalt=" + sachsenAnhalt + ", " : "") +
            (thueringenSachsenAnhalt != null ? "thueringenSachsenAnhalt=" + thueringenSachsenAnhalt + ", " : "") +
            (thueringen != null ? "thueringen=" + thueringen + ", " : "") +
            (deutschland != null ? "deutschland=" + deutschland + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
