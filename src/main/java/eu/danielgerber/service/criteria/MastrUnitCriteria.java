package eu.danielgerber.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link eu.danielgerber.domain.MastrUnit} entity. This class is used
 * in {@link eu.danielgerber.web.rest.MastrUnitResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /mastr-units?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class MastrUnitCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter mastrNummer;

    private LongFilter anlagenBetreiberId;

    private LongFilter anlagenBetreiberPersonenArt;

    private StringFilter anlagenBetreiberMaskedName;

    private StringFilter anlagenBetreiberMastrNummer;

    private StringFilter anlagenBetreiberName;

    private IntegerFilter betriebsStatusId;

    private StringFilter betriebsStatusName;

    private StringFilter bundesland;

    private IntegerFilter bundeslandId;

    private IntegerFilter landkreisId;

    private IntegerFilter gemeindeId;

    private LocalDateFilter datumLetzteAktualisierung;

    private LocalDateFilter einheitRegistrierungsDatum;

    private LocalDateFilter endgueltigeStilllegungDatum;

    private LocalDateFilter geplantesInbetriebsnahmeDatum;

    private LocalDateFilter inbetriebnahmeDatum;

    private LocalDateFilter kwkAnlageInbetriebnahmeDatum;

    private LocalDateFilter kwkAnlageRegistrierungsDatum;

    private LocalDateFilter eegInbetriebnahmeDatum;

    private LocalDateFilter eegAnlageRegistrierungsDatum;

    private LocalDateFilter genehmigungDatum;

    private LocalDateFilter genehmigungRegistrierungsDatum;

    private IntegerFilter isNbPruefungAbgeschlossen;

    private BooleanFilter isAnonymisiert;

    private BooleanFilter isBuergerEnergie;

    private BooleanFilter isEinheitNotstromaggregat;

    private BooleanFilter isMieterstromAngemeldet;

    private BooleanFilter isWasserkraftErtuechtigung;

    private BooleanFilter isPilotWindanlage;

    private BooleanFilter isPrototypAnlage;

    private DoubleFilter lat;

    private DoubleFilter lng;

    private StringFilter ort;

    private IntegerFilter plz;

    private StringFilter strasse;

    private StringFilter hausnummer;

    private StringFilter einheitname;

    private StringFilter flurstueck;

    private StringFilter gemarkung;

    private StringFilter gemeinde;

    private IntegerFilter landId;

    private StringFilter landkreis;

    private LongFilter ags;

    private LongFilter lokationId;

    private StringFilter lokationMastrNr;

    private StringFilter netzbetreiberId;

    private StringFilter netzbetreiberMaskedNamen;

    private StringFilter netzbetreiberMastrNummer;

    private StringFilter netzbetreiberNamen;

    private StringFilter netzbetreiberPersonenArt;

    private IntegerFilter systemStatusId;

    private StringFilter systemStatusName;

    private IntegerFilter typ;

    private StringFilter aktenzeichenGenehmigung;

    private IntegerFilter anzahlSolarmodule;

    private IntegerFilter batterieTechnologie;

    private DoubleFilter bruttoLeistung;

    private DoubleFilter eegInstallierteLeistung;

    private StringFilter eegAnlageMastrNummer;

    private StringFilter eegAnlagenSchluessel;

    private StringFilter eegZuschlag;

    private StringFilter zuschlagsNummern;

    private IntegerFilter energieTraegerId;

    private StringFilter energieTraegerName;

    private IntegerFilter gemeinsamerWechselrichter;

    private StringFilter genehmigungBehoerde;

    private StringFilter genehmigungsMastrNummer;

    private StringFilter gruppierungsObjekte;

    private StringFilter gruppierungsObjekteIds;

    private BooleanFilter hatFlexibilitaetsPraemie;

    private IntegerFilter hauptAusrichtungSolarmodule;

    private StringFilter hauptAusrichtungSolarmoduleBezeichnung;

    private IntegerFilter hauptBrennstoffId;

    private StringFilter hauptBrennstoffNamen;

    private IntegerFilter hauptNeigungswinkelSolarModule;

    private IntegerFilter herstellerWindenergieanlageId;

    private StringFilter herstellerWindenergieanlageBezeichnung;

    private DoubleFilter kwkAnlageElektrischeLeistung;

    private StringFilter kwkAnlageMastrNummer;

    private StringFilter kwkZuschlag;

    private IntegerFilter lageEinheit;

    private StringFilter lageEinheitBezeichnung;

    private IntegerFilter leistungsBegrenzung;

    private DoubleFilter nabenhoeheWindenergieanlage;

    private DoubleFilter rotorDurchmesserWindenergieanlage;

    private DoubleFilter nettoNennLeistung;

    private DoubleFilter nutzbareSpeicherKapazitaet;

    private IntegerFilter nutzungsBereichGebsa;

    private StringFilter standortAnonymisiert;

    private StringFilter spannungsEbenenId;

    private StringFilter spannungsEbenenNamen;

    private StringFilter speicherEinheitMastrNummer;

    private IntegerFilter technologieStromerzeugungId;

    private StringFilter technologieStromerzeugung;

    private DoubleFilter thermischeNutzLeistung;

    private StringFilter typenBezeichnung;

    private IntegerFilter vollTeilEinspeisung;

    private StringFilter vollTeilEinspeisungBezeichnung;

    private StringFilter windparkName;

    private LongFilter administrationUnitId;

    private Boolean distinct;

    public MastrUnitCriteria() {}

    public MastrUnitCriteria(MastrUnitCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.mastrNummer = other.mastrNummer == null ? null : other.mastrNummer.copy();
        this.anlagenBetreiberId = other.anlagenBetreiberId == null ? null : other.anlagenBetreiberId.copy();
        this.anlagenBetreiberPersonenArt = other.anlagenBetreiberPersonenArt == null ? null : other.anlagenBetreiberPersonenArt.copy();
        this.anlagenBetreiberMaskedName = other.anlagenBetreiberMaskedName == null ? null : other.anlagenBetreiberMaskedName.copy();
        this.anlagenBetreiberMastrNummer = other.anlagenBetreiberMastrNummer == null ? null : other.anlagenBetreiberMastrNummer.copy();
        this.anlagenBetreiberName = other.anlagenBetreiberName == null ? null : other.anlagenBetreiberName.copy();
        this.betriebsStatusId = other.betriebsStatusId == null ? null : other.betriebsStatusId.copy();
        this.betriebsStatusName = other.betriebsStatusName == null ? null : other.betriebsStatusName.copy();
        this.bundesland = other.bundesland == null ? null : other.bundesland.copy();
        this.bundeslandId = other.bundeslandId == null ? null : other.bundeslandId.copy();
        this.landkreisId = other.landkreisId == null ? null : other.landkreisId.copy();
        this.gemeindeId = other.gemeindeId == null ? null : other.gemeindeId.copy();
        this.datumLetzteAktualisierung = other.datumLetzteAktualisierung == null ? null : other.datumLetzteAktualisierung.copy();
        this.einheitRegistrierungsDatum = other.einheitRegistrierungsDatum == null ? null : other.einheitRegistrierungsDatum.copy();
        this.endgueltigeStilllegungDatum = other.endgueltigeStilllegungDatum == null ? null : other.endgueltigeStilllegungDatum.copy();
        this.geplantesInbetriebsnahmeDatum =
            other.geplantesInbetriebsnahmeDatum == null ? null : other.geplantesInbetriebsnahmeDatum.copy();
        this.inbetriebnahmeDatum = other.inbetriebnahmeDatum == null ? null : other.inbetriebnahmeDatum.copy();
        this.kwkAnlageInbetriebnahmeDatum = other.kwkAnlageInbetriebnahmeDatum == null ? null : other.kwkAnlageInbetriebnahmeDatum.copy();
        this.kwkAnlageRegistrierungsDatum = other.kwkAnlageRegistrierungsDatum == null ? null : other.kwkAnlageRegistrierungsDatum.copy();
        this.eegInbetriebnahmeDatum = other.eegInbetriebnahmeDatum == null ? null : other.eegInbetriebnahmeDatum.copy();
        this.eegAnlageRegistrierungsDatum = other.eegAnlageRegistrierungsDatum == null ? null : other.eegAnlageRegistrierungsDatum.copy();
        this.genehmigungDatum = other.genehmigungDatum == null ? null : other.genehmigungDatum.copy();
        this.genehmigungRegistrierungsDatum =
            other.genehmigungRegistrierungsDatum == null ? null : other.genehmigungRegistrierungsDatum.copy();
        this.isNbPruefungAbgeschlossen = other.isNbPruefungAbgeschlossen == null ? null : other.isNbPruefungAbgeschlossen.copy();
        this.isAnonymisiert = other.isAnonymisiert == null ? null : other.isAnonymisiert.copy();
        this.isBuergerEnergie = other.isBuergerEnergie == null ? null : other.isBuergerEnergie.copy();
        this.isEinheitNotstromaggregat = other.isEinheitNotstromaggregat == null ? null : other.isEinheitNotstromaggregat.copy();
        this.isMieterstromAngemeldet = other.isMieterstromAngemeldet == null ? null : other.isMieterstromAngemeldet.copy();
        this.isWasserkraftErtuechtigung = other.isWasserkraftErtuechtigung == null ? null : other.isWasserkraftErtuechtigung.copy();
        this.isPilotWindanlage = other.isPilotWindanlage == null ? null : other.isPilotWindanlage.copy();
        this.isPrototypAnlage = other.isPrototypAnlage == null ? null : other.isPrototypAnlage.copy();
        this.lat = other.lat == null ? null : other.lat.copy();
        this.lng = other.lng == null ? null : other.lng.copy();
        this.ort = other.ort == null ? null : other.ort.copy();
        this.plz = other.plz == null ? null : other.plz.copy();
        this.strasse = other.strasse == null ? null : other.strasse.copy();
        this.hausnummer = other.hausnummer == null ? null : other.hausnummer.copy();
        this.einheitname = other.einheitname == null ? null : other.einheitname.copy();
        this.flurstueck = other.flurstueck == null ? null : other.flurstueck.copy();
        this.gemarkung = other.gemarkung == null ? null : other.gemarkung.copy();
        this.gemeinde = other.gemeinde == null ? null : other.gemeinde.copy();
        this.landId = other.landId == null ? null : other.landId.copy();
        this.landkreis = other.landkreis == null ? null : other.landkreis.copy();
        this.ags = other.ags == null ? null : other.ags.copy();
        this.lokationId = other.lokationId == null ? null : other.lokationId.copy();
        this.lokationMastrNr = other.lokationMastrNr == null ? null : other.lokationMastrNr.copy();
        this.netzbetreiberId = other.netzbetreiberId == null ? null : other.netzbetreiberId.copy();
        this.netzbetreiberMaskedNamen = other.netzbetreiberMaskedNamen == null ? null : other.netzbetreiberMaskedNamen.copy();
        this.netzbetreiberMastrNummer = other.netzbetreiberMastrNummer == null ? null : other.netzbetreiberMastrNummer.copy();
        this.netzbetreiberNamen = other.netzbetreiberNamen == null ? null : other.netzbetreiberNamen.copy();
        this.netzbetreiberPersonenArt = other.netzbetreiberPersonenArt == null ? null : other.netzbetreiberPersonenArt.copy();
        this.systemStatusId = other.systemStatusId == null ? null : other.systemStatusId.copy();
        this.systemStatusName = other.systemStatusName == null ? null : other.systemStatusName.copy();
        this.typ = other.typ == null ? null : other.typ.copy();
        this.aktenzeichenGenehmigung = other.aktenzeichenGenehmigung == null ? null : other.aktenzeichenGenehmigung.copy();
        this.anzahlSolarmodule = other.anzahlSolarmodule == null ? null : other.anzahlSolarmodule.copy();
        this.batterieTechnologie = other.batterieTechnologie == null ? null : other.batterieTechnologie.copy();
        this.bruttoLeistung = other.bruttoLeistung == null ? null : other.bruttoLeistung.copy();
        this.eegInstallierteLeistung = other.eegInstallierteLeistung == null ? null : other.eegInstallierteLeistung.copy();
        this.eegAnlageMastrNummer = other.eegAnlageMastrNummer == null ? null : other.eegAnlageMastrNummer.copy();
        this.eegAnlagenSchluessel = other.eegAnlagenSchluessel == null ? null : other.eegAnlagenSchluessel.copy();
        this.eegZuschlag = other.eegZuschlag == null ? null : other.eegZuschlag.copy();
        this.zuschlagsNummern = other.zuschlagsNummern == null ? null : other.zuschlagsNummern.copy();
        this.energieTraegerId = other.energieTraegerId == null ? null : other.energieTraegerId.copy();
        this.energieTraegerName = other.energieTraegerName == null ? null : other.energieTraegerName.copy();
        this.gemeinsamerWechselrichter = other.gemeinsamerWechselrichter == null ? null : other.gemeinsamerWechselrichter.copy();
        this.genehmigungBehoerde = other.genehmigungBehoerde == null ? null : other.genehmigungBehoerde.copy();
        this.genehmigungsMastrNummer = other.genehmigungsMastrNummer == null ? null : other.genehmigungsMastrNummer.copy();
        this.gruppierungsObjekte = other.gruppierungsObjekte == null ? null : other.gruppierungsObjekte.copy();
        this.gruppierungsObjekteIds = other.gruppierungsObjekteIds == null ? null : other.gruppierungsObjekteIds.copy();
        this.hatFlexibilitaetsPraemie = other.hatFlexibilitaetsPraemie == null ? null : other.hatFlexibilitaetsPraemie.copy();
        this.hauptAusrichtungSolarmodule = other.hauptAusrichtungSolarmodule == null ? null : other.hauptAusrichtungSolarmodule.copy();
        this.hauptAusrichtungSolarmoduleBezeichnung =
            other.hauptAusrichtungSolarmoduleBezeichnung == null ? null : other.hauptAusrichtungSolarmoduleBezeichnung.copy();
        this.hauptBrennstoffId = other.hauptBrennstoffId == null ? null : other.hauptBrennstoffId.copy();
        this.hauptBrennstoffNamen = other.hauptBrennstoffNamen == null ? null : other.hauptBrennstoffNamen.copy();
        this.hauptNeigungswinkelSolarModule =
            other.hauptNeigungswinkelSolarModule == null ? null : other.hauptNeigungswinkelSolarModule.copy();
        this.herstellerWindenergieanlageId =
            other.herstellerWindenergieanlageId == null ? null : other.herstellerWindenergieanlageId.copy();
        this.herstellerWindenergieanlageBezeichnung =
            other.herstellerWindenergieanlageBezeichnung == null ? null : other.herstellerWindenergieanlageBezeichnung.copy();
        this.kwkAnlageElektrischeLeistung = other.kwkAnlageElektrischeLeistung == null ? null : other.kwkAnlageElektrischeLeistung.copy();
        this.kwkAnlageMastrNummer = other.kwkAnlageMastrNummer == null ? null : other.kwkAnlageMastrNummer.copy();
        this.kwkZuschlag = other.kwkZuschlag == null ? null : other.kwkZuschlag.copy();
        this.lageEinheit = other.lageEinheit == null ? null : other.lageEinheit.copy();
        this.lageEinheitBezeichnung = other.lageEinheitBezeichnung == null ? null : other.lageEinheitBezeichnung.copy();
        this.leistungsBegrenzung = other.leistungsBegrenzung == null ? null : other.leistungsBegrenzung.copy();
        this.nabenhoeheWindenergieanlage = other.nabenhoeheWindenergieanlage == null ? null : other.nabenhoeheWindenergieanlage.copy();
        this.rotorDurchmesserWindenergieanlage =
            other.rotorDurchmesserWindenergieanlage == null ? null : other.rotorDurchmesserWindenergieanlage.copy();
        this.nettoNennLeistung = other.nettoNennLeistung == null ? null : other.nettoNennLeistung.copy();
        this.nutzbareSpeicherKapazitaet = other.nutzbareSpeicherKapazitaet == null ? null : other.nutzbareSpeicherKapazitaet.copy();
        this.nutzungsBereichGebsa = other.nutzungsBereichGebsa == null ? null : other.nutzungsBereichGebsa.copy();
        this.standortAnonymisiert = other.standortAnonymisiert == null ? null : other.standortAnonymisiert.copy();
        this.spannungsEbenenId = other.spannungsEbenenId == null ? null : other.spannungsEbenenId.copy();
        this.spannungsEbenenNamen = other.spannungsEbenenNamen == null ? null : other.spannungsEbenenNamen.copy();
        this.speicherEinheitMastrNummer = other.speicherEinheitMastrNummer == null ? null : other.speicherEinheitMastrNummer.copy();
        this.technologieStromerzeugungId = other.technologieStromerzeugungId == null ? null : other.technologieStromerzeugungId.copy();
        this.technologieStromerzeugung = other.technologieStromerzeugung == null ? null : other.technologieStromerzeugung.copy();
        this.thermischeNutzLeistung = other.thermischeNutzLeistung == null ? null : other.thermischeNutzLeistung.copy();
        this.typenBezeichnung = other.typenBezeichnung == null ? null : other.typenBezeichnung.copy();
        this.vollTeilEinspeisung = other.vollTeilEinspeisung == null ? null : other.vollTeilEinspeisung.copy();
        this.vollTeilEinspeisungBezeichnung =
            other.vollTeilEinspeisungBezeichnung == null ? null : other.vollTeilEinspeisungBezeichnung.copy();
        this.windparkName = other.windparkName == null ? null : other.windparkName.copy();
        this.administrationUnitId = other.administrationUnitId == null ? null : other.administrationUnitId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public MastrUnitCriteria copy() {
        return new MastrUnitCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getMastrNummer() {
        return mastrNummer;
    }

    public StringFilter mastrNummer() {
        if (mastrNummer == null) {
            mastrNummer = new StringFilter();
        }
        return mastrNummer;
    }

    public void setMastrNummer(StringFilter mastrNummer) {
        this.mastrNummer = mastrNummer;
    }

    public LongFilter getAnlagenBetreiberId() {
        return anlagenBetreiberId;
    }

    public LongFilter anlagenBetreiberId() {
        if (anlagenBetreiberId == null) {
            anlagenBetreiberId = new LongFilter();
        }
        return anlagenBetreiberId;
    }

    public void setAnlagenBetreiberId(LongFilter anlagenBetreiberId) {
        this.anlagenBetreiberId = anlagenBetreiberId;
    }

    public LongFilter getAnlagenBetreiberPersonenArt() {
        return anlagenBetreiberPersonenArt;
    }

    public LongFilter anlagenBetreiberPersonenArt() {
        if (anlagenBetreiberPersonenArt == null) {
            anlagenBetreiberPersonenArt = new LongFilter();
        }
        return anlagenBetreiberPersonenArt;
    }

    public void setAnlagenBetreiberPersonenArt(LongFilter anlagenBetreiberPersonenArt) {
        this.anlagenBetreiberPersonenArt = anlagenBetreiberPersonenArt;
    }

    public StringFilter getAnlagenBetreiberMaskedName() {
        return anlagenBetreiberMaskedName;
    }

    public StringFilter anlagenBetreiberMaskedName() {
        if (anlagenBetreiberMaskedName == null) {
            anlagenBetreiberMaskedName = new StringFilter();
        }
        return anlagenBetreiberMaskedName;
    }

    public void setAnlagenBetreiberMaskedName(StringFilter anlagenBetreiberMaskedName) {
        this.anlagenBetreiberMaskedName = anlagenBetreiberMaskedName;
    }

    public StringFilter getAnlagenBetreiberMastrNummer() {
        return anlagenBetreiberMastrNummer;
    }

    public StringFilter anlagenBetreiberMastrNummer() {
        if (anlagenBetreiberMastrNummer == null) {
            anlagenBetreiberMastrNummer = new StringFilter();
        }
        return anlagenBetreiberMastrNummer;
    }

    public void setAnlagenBetreiberMastrNummer(StringFilter anlagenBetreiberMastrNummer) {
        this.anlagenBetreiberMastrNummer = anlagenBetreiberMastrNummer;
    }

    public StringFilter getAnlagenBetreiberName() {
        return anlagenBetreiberName;
    }

    public StringFilter anlagenBetreiberName() {
        if (anlagenBetreiberName == null) {
            anlagenBetreiberName = new StringFilter();
        }
        return anlagenBetreiberName;
    }

    public void setAnlagenBetreiberName(StringFilter anlagenBetreiberName) {
        this.anlagenBetreiberName = anlagenBetreiberName;
    }

    public IntegerFilter getBetriebsStatusId() {
        return betriebsStatusId;
    }

    public IntegerFilter betriebsStatusId() {
        if (betriebsStatusId == null) {
            betriebsStatusId = new IntegerFilter();
        }
        return betriebsStatusId;
    }

    public void setBetriebsStatusId(IntegerFilter betriebsStatusId) {
        this.betriebsStatusId = betriebsStatusId;
    }

    public StringFilter getBetriebsStatusName() {
        return betriebsStatusName;
    }

    public StringFilter betriebsStatusName() {
        if (betriebsStatusName == null) {
            betriebsStatusName = new StringFilter();
        }
        return betriebsStatusName;
    }

    public void setBetriebsStatusName(StringFilter betriebsStatusName) {
        this.betriebsStatusName = betriebsStatusName;
    }

    public StringFilter getBundesland() {
        return bundesland;
    }

    public StringFilter bundesland() {
        if (bundesland == null) {
            bundesland = new StringFilter();
        }
        return bundesland;
    }

    public void setBundesland(StringFilter bundesland) {
        this.bundesland = bundesland;
    }

    public IntegerFilter getBundeslandId() {
        return bundeslandId;
    }

    public IntegerFilter bundeslandId() {
        if (bundeslandId == null) {
            bundeslandId = new IntegerFilter();
        }
        return bundeslandId;
    }

    public void setBundeslandId(IntegerFilter bundeslandId) {
        this.bundeslandId = bundeslandId;
    }

    public IntegerFilter getLandkreisId() {
        return landkreisId;
    }

    public IntegerFilter landkreisId() {
        if (landkreisId == null) {
            landkreisId = new IntegerFilter();
        }
        return landkreisId;
    }

    public void setLandkreisId(IntegerFilter landkreisId) {
        this.landkreisId = landkreisId;
    }

    public IntegerFilter getGemeindeId() {
        return gemeindeId;
    }

    public IntegerFilter gemeindeId() {
        if (gemeindeId == null) {
            gemeindeId = new IntegerFilter();
        }
        return gemeindeId;
    }

    public void setGemeindeId(IntegerFilter gemeindeId) {
        this.gemeindeId = gemeindeId;
    }

    public LocalDateFilter getDatumLetzteAktualisierung() {
        return datumLetzteAktualisierung;
    }

    public LocalDateFilter datumLetzteAktualisierung() {
        if (datumLetzteAktualisierung == null) {
            datumLetzteAktualisierung = new LocalDateFilter();
        }
        return datumLetzteAktualisierung;
    }

    public void setDatumLetzteAktualisierung(LocalDateFilter datumLetzteAktualisierung) {
        this.datumLetzteAktualisierung = datumLetzteAktualisierung;
    }

    public LocalDateFilter getEinheitRegistrierungsDatum() {
        return einheitRegistrierungsDatum;
    }

    public LocalDateFilter einheitRegistrierungsDatum() {
        if (einheitRegistrierungsDatum == null) {
            einheitRegistrierungsDatum = new LocalDateFilter();
        }
        return einheitRegistrierungsDatum;
    }

    public void setEinheitRegistrierungsDatum(LocalDateFilter einheitRegistrierungsDatum) {
        this.einheitRegistrierungsDatum = einheitRegistrierungsDatum;
    }

    public LocalDateFilter getEndgueltigeStilllegungDatum() {
        return endgueltigeStilllegungDatum;
    }

    public LocalDateFilter endgueltigeStilllegungDatum() {
        if (endgueltigeStilllegungDatum == null) {
            endgueltigeStilllegungDatum = new LocalDateFilter();
        }
        return endgueltigeStilllegungDatum;
    }

    public void setEndgueltigeStilllegungDatum(LocalDateFilter endgueltigeStilllegungDatum) {
        this.endgueltigeStilllegungDatum = endgueltigeStilllegungDatum;
    }

    public LocalDateFilter getGeplantesInbetriebsnahmeDatum() {
        return geplantesInbetriebsnahmeDatum;
    }

    public LocalDateFilter geplantesInbetriebsnahmeDatum() {
        if (geplantesInbetriebsnahmeDatum == null) {
            geplantesInbetriebsnahmeDatum = new LocalDateFilter();
        }
        return geplantesInbetriebsnahmeDatum;
    }

    public void setGeplantesInbetriebsnahmeDatum(LocalDateFilter geplantesInbetriebsnahmeDatum) {
        this.geplantesInbetriebsnahmeDatum = geplantesInbetriebsnahmeDatum;
    }

    public LocalDateFilter getInbetriebnahmeDatum() {
        return inbetriebnahmeDatum;
    }

    public LocalDateFilter inbetriebnahmeDatum() {
        if (inbetriebnahmeDatum == null) {
            inbetriebnahmeDatum = new LocalDateFilter();
        }
        return inbetriebnahmeDatum;
    }

    public void setInbetriebnahmeDatum(LocalDateFilter inbetriebnahmeDatum) {
        this.inbetriebnahmeDatum = inbetriebnahmeDatum;
    }

    public LocalDateFilter getKwkAnlageInbetriebnahmeDatum() {
        return kwkAnlageInbetriebnahmeDatum;
    }

    public LocalDateFilter kwkAnlageInbetriebnahmeDatum() {
        if (kwkAnlageInbetriebnahmeDatum == null) {
            kwkAnlageInbetriebnahmeDatum = new LocalDateFilter();
        }
        return kwkAnlageInbetriebnahmeDatum;
    }

    public void setKwkAnlageInbetriebnahmeDatum(LocalDateFilter kwkAnlageInbetriebnahmeDatum) {
        this.kwkAnlageInbetriebnahmeDatum = kwkAnlageInbetriebnahmeDatum;
    }

    public LocalDateFilter getKwkAnlageRegistrierungsDatum() {
        return kwkAnlageRegistrierungsDatum;
    }

    public LocalDateFilter kwkAnlageRegistrierungsDatum() {
        if (kwkAnlageRegistrierungsDatum == null) {
            kwkAnlageRegistrierungsDatum = new LocalDateFilter();
        }
        return kwkAnlageRegistrierungsDatum;
    }

    public void setKwkAnlageRegistrierungsDatum(LocalDateFilter kwkAnlageRegistrierungsDatum) {
        this.kwkAnlageRegistrierungsDatum = kwkAnlageRegistrierungsDatum;
    }

    public LocalDateFilter getEegInbetriebnahmeDatum() {
        return eegInbetriebnahmeDatum;
    }

    public LocalDateFilter eegInbetriebnahmeDatum() {
        if (eegInbetriebnahmeDatum == null) {
            eegInbetriebnahmeDatum = new LocalDateFilter();
        }
        return eegInbetriebnahmeDatum;
    }

    public void setEegInbetriebnahmeDatum(LocalDateFilter eegInbetriebnahmeDatum) {
        this.eegInbetriebnahmeDatum = eegInbetriebnahmeDatum;
    }

    public LocalDateFilter getEegAnlageRegistrierungsDatum() {
        return eegAnlageRegistrierungsDatum;
    }

    public LocalDateFilter eegAnlageRegistrierungsDatum() {
        if (eegAnlageRegistrierungsDatum == null) {
            eegAnlageRegistrierungsDatum = new LocalDateFilter();
        }
        return eegAnlageRegistrierungsDatum;
    }

    public void setEegAnlageRegistrierungsDatum(LocalDateFilter eegAnlageRegistrierungsDatum) {
        this.eegAnlageRegistrierungsDatum = eegAnlageRegistrierungsDatum;
    }

    public LocalDateFilter getGenehmigungDatum() {
        return genehmigungDatum;
    }

    public LocalDateFilter genehmigungDatum() {
        if (genehmigungDatum == null) {
            genehmigungDatum = new LocalDateFilter();
        }
        return genehmigungDatum;
    }

    public void setGenehmigungDatum(LocalDateFilter genehmigungDatum) {
        this.genehmigungDatum = genehmigungDatum;
    }

    public LocalDateFilter getGenehmigungRegistrierungsDatum() {
        return genehmigungRegistrierungsDatum;
    }

    public LocalDateFilter genehmigungRegistrierungsDatum() {
        if (genehmigungRegistrierungsDatum == null) {
            genehmigungRegistrierungsDatum = new LocalDateFilter();
        }
        return genehmigungRegistrierungsDatum;
    }

    public void setGenehmigungRegistrierungsDatum(LocalDateFilter genehmigungRegistrierungsDatum) {
        this.genehmigungRegistrierungsDatum = genehmigungRegistrierungsDatum;
    }

    public IntegerFilter getIsNbPruefungAbgeschlossen() {
        return isNbPruefungAbgeschlossen;
    }

    public IntegerFilter isNbPruefungAbgeschlossen() {
        if (isNbPruefungAbgeschlossen == null) {
            isNbPruefungAbgeschlossen = new IntegerFilter();
        }
        return isNbPruefungAbgeschlossen;
    }

    public void setIsNbPruefungAbgeschlossen(IntegerFilter isNbPruefungAbgeschlossen) {
        this.isNbPruefungAbgeschlossen = isNbPruefungAbgeschlossen;
    }

    public BooleanFilter getIsAnonymisiert() {
        return isAnonymisiert;
    }

    public BooleanFilter isAnonymisiert() {
        if (isAnonymisiert == null) {
            isAnonymisiert = new BooleanFilter();
        }
        return isAnonymisiert;
    }

    public void setIsAnonymisiert(BooleanFilter isAnonymisiert) {
        this.isAnonymisiert = isAnonymisiert;
    }

    public BooleanFilter getIsBuergerEnergie() {
        return isBuergerEnergie;
    }

    public BooleanFilter isBuergerEnergie() {
        if (isBuergerEnergie == null) {
            isBuergerEnergie = new BooleanFilter();
        }
        return isBuergerEnergie;
    }

    public void setIsBuergerEnergie(BooleanFilter isBuergerEnergie) {
        this.isBuergerEnergie = isBuergerEnergie;
    }

    public BooleanFilter getIsEinheitNotstromaggregat() {
        return isEinheitNotstromaggregat;
    }

    public BooleanFilter isEinheitNotstromaggregat() {
        if (isEinheitNotstromaggregat == null) {
            isEinheitNotstromaggregat = new BooleanFilter();
        }
        return isEinheitNotstromaggregat;
    }

    public void setIsEinheitNotstromaggregat(BooleanFilter isEinheitNotstromaggregat) {
        this.isEinheitNotstromaggregat = isEinheitNotstromaggregat;
    }

    public BooleanFilter getIsMieterstromAngemeldet() {
        return isMieterstromAngemeldet;
    }

    public BooleanFilter isMieterstromAngemeldet() {
        if (isMieterstromAngemeldet == null) {
            isMieterstromAngemeldet = new BooleanFilter();
        }
        return isMieterstromAngemeldet;
    }

    public void setIsMieterstromAngemeldet(BooleanFilter isMieterstromAngemeldet) {
        this.isMieterstromAngemeldet = isMieterstromAngemeldet;
    }

    public BooleanFilter getIsWasserkraftErtuechtigung() {
        return isWasserkraftErtuechtigung;
    }

    public BooleanFilter isWasserkraftErtuechtigung() {
        if (isWasserkraftErtuechtigung == null) {
            isWasserkraftErtuechtigung = new BooleanFilter();
        }
        return isWasserkraftErtuechtigung;
    }

    public void setIsWasserkraftErtuechtigung(BooleanFilter isWasserkraftErtuechtigung) {
        this.isWasserkraftErtuechtigung = isWasserkraftErtuechtigung;
    }

    public BooleanFilter getIsPilotWindanlage() {
        return isPilotWindanlage;
    }

    public BooleanFilter isPilotWindanlage() {
        if (isPilotWindanlage == null) {
            isPilotWindanlage = new BooleanFilter();
        }
        return isPilotWindanlage;
    }

    public void setIsPilotWindanlage(BooleanFilter isPilotWindanlage) {
        this.isPilotWindanlage = isPilotWindanlage;
    }

    public BooleanFilter getIsPrototypAnlage() {
        return isPrototypAnlage;
    }

    public BooleanFilter isPrototypAnlage() {
        if (isPrototypAnlage == null) {
            isPrototypAnlage = new BooleanFilter();
        }
        return isPrototypAnlage;
    }

    public void setIsPrototypAnlage(BooleanFilter isPrototypAnlage) {
        this.isPrototypAnlage = isPrototypAnlage;
    }

    public DoubleFilter getLat() {
        return lat;
    }

    public DoubleFilter lat() {
        if (lat == null) {
            lat = new DoubleFilter();
        }
        return lat;
    }

    public void setLat(DoubleFilter lat) {
        this.lat = lat;
    }

    public DoubleFilter getLng() {
        return lng;
    }

    public DoubleFilter lng() {
        if (lng == null) {
            lng = new DoubleFilter();
        }
        return lng;
    }

    public void setLng(DoubleFilter lng) {
        this.lng = lng;
    }

    public StringFilter getOrt() {
        return ort;
    }

    public StringFilter ort() {
        if (ort == null) {
            ort = new StringFilter();
        }
        return ort;
    }

    public void setOrt(StringFilter ort) {
        this.ort = ort;
    }

    public IntegerFilter getPlz() {
        return plz;
    }

    public IntegerFilter plz() {
        if (plz == null) {
            plz = new IntegerFilter();
        }
        return plz;
    }

    public void setPlz(IntegerFilter plz) {
        this.plz = plz;
    }

    public StringFilter getStrasse() {
        return strasse;
    }

    public StringFilter strasse() {
        if (strasse == null) {
            strasse = new StringFilter();
        }
        return strasse;
    }

    public void setStrasse(StringFilter strasse) {
        this.strasse = strasse;
    }

    public StringFilter getHausnummer() {
        return hausnummer;
    }

    public StringFilter hausnummer() {
        if (hausnummer == null) {
            hausnummer = new StringFilter();
        }
        return hausnummer;
    }

    public void setHausnummer(StringFilter hausnummer) {
        this.hausnummer = hausnummer;
    }

    public StringFilter getEinheitname() {
        return einheitname;
    }

    public StringFilter einheitname() {
        if (einheitname == null) {
            einheitname = new StringFilter();
        }
        return einheitname;
    }

    public void setEinheitname(StringFilter einheitname) {
        this.einheitname = einheitname;
    }

    public StringFilter getFlurstueck() {
        return flurstueck;
    }

    public StringFilter flurstueck() {
        if (flurstueck == null) {
            flurstueck = new StringFilter();
        }
        return flurstueck;
    }

    public void setFlurstueck(StringFilter flurstueck) {
        this.flurstueck = flurstueck;
    }

    public StringFilter getGemarkung() {
        return gemarkung;
    }

    public StringFilter gemarkung() {
        if (gemarkung == null) {
            gemarkung = new StringFilter();
        }
        return gemarkung;
    }

    public void setGemarkung(StringFilter gemarkung) {
        this.gemarkung = gemarkung;
    }

    public StringFilter getGemeinde() {
        return gemeinde;
    }

    public StringFilter gemeinde() {
        if (gemeinde == null) {
            gemeinde = new StringFilter();
        }
        return gemeinde;
    }

    public void setGemeinde(StringFilter gemeinde) {
        this.gemeinde = gemeinde;
    }

    public IntegerFilter getLandId() {
        return landId;
    }

    public IntegerFilter landId() {
        if (landId == null) {
            landId = new IntegerFilter();
        }
        return landId;
    }

    public void setLandId(IntegerFilter landId) {
        this.landId = landId;
    }

    public StringFilter getLandkreis() {
        return landkreis;
    }

    public StringFilter landkreis() {
        if (landkreis == null) {
            landkreis = new StringFilter();
        }
        return landkreis;
    }

    public void setLandkreis(StringFilter landkreis) {
        this.landkreis = landkreis;
    }

    public LongFilter getAgs() {
        return ags;
    }

    public LongFilter ags() {
        if (ags == null) {
            ags = new LongFilter();
        }
        return ags;
    }

    public void setAgs(LongFilter ags) {
        this.ags = ags;
    }

    public LongFilter getLokationId() {
        return lokationId;
    }

    public LongFilter lokationId() {
        if (lokationId == null) {
            lokationId = new LongFilter();
        }
        return lokationId;
    }

    public void setLokationId(LongFilter lokationId) {
        this.lokationId = lokationId;
    }

    public StringFilter getLokationMastrNr() {
        return lokationMastrNr;
    }

    public StringFilter lokationMastrNr() {
        if (lokationMastrNr == null) {
            lokationMastrNr = new StringFilter();
        }
        return lokationMastrNr;
    }

    public void setLokationMastrNr(StringFilter lokationMastrNr) {
        this.lokationMastrNr = lokationMastrNr;
    }

    public StringFilter getNetzbetreiberId() {
        return netzbetreiberId;
    }

    public StringFilter netzbetreiberId() {
        if (netzbetreiberId == null) {
            netzbetreiberId = new StringFilter();
        }
        return netzbetreiberId;
    }

    public void setNetzbetreiberId(StringFilter netzbetreiberId) {
        this.netzbetreiberId = netzbetreiberId;
    }

    public StringFilter getNetzbetreiberMaskedNamen() {
        return netzbetreiberMaskedNamen;
    }

    public StringFilter netzbetreiberMaskedNamen() {
        if (netzbetreiberMaskedNamen == null) {
            netzbetreiberMaskedNamen = new StringFilter();
        }
        return netzbetreiberMaskedNamen;
    }

    public void setNetzbetreiberMaskedNamen(StringFilter netzbetreiberMaskedNamen) {
        this.netzbetreiberMaskedNamen = netzbetreiberMaskedNamen;
    }

    public StringFilter getNetzbetreiberMastrNummer() {
        return netzbetreiberMastrNummer;
    }

    public StringFilter netzbetreiberMastrNummer() {
        if (netzbetreiberMastrNummer == null) {
            netzbetreiberMastrNummer = new StringFilter();
        }
        return netzbetreiberMastrNummer;
    }

    public void setNetzbetreiberMastrNummer(StringFilter netzbetreiberMastrNummer) {
        this.netzbetreiberMastrNummer = netzbetreiberMastrNummer;
    }

    public StringFilter getNetzbetreiberNamen() {
        return netzbetreiberNamen;
    }

    public StringFilter netzbetreiberNamen() {
        if (netzbetreiberNamen == null) {
            netzbetreiberNamen = new StringFilter();
        }
        return netzbetreiberNamen;
    }

    public void setNetzbetreiberNamen(StringFilter netzbetreiberNamen) {
        this.netzbetreiberNamen = netzbetreiberNamen;
    }

    public StringFilter getNetzbetreiberPersonenArt() {
        return netzbetreiberPersonenArt;
    }

    public StringFilter netzbetreiberPersonenArt() {
        if (netzbetreiberPersonenArt == null) {
            netzbetreiberPersonenArt = new StringFilter();
        }
        return netzbetreiberPersonenArt;
    }

    public void setNetzbetreiberPersonenArt(StringFilter netzbetreiberPersonenArt) {
        this.netzbetreiberPersonenArt = netzbetreiberPersonenArt;
    }

    public IntegerFilter getSystemStatusId() {
        return systemStatusId;
    }

    public IntegerFilter systemStatusId() {
        if (systemStatusId == null) {
            systemStatusId = new IntegerFilter();
        }
        return systemStatusId;
    }

    public void setSystemStatusId(IntegerFilter systemStatusId) {
        this.systemStatusId = systemStatusId;
    }

    public StringFilter getSystemStatusName() {
        return systemStatusName;
    }

    public StringFilter systemStatusName() {
        if (systemStatusName == null) {
            systemStatusName = new StringFilter();
        }
        return systemStatusName;
    }

    public void setSystemStatusName(StringFilter systemStatusName) {
        this.systemStatusName = systemStatusName;
    }

    public IntegerFilter getTyp() {
        return typ;
    }

    public IntegerFilter typ() {
        if (typ == null) {
            typ = new IntegerFilter();
        }
        return typ;
    }

    public void setTyp(IntegerFilter typ) {
        this.typ = typ;
    }

    public StringFilter getAktenzeichenGenehmigung() {
        return aktenzeichenGenehmigung;
    }

    public StringFilter aktenzeichenGenehmigung() {
        if (aktenzeichenGenehmigung == null) {
            aktenzeichenGenehmigung = new StringFilter();
        }
        return aktenzeichenGenehmigung;
    }

    public void setAktenzeichenGenehmigung(StringFilter aktenzeichenGenehmigung) {
        this.aktenzeichenGenehmigung = aktenzeichenGenehmigung;
    }

    public IntegerFilter getAnzahlSolarmodule() {
        return anzahlSolarmodule;
    }

    public IntegerFilter anzahlSolarmodule() {
        if (anzahlSolarmodule == null) {
            anzahlSolarmodule = new IntegerFilter();
        }
        return anzahlSolarmodule;
    }

    public void setAnzahlSolarmodule(IntegerFilter anzahlSolarmodule) {
        this.anzahlSolarmodule = anzahlSolarmodule;
    }

    public IntegerFilter getBatterieTechnologie() {
        return batterieTechnologie;
    }

    public IntegerFilter batterieTechnologie() {
        if (batterieTechnologie == null) {
            batterieTechnologie = new IntegerFilter();
        }
        return batterieTechnologie;
    }

    public void setBatterieTechnologie(IntegerFilter batterieTechnologie) {
        this.batterieTechnologie = batterieTechnologie;
    }

    public DoubleFilter getBruttoLeistung() {
        return bruttoLeistung;
    }

    public DoubleFilter bruttoLeistung() {
        if (bruttoLeistung == null) {
            bruttoLeistung = new DoubleFilter();
        }
        return bruttoLeistung;
    }

    public void setBruttoLeistung(DoubleFilter bruttoLeistung) {
        this.bruttoLeistung = bruttoLeistung;
    }

    public DoubleFilter getEegInstallierteLeistung() {
        return eegInstallierteLeistung;
    }

    public DoubleFilter eegInstallierteLeistung() {
        if (eegInstallierteLeistung == null) {
            eegInstallierteLeistung = new DoubleFilter();
        }
        return eegInstallierteLeistung;
    }

    public void setEegInstallierteLeistung(DoubleFilter eegInstallierteLeistung) {
        this.eegInstallierteLeistung = eegInstallierteLeistung;
    }

    public StringFilter getEegAnlageMastrNummer() {
        return eegAnlageMastrNummer;
    }

    public StringFilter eegAnlageMastrNummer() {
        if (eegAnlageMastrNummer == null) {
            eegAnlageMastrNummer = new StringFilter();
        }
        return eegAnlageMastrNummer;
    }

    public void setEegAnlageMastrNummer(StringFilter eegAnlageMastrNummer) {
        this.eegAnlageMastrNummer = eegAnlageMastrNummer;
    }

    public StringFilter getEegAnlagenSchluessel() {
        return eegAnlagenSchluessel;
    }

    public StringFilter eegAnlagenSchluessel() {
        if (eegAnlagenSchluessel == null) {
            eegAnlagenSchluessel = new StringFilter();
        }
        return eegAnlagenSchluessel;
    }

    public void setEegAnlagenSchluessel(StringFilter eegAnlagenSchluessel) {
        this.eegAnlagenSchluessel = eegAnlagenSchluessel;
    }

    public StringFilter getEegZuschlag() {
        return eegZuschlag;
    }

    public StringFilter eegZuschlag() {
        if (eegZuschlag == null) {
            eegZuschlag = new StringFilter();
        }
        return eegZuschlag;
    }

    public void setEegZuschlag(StringFilter eegZuschlag) {
        this.eegZuschlag = eegZuschlag;
    }

    public StringFilter getZuschlagsNummern() {
        return zuschlagsNummern;
    }

    public StringFilter zuschlagsNummern() {
        if (zuschlagsNummern == null) {
            zuschlagsNummern = new StringFilter();
        }
        return zuschlagsNummern;
    }

    public void setZuschlagsNummern(StringFilter zuschlagsNummern) {
        this.zuschlagsNummern = zuschlagsNummern;
    }

    public IntegerFilter getEnergieTraegerId() {
        return energieTraegerId;
    }

    public IntegerFilter energieTraegerId() {
        if (energieTraegerId == null) {
            energieTraegerId = new IntegerFilter();
        }
        return energieTraegerId;
    }

    public void setEnergieTraegerId(IntegerFilter energieTraegerId) {
        this.energieTraegerId = energieTraegerId;
    }

    public StringFilter getEnergieTraegerName() {
        return energieTraegerName;
    }

    public StringFilter energieTraegerName() {
        if (energieTraegerName == null) {
            energieTraegerName = new StringFilter();
        }
        return energieTraegerName;
    }

    public void setEnergieTraegerName(StringFilter energieTraegerName) {
        this.energieTraegerName = energieTraegerName;
    }

    public IntegerFilter getGemeinsamerWechselrichter() {
        return gemeinsamerWechselrichter;
    }

    public IntegerFilter gemeinsamerWechselrichter() {
        if (gemeinsamerWechselrichter == null) {
            gemeinsamerWechselrichter = new IntegerFilter();
        }
        return gemeinsamerWechselrichter;
    }

    public void setGemeinsamerWechselrichter(IntegerFilter gemeinsamerWechselrichter) {
        this.gemeinsamerWechselrichter = gemeinsamerWechselrichter;
    }

    public StringFilter getGenehmigungBehoerde() {
        return genehmigungBehoerde;
    }

    public StringFilter genehmigungBehoerde() {
        if (genehmigungBehoerde == null) {
            genehmigungBehoerde = new StringFilter();
        }
        return genehmigungBehoerde;
    }

    public void setGenehmigungBehoerde(StringFilter genehmigungBehoerde) {
        this.genehmigungBehoerde = genehmigungBehoerde;
    }

    public StringFilter getGenehmigungsMastrNummer() {
        return genehmigungsMastrNummer;
    }

    public StringFilter genehmigungsMastrNummer() {
        if (genehmigungsMastrNummer == null) {
            genehmigungsMastrNummer = new StringFilter();
        }
        return genehmigungsMastrNummer;
    }

    public void setGenehmigungsMastrNummer(StringFilter genehmigungsMastrNummer) {
        this.genehmigungsMastrNummer = genehmigungsMastrNummer;
    }

    public StringFilter getGruppierungsObjekte() {
        return gruppierungsObjekte;
    }

    public StringFilter gruppierungsObjekte() {
        if (gruppierungsObjekte == null) {
            gruppierungsObjekte = new StringFilter();
        }
        return gruppierungsObjekte;
    }

    public void setGruppierungsObjekte(StringFilter gruppierungsObjekte) {
        this.gruppierungsObjekte = gruppierungsObjekte;
    }

    public StringFilter getGruppierungsObjekteIds() {
        return gruppierungsObjekteIds;
    }

    public StringFilter gruppierungsObjekteIds() {
        if (gruppierungsObjekteIds == null) {
            gruppierungsObjekteIds = new StringFilter();
        }
        return gruppierungsObjekteIds;
    }

    public void setGruppierungsObjekteIds(StringFilter gruppierungsObjekteIds) {
        this.gruppierungsObjekteIds = gruppierungsObjekteIds;
    }

    public BooleanFilter getHatFlexibilitaetsPraemie() {
        return hatFlexibilitaetsPraemie;
    }

    public BooleanFilter hatFlexibilitaetsPraemie() {
        if (hatFlexibilitaetsPraemie == null) {
            hatFlexibilitaetsPraemie = new BooleanFilter();
        }
        return hatFlexibilitaetsPraemie;
    }

    public void setHatFlexibilitaetsPraemie(BooleanFilter hatFlexibilitaetsPraemie) {
        this.hatFlexibilitaetsPraemie = hatFlexibilitaetsPraemie;
    }

    public IntegerFilter getHauptAusrichtungSolarmodule() {
        return hauptAusrichtungSolarmodule;
    }

    public IntegerFilter hauptAusrichtungSolarmodule() {
        if (hauptAusrichtungSolarmodule == null) {
            hauptAusrichtungSolarmodule = new IntegerFilter();
        }
        return hauptAusrichtungSolarmodule;
    }

    public void setHauptAusrichtungSolarmodule(IntegerFilter hauptAusrichtungSolarmodule) {
        this.hauptAusrichtungSolarmodule = hauptAusrichtungSolarmodule;
    }

    public StringFilter getHauptAusrichtungSolarmoduleBezeichnung() {
        return hauptAusrichtungSolarmoduleBezeichnung;
    }

    public StringFilter hauptAusrichtungSolarmoduleBezeichnung() {
        if (hauptAusrichtungSolarmoduleBezeichnung == null) {
            hauptAusrichtungSolarmoduleBezeichnung = new StringFilter();
        }
        return hauptAusrichtungSolarmoduleBezeichnung;
    }

    public void setHauptAusrichtungSolarmoduleBezeichnung(StringFilter hauptAusrichtungSolarmoduleBezeichnung) {
        this.hauptAusrichtungSolarmoduleBezeichnung = hauptAusrichtungSolarmoduleBezeichnung;
    }

    public IntegerFilter getHauptBrennstoffId() {
        return hauptBrennstoffId;
    }

    public IntegerFilter hauptBrennstoffId() {
        if (hauptBrennstoffId == null) {
            hauptBrennstoffId = new IntegerFilter();
        }
        return hauptBrennstoffId;
    }

    public void setHauptBrennstoffId(IntegerFilter hauptBrennstoffId) {
        this.hauptBrennstoffId = hauptBrennstoffId;
    }

    public StringFilter getHauptBrennstoffNamen() {
        return hauptBrennstoffNamen;
    }

    public StringFilter hauptBrennstoffNamen() {
        if (hauptBrennstoffNamen == null) {
            hauptBrennstoffNamen = new StringFilter();
        }
        return hauptBrennstoffNamen;
    }

    public void setHauptBrennstoffNamen(StringFilter hauptBrennstoffNamen) {
        this.hauptBrennstoffNamen = hauptBrennstoffNamen;
    }

    public IntegerFilter getHauptNeigungswinkelSolarModule() {
        return hauptNeigungswinkelSolarModule;
    }

    public IntegerFilter hauptNeigungswinkelSolarModule() {
        if (hauptNeigungswinkelSolarModule == null) {
            hauptNeigungswinkelSolarModule = new IntegerFilter();
        }
        return hauptNeigungswinkelSolarModule;
    }

    public void setHauptNeigungswinkelSolarModule(IntegerFilter hauptNeigungswinkelSolarModule) {
        this.hauptNeigungswinkelSolarModule = hauptNeigungswinkelSolarModule;
    }

    public IntegerFilter getHerstellerWindenergieanlageId() {
        return herstellerWindenergieanlageId;
    }

    public IntegerFilter herstellerWindenergieanlageId() {
        if (herstellerWindenergieanlageId == null) {
            herstellerWindenergieanlageId = new IntegerFilter();
        }
        return herstellerWindenergieanlageId;
    }

    public void setHerstellerWindenergieanlageId(IntegerFilter herstellerWindenergieanlageId) {
        this.herstellerWindenergieanlageId = herstellerWindenergieanlageId;
    }

    public StringFilter getHerstellerWindenergieanlageBezeichnung() {
        return herstellerWindenergieanlageBezeichnung;
    }

    public StringFilter herstellerWindenergieanlageBezeichnung() {
        if (herstellerWindenergieanlageBezeichnung == null) {
            herstellerWindenergieanlageBezeichnung = new StringFilter();
        }
        return herstellerWindenergieanlageBezeichnung;
    }

    public void setHerstellerWindenergieanlageBezeichnung(StringFilter herstellerWindenergieanlageBezeichnung) {
        this.herstellerWindenergieanlageBezeichnung = herstellerWindenergieanlageBezeichnung;
    }

    public DoubleFilter getKwkAnlageElektrischeLeistung() {
        return kwkAnlageElektrischeLeistung;
    }

    public DoubleFilter kwkAnlageElektrischeLeistung() {
        if (kwkAnlageElektrischeLeistung == null) {
            kwkAnlageElektrischeLeistung = new DoubleFilter();
        }
        return kwkAnlageElektrischeLeistung;
    }

    public void setKwkAnlageElektrischeLeistung(DoubleFilter kwkAnlageElektrischeLeistung) {
        this.kwkAnlageElektrischeLeistung = kwkAnlageElektrischeLeistung;
    }

    public StringFilter getKwkAnlageMastrNummer() {
        return kwkAnlageMastrNummer;
    }

    public StringFilter kwkAnlageMastrNummer() {
        if (kwkAnlageMastrNummer == null) {
            kwkAnlageMastrNummer = new StringFilter();
        }
        return kwkAnlageMastrNummer;
    }

    public void setKwkAnlageMastrNummer(StringFilter kwkAnlageMastrNummer) {
        this.kwkAnlageMastrNummer = kwkAnlageMastrNummer;
    }

    public StringFilter getKwkZuschlag() {
        return kwkZuschlag;
    }

    public StringFilter kwkZuschlag() {
        if (kwkZuschlag == null) {
            kwkZuschlag = new StringFilter();
        }
        return kwkZuschlag;
    }

    public void setKwkZuschlag(StringFilter kwkZuschlag) {
        this.kwkZuschlag = kwkZuschlag;
    }

    public IntegerFilter getLageEinheit() {
        return lageEinheit;
    }

    public IntegerFilter lageEinheit() {
        if (lageEinheit == null) {
            lageEinheit = new IntegerFilter();
        }
        return lageEinheit;
    }

    public void setLageEinheit(IntegerFilter lageEinheit) {
        this.lageEinheit = lageEinheit;
    }

    public StringFilter getLageEinheitBezeichnung() {
        return lageEinheitBezeichnung;
    }

    public StringFilter lageEinheitBezeichnung() {
        if (lageEinheitBezeichnung == null) {
            lageEinheitBezeichnung = new StringFilter();
        }
        return lageEinheitBezeichnung;
    }

    public void setLageEinheitBezeichnung(StringFilter lageEinheitBezeichnung) {
        this.lageEinheitBezeichnung = lageEinheitBezeichnung;
    }

    public IntegerFilter getLeistungsBegrenzung() {
        return leistungsBegrenzung;
    }

    public IntegerFilter leistungsBegrenzung() {
        if (leistungsBegrenzung == null) {
            leistungsBegrenzung = new IntegerFilter();
        }
        return leistungsBegrenzung;
    }

    public void setLeistungsBegrenzung(IntegerFilter leistungsBegrenzung) {
        this.leistungsBegrenzung = leistungsBegrenzung;
    }

    public DoubleFilter getNabenhoeheWindenergieanlage() {
        return nabenhoeheWindenergieanlage;
    }

    public DoubleFilter nabenhoeheWindenergieanlage() {
        if (nabenhoeheWindenergieanlage == null) {
            nabenhoeheWindenergieanlage = new DoubleFilter();
        }
        return nabenhoeheWindenergieanlage;
    }

    public void setNabenhoeheWindenergieanlage(DoubleFilter nabenhoeheWindenergieanlage) {
        this.nabenhoeheWindenergieanlage = nabenhoeheWindenergieanlage;
    }

    public DoubleFilter getRotorDurchmesserWindenergieanlage() {
        return rotorDurchmesserWindenergieanlage;
    }

    public DoubleFilter rotorDurchmesserWindenergieanlage() {
        if (rotorDurchmesserWindenergieanlage == null) {
            rotorDurchmesserWindenergieanlage = new DoubleFilter();
        }
        return rotorDurchmesserWindenergieanlage;
    }

    public void setRotorDurchmesserWindenergieanlage(DoubleFilter rotorDurchmesserWindenergieanlage) {
        this.rotorDurchmesserWindenergieanlage = rotorDurchmesserWindenergieanlage;
    }

    public DoubleFilter getNettoNennLeistung() {
        return nettoNennLeistung;
    }

    public DoubleFilter nettoNennLeistung() {
        if (nettoNennLeistung == null) {
            nettoNennLeistung = new DoubleFilter();
        }
        return nettoNennLeistung;
    }

    public void setNettoNennLeistung(DoubleFilter nettoNennLeistung) {
        this.nettoNennLeistung = nettoNennLeistung;
    }

    public DoubleFilter getNutzbareSpeicherKapazitaet() {
        return nutzbareSpeicherKapazitaet;
    }

    public DoubleFilter nutzbareSpeicherKapazitaet() {
        if (nutzbareSpeicherKapazitaet == null) {
            nutzbareSpeicherKapazitaet = new DoubleFilter();
        }
        return nutzbareSpeicherKapazitaet;
    }

    public void setNutzbareSpeicherKapazitaet(DoubleFilter nutzbareSpeicherKapazitaet) {
        this.nutzbareSpeicherKapazitaet = nutzbareSpeicherKapazitaet;
    }

    public IntegerFilter getNutzungsBereichGebsa() {
        return nutzungsBereichGebsa;
    }

    public IntegerFilter nutzungsBereichGebsa() {
        if (nutzungsBereichGebsa == null) {
            nutzungsBereichGebsa = new IntegerFilter();
        }
        return nutzungsBereichGebsa;
    }

    public void setNutzungsBereichGebsa(IntegerFilter nutzungsBereichGebsa) {
        this.nutzungsBereichGebsa = nutzungsBereichGebsa;
    }

    public StringFilter getStandortAnonymisiert() {
        return standortAnonymisiert;
    }

    public StringFilter standortAnonymisiert() {
        if (standortAnonymisiert == null) {
            standortAnonymisiert = new StringFilter();
        }
        return standortAnonymisiert;
    }

    public void setStandortAnonymisiert(StringFilter standortAnonymisiert) {
        this.standortAnonymisiert = standortAnonymisiert;
    }

    public StringFilter getSpannungsEbenenId() {
        return spannungsEbenenId;
    }

    public StringFilter spannungsEbenenId() {
        if (spannungsEbenenId == null) {
            spannungsEbenenId = new StringFilter();
        }
        return spannungsEbenenId;
    }

    public void setSpannungsEbenenId(StringFilter spannungsEbenenId) {
        this.spannungsEbenenId = spannungsEbenenId;
    }

    public StringFilter getSpannungsEbenenNamen() {
        return spannungsEbenenNamen;
    }

    public StringFilter spannungsEbenenNamen() {
        if (spannungsEbenenNamen == null) {
            spannungsEbenenNamen = new StringFilter();
        }
        return spannungsEbenenNamen;
    }

    public void setSpannungsEbenenNamen(StringFilter spannungsEbenenNamen) {
        this.spannungsEbenenNamen = spannungsEbenenNamen;
    }

    public StringFilter getSpeicherEinheitMastrNummer() {
        return speicherEinheitMastrNummer;
    }

    public StringFilter speicherEinheitMastrNummer() {
        if (speicherEinheitMastrNummer == null) {
            speicherEinheitMastrNummer = new StringFilter();
        }
        return speicherEinheitMastrNummer;
    }

    public void setSpeicherEinheitMastrNummer(StringFilter speicherEinheitMastrNummer) {
        this.speicherEinheitMastrNummer = speicherEinheitMastrNummer;
    }

    public IntegerFilter getTechnologieStromerzeugungId() {
        return technologieStromerzeugungId;
    }

    public IntegerFilter technologieStromerzeugungId() {
        if (technologieStromerzeugungId == null) {
            technologieStromerzeugungId = new IntegerFilter();
        }
        return technologieStromerzeugungId;
    }

    public void setTechnologieStromerzeugungId(IntegerFilter technologieStromerzeugungId) {
        this.technologieStromerzeugungId = technologieStromerzeugungId;
    }

    public StringFilter getTechnologieStromerzeugung() {
        return technologieStromerzeugung;
    }

    public StringFilter technologieStromerzeugung() {
        if (technologieStromerzeugung == null) {
            technologieStromerzeugung = new StringFilter();
        }
        return technologieStromerzeugung;
    }

    public void setTechnologieStromerzeugung(StringFilter technologieStromerzeugung) {
        this.technologieStromerzeugung = technologieStromerzeugung;
    }

    public DoubleFilter getThermischeNutzLeistung() {
        return thermischeNutzLeistung;
    }

    public DoubleFilter thermischeNutzLeistung() {
        if (thermischeNutzLeistung == null) {
            thermischeNutzLeistung = new DoubleFilter();
        }
        return thermischeNutzLeistung;
    }

    public void setThermischeNutzLeistung(DoubleFilter thermischeNutzLeistung) {
        this.thermischeNutzLeistung = thermischeNutzLeistung;
    }

    public StringFilter getTypenBezeichnung() {
        return typenBezeichnung;
    }

    public StringFilter typenBezeichnung() {
        if (typenBezeichnung == null) {
            typenBezeichnung = new StringFilter();
        }
        return typenBezeichnung;
    }

    public void setTypenBezeichnung(StringFilter typenBezeichnung) {
        this.typenBezeichnung = typenBezeichnung;
    }

    public IntegerFilter getVollTeilEinspeisung() {
        return vollTeilEinspeisung;
    }

    public IntegerFilter vollTeilEinspeisung() {
        if (vollTeilEinspeisung == null) {
            vollTeilEinspeisung = new IntegerFilter();
        }
        return vollTeilEinspeisung;
    }

    public void setVollTeilEinspeisung(IntegerFilter vollTeilEinspeisung) {
        this.vollTeilEinspeisung = vollTeilEinspeisung;
    }

    public StringFilter getVollTeilEinspeisungBezeichnung() {
        return vollTeilEinspeisungBezeichnung;
    }

    public StringFilter vollTeilEinspeisungBezeichnung() {
        if (vollTeilEinspeisungBezeichnung == null) {
            vollTeilEinspeisungBezeichnung = new StringFilter();
        }
        return vollTeilEinspeisungBezeichnung;
    }

    public void setVollTeilEinspeisungBezeichnung(StringFilter vollTeilEinspeisungBezeichnung) {
        this.vollTeilEinspeisungBezeichnung = vollTeilEinspeisungBezeichnung;
    }

    public StringFilter getWindparkName() {
        return windparkName;
    }

    public StringFilter windparkName() {
        if (windparkName == null) {
            windparkName = new StringFilter();
        }
        return windparkName;
    }

    public void setWindparkName(StringFilter windparkName) {
        this.windparkName = windparkName;
    }

    public LongFilter getAdministrationUnitId() {
        return administrationUnitId;
    }

    public LongFilter administrationUnitId() {
        if (administrationUnitId == null) {
            administrationUnitId = new LongFilter();
        }
        return administrationUnitId;
    }

    public void setAdministrationUnitId(LongFilter administrationUnitId) {
        this.administrationUnitId = administrationUnitId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MastrUnitCriteria that = (MastrUnitCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(mastrNummer, that.mastrNummer) &&
            Objects.equals(anlagenBetreiberId, that.anlagenBetreiberId) &&
            Objects.equals(anlagenBetreiberPersonenArt, that.anlagenBetreiberPersonenArt) &&
            Objects.equals(anlagenBetreiberMaskedName, that.anlagenBetreiberMaskedName) &&
            Objects.equals(anlagenBetreiberMastrNummer, that.anlagenBetreiberMastrNummer) &&
            Objects.equals(anlagenBetreiberName, that.anlagenBetreiberName) &&
            Objects.equals(betriebsStatusId, that.betriebsStatusId) &&
            Objects.equals(betriebsStatusName, that.betriebsStatusName) &&
            Objects.equals(bundesland, that.bundesland) &&
            Objects.equals(bundeslandId, that.bundeslandId) &&
            Objects.equals(landkreisId, that.landkreisId) &&
            Objects.equals(gemeindeId, that.gemeindeId) &&
            Objects.equals(datumLetzteAktualisierung, that.datumLetzteAktualisierung) &&
            Objects.equals(einheitRegistrierungsDatum, that.einheitRegistrierungsDatum) &&
            Objects.equals(endgueltigeStilllegungDatum, that.endgueltigeStilllegungDatum) &&
            Objects.equals(geplantesInbetriebsnahmeDatum, that.geplantesInbetriebsnahmeDatum) &&
            Objects.equals(inbetriebnahmeDatum, that.inbetriebnahmeDatum) &&
            Objects.equals(kwkAnlageInbetriebnahmeDatum, that.kwkAnlageInbetriebnahmeDatum) &&
            Objects.equals(kwkAnlageRegistrierungsDatum, that.kwkAnlageRegistrierungsDatum) &&
            Objects.equals(eegInbetriebnahmeDatum, that.eegInbetriebnahmeDatum) &&
            Objects.equals(eegAnlageRegistrierungsDatum, that.eegAnlageRegistrierungsDatum) &&
            Objects.equals(genehmigungDatum, that.genehmigungDatum) &&
            Objects.equals(genehmigungRegistrierungsDatum, that.genehmigungRegistrierungsDatum) &&
            Objects.equals(isNbPruefungAbgeschlossen, that.isNbPruefungAbgeschlossen) &&
            Objects.equals(isAnonymisiert, that.isAnonymisiert) &&
            Objects.equals(isBuergerEnergie, that.isBuergerEnergie) &&
            Objects.equals(isEinheitNotstromaggregat, that.isEinheitNotstromaggregat) &&
            Objects.equals(isMieterstromAngemeldet, that.isMieterstromAngemeldet) &&
            Objects.equals(isWasserkraftErtuechtigung, that.isWasserkraftErtuechtigung) &&
            Objects.equals(isPilotWindanlage, that.isPilotWindanlage) &&
            Objects.equals(isPrototypAnlage, that.isPrototypAnlage) &&
            Objects.equals(lat, that.lat) &&
            Objects.equals(lng, that.lng) &&
            Objects.equals(ort, that.ort) &&
            Objects.equals(plz, that.plz) &&
            Objects.equals(strasse, that.strasse) &&
            Objects.equals(hausnummer, that.hausnummer) &&
            Objects.equals(einheitname, that.einheitname) &&
            Objects.equals(flurstueck, that.flurstueck) &&
            Objects.equals(gemarkung, that.gemarkung) &&
            Objects.equals(gemeinde, that.gemeinde) &&
            Objects.equals(landId, that.landId) &&
            Objects.equals(landkreis, that.landkreis) &&
            Objects.equals(ags, that.ags) &&
            Objects.equals(lokationId, that.lokationId) &&
            Objects.equals(lokationMastrNr, that.lokationMastrNr) &&
            Objects.equals(netzbetreiberId, that.netzbetreiberId) &&
            Objects.equals(netzbetreiberMaskedNamen, that.netzbetreiberMaskedNamen) &&
            Objects.equals(netzbetreiberMastrNummer, that.netzbetreiberMastrNummer) &&
            Objects.equals(netzbetreiberNamen, that.netzbetreiberNamen) &&
            Objects.equals(netzbetreiberPersonenArt, that.netzbetreiberPersonenArt) &&
            Objects.equals(systemStatusId, that.systemStatusId) &&
            Objects.equals(systemStatusName, that.systemStatusName) &&
            Objects.equals(typ, that.typ) &&
            Objects.equals(aktenzeichenGenehmigung, that.aktenzeichenGenehmigung) &&
            Objects.equals(anzahlSolarmodule, that.anzahlSolarmodule) &&
            Objects.equals(batterieTechnologie, that.batterieTechnologie) &&
            Objects.equals(bruttoLeistung, that.bruttoLeistung) &&
            Objects.equals(eegInstallierteLeistung, that.eegInstallierteLeistung) &&
            Objects.equals(eegAnlageMastrNummer, that.eegAnlageMastrNummer) &&
            Objects.equals(eegAnlagenSchluessel, that.eegAnlagenSchluessel) &&
            Objects.equals(eegZuschlag, that.eegZuschlag) &&
            Objects.equals(zuschlagsNummern, that.zuschlagsNummern) &&
            Objects.equals(energieTraegerId, that.energieTraegerId) &&
            Objects.equals(energieTraegerName, that.energieTraegerName) &&
            Objects.equals(gemeinsamerWechselrichter, that.gemeinsamerWechselrichter) &&
            Objects.equals(genehmigungBehoerde, that.genehmigungBehoerde) &&
            Objects.equals(genehmigungsMastrNummer, that.genehmigungsMastrNummer) &&
            Objects.equals(gruppierungsObjekte, that.gruppierungsObjekte) &&
            Objects.equals(gruppierungsObjekteIds, that.gruppierungsObjekteIds) &&
            Objects.equals(hatFlexibilitaetsPraemie, that.hatFlexibilitaetsPraemie) &&
            Objects.equals(hauptAusrichtungSolarmodule, that.hauptAusrichtungSolarmodule) &&
            Objects.equals(hauptAusrichtungSolarmoduleBezeichnung, that.hauptAusrichtungSolarmoduleBezeichnung) &&
            Objects.equals(hauptBrennstoffId, that.hauptBrennstoffId) &&
            Objects.equals(hauptBrennstoffNamen, that.hauptBrennstoffNamen) &&
            Objects.equals(hauptNeigungswinkelSolarModule, that.hauptNeigungswinkelSolarModule) &&
            Objects.equals(herstellerWindenergieanlageId, that.herstellerWindenergieanlageId) &&
            Objects.equals(herstellerWindenergieanlageBezeichnung, that.herstellerWindenergieanlageBezeichnung) &&
            Objects.equals(kwkAnlageElektrischeLeistung, that.kwkAnlageElektrischeLeistung) &&
            Objects.equals(kwkAnlageMastrNummer, that.kwkAnlageMastrNummer) &&
            Objects.equals(kwkZuschlag, that.kwkZuschlag) &&
            Objects.equals(lageEinheit, that.lageEinheit) &&
            Objects.equals(lageEinheitBezeichnung, that.lageEinheitBezeichnung) &&
            Objects.equals(leistungsBegrenzung, that.leistungsBegrenzung) &&
            Objects.equals(nabenhoeheWindenergieanlage, that.nabenhoeheWindenergieanlage) &&
            Objects.equals(rotorDurchmesserWindenergieanlage, that.rotorDurchmesserWindenergieanlage) &&
            Objects.equals(nettoNennLeistung, that.nettoNennLeistung) &&
            Objects.equals(nutzbareSpeicherKapazitaet, that.nutzbareSpeicherKapazitaet) &&
            Objects.equals(nutzungsBereichGebsa, that.nutzungsBereichGebsa) &&
            Objects.equals(standortAnonymisiert, that.standortAnonymisiert) &&
            Objects.equals(spannungsEbenenId, that.spannungsEbenenId) &&
            Objects.equals(spannungsEbenenNamen, that.spannungsEbenenNamen) &&
            Objects.equals(speicherEinheitMastrNummer, that.speicherEinheitMastrNummer) &&
            Objects.equals(technologieStromerzeugungId, that.technologieStromerzeugungId) &&
            Objects.equals(technologieStromerzeugung, that.technologieStromerzeugung) &&
            Objects.equals(thermischeNutzLeistung, that.thermischeNutzLeistung) &&
            Objects.equals(typenBezeichnung, that.typenBezeichnung) &&
            Objects.equals(vollTeilEinspeisung, that.vollTeilEinspeisung) &&
            Objects.equals(vollTeilEinspeisungBezeichnung, that.vollTeilEinspeisungBezeichnung) &&
            Objects.equals(windparkName, that.windparkName) &&
            Objects.equals(administrationUnitId, that.administrationUnitId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            mastrNummer,
            anlagenBetreiberId,
            anlagenBetreiberPersonenArt,
            anlagenBetreiberMaskedName,
            anlagenBetreiberMastrNummer,
            anlagenBetreiberName,
            betriebsStatusId,
            betriebsStatusName,
            bundesland,
            bundeslandId,
            landkreisId,
            gemeindeId,
            datumLetzteAktualisierung,
            einheitRegistrierungsDatum,
            endgueltigeStilllegungDatum,
            geplantesInbetriebsnahmeDatum,
            inbetriebnahmeDatum,
            kwkAnlageInbetriebnahmeDatum,
            kwkAnlageRegistrierungsDatum,
            eegInbetriebnahmeDatum,
            eegAnlageRegistrierungsDatum,
            genehmigungDatum,
            genehmigungRegistrierungsDatum,
            isNbPruefungAbgeschlossen,
            isAnonymisiert,
            isBuergerEnergie,
            isEinheitNotstromaggregat,
            isMieterstromAngemeldet,
            isWasserkraftErtuechtigung,
            isPilotWindanlage,
            isPrototypAnlage,
            lat,
            lng,
            ort,
            plz,
            strasse,
            hausnummer,
            einheitname,
            flurstueck,
            gemarkung,
            gemeinde,
            landId,
            landkreis,
            ags,
            lokationId,
            lokationMastrNr,
            netzbetreiberId,
            netzbetreiberMaskedNamen,
            netzbetreiberMastrNummer,
            netzbetreiberNamen,
            netzbetreiberPersonenArt,
            systemStatusId,
            systemStatusName,
            typ,
            aktenzeichenGenehmigung,
            anzahlSolarmodule,
            batterieTechnologie,
            bruttoLeistung,
            eegInstallierteLeistung,
            eegAnlageMastrNummer,
            eegAnlagenSchluessel,
            eegZuschlag,
            zuschlagsNummern,
            energieTraegerId,
            energieTraegerName,
            gemeinsamerWechselrichter,
            genehmigungBehoerde,
            genehmigungsMastrNummer,
            gruppierungsObjekte,
            gruppierungsObjekteIds,
            hatFlexibilitaetsPraemie,
            hauptAusrichtungSolarmodule,
            hauptAusrichtungSolarmoduleBezeichnung,
            hauptBrennstoffId,
            hauptBrennstoffNamen,
            hauptNeigungswinkelSolarModule,
            herstellerWindenergieanlageId,
            herstellerWindenergieanlageBezeichnung,
            kwkAnlageElektrischeLeistung,
            kwkAnlageMastrNummer,
            kwkZuschlag,
            lageEinheit,
            lageEinheitBezeichnung,
            leistungsBegrenzung,
            nabenhoeheWindenergieanlage,
            rotorDurchmesserWindenergieanlage,
            nettoNennLeistung,
            nutzbareSpeicherKapazitaet,
            nutzungsBereichGebsa,
            standortAnonymisiert,
            spannungsEbenenId,
            spannungsEbenenNamen,
            speicherEinheitMastrNummer,
            technologieStromerzeugungId,
            technologieStromerzeugung,
            thermischeNutzLeistung,
            typenBezeichnung,
            vollTeilEinspeisung,
            vollTeilEinspeisungBezeichnung,
            windparkName,
            administrationUnitId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MastrUnitCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (mastrNummer != null ? "mastrNummer=" + mastrNummer + ", " : "") +
            (anlagenBetreiberId != null ? "anlagenBetreiberId=" + anlagenBetreiberId + ", " : "") +
            (anlagenBetreiberPersonenArt != null ? "anlagenBetreiberPersonenArt=" + anlagenBetreiberPersonenArt + ", " : "") +
            (anlagenBetreiberMaskedName != null ? "anlagenBetreiberMaskedName=" + anlagenBetreiberMaskedName + ", " : "") +
            (anlagenBetreiberMastrNummer != null ? "anlagenBetreiberMastrNummer=" + anlagenBetreiberMastrNummer + ", " : "") +
            (anlagenBetreiberName != null ? "anlagenBetreiberName=" + anlagenBetreiberName + ", " : "") +
            (betriebsStatusId != null ? "betriebsStatusId=" + betriebsStatusId + ", " : "") +
            (betriebsStatusName != null ? "betriebsStatusName=" + betriebsStatusName + ", " : "") +
            (bundesland != null ? "bundesland=" + bundesland + ", " : "") +
            (bundeslandId != null ? "bundeslandId=" + bundeslandId + ", " : "") +
            (landkreisId != null ? "landkreisId=" + landkreisId + ", " : "") +
            (gemeindeId != null ? "gemeindeId=" + gemeindeId + ", " : "") +
            (datumLetzteAktualisierung != null ? "datumLetzteAktualisierung=" + datumLetzteAktualisierung + ", " : "") +
            (einheitRegistrierungsDatum != null ? "einheitRegistrierungsDatum=" + einheitRegistrierungsDatum + ", " : "") +
            (endgueltigeStilllegungDatum != null ? "endgueltigeStilllegungDatum=" + endgueltigeStilllegungDatum + ", " : "") +
            (geplantesInbetriebsnahmeDatum != null ? "geplantesInbetriebsnahmeDatum=" + geplantesInbetriebsnahmeDatum + ", " : "") +
            (inbetriebnahmeDatum != null ? "inbetriebnahmeDatum=" + inbetriebnahmeDatum + ", " : "") +
            (kwkAnlageInbetriebnahmeDatum != null ? "kwkAnlageInbetriebnahmeDatum=" + kwkAnlageInbetriebnahmeDatum + ", " : "") +
            (kwkAnlageRegistrierungsDatum != null ? "kwkAnlageRegistrierungsDatum=" + kwkAnlageRegistrierungsDatum + ", " : "") +
            (eegInbetriebnahmeDatum != null ? "eegInbetriebnahmeDatum=" + eegInbetriebnahmeDatum + ", " : "") +
            (eegAnlageRegistrierungsDatum != null ? "eegAnlageRegistrierungsDatum=" + eegAnlageRegistrierungsDatum + ", " : "") +
            (genehmigungDatum != null ? "genehmigungDatum=" + genehmigungDatum + ", " : "") +
            (genehmigungRegistrierungsDatum != null ? "genehmigungRegistrierungsDatum=" + genehmigungRegistrierungsDatum + ", " : "") +
            (isNbPruefungAbgeschlossen != null ? "isNbPruefungAbgeschlossen=" + isNbPruefungAbgeschlossen + ", " : "") +
            (isAnonymisiert != null ? "isAnonymisiert=" + isAnonymisiert + ", " : "") +
            (isBuergerEnergie != null ? "isBuergerEnergie=" + isBuergerEnergie + ", " : "") +
            (isEinheitNotstromaggregat != null ? "isEinheitNotstromaggregat=" + isEinheitNotstromaggregat + ", " : "") +
            (isMieterstromAngemeldet != null ? "isMieterstromAngemeldet=" + isMieterstromAngemeldet + ", " : "") +
            (isWasserkraftErtuechtigung != null ? "isWasserkraftErtuechtigung=" + isWasserkraftErtuechtigung + ", " : "") +
            (isPilotWindanlage != null ? "isPilotWindanlage=" + isPilotWindanlage + ", " : "") +
            (isPrototypAnlage != null ? "isPrototypAnlage=" + isPrototypAnlage + ", " : "") +
            (lat != null ? "lat=" + lat + ", " : "") +
            (lng != null ? "lng=" + lng + ", " : "") +
            (ort != null ? "ort=" + ort + ", " : "") +
            (plz != null ? "plz=" + plz + ", " : "") +
            (strasse != null ? "strasse=" + strasse + ", " : "") +
            (hausnummer != null ? "hausnummer=" + hausnummer + ", " : "") +
            (einheitname != null ? "einheitname=" + einheitname + ", " : "") +
            (flurstueck != null ? "flurstueck=" + flurstueck + ", " : "") +
            (gemarkung != null ? "gemarkung=" + gemarkung + ", " : "") +
            (gemeinde != null ? "gemeinde=" + gemeinde + ", " : "") +
            (landId != null ? "landId=" + landId + ", " : "") +
            (landkreis != null ? "landkreis=" + landkreis + ", " : "") +
            (ags != null ? "ags=" + ags + ", " : "") +
            (lokationId != null ? "lokationId=" + lokationId + ", " : "") +
            (lokationMastrNr != null ? "lokationMastrNr=" + lokationMastrNr + ", " : "") +
            (netzbetreiberId != null ? "netzbetreiberId=" + netzbetreiberId + ", " : "") +
            (netzbetreiberMaskedNamen != null ? "netzbetreiberMaskedNamen=" + netzbetreiberMaskedNamen + ", " : "") +
            (netzbetreiberMastrNummer != null ? "netzbetreiberMastrNummer=" + netzbetreiberMastrNummer + ", " : "") +
            (netzbetreiberNamen != null ? "netzbetreiberNamen=" + netzbetreiberNamen + ", " : "") +
            (netzbetreiberPersonenArt != null ? "netzbetreiberPersonenArt=" + netzbetreiberPersonenArt + ", " : "") +
            (systemStatusId != null ? "systemStatusId=" + systemStatusId + ", " : "") +
            (systemStatusName != null ? "systemStatusName=" + systemStatusName + ", " : "") +
            (typ != null ? "typ=" + typ + ", " : "") +
            (aktenzeichenGenehmigung != null ? "aktenzeichenGenehmigung=" + aktenzeichenGenehmigung + ", " : "") +
            (anzahlSolarmodule != null ? "anzahlSolarmodule=" + anzahlSolarmodule + ", " : "") +
            (batterieTechnologie != null ? "batterieTechnologie=" + batterieTechnologie + ", " : "") +
            (bruttoLeistung != null ? "bruttoLeistung=" + bruttoLeistung + ", " : "") +
            (eegInstallierteLeistung != null ? "eegInstallierteLeistung=" + eegInstallierteLeistung + ", " : "") +
            (eegAnlageMastrNummer != null ? "eegAnlageMastrNummer=" + eegAnlageMastrNummer + ", " : "") +
            (eegAnlagenSchluessel != null ? "eegAnlagenSchluessel=" + eegAnlagenSchluessel + ", " : "") +
            (eegZuschlag != null ? "eegZuschlag=" + eegZuschlag + ", " : "") +
            (zuschlagsNummern != null ? "zuschlagsNummern=" + zuschlagsNummern + ", " : "") +
            (energieTraegerId != null ? "energieTraegerId=" + energieTraegerId + ", " : "") +
            (energieTraegerName != null ? "energieTraegerName=" + energieTraegerName + ", " : "") +
            (gemeinsamerWechselrichter != null ? "gemeinsamerWechselrichter=" + gemeinsamerWechselrichter + ", " : "") +
            (genehmigungBehoerde != null ? "genehmigungBehoerde=" + genehmigungBehoerde + ", " : "") +
            (genehmigungsMastrNummer != null ? "genehmigungsMastrNummer=" + genehmigungsMastrNummer + ", " : "") +
            (gruppierungsObjekte != null ? "gruppierungsObjekte=" + gruppierungsObjekte + ", " : "") +
            (gruppierungsObjekteIds != null ? "gruppierungsObjekteIds=" + gruppierungsObjekteIds + ", " : "") +
            (hatFlexibilitaetsPraemie != null ? "hatFlexibilitaetsPraemie=" + hatFlexibilitaetsPraemie + ", " : "") +
            (hauptAusrichtungSolarmodule != null ? "hauptAusrichtungSolarmodule=" + hauptAusrichtungSolarmodule + ", " : "") +
            (hauptAusrichtungSolarmoduleBezeichnung != null ? "hauptAusrichtungSolarmoduleBezeichnung=" + hauptAusrichtungSolarmoduleBezeichnung + ", " : "") +
            (hauptBrennstoffId != null ? "hauptBrennstoffId=" + hauptBrennstoffId + ", " : "") +
            (hauptBrennstoffNamen != null ? "hauptBrennstoffNamen=" + hauptBrennstoffNamen + ", " : "") +
            (hauptNeigungswinkelSolarModule != null ? "hauptNeigungswinkelSolarModule=" + hauptNeigungswinkelSolarModule + ", " : "") +
            (herstellerWindenergieanlageId != null ? "herstellerWindenergieanlageId=" + herstellerWindenergieanlageId + ", " : "") +
            (herstellerWindenergieanlageBezeichnung != null ? "herstellerWindenergieanlageBezeichnung=" + herstellerWindenergieanlageBezeichnung + ", " : "") +
            (kwkAnlageElektrischeLeistung != null ? "kwkAnlageElektrischeLeistung=" + kwkAnlageElektrischeLeistung + ", " : "") +
            (kwkAnlageMastrNummer != null ? "kwkAnlageMastrNummer=" + kwkAnlageMastrNummer + ", " : "") +
            (kwkZuschlag != null ? "kwkZuschlag=" + kwkZuschlag + ", " : "") +
            (lageEinheit != null ? "lageEinheit=" + lageEinheit + ", " : "") +
            (lageEinheitBezeichnung != null ? "lageEinheitBezeichnung=" + lageEinheitBezeichnung + ", " : "") +
            (leistungsBegrenzung != null ? "leistungsBegrenzung=" + leistungsBegrenzung + ", " : "") +
            (nabenhoeheWindenergieanlage != null ? "nabenhoeheWindenergieanlage=" + nabenhoeheWindenergieanlage + ", " : "") +
            (rotorDurchmesserWindenergieanlage != null ? "rotorDurchmesserWindenergieanlage=" + rotorDurchmesserWindenergieanlage + ", " : "") +
            (nettoNennLeistung != null ? "nettoNennLeistung=" + nettoNennLeistung + ", " : "") +
            (nutzbareSpeicherKapazitaet != null ? "nutzbareSpeicherKapazitaet=" + nutzbareSpeicherKapazitaet + ", " : "") +
            (nutzungsBereichGebsa != null ? "nutzungsBereichGebsa=" + nutzungsBereichGebsa + ", " : "") +
            (standortAnonymisiert != null ? "standortAnonymisiert=" + standortAnonymisiert + ", " : "") +
            (spannungsEbenenId != null ? "spannungsEbenenId=" + spannungsEbenenId + ", " : "") +
            (spannungsEbenenNamen != null ? "spannungsEbenenNamen=" + spannungsEbenenNamen + ", " : "") +
            (speicherEinheitMastrNummer != null ? "speicherEinheitMastrNummer=" + speicherEinheitMastrNummer + ", " : "") +
            (technologieStromerzeugungId != null ? "technologieStromerzeugungId=" + technologieStromerzeugungId + ", " : "") +
            (technologieStromerzeugung != null ? "technologieStromerzeugung=" + technologieStromerzeugung + ", " : "") +
            (thermischeNutzLeistung != null ? "thermischeNutzLeistung=" + thermischeNutzLeistung + ", " : "") +
            (typenBezeichnung != null ? "typenBezeichnung=" + typenBezeichnung + ", " : "") +
            (vollTeilEinspeisung != null ? "vollTeilEinspeisung=" + vollTeilEinspeisung + ", " : "") +
            (vollTeilEinspeisungBezeichnung != null ? "vollTeilEinspeisungBezeichnung=" + vollTeilEinspeisungBezeichnung + ", " : "") +
            (windparkName != null ? "windparkName=" + windparkName + ", " : "") +
            (administrationUnitId != null ? "administrationUnitId=" + administrationUnitId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
