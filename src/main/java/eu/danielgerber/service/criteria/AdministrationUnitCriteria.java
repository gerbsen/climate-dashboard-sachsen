package eu.danielgerber.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.*;

/**
 * Criteria class for the {@link eu.danielgerber.domain.AdministrationUnit} entity. This class is used
 * in {@link eu.danielgerber.web.rest.AdministrationUnitResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /administration-units?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AdministrationUnitCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter objektidentifikator;

    private LocalDateFilter beginnlebenszeit;

    private StringFilter adminEbeneAde;

    private IntegerFilter ade;

    private StringFilter geofaktorGf;

    private LongFilter gf;

    private StringFilter besondereGebieteBsg;

    private LongFilter bsg;

    private StringFilter regionalschluesselArs;

    private StringFilter gemeindeschluesselAgs;

    private StringFilter verwaltungssitzSdvArs;

    private StringFilter geografischernameGen;

    private StringFilter bezeichnung;

    private LongFilter identifikatorIbz;

    private StringFilter bemerkung;

    private StringFilter namensbildungNbd;

    private BooleanFilter nbd;

    private StringFilter land;

    private StringFilter regierungsbezirk;

    private StringFilter kreis;

    private StringFilter verwaltungsgemeinschaftteil1;

    private StringFilter verwaltungsgemeinschaftteil2;

    private StringFilter gemeinde;

    private StringFilter funkSchluesselstelle3;

    private StringFilter fkS3;

    private StringFilter europStatistikschluesselNuts;

    private StringFilter regioschluesselaufgefuellt;

    private StringFilter gemeindeschluesselaufgefuellt;

    private LocalDateFilter wirksamkeitWsk;


    private LongFilter districtId;

    private LongFilter mastrUnitId;

    private LongFilter chargingStationId;

    private LongFilter statisticId;

    private Boolean distinct;

    public AdministrationUnitCriteria() {}

    public AdministrationUnitCriteria(AdministrationUnitCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.objektidentifikator = other.objektidentifikator == null ? null : other.objektidentifikator.copy();
        this.beginnlebenszeit = other.beginnlebenszeit == null ? null : other.beginnlebenszeit.copy();
        this.adminEbeneAde = other.adminEbeneAde == null ? null : other.adminEbeneAde.copy();
        this.ade = other.ade == null ? null : other.ade.copy();
        this.geofaktorGf = other.geofaktorGf == null ? null : other.geofaktorGf.copy();
        this.gf = other.gf == null ? null : other.gf.copy();
        this.besondereGebieteBsg = other.besondereGebieteBsg == null ? null : other.besondereGebieteBsg.copy();
        this.bsg = other.bsg == null ? null : other.bsg.copy();
        this.regionalschluesselArs = other.regionalschluesselArs == null ? null : other.regionalschluesselArs.copy();
        this.gemeindeschluesselAgs = other.gemeindeschluesselAgs == null ? null : other.gemeindeschluesselAgs.copy();
        this.verwaltungssitzSdvArs = other.verwaltungssitzSdvArs == null ? null : other.verwaltungssitzSdvArs.copy();
        this.geografischernameGen = other.geografischernameGen == null ? null : other.geografischernameGen.copy();
        this.bezeichnung = other.bezeichnung == null ? null : other.bezeichnung.copy();
        this.identifikatorIbz = other.identifikatorIbz == null ? null : other.identifikatorIbz.copy();
        this.bemerkung = other.bemerkung == null ? null : other.bemerkung.copy();
        this.namensbildungNbd = other.namensbildungNbd == null ? null : other.namensbildungNbd.copy();
        this.nbd = other.nbd == null ? null : other.nbd.copy();
        this.land = other.land == null ? null : other.land.copy();
        this.regierungsbezirk = other.regierungsbezirk == null ? null : other.regierungsbezirk.copy();
        this.kreis = other.kreis == null ? null : other.kreis.copy();
        this.verwaltungsgemeinschaftteil1 = other.verwaltungsgemeinschaftteil1 == null ? null : other.verwaltungsgemeinschaftteil1.copy();
        this.verwaltungsgemeinschaftteil2 = other.verwaltungsgemeinschaftteil2 == null ? null : other.verwaltungsgemeinschaftteil2.copy();
        this.gemeinde = other.gemeinde == null ? null : other.gemeinde.copy();
        this.funkSchluesselstelle3 = other.funkSchluesselstelle3 == null ? null : other.funkSchluesselstelle3.copy();
        this.fkS3 = other.fkS3 == null ? null : other.fkS3.copy();
        this.europStatistikschluesselNuts = other.europStatistikschluesselNuts == null ? null : other.europStatistikschluesselNuts.copy();
        this.regioschluesselaufgefuellt = other.regioschluesselaufgefuellt == null ? null : other.regioschluesselaufgefuellt.copy();
        this.gemeindeschluesselaufgefuellt =
            other.gemeindeschluesselaufgefuellt == null ? null : other.gemeindeschluesselaufgefuellt.copy();
        this.wirksamkeitWsk = other.wirksamkeitWsk == null ? null : other.wirksamkeitWsk.copy();
        this.districtId = other.districtId == null ? null : other.districtId.copy();
        this.mastrUnitId = other.mastrUnitId == null ? null : other.mastrUnitId.copy();
        this.chargingStationId = other.chargingStationId == null ? null : other.chargingStationId.copy();
        this.statisticId = other.statisticId == null ? null : other.statisticId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public AdministrationUnitCriteria copy() {
        return new AdministrationUnitCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getObjektidentifikator() {
        return objektidentifikator;
    }

    public StringFilter objektidentifikator() {
        if (objektidentifikator == null) {
            objektidentifikator = new StringFilter();
        }
        return objektidentifikator;
    }

    public void setObjektidentifikator(StringFilter objektidentifikator) {
        this.objektidentifikator = objektidentifikator;
    }

    public LocalDateFilter getBeginnlebenszeit() {
        return beginnlebenszeit;
    }

    public LocalDateFilter beginnlebenszeit() {
        if (beginnlebenszeit == null) {
            beginnlebenszeit = new LocalDateFilter();
        }
        return beginnlebenszeit;
    }

    public void setBeginnlebenszeit(LocalDateFilter beginnlebenszeit) {
        this.beginnlebenszeit = beginnlebenszeit;
    }

    public StringFilter getAdminEbeneAde() {
        return adminEbeneAde;
    }

    public StringFilter adminEbeneAde() {
        if (adminEbeneAde == null) {
            adminEbeneAde = new StringFilter();
        }
        return adminEbeneAde;
    }

    public void setAdminEbeneAde(StringFilter adminEbeneAde) {
        this.adminEbeneAde = adminEbeneAde;
    }

    public IntegerFilter getAde() {
        return ade;
    }

    public IntegerFilter ade() {
        if (ade == null) {
            ade = new IntegerFilter();
        }
        return ade;
    }

    public void setAde(IntegerFilter ade) {
        this.ade = ade;
    }

    public StringFilter getGeofaktorGf() {
        return geofaktorGf;
    }

    public StringFilter geofaktorGf() {
        if (geofaktorGf == null) {
            geofaktorGf = new StringFilter();
        }
        return geofaktorGf;
    }

    public void setGeofaktorGf(StringFilter geofaktorGf) {
        this.geofaktorGf = geofaktorGf;
    }

    public LongFilter getGf() {
        return gf;
    }

    public LongFilter gf() {
        if (gf == null) {
            gf = new LongFilter();
        }
        return gf;
    }

    public void setGf(LongFilter gf) {
        this.gf = gf;
    }

    public StringFilter getBesondereGebieteBsg() {
        return besondereGebieteBsg;
    }

    public StringFilter besondereGebieteBsg() {
        if (besondereGebieteBsg == null) {
            besondereGebieteBsg = new StringFilter();
        }
        return besondereGebieteBsg;
    }

    public void setBesondereGebieteBsg(StringFilter besondereGebieteBsg) {
        this.besondereGebieteBsg = besondereGebieteBsg;
    }

    public LongFilter getBsg() {
        return bsg;
    }

    public LongFilter bsg() {
        if (bsg == null) {
            bsg = new LongFilter();
        }
        return bsg;
    }

    public void setBsg(LongFilter bsg) {
        this.bsg = bsg;
    }

    public StringFilter getRegionalschluesselArs() {
        return regionalschluesselArs;
    }

    public StringFilter regionalschluesselArs() {
        if (regionalschluesselArs == null) {
            regionalschluesselArs = new StringFilter();
        }
        return regionalschluesselArs;
    }

    public void setRegionalschluesselArs(StringFilter regionalschluesselArs) {
        this.regionalschluesselArs = regionalschluesselArs;
    }

    public StringFilter getGemeindeschluesselAgs() {
        return gemeindeschluesselAgs;
    }

    public StringFilter gemeindeschluesselAgs() {
        if (gemeindeschluesselAgs == null) {
            gemeindeschluesselAgs = new StringFilter();
        }
        return gemeindeschluesselAgs;
    }

    public void setGemeindeschluesselAgs(StringFilter gemeindeschluesselAgs) {
        this.gemeindeschluesselAgs = gemeindeschluesselAgs;
    }

    public StringFilter getVerwaltungssitzSdvArs() {
        return verwaltungssitzSdvArs;
    }

    public StringFilter verwaltungssitzSdvArs() {
        if (verwaltungssitzSdvArs == null) {
            verwaltungssitzSdvArs = new StringFilter();
        }
        return verwaltungssitzSdvArs;
    }

    public void setVerwaltungssitzSdvArs(StringFilter verwaltungssitzSdvArs) {
        this.verwaltungssitzSdvArs = verwaltungssitzSdvArs;
    }

    public StringFilter getGeografischernameGen() {
        return geografischernameGen;
    }

    public StringFilter geografischernameGen() {
        if (geografischernameGen == null) {
            geografischernameGen = new StringFilter();
        }
        return geografischernameGen;
    }

    public void setGeografischernameGen(StringFilter geografischernameGen) {
        this.geografischernameGen = geografischernameGen;
    }

    public StringFilter getBezeichnung() {
        return bezeichnung;
    }

    public StringFilter bezeichnung() {
        if (bezeichnung == null) {
            bezeichnung = new StringFilter();
        }
        return bezeichnung;
    }

    public void setBezeichnung(StringFilter bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public LongFilter getIdentifikatorIbz() {
        return identifikatorIbz;
    }

    public LongFilter identifikatorIbz() {
        if (identifikatorIbz == null) {
            identifikatorIbz = new LongFilter();
        }
        return identifikatorIbz;
    }

    public void setIdentifikatorIbz(LongFilter identifikatorIbz) {
        this.identifikatorIbz = identifikatorIbz;
    }

    public StringFilter getBemerkung() {
        return bemerkung;
    }

    public StringFilter bemerkung() {
        if (bemerkung == null) {
            bemerkung = new StringFilter();
        }
        return bemerkung;
    }

    public void setBemerkung(StringFilter bemerkung) {
        this.bemerkung = bemerkung;
    }

    public StringFilter getNamensbildungNbd() {
        return namensbildungNbd;
    }

    public StringFilter namensbildungNbd() {
        if (namensbildungNbd == null) {
            namensbildungNbd = new StringFilter();
        }
        return namensbildungNbd;
    }

    public void setNamensbildungNbd(StringFilter namensbildungNbd) {
        this.namensbildungNbd = namensbildungNbd;
    }

    public BooleanFilter getNbd() {
        return nbd;
    }

    public BooleanFilter nbd() {
        if (nbd == null) {
            nbd = new BooleanFilter();
        }
        return nbd;
    }

    public void setNbd(BooleanFilter nbd) {
        this.nbd = nbd;
    }

    public StringFilter getLand() {
        return land;
    }

    public StringFilter land() {
        if (land == null) {
            land = new StringFilter();
        }
        return land;
    }

    public void setLand(StringFilter land) {
        this.land = land;
    }

    public StringFilter getRegierungsbezirk() {
        return regierungsbezirk;
    }

    public StringFilter regierungsbezirk() {
        if (regierungsbezirk == null) {
            regierungsbezirk = new StringFilter();
        }
        return regierungsbezirk;
    }

    public void setRegierungsbezirk(StringFilter regierungsbezirk) {
        this.regierungsbezirk = regierungsbezirk;
    }

    public StringFilter getKreis() {
        return kreis;
    }

    public StringFilter kreis() {
        if (kreis == null) {
            kreis = new StringFilter();
        }
        return kreis;
    }

    public void setKreis(StringFilter kreis) {
        this.kreis = kreis;
    }

    public StringFilter getVerwaltungsgemeinschaftteil1() {
        return verwaltungsgemeinschaftteil1;
    }

    public StringFilter verwaltungsgemeinschaftteil1() {
        if (verwaltungsgemeinschaftteil1 == null) {
            verwaltungsgemeinschaftteil1 = new StringFilter();
        }
        return verwaltungsgemeinschaftteil1;
    }

    public void setVerwaltungsgemeinschaftteil1(StringFilter verwaltungsgemeinschaftteil1) {
        this.verwaltungsgemeinschaftteil1 = verwaltungsgemeinschaftteil1;
    }

    public StringFilter getVerwaltungsgemeinschaftteil2() {
        return verwaltungsgemeinschaftteil2;
    }

    public StringFilter verwaltungsgemeinschaftteil2() {
        if (verwaltungsgemeinschaftteil2 == null) {
            verwaltungsgemeinschaftteil2 = new StringFilter();
        }
        return verwaltungsgemeinschaftteil2;
    }

    public void setVerwaltungsgemeinschaftteil2(StringFilter verwaltungsgemeinschaftteil2) {
        this.verwaltungsgemeinschaftteil2 = verwaltungsgemeinschaftteil2;
    }

    public StringFilter getGemeinde() {
        return gemeinde;
    }

    public StringFilter gemeinde() {
        if (gemeinde == null) {
            gemeinde = new StringFilter();
        }
        return gemeinde;
    }

    public void setGemeinde(StringFilter gemeinde) {
        this.gemeinde = gemeinde;
    }

    public StringFilter getFunkSchluesselstelle3() {
        return funkSchluesselstelle3;
    }

    public StringFilter funkSchluesselstelle3() {
        if (funkSchluesselstelle3 == null) {
            funkSchluesselstelle3 = new StringFilter();
        }
        return funkSchluesselstelle3;
    }

    public void setFunkSchluesselstelle3(StringFilter funkSchluesselstelle3) {
        this.funkSchluesselstelle3 = funkSchluesselstelle3;
    }

    public StringFilter getFkS3() {
        return fkS3;
    }

    public StringFilter fkS3() {
        if (fkS3 == null) {
            fkS3 = new StringFilter();
        }
        return fkS3;
    }

    public void setFkS3(StringFilter fkS3) {
        this.fkS3 = fkS3;
    }

    public StringFilter getEuropStatistikschluesselNuts() {
        return europStatistikschluesselNuts;
    }

    public StringFilter europStatistikschluesselNuts() {
        if (europStatistikschluesselNuts == null) {
            europStatistikschluesselNuts = new StringFilter();
        }
        return europStatistikschluesselNuts;
    }

    public void setEuropStatistikschluesselNuts(StringFilter europStatistikschluesselNuts) {
        this.europStatistikschluesselNuts = europStatistikschluesselNuts;
    }

    public StringFilter getRegioschluesselaufgefuellt() {
        return regioschluesselaufgefuellt;
    }

    public StringFilter regioschluesselaufgefuellt() {
        if (regioschluesselaufgefuellt == null) {
            regioschluesselaufgefuellt = new StringFilter();
        }
        return regioschluesselaufgefuellt;
    }

    public void setRegioschluesselaufgefuellt(StringFilter regioschluesselaufgefuellt) {
        this.regioschluesselaufgefuellt = regioschluesselaufgefuellt;
    }

    public StringFilter getGemeindeschluesselaufgefuellt() {
        return gemeindeschluesselaufgefuellt;
    }

    public StringFilter gemeindeschluesselaufgefuellt() {
        if (gemeindeschluesselaufgefuellt == null) {
            gemeindeschluesselaufgefuellt = new StringFilter();
        }
        return gemeindeschluesselaufgefuellt;
    }

    public void setGemeindeschluesselaufgefuellt(StringFilter gemeindeschluesselaufgefuellt) {
        this.gemeindeschluesselaufgefuellt = gemeindeschluesselaufgefuellt;
    }

    public LocalDateFilter getWirksamkeitWsk() {
        return wirksamkeitWsk;
    }

    public LocalDateFilter wirksamkeitWsk() {
        if (wirksamkeitWsk == null) {
            wirksamkeitWsk = new LocalDateFilter();
        }
        return wirksamkeitWsk;
    }

    public void setWirksamkeitWsk(LocalDateFilter wirksamkeitWsk) {
        this.wirksamkeitWsk = wirksamkeitWsk;
    }

    public LongFilter getDistrictId() {
        return districtId;
    }

    public LongFilter districtId() {
        if (districtId == null) {
            districtId = new LongFilter();
        }
        return districtId;
    }

    public void setDistrictId(LongFilter districtId) {
        this.districtId = districtId;
    }

    public LongFilter getMastrUnitId() {
        return mastrUnitId;
    }

    public LongFilter mastrUnitId() {
        if (mastrUnitId == null) {
            mastrUnitId = new LongFilter();
        }
        return mastrUnitId;
    }

    public void setMastrUnitId(LongFilter mastrUnitId) {
        this.mastrUnitId = mastrUnitId;
    }

    public LongFilter getChargingStationId() {
        return chargingStationId;
    }

    public LongFilter chargingStationId() {
        if (chargingStationId == null) {
            chargingStationId = new LongFilter();
        }
        return chargingStationId;
    }

    public void setChargingStationId(LongFilter chargingStationId) {
        this.chargingStationId = chargingStationId;
    }

    public LongFilter getStatisticId() {
        return statisticId;
    }

    public LongFilter statisticId() {
        if (statisticId == null) {
            statisticId = new LongFilter();
        }
        return statisticId;
    }

    public void setStatisticId(LongFilter statisticId) {
        this.statisticId = statisticId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AdministrationUnitCriteria that = (AdministrationUnitCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(objektidentifikator, that.objektidentifikator) &&
            Objects.equals(beginnlebenszeit, that.beginnlebenszeit) &&
            Objects.equals(adminEbeneAde, that.adminEbeneAde) &&
            Objects.equals(ade, that.ade) &&
            Objects.equals(geofaktorGf, that.geofaktorGf) &&
            Objects.equals(gf, that.gf) &&
            Objects.equals(besondereGebieteBsg, that.besondereGebieteBsg) &&
            Objects.equals(bsg, that.bsg) &&
            Objects.equals(regionalschluesselArs, that.regionalschluesselArs) &&
            Objects.equals(gemeindeschluesselAgs, that.gemeindeschluesselAgs) &&
            Objects.equals(verwaltungssitzSdvArs, that.verwaltungssitzSdvArs) &&
            Objects.equals(geografischernameGen, that.geografischernameGen) &&
            Objects.equals(bezeichnung, that.bezeichnung) &&
            Objects.equals(identifikatorIbz, that.identifikatorIbz) &&
            Objects.equals(bemerkung, that.bemerkung) &&
            Objects.equals(namensbildungNbd, that.namensbildungNbd) &&
            Objects.equals(nbd, that.nbd) &&
            Objects.equals(land, that.land) &&
            Objects.equals(regierungsbezirk, that.regierungsbezirk) &&
            Objects.equals(kreis, that.kreis) &&
            Objects.equals(verwaltungsgemeinschaftteil1, that.verwaltungsgemeinschaftteil1) &&
            Objects.equals(verwaltungsgemeinschaftteil2, that.verwaltungsgemeinschaftteil2) &&
            Objects.equals(gemeinde, that.gemeinde) &&
            Objects.equals(funkSchluesselstelle3, that.funkSchluesselstelle3) &&
            Objects.equals(fkS3, that.fkS3) &&
            Objects.equals(europStatistikschluesselNuts, that.europStatistikschluesselNuts) &&
            Objects.equals(regioschluesselaufgefuellt, that.regioschluesselaufgefuellt) &&
            Objects.equals(gemeindeschluesselaufgefuellt, that.gemeindeschluesselaufgefuellt) &&
            Objects.equals(wirksamkeitWsk, that.wirksamkeitWsk) &&
            Objects.equals(districtId, that.districtId) &&
            Objects.equals(mastrUnitId, that.mastrUnitId) &&
            Objects.equals(chargingStationId, that.chargingStationId) &&
            Objects.equals(statisticId, that.statisticId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            objektidentifikator,
            beginnlebenszeit,
            adminEbeneAde,
            ade,
            geofaktorGf,
            gf,
            besondereGebieteBsg,
            bsg,
            regionalschluesselArs,
            gemeindeschluesselAgs,
            verwaltungssitzSdvArs,
            geografischernameGen,
            bezeichnung,
            identifikatorIbz,
            bemerkung,
            namensbildungNbd,
            nbd,
            land,
            regierungsbezirk,
            kreis,
            verwaltungsgemeinschaftteil1,
            verwaltungsgemeinschaftteil2,
            gemeinde,
            funkSchluesselstelle3,
            fkS3,
            europStatistikschluesselNuts,
            regioschluesselaufgefuellt,
            gemeindeschluesselaufgefuellt,
            wirksamkeitWsk,
            districtId,
            mastrUnitId,
            chargingStationId,
            statisticId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AdministrationUnitCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (objektidentifikator != null ? "objektidentifikator=" + objektidentifikator + ", " : "") +
            (beginnlebenszeit != null ? "beginnlebenszeit=" + beginnlebenszeit + ", " : "") +
            (adminEbeneAde != null ? "adminEbeneAde=" + adminEbeneAde + ", " : "") +
            (ade != null ? "ade=" + ade + ", " : "") +
            (geofaktorGf != null ? "geofaktorGf=" + geofaktorGf + ", " : "") +
            (gf != null ? "gf=" + gf + ", " : "") +
            (besondereGebieteBsg != null ? "besondereGebieteBsg=" + besondereGebieteBsg + ", " : "") +
            (bsg != null ? "bsg=" + bsg + ", " : "") +
            (regionalschluesselArs != null ? "regionalschluesselArs=" + regionalschluesselArs + ", " : "") +
            (gemeindeschluesselAgs != null ? "gemeindeschluesselAgs=" + gemeindeschluesselAgs + ", " : "") +
            (verwaltungssitzSdvArs != null ? "verwaltungssitzSdvArs=" + verwaltungssitzSdvArs + ", " : "") +
            (geografischernameGen != null ? "geografischernameGen=" + geografischernameGen + ", " : "") +
            (bezeichnung != null ? "bezeichnung=" + bezeichnung + ", " : "") +
            (identifikatorIbz != null ? "identifikatorIbz=" + identifikatorIbz + ", " : "") +
            (bemerkung != null ? "bemerkung=" + bemerkung + ", " : "") +
            (namensbildungNbd != null ? "namensbildungNbd=" + namensbildungNbd + ", " : "") +
            (nbd != null ? "nbd=" + nbd + ", " : "") +
            (land != null ? "land=" + land + ", " : "") +
            (regierungsbezirk != null ? "regierungsbezirk=" + regierungsbezirk + ", " : "") +
            (kreis != null ? "kreis=" + kreis + ", " : "") +
            (verwaltungsgemeinschaftteil1 != null ? "verwaltungsgemeinschaftteil1=" + verwaltungsgemeinschaftteil1 + ", " : "") +
            (verwaltungsgemeinschaftteil2 != null ? "verwaltungsgemeinschaftteil2=" + verwaltungsgemeinschaftteil2 + ", " : "") +
            (gemeinde != null ? "gemeinde=" + gemeinde + ", " : "") +
            (funkSchluesselstelle3 != null ? "funkSchluesselstelle3=" + funkSchluesselstelle3 + ", " : "") +
            (fkS3 != null ? "fkS3=" + fkS3 + ", " : "") +
            (europStatistikschluesselNuts != null ? "europStatistikschluesselNuts=" + europStatistikschluesselNuts + ", " : "") +
            (regioschluesselaufgefuellt != null ? "regioschluesselaufgefuellt=" + regioschluesselaufgefuellt + ", " : "") +
            (gemeindeschluesselaufgefuellt != null ? "gemeindeschluesselaufgefuellt=" + gemeindeschluesselaufgefuellt + ", " : "") +
            (wirksamkeitWsk != null ? "wirksamkeitWsk=" + wirksamkeitWsk + ", " : "") +
            (districtId != null ? "districtId=" + districtId + ", " : "") +
            (mastrUnitId != null ? "mastrUnitId=" + mastrUnitId + ", " : "") +
            (chargingStationId != null ? "chargingStationId=" + chargingStationId + ", " : "") +
            (statisticId != null ? "statisticId=" + statisticId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
