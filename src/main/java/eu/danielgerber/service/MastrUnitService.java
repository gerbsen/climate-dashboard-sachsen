package eu.danielgerber.service;

import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
import static org.geolatte.geom.builder.DSL.g;
import static org.geolatte.geom.builder.DSL.point;
import static org.geolatte.geom.crs.CoordinateReferenceSystems.WGS84;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.geolatte.geom.jts.JTS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import eu.danielgerber.config.ApplicationProperties;
import eu.danielgerber.domain.AdministrationUnit;
import eu.danielgerber.domain.MastrResult;
import eu.danielgerber.domain.MastrUnit;
import eu.danielgerber.domain.Statistic;
import eu.danielgerber.domain.enums.OperationState;
import eu.danielgerber.repository.AdministrationUnitRepository;
import eu.danielgerber.repository.MastrUnitRepository;
import eu.danielgerber.repository.StatisticRepository;
import eu.danielgerber.service.adapter.GeometryAdapter;
import eu.danielgerber.service.adapter.InstantAdapter;
import eu.danielgerber.service.adapter.LocalDateAdapter;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Service Implementation for managing {@link MastrUnit}.
 */
@Service
@Transactional
public class MastrUnitService {

    private final Logger log = LoggerFactory.getLogger(MastrUnitService.class);
    private final MastrUnitRepository mastrUnitRepository;
    private final AdministrationUnitRepository administrationUnitRepository;
    private final ApplicationProperties applicationProperties;
    private final StatisticRepository statisticsRepository;
    private static final String MASTR = "MaStR";

    public MastrUnitService(MastrUnitRepository mastrUnitRepository,
                            AdministrationUnitRepository administrationUnitRepository,
                            StatisticRepository statisticsRepository,
                            ApplicationProperties properties) {
        this.mastrUnitRepository = mastrUnitRepository;
        this.statisticsRepository = statisticsRepository;
        this.administrationUnitRepository = administrationUnitRepository;
        this.applicationProperties = properties;
    }

    /**
     * Save a mastrUnit.
     *
     * @param mastrUnit the entity to save.
     * @return the persisted entity.
     */
    public MastrUnit save(MastrUnit mastrUnit) {
        log.debug("Request to save MastrUnit : {}", mastrUnit);
        return mastrUnitRepository.save(mastrUnit);
    }

    /**
     * Save a list of mastrUnits.
     *
     * @param mastrUnits the entities to save.
     * @return the persisted entities.
     */
    public List<MastrUnit> saveAll(List<MastrUnit> mastrUnits) {
        log.debug("Request to save MastrUnits : {}", mastrUnits);
        return mastrUnitRepository.saveAll(mastrUnits);
    }

    /**
     * Update a mastrUnit.
     *
     * @param mastrUnit the entity to save.
     * @return the persisted entity.
     */
    public MastrUnit update(MastrUnit mastrUnit) {
        log.debug("Request to update MastrUnit : {}", mastrUnit);
        return mastrUnitRepository.save(mastrUnit);
    }

    /**
     * Partially update a mastrUnit.
     *
     * @param mastrUnit the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<MastrUnit> partialUpdate(MastrUnit mastrUnit) {
        log.debug("Request to partially update MastrUnit : {}", mastrUnit);

        return mastrUnitRepository
            .findById(mastrUnit.getId())
            .map(existingMastrUnit -> {
                if (mastrUnit.getMastrNummer() != null) {
                    existingMastrUnit.setMastrNummer(mastrUnit.getMastrNummer());
                }
                if (mastrUnit.getAnlagenBetreiberId() != null) {
                    existingMastrUnit.setAnlagenBetreiberId(mastrUnit.getAnlagenBetreiberId());
                }
                if (mastrUnit.getAnlagenBetreiberPersonenArt() != null) {
                    existingMastrUnit.setAnlagenBetreiberPersonenArt(mastrUnit.getAnlagenBetreiberPersonenArt());
                }
                if (mastrUnit.getAnlagenBetreiberMaskedName() != null) {
                    existingMastrUnit.setAnlagenBetreiberMaskedName(mastrUnit.getAnlagenBetreiberMaskedName());
                }
                if (mastrUnit.getAnlagenBetreiberMastrNummer() != null) {
                    existingMastrUnit.setAnlagenBetreiberMastrNummer(mastrUnit.getAnlagenBetreiberMastrNummer());
                }
                if (mastrUnit.getAnlagenBetreiberName() != null) {
                    existingMastrUnit.setAnlagenBetreiberName(mastrUnit.getAnlagenBetreiberName());
                }
                if (mastrUnit.getBetriebsStatusId() != null) {
                    existingMastrUnit.setBetriebsStatusId(mastrUnit.getBetriebsStatusId());
                }
                if (mastrUnit.getBetriebsStatusName() != null) {
                    existingMastrUnit.setBetriebsStatusName(mastrUnit.getBetriebsStatusName());
                }
                if (mastrUnit.getBundesland() != null) {
                    existingMastrUnit.setBundesland(mastrUnit.getBundesland());
                }
                if (mastrUnit.getBundeslandId() != null) {
                    existingMastrUnit.setBundeslandId(mastrUnit.getBundeslandId());
                }
                if (mastrUnit.getLandkreisId() != null) {
                    existingMastrUnit.setLandkreisId(mastrUnit.getLandkreisId());
                }
                if (mastrUnit.getGemeindeId() != null) {
                    existingMastrUnit.setGemeindeId(mastrUnit.getGemeindeId());
                }
                if (mastrUnit.getDatumLetzteAktualisierung() != null) {
                    existingMastrUnit.setDatumLetzteAktualisierung(mastrUnit.getDatumLetzteAktualisierung());
                }
                if (mastrUnit.getEinheitRegistrierungsDatum() != null) {
                    existingMastrUnit.setEinheitRegistrierungsDatum(mastrUnit.getEinheitRegistrierungsDatum());
                }
                if (mastrUnit.getEndgueltigeStilllegungDatum() != null) {
                    existingMastrUnit.setEndgueltigeStilllegungDatum(mastrUnit.getEndgueltigeStilllegungDatum());
                }
                if (mastrUnit.getGeplantesInbetriebsnahmeDatum() != null) {
                    existingMastrUnit.setGeplantesInbetriebsnahmeDatum(mastrUnit.getGeplantesInbetriebsnahmeDatum());
                }
                if (mastrUnit.getInbetriebnahmeDatum() != null) {
                    existingMastrUnit.setInbetriebnahmeDatum(mastrUnit.getInbetriebnahmeDatum());
                }
                if (mastrUnit.getKwkAnlageInbetriebnahmeDatum() != null) {
                    existingMastrUnit.setKwkAnlageInbetriebnahmeDatum(mastrUnit.getKwkAnlageInbetriebnahmeDatum());
                }
                if (mastrUnit.getKwkAnlageRegistrierungsDatum() != null) {
                    existingMastrUnit.setKwkAnlageRegistrierungsDatum(mastrUnit.getKwkAnlageRegistrierungsDatum());
                }
                if (mastrUnit.getEegInbetriebnahmeDatum() != null) {
                    existingMastrUnit.setEegInbetriebnahmeDatum(mastrUnit.getEegInbetriebnahmeDatum());
                }
                if (mastrUnit.getEegAnlageRegistrierungsDatum() != null) {
                    existingMastrUnit.setEegAnlageRegistrierungsDatum(mastrUnit.getEegAnlageRegistrierungsDatum());
                }
                if (mastrUnit.getGenehmigungDatum() != null) {
                    existingMastrUnit.setGenehmigungDatum(mastrUnit.getGenehmigungDatum());
                }
                if (mastrUnit.getGenehmigungRegistrierungsDatum() != null) {
                    existingMastrUnit.setGenehmigungRegistrierungsDatum(mastrUnit.getGenehmigungRegistrierungsDatum());
                }
                if (mastrUnit.getIsNbPruefungAbgeschlossen() != null) {
                    existingMastrUnit.setIsNbPruefungAbgeschlossen(mastrUnit.getIsNbPruefungAbgeschlossen());
                }
                if (mastrUnit.getIsAnonymisiert() != null) {
                    existingMastrUnit.setIsAnonymisiert(mastrUnit.getIsAnonymisiert());
                }
                if (mastrUnit.getIsBuergerEnergie() != null) {
                    existingMastrUnit.setIsBuergerEnergie(mastrUnit.getIsBuergerEnergie());
                }
                if (mastrUnit.getIsEinheitNotstromaggregat() != null) {
                    existingMastrUnit.setIsEinheitNotstromaggregat(mastrUnit.getIsEinheitNotstromaggregat());
                }
                if (mastrUnit.getIsMieterstromAngemeldet() != null) {
                    existingMastrUnit.setIsMieterstromAngemeldet(mastrUnit.getIsMieterstromAngemeldet());
                }
                if (mastrUnit.getIsWasserkraftErtuechtigung() != null) {
                    existingMastrUnit.setIsWasserkraftErtuechtigung(mastrUnit.getIsWasserkraftErtuechtigung());
                }
                if (mastrUnit.getIsPilotWindanlage() != null) {
                    existingMastrUnit.setIsPilotWindanlage(mastrUnit.getIsPilotWindanlage());
                }
                if (mastrUnit.getIsPrototypAnlage() != null) {
                    existingMastrUnit.setIsPrototypAnlage(mastrUnit.getIsPrototypAnlage());
                }
                if (mastrUnit.getLat() != null) {
                    existingMastrUnit.setLat(mastrUnit.getLat());
                }
                if (mastrUnit.getLng() != null) {
                    existingMastrUnit.setLng(mastrUnit.getLng());
                }
                if (mastrUnit.getOrt() != null) {
                    existingMastrUnit.setOrt(mastrUnit.getOrt());
                }
                if (mastrUnit.getPlz() != null) {
                    existingMastrUnit.setPlz(mastrUnit.getPlz());
                }
                if (mastrUnit.getStrasse() != null) {
                    existingMastrUnit.setStrasse(mastrUnit.getStrasse());
                }
                if (mastrUnit.getHausnummer() != null) {
                    existingMastrUnit.setHausnummer(mastrUnit.getHausnummer());
                }
                if (mastrUnit.getEinheitname() != null) {
                    existingMastrUnit.setEinheitname(mastrUnit.getEinheitname());
                }
                if (mastrUnit.getFlurstueck() != null) {
                    existingMastrUnit.setFlurstueck(mastrUnit.getFlurstueck());
                }
                if (mastrUnit.getGemarkung() != null) {
                    existingMastrUnit.setGemarkung(mastrUnit.getGemarkung());
                }
                if (mastrUnit.getGemeinde() != null) {
                    existingMastrUnit.setGemeinde(mastrUnit.getGemeinde());
                }
                if (mastrUnit.getLandId() != null) {
                    existingMastrUnit.setLandId(mastrUnit.getLandId());
                }
                if (mastrUnit.getLandkreis() != null) {
                    existingMastrUnit.setLandkreis(mastrUnit.getLandkreis());
                }
                if (mastrUnit.getAgs() != null) {
                    existingMastrUnit.setAgs(mastrUnit.getAgs());
                }
                if (mastrUnit.getLokationId() != null) {
                    existingMastrUnit.setLokationId(mastrUnit.getLokationId());
                }
                if (mastrUnit.getLokationMastrNr() != null) {
                    existingMastrUnit.setLokationMastrNr(mastrUnit.getLokationMastrNr());
                }
                if (mastrUnit.getNetzbetreiberId() != null) {
                    existingMastrUnit.setNetzbetreiberId(mastrUnit.getNetzbetreiberId());
                }
                if (mastrUnit.getNetzbetreiberMaskedNamen() != null) {
                    existingMastrUnit.setNetzbetreiberMaskedNamen(mastrUnit.getNetzbetreiberMaskedNamen());
                }
                if (mastrUnit.getNetzbetreiberMastrNummer() != null) {
                    existingMastrUnit.setNetzbetreiberMastrNummer(mastrUnit.getNetzbetreiberMastrNummer());
                }
                if (mastrUnit.getNetzbetreiberNamen() != null) {
                    existingMastrUnit.setNetzbetreiberNamen(mastrUnit.getNetzbetreiberNamen());
                }
                if (mastrUnit.getNetzbetreiberPersonenArt() != null) {
                    existingMastrUnit.setNetzbetreiberPersonenArt(mastrUnit.getNetzbetreiberPersonenArt());
                }
                if (mastrUnit.getSystemStatusId() != null) {
                    existingMastrUnit.setSystemStatusId(mastrUnit.getSystemStatusId());
                }
                if (mastrUnit.getSystemStatusName() != null) {
                    existingMastrUnit.setSystemStatusName(mastrUnit.getSystemStatusName());
                }
                if (mastrUnit.getTyp() != null) {
                    existingMastrUnit.setTyp(mastrUnit.getTyp());
                }
                if (mastrUnit.getAktenzeichenGenehmigung() != null) {
                    existingMastrUnit.setAktenzeichenGenehmigung(mastrUnit.getAktenzeichenGenehmigung());
                }
                if (mastrUnit.getAnzahlSolarmodule() != null) {
                    existingMastrUnit.setAnzahlSolarmodule(mastrUnit.getAnzahlSolarmodule());
                }
                if (mastrUnit.getBatterieTechnologie() != null) {
                    existingMastrUnit.setBatterieTechnologie(mastrUnit.getBatterieTechnologie());
                }
                if (mastrUnit.getBruttoLeistung() != null) {
                    existingMastrUnit.setBruttoLeistung(mastrUnit.getBruttoLeistung());
                }
                if (mastrUnit.getEegInstallierteLeistung() != null) {
                    existingMastrUnit.setEegInstallierteLeistung(mastrUnit.getEegInstallierteLeistung());
                }
                if (mastrUnit.getEegAnlageMastrNummer() != null) {
                    existingMastrUnit.setEegAnlageMastrNummer(mastrUnit.getEegAnlageMastrNummer());
                }
                if (mastrUnit.getEegAnlagenSchluessel() != null) {
                    existingMastrUnit.setEegAnlagenSchluessel(mastrUnit.getEegAnlagenSchluessel());
                }
                if (mastrUnit.getEegZuschlag() != null) {
                    existingMastrUnit.setEegZuschlag(mastrUnit.getEegZuschlag());
                }
                if (mastrUnit.getZuschlagsNummern() != null) {
                    existingMastrUnit.setZuschlagsNummern(mastrUnit.getZuschlagsNummern());
                }
                if (mastrUnit.getEnergieTraegerId() != null) {
                    existingMastrUnit.setEnergieTraegerId(mastrUnit.getEnergieTraegerId());
                }
                if (mastrUnit.getEnergieTraegerName() != null) {
                    existingMastrUnit.setEnergieTraegerName(mastrUnit.getEnergieTraegerName());
                }
                if (mastrUnit.getGemeinsamerWechselrichter() != null) {
                    existingMastrUnit.setGemeinsamerWechselrichter(mastrUnit.getGemeinsamerWechselrichter());
                }
                if (mastrUnit.getGenehmigungBehoerde() != null) {
                    existingMastrUnit.setGenehmigungBehoerde(mastrUnit.getGenehmigungBehoerde());
                }
                if (mastrUnit.getGenehmigungsMastrNummer() != null) {
                    existingMastrUnit.setGenehmigungsMastrNummer(mastrUnit.getGenehmigungsMastrNummer());
                }
                if (mastrUnit.getGruppierungsObjekte() != null) {
                    existingMastrUnit.setGruppierungsObjekte(mastrUnit.getGruppierungsObjekte());
                }
                if (mastrUnit.getGruppierungsObjekteIds() != null) {
                    existingMastrUnit.setGruppierungsObjekteIds(mastrUnit.getGruppierungsObjekteIds());
                }
                if (mastrUnit.getHatFlexibilitaetsPraemie() != null) {
                    existingMastrUnit.setHatFlexibilitaetsPraemie(mastrUnit.getHatFlexibilitaetsPraemie());
                }
                if (mastrUnit.getHauptAusrichtungSolarmodule() != null) {
                    existingMastrUnit.setHauptAusrichtungSolarmodule(mastrUnit.getHauptAusrichtungSolarmodule());
                }
                if (mastrUnit.getHauptAusrichtungSolarmoduleBezeichnung() != null) {
                    existingMastrUnit.setHauptAusrichtungSolarmoduleBezeichnung(mastrUnit.getHauptAusrichtungSolarmoduleBezeichnung());
                }
                if (mastrUnit.getHauptBrennstoffId() != null) {
                    existingMastrUnit.setHauptBrennstoffId(mastrUnit.getHauptBrennstoffId());
                }
                if (mastrUnit.getHauptBrennstoffNamen() != null) {
                    existingMastrUnit.setHauptBrennstoffNamen(mastrUnit.getHauptBrennstoffNamen());
                }
                if (mastrUnit.getHauptNeigungswinkelSolarModule() != null) {
                    existingMastrUnit.setHauptNeigungswinkelSolarModule(mastrUnit.getHauptNeigungswinkelSolarModule());
                }
                if (mastrUnit.getHerstellerWindenergieanlageId() != null) {
                    existingMastrUnit.setHerstellerWindenergieanlageId(mastrUnit.getHerstellerWindenergieanlageId());
                }
                if (mastrUnit.getHerstellerWindenergieanlageBezeichnung() != null) {
                    existingMastrUnit.setHerstellerWindenergieanlageBezeichnung(mastrUnit.getHerstellerWindenergieanlageBezeichnung());
                }
                if (mastrUnit.getKwkAnlageElektrischeLeistung() != null) {
                    existingMastrUnit.setKwkAnlageElektrischeLeistung(mastrUnit.getKwkAnlageElektrischeLeistung());
                }
                if (mastrUnit.getKwkAnlageMastrNummer() != null) {
                    existingMastrUnit.setKwkAnlageMastrNummer(mastrUnit.getKwkAnlageMastrNummer());
                }
                if (mastrUnit.getKwkZuschlag() != null) {
                    existingMastrUnit.setKwkZuschlag(mastrUnit.getKwkZuschlag());
                }
                if (mastrUnit.getLageEinheit() != null) {
                    existingMastrUnit.setLageEinheit(mastrUnit.getLageEinheit());
                }
                if (mastrUnit.getLageEinheitBezeichnung() != null) {
                    existingMastrUnit.setLageEinheitBezeichnung(mastrUnit.getLageEinheitBezeichnung());
                }
                if (mastrUnit.getLeistungsBegrenzung() != null) {
                    existingMastrUnit.setLeistungsBegrenzung(mastrUnit.getLeistungsBegrenzung());
                }
                if (mastrUnit.getNabenhoeheWindenergieanlage() != null) {
                    existingMastrUnit.setNabenhoeheWindenergieanlage(mastrUnit.getNabenhoeheWindenergieanlage());
                }
                if (mastrUnit.getRotorDurchmesserWindenergieanlage() != null) {
                    existingMastrUnit.setRotorDurchmesserWindenergieanlage(mastrUnit.getRotorDurchmesserWindenergieanlage());
                }
                if (mastrUnit.getNettoNennLeistung() != null) {
                    existingMastrUnit.setNettoNennLeistung(mastrUnit.getNettoNennLeistung());
                }
                if (mastrUnit.getNutzbareSpeicherKapazitaet() != null) {
                    existingMastrUnit.setNutzbareSpeicherKapazitaet(mastrUnit.getNutzbareSpeicherKapazitaet());
                }
                if (mastrUnit.getNutzungsBereichGebsa() != null) {
                    existingMastrUnit.setNutzungsBereichGebsa(mastrUnit.getNutzungsBereichGebsa());
                }
                if (mastrUnit.getStandortAnonymisiert() != null) {
                    existingMastrUnit.setStandortAnonymisiert(mastrUnit.getStandortAnonymisiert());
                }
                if (mastrUnit.getSpannungsEbenenId() != null) {
                    existingMastrUnit.setSpannungsEbenenId(mastrUnit.getSpannungsEbenenId());
                }
                if (mastrUnit.getSpannungsEbenenNamen() != null) {
                    existingMastrUnit.setSpannungsEbenenNamen(mastrUnit.getSpannungsEbenenNamen());
                }
                if (mastrUnit.getSpeicherEinheitMastrNummer() != null) {
                    existingMastrUnit.setSpeicherEinheitMastrNummer(mastrUnit.getSpeicherEinheitMastrNummer());
                }
                if (mastrUnit.getTechnologieStromerzeugungId() != null) {
                    existingMastrUnit.setTechnologieStromerzeugungId(mastrUnit.getTechnologieStromerzeugungId());
                }
                if (mastrUnit.getTechnologieStromerzeugung() != null) {
                    existingMastrUnit.setTechnologieStromerzeugung(mastrUnit.getTechnologieStromerzeugung());
                }
                if (mastrUnit.getThermischeNutzLeistung() != null) {
                    existingMastrUnit.setThermischeNutzLeistung(mastrUnit.getThermischeNutzLeistung());
                }
                if (mastrUnit.getTypenBezeichnung() != null) {
                    existingMastrUnit.setTypenBezeichnung(mastrUnit.getTypenBezeichnung());
                }
                if (mastrUnit.getVollTeilEinspeisung() != null) {
                    existingMastrUnit.setVollTeilEinspeisung(mastrUnit.getVollTeilEinspeisung());
                }
                if (mastrUnit.getVollTeilEinspeisungBezeichnung() != null) {
                    existingMastrUnit.setVollTeilEinspeisungBezeichnung(mastrUnit.getVollTeilEinspeisungBezeichnung());
                }
                if (mastrUnit.getWindparkName() != null) {
                    existingMastrUnit.setWindparkName(mastrUnit.getWindparkName());
                }
                if (mastrUnit.getGeometry() != null) {
                    existingMastrUnit.setGeometry(mastrUnit.getGeometry());
                }

                return existingMastrUnit;
            })
            .map(mastrUnitRepository::save);
    }

    /**
     * Get all the mastrUnits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MastrUnit> findAll(Pageable pageable) {
        log.debug("Request to get all MastrUnits");
        return mastrUnitRepository.findAll(pageable);
    }

    /**
     * Get one mastrUnit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MastrUnit> findOne(Long id) {
        log.debug("Request to get MastrUnit : {}", id);
        return mastrUnitRepository.findById(id);
    }

    /**
     * Delete the mastrUnit by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MastrUnit : {}", id);
        mastrUnitRepository.deleteById(id);
    }

    @PostConstruct
    @Transactional
    public void downloadAndImportMastr() throws IOException {

        if ( this.applicationProperties.getDownloadMastrOnStartup() ) {

            log.info(String.format("Starting to download MaStR."));
            String SERVICE_URL = "https://www.marktstammdatenregister.de/MaStR/Einheit/EinheitJson/GetErweiterteOeffentlicheEinheitStromerzeugung";
            JsonAdapter<MastrResult> adapter =
                new Moshi.Builder()
                    .add(new LocalDateAdapter())
                    .add(new GeometryAdapter())
                    .add(new InstantAdapter())
                    .build().adapter(MastrResult.class);

            OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

            Integer page = 1;
            Integer pageSize = 25000;
            this.mastrUnitRepository.deleteAll();

            while (true) {

                long start = System.currentTimeMillis();
                String url = String.format("%s?page=%d&pageSize=%d&group=&filter=Bundesland~eq~%%27%s%%27", SERVICE_URL, page, pageSize, this.applicationProperties.getStateId());
                log.info(String.format("Downloading with URL: '%s'.", url));
                Request request = new Request.Builder()
                    .url(url)
                    .build();

                try (Response response = client.newCall(request).execute()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                    else log.info(String.format("Request #%d finished in %dms.", page, System.currentTimeMillis() - start));

                    start = System.currentTimeMillis();
                    String jsonString = response.body().string();
                    MastrResult result = adapter.fromJson(jsonString);

                    // aue-bad schlema has lots of wrong (probably AGS before merge) AGS
                    result.getData()
                        .parallelStream()
                        .filter(unit -> unit.getAgs() != null && (unit.getAgs() == 14521030L || unit.getAgs() == 14521050L))
                        .forEach(unit -> unit.setAgs(14521035L));
                    result.getData()
                        .parallelStream()
                        .filter(mastrUnit -> mastrUnit.getAgs() != null && mastrUnit.getLat() != null && mastrUnit.getLng() != null)
                        .forEach(mastrUnit ->
                            mastrUnit.setGeometry(JTS.to(point(WGS84, g(mastrUnit.getLng(), mastrUnit.getLat())))));

                    Map<Long, AdministrationUnit> adminUnitCache = new HashMap<>();

                    for ( MastrUnit unit : result.getData() ) {
                        if ( !adminUnitCache.containsKey(unit.getAgs())) {
                            AdministrationUnit au = this.administrationUnitRepository.findByGemeindeschluesselaufgefuelltAndAde(String.format("%08d", unit.getAgs()), AdministrativeLevel.COMMUNITY.getLevel());
                            adminUnitCache.put(unit.getAgs(), au);
                        }
                        unit.setAdministrationUnit(adminUnitCache.get(unit.getAgs()));
                    }
                    
                    // we need to save each batch, since saving all at once after the download
                    // results in memory overflow for large states like bayern
                    this.saveAll(result.getData());
                    log.info(String.format("Internal processing of request #%d finished in %dms.", page++, System.currentTimeMillis() - start));

                    if ( result.getData().size() < pageSize)
                        break;
                }
            }
            this.createAdministrationUnitStatistics();
        }
        else {
            log.warn(this.mastrUnitRepository.count() == 0 ? "Alert: There are zero MastrUnits in the database. Please import!" : "Importing of MaStR disabled.");
        }
    }

    /**
     * This method generates statistics for all administration units on the community as well as 
     * the district level regarding MaStR data.
     * We cannot use @PostConstruct since the method needs a prefilled database with MaStR data
     * and the order in with @PostConstruct annotated methods in Spring is not garantueed. 
     */
    // @PostConstruct
    private void createAdministrationUnitStatistics() {

      // delete all the old ones
      this.statisticsRepository.deleteBySource(MASTR);

      log.info("Starting to create MaStr energy type statistics for districts.");
      Map<AdministrationUnit, List<MastrUnit>> unitsInDistrict = this.mastrUnitRepository.findAll().stream()
        .filter(x -> !"SSE908588096555".equals(x.getGruppierungsObjekte()))
        .filter(x -> x.getAdministrationUnit() != null && x.getAdministrationUnit().getDistrict() != null)
        .filter(x -> OperationState.ACTIVE.getId().equals(x.getBetriebsStatusId()))
        .filter(x -> x.getInbetriebnahmeDatum() != null && (x.getInbetriebnahmeDatum().isBefore(LocalDate.now()) || x.getInbetriebnahmeDatum().isEqual(LocalDate.now())))
        .collect(Collectors.groupingBy(x -> x.getAdministrationUnit().getDistrict()));
      createStatistics(unitsInDistrict);

      log.info("Starting to create MaStr energy type statistics for communities.");
      Map<AdministrationUnit, List<MastrUnit>> unitsInCommunity = this.mastrUnitRepository.findAll().stream()
        .filter(x -> x.getAdministrationUnit() != null && x.getAdministrationUnit().getDistrict() != null)
        .filter(x -> OperationState.ACTIVE.getId().equals(x.getBetriebsStatusId()))
        .filter(x -> x.getInbetriebnahmeDatum() != null && (x.getInbetriebnahmeDatum().isBefore(LocalDate.now()) || x.getInbetriebnahmeDatum().isEqual(LocalDate.now())))
        .collect(Collectors.groupingBy(x -> x.getAdministrationUnit()));
      createStatistics(unitsInCommunity);
    }

    /**
     * @param unitsInAdministrationUnit a list 
     */
    private void createStatistics(Map<AdministrationUnit, List<MastrUnit>> unitsInAdministrationUnit) {

      for ( Map.Entry<AdministrationUnit, List<MastrUnit>> entry : unitsInAdministrationUnit.entrySet() ) {
        AdministrationUnit unit = this.administrationUnitRepository.findOneWithToOneRelationships(entry.getKey().getId()).get();

        Arrays.asList("Solare Strahlungsenergie", "Speicher", "Biomasse", "Wasser", "Wind").forEach(energieTraegerName -> {
            List<MastrUnit> matchingUnits = entry.getValue().parallelStream()
              .filter(mu -> mu.getEnergieTraegerName().equals(energieTraegerName)).collect(Collectors.toList());
            this.mastrForAdminUnit(unit, matchingUnits, energieTraegerName);
        });
        this.statisticsRepository.saveAll(unit.getStatistics());
      } 
    }

    private void mastrForAdminUnit(AdministrationUnit unit, List<MastrUnit> units, String energieTraegerName){

      Statistic s1 = this.createStatistic(energieTraegerName + "_sum", "kW", unit, "Leistung " + getName(energieTraegerName), 
        units.parallelStream().mapToDouble(mu -> mu.getBruttoLeistung()).sum());
      if ( s1.getValue() != null ) unit.addStatistic(s1);

      Statistic s2 = this.createStatistic(energieTraegerName + "_sum_this_year", "kW", unit, "Leistung " + getName(energieTraegerName) + " ("+ LocalDate.now().getYear() +")", 
        units.parallelStream().filter(mu -> mu.getInbetriebnahmeDatum().isAfter(LocalDate.now().with(firstDayOfYear()))).mapToDouble(mu -> mu.getBruttoLeistung()).sum());
      if ( s2.getValue() != null ) unit.addStatistic(s2);

      Statistic s3 = this.createStatistic(energieTraegerName + "_average", "kW", unit, "⌀ Leistung " + getName(energieTraegerName), 
        units.parallelStream().mapToDouble(mu -> mu.getBruttoLeistung()).summaryStatistics().getAverage());
      if ( s3.getValue() != null ) unit.addStatistic(s3);

      Statistic s4 = this.createStatistic(energieTraegerName + "_count", "", unit, "Anzahl " + getName(energieTraegerName), 
        (double) units.parallelStream().mapToDouble(mu -> mu.getBruttoLeistung()).summaryStatistics().getCount());
      if ( s4.getValue() != null ) unit.addStatistic(s4);

      if ( "Speicher".equals(energieTraegerName) ) {

        Statistic s5 = this.createStatistic(energieTraegerName + "_capacity_sum", "kWh", unit, "Speicherkapazität (gesamt)", 
          units.parallelStream().filter(mu -> mu.getNutzbareSpeicherKapazitaet() != null).mapToDouble(mu -> mu.getNutzbareSpeicherKapazitaet()).sum());
        if ( s5.getValue() != null ) unit.addStatistic(s5);

        Statistic s6 = this.createStatistic(energieTraegerName + "_capacity_sum_this_year", "kWh", unit, "Speicherkapazität ("+ LocalDate.now().getYear() +")", 
          units.parallelStream().filter(mu -> mu. getNutzbareSpeicherKapazitaet() != null && mu.getInbetriebnahmeDatum().isAfter(LocalDate.now().with(firstDayOfYear()))).mapToDouble(mu -> mu.getNutzbareSpeicherKapazitaet()).sum());
        if ( s6.getValue() != null ) unit.addStatistic(s6);

        Statistic s7 = this.createStatistic(energieTraegerName + "_capacity_average", "kWh", unit, "⌀ Speicherkapazität", 
          units.parallelStream().filter(mu -> mu.getNutzbareSpeicherKapazitaet() != null).mapToDouble(mu -> mu.getNutzbareSpeicherKapazitaet()).summaryStatistics().getAverage());
        if ( s7.getValue() != null ) unit.addStatistic(s7);
      }

      if ( "Solare Strahlungsenergie".equals(energieTraegerName) || "Wind".equals(energieTraegerName) ) {

        Double[][] intervals = "Solare Strahlungsenergie".equals(energieTraegerName) ? 
          new Double[][]{ {0.0, 1.}, {1., 10.}, {10.0, 1000.}, {1000., Double.MAX_VALUE} } : 
          new Double[][]{ {0.0, 1000.}, {1000., 3000.}, {3000.0, 5000.}, {5000.0, 7000.}, {7000., 10000.}, {10000.0, Double.MAX_VALUE} };

        for ( Double[] interval : intervals ) {

          Double min = interval[0], max = interval[1];
          Double sum = units.stream().filter(mu -> mu.getBruttoLeistung() >= min && mu.getBruttoLeistung() < max).mapToDouble(MastrUnit::getBruttoLeistung).sum();
          Double count = (double) units.stream().filter(mu -> mu.getBruttoLeistung() >= min && mu.getBruttoLeistung() < max).count();

          Statistic s8 = this.createStatistic(
            String.format(Locale.US, "%s_count_%.1f_%.1f", energieTraegerName, min, Double.MAX_VALUE == max ? 1000000 : max), "", unit,
            String.format("Anzahl %s (%.1f bis %.1f kW)", getName(energieTraegerName), min, Double.MAX_VALUE == max ? 1000000 : max), count);
          unit.addStatistic(s8);

          Statistic s9 = this.createStatistic(
            String.format(Locale.US, "%s_sum_%.1f_%.1f", energieTraegerName, min, Double.MAX_VALUE == max ? 1000000 : max), "kW", unit, 
            String.format("Leistung %s (%.1f bis %.1f kW)", getName(energieTraegerName), min, Double.MAX_VALUE == max ? 1000000 : max), sum);
          unit.addStatistic(s9);
        }
      } 
    }

    private String getName(String energieTraegerName) {
      
      switch (energieTraegerName) {
        case "Wind":
          return "Windkraftanlagen";
        case "Solare Strahlungsenergie":
          return "PV-Anlagen";
        case "Biomasse":
          return "Biomasseanlagen";
        case "Speicher":
          return "Speicher";
        case "Wasser":
          return "Wasserkraftanlagen";
        default:
          throw new RuntimeException("unsupported energy type");
      }
    }

    private Statistic createStatistic(String key, String unit, AdministrationUnit adminUnit, String name, double value) {

      return new Statistic()
        .key(key)
        .source(MASTR)
        .unit(unit)
        .administrationUnit(adminUnit)
        .date(LocalDate.now())
        .name(name)
        .value(Double.isInfinite(value) ? null : value);
    }
}
