package eu.danielgerber.service;

import eu.danielgerber.domain.*; // for static metamodels
import eu.danielgerber.domain.DWDMeasurement;
import eu.danielgerber.repository.DWDMeasurementRepository;
import eu.danielgerber.service.criteria.DWDMeasurementCriteria;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link DWDMeasurement} entities in the database.
 * The main input is a {@link DWDMeasurementCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link DWDMeasurement} or a {@link Page} of {@link DWDMeasurement} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class DWDMeasurementQueryService extends QueryService<DWDMeasurement> {

    private final Logger log = LoggerFactory.getLogger(DWDMeasurementQueryService.class);

    private final DWDMeasurementRepository dWDMeasurementRepository;

    public DWDMeasurementQueryService(DWDMeasurementRepository dWDMeasurementRepository) {
        this.dWDMeasurementRepository = dWDMeasurementRepository;
    }

    /**
     * Return a {@link List} of {@link DWDMeasurement} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<DWDMeasurement> findByCriteria(DWDMeasurementCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<DWDMeasurement> specification = createSpecification(criteria);
        return dWDMeasurementRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link DWDMeasurement} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<DWDMeasurement> findByCriteria(DWDMeasurementCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<DWDMeasurement> specification = createSpecification(criteria);
        return dWDMeasurementRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(DWDMeasurementCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<DWDMeasurement> specification = createSpecification(criteria);
        return dWDMeasurementRepository.count(specification);
    }

    /**
     * Function to convert {@link DWDMeasurementCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<DWDMeasurement> createSpecification(DWDMeasurementCriteria criteria) {
        Specification<DWDMeasurement> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), DWDMeasurement_.id));
            }
            if (criteria.getYear() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getYear(), DWDMeasurement_.year));
            }
            if (criteria.getStatistic() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatistic(), DWDMeasurement_.statistic));
            }
            if (criteria.getBrandenburgBerlin() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getBrandenburgBerlin(), DWDMeasurement_.brandenburgBerlin));
            }
            if (criteria.getBrandenburg() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBrandenburg(), DWDMeasurement_.brandenburg));
            }
            if (criteria.getBadenWuerttemberg() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getBadenWuerttemberg(), DWDMeasurement_.badenWuerttemberg));
            }
            if (criteria.getBayern() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBayern(), DWDMeasurement_.bayern));
            }
            if (criteria.getHessen() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getHessen(), DWDMeasurement_.hessen));
            }
            if (criteria.getMecklenburgVorpommern() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getMecklenburgVorpommern(), DWDMeasurement_.mecklenburgVorpommern));
            }
            if (criteria.getNiedersachsen() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNiedersachsen(), DWDMeasurement_.niedersachsen));
            }
            if (criteria.getNiedersachsenHamburgBremen() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getNiedersachsenHamburgBremen(), DWDMeasurement_.niedersachsenHamburgBremen)
                    );
            }
            if (criteria.getNordrheinWestfalen() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getNordrheinWestfalen(), DWDMeasurement_.nordrheinWestfalen));
            }
            if (criteria.getRheinlandPfalz() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRheinlandPfalz(), DWDMeasurement_.rheinlandPfalz));
            }
            if (criteria.getSchleswigHolstein() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getSchleswigHolstein(), DWDMeasurement_.schleswigHolstein));
            }
            if (criteria.getSaarland() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSaarland(), DWDMeasurement_.saarland));
            }
            if (criteria.getSachsen() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSachsen(), DWDMeasurement_.sachsen));
            }
            if (criteria.getSachsenAnhalt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSachsenAnhalt(), DWDMeasurement_.sachsenAnhalt));
            }
            if (criteria.getThueringenSachsenAnhalt() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getThueringenSachsenAnhalt(), DWDMeasurement_.thueringenSachsenAnhalt)
                    );
            }
            if (criteria.getThueringen() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThueringen(), DWDMeasurement_.thueringen));
            }
            if (criteria.getDeutschland() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeutschland(), DWDMeasurement_.deutschland));
            }
        }
        return specification;
    }
}
