package eu.danielgerber.service;

import eu.danielgerber.config.ApplicationProperties;
import eu.danielgerber.domain.UFZMeasurement;
import eu.danielgerber.domain.UFZRasterPoint;
import eu.danielgerber.repository.UFZMeasurementRepository;
import eu.danielgerber.repository.UFZRasterPointRepository;
import ucar.ma2.Array;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.NetcdfFiles;
import ucar.nc2.Variable;

import static org.geolatte.geom.builder.DSL.g;
import static org.geolatte.geom.builder.DSL.point;
import static org.geolatte.geom.crs.CoordinateReferenceSystems.WGS84;

import org.geotools.referencing.CRS;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.geolatte.geom.jts.JTS;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link UFZMeasurement}.
 */
@Service
@Transactional
public class UFZMeasurementService {

  private final Logger log = LoggerFactory.getLogger(UFZMeasurementService.class);

  private final UFZMeasurementRepository uFZMeasurementRepository;
  private final UFZRasterPointRepository uFZRasterPointRepository;

  private ApplicationProperties applicationProperties;

  public UFZMeasurementService(UFZMeasurementRepository uFZMeasurementRepository,
      UFZRasterPointRepository uFZRasterPointRepository, ApplicationProperties applicationProperties) {
    this.uFZMeasurementRepository = uFZMeasurementRepository;
    this.uFZRasterPointRepository = uFZRasterPointRepository;
    this.applicationProperties = applicationProperties;
  }

  /**
   * Save a uFZMeasurement.
   *
   * @param uFZMeasurement the entity to save.
   * @return the persisted entity.
   */
  public UFZMeasurement save(UFZMeasurement uFZMeasurement) {
    log.debug("Request to save UFZMeasurement : {}", uFZMeasurement);
    return uFZMeasurementRepository.save(uFZMeasurement);
  }

  /**
   * Update a uFZMeasurement.
   *
   * @param uFZMeasurement the entity to save.
   * @return the persisted entity.
   */
  public UFZMeasurement update(UFZMeasurement uFZMeasurement) {
    log.debug("Request to update UFZMeasurement : {}", uFZMeasurement);
    return uFZMeasurementRepository.save(uFZMeasurement);
  }

  /**
   * Partially update a uFZMeasurement.
   *
   * @param uFZMeasurement the entity to update partially.
   * @return the persisted entity.
   */
  public Optional<UFZMeasurement> partialUpdate(UFZMeasurement uFZMeasurement) {
    log.debug("Request to partially update UFZMeasurement : {}", uFZMeasurement);

    return uFZMeasurementRepository
        .findById(uFZMeasurement.getId())
        .map(existingUFZMeasurement -> {
          if (uFZMeasurement.getSmiGesamtboden() != null) {
            existingUFZMeasurement.setSmiGesamtboden(uFZMeasurement.getSmiGesamtboden());
          }
          if (uFZMeasurement.getSmiOberboden() != null) {
            existingUFZMeasurement.setSmiOberboden(uFZMeasurement.getSmiOberboden());
          }
          if (uFZMeasurement.getNutzbareFeldkapazitaet() != null) {
            existingUFZMeasurement.setNutzbareFeldkapazitaet(uFZMeasurement.getNutzbareFeldkapazitaet());
          }
          if (uFZMeasurement.getDate() != null) {
            existingUFZMeasurement.setDate(uFZMeasurement.getDate());
          }

          return existingUFZMeasurement;
        })
        .map(uFZMeasurementRepository::save);
  }

  /**
   * Get all the uFZMeasurements.
   *
   * @return the list of entities.
   */
  @Transactional(readOnly = true)
  public List<UFZMeasurement> findAll() {
    log.debug("Request to get all UFZMeasurements");
    return uFZMeasurementRepository.findAll();
  }

  /**
   * Get one uFZMeasurement by id.
   *
   * @param id the id of the entity.
   * @return the entity.
   */
  @Transactional(readOnly = true)
  public Optional<UFZMeasurement> findOne(Long id) {
    log.debug("Request to get UFZMeasurement : {}", id);
    return uFZMeasurementRepository.findById(id);
  }

  /**
   * Delete the uFZMeasurement by id.
   *
   * @param id the id of the entity.
   */
  public void delete(Long id) {
    log.debug("Request to delete UFZMeasurement : {}", id);
    uFZMeasurementRepository.deleteById(id);
  }

  @PostConstruct
  @Transactional
  public void downloadAndImportUfzDuerreMonitor() throws IOException, ParseException, InvalidRangeException, MismatchedDimensionException, TransformException, FactoryException {

    if (this.applicationProperties.getDownloadUfzOnStartup()) {

      this.log.info("Starting to download UFZ files.");
      // download the file and store it in a temp file
      InputStream in1 = new URL("https://files.ufz.de/~drought/SM_L02_daily_n14.nc").openStream();
      Path oberboden = Files.createTempFile(null, null);
      Files.copy(in1, oberboden, StandardCopyOption.REPLACE_EXISTING);
      InputStream in2 = new URL("https://files.ufz.de/~drought/SM_Lall_daily_n14.nc").openStream();
      Path gesamtboden = Files.createTempFile(null, null);
      Files.copy(in2, gesamtboden, StandardCopyOption.REPLACE_EXISTING);
      InputStream in3 = new URL("https://files.ufz.de/~drought/nFK_0_25_daily_n14.nc").openStream();
      Path nfk = Files.createTempFile(null, null);
      Files.copy(in3, nfk, StandardCopyOption.REPLACE_EXISTING);
      this.log.info("Finished downloading of UFZ files.");

      // Open the netCDF files
      try (var oberbodenFile = NetcdfFiles.open(oberboden.toAbsolutePath().toString())) {
        try (var gesamtBodenFile = NetcdfFiles.open(gesamtboden.toAbsolutePath().toString())) {
          try (var nFKFile = NetcdfFiles.open(nfk.toAbsolutePath().toString())) {

            Map<Long, UFZRasterPoint> idsToPoints = uFZRasterPointRepository.findAllAndFetchMeasurementsEagerly().stream()
                .collect(Collectors.toMap(UFZRasterPoint::getId, Function.identity()));

            Variable timesVariable = oberbodenFile.findVariable("time");
            Array timesArray = timesVariable.read();
            String timeUnits = timesVariable.getUnitsString().replace("days since ", "").replace(" 00:00:00", "");
            LocalDate baseDate = LocalDate.parse(timeUnits, DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            for (int dateIndex = 0; dateIndex < 14; dateIndex++) {

              this.log.info("Starting to import UFZ files for date: '{}'.", baseDate.plusDays(timesArray.getInt(dateIndex)));

              Array lat = oberbodenFile.findVariable("lat").read();
              Array lng = oberbodenFile.findVariable("lon").read();
              Array SMI_oberboden = oberbodenFile.findVariable("SMI").read(dateIndex + ",:,:");
              Array SMI_gesamtboden = gesamtBodenFile.findVariable("SMI").read(dateIndex + ",:,:");
              Array nFK = nFKFile.findVariable("nFK").read(dateIndex + ",:,:");

              for (int i = 0; i < lat.getSize(); i++) {
                if (SMI_oberboden.getFloat(i) != -9999.0 && SMI_gesamtboden.getFloat(i) != -9999.0
                    && nFK.getFloat(i) != -9999.0) {

                  Point point = JTS.to(point(WGS84, g(lng.getDouble(i), lat.getDouble(i))));

                  // DANGER: raster points do not have IDs, but they come in the same order in all
                  // files so in order to group measurements by rasterpoint we can either match lat/lng
                  // or use the index
                  UFZRasterPoint p = new UFZRasterPoint().id((long) i)
                      .lat(lat.getDouble(i))
                      .lng(lng.getDouble(i))
                      .geometry(UFZMeasurementService.createRectangularBuffer(point));

                  if (!idsToPoints.containsKey((long) i))
                    idsToPoints.put((long) i, p);

                  UFZRasterPoint ufzRasterPoint = idsToPoints.get((long) i);

                  if (!ufzRasterPoint.hasMeasurementForDate(baseDate.plusDays(timesArray.getInt(dateIndex)))) {
                    ufzRasterPoint.addUfzMeasurement(
                        new UFZMeasurement()
                            .date(baseDate.plusDays(timesArray.getInt(dateIndex)))
                            .smiGesamtboden((double) SMI_gesamtboden.getFloat(i))
                            .smiOberboden((double) SMI_oberboden.getFloat(i))
                            .nutzbareFeldkapazitaet((double) nFK.getFloat(i)));
                  }
                }
              }
            }

            this.log.info("Starting to save ufz raster points and measurements to database.");
            this.uFZRasterPointRepository.saveAll(idsToPoints.values());
            this.log.info(
                "Deleting all raster points and measurements which are not in the configured state: {} with id {}.",
                this.applicationProperties.getState(), this.applicationProperties.getAgsStateId());
            // the nc file is for all of germany, we need to delete the rest
            // we could check for each point if it is inside a given polygon,
            // but this would be very many very slow point-in-polygon tests
            this.uFZRasterPointRepository.deleteAllNotInState(this.applicationProperties.getAgsStateId());
          }
        }
      }
    }
    else {
      log.warn(this.uFZRasterPointRepository.count() == 0 ? "Alert: There are zero raster points in the database. Please import!" : "Importing of UFZ data disabled.");
    }
  }

  /**
     * Creates a rectangular buffer around a point.
     * 
     * @param point The point to create a buffer around.
     * @param width The width of the buffer rectangle.
     * @param height The height of the buffer rectangle.
     * 
     * @return A Geometry representing the rectangular buffer.
     * 
     * @throws TransformException 
     * @throws MismatchedDimensionException 
       @throws FactoryException 
     */
    public static Geometry createRectangularBuffer(Point point) throws MismatchedDimensionException, TransformException, FactoryException {
        
        GeometryFactory factory = new GeometryFactory();
        
        // Calculate half width and half height for offsetting the rectangle
        double halfWidth = 4000 / 2.0;
        double halfHeight = 6500 / 2.0;

        // Define the source and target coordinate reference systems
        CoordinateReferenceSystem sourceCRS = CRS.decode("EPSG:4326");
        CoordinateReferenceSystem targetCRS = CRS.decode("EPSG:3857");
        
        // Find a math transform between the source CRS and the target CRS
        MathTransform transform = CRS.findMathTransform(sourceCRS, targetCRS, true);
        
        // Use the transform to transform the point
        Geometry targetGeometry3857 = org.geotools.geometry.jts.JTS.transform(point, transform);

        // Get the coordinates of the point
        Coordinate center = targetGeometry3857.getCoordinate();
        
        // Calculate the coordinates of the rectangle's corners
        Coordinate[] coords = new Coordinate[]{
            new Coordinate(center.x - halfWidth, center.y - halfHeight),
            new Coordinate(center.x - halfWidth, center.y + halfHeight),
            new Coordinate(center.x + halfWidth, center.y + halfHeight),
            new Coordinate(center.x + halfWidth, center.y - halfHeight),
            new Coordinate(center.x - halfWidth, center.y - halfHeight) // Close the linear ring
        };

        // Create a LinearRing from the corner coordinates
        LinearRing shell = factory.createLinearRing(coords);
        shell.setSRID(4326);
        
        // Create a Polygon (rectangle) from the LinearRing
        Polygon rectangle = factory.createPolygon(shell, null);
        rectangle.setSRID(4326);

        // Create a Geometry from the polygon
        Geometry bufferedGeom4326 = org.geotools.geometry.jts.JTS.transform(rectangle, transform.inverse());
        bufferedGeom4326.setSRID(4326);
        
        return bufferedGeom4326;
    }

  public static void main(String[] args) {
    System.out.println(LocalDate.of(2016, 1, 1));
  }
}
