package eu.danielgerber.service;

import eu.danielgerber.domain.UFZRasterPoint;
import eu.danielgerber.repository.UFZRasterPointRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link UFZRasterPoint}.
 */
@Service
@Transactional
public class UFZRasterPointService {

    private final Logger log = LoggerFactory.getLogger(UFZRasterPointService.class);

    private final UFZRasterPointRepository uFZRasterPointRepository;

    public UFZRasterPointService(UFZRasterPointRepository uFZRasterPointRepository) {
        this.uFZRasterPointRepository = uFZRasterPointRepository;
    }

    /**
     * Save a uFZRasterPoint.
     *
     * @param uFZRasterPoint the entity to save.
     * @return the persisted entity.
     */
    public UFZRasterPoint save(UFZRasterPoint uFZRasterPoint) {
        log.debug("Request to save UFZRasterPoint : {}", uFZRasterPoint);
        return uFZRasterPointRepository.save(uFZRasterPoint);
    }

    /**
     * Update a uFZRasterPoint.
     *
     * @param uFZRasterPoint the entity to save.
     * @return the persisted entity.
     */
    public UFZRasterPoint update(UFZRasterPoint uFZRasterPoint) {
        log.debug("Request to update UFZRasterPoint : {}", uFZRasterPoint);
        return uFZRasterPointRepository.save(uFZRasterPoint);
    }

    /**
     * Partially update a uFZRasterPoint.
     *
     * @param uFZRasterPoint the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<UFZRasterPoint> partialUpdate(UFZRasterPoint uFZRasterPoint) {
        log.debug("Request to partially update UFZRasterPoint : {}", uFZRasterPoint);

        return uFZRasterPointRepository
            .findById(uFZRasterPoint.getId())
            .map(existingUFZRasterPoint -> {
                if (uFZRasterPoint.getLat() != null) {
                    existingUFZRasterPoint.setLat(uFZRasterPoint.getLat());
                }
                if (uFZRasterPoint.getLng() != null) {
                    existingUFZRasterPoint.setLng(uFZRasterPoint.getLng());
                }
                if (uFZRasterPoint.getGeometry() != null) {
                    existingUFZRasterPoint.setGeometry(uFZRasterPoint.getGeometry());
                }

                return existingUFZRasterPoint;
            })
            .map(uFZRasterPointRepository::save);
    }

    /**
     * Get all the uFZRasterPoints.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<UFZRasterPoint> findAll() {
        log.debug("Request to get all UFZRasterPoints");
        return uFZRasterPointRepository.findAll();
    }

    /**
     * Get one uFZRasterPoint by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UFZRasterPoint> findOne(Long id) {
        log.debug("Request to get UFZRasterPoint : {}", id);
        return uFZRasterPointRepository.findById(id);
    }

    /**
     * Delete the uFZRasterPoint by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete UFZRasterPoint : {}", id);
        uFZRasterPointRepository.deleteById(id);
    }
}
