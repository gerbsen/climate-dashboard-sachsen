package eu.danielgerber.service;

import eu.danielgerber.domain.*; // for static metamodels
import eu.danielgerber.domain.AdministrationUnit;
import eu.danielgerber.repository.AdministrationUnitRepository;
import eu.danielgerber.service.criteria.AdministrationUnitCriteria;
import java.util.List;
import javax.persistence.criteria.JoinType;

import org.locationtech.jts.geom.Geometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link AdministrationUnit} entities in the database.
 * The main input is a {@link AdministrationUnitCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AdministrationUnit} or a {@link Page} of {@link AdministrationUnit} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AdministrationUnitQueryService extends QueryService<AdministrationUnit> {

    private final Logger log = LoggerFactory.getLogger(AdministrationUnitQueryService.class);

    private final AdministrationUnitRepository administrationUnitRepository;

    public AdministrationUnitQueryService(AdministrationUnitRepository administrationUnitRepository) {
        this.administrationUnitRepository = administrationUnitRepository;
    }

    /**
     * Return a {@link List} of {@link AdministrationUnit} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AdministrationUnit> findByCriteria(AdministrationUnitCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AdministrationUnit> specification = createSpecification(criteria);
        return administrationUnitRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AdministrationUnit} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AdministrationUnit> findByCriteria(AdministrationUnitCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AdministrationUnit> specification = createSpecification(criteria);
        return administrationUnitRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AdministrationUnitCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AdministrationUnit> specification = createSpecification(criteria);
        return administrationUnitRepository.count(specification);
    }

    /**
     * Return the administration unit (admin_ebene_ade == 6) a given point lays in, of found, else null
     *
     * @param geometry, the geometry to check
     * @return an administration unit
     */
    public AdministrationUnit getAdministrationUnitByStContainsPoint(Geometry geometry) {
        return this.administrationUnitRepository.findByStContainsPoint(geometry);
    }

    /**
     * Function to convert {@link AdministrationUnitCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AdministrationUnit> createSpecification(AdministrationUnitCriteria criteria) {
        Specification<AdministrationUnit> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), AdministrationUnit_.id));
            }
            if (criteria.getObjektidentifikator() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getObjektidentifikator(), AdministrationUnit_.objektidentifikator));
            }
            if (criteria.getBeginnlebenszeit() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getBeginnlebenszeit(), AdministrationUnit_.beginnlebenszeit));
            }
            if (criteria.getAdminEbeneAde() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAdminEbeneAde(), AdministrationUnit_.adminEbeneAde));
            }
            if (criteria.getAde() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAde(), AdministrationUnit_.ade));
            }
            if (criteria.getGeofaktorGf() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGeofaktorGf(), AdministrationUnit_.geofaktorGf));
            }
            if (criteria.getGf() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGf(), AdministrationUnit_.gf));
            }
            if (criteria.getBesondereGebieteBsg() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getBesondereGebieteBsg(), AdministrationUnit_.besondereGebieteBsg));
            }
            if (criteria.getBsg() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBsg(), AdministrationUnit_.bsg));
            }
            if (criteria.getRegionalschluesselArs() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getRegionalschluesselArs(), AdministrationUnit_.regionalschluesselArs)
                    );
            }
            if (criteria.getGemeindeschluesselAgs() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getGemeindeschluesselAgs(), AdministrationUnit_.gemeindeschluesselAgs)
                    );
            }
            if (criteria.getVerwaltungssitzSdvArs() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getVerwaltungssitzSdvArs(), AdministrationUnit_.verwaltungssitzSdvArs)
                    );
            }
            if (criteria.getGeografischernameGen() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getGeografischernameGen(), AdministrationUnit_.geografischernameGen)
                    );
            }
            if (criteria.getBezeichnung() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBezeichnung(), AdministrationUnit_.bezeichnung));
            }
            if (criteria.getIdentifikatorIbz() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getIdentifikatorIbz(), AdministrationUnit_.identifikatorIbz));
            }
            if (criteria.getBemerkung() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBemerkung(), AdministrationUnit_.bemerkung));
            }
            if (criteria.getNamensbildungNbd() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getNamensbildungNbd(), AdministrationUnit_.namensbildungNbd));
            }
            if (criteria.getNbd() != null) {
                specification = specification.and(buildSpecification(criteria.getNbd(), AdministrationUnit_.nbd));
            }
            if (criteria.getLand() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLand(), AdministrationUnit_.land));
            }
            if (criteria.getRegierungsbezirk() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getRegierungsbezirk(), AdministrationUnit_.regierungsbezirk));
            }
            if (criteria.getKreis() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKreis(), AdministrationUnit_.kreis));
            }
            if (criteria.getVerwaltungsgemeinschaftteil1() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(
                            criteria.getVerwaltungsgemeinschaftteil1(),
                            AdministrationUnit_.verwaltungsgemeinschaftteil1
                        )
                    );
            }
            if (criteria.getVerwaltungsgemeinschaftteil2() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(
                            criteria.getVerwaltungsgemeinschaftteil2(),
                            AdministrationUnit_.verwaltungsgemeinschaftteil2
                        )
                    );
            }
            if (criteria.getGemeinde() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGemeinde(), AdministrationUnit_.gemeinde));
            }
            if (criteria.getFunkSchluesselstelle3() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getFunkSchluesselstelle3(), AdministrationUnit_.funkSchluesselstelle3)
                    );
            }
            if (criteria.getFkS3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFkS3(), AdministrationUnit_.fkS3));
            }
            if (criteria.getEuropStatistikschluesselNuts() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(
                            criteria.getEuropStatistikschluesselNuts(),
                            AdministrationUnit_.europStatistikschluesselNuts
                        )
                    );
            }
            if (criteria.getRegioschluesselaufgefuellt() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getRegioschluesselaufgefuellt(), AdministrationUnit_.regioschluesselaufgefuellt)
                    );
            }
            if (criteria.getGemeindeschluesselaufgefuellt() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(
                            criteria.getGemeindeschluesselaufgefuellt(),
                            AdministrationUnit_.gemeindeschluesselaufgefuellt
                        )
                    );
            }
            if (criteria.getWirksamkeitWsk() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getWirksamkeitWsk(), AdministrationUnit_.wirksamkeitWsk));
            }
            if (criteria.getDistrictId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getDistrictId(),
                            root -> root.join(AdministrationUnit_.district, JoinType.LEFT).get(AdministrationUnit_.id)
                        )
                    );
            }
            if (criteria.getMastrUnitId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getMastrUnitId(),
                            root -> root.join(AdministrationUnit_.mastrUnits, JoinType.LEFT).get(MastrUnit_.id)
                        )
                    );
            }
            if (criteria.getChargingStationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getChargingStationId(),
                            root -> root.join(AdministrationUnit_.chargingStations, JoinType.LEFT).get(ChargingStation_.id)
                        )
                    );
            }
            if (criteria.getStatisticId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getStatisticId(),
                            root -> root.join(AdministrationUnit_.statistics, JoinType.LEFT).get(Statistic_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
