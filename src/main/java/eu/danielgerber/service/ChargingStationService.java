package eu.danielgerber.service;

import static eu.danielgerber.Constants.STATISTIC_AREA;
import static eu.danielgerber.Constants.STATISTIC_POPULATION_TOTAL;
import static org.geolatte.geom.builder.DSL.g;
import static org.geolatte.geom.builder.DSL.point;
import static org.geolatte.geom.crs.CoordinateReferenceSystems.WGS84;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.geolatte.geom.jts.JTS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;

import eu.danielgerber.config.ApplicationProperties;
import eu.danielgerber.domain.AdministrationUnit;
import eu.danielgerber.domain.ChargingStation;
import eu.danielgerber.domain.Statistic;
import eu.danielgerber.repository.AdministrationUnitRepository;
import eu.danielgerber.repository.ChargingStationRepository;
import eu.danielgerber.repository.StatisticRepository;

/**
 * Service Implementation for managing {@link ChargingStation}.
 */
@Service
@DependsOn("liquibase")
@Transactional
public class ChargingStationService {

    private static final String B_NETZ_A = "charging-station-service";

    private final Logger log = LoggerFactory.getLogger(ChargingStationService.class);

    private final StatisticRepository statisticRepository;
    private final ChargingStationRepository chargingStationRepository;
    private final AdministrationUnitQueryService administrationUnitQueryService;
    private final AdministrationUnitRepository administrationUnitRepository;
    private ApplicationProperties applicationProperties;

    public ChargingStationService(ChargingStationRepository chargingStationRepository,
                                  AdministrationUnitRepository administrationUnitRepository,
                                  StatisticRepository statisticRepository,
                                  AdministrationUnitQueryService administrationUnitQueryService,
                                  ApplicationProperties applicationProperties) {
        this.chargingStationRepository = chargingStationRepository;
        this.administrationUnitRepository = administrationUnitRepository;
        this.administrationUnitQueryService = administrationUnitQueryService;
        this.statisticRepository = statisticRepository;
        this.applicationProperties = applicationProperties;
    }

    /**
     * Save a chargingStation.
     *
     * @param chargingStation the entity to save.
     * @return the persisted entity.
     */
    public ChargingStation save(ChargingStation chargingStation) {
        log.debug("Request to save ChargingStation : {}", chargingStation);
        return chargingStationRepository.save(chargingStation);
    }

    /**
     * Update a chargingStation.
     *
     * @param chargingStation the entity to save.
     * @return the persisted entity.
     */
    public ChargingStation update(ChargingStation chargingStation) {
        log.debug("Request to update ChargingStation : {}", chargingStation);
        return chargingStationRepository.save(chargingStation);
    }

    /**
     * Partially update a chargingStation.
     *
     * @param chargingStation the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ChargingStation> partialUpdate(ChargingStation chargingStation) {
        log.debug("Request to partially update ChargingStation : {}", chargingStation);

        return chargingStationRepository
            .findById(chargingStation.getId())
            .map(existingChargingStation -> {
                if (chargingStation.getBetreiber() != null) {
                    existingChargingStation.setBetreiber(chargingStation.getBetreiber());
                }
                if (chargingStation.getStrasse() != null) {
                    existingChargingStation.setStrasse(chargingStation.getStrasse());
                }
                if (chargingStation.getHausnummer() != null) {
                    existingChargingStation.setHausnummer(chargingStation.getHausnummer());
                }
                if (chargingStation.getAdresszusatz() != null) {
                    existingChargingStation.setAdresszusatz(chargingStation.getAdresszusatz());
                }
                if (chargingStation.getPostleitzahl() != null) {
                    existingChargingStation.setPostleitzahl(chargingStation.getPostleitzahl());
                }
                if (chargingStation.getOrt() != null) {
                    existingChargingStation.setOrt(chargingStation.getOrt());
                }
                if (chargingStation.getBundesland() != null) {
                    existingChargingStation.setBundesland(chargingStation.getBundesland());
                }
                if (chargingStation.getKreisKreisfreieStadt() != null) {
                    existingChargingStation.setKreisKreisfreieStadt(chargingStation.getKreisKreisfreieStadt());
                }
                if (chargingStation.getInbetriebnahmeDatum() != null) {
                    existingChargingStation.setInbetriebnahmeDatum(chargingStation.getInbetriebnahmeDatum());
                }
                if (chargingStation.getNennleistungLadeeinrichtung() != null) {
                    existingChargingStation.setNennleistungLadeeinrichtung(chargingStation.getNennleistungLadeeinrichtung());
                }
                if (chargingStation.getArtDerLadeeinrichtung() != null) {
                    existingChargingStation.setArtDerLadeeinrichtung(chargingStation.getArtDerLadeeinrichtung());
                }
                if (chargingStation.getAnzahlLadepunkte() != null) {
                    existingChargingStation.setAnzahlLadepunkte(chargingStation.getAnzahlLadepunkte());
                }
                if (chargingStation.getErsterLadepunktSteckertypen() != null) {
                    existingChargingStation.setErsterLadepunktSteckertypen(chargingStation.getErsterLadepunktSteckertypen());
                }
                if (chargingStation.getErsterLadepunktLeistung() != null) {
                    existingChargingStation.setErsterLadepunktLeistung(chargingStation.getErsterLadepunktLeistung());
                }
                if (chargingStation.getZweiterLadepunktSteckertypen() != null) {
                    existingChargingStation.setZweiterLadepunktSteckertypen(chargingStation.getZweiterLadepunktSteckertypen());
                }
                if (chargingStation.getZweiterLadepunktLeistung() != null) {
                    existingChargingStation.setZweiterLadepunktLeistung(chargingStation.getZweiterLadepunktLeistung());
                }
                if (chargingStation.getDritterLadepunktSteckertypen() != null) {
                    existingChargingStation.setDritterLadepunktSteckertypen(chargingStation.getDritterLadepunktSteckertypen());
                }
                if (chargingStation.getDritterLadepunktLeistung() != null) {
                    existingChargingStation.setDritterLadepunktLeistung(chargingStation.getDritterLadepunktLeistung());
                }
                if (chargingStation.getVierterLadepunktSteckertypen() != null) {
                    existingChargingStation.setVierterLadepunktSteckertypen(chargingStation.getVierterLadepunktSteckertypen());
                }
                if (chargingStation.getVierterLadepunktLeistung() != null) {
                    existingChargingStation.setVierterLadepunktLeistung(chargingStation.getVierterLadepunktLeistung());
                }
                if (chargingStation.getGeometry() != null) {
                    existingChargingStation.setGeometry(chargingStation.getGeometry());
                }

                return existingChargingStation;
            })
            .map(chargingStationRepository::save);
    }

    /**
     * Get all the chargingStations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ChargingStation> findAll(Pageable pageable) {
        log.debug("Request to get all ChargingStations");
        return chargingStationRepository.findAll(pageable);
    }

    /**
     * Get one chargingStation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ChargingStation> findOne(Long id) {
        log.debug("Request to get ChargingStation : {}", id);
        return chargingStationRepository.findById(id);
    }

    /**
     * Delete the chargingStation by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ChargingStation : {}", id);
        chargingStationRepository.deleteById(id);
    }

    @PostConstruct
    @Transactional
    public void getBNetzaChargingStatations() throws IOException, CsvValidationException, ParseException, InterruptedException {

        if ( this.applicationProperties.getDownloadLadesaulenRegisterOnStartup() ) {

            // download the file and store it in a temp file
            InputStream in = new URL(this.applicationProperties.getLadesaeulenRegisterUrl()).openStream();
            Path tempFile = Files.createTempFile(null, null);
            Files.copy(in, tempFile, StandardCopyOption.REPLACE_EXISTING);

            // setup csv reader and utils
            List<ChargingStation> stations = new ArrayList<>();
            SimpleDateFormat parser = new SimpleDateFormat("dd.MM.yyyy");
            Map<AdministrationUnit, Set<ChargingStation>> chargingStationsInCommunity = new HashMap<>();
            Map<AdministrationUnit, Set<ChargingStation>> chargingStationsInDistrict = new HashMap<>();
            CSVReader reader = new CSVReaderBuilder(new FileReader(tempFile.toFile(), StandardCharsets.ISO_8859_1))
                    .withCSVParser(new CSVParserBuilder().withSeparator(';').build()).build();

            String[] row;
            int rowNumber = 0;
            LocalDate maxDate = null;
            this.log.info("Deleting all old charging station entries from database.");
            this.chargingStationRepository.deleteAll();
            this.log.info("Deleting all old statistics generated for the charging stations in the administration units from database.");
            this.statisticRepository.deleteBySource(B_NETZ_A);

            while ((row = reader.readNext()) != null) {

                // at this row there is the current date
                if ( rowNumber++ == 7 ) {
                    maxDate = parser.parse(row[0].replace("Letzte Aktualisierung vom: ", "")).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                }
                // real data starts at line 11 (0 indexed)
                else if (rowNumber > 11 ) {
                    // import only those stations which are in our configured state
                    if ( this.applicationProperties.getState().equals(row[8]) ) {

                      ChargingStation chargingStation = new ChargingStation()
                          .betreiber(row[0]).strasse(row[2]).hausnummer(row[3]).adresszusatz(row[4])
                          .postleitzahl(row[5]).ort(row[6]).kreisKreisfreieStadt(row[7]).bundesland(row[8])
                          .geometry(JTS.to(point(WGS84,g(
                            Double.valueOf(row[10].replace(",",".").replaceAll("\\.$", "")),
                            Double.valueOf(row[9].replace(",",".").replaceAll("\\.$", ""))
                          ))))
                          .inbetriebnahmeDatum(parser.parse(row[11]).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                          .nennleistungLadeeinrichtung(Double.valueOf(row[12].replace(",", ".")))
                          .artDerLadeeinrichtung(row[13]).anzahlLadepunkte(Integer.valueOf(row[14]))
                          .ersterLadepunktSteckertypen(row[15]).ersterLadepunktLeistung(parseDouble(row, 16))
                          .zweiterLadepunktSteckertypen(row[18]).zweiterLadepunktLeistung(parseDouble(row, 19))
                          .dritterLadepunktSteckertypen(row[21]).dritterLadepunktLeistung(parseDouble(row, 22))
                          .vierterLadepunktSteckertypen(row[24]).vierterLadepunktLeistung(parseDouble(row, 25));
                      chargingStation.administrationUnit(this.administrationUnitQueryService.
                              getAdministrationUnitByStContainsPoint(chargingStation.getGeometry()));

                      // some points are wrong in the register / do not lay in the correct area
                      if ( chargingStation.getAdministrationUnit() != null ) {
                          stations.add(chargingStation);
                          chargingStationsInCommunity.putIfAbsent(chargingStation.getAdministrationUnit(), new HashSet<>());
                          chargingStationsInCommunity.get(chargingStation.getAdministrationUnit()).add(chargingStation);

                          AdministrationUnit district = administrationUnitRepository.findOneWithToOneRelationships(chargingStation.getAdministrationUnit().getDistrict().getId()).get();
                          chargingStationsInDistrict.putIfAbsent(district, new HashSet<>());
                          chargingStationsInDistrict.get(district).add(chargingStation);
                      }
                      else {
                          this.log.info("Could not set the admin unit for: {}", chargingStation);
                      }

                      if ( stations.size() % 100 == 0 ) {
                          this.log.info("Currently received {} charging stations from BNetzA service.", stations.size());
                          // break;
                      }
                    }
                }
            }
            reader.close();
            in.close();

            this.log.info("Starting to generate statistics for charging station in communities.");
            this.calculateStatisticsForAdministrationUnits(chargingStationsInCommunity, maxDate);
            this.log.info("Starting to generate statistics for charging station in districts.");
            this.calculateStatisticsForAdministrationUnits(chargingStationsInDistrict, maxDate);
            this.log.info("Saving all new charging station entries in the database.");
            this.chargingStationRepository.saveAll(stations);
            this.log.info("Saving all new statistic entries for communities in the database.");
            for ( AdministrationUnit au : chargingStationsInCommunity.keySet() ) {
                this.statisticRepository.saveAll(au.getStatistics());
            }
            this.log.info("Saving all new statistic entries for districts in the database.");
            for ( AdministrationUnit au : chargingStationsInDistrict.keySet() ) {
                this.statisticRepository.saveAll(au.getStatistics());
            }
        }
        else {
            log.warn(this.chargingStationRepository.count() == 0 ? "Alert: There are zero ChargingStations in the database. Please import!" : "Importing of ChargingStations disabled.");
        }
    }

    private void calculateStatisticsForAdministrationUnits(Map<AdministrationUnit, Set<ChargingStation>> chargingStationsInAdminUnit,
            LocalDate maxDate) {
        
        for (Map.Entry<AdministrationUnit, Set<ChargingStation>> entry: chargingStationsInAdminUnit.entrySet() ) {

            AdministrationUnit unit = entry.getKey();
            this.log.info("Calculating statistics for administrative unit: {}", unit.getGeografischernameGen());
            Set<ChargingStation> stationsInUnit = entry.getValue();
            Statistic population = unit.getStatisticByKey(STATISTIC_POPULATION_TOTAL);
            Statistic area = unit.getStatisticByKey(STATISTIC_AREA);

            // -- counts (all, normal, fast)
            unit.addStatistic(new Statistic().key("charging_station_count").source(B_NETZ_A).unit("").name("Anzahl Ladepunkte")
                    .value(stationsInUnit.parallelStream().mapToDouble(cs -> cs.getAnzahlLadepunkte()).sum())
                    .administrationUnit(unit).date(maxDate));
            unit.addStatistic(new Statistic().key("fast_charging_station_count").source(B_NETZ_A).unit("").name("Anzahl Schnellladeeinrichtung")
                    .value(stationsInUnit.parallelStream().filter(f -> "Schnellladeeinrichtung".equals(f.getArtDerLadeeinrichtung()))
                        .mapToDouble(cs -> cs.getAnzahlLadepunkte()).sum()).administrationUnit(unit).date(maxDate));
            unit.addStatistic(new Statistic().key("normal_charging_station_count").source(B_NETZ_A).unit("").name("Anzahl Normalladeeinrichtung")
                    .value(stationsInUnit.parallelStream().filter(f -> "Normalladeeinrichtung".equals(f.getArtDerLadeeinrichtung()))
                        .mapToDouble(cs -> cs.getAnzahlLadepunkte()).sum()).administrationUnit(unit).date(maxDate));
            // ---- Company
            unit.addStatistic(new Statistic().key("company_count").source(B_NETZ_A).unit("").name("Anzahl Firmen")
                    .value((double) stationsInUnit.parallelStream().map(cs -> cs.getBetreiber()).collect(Collectors.toSet()).size())
                    .administrationUnit(unit).date(maxDate));
            // ---- Nennleistung Ladeeinrichtung
            unit.addStatistic(new Statistic().key("sum_nominal_power").source(B_NETZ_A).unit("kW").name("Nennleistung Ladeeinrichtung (Sum)")
                    .value(stationsInUnit.parallelStream().mapToDouble(cs -> cs.getNennleistungLadeeinrichtung()).sum())
                    .administrationUnit(unit).date(maxDate));
            unit.addStatistic(new Statistic().key("min_nominal_power").source(B_NETZ_A).unit("kW").name("Nennleistung Ladeeinrichtung (Min)")
                    .value(stationsInUnit.parallelStream().mapToDouble(cs -> cs.getNennleistungLadeeinrichtung()).min().getAsDouble())
                    .administrationUnit(unit).date(maxDate));
            unit.addStatistic(new Statistic().key("max_nominal_power").source(B_NETZ_A).unit("kW").name("Nennleistung Ladeeinrichtung (Max)")
                    .value(stationsInUnit.parallelStream().mapToDouble(cs -> cs.getNennleistungLadeeinrichtung()).max().getAsDouble())
                    .administrationUnit(unit).date(maxDate));
            unit.addStatistic(new Statistic().key("median_nominal_power").source(B_NETZ_A).unit("kW").name("Nennleistung Ladeeinrichtung (Median)")
                    .value(new Median().evaluate(stationsInUnit.parallelStream().mapToDouble(cs -> cs.getNennleistungLadeeinrichtung()).toArray()))
                    .administrationUnit(unit).date(maxDate));
            unit.addStatistic(new Statistic().key("average_nominal_power").source(B_NETZ_A).unit("kW").name("Nennleistung Ladeeinrichtung (Average)")
                    .value(stationsInUnit.parallelStream().mapToDouble(cs -> cs.getNennleistungLadeeinrichtung()).average().getAsDouble())
                    .administrationUnit(unit).date(maxDate));
            // ---- relative stats for count (to population and area)
            unit.addStatistic(new Statistic().key("charging_station_count_per_population").source(B_NETZ_A).unit("").name("Anzahl Ladepunkte / 1.000 Personen")
                    .value(stationsInUnit.parallelStream().mapToDouble(cs -> cs.getAnzahlLadepunkte()).sum() / (population.getValue() / 1000))
                    .administrationUnit(unit).date(maxDate));
            unit.addStatistic(new Statistic().key("charging_station_count_per_area").source(B_NETZ_A).unit("Ladepunkte / km²").name("Anzahl Ladepunkte / Fläche")
                    .value(stationsInUnit.parallelStream().mapToDouble(cs -> cs.getAnzahlLadepunkte()).sum() / area.getValue())
                    .administrationUnit(unit).date(maxDate));
            // ---- relative stats for power (to population and area)
            unit.addStatistic(new Statistic().key("nominal_power_per_population").source(B_NETZ_A).unit("").name("Nennleistung Ladeeinrichtung / Bevölkerung")
                    .value(stationsInUnit.parallelStream().mapToDouble(cs -> cs.getNennleistungLadeeinrichtung()).sum() / population.getValue())
                    .administrationUnit(unit).date(maxDate));
            unit.addStatistic(new Statistic().key("nominal_power_per_area").source(B_NETZ_A).unit("kW / km²").name("Nennleistung Ladeeinrichtung / Fläche")
                    .value(stationsInUnit.parallelStream().mapToDouble(cs -> cs.getNennleistungLadeeinrichtung()).sum() / area.getValue())
                    .administrationUnit(unit).date(maxDate));
        }
    }

    private static Double parseDouble(String[] row, Integer index) {

        if ( row == null || row.length < index || row[index] == null || "".equals(row[index]) )
            return 0D;

        return Double.valueOf(row[index].replace(",", "."));
    }
}
