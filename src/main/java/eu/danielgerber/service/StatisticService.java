package eu.danielgerber.service;

import eu.danielgerber.domain.Statistic;
import eu.danielgerber.repository.StatisticRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Statistic}.
 */
@Service
@Transactional
public class StatisticService {

    private final Logger log = LoggerFactory.getLogger(StatisticService.class);

    private final StatisticRepository statisticRepository;

    public StatisticService(StatisticRepository statisticRepository) {
        this.statisticRepository = statisticRepository;
    }

    /**
     * Save a statistic.
     *
     * @param statistic the entity to save.
     * @return the persisted entity.
     */
    public Statistic save(Statistic statistic) {
        log.debug("Request to save Statistic : {}", statistic);
        return statisticRepository.save(statistic);
    }

    /**
     * Update a statistic.
     *
     * @param statistic the entity to save.
     * @return the persisted entity.
     */
    public Statistic update(Statistic statistic) {
        log.debug("Request to update Statistic : {}", statistic);
        return statisticRepository.save(statistic);
    }

    /**
     * Partially update a statistic.
     *
     * @param statistic the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Statistic> partialUpdate(Statistic statistic) {
        log.debug("Request to partially update Statistic : {}", statistic);

        return statisticRepository
            .findById(statistic.getId())
            .map(existingStatistic -> {
                if (statistic.getName() != null) {
                    existingStatistic.setName(statistic.getName());
                }
                if (statistic.getKey() != null) {
                    existingStatistic.setKey(statistic.getKey());
                }
                if (statistic.getValue() != null) {
                    existingStatistic.setValue(statistic.getValue());
                }
                if (statistic.getUnit() != null) {
                    existingStatistic.setUnit(statistic.getUnit());
                }
                if (statistic.getDate() != null) {
                    existingStatistic.setDate(statistic.getDate());
                }
                if (statistic.getSource() != null) {
                    existingStatistic.setSource(statistic.getSource());
                }

                return existingStatistic;
            })
            .map(statisticRepository::save);
    }

    /**
     * Get all the statistics.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Statistic> findAll() {
        log.debug("Request to get all Statistics");
        return statisticRepository.findAll();
    }

    /**
     * Get one statistic by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Statistic> findOne(Long id) {
        log.debug("Request to get Statistic : {}", id);
        return statisticRepository.findById(id);
    }

    /**
     * Delete the statistic by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Statistic : {}", id);
        statisticRepository.deleteById(id);
    }
}
