package eu.danielgerber.service;

import eu.danielgerber.domain.*; // for static metamodels
import eu.danielgerber.repository.UFZMeasurementRepository;
import eu.danielgerber.service.criteria.UFZMeasurementCriteria;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link UFZMeasurement} entities in the database.
 * The main input is a {@link UFZMeasurementCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UFZMeasurement} or a {@link Page} of {@link UFZMeasurement} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UFZMeasurementQueryService extends QueryService<UFZMeasurement> {

    private final Logger log = LoggerFactory.getLogger(UFZMeasurementQueryService.class);

    private final UFZMeasurementRepository uFZMeasurementRepository;

    public UFZMeasurementQueryService(UFZMeasurementRepository uFZMeasurementRepository) {
        this.uFZMeasurementRepository = uFZMeasurementRepository;
    }

    /**
     * Return a {@link List} of {@link UFZMeasurement} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UFZMeasurement> findByCriteria(UFZMeasurementCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UFZMeasurement> specification = createSpecification(criteria);
        return uFZMeasurementRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link UFZMeasurement} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UFZMeasurement> findByCriteria(UFZMeasurementCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UFZMeasurement> specification = createSpecification(criteria);
        return uFZMeasurementRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UFZMeasurementCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UFZMeasurement> specification = createSpecification(criteria);
        return uFZMeasurementRepository.count(specification);
    }

    /**
     * Function to convert {@link UFZMeasurementCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<UFZMeasurement> createSpecification(UFZMeasurementCriteria criteria) {
        Specification<UFZMeasurement> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), UFZMeasurement_.id));
            }
            if (criteria.getSmiGesamtboden() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSmiGesamtboden(), UFZMeasurement_.smiGesamtboden));
            }
            if (criteria.getSmiOberboden() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSmiOberboden(), UFZMeasurement_.smiOberboden));
            }
            if (criteria.getNutzbareFeldkapazitaet() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getNutzbareFeldkapazitaet(), UFZMeasurement_.nutzbareFeldkapazitaet)
                    );
            }
            if (criteria.getDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDate(), UFZMeasurement_.date));
            }
            if (criteria.getUfzRasterPointId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getUfzRasterPointId(),
                            root -> root.join(UFZMeasurement_.ufzRasterPoint, JoinType.LEFT).get(UFZRasterPoint_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
