import { Component } from '@angular/core';
import {STATE_CONFIGS, StateConfig, StateName} from "../../config/state.constants";

@Component({
  selector: 'jhi-footer',
  templateUrl: './footer.component.html',
})
export class FooterComponent {

  STATE_CONFIG : StateConfig = STATE_CONFIGS[process.env.STATE as StateName || "Sachsen"];
}
