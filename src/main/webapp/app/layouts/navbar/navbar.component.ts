import { Component, OnInit } from '@angular/core';
import {STATE_CONFIGS, StateConfig, StateName} from "../../config/state.constants";

@Component({
  selector: 'jhi-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  STATE_CONFIG : StateConfig = STATE_CONFIGS[process.env.STATE as StateName || "Sachsen"];

  constructor() { }

  ngOnInit(): void {
  }

}
