import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAdministrationUnit } from '../models/administration-unit.model';
import { AdministrationUnitService } from '../services/administration-unit.service';

@Injectable({ providedIn: 'root' })
export class AdministrationUnitRoutingResolveService implements Resolve<IAdministrationUnit | null> {
  constructor(protected service: AdministrationUnitService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAdministrationUnit | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((administrationUnit: HttpResponse<IAdministrationUnit>) => {
          if (administrationUnit.body) {
            return of(administrationUnit.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
