export interface IChargingStationsOperator {
  operator: String;
  power: number;
  points: number;
  average: number;
}

export interface IChargingStationsOperatorDTO {
  millis: number;
  operators: IChargingStationsOperator[];
}
