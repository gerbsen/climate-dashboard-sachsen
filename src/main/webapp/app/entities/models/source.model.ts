type UpdateInterval = "monthly" | "daily" | "yearly";

export interface ISource {
  url: string;
  label: string;
  text?: string;
  imageFileName?: string;
  lastModified?: string;
  updateInterval?: UpdateInterval;
}

export class Source implements ISource {
  constructor(
    public url: string,
    public label: string,
    public text?: string,
    public imageFileName?: string,
    public lastModified?: string,
    public updateInterval?: UpdateInterval,
  ) {
  }
}
