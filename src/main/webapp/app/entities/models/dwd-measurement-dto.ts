import { DWDState } from "app/config/state.constants";

export type DWDYearMeasurement = {
    year: number,
    january: number,
    february: number,
    march: number,
    april: number,
    may: number,
    june: number,
    july: number,
    august: number,
    september: number,
    october: number,
    november: number,
    december: number
}

export interface IDWDMeasurementDTO {
    millis: number;
    state: DWDState;
    years: number[];
    airTemperatureMean: (number | null)[];
    airTemperatureMeanRolling30: (number | null)[];
    tropicalNightsTminGE20: (number | null)[];
    tropicalNightsTminGE20Rolling30: (number | null)[];
    sunshineDuration: (number | null)[];
    sunshineDurationRolling30: (number | null)[];
    precipE20mmDays: (number | null)[];
    precipE20mmDaysRolling30: (number | null)[];
    precipE10mmDays: (number | null)[];
    precipE10mmDaysRolling30: (number | null)[];
    hotDays: (number | null)[];
    hotDaysRolling30: (number | null)[];
    summerDays: (number | null)[];
    summerDaysRolling30: (number | null)[];
    frostDays: (number | null)[];
    frostDaysRolling30: (number | null)[];
    iceDays: (number | null)[];
    iceDaysRolling30: (number | null)[];
    regionalAverageAirTemperatureMean: { [key: number]: DWDYearMeasurement; }
    regionalAveragePercipitation: { [key: number]: DWDYearMeasurement; }
    regionalAverageSunshineDuration: { [key: number]: DWDYearMeasurement; }
}