import dayjs from 'dayjs/esm';
import { IUFZRasterPoint } from 'app/entities/models/ufz-raster-point.model';

export interface IUFZMeasurement {
  id: number;
  smiGesamtboden?: number | null;
  smiOberboden?: number | null;
  nutzbareFeldkapazitaet?: number | null;
  date?: dayjs.Dayjs | null;
  ufzRasterPoint?: IUFZRasterPoint;
}

export type NewUFZMeasurement = Omit<IUFZMeasurement, 'id'> & { id: null };
