import { IntervalFormat } from "./mastr-unit-for-interval-statistics.model";

export interface IChargingStationsStatisticsForInterval {
  timeUnit: String;
  timeUnitChargingPoints: number;
  timeUnitPower: number;
  totalChargingPoints: number;
  totalPower: number;
}

export interface IChargingStationsStatisticsForIntervalDTO {
  millis: number;
  intervalStatistics: {[key in IntervalFormat]: IChargingStationsStatisticsForInterval[];}
}
