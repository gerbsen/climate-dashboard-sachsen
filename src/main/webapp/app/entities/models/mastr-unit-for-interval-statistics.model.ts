
export type AggregateFunction = "SUM" | "MIN" | "MAX" | "AVG";
export type IntervalFormat = "WEEK" | "MONTH" | "YEAR";
export type MastrUnitProperty = "GROSS_POWER" | "NUMBER_OF_SOLAR_MODULES" | "HUB_HEIGHT_WINDTURBINE"
    | "ROTOR_DIAMETER_WINDTURBINE" | "NET_NOMINAL_CAPACITY" | "USEABLE_STORAGE_CAPACITY";
export type EnergySource = "OTHER_GASES" | "MINERAL_OIL_PRODUCTS" | "STORAGE" | "WATER" | "SLUDGE" | "BIOMASS"
    | "SOLAR_POWER" | "NATURAL_GAS" | "WIND" | "GEOTHERMICS" | "MINE_GAS" | "SOLAR_THERMICS" | "LIGNITE_COAL"
    | "NONE_BIOGENIC_WASTE" | "HEAT";
export type OperationState = "IN_PLANING" | "ACTIVE" | "TEMPORARILY_INACTIVE" | "PERMANENTLY_INACTIVE";

export interface IMastrUnitForIntervalStatisticsElement {
    count: number;
    date: string;
    aggregate: number;
}

export interface IMastrUnitForIntervalStatistics {
    results: IMastrUnitForIntervalStatisticsElement[];
    ags: string;
    aggregateFunction: AggregateFunction;
    intervalFormat: IntervalFormat;
    propertyName: MastrUnitProperty;
    energySource: EnergySource;
    millis: number;
}