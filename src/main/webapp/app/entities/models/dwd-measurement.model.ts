export interface IDWDMeasurement {
  id: number;
  year?: number | null;
  statistic?: string | null;
  brandenburgBerlin?: number | null;
  brandenburg?: number | null;
  badenWuerttemberg?: number | null;
  bayern?: number | null;
  hessen?: number | null;
  mecklenburgVorpommern?: number | null;
  niedersachsen?: number | null;
  niedersachsenHamburgBremen?: number | null;
  nordrheinWestfalen?: number | null;
  rheinlandPfalz?: number | null;
  schleswigHolstein?: number | null;
  saarland?: number | null;
  sachsen?: number | null;
  sachsenAnhalt?: number | null;
  thueringenSachsenAnhalt?: number | null;
  thueringen?: number | null;
  deutschland?: number | null;
}

export type NewDWDMeasurement = Omit<IDWDMeasurement, 'id'> & { id: null };
