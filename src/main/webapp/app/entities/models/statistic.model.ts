import dayjs from 'dayjs/esm';
import { IAdministrationUnit } from 'app/entities/models/administration-unit.model';
import { IChargingStation } from 'app/entities/models/charging-station.model';

export interface IStatistic {
  id: number;
  name?: string | null;
  key?: string | null;
  value?: number | null;
  unit?: string | null;
  date?: dayjs.Dayjs | null;
  source?: string | null;
  administrationUnit?: Pick<IAdministrationUnit, 'id'> | null;
  chargingStation?: Pick<IChargingStation, 'id'> | null;
}

export type NewStatistic = Omit<IStatistic, 'id'> & { id: null };
