import dayjs from 'dayjs/esm';
import { IAdministrationUnit } from 'app/entities/models/administration-unit.model';
import * as geojson from "geojson";

export interface IChargingStation {
  id: number;
  betreiber?: string | null;
  strasse?: string | null;
  hausnummer?: string | null;
  adresszusatz?: string | null;
  postleitzahl?: string | null;
  ort?: string | null;
  bundesland?: string | null;
  kreisKreisfreieStadt?: string | null;
  inbetriebnahmeDatum?: dayjs.Dayjs;
  nennleistungLadeeinrichtung?: number | null;
  artDerLadeeinrichtung?: string | null;
  anzahlLadepunkte?: number | null;
  ersterLadepunktSteckertypen?: string | null;
  ersterLadepunktLeistung?: number | null;
  zweiterLadepunktSteckertypen?: string | null;
  zweiterLadepunktLeistung?: number | null;
  dritterLadepunktSteckertypen?: string | null;
  dritterLadepunktLeistung?: number | null;
  vierterLadepunktSteckertypen?: string | null;
  vierterLadepunktLeistung?: number | null;
  geometry?: geojson.Geometry | null;
  administrationUnit?: Pick<IAdministrationUnit, 'id'> | null;
}

export type NewChargingStation = Omit<IChargingStation, 'id'> & { id: null };
