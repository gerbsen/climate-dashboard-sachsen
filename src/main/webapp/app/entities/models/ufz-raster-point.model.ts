import * as geojson from "geojson";
import { IUFZMeasurement } from "./ufz-measurement.model";

export interface IUFZRasterPoint {
  id: number;
  lat?: number | null;
  lng?: number | null;
  geometry?: geojson.Geometry;
  ufzMeasurements? : IUFZMeasurement[] | null;
}

export type NewUFZRasterPoint = Omit<IUFZRasterPoint, 'id'> & { id: null };
