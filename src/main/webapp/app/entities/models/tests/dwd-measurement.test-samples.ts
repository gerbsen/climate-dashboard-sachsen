import { IDWDMeasurement, NewDWDMeasurement } from '../dwd-measurement.model';

export const sampleWithRequiredData: IDWDMeasurement = {
  id: 61776,
};

export const sampleWithPartialData: IDWDMeasurement = {
  id: 49729,
  year: 64346,
  statistic: 'Kids',
  brandenburgBerlin: 96342,
  brandenburg: 21739,
  badenWuerttemberg: 10747,
  bayern: 85224,
  nordrheinWestfalen: 52936,
  rheinlandPfalz: 98218,
  schleswigHolstein: 53953,
  saarland: 92314,
  sachsen: 7405,
  thueringenSachsenAnhalt: 1133,
  deutschland: 96353,
};

export const sampleWithFullData: IDWDMeasurement = {
  id: 37475,
  year: 3637,
  statistic: 'paradigm withdrawal Expanded',
  brandenburgBerlin: 97383,
  brandenburg: 21774,
  badenWuerttemberg: 18844,
  bayern: 64482,
  hessen: 73706,
  mecklenburgVorpommern: 53105,
  niedersachsen: 34070,
  niedersachsenHamburgBremen: 778,
  nordrheinWestfalen: 40686,
  rheinlandPfalz: 26222,
  schleswigHolstein: 76899,
  saarland: 1244,
  sachsen: 34933,
  sachsenAnhalt: 90046,
  thueringenSachsenAnhalt: 75329,
  thueringen: 2389,
  deutschland: 94311,
};

export const sampleWithNewData: NewDWDMeasurement = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
