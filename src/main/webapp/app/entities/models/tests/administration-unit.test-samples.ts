import dayjs from 'dayjs/esm';

import { IAdministrationUnit, NewAdministrationUnit } from '../administration-unit.model';

export const sampleWithRequiredData: IAdministrationUnit = {
  id: 4610,
};

export const sampleWithPartialData: IAdministrationUnit = {
  id: 16689,
  objektidentifikator: 'Awesome overriding',
  beginnlebenszeit: dayjs('2023-10-31'),
  adminEbeneAde: 'RAM fuchsia Hessen',
  ade: 49961,
  gf: 99707,
  besondereGebieteBsg: 'Customer',
  bsg: 9376,
  regionalschluesselArs: 'Madagaskar Keyboard Senior',
  gemeindeschluesselAgs: 'AI',
  geografischernameGen: 'integrate Mexiko Steel',
  identifikatorIbz: 15976,
  bemerkung: 'Peso Somalien Licensed',
  land: 'program Pants Hessen',
  regierungsbezirk: 'Assurance Dalasi',
  verwaltungsgemeinschaftteil1: 'Afghani',
  verwaltungsgemeinschaftteil2: 'redundant Mobility Trail',
  gemeinde: 'structure optimize',
  fkS3: 'bi-directional e-tailers',
  europStatistikschluesselNuts: 'groupware Generic web-enabled',
  gemeindeschluesselaufgefuellt: 'bluetooth',
  wirksamkeitWsk: dayjs('2023-10-31'),
};

export const sampleWithFullData: IAdministrationUnit = {
  id: 64618,
  objektidentifikator: 'application',
  beginnlebenszeit: dayjs('2023-10-30'),
  adminEbeneAde: 'National National green',
  ade: 71633,
  geofaktorGf: 'PCI transmitting Fish',
  gf: 86832,
  besondereGebieteBsg: 'Rheinland-Pfalz',
  bsg: 70889,
  regionalschluesselArs: 'maximize Gloves',
  gemeindeschluesselAgs: 'Brandenburg Upgradable',
  verwaltungssitzSdvArs: 'Rustic Security invoice',
  geografischernameGen: 'Regional',
  bezeichnung: 'logistical front-end',
  identifikatorIbz: 78042,
  bemerkung: 'hack',
  namensbildungNbd: 'program Pakistan',
  nbd: true,
  land: 'Re-engineered',
  regierungsbezirk: 'Practical Rubber',
  kreis: 'Soft Terrace',
  verwaltungsgemeinschaftteil1: 'turn-key',
  verwaltungsgemeinschaftteil2: 'Bayern Cotton FTP',
  gemeinde: 'Cross-group',
  funkSchluesselstelle3: 'parse',
  fkS3: 'navigating Car',
  europStatistikschluesselNuts: 'gold frictionless RAM',
  regioschluesselaufgefuellt: 'Assistant Mecklenburg-Vorpommern',
  gemeindeschluesselaufgefuellt: 'Coordinator Bedfordshire',
  wirksamkeitWsk: dayjs('2023-10-31'),
  geometry: 'Planner Lead secured',
};

export const sampleWithNewData: NewAdministrationUnit = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
