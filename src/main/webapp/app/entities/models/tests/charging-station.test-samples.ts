import dayjs from 'dayjs/esm';

import { IChargingStation, NewChargingStation } from '../charging-station.model';

export const sampleWithRequiredData: IChargingStation = {
  id: 56093,
};

export const sampleWithPartialData: IChargingStation = {
  id: 69062,
  strasse: 'Bouvetinsel',
  postleitzahl: 'Rue deposit Berlin',
  kreisKreisfreieStadt: 'programming navigating Producer',
  inbetriebnahmeDatum: dayjs('2023-11-23'),
  dritterLadepunktLeistung: 72889,
  vierterLadepunktSteckertypen: 'deposit Chair',
};

export const sampleWithFullData: IChargingStation = {
  id: 18165,
  betreiber: 'Communications',
  strasse: 'out-of-the-box',
  hausnummer: 'Rheinland-Pfalz Outdoors user-centric',
  adresszusatz: 'up Synergized Fresh',
  postleitzahl: 'Investment ivory',
  ort: 'Mecklenburg-Vorpommern',
  bundesland: 'Electronics solution-oriented',
  kreisKreisfreieStadt: 'Soft Bayern Liaison',
  inbetriebnahmeDatum: dayjs('2023-11-23'),
  nennleistungLadeeinrichtung: 60318,
  artDerLadeeinrichtung: 'magnetic Norwegen',
  anzahlLadepunkte: 9166,
  ersterLadepunktSteckertypen: 'Incredible port dynamic',
  ersterLadepunktLeistung: 60331,
  zweiterLadepunktSteckertypen: '1080p',
  zweiterLadepunktLeistung: 93334,
  dritterLadepunktSteckertypen: 'envisioneer Dominican',
  dritterLadepunktLeistung: 74546,
  vierterLadepunktSteckertypen: 'Metal Hessen',
  vierterLadepunktLeistung: 15481,
  geometry: 'Sachsen-Anhalt',
};

export const sampleWithNewData: NewChargingStation = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
