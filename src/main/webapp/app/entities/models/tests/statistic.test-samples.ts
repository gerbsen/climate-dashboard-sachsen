import dayjs from 'dayjs/esm';

import { IStatistic, NewStatistic } from '../statistic.model';

export const sampleWithRequiredData: IStatistic = {
  id: 2001,
};

export const sampleWithPartialData: IStatistic = {
  id: 21101,
  name: 'magnetic',
  key: 'Director',
  value: 75879,
  unit: 'Account Front-line',
  source: 'Aruba COM protocol',
};

export const sampleWithFullData: IStatistic = {
  id: 62073,
  name: 'Gorgeous Function-based Fish',
  key: 'Mandatory',
  value: 73089,
  unit: 'monitor Gloves Incredible',
  date: dayjs('2023-11-14'),
  source: 'Outdoors Grocery',
};

export const sampleWithNewData: NewStatistic = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
