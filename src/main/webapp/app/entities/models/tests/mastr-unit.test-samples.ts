import dayjs from 'dayjs/esm';

import { IMastrUnit, NewMastrUnit } from '../mastr-unit.model';

export const sampleWithRequiredData: IMastrUnit = {
  id: 30050,
};

export const sampleWithPartialData: IMastrUnit = {
  id: 9084,
  bundesland: 'Taka Principal',
  bundeslandId: 95019,
  landkreisId: 12287,
  gemeindeId: 479,
  datumLetzteAktualisierung: dayjs('2023-02-22'),
  einheitRegistrierungsDatum: dayjs('2023-02-22'),
  endgueltigeStilllegungDatum: dayjs('2023-02-22'),
  geplantesInbetriebsnahmeDatum: dayjs('2023-02-22'),
  kwkAnlageInbetriebnahmeDatum: dayjs('2023-02-22'),
  eegAnlageRegistrierungsDatum: dayjs('2023-02-23'),
  genehmigungDatum: dayjs('2023-02-22'),
  genehmigungRegistrierungsDatum: dayjs('2023-02-23'),
  isBuergerEnergie: false,
  isPilotWindanlage: false,
  isPrototypAnlage: true,
  lat: 81598,
  ort: 'Security Markets',
  plz: 83069,
  strasse: 'Intelligent matrix Brandenburg',
  flurstueck: 'parsing Crest',
  landId: 64673,
  landkreis: 'users',
  lokationMastrNr: 'methodical',
  netzbetreiberMaskedNamen: 'JSON Books',
  netzbetreiberMastrNummer: 'Granite system',
  netzbetreiberNamen: 'impactful Producer',
  netzbetreiberPersonenArt: 'Baden-Württemberg SDD pink',
  typ: 15647,
  batterieTechnologie: 4313,
  eegInstallierteLeistung: 22218,
  eegAnlagenSchluessel: 'Caicosinseln',
  energieTraegerId: 25648,
  energieTraegerName: 'Legacy magnetic state',
  gemeinsamerWechselrichter: 99706,
  genehmigungBehoerde: 'Principal Borders scale',
  genehmigungsMastrNummer: 'plug-and-play',
  gruppierungsObjekteIds: 'database',
  hauptAusrichtungSolarmodule: 47157,
  hauptAusrichtungSolarmoduleBezeichnung: 'payment hacking',
  hauptBrennstoffNamen: 'Communications redefine green',
  kwkZuschlag: 'revolutionary Hessen toolset',
  rotorDurchmesserWindenergieanlage: 27403,
  nutzungsBereichGebsa: 23823,
  standortAnonymisiert: 'Bahamas',
  spannungsEbenenId: 'Shoes Music vortals',
  vollTeilEinspeisung: 83688,
  vollTeilEinspeisungBezeichnung: 'Coordinator bluetooth SMTP',
  geometry: 'Avon',
};

export const sampleWithFullData: IMastrUnit = {
  id: 6826,
  mastrNummer: 'ROI Small',
  anlagenBetreiberId: 33569,
  anlagenBetreiberPersonenArt: 75031,
  anlagenBetreiberMaskedName: 'Falls',
  anlagenBetreiberMastrNummer: 'enhance encoding Account',
  anlagenBetreiberName: 'Granite',
  betriebsStatusId: 29592,
  betriebsStatusName: 'Future',
  bundesland: 'Focused website Shoes',
  bundeslandId: 97735,
  landkreisId: 18445,
  gemeindeId: 86447,
  datumLetzteAktualisierung: dayjs('2023-02-23'),
  einheitRegistrierungsDatum: dayjs('2023-02-22'),
  endgueltigeStilllegungDatum: dayjs('2023-02-22'),
  geplantesInbetriebsnahmeDatum: dayjs('2023-02-23'),
  inbetriebnahmeDatum: dayjs('2023-02-22'),
  kwkAnlageInbetriebnahmeDatum: dayjs('2023-02-23'),
  kwkAnlageRegistrierungsDatum: dayjs('2023-02-22'),
  eegInbetriebnahmeDatum: dayjs('2023-02-22'),
  eegAnlageRegistrierungsDatum: dayjs('2023-02-23'),
  genehmigungDatum: dayjs('2023-02-22'),
  genehmigungRegistrierungsDatum: dayjs('2023-02-23'),
  isNbPruefungAbgeschlossen: 30773,
  isAnonymisiert: true,
  isBuergerEnergie: false,
  isEinheitNotstromaggregat: false,
  isMieterstromAngemeldet: true,
  isWasserkraftErtuechtigung: true,
  isPilotWindanlage: false,
  isPrototypAnlage: true,
  lat: 71264,
  lng: 94831,
  ort: 'Producer Frozen',
  plz: 58844,
  strasse: 'empower',
  hausnummer: 'Eritrea',
  einheitname: 'SSL',
  flurstueck: 'copying Kids',
  gemarkung: 'Mecklenburg-Vorpommern HDD XML',
  gemeinde: 'Bacon salmon Islands',
  landId: 68319,
  landkreis: 'black auxiliary',
  ags: 41673,
  lokationId: 30827,
  lokationMastrNr: 'orchid Eritrea Cotton',
  netzbetreiberId: 'Functionality',
  netzbetreiberMaskedNamen: 'mint Solutions',
  netzbetreiberMastrNummer: 'uniform Metrics Data',
  netzbetreiberNamen: 'driver driver optimizing',
  netzbetreiberPersonenArt: 'Computer Village',
  systemStatusId: 25293,
  systemStatusName: 'Reverse-engineered web-readiness backing',
  typ: 27690,
  aktenzeichenGenehmigung: 'e-tailers',
  anzahlSolarmodule: 3097,
  batterieTechnologie: 44373,
  bruttoLeistung: 88769,
  eegInstallierteLeistung: 86281,
  eegAnlageMastrNummer: 'Multi-channelled Robust Handmade',
  eegAnlagenSchluessel: 'compressing',
  eegZuschlag: 'Identity Re-contextualized',
  zuschlagsNummern: 'generate',
  energieTraegerId: 53691,
  energieTraegerName: 'Keyboard Berkshire Amerikanisch-Samoa',
  gemeinsamerWechselrichter: 61826,
  genehmigungBehoerde: 'enhance online',
  genehmigungsMastrNummer: 'Senior Computer Granite',
  gruppierungsObjekte: 'interface Avon Home',
  gruppierungsObjekteIds: 'feed deliverables',
  hatFlexibilitaetsPraemie: false,
  hauptAusrichtungSolarmodule: 75415,
  hauptAusrichtungSolarmoduleBezeichnung: 'Rubber scalable invoice',
  hauptBrennstoffId: 4365,
  hauptBrennstoffNamen: 'parse',
  hauptNeigungswinkelSolarModule: 99433,
  herstellerWindenergieanlageId: 46269,
  herstellerWindenergieanlageBezeichnung: 'Berkshire Networked',
  kwkAnlageElektrischeLeistung: 95689,
  kwkAnlageMastrNummer: 'Data',
  kwkZuschlag: 'streamline flexibility Home',
  lageEinheit: 45470,
  lageEinheitBezeichnung: 'quantifying Creative Vanuatu',
  leistungsBegrenzung: 25424,
  nabenhoeheWindenergieanlage: 19928,
  rotorDurchmesserWindenergieanlage: 13513,
  nettoNennLeistung: 39410,
  nutzbareSpeicherKapazitaet: 92702,
  nutzungsBereichGebsa: 53918,
  standortAnonymisiert: 'District Sausages SSL',
  spannungsEbenenId: 'best-of-breed Nakfa',
  spannungsEbenenNamen: 'seize',
  speicherEinheitMastrNummer: 'coherent alliance state',
  technologieStromerzeugungId: 86501,
  technologieStromerzeugung: 'Director Granite',
  thermischeNutzLeistung: 32456,
  typenBezeichnung: 'Cloned installation',
  vollTeilEinspeisung: 23350,
  vollTeilEinspeisungBezeichnung: 'Branding',
  windparkName: 'Metal Bedfordshire Venezuela',
  geometry: 'Fall Berlin',
};

export const sampleWithNewData: NewMastrUnit = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
