import dayjs from 'dayjs/esm';
import { IStatistic } from 'app/entities/models/statistic.model';
import * as geojson from "geojson";

export interface IAdministrationUnit {
  id: number;
  objektidentifikator?: string | null;
  beginnlebenszeit?: dayjs.Dayjs | null;
  adminEbeneAde?: string | null;
  ade?: number | null;
  geofaktorGf?: string | null;
  gf?: number | null;
  besondereGebieteBsg?: string | null;
  bsg?: number | null;
  regionalschluesselArs?: string | null;
  gemeindeschluesselAgs?: string | null;
  verwaltungssitzSdvArs?: string | null;
  geografischernameGen?: string | null;
  bezeichnung?: string | null;
  identifikatorIbz?: number | null;
  bemerkung?: string | null;
  namensbildungNbd?: string | null;
  nbd?: boolean | null;
  land?: string | null;
  regierungsbezirk?: string | null;
  kreis?: string | null;
  verwaltungsgemeinschaftteil1?: string | null;
  verwaltungsgemeinschaftteil2?: string | null;
  gemeinde?: string | null;
  funkSchluesselstelle3?: string | null;
  fkS3?: string | null;
  europStatistikschluesselNuts?: string | null;
  regioschluesselaufgefuellt?: string | null;
  gemeindeschluesselaufgefuellt?: string | null;
  wirksamkeitWsk?: dayjs.Dayjs | null;
  geometry?: geojson.Geometry | null;
  statistics?: IStatistic[] | null;
  properties?: any | null;
}

export type NewAdministrationUnit = Omit<IAdministrationUnit, 'id'> & { id: null };
