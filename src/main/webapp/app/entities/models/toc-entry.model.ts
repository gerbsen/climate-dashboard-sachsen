export interface ITocEntry {
  id: string;
  label: string;
  subEntries: ITocEntry[];
}

export class TocEntry implements ITocEntry {
  constructor(
    public id: string,
    public label: string,
    public subEntries: ITocEntry[],
  ) {}
}
