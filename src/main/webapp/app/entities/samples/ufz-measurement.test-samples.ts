import dayjs from 'dayjs/esm';

import { IUFZMeasurement, NewUFZMeasurement } from '../models/ufz-measurement.model';

export const sampleWithRequiredData: IUFZMeasurement = {
  id: 48962,
};

export const sampleWithPartialData: IUFZMeasurement = {
  id: 6887,
  smiOberboden: 53446,
  nutzbareFeldkapazitaet: 22858,
  date: dayjs('2024-02-11'),
};

export const sampleWithFullData: IUFZMeasurement = {
  id: 90586,
  smiGesamtboden: 19221,
  smiOberboden: 39838,
  nutzbareFeldkapazitaet: 53453,
  date: dayjs('2024-02-11'),
};

export const sampleWithNewData: NewUFZMeasurement = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
