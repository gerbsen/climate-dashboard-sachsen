import { IUFZRasterPoint, NewUFZRasterPoint } from '../models/ufz-raster-point.model';

export const sampleWithRequiredData: IUFZRasterPoint = {
  id: 68644,
};

export const sampleWithPartialData: IUFZRasterPoint = {
  id: 2856,
  lat: 61358,
  lng: 53549,
};

export const sampleWithFullData: IUFZRasterPoint = {
  id: 86894,
  lat: 51134,
  lng: 85754,
  geometry: 'Table Configurable',
};

export const sampleWithNewData: NewUFZRasterPoint = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
