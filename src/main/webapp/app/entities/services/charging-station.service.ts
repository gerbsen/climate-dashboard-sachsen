import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IChargingStation, NewChargingStation } from '../models/charging-station.model';
import { IntervalFormat } from '../models/mastr-unit-for-interval-statistics.model';
import { IChargingStationsStatisticsForIntervalDTO } from '../models/charging-stations-statistics-for-interval-dto.model';
import { IChargingStationsOperatorDTO } from '../models/charging-stations-operator-dto.model';

export type PartialUpdateChargingStation = Partial<IChargingStation> & Pick<IChargingStation, 'id'>;

type RestOf<T extends IChargingStation | NewChargingStation> = Omit<T, 'inbetriebnahmeDatum'> & {
  inbetriebnahmeDatum?: string | null;
};

export type RestChargingStation = RestOf<IChargingStation>;

export type NewRestChargingStation = RestOf<NewChargingStation>;

export type PartialUpdateRestChargingStation = RestOf<PartialUpdateChargingStation>;

export type EntityResponseType = HttpResponse<IChargingStation>;
export type EntityArrayResponseType = HttpResponse<IChargingStation[]>;

@Injectable({ providedIn: 'root' })
export class ChargingStationService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/charging-stations');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestChargingStation>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestChargingStation[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  statisticsForInterval(): Observable<IChargingStationsStatisticsForIntervalDTO> {
    return this.http.get<IChargingStationsStatisticsForIntervalDTO>(
      `${this.resourceUrl}/statistics-for-interval`);
  }

  getOperators(startDate?: dayjs.Dayjs, endDate?: dayjs.Dayjs): Observable<IChargingStationsOperatorDTO> {
    const options: any = {}
    if ( startDate ) options.startDate = startDate.format('YYYY-MM-DD');
    if ( endDate ) options.endDate = endDate.format('YYYY-MM-DD');
    return this.http.get<IChargingStationsOperatorDTO>(`${this.resourceUrl}/operators`, { params: options });
  }

  getChargingStationIdentifier(chargingStation: Pick<IChargingStation, 'id'>): number {
    return chargingStation.id;
  }

  compareChargingStation(o1: Pick<IChargingStation, 'id'> | null, o2: Pick<IChargingStation, 'id'> | null): boolean {
    return o1 && o2 ? this.getChargingStationIdentifier(o1) === this.getChargingStationIdentifier(o2) : o1 === o2;
  }

  addChargingStationToCollectionIfMissing<Type extends Pick<IChargingStation, 'id'>>(
    chargingStationCollection: Type[],
    ...chargingStationsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const chargingStations: Type[] = chargingStationsToCheck.filter(isPresent);
    if (chargingStations.length > 0) {
      const chargingStationCollectionIdentifiers = chargingStationCollection.map(
        chargingStationItem => this.getChargingStationIdentifier(chargingStationItem)!
      );
      const chargingStationsToAdd = chargingStations.filter(chargingStationItem => {
        const chargingStationIdentifier = this.getChargingStationIdentifier(chargingStationItem);
        if (chargingStationCollectionIdentifiers.includes(chargingStationIdentifier)) {
          return false;
        }
        chargingStationCollectionIdentifiers.push(chargingStationIdentifier);
        return true;
      });
      return [...chargingStationsToAdd, ...chargingStationCollection];
    }
    return chargingStationCollection;
  }

  protected convertDateFromClient<T extends IChargingStation | NewChargingStation | PartialUpdateChargingStation>(
    chargingStation: T
  ): RestOf<T> {
    return {
      ...chargingStation,
      inbetriebnahmeDatum: chargingStation.inbetriebnahmeDatum?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restChargingStation: RestChargingStation): IChargingStation {
    return {
      ...restChargingStation,
      inbetriebnahmeDatum: restChargingStation.inbetriebnahmeDatum ? dayjs(restChargingStation.inbetriebnahmeDatum) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestChargingStation>): HttpResponse<IChargingStation> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestChargingStation[]>): HttpResponse<IChargingStation[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
