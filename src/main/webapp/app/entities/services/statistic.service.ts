import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IStatistic, NewStatistic } from '../models/statistic.model';

export type PartialUpdateStatistic = Partial<IStatistic> & Pick<IStatistic, 'id'>;

type RestOf<T extends IStatistic | NewStatistic> = Omit<T, 'date'> & {
  date?: string | null;
};

export type RestStatistic = RestOf<IStatistic>;

export type NewRestStatistic = RestOf<NewStatistic>;

export type PartialUpdateRestStatistic = RestOf<PartialUpdateStatistic>;

export type EntityResponseType = HttpResponse<IStatistic>;
export type EntityArrayResponseType = HttpResponse<IStatistic[]>;

@Injectable({ providedIn: 'root' })
export class StatisticService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/statistics');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestStatistic>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestStatistic[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  getStatisticIdentifier(statistic: Pick<IStatistic, 'id'>): number {
    return statistic.id;
  }

  compareStatistic(o1: Pick<IStatistic, 'id'> | null, o2: Pick<IStatistic, 'id'> | null): boolean {
    return o1 && o2 ? this.getStatisticIdentifier(o1) === this.getStatisticIdentifier(o2) : o1 === o2;
  }

  addStatisticToCollectionIfMissing<Type extends Pick<IStatistic, 'id'>>(
    statisticCollection: Type[],
    ...statisticsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const statistics: Type[] = statisticsToCheck.filter(isPresent);
    if (statistics.length > 0) {
      const statisticCollectionIdentifiers = statisticCollection.map(statisticItem => this.getStatisticIdentifier(statisticItem)!);
      const statisticsToAdd = statistics.filter(statisticItem => {
        const statisticIdentifier = this.getStatisticIdentifier(statisticItem);
        if (statisticCollectionIdentifiers.includes(statisticIdentifier)) {
          return false;
        }
        statisticCollectionIdentifiers.push(statisticIdentifier);
        return true;
      });
      return [...statisticsToAdd, ...statisticCollection];
    }
    return statisticCollection;
  }

  protected convertDateFromClient<T extends IStatistic | NewStatistic | PartialUpdateStatistic>(statistic: T): RestOf<T> {
    return {
      ...statistic,
      date: statistic.date?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restStatistic: RestStatistic): IStatistic {
    return {
      ...restStatistic,
      date: restStatistic.date ? dayjs(restStatistic.date) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestStatistic>): HttpResponse<IStatistic> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestStatistic[]>): HttpResponse<IStatistic[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
