import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IUFZRasterPoint, NewUFZRasterPoint } from '../models/ufz-raster-point.model';

export type PartialUpdateUFZRasterPoint = Partial<IUFZRasterPoint> & Pick<IUFZRasterPoint, 'id'>;

export type EntityResponseType = HttpResponse<IUFZRasterPoint>;
export type EntityArrayResponseType = HttpResponse<IUFZRasterPoint[]>;

@Injectable({ providedIn: 'root' })
export class UFZRasterPointService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/ufz-raster-points');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUFZRasterPoint>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUFZRasterPoint[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  getUFZRasterPointIdentifier(uFZRasterPoint: Pick<IUFZRasterPoint, 'id'>): number {
    return uFZRasterPoint.id;
  }

  compareUFZRasterPoint(o1: Pick<IUFZRasterPoint, 'id'> | null, o2: Pick<IUFZRasterPoint, 'id'> | null): boolean {
    return o1 && o2 ? this.getUFZRasterPointIdentifier(o1) === this.getUFZRasterPointIdentifier(o2) : o1 === o2;
  }

  addUFZRasterPointToCollectionIfMissing<Type extends Pick<IUFZRasterPoint, 'id'>>(
    uFZRasterPointCollection: Type[],
    ...uFZRasterPointsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const uFZRasterPoints: Type[] = uFZRasterPointsToCheck.filter(isPresent);
    if (uFZRasterPoints.length > 0) {
      const uFZRasterPointCollectionIdentifiers = uFZRasterPointCollection.map(
        uFZRasterPointItem => this.getUFZRasterPointIdentifier(uFZRasterPointItem)!
      );
      const uFZRasterPointsToAdd = uFZRasterPoints.filter(uFZRasterPointItem => {
        const uFZRasterPointIdentifier = this.getUFZRasterPointIdentifier(uFZRasterPointItem);
        if (uFZRasterPointCollectionIdentifiers.includes(uFZRasterPointIdentifier)) {
          return false;
        }
        uFZRasterPointCollectionIdentifiers.push(uFZRasterPointIdentifier);
        return true;
      });
      return [...uFZRasterPointsToAdd, ...uFZRasterPointCollection];
    }
    return uFZRasterPointCollection;
  }
}
