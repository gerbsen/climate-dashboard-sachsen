import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IUFZMeasurement, NewUFZMeasurement } from '../models/ufz-measurement.model';

export type PartialUpdateUFZMeasurement = Partial<IUFZMeasurement> & Pick<IUFZMeasurement, 'id'>;

type RestOf<T extends IUFZMeasurement | NewUFZMeasurement> = Omit<T, 'date'> & {
  date?: string | null;
};

export type RestUFZMeasurement = RestOf<IUFZMeasurement>;

export type NewRestUFZMeasurement = RestOf<NewUFZMeasurement>;

export type PartialUpdateRestUFZMeasurement = RestOf<PartialUpdateUFZMeasurement>;

export type EntityResponseType = HttpResponse<IUFZMeasurement>;
export type EntityArrayResponseType = HttpResponse<IUFZMeasurement[]>;

@Injectable({ providedIn: 'root' })
export class UFZMeasurementService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/ufz-measurements');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestUFZMeasurement>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  getDates(): Observable<HttpResponse<string[]>> {
    return this.http.get<string[]>(`${this.resourceUrl}/dates`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUFZMeasurement[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  getUFZMeasurementIdentifier(uFZMeasurement: Pick<IUFZMeasurement, 'id'>): number {
    return uFZMeasurement.id;
  }

  compareUFZMeasurement(o1: Pick<IUFZMeasurement, 'id'> | null, o2: Pick<IUFZMeasurement, 'id'> | null): boolean {
    return o1 && o2 ? this.getUFZMeasurementIdentifier(o1) === this.getUFZMeasurementIdentifier(o2) : o1 === o2;
  }

  addUFZMeasurementToCollectionIfMissing<Type extends Pick<IUFZMeasurement, 'id'>>(
    uFZMeasurementCollection: Type[],
    ...uFZMeasurementsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const uFZMeasurements: Type[] = uFZMeasurementsToCheck.filter(isPresent);
    if (uFZMeasurements.length > 0) {
      const uFZMeasurementCollectionIdentifiers = uFZMeasurementCollection.map(
        uFZMeasurementItem => this.getUFZMeasurementIdentifier(uFZMeasurementItem)!
      );
      const uFZMeasurementsToAdd = uFZMeasurements.filter(uFZMeasurementItem => {
        const uFZMeasurementIdentifier = this.getUFZMeasurementIdentifier(uFZMeasurementItem);
        if (uFZMeasurementCollectionIdentifiers.includes(uFZMeasurementIdentifier)) {
          return false;
        }
        uFZMeasurementCollectionIdentifiers.push(uFZMeasurementIdentifier);
        return true;
      });
      return [...uFZMeasurementsToAdd, ...uFZMeasurementCollection];
    }
    return uFZMeasurementCollection;
  }

  protected convertDateFromClient<T extends IUFZMeasurement | NewUFZMeasurement | PartialUpdateUFZMeasurement>(
    uFZMeasurement: T
  ): RestOf<T> {
    return {
      ...uFZMeasurement,
      date: uFZMeasurement.date?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restUFZMeasurement: RestUFZMeasurement): IUFZMeasurement {
    return {
      ...restUFZMeasurement,
      date: restUFZMeasurement.date ? dayjs(restUFZMeasurement.date) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestUFZMeasurement>): HttpResponse<IUFZMeasurement> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestUFZMeasurement[]>): HttpResponse<IUFZMeasurement[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
