import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IUFZRasterPoint } from '../../models/ufz-raster-point.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../../samples/ufz-raster-point.test-samples';

import { UFZRasterPointService } from '../ufz-raster-point.service';

const requireRestSample: IUFZRasterPoint = {
  ...sampleWithRequiredData,
};

describe('UFZRasterPoint Service', () => {
  let service: UFZRasterPointService;
  let httpMock: HttpTestingController;
  let expectedResult: IUFZRasterPoint | IUFZRasterPoint[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(UFZRasterPointService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of UFZRasterPoint', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    describe('addUFZRasterPointToCollectionIfMissing', () => {
      it('should add a UFZRasterPoint to an empty array', () => {
        const uFZRasterPoint: IUFZRasterPoint = sampleWithRequiredData;
        expectedResult = service.addUFZRasterPointToCollectionIfMissing([], uFZRasterPoint);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(uFZRasterPoint);
      });

      it('should not add a UFZRasterPoint to an array that contains it', () => {
        const uFZRasterPoint: IUFZRasterPoint = sampleWithRequiredData;
        const uFZRasterPointCollection: IUFZRasterPoint[] = [
          {
            ...uFZRasterPoint,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addUFZRasterPointToCollectionIfMissing(uFZRasterPointCollection, uFZRasterPoint);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a UFZRasterPoint to an array that doesn't contain it", () => {
        const uFZRasterPoint: IUFZRasterPoint = sampleWithRequiredData;
        const uFZRasterPointCollection: IUFZRasterPoint[] = [sampleWithPartialData];
        expectedResult = service.addUFZRasterPointToCollectionIfMissing(uFZRasterPointCollection, uFZRasterPoint);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(uFZRasterPoint);
      });

      it('should add only unique UFZRasterPoint to an array', () => {
        const uFZRasterPointArray: IUFZRasterPoint[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const uFZRasterPointCollection: IUFZRasterPoint[] = [sampleWithRequiredData];
        expectedResult = service.addUFZRasterPointToCollectionIfMissing(uFZRasterPointCollection, ...uFZRasterPointArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const uFZRasterPoint: IUFZRasterPoint = sampleWithRequiredData;
        const uFZRasterPoint2: IUFZRasterPoint = sampleWithPartialData;
        expectedResult = service.addUFZRasterPointToCollectionIfMissing([], uFZRasterPoint, uFZRasterPoint2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(uFZRasterPoint);
        expect(expectedResult).toContain(uFZRasterPoint2);
      });

      it('should accept null and undefined values', () => {
        const uFZRasterPoint: IUFZRasterPoint = sampleWithRequiredData;
        expectedResult = service.addUFZRasterPointToCollectionIfMissing([], null, uFZRasterPoint, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(uFZRasterPoint);
      });

      it('should return initial array if no UFZRasterPoint is added', () => {
        const uFZRasterPointCollection: IUFZRasterPoint[] = [sampleWithRequiredData];
        expectedResult = service.addUFZRasterPointToCollectionIfMissing(uFZRasterPointCollection, undefined, null);
        expect(expectedResult).toEqual(uFZRasterPointCollection);
      });
    });

    describe('compareUFZRasterPoint', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareUFZRasterPoint(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareUFZRasterPoint(entity1, entity2);
        const compareResult2 = service.compareUFZRasterPoint(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareUFZRasterPoint(entity1, entity2);
        const compareResult2 = service.compareUFZRasterPoint(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareUFZRasterPoint(entity1, entity2);
        const compareResult2 = service.compareUFZRasterPoint(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
