import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IUFZMeasurement } from '../../models/ufz-measurement.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../../samples/ufz-measurement.test-samples';

import { UFZMeasurementService, RestUFZMeasurement } from '../ufz-measurement.service';

const requireRestSample: RestUFZMeasurement = {
  ...sampleWithRequiredData,
  date: sampleWithRequiredData.date?.format(DATE_FORMAT),
};

describe('UFZMeasurement Service', () => {
  let service: UFZMeasurementService;
  let httpMock: HttpTestingController;
  let expectedResult: IUFZMeasurement | IUFZMeasurement[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(UFZMeasurementService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of UFZMeasurement', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    describe('addUFZMeasurementToCollectionIfMissing', () => {
      it('should add a UFZMeasurement to an empty array', () => {
        const uFZMeasurement: IUFZMeasurement = sampleWithRequiredData;
        expectedResult = service.addUFZMeasurementToCollectionIfMissing([], uFZMeasurement);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(uFZMeasurement);
      });

      it('should not add a UFZMeasurement to an array that contains it', () => {
        const uFZMeasurement: IUFZMeasurement = sampleWithRequiredData;
        const uFZMeasurementCollection: IUFZMeasurement[] = [
          {
            ...uFZMeasurement,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addUFZMeasurementToCollectionIfMissing(uFZMeasurementCollection, uFZMeasurement);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a UFZMeasurement to an array that doesn't contain it", () => {
        const uFZMeasurement: IUFZMeasurement = sampleWithRequiredData;
        const uFZMeasurementCollection: IUFZMeasurement[] = [sampleWithPartialData];
        expectedResult = service.addUFZMeasurementToCollectionIfMissing(uFZMeasurementCollection, uFZMeasurement);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(uFZMeasurement);
      });

      it('should add only unique UFZMeasurement to an array', () => {
        const uFZMeasurementArray: IUFZMeasurement[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const uFZMeasurementCollection: IUFZMeasurement[] = [sampleWithRequiredData];
        expectedResult = service.addUFZMeasurementToCollectionIfMissing(uFZMeasurementCollection, ...uFZMeasurementArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const uFZMeasurement: IUFZMeasurement = sampleWithRequiredData;
        const uFZMeasurement2: IUFZMeasurement = sampleWithPartialData;
        expectedResult = service.addUFZMeasurementToCollectionIfMissing([], uFZMeasurement, uFZMeasurement2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(uFZMeasurement);
        expect(expectedResult).toContain(uFZMeasurement2);
      });

      it('should accept null and undefined values', () => {
        const uFZMeasurement: IUFZMeasurement = sampleWithRequiredData;
        expectedResult = service.addUFZMeasurementToCollectionIfMissing([], null, uFZMeasurement, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(uFZMeasurement);
      });

      it('should return initial array if no UFZMeasurement is added', () => {
        const uFZMeasurementCollection: IUFZMeasurement[] = [sampleWithRequiredData];
        expectedResult = service.addUFZMeasurementToCollectionIfMissing(uFZMeasurementCollection, undefined, null);
        expect(expectedResult).toEqual(uFZMeasurementCollection);
      });
    });

    describe('compareUFZMeasurement', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareUFZMeasurement(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareUFZMeasurement(entity1, entity2);
        const compareResult2 = service.compareUFZMeasurement(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareUFZMeasurement(entity1, entity2);
        const compareResult2 = service.compareUFZMeasurement(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareUFZMeasurement(entity1, entity2);
        const compareResult2 = service.compareUFZMeasurement(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
