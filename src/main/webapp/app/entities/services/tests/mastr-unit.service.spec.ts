import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IMastrUnit } from '../../models/mastr-unit.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../../models/tests/mastr-unit.test-samples';

import { MastrUnitService, RestMastrUnit } from '../mastr-unit.service';

const requireRestSample: RestMastrUnit = {
  ...sampleWithRequiredData,
  datumLetzteAktualisierung: sampleWithRequiredData.datumLetzteAktualisierung?.format(DATE_FORMAT),
  einheitRegistrierungsDatum: sampleWithRequiredData.einheitRegistrierungsDatum?.format(DATE_FORMAT),
  endgueltigeStilllegungDatum: sampleWithRequiredData.endgueltigeStilllegungDatum?.format(DATE_FORMAT),
  geplantesInbetriebsnahmeDatum: sampleWithRequiredData.geplantesInbetriebsnahmeDatum?.format(DATE_FORMAT),
  inbetriebnahmeDatum: sampleWithRequiredData.inbetriebnahmeDatum?.format(DATE_FORMAT),
  kwkAnlageInbetriebnahmeDatum: sampleWithRequiredData.kwkAnlageInbetriebnahmeDatum?.format(DATE_FORMAT),
  kwkAnlageRegistrierungsDatum: sampleWithRequiredData.kwkAnlageRegistrierungsDatum?.format(DATE_FORMAT),
  eegInbetriebnahmeDatum: sampleWithRequiredData.eegInbetriebnahmeDatum?.format(DATE_FORMAT),
  eegAnlageRegistrierungsDatum: sampleWithRequiredData.eegAnlageRegistrierungsDatum?.format(DATE_FORMAT),
  genehmigungDatum: sampleWithRequiredData.genehmigungDatum?.format(DATE_FORMAT),
  genehmigungRegistrierungsDatum: sampleWithRequiredData.genehmigungRegistrierungsDatum?.format(DATE_FORMAT),
};

describe('MastrUnit Service', () => {
  let service: MastrUnitService;
  let httpMock: HttpTestingController;
  let expectedResult: IMastrUnit | IMastrUnit[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(MastrUnitService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of MastrUnit', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    describe('addMastrUnitToCollectionIfMissing', () => {
      it('should add a MastrUnit to an empty array', () => {
        const mastrUnit: IMastrUnit = sampleWithRequiredData;
        expectedResult = service.addMastrUnitToCollectionIfMissing([], mastrUnit);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(mastrUnit);
      });

      it('should not add a MastrUnit to an array that contains it', () => {
        const mastrUnit: IMastrUnit = sampleWithRequiredData;
        const mastrUnitCollection: IMastrUnit[] = [
          {
            ...mastrUnit,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addMastrUnitToCollectionIfMissing(mastrUnitCollection, mastrUnit);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a MastrUnit to an array that doesn't contain it", () => {
        const mastrUnit: IMastrUnit = sampleWithRequiredData;
        const mastrUnitCollection: IMastrUnit[] = [sampleWithPartialData];
        expectedResult = service.addMastrUnitToCollectionIfMissing(mastrUnitCollection, mastrUnit);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(mastrUnit);
      });

      it('should add only unique MastrUnit to an array', () => {
        const mastrUnitArray: IMastrUnit[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const mastrUnitCollection: IMastrUnit[] = [sampleWithRequiredData];
        expectedResult = service.addMastrUnitToCollectionIfMissing(mastrUnitCollection, ...mastrUnitArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const mastrUnit: IMastrUnit = sampleWithRequiredData;
        const mastrUnit2: IMastrUnit = sampleWithPartialData;
        expectedResult = service.addMastrUnitToCollectionIfMissing([], mastrUnit, mastrUnit2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(mastrUnit);
        expect(expectedResult).toContain(mastrUnit2);
      });

      it('should accept null and undefined values', () => {
        const mastrUnit: IMastrUnit = sampleWithRequiredData;
        expectedResult = service.addMastrUnitToCollectionIfMissing([], null, mastrUnit, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(mastrUnit);
      });

      it('should return initial array if no MastrUnit is added', () => {
        const mastrUnitCollection: IMastrUnit[] = [sampleWithRequiredData];
        expectedResult = service.addMastrUnitToCollectionIfMissing(mastrUnitCollection, undefined, null);
        expect(expectedResult).toEqual(mastrUnitCollection);
      });
    });

    describe('compareMastrUnit', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareMastrUnit(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareMastrUnit(entity1, entity2);
        const compareResult2 = service.compareMastrUnit(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareMastrUnit(entity1, entity2);
        const compareResult2 = service.compareMastrUnit(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareMastrUnit(entity1, entity2);
        const compareResult2 = service.compareMastrUnit(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
