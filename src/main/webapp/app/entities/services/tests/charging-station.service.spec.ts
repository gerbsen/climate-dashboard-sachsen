import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IChargingStation } from '../../models/charging-station.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../../models/tests/charging-station.test-samples';

import { ChargingStationService, RestChargingStation } from '../charging-station.service';

const requireRestSample: RestChargingStation = {
  ...sampleWithRequiredData,
  inbetriebnahmeDatum: sampleWithRequiredData.inbetriebnahmeDatum?.format(DATE_FORMAT),
};

describe('ChargingStation Service', () => {
  let service: ChargingStationService;
  let httpMock: HttpTestingController;
  let expectedResult: IChargingStation | IChargingStation[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ChargingStationService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ChargingStation', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    describe('addChargingStationToCollectionIfMissing', () => {
      it('should add a ChargingStation to an empty array', () => {
        const chargingStation: IChargingStation = sampleWithRequiredData;
        expectedResult = service.addChargingStationToCollectionIfMissing([], chargingStation);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(chargingStation);
      });

      it('should not add a ChargingStation to an array that contains it', () => {
        const chargingStation: IChargingStation = sampleWithRequiredData;
        const chargingStationCollection: IChargingStation[] = [
          {
            ...chargingStation,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addChargingStationToCollectionIfMissing(chargingStationCollection, chargingStation);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ChargingStation to an array that doesn't contain it", () => {
        const chargingStation: IChargingStation = sampleWithRequiredData;
        const chargingStationCollection: IChargingStation[] = [sampleWithPartialData];
        expectedResult = service.addChargingStationToCollectionIfMissing(chargingStationCollection, chargingStation);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(chargingStation);
      });

      it('should add only unique ChargingStation to an array', () => {
        const chargingStationArray: IChargingStation[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const chargingStationCollection: IChargingStation[] = [sampleWithRequiredData];
        expectedResult = service.addChargingStationToCollectionIfMissing(chargingStationCollection, ...chargingStationArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const chargingStation: IChargingStation = sampleWithRequiredData;
        const chargingStation2: IChargingStation = sampleWithPartialData;
        expectedResult = service.addChargingStationToCollectionIfMissing([], chargingStation, chargingStation2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(chargingStation);
        expect(expectedResult).toContain(chargingStation2);
      });

      it('should accept null and undefined values', () => {
        const chargingStation: IChargingStation = sampleWithRequiredData;
        expectedResult = service.addChargingStationToCollectionIfMissing([], null, chargingStation, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(chargingStation);
      });

      it('should return initial array if no ChargingStation is added', () => {
        const chargingStationCollection: IChargingStation[] = [sampleWithRequiredData];
        expectedResult = service.addChargingStationToCollectionIfMissing(chargingStationCollection, undefined, null);
        expect(expectedResult).toEqual(chargingStationCollection);
      });
    });

    describe('compareChargingStation', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareChargingStation(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareChargingStation(entity1, entity2);
        const compareResult2 = service.compareChargingStation(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareChargingStation(entity1, entity2);
        const compareResult2 = service.compareChargingStation(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareChargingStation(entity1, entity2);
        const compareResult2 = service.compareChargingStation(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
