import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IDWDMeasurement } from '../../dwd-measurement/dwd-measurement.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../../dwd-measurement/dwd-measurement.test-samples';

import { DWDMeasurementService } from '../dwd-measurement.service';

const requireRestSample: IDWDMeasurement = {
  ...sampleWithRequiredData,
};

describe('DWDMeasurement Service', () => {
  let service: DWDMeasurementService;
  let httpMock: HttpTestingController;
  let expectedResult: IDWDMeasurement | IDWDMeasurement[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(DWDMeasurementService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of DWDMeasurement', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    describe('addDWDMeasurementToCollectionIfMissing', () => {
      it('should add a DWDMeasurement to an empty array', () => {
        const dWDMeasurement: IDWDMeasurement = sampleWithRequiredData;
        expectedResult = service.addDWDMeasurementToCollectionIfMissing([], dWDMeasurement);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(dWDMeasurement);
      });

      it('should not add a DWDMeasurement to an array that contains it', () => {
        const dWDMeasurement: IDWDMeasurement = sampleWithRequiredData;
        const dWDMeasurementCollection: IDWDMeasurement[] = [
          {
            ...dWDMeasurement,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addDWDMeasurementToCollectionIfMissing(dWDMeasurementCollection, dWDMeasurement);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a DWDMeasurement to an array that doesn't contain it", () => {
        const dWDMeasurement: IDWDMeasurement = sampleWithRequiredData;
        const dWDMeasurementCollection: IDWDMeasurement[] = [sampleWithPartialData];
        expectedResult = service.addDWDMeasurementToCollectionIfMissing(dWDMeasurementCollection, dWDMeasurement);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(dWDMeasurement);
      });

      it('should add only unique DWDMeasurement to an array', () => {
        const dWDMeasurementArray: IDWDMeasurement[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const dWDMeasurementCollection: IDWDMeasurement[] = [sampleWithRequiredData];
        expectedResult = service.addDWDMeasurementToCollectionIfMissing(dWDMeasurementCollection, ...dWDMeasurementArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const dWDMeasurement: IDWDMeasurement = sampleWithRequiredData;
        const dWDMeasurement2: IDWDMeasurement = sampleWithPartialData;
        expectedResult = service.addDWDMeasurementToCollectionIfMissing([], dWDMeasurement, dWDMeasurement2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(dWDMeasurement);
        expect(expectedResult).toContain(dWDMeasurement2);
      });

      it('should accept null and undefined values', () => {
        const dWDMeasurement: IDWDMeasurement = sampleWithRequiredData;
        expectedResult = service.addDWDMeasurementToCollectionIfMissing([], null, dWDMeasurement, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(dWDMeasurement);
      });

      it('should return initial array if no DWDMeasurement is added', () => {
        const dWDMeasurementCollection: IDWDMeasurement[] = [sampleWithRequiredData];
        expectedResult = service.addDWDMeasurementToCollectionIfMissing(dWDMeasurementCollection, undefined, null);
        expect(expectedResult).toEqual(dWDMeasurementCollection);
      });
    });

    describe('compareDWDMeasurement', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareDWDMeasurement(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareDWDMeasurement(entity1, entity2);
        const compareResult2 = service.compareDWDMeasurement(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareDWDMeasurement(entity1, entity2);
        const compareResult2 = service.compareDWDMeasurement(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareDWDMeasurement(entity1, entity2);
        const compareResult2 = service.compareDWDMeasurement(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
