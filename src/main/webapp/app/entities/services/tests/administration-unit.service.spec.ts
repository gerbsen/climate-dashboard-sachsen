import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IAdministrationUnit } from '../../models/administration-unit.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../../models/tests/administration-unit.test-samples';

import { AdministrationUnitService, RestAdministrationUnit } from '../administration-unit.service';

const requireRestSample: RestAdministrationUnit = {
  ...sampleWithRequiredData,
  beginnlebenszeit: sampleWithRequiredData.beginnlebenszeit?.format(DATE_FORMAT),
  wirksamkeitWsk: sampleWithRequiredData.wirksamkeitWsk?.format(DATE_FORMAT),
};

describe('AdministrationUnit Service', () => {
  let service: AdministrationUnitService;
  let httpMock: HttpTestingController;
  let expectedResult: IAdministrationUnit | IAdministrationUnit[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AdministrationUnitService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of AdministrationUnit', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    describe('addAdministrationUnitToCollectionIfMissing', () => {
      it('should add a AdministrationUnit to an empty array', () => {
        const administrationUnit: IAdministrationUnit = sampleWithRequiredData;
        expectedResult = service.addAdministrationUnitToCollectionIfMissing([], administrationUnit);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(administrationUnit);
      });

      it('should not add a AdministrationUnit to an array that contains it', () => {
        const administrationUnit: IAdministrationUnit = sampleWithRequiredData;
        const administrationUnitCollection: IAdministrationUnit[] = [
          {
            ...administrationUnit,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addAdministrationUnitToCollectionIfMissing(administrationUnitCollection, administrationUnit);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a AdministrationUnit to an array that doesn't contain it", () => {
        const administrationUnit: IAdministrationUnit = sampleWithRequiredData;
        const administrationUnitCollection: IAdministrationUnit[] = [sampleWithPartialData];
        expectedResult = service.addAdministrationUnitToCollectionIfMissing(administrationUnitCollection, administrationUnit);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(administrationUnit);
      });

      it('should add only unique AdministrationUnit to an array', () => {
        const administrationUnitArray: IAdministrationUnit[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const administrationUnitCollection: IAdministrationUnit[] = [sampleWithRequiredData];
        expectedResult = service.addAdministrationUnitToCollectionIfMissing(administrationUnitCollection, ...administrationUnitArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const administrationUnit: IAdministrationUnit = sampleWithRequiredData;
        const administrationUnit2: IAdministrationUnit = sampleWithPartialData;
        expectedResult = service.addAdministrationUnitToCollectionIfMissing([], administrationUnit, administrationUnit2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(administrationUnit);
        expect(expectedResult).toContain(administrationUnit2);
      });

      it('should accept null and undefined values', () => {
        const administrationUnit: IAdministrationUnit = sampleWithRequiredData;
        expectedResult = service.addAdministrationUnitToCollectionIfMissing([], null, administrationUnit, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(administrationUnit);
      });

      it('should return initial array if no AdministrationUnit is added', () => {
        const administrationUnitCollection: IAdministrationUnit[] = [sampleWithRequiredData];
        expectedResult = service.addAdministrationUnitToCollectionIfMissing(administrationUnitCollection, undefined, null);
        expect(expectedResult).toEqual(administrationUnitCollection);
      });
    });

    describe('compareAdministrationUnit', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareAdministrationUnit(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareAdministrationUnit(entity1, entity2);
        const compareResult2 = service.compareAdministrationUnit(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareAdministrationUnit(entity1, entity2);
        const compareResult2 = service.compareAdministrationUnit(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareAdministrationUnit(entity1, entity2);
        const compareResult2 = service.compareAdministrationUnit(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
