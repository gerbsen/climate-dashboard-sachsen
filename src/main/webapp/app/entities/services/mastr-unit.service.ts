import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';
import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMastrUnit, NewMastrUnit } from '../models/mastr-unit.model';
import {AggregateFunction, EnergySource, IMastrUnitForIntervalStatistics, IntervalFormat, MastrUnitProperty, OperationState} from "../models/mastr-unit-for-interval-statistics.model";

export type PartialUpdateMastrUnit = Partial<IMastrUnit> & Pick<IMastrUnit, 'id'>;

type RestOf<T extends IMastrUnit | NewMastrUnit> = Omit<
  T,
  | 'datumLetzteAktualisierung'
  | 'einheitRegistrierungsDatum'
  | 'endgueltigeStilllegungDatum'
  | 'geplantesInbetriebsnahmeDatum'
  | 'inbetriebnahmeDatum'
  | 'kwkAnlageInbetriebnahmeDatum'
  | 'kwkAnlageRegistrierungsDatum'
  | 'eegInbetriebnahmeDatum'
  | 'eegAnlageRegistrierungsDatum'
  | 'genehmigungDatum'
  | 'genehmigungRegistrierungsDatum'
> & {
  datumLetzteAktualisierung?: string | null;
  einheitRegistrierungsDatum?: string | null;
  endgueltigeStilllegungDatum?: string | null;
  geplantesInbetriebsnahmeDatum?: string | null;
  inbetriebnahmeDatum?: string | null;
  kwkAnlageInbetriebnahmeDatum?: string | null;
  kwkAnlageRegistrierungsDatum?: string | null;
  eegInbetriebnahmeDatum?: string | null;
  eegAnlageRegistrierungsDatum?: string | null;
  genehmigungDatum?: string | null;
  genehmigungRegistrierungsDatum?: string | null;
};

export type RestMastrUnit = RestOf<IMastrUnit>;

export type NewRestMastrUnit = RestOf<NewMastrUnit>;

export type PartialUpdateRestMastrUnit = RestOf<PartialUpdateMastrUnit>;

export type EntityResponseType = HttpResponse<IMastrUnit>;
export type EntityArrayResponseType = HttpResponse<IMastrUnit[]>;

@Injectable({ providedIn: 'root' })
export class MastrUnitService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/mastr-units');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestMastrUnit>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  queryMastrUnitPublicId(mastrNummer: string): Observable<any> {
    return this.http
      .get<any>(`${this.resourceUrl}/mastr-search/${mastrNummer}`, { observe: 'response' })
      .pipe(map(res => res.body));;
  }

  getMastrUnitStateStatistics(operationState: OperationState, energySource: EnergySource): Observable<any> {
    return this.http
      .get<any>(`${this.resourceUrl}/state`, { params: { energySource, operationState }, observe: 'response' })
    .pipe(map(res => res.body));
  }

  getMastrStatisticsForIntervalForState(
    interval: IntervalFormat, 
    property: MastrUnitProperty, 
    energySource: EnergySource, 
    aggregateFunction: AggregateFunction = "SUM"): Observable<IMastrUnitForIntervalStatistics | null> {
    return this.http
        .get<IMastrUnitForIntervalStatistics>(`${this.resourceUrl}/state/statistics-for-interval`, { params: {
            interval, property, energySource, aggregateFunction }, observe: 'response' })
        .pipe(map(res => res.body));
  }

  getMastrStatisticsForIntervalForCommunity(ags: string, interval: IntervalFormat, property: MastrUnitProperty, energySource: EnergySource, aggregateFunction: AggregateFunction = "SUM"): Observable<IMastrUnitForIntervalStatistics | null> {
    return this.http
        .get<IMastrUnitForIntervalStatistics>(`${this.resourceUrl}/${ags}/statistics-for-interval`, { params: {
            interval, property, energySource, aggregateFunction }, observe: 'response' })
        .pipe(map(res => res.body));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestMastrUnit[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  getMastrUnitIdentifier(mastrUnit: Pick<IMastrUnit, 'id'>): number {
    return mastrUnit.id;
  }

  compareMastrUnit(o1: Pick<IMastrUnit, 'id'> | null, o2: Pick<IMastrUnit, 'id'> | null): boolean {
    return o1 && o2 ? this.getMastrUnitIdentifier(o1) === this.getMastrUnitIdentifier(o2) : o1 === o2;
  }

  addMastrUnitToCollectionIfMissing<Type extends Pick<IMastrUnit, 'id'>>(
    mastrUnitCollection: Type[],
    ...mastrUnitsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const mastrUnits: Type[] = mastrUnitsToCheck.filter(isPresent);
    if (mastrUnits.length > 0) {
      const mastrUnitCollectionIdentifiers = mastrUnitCollection.map(mastrUnitItem => this.getMastrUnitIdentifier(mastrUnitItem)!);
      const mastrUnitsToAdd = mastrUnits.filter(mastrUnitItem => {
        const mastrUnitIdentifier = this.getMastrUnitIdentifier(mastrUnitItem);
        if (mastrUnitCollectionIdentifiers.includes(mastrUnitIdentifier)) {
          return false;
        }
        mastrUnitCollectionIdentifiers.push(mastrUnitIdentifier);
        return true;
      });
      return [...mastrUnitsToAdd, ...mastrUnitCollection];
    }
    return mastrUnitCollection;
  }

  protected convertDateFromClient<T extends IMastrUnit | NewMastrUnit | PartialUpdateMastrUnit>(mastrUnit: T): RestOf<T> {
    return {
      ...mastrUnit,
      datumLetzteAktualisierung: mastrUnit.datumLetzteAktualisierung?.format(DATE_FORMAT) ?? null,
      einheitRegistrierungsDatum: mastrUnit.einheitRegistrierungsDatum?.format(DATE_FORMAT) ?? null,
      endgueltigeStilllegungDatum: mastrUnit.endgueltigeStilllegungDatum?.format(DATE_FORMAT) ?? null,
      geplantesInbetriebsnahmeDatum: mastrUnit.geplantesInbetriebsnahmeDatum?.format(DATE_FORMAT) ?? null,
      inbetriebnahmeDatum: mastrUnit.inbetriebnahmeDatum?.format(DATE_FORMAT) ?? null,
      kwkAnlageInbetriebnahmeDatum: mastrUnit.kwkAnlageInbetriebnahmeDatum?.format(DATE_FORMAT) ?? null,
      kwkAnlageRegistrierungsDatum: mastrUnit.kwkAnlageRegistrierungsDatum?.format(DATE_FORMAT) ?? null,
      eegInbetriebnahmeDatum: mastrUnit.eegInbetriebnahmeDatum?.format(DATE_FORMAT) ?? null,
      eegAnlageRegistrierungsDatum: mastrUnit.eegAnlageRegistrierungsDatum?.format(DATE_FORMAT) ?? null,
      genehmigungDatum: mastrUnit.genehmigungDatum?.format(DATE_FORMAT) ?? null,
      genehmigungRegistrierungsDatum: mastrUnit.genehmigungRegistrierungsDatum?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restMastrUnit: RestMastrUnit): IMastrUnit {
    return {
      ...restMastrUnit,
      datumLetzteAktualisierung: restMastrUnit.datumLetzteAktualisierung ? dayjs(restMastrUnit.datumLetzteAktualisierung) : undefined,
      einheitRegistrierungsDatum: restMastrUnit.einheitRegistrierungsDatum ? dayjs(restMastrUnit.einheitRegistrierungsDatum) : undefined,
      endgueltigeStilllegungDatum: restMastrUnit.endgueltigeStilllegungDatum ? dayjs(restMastrUnit.endgueltigeStilllegungDatum) : undefined,
      geplantesInbetriebsnahmeDatum: restMastrUnit.geplantesInbetriebsnahmeDatum
        ? dayjs(restMastrUnit.geplantesInbetriebsnahmeDatum)
        : undefined,
      inbetriebnahmeDatum: restMastrUnit.inbetriebnahmeDatum ? dayjs(restMastrUnit.inbetriebnahmeDatum) : undefined,
      kwkAnlageInbetriebnahmeDatum: restMastrUnit.kwkAnlageInbetriebnahmeDatum
        ? dayjs(restMastrUnit.kwkAnlageInbetriebnahmeDatum)
        : undefined,
      kwkAnlageRegistrierungsDatum: restMastrUnit.kwkAnlageRegistrierungsDatum
        ? dayjs(restMastrUnit.kwkAnlageRegistrierungsDatum)
        : undefined,
      eegInbetriebnahmeDatum: restMastrUnit.eegInbetriebnahmeDatum ? dayjs(restMastrUnit.eegInbetriebnahmeDatum) : undefined,
      eegAnlageRegistrierungsDatum: restMastrUnit.eegAnlageRegistrierungsDatum
        ? dayjs(restMastrUnit.eegAnlageRegistrierungsDatum)
        : undefined,
      genehmigungDatum: restMastrUnit.genehmigungDatum ? dayjs(restMastrUnit.genehmigungDatum) : undefined,
      genehmigungRegistrierungsDatum: restMastrUnit.genehmigungRegistrierungsDatum
        ? dayjs(restMastrUnit.genehmigungRegistrierungsDatum)
        : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestMastrUnit>): HttpResponse<IMastrUnit> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestMastrUnit[]>): HttpResponse<IMastrUnit[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
