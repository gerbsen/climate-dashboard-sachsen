import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDWDMeasurement } from '../models/dwd-measurement.model';
import { DWDState } from 'app/config/state.constants';
import { IDWDMeasurementDTO } from '../models/dwd-measurement-dto';

export type PartialUpdateDWDMeasurement = Partial<IDWDMeasurement> & Pick<IDWDMeasurement, 'id'>;

export type EntityResponseType = HttpResponse<IDWDMeasurement>;
export type EntityArrayResponseType = HttpResponse<IDWDMeasurement[]>;

@Injectable({ providedIn: 'root' })
export class DWDMeasurementService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/dwd-measurements');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDWDMeasurement>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getDWDMeasurementsForState(state: DWDState): Observable<HttpResponse<IDWDMeasurementDTO>> {
    return this.http.get<IDWDMeasurementDTO>(`${this.resourceUrl}/time-series/all/${state}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDWDMeasurement[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  getDWDMeasurementIdentifier(dWDMeasurement: Pick<IDWDMeasurement, 'id'>): number {
    return dWDMeasurement.id;
  }

  compareDWDMeasurement(o1: Pick<IDWDMeasurement, 'id'> | null, o2: Pick<IDWDMeasurement, 'id'> | null): boolean {
    return o1 && o2 ? this.getDWDMeasurementIdentifier(o1) === this.getDWDMeasurementIdentifier(o2) : o1 === o2;
  }

  addDWDMeasurementToCollectionIfMissing<Type extends Pick<IDWDMeasurement, 'id'>>(
    dWDMeasurementCollection: Type[],
    ...dWDMeasurementsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const dWDMeasurements: Type[] = dWDMeasurementsToCheck.filter(isPresent);
    if (dWDMeasurements.length > 0) {
      const dWDMeasurementCollectionIdentifiers = dWDMeasurementCollection.map(
        dWDMeasurementItem => this.getDWDMeasurementIdentifier(dWDMeasurementItem)!
      );
      const dWDMeasurementsToAdd = dWDMeasurements.filter(dWDMeasurementItem => {
        const dWDMeasurementIdentifier = this.getDWDMeasurementIdentifier(dWDMeasurementItem);
        if (dWDMeasurementCollectionIdentifiers.includes(dWDMeasurementIdentifier)) {
          return false;
        }
        dWDMeasurementCollectionIdentifiers.push(dWDMeasurementIdentifier);
        return true;
      });
      return [...dWDMeasurementsToAdd, ...dWDMeasurementCollection];
    }
    return dWDMeasurementCollection;
  }
}
