import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAdministrationUnit, NewAdministrationUnit } from '../models/administration-unit.model';

export type PartialUpdateAdministrationUnit = Partial<IAdministrationUnit> & Pick<IAdministrationUnit, 'id'>;

type RestOf<T extends IAdministrationUnit | NewAdministrationUnit> = Omit<T, 'beginnlebenszeit' | 'wirksamkeitWsk'> & {
  beginnlebenszeit?: string | null;
  wirksamkeitWsk?: string | null;
};

export type RestAdministrationUnit = RestOf<IAdministrationUnit>;

export type NewRestAdministrationUnit = RestOf<NewAdministrationUnit>;

export type PartialUpdateRestAdministrationUnit = RestOf<PartialUpdateAdministrationUnit>;

export type EntityResponseType = HttpResponse<IAdministrationUnit>;
export type EntityArrayResponseType = HttpResponse<IAdministrationUnit[]>;

@Injectable({ providedIn: 'root' })
export class AdministrationUnitService {
  
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/administration-units');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestAdministrationUnit>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestAdministrationUnit[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  getAdministrationUnitIdentifier(administrationUnit: Pick<IAdministrationUnit, 'id'>): number {
    return administrationUnit.id;
  }

  compareAdministrationUnit(o1: Pick<IAdministrationUnit, 'id'> | null, o2: Pick<IAdministrationUnit, 'id'> | null): boolean {
    return o1 && o2 ? this.getAdministrationUnitIdentifier(o1) === this.getAdministrationUnitIdentifier(o2) : o1 === o2;
  }

  addAdministrationUnitToCollectionIfMissing<Type extends Pick<IAdministrationUnit, 'id'>>(
    administrationUnitCollection: Type[],
    ...administrationUnitsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const administrationUnits: Type[] = administrationUnitsToCheck.filter(isPresent);
    if (administrationUnits.length > 0) {
      const administrationUnitCollectionIdentifiers = administrationUnitCollection.map(
        administrationUnitItem => this.getAdministrationUnitIdentifier(administrationUnitItem)!
      );
      const administrationUnitsToAdd = administrationUnits.filter(administrationUnitItem => {
        const administrationUnitIdentifier = this.getAdministrationUnitIdentifier(administrationUnitItem);
        if (administrationUnitCollectionIdentifiers.includes(administrationUnitIdentifier)) {
          return false;
        }
        administrationUnitCollectionIdentifiers.push(administrationUnitIdentifier);
        return true;
      });
      return [...administrationUnitsToAdd, ...administrationUnitCollection];
    }
    return administrationUnitCollection;
  }

  protected convertDateFromClient<T extends IAdministrationUnit | NewAdministrationUnit | PartialUpdateAdministrationUnit>(
    administrationUnit: T
  ): RestOf<T> {
    return {
      ...administrationUnit,
      beginnlebenszeit: administrationUnit.beginnlebenszeit?.format(DATE_FORMAT) ?? null,
      wirksamkeitWsk: administrationUnit.wirksamkeitWsk?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restAdministrationUnit: RestAdministrationUnit): IAdministrationUnit {
    const temp = {
      ...restAdministrationUnit,
      beginnlebenszeit: restAdministrationUnit.beginnlebenszeit ? dayjs(restAdministrationUnit.beginnlebenszeit) : undefined,
      wirksamkeitWsk: restAdministrationUnit.wirksamkeitWsk ? dayjs(restAdministrationUnit.wirksamkeitWsk) : undefined,
    };
    for (const iStatistic of temp.statistics!) {
      iStatistic.date = iStatistic.date ? dayjs(iStatistic.date) : undefined
    }
    return temp;
  }

  protected convertResponseFromServer(res: HttpResponse<RestAdministrationUnit>): HttpResponse<IAdministrationUnit> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestAdministrationUnit[]>): HttpResponse<IAdministrationUnit[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
