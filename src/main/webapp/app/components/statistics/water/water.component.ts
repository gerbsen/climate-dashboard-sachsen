import {Component} from '@angular/core';
import * as geojson from 'geojson';
import {Source} from "../../../entities/models/source.model";
import {ITocEntry, TocEntry} from "../../../entities/models/toc-entry.model";
import {STATE_CONFIGS, StateConfig, StateName} from "../../../config/state.constants";
import { UFZMeasurementService } from 'app/entities/services/ufz-measurement.service';
import { LngLatBounds, MapLayerMouseEvent } from 'maplibre-gl';
import { MAP_KEY, MAP_STYLES, MapStyle } from 'app/config/maps.constants';
import { IUFZMeasurement } from 'app/entities/models/ufz-measurement.model';
import { UFZRasterPointService } from 'app/entities/services/ufz-raster-point.service';
import { IUFZRasterPoint } from 'app/entities/models/ufz-raster-point.model';
import { CHARTJS_COLORS, COLOR_WATER, COLOR_WHITE } from 'app/config/colors.constants';
import { SlideInOutAnimation } from 'app/animations/animations';
import dayjs from 'dayjs/esm';
type MapTypes = 'oberboden-map' | 'gesamtboden-map' | 'nutzbareFeldkapazitaet-map';

@Component({
  selector: 'jhi-water',
  templateUrl: './water.component.html',
  styleUrls: ['./water.component.scss'],
  animations: [SlideInOutAnimation]
})
export class WaterComponent {

  STATE_CONFIG : StateConfig = STATE_CONFIGS[process.env.STATE as StateName || "Sachsen"];
  mapStyles = MAP_STYLES;
  selectedMapStyle : MapStyle = this.STATE_CONFIG.defaultMapStyle;
  source = new Source("https://www.ufz.de/index.php?de=37937", "UFZ-Dürremonitor - Helmholtz-Zentrum für Umweltforschung");
  
  mapOptions = {
    fitBounds : {} as any,
    options : {
      zoom: this.STATE_CONFIG.mapInitOptions.zoom,
      center: [this.STATE_CONFIG.mapInitOptions.center.lat, this.STATE_CONFIG.mapInitOptions.center.lng],
      attribution : 'openstreetmap.org | Bundesamt für Kartographie und Geodäsie <a href="https://gdz.bkg.bund.de/index.php/default/digitale-geodaten/verwaltungsgebiete/verwaltungsgebiete-1-2-500-000-stand-31-12-vg2500-12-31.html">[GeoSN]</a>',
      tileUrl : `https://api.maptiler.com/maps/${this.selectedMapStyle.key}/style.json?key=${MAP_KEY}`
    },
    geometry : {} as geojson.MultiPolygon,
    bounds: {} as LngLatBounds
  }

  gesamtbodenMeasurements: IUFZMeasurement[] = [];
  gesamtbodenSelectedRasterPoint = 0;
  gesamtbodenSelectedDate : string = "";
  gesamtbodenHoverFilter: ['==', string, any] = ['==', 'ufzRasterPointId', ''];
  gesamtbodenData: IUFZRasterPoint | undefined;
  gesamtbodenChartData: any | undefined = undefined;
  gesamtbodenChartOptions: any = {};
  gesamtbodenChartAnimation = "out";
  gesamtbodenSliderModel = 0;
  gesamtbodenIsLoading = false;
  oberbodenMeasurements: IUFZMeasurement[] = [];
  oberbodenSelectedRasterPoint= 0;
  oberbodenSelectedDate : string = "";
  oberbodenHoverFilter: ['==', string, any] = ['==', 'ufzRasterPointId', ''];
  oberbodenData: IUFZRasterPoint | undefined;
  oberbodenChartData: any | undefined = undefined;
  oberbodenChartOptions: any = {};
  oberbodenChartAnimation = "out";
  oberbodenSliderModel = 0;
  oberbodenIsLoading = false;
  nutzbareFeldkapazitaetMeasurements: IUFZMeasurement[] = [];
  nutzbareFeldkapazitaetSelectedRasterPoint= 0;
  nutzbareFeldkapazitaetSelectedDate : string = "";
  nutzbareFeldkapazitaetHoverFilter: ['==', string, any] = ['==', 'ufzRasterPointId', ''];
  nutzbareFeldkapazitaetData: IUFZRasterPoint | undefined;
  nutzbareFeldkapazitaetChartData: any | undefined = undefined;
  nutzbareFeldkapazitaetChartOptions: any = {};
  nutzbareFeldkapazitaetChartAnimation = "out";
  nutzbareFeldkapazitaetSliderModel = 0;
  nutzbareFeldkapazitaetIsLoading = false;
  possibleDates : string[] = [];

  entries: ITocEntry[] = [];

  constructor(private uFZMeasurementService: UFZMeasurementService, private uFZRasterPointService: UFZRasterPointService) {

    this.uFZMeasurementService.getDates().subscribe(value => {
      
      this.possibleDates = value.body!.reverse();
      this.gesamtbodenSliderModel = this.possibleDates.length - 1;
      this.oberbodenSliderModel = this.possibleDates.length - 1;
      this.nutzbareFeldkapazitaetSliderModel = this.possibleDates.length - 1;
      this.gesamtbodenSelectedDate = this.possibleDates[this.possibleDates.length - 1];
      this.oberbodenSelectedDate = this.possibleDates[this.possibleDates.length - 1];
      this.nutzbareFeldkapazitaetSelectedDate = this.possibleDates[this.possibleDates.length - 1];
    })

    this.uFZMeasurementService.query({ 'date.specified': true, sort: ['date,desc'], size: 1 })
      .subscribe(value => {
        const latestDate = value.body![0].date;
        
        this.uFZMeasurementService.query({ 'date.specified': true, 'date.equals': latestDate, size: 2147483647 })
          .subscribe(value => {
            this.gesamtbodenMeasurements = this.oberbodenMeasurements = this.nutzbareFeldkapazitaetMeasurements = value.body!;
          });
      });

    this.entries.push(new TocEntry("ufz-duerremonitor", "UFZ Dürremonitor", []));
    this.entries.push(new TocEntry("ufz-gesamtboden", "Gesamtboden", []));
    this.entries.push(new TocEntry("ufz-oberboden", "Oberboden", []));
    this.entries.push(new TocEntry("ufz-nutzbareFeldkapazitaet", "Pflanzenverfügbares Wasser", []));

    this.nutzbareFeldkapazitaetChartOptions = this.oberbodenChartOptions = this.gesamtbodenChartOptions = {
      plugins : {
        legend: {  position : 'top', display: true },
        datalabels: { display: false }
      },
      interaction: { intersect: false, mode: 'index', },
      radius: 0,
      fill: false,
      scales: {
        y: {
          type: 'linear',
          display: true,
          position: 'left',
        },
        y1: {
          type: 'linear',
          display: true,
          position: 'right',
          // grid line settings
          grid: {
            drawOnChartArea: false, // only want the grid lines for one axis to show up
          },
        },
      }
    }
  }

  updateRasterPoints(map: MapTypes) {
    
    if ( map === 'gesamtboden-map' ) {

      this.gesamtbodenIsLoading = true;
      this.uFZMeasurementService.query({ 'date.specified': true, 'date.equals': this.possibleDates[this.gesamtbodenSliderModel], size: 2147483647 })
        .subscribe(value => {
          this.gesamtbodenMeasurements = value.body!;
          this.gesamtbodenIsLoading = false;
        });
    }
    else if ( map === 'oberboden-map' ) {
      this.oberbodenIsLoading = true;
      this.uFZMeasurementService.query({ 'date.specified': true, 'date.equals': this.possibleDates[this.oberbodenSliderModel], size: 2147483647 })
        .subscribe(value => {
          this.oberbodenMeasurements = value.body!;
          this.oberbodenIsLoading = false;
        });
    } else if ( map === 'nutzbareFeldkapazitaet-map' ) {
      this.nutzbareFeldkapazitaetIsLoading = true;
      this.uFZMeasurementService.query({ 'date.specified': true, 'date.equals': this.possibleDates[this.nutzbareFeldkapazitaetSliderModel], size: 2147483647 })
        .subscribe(value => {
          this.nutzbareFeldkapazitaetMeasurements = value.body!;
          this.nutzbareFeldkapazitaetIsLoading = false;
        });
    } 
  }

  onClickRasterPoint(evt: MapLayerMouseEvent, mapId: MapTypes) {

    this.uFZRasterPointService.find(evt.features![0].properties!.ufzRasterPointId)
        .subscribe(value => {
          if ( mapId === 'oberboden-map' ) {
            this.oberbodenData = value.body!;
            this.oberbodenChartAnimation = 'in';
            this.oberbodenChartData = this.getChartData(this.oberbodenData);
          }
          else if ( mapId === 'gesamtboden-map' ){
            this.gesamtbodenData = value.body!;
            this.gesamtbodenChartAnimation = 'in';
            this.gesamtbodenChartData = this.getChartData(this.gesamtbodenData);
          } 
          else if ( mapId === 'nutzbareFeldkapazitaet-map') {
            this.nutzbareFeldkapazitaetData = value.body!;
            this.nutzbareFeldkapazitaetChartAnimation = 'in';
            this.nutzbareFeldkapazitaetChartData = this.getChartData(this.nutzbareFeldkapazitaetData);
          }
        });
  }

  updateMap() {
    this.mapOptions.options.tileUrl = `https://api.maptiler.com/maps/${this.selectedMapStyle.key}/style.json?key=${MAP_KEY}`
  }

  activateHoverOn(evt: any, mapId: MapTypes) {
    
    if ( mapId === 'oberboden-map' ) {
      this.oberbodenHoverFilter = ['==', 'ufzRasterPointId', evt.features[0].properties.ufzRasterPointId];
      this.oberbodenSelectedRasterPoint = evt.features[0].properties.smiOberboden;
    }
    else if ( mapId === 'gesamtboden-map' ){
      this.gesamtbodenHoverFilter = ['==', 'ufzRasterPointId', evt.features[0].properties.ufzRasterPointId];
      this.gesamtbodenSelectedRasterPoint = evt.features[0].properties.smiGesamtboden;
    } 
    else if ( mapId === 'nutzbareFeldkapazitaet-map') {
      this.nutzbareFeldkapazitaetHoverFilter = ['==', 'ufzRasterPointId', evt.features[0].properties.ufzRasterPointId];
      this.nutzbareFeldkapazitaetSelectedRasterPoint = evt.features[0].properties.nutzbareFeldkapazitaet;
    }
  }

  disableHover(mapId: MapTypes) {
    if ( mapId === 'oberboden-map' ) {
      this.oberbodenHoverFilter = ['==', 'ufzRasterPointId', ''];
    }
    else if ( mapId === 'gesamtboden-map' ){
      this.gesamtbodenHoverFilter = ['==', 'ufzRasterPointId', ''];
    } 
    else if ( mapId === 'nutzbareFeldkapazitaet-map') {
      this.nutzbareFeldkapazitaetHoverFilter = ['==', 'ufzRasterPointId', ''];
    }
  }

  getChartData(chartData: any): any {
    // this could also be done with a linear gradient but this would make it hard to know which line is which
    // https://codepen.io/plavookac/pen/BpYYdm

    const measurements = chartData.ufzMeasurements?.sort((a : IUFZMeasurement, b :IUFZMeasurement) => dayjs(b.date).isBefore(dayjs(a.date)));

    return {
      labels: measurements.map((m : IUFZMeasurement) => dayjs(m.date).format('DD.MM.YY')),
      datasets: [
        {
          label: 'Nutzbare Feldkapazität',
          data: measurements.map((m : IUFZMeasurement) => m.nutzbareFeldkapazitaet),
          borderColor: COLOR_WATER,
          backgroundColor: COLOR_WATER,
          pointBackgroundColor: COLOR_WATER,
          pointBorderColor: COLOR_WHITE,
          yAxisID: 'y',
          tension: 0.2
        },
        {
          label: 'SMI: Gesamtboden (1,8m)',
          data: measurements.map((m : IUFZMeasurement) => m.smiGesamtboden),
          borderColor: CHARTJS_COLORS[0],
          backgroundColor: CHARTJS_COLORS[0],
          pointBackgroundColor: CHARTJS_COLORS[0],
          pointBorderColor: CHARTJS_COLORS[0],
          yAxisID: 'y1',
          tension: 0.2
        },
        {
          label: 'SMI: Oberboden (0,25m)',
          data: measurements.map((m : IUFZMeasurement) => m.smiOberboden),
          borderColor: CHARTJS_COLORS[2],
          backgroundColor: CHARTJS_COLORS[2],
          pointBackgroundColor: CHARTJS_COLORS[2],
          pointBorderColor: CHARTJS_COLORS[2],
          yAxisID: 'y1',
          tension: 0.2
        }]
      }
  }
}
