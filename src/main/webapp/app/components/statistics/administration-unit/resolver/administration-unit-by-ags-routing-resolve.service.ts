import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { IAdministrationUnit } from 'app/entities/models/administration-unit.model';
import {AdministrationUnitService} from "../../../../entities/services/administration-unit.service";

@Injectable({ providedIn: 'root' })
export class AdministrationUnitByAgsRoutingResolveService implements Resolve<IAdministrationUnit | null> {
  constructor(protected service: AdministrationUnitService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAdministrationUnit | null | never> {
    if (route.params['gemeindeschluesselaufgefuellt']) {
      return this.service.query({ 'gemeindeschluesselaufgefuellt.equals' : route.params['gemeindeschluesselaufgefuellt'], 'ade.equals': 6 }).pipe(
        mergeMap((administrationUnit: HttpResponse<IAdministrationUnit[]>) => {
          if (administrationUnit.body) {
            return of(administrationUnit.body[0]);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
