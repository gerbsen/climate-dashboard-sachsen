import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {IAdministrationUnit} from "../../../entities/models/administration-unit.model";
import {MAP_KEY, MAP_STYLES, MapStyle} from 'app/config/maps.constants';
import * as geojson from 'geojson';
import {LngLatBounds, MapLayerMouseEvent} from 'maplibre-gl';
import {MastrUnitService} from "../../../entities/services/mastr-unit.service";
import {IMastrUnit} from "../../../entities/models/mastr-unit.model";
import {Table} from "primeng/table";
import {AlertService} from "../../../core/util/alert.service";
import {ITocEntry, TocEntry} from "../../../entities/models/toc-entry.model";
import {SOURCES, STATE_CONFIGS, StateConfig, StateName} from "../../../config/state.constants";
import {AdministrationUnitService} from "../../../entities/services/administration-unit.service";
import {IStatistic} from "../../../entities/models/statistic.model";
import { COLOR_ELECTRICITY, MASTR_COLORS } from 'app/config/colors.constants';
import { ChargingStationService } from 'app/entities/services/charging-station.service';
import { IChargingStation } from 'app/entities/models/charging-station.model';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { DecimalPipe } from '@angular/common';
import dayjs from 'dayjs/esm';
import isBetween from 'dayjs/esm/plugin/isBetween';
import dayOfYear from 'dayjs/esm/plugin/dayOfYear';
dayjs.extend(isBetween)
dayjs.extend(dayOfYear)

interface EnergySource {
  key: number,
  color: string,
  units: IMastrUnit[],
  label: string
}

@Component({
  selector: 'jhi-administrationUnit',
  providers: [DecimalPipe],
  templateUrl: './administration-unit.component.html',
  styleUrls: ['./administration-unit.component.scss']
})
export class AdministrationUnitComponent implements OnInit {

  energySources: EnergySource[] = [
    { key: 2497, color: MASTR_COLORS['Wind'], units: [], label: 'Wind' },
    { key: 2495, color: MASTR_COLORS['Solare Strahlungsenergie'], units: [], label: 'Solare Strahlungsenergie' },
    { key: 2493, color: MASTR_COLORS['Biomasse'], units: [], label: 'Biomasse' },
    { key: 2498, color: MASTR_COLORS['Wasser'], units: [], label: 'Wasser' },
    { key: 2496, color: MASTR_COLORS['Speicher'], units: [], label: 'Speicher' },
    { key: 2404, color: MASTR_COLORS['Solarthermie'], units: [], label: 'Solarthermie' },
    { key: 2403, color: MASTR_COLORS['Geothermie'], units: [], label: 'Geothermie' },
    { key: 2413, color: MASTR_COLORS['Wärme'], units: [], label: 'Wärme' },
    { key: 2408, color: MASTR_COLORS['Braunkohle'], units: [], label: 'Braunkohle' },
    { key: 2409, color: MASTR_COLORS['Mineralölprodukte'], units: [], label: 'Mineralölprodukte' },
    { key: 2405, color: MASTR_COLORS['Klärschlamm'], units: [], label: 'Klärschlamm' },
    { key: 2410, color: MASTR_COLORS['Erdgas'], units: [], label: 'Erdgas' },
    { key: 2406, color: MASTR_COLORS['Grubengas'], units: [], label: 'Grubengas' },
    { key: 2411, color: MASTR_COLORS['andere Gase'], units: [], label: 'andere Gase' },
    { key: 2412, color: MASTR_COLORS['nicht biogener Abfall'], units: [], label: 'nicht biogener Abfall' }
  ];
  latestChargingStation!: IChargingStation;
  BNETZA_SOURCE = SOURCES.BNETZA;
  COLOR_ELECTRICITY = COLOR_ELECTRICITY;

  administrationUnit: IAdministrationUnit | null = null;
  parentAdministrationUnit: IAdministrationUnit | null = null;
  chargingStations: IChargingStation[] = [];
  
  STATE_CONFIG : StateConfig = STATE_CONFIGS[process.env.STATE as StateName || "Sachsen"];
  mapStyles = MAP_STYLES;
  selectedMapStyle : MapStyle = this.STATE_CONFIG.defaultMapStyle;
  mapOptions = {
    fitBounds : {} as any,
    options : {
      zoom: this.STATE_CONFIG.mapInitOptions.zoom,
      center: [this.STATE_CONFIG.mapInitOptions.center.lat, this.STATE_CONFIG.mapInitOptions.center.lng],
      attribution : 'openstreetmap.org | Bundesamt für Kartographie und Geodäsie <a href="https://gdz.bkg.bund.de/index.php/default/digitale-geodaten/verwaltungsgebiete/verwaltungsgebiete-1-2-500-000-stand-31-12-vg2500-12-31.html">[GeoSN]</a>',
      tileUrl : `https://api.maptiler.com/maps/${this.selectedMapStyle.key}/style.json?key=${MAP_KEY}`
    },
    geometry : {} as geojson.MultiPolygon,
    bounds: {} as LngLatBounds
  }
  selectedMastrUnit : any;
  selectedChargingStation: any;
  cursorStyle: string = '';
  entries: ITocEntry[] = [];
  sunChartData: any;
  batteryChartData: any;
  averageSunPerDay: number = 0;
  averageSunPerDayLastYear: number = 0;

  constructor(protected activatedRoute: ActivatedRoute,
              protected mastrUnitService: MastrUnitService,
              protected chargingStationService: ChargingStationService,
              protected administrationUnitService: AdministrationUnitService,
              protected alertService: AlertService,
              protected decimalPipe: DecimalPipe) {}

  ngOnInit(): void {

    this.activatedRoute.data.subscribe(({ administrationUnit }) => {
      this.administrationUnit = administrationUnit;
      this.mapOptions.geometry = administrationUnit.geometry;
      const initialCoordinate = this.mapOptions.geometry.coordinates[0][0][0];
      let initialBounds = new LngLatBounds(initialCoordinate, initialCoordinate);
      this.mapOptions.bounds = this.mapOptions.geometry.coordinates[0][0].reduce((bounds, coord) => {
        return bounds.extend([coord[0], coord[1]]);
      }, initialBounds);
      this.entries.push(new TocEntry("mastr-progress", "EE-Fortschritt", [
        new TocEntry("mastr-progress-sun", "Fotovoltaik", []),
        new TocEntry("mastr-progress-storage", "Speicher", [])
      ]));
      this.entries.push(new TocEntry("mastr-map", "Karte", []));
      this.entries.push(new TocEntry("mastr-table", "MaStR-Tabelle", []));
      this.entries.push(new TocEntry("ladesaeulen", "Ladesäulen", []));

      this.administrationUnitService.query({
        'ade.equals': 4,
        'gemeindeschluesselaufgefuellt.equals': `${administrationUnit.land}${administrationUnit.regierungsbezirk}${administrationUnit.kreis}000`
      }).subscribe(res => {
        if ( res.body !== null ) {
          this.parentAdministrationUnit = res.body[0];
        }
      })

      this.chargingStationService.query({ 'inbetriebnahmeDatum.specified': true, sort: ['inbetriebnahmeDatum,desc'], size: 1 })
      .subscribe(value => {
        this.latestChargingStation = value.body![0]
      });

      this.chargingStationService.query({
        'administrationUnitId.equals' : this.administrationUnit?.id,
        size: 1000
      }).subscribe(res => {
        if ( res.body !== null ) {
          this.chargingStations = res.body;
        }
      });

      for ( const source of this.energySources ) {
        this.mastrUnitService.query({
          'administrationUnitId.equals' : this.administrationUnit?.id,
          'energieTraegerId.equals' : source.key,
          'system_status_id' : 35,
          sort: ['bruttoLeistung,desc'],
          size : 25 }).subscribe(res => {
            if ( res.body !== null ) {
              source.units = res.body;
            }
        });
      }
    });
  }

  getAdministrationUnitName() {
    return this.administrationUnit!.nbd ? 
      this.administrationUnit!.bezeichnung + " " + this.administrationUnit!.geografischernameGen : 
      this.administrationUnit!.geografischernameGen;
  }

  updateMap() {
    this.mapOptions.options.tileUrl = `https://api.maptiler.com/maps/${this.selectedMapStyle.key}/style.json?key=${MAP_KEY}`
  }

  applyFilterGlobal(table: Table, $event: Event, stringVal: string) {
    table!.filterGlobal(($event.target as HTMLInputElement).value, stringVal);
  }

  onClickChargingStations(evt: MapLayerMouseEvent) {
    this.selectedChargingStation = evt.features![0];
  }
  onClickMastrUnit(evt: MapLayerMouseEvent) {
    this.selectedMastrUnit = evt.features![0];
  }

  getNotEmptyEnergySources() {
    return this.energySources.filter(value => value.units.length > 0);
  }

  getFirstActiveId() {
    const noneEmptySources = this.getNotEmptyEnergySources();
    if ( noneEmptySources.length > 0 ) return noneEmptySources[0].key;
    else return this.energySources[0].key;
  }

  openMastr(mastrNummer: string) {

    const url = this.mastrUnitService.queryMastrUnitPublicId(mastrNummer).subscribe({
      next: (res) => {
        window.open("https://www.marktstammdatenregister.de" + res.url, '_blank');
        return false;
      },
      error: () => {
        this.alertService.addAlert({
          type: "warning",
          message : "ID kann nicht aus dem Marktstammdatenregister abgerufen werden." })
      }
    });
  }

  getStatistic(unit: IAdministrationUnit | null, propertyKey: String): IStatistic | undefined {
    for (const stat of unit?.statistics!) {
      if (stat.key === propertyKey ) return stat;
    }
    return undefined;
  }

  getPopulationPercentage(administrationUnit: IAdministrationUnit, parentAdministrationUnit: IAdministrationUnit) {

    let value1 = this.getStatistic(administrationUnit, 'population_total')!.value;
    let value2 = this.getStatistic(parentAdministrationUnit, 'population_total')!.value;
    return value1! / value2!;
  }

  getAreaPercentage(administrationUnit: IAdministrationUnit, parentAdministrationUnit: IAdministrationUnit) {

    let value1 = this.getStatistic(administrationUnit, 'area')!.value;
    let value2 = this.getStatistic(parentAdministrationUnit, 'area')!.value;
    return value1! / value2!;
  }

  addNewSunChartData($event: any) { 
    this.sunChartData = $event; 
    this.averageSunPerDay = this.sunChartData.datasets[0].data[this.sunChartData.datasets[0].data.length - 1].y / dayjs().dayOfYear();
    this.averageSunPerDayLastYear = this.sunChartData.datasets[0].data[this.sunChartData.datasets[0].data.length - 2].y / 365;
  }
  addNewBatteryChartData($event: string) { this.batteryChartData = $event;}

  getChargingStationsInPastYear() {
    return this.chargingStations.filter(cs => dayjs(cs.inbetriebnahmeDatum)
      .isBetween(
        dayjs(this.latestChargingStation.inbetriebnahmeDatum).subtract(1, 'year'),
        dayjs(this.latestChargingStation.inbetriebnahmeDatum) 
      ));
  }

  getChargingStationsBeforeOneYearAgo() {
    return this.chargingStations.filter(cs => dayjs(cs.inbetriebnahmeDatum)
      .isBefore(
        dayjs(this.latestChargingStation.inbetriebnahmeDatum).subtract(1, 'year')
      ));
  }

  getPower(chargingStations: IChargingStation[]) {
    return chargingStations.reduce((sum, current) => sum + current.nennleistungLadeeinrichtung!, 0);
  }

  getIconProp(icon: string): IconProp {
    return icon as IconProp;
  }

  getAveragePowerPerClass(administrationUnit: IAdministrationUnit, statistic: any): string {
    const average = 
      this.getStatistic(administrationUnit, 'Solare Strahlungsenergie_sum' + statistic.key)?.value! / 
      this.getStatistic(administrationUnit, 'Solare Strahlungsenergie_count' + statistic.key)?.value!;

    return average <= 1000 ? 
      this.decimalPipe.transform(average, '1.1-1') + " kW p.A." :
      this.decimalPipe.transform(average / 1000, '1.1-1') + " MW p.A.";
  }
}