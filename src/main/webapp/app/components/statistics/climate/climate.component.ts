import {Component, OnInit} from '@angular/core';
import {DataService} from "../../../shared/service/data.service";
import {ITocEntry, TocEntry} from "../../../entities/models/toc-entry.model";
import {SOURCES, STATE_CONFIGS, StateConfig, StateName} from "../../../config/state.constants";
import moment from 'moment';
import { COPERNICUS_DATA } from 'app/config/data/copernicus/copernicus.constants';
import { CHARTJS_COLORS, CLIMATE_COLORS, COLOR_RAMPS } from 'app/config/colors.constants';
import { DWDMeasurementService } from 'app/entities/services/dwd-measurement.service';
import { Chart } from 'chart.js';
import annotationPlugin from 'chartjs-plugin-annotation';
import Gradient from 'javascript-color-gradient';
Chart.register(annotationPlugin);

@Component({
  selector: 'jhi-climate',
  templateUrl: './climate.component.html',
  styleUrls: ['./climate.component.scss']
})
export class ClimateComponent implements OnInit {

  COPERNICUS_REF_TEMP = SOURCES.COPERNICUS_REF_TEMP;
  CHARTJS_COLORS = CHARTJS_COLORS;
  CLIMATE_COLORS = CLIMATE_COLORS;
  STATE_CONFIG : StateConfig = STATE_CONFIGS[process.env.STATE as StateName || "Sachsen"];
  dwdClimateData : any = { years: [], frost: [], summer: [], heat: [], ice: [] }
  lastYearValueHeat   = 0;
  firstYearValueHeat  = 0;
  lastYearValueSummer = 0  
  firstYearValueSummer = 0
  lastYearValueFrost  = 0;
  firstYearValueFrost = 0;
  lastYearValueIce  = 0;
  firstYearValueIce = 0;
  higherThen2Sigma = 0;
  entries: ITocEntry[] = [];
  monthTempChartData: { datasets: any[]; labels: string[] } = { datasets: [], labels: []};
  monthPercipiationChartData: { datasets: any[]; labels: string[] } = { datasets: [], labels: []};
  monthSunshineDurationChartData: { datasets: any[]; labels: string[] } = { datasets: [], labels: []};
  possibleYearsAverageTemp: any[] = [];
  selectedYearAverageTemp: any[] = ["Mittelwerte 1950 - 1980", Number(moment().format('YYYY')), Number(moment().subtract(1, 'year').format('YYYY'))];
  possibleYearsAveragePercipitation: any[] = [];
  selectedYearAveragePercipitation: any[] = ["Mittelwerte 1950 - 1980", Number(moment().format('YYYY')), Number(moment().subtract(1, 'year').format('YYYY'))];
  possibleYearsAverageSunshineDuration: any[] = [];
  selectedYearAverageSunshineDuration: any[] = ["Mittelwerte 1950 - 1980", Number(moment().format('YYYY')), Number(moment().subtract(1, 'year').format('YYYY'))];
  monthTempChartOptions: any = {};
  monthPercipitationChartOptions: any = {};
  monthSunshineDurationChartOptions: any = {};
  averageTemperaturData: any = {};
  selectedYearGlobal2mAirTemperature: string[] = ["1979-2000 Mittel", "1850-1900", "1850-1900 + 1,5°C", "1850-1900 + 2°C", moment().format('YYYY'), moment().subtract(1, 'year').format('YYYY')];
  selectedYearDiffPreIndustrial: string[] = [moment().format('YYYY'), moment().subtract(1, 'year').format('YYYY')];
  possibleYearsDiffPreIndustrial: any[] = [];
  global2mAirTemperatureData: { datasets: any[]; labels: string[] } = { datasets: [], labels: [] };
  diffPreIndustrialData: { datasets: any[]; labels: string[] } = { datasets: [], labels: [] };
  global2mAirTemperatureOptions: any = {};
  diffPreIndustrialOptions: any = {};
  numberOfDaysSoFarThisYear: any = 0;
  parisData: any = { paris15Curr : 0, paris20Curr : 0, paris15Last : 0, paris20Last : 0, currentYear: "", lastYear: "" }

  constructor(private dataService: DataService, private dwdMeasurementService: DWDMeasurementService) {

    this.dwdMeasurementService.getDWDMeasurementsForState(this.STATE_CONFIG.dwdStateKey).subscribe((res) => {

      if (res.body) {

        this.dwdClimateData = { 
          years:  res.body.years.slice(-43),
          frost:  res.body.frostDaysRolling30.slice(-43),
          ice:    res.body.iceDaysRolling30.slice(-43),
          summer: res.body.summerDaysRolling30.slice(-43),
          heat:   res.body.hotDaysRolling30.slice(-43)
        }
        
        this.lastYearValueHeat    = this.dwdClimateData['heat'][0];
        this.firstYearValueHeat   = this.dwdClimateData['heat'][this.dwdClimateData['heat'].length - 1];
        this.lastYearValueSummer  = this.dwdClimateData['summer'][0];
        this.firstYearValueSummer = this.dwdClimateData['summer'][this.dwdClimateData['summer'].length - 1];
        this.lastYearValueFrost   = this.dwdClimateData['frost'][0];
        this.firstYearValueFrost  = this.dwdClimateData['frost'][this.dwdClimateData['frost'].length - 1];
        this.lastYearValueIce     = this.dwdClimateData['ice'][0];
        this.firstYearValueIce    = this.dwdClimateData['ice'][this.dwdClimateData['ice'].length - 1];

        this.averageTemperaturData          = this.dataService.getAverageTemperaturData(res.body);
        this.monthTempChartData             = this.dataService.getAverageMonthData(res.body, "temperaturMean");
        this.monthPercipiationChartData     = this.dataService.getAverageMonthData(res.body, "percipitation");
        this.monthSunshineDurationChartData = this.dataService.getAverageMonthData(res.body, "sunshineDuration");

        this.possibleYearsAverageTemp = this.monthTempChartData.datasets.map(element => ({ key: element.label, value: element.label }));
        this.possibleYearsAveragePercipitation = this.monthPercipiationChartData.datasets.map(element => ({ key: element.label, value: element.label }));
        this.possibleYearsAverageSunshineDuration = this.monthSunshineDurationChartData.datasets.map(element => ({ key: element.label, value: element.label }));

        this.monthTempChartOptions = this.global2mAirTemperatureOptions = this.monthPercipitationChartOptions = this.monthSunshineDurationChartOptions = {
          plugins : {
            legend: { position : 'top', display: false },
            datalabels: { display: false }
          },
          interaction: { intersect: false, mode: 'index' },
          fill: false,
          pointStyle: 'circle',
          pointRadius: 6,
          pointHoverRadius: 6
        };
        this.global2mAirTemperatureOptions.pointRadius = 0;
        this.diffPreIndustrialOptions = {
          plugins : {
            tooltip: {
              usePointStyle: true,
              pointStyle: 'circle',
            },
            legend: { display: false },
            datalabels: { display: false },
            annotation: {
              annotations: [{
                type: 'line',
                mode: 'horizontal',
                value: 1.5,
                scaleID: 'y',
                borderColor: CHARTJS_COLORS[0],
                borderWidth: 1,
                label: {
                  display: true,
                  content: '1,5°C Grenze'
                }
              },{
                type: 'line',
                mode: 'horizontal',
                value: 2,
                scaleID: 'y',
                borderColor: CHARTJS_COLORS[7],
                borderWidth: 2,
                label: {
                  display: true,
                  content: '2°C Grenze'
                }
              }]
            },
          },
          interaction: { intersect: false, mode: 'index' },
          fill: false,
          pointRadius: 0
        }
        
        this.handleDiffPreIndustrial();
        this.handleAverageTempYearChange();
        this.handleAveragePercipitationYearChange();
        this.handleAverageSunshineDurationYearChange();
        this.addGlobalTemperatureData();
      }
    });
  }

  ngOnInit(): void {
    this.entries.push(new TocEntry("klimadaten", "Aktuelle Daten", []));
    this.entries.push(new TocEntry("temperatur-veraenderung", "Veränderung der Temperatur", []));
    this.entries.push(new TocEntry("temperatur-veraenderung-monat", "Veränderung der Temperatur pro Monat", []));
    this.entries.push(new TocEntry("niederschlag-veraenderung-monat", "Veränderung des Niederschlags pro Monat", []));
    this.entries.push(new TocEntry("sonnenscheindauer-veraenderung-monat", "Veränderung der Sonnenscheindauer pro Monat", []));
    this.entries.push(new TocEntry("weitere-veraenderungen", "Weitere Veränderungen", [
      new TocEntry("anzahl-der-tage", "Anzahl der Frost-/Sommer- und Heißen Tage", [])
    ]));
    this.entries.push(new TocEntry("global-data", "Globale Veränderungen", [
      new TocEntry("taegliche-temperatur", "Tägliche Temperatur", []),
      new TocEntry("vergleich-paris", "Vergleich Paris", [])
    ]));
  }

  getChartOptions(): any {
    return {
      plugins : {
        legend: {
          position : 'top',
          display: false
        },
        datalabels: {
          display: false
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      radius: 0,
      fill: false
    };
  }

  getDayChangesData() : any {
    return {
      labels: this.dwdClimateData.years,
      datasets: [
        {
          label: 'Eistage (30 Jahre Mittel)',
          data: this.dwdClimateData.ice,
          backgroundColor: CLIMATE_COLORS.ICE_DAYS,
          borderColor: CLIMATE_COLORS.ICE_DAYS,
          pointBackgroundColor: CLIMATE_COLORS.ICE_DAYS,
          pointBorderColor: 'rgb(255,255,255)',
          fill: false,
          hidden: true,
        },
        {
          label: 'Frosttage (30 Jahre Mittel)',
          data: this.dwdClimateData.frost,
          backgroundColor: CLIMATE_COLORS.FROST_DAYS,
          borderColor: CLIMATE_COLORS.FROST_DAYS,
          pointBackgroundColor: CLIMATE_COLORS.FROST_DAYS,
          pointBorderColor: 'rgb(255,255,255)',
          fill: false,
          hidden: true,
        },{
          label: 'Sommertage (30 Jahre Mittel)',
          data: this.dwdClimateData.summer,
          backgroundColor: CLIMATE_COLORS.SUMMER_DAYS,
          borderColor: CLIMATE_COLORS.SUMMER_DAYS,
          pointBackgroundColor: CLIMATE_COLORS.SUMMER_DAYS,
          pointBorderColor: 'rgb(255,255,255)',
          fill: false,
          hidden: true
        },{
          label: 'Heiße Tage (30 Jahre Mittel)',
          data: this.dwdClimateData.heat,
          backgroundColor: CLIMATE_COLORS.HOT_DAYS,
          borderColor: CLIMATE_COLORS.HOT_DAYS,
          pointBackgroundColor: CLIMATE_COLORS.HOT_DAYS,
          pointBorderColor: 'rgb(255,255,255)',
          borderRadius: 8,
          fill: false
        }]
    };
  }

  getDayChangesOptions(): any {
    return {
      plugins : {
        title: { display: false },
        legend: { position : 'bottom' },
        datalabels: { display: false }
      },
      interaction: { intersect: false, mode: 'index' },
      radius: 0,
      fill: false,
    };
  }

  getMonthChartOptions() {
    return this.getChartOptions();
  }

  handleDiffPreIndustrial() {
    const temp : any[] = [];
    this.diffPreIndustrialData.datasets.forEach(dataset => {
      dataset.hidden = !this.selectedYearDiffPreIndustrial.includes(dataset.label);
      temp.push(dataset);
    })
    this.diffPreIndustrialData = { datasets : temp, labels : this.diffPreIndustrialData.labels };
  }
  handleAverageTempYearChange() {
    const temp : any[] = [];
    this.monthTempChartData.datasets.forEach(dataset => {
      dataset.hidden = !this.selectedYearAverageTemp.includes(dataset.label);
      temp.push(dataset);
    })
    this.monthTempChartData = { datasets : temp, labels : this.monthTempChartData.labels };
  }
  handleAveragePercipitationYearChange() {
    const temp : any[] = [];
    this.monthPercipiationChartData.datasets.forEach(dataset => {
      dataset.hidden = !this.selectedYearAveragePercipitation.includes(dataset.label);
      temp.push(dataset);
    })
    this.monthPercipiationChartData = { datasets : temp, labels : this.monthPercipiationChartData.labels };
  }
  handleAverageSunshineDurationYearChange() {
    const temp : any[] = [];
    this.monthSunshineDurationChartData.datasets.forEach(dataset => {
      dataset.hidden = !this.selectedYearAverageSunshineDuration.includes(dataset.label);
      temp.push(dataset);
    })
    this.monthSunshineDurationChartData = { datasets : temp, labels : this.monthSunshineDurationChartData.labels };
  }
  handleGlobal2mAirTemperatureYearChange() {

    const temp : any[] = [];
    this.global2mAirTemperatureData.datasets.forEach((dataset:any) => {
      dataset.hidden = !this.selectedYearGlobal2mAirTemperature.includes(dataset.label);
      temp.push(dataset);
    })
    this.global2mAirTemperatureData = { datasets : temp, labels : this.global2mAirTemperatureData.labels };
  }

  private addGlobalTemperatureData(): void {

    this.dataService.fetchGlobal2mData().subscribe((result) => {
      result.forEach(function(res){
        if ( res.name === "1979-2000 avg" ) res.name = "1979-2000 Mittel";
        if ( res.name === "1981-2010 avg" ) res.name = "1981-2010 Mittel";
        if ( res.name === "1991-2020 avg" ) res.name = "1991-2020 Mittel";
      })

      this.global2mAirTemperatureData.labels = this.getDates("2000/01/01", "2000/12/31");
      const allColors = new Gradient().setColorGradient(...COLOR_RAMPS[4]).setMidpoint(result.length).getColors();
      this.global2mAirTemperatureData.datasets = result.map((element, index) => ({
        data: element.data,
        label: element.name,
        backgroundColor: element.name.includes("Mittel") ? "black" : allColors[index],
        borderColor: element.name.includes("Mittel") ? "black" : allColors[index],
        pointBackgroundColor: element.name.includes("Mittel") ? "black" : allColors[index],
      }));
      this.handleGlobal2mAirTemperatureYearChange();

      // calculate the days above paris climate agreement
      const currentYearData = this.global2mAirTemperatureData.datasets[this.global2mAirTemperatureData.datasets.length - 4];
      const lastYearData    = this.global2mAirTemperatureData.datasets[this.global2mAirTemperatureData.datasets.length - 5];
      this.numberOfDaysSoFarThisYear = this.getDates(moment().format("YYYY") + "/01/01", moment().format("YYYY/MM/DD")).length;

      // calculate the difference between each year and pre industrial age
      const filteredYears = result.filter(yearData => Number.isInteger(Number(yearData.name)));
      const colors = new Gradient().setColorGradient(...COLOR_RAMPS[4]).setMidpoint(filteredYears.length).getColors();
      filteredYears.map((yearData, index) => {
        
        const data: (number|null)[] = [];
        const diffPreIndustrialData = {
          data: data,
          label: yearData.name,
          backgroundColor: colors[index],
          borderColor: colors[index],
          borderWidth: 1,
          pointBackgroundColor: colors[index],
        };
        this.diffPreIndustrialData.datasets.push(diffPreIndustrialData);
        this.diffPreIndustrialData.labels.push(yearData.name)
        this.possibleYearsDiffPreIndustrial.push({ key: yearData.name, value: yearData.name })

        for ( let index = 0; index <= 365; index++) {
          diffPreIndustrialData.data.push(
            yearData.data[index] === null ? null : yearData.data[index] - COPERNICUS_DATA.temperatures[index]!)
        }
      })
      this.diffPreIndustrialData.labels = this.getDates("2000/01/01", "2000/12/31");
      this.handleDiffPreIndustrial();

      // calculate diff if this and previous year to preindustrial levels and paris agreement
      this.parisData.currentYear = currentYearData.label;
      this.parisData.lastYear = lastYearData.label;

      for (let index = 0; index < this.numberOfDaysSoFarThisYear; index++) {
        if (COPERNICUS_DATA.temperatures[index] != null){
          if ( currentYearData.data[index] >= COPERNICUS_DATA.temperatures[index]! + 1.5 ) this.parisData.paris15Curr++;
          if ( currentYearData.data[index] >= COPERNICUS_DATA.temperatures[index]! + 2.0 ) this.parisData.paris20Curr++;
          if ( lastYearData.data[index] >= COPERNICUS_DATA.temperatures[index]! + 1.5 ) this.parisData.paris15Last++;
          if ( lastYearData.data[index] >= COPERNICUS_DATA.temperatures[index]! + 2.0 ) this.parisData.paris20Last++;
        }
      }

      this.global2mAirTemperatureData.labels.push("1850-1900");
      this.global2mAirTemperatureData.datasets.push({
        data: COPERNICUS_DATA.temperatures,
        label: "1850-1900",
        hidden: false,
        backgroundColor: CHARTJS_COLORS[CHARTJS_COLORS.length - 1],
        borderColor: CHARTJS_COLORS[CHARTJS_COLORS.length - 1],
        pointBackgroundColor: CHARTJS_COLORS[CHARTJS_COLORS.length - 1],
      });
      this.global2mAirTemperatureData.labels.push("1850-1900 + 1,5°C");
      this.global2mAirTemperatureData.datasets.push({
        data: COPERNICUS_DATA.temperatures.map(v => v + 1.5),
        label: "1850-1900 + 1,5°C",
        hidden: false,
        backgroundColor: CHARTJS_COLORS[CHARTJS_COLORS.length - 2],
        borderColor: CHARTJS_COLORS[CHARTJS_COLORS.length - 2],
        pointBackgroundColor: CHARTJS_COLORS[CHARTJS_COLORS.length - 2],
        borderDash: [6, 6],
      });
      this.global2mAirTemperatureData.labels.push("1850-1900 + 2°C");
      this.global2mAirTemperatureData.datasets.push({
        data: COPERNICUS_DATA.temperatures.map(v => v + 2),
        label: "1850-1900 + 2°C",
        hidden: false,
        backgroundColor: CHARTJS_COLORS[CHARTJS_COLORS.length - 3],
        borderColor: CHARTJS_COLORS[CHARTJS_COLORS.length - 3],
        pointBackgroundColor: CHARTJS_COLORS[CHARTJS_COLORS.length - 3],
        borderDash: [6, 6],
      });
    })
  }

  getPreIndustrialDatasets(dataset: any[]): any[] {
    return dataset.filter(x => Number.isInteger(Number(x.label)));
  }
  
  private getDates(startDate: any, endDate: any) : any {
    var dateArray = [];
    var currentDate: moment.Moment = moment(startDate);
    var stopDate: moment.Moment = moment(endDate);
    while (currentDate <= stopDate) {
      dateArray.push( moment(currentDate).format('D.M.') )
      currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
  }
}
