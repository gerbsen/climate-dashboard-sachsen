import { Component, OnInit } from '@angular/core';
import {DatePipe, DecimalPipe} from "@angular/common";
import {DataService} from "../../../shared/service/data.service";
import {Source} from "../../../entities/models/source.model";
import 'chartjs-adapter-date-fns';
import {ITocEntry, TocEntry} from "../../../entities/models/toc-entry.model";
import {STATE_CONFIGS, StateConfig, StateName} from "../../../config/state.constants";

@Component({
  selector: 'jhi-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.scss']
})
export class StorageComponent implements OnInit {

  STATE_CONFIG : StateConfig = STATE_CONFIGS[process.env.STATE as StateName || "Sachsen"];

  batterySources = [
    new Source("https://www.marktstammdatenregister.de/MaStR", "Marktstammdatenregister der Bundesnetzagentur"),
    new Source("https://battery-charts.rwth-aachen.de/", "Battery Charts - RWTH Aachen")
  ]

  storageChartOptions: any;
  chartData: any = {};
  locale: string = 'de-DE';
  entries: ITocEntry[] = [];

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.entries.push(new TocEntry("home", "Batteriespeicher", []));
    this.entries.push(new TocEntry("batteriekapazität", "Batteriekapazität", []));
    this.entries.push(new TocEntry("batteriekapazität-zubau", "Zubau an Batteriekapazität", []));
    this.entries.push(new TocEntry("batterieleistung-zubau", "Zubau an Batterieleistung", []));
    this.entries.push(new TocEntry("anzahl", "Anzahl an Batteriespeichern", []));
    this.entries.push(new TocEntry("anteil-zubau-nach-bauart", "Anteil des Zubaus nach Art des Speichers", []));
    this.entries.push(new TocEntry("durchschnittliche-batterieeigenschaften", "Durchschnittliche Batterieeigenschaften", []));
    this.entries.push(new TocEntry("geplante-batteriekapazität", "Geplante Kapazität", []));
    this.entries.push(new TocEntry("methodik", "Methodik", []));

    this.storageChartOptions = this.getChartOptions();
    for ( const key of [
        'RWTH_AACHEN_BATTERY_CAPACITY',
        'RWTH_AACHEN_BATTERY_POWER',
        'RWTH_AACHEN_BATTERY_CAPACITY_INCREASE',
        'RWTH_AACHEN_BATTERY_POWER_INCREASE',
        'RWTH_AACHEN_BATTERY_COUNT',
        // 'RWTH_AACHEN_BATTERY_TYPE',
        'RWTH_AACHEN_BATTERY_MEAN_CAPACITY',
        'RWTH_AACHEN_BATTERY_CAPACITY_PLANNED']) {
      this.chartData[key] = {};
      this.chartData[key].config = this.getMastrChartOptions(key)
      if ( key !== "RWTH_AACHEN_BATTERY_MEAN_CAPACITY" && key !== "RWTH_AACHEN_BATTERY_TYPE") {
        this.chartData[key].data = this.dataService.getRWTHAachenData(key)
      }
    }
  }

  getChartOptions(): any {
    return {
      // scales: { x: { stacked: true }, y: { stacked: true } },
      interaction: {  intersect: false, mode: 'index' },
      plugins: {
        legend: { display: true, position: 'top' },
        datalabels: {
          anchor: 'center',
          offset: 100,
          color: '#000000',
          display: true,
          font: {
            weight: 'normal'
          },
          padding: 10,
          formatter: function(value: number, context : any) {
            return value.toFixed(1) + " kW";
          }

        },
        tooltip: { position: 'nearest', callbacks: {
            label(context : any) {
              return ` ${
                new DecimalPipe('de').transform(context.raw, '1.1-1') ?? 'N/A'
              } kW`;
            }
          }}
      }
    };
  }

  getMastrChartData(rwthAachenDataKey: string) {
    return this.dataService.getRWTHAachenData(rwthAachenDataKey)
  }

  getMastrChartOptions(rwthAachenDataKey: string) {
    const unit = `${rwthAachenDataKey === 'RWTH_AACHEN_BATTERY_POWER' || rwthAachenDataKey === 'RWTH_AACHEN_BATTERY_POWER_INCREASE' ?
      "kW" : ( rwthAachenDataKey === 'RWTH_AACHEN_BATTERY_COUNT' ? "" : ( rwthAachenDataKey === 'RWTH_AACHEN_BATTERY_TYPE' ? "%" : "kWh") ) }`;

    let config = {
      interaction: {  intersect: false, mode: 'index' },
      scales: { x: { stacked: true, type: 'time', time : { displayFormats: { month: 'yyyy-MM' }} }, y: { stacked: true } },
      elements: { point:{ radius: 0 } },
      plugins: {
        legend: { display: true, position: 'top' },
        datalabels: { display: false },
        tooltip: { position: 'nearest', callbacks: {
            title(context : any) {
              return new DatePipe('de').transform(context[0].parsed.x, "d. MMMM y");
            },
            label(context : any) {
              if ( rwthAachenDataKey === "RWTH_AACHEN_BATTERY_MEAN_CAPACITY" ) {
                return `${context.dataset.label}: ${new DecimalPipe('de').transform(context.raw, '1.1-1')} ${context.dataset.label === "Speicherleistung" ? "kW" : "kWh" }`;
              }
              else if ( rwthAachenDataKey === "RWTH_AACHEN_BATTERY_TYPE" ) {
                return context.dataset.label + ": " + (context.raw * 100).toFixed(1) + "%"
              }
              else {
                return (context.dataset.label || context.label) + `: ${new DecimalPipe('de').transform(context.raw, '1.0')?.trim() ?? 'N/A'} ${unit.trim()}`;
              }
            }
          }}
      }
    }
    if ( rwthAachenDataKey === "RWTH_AACHEN_BATTERY_TYPE" ) {
      // @ts-ignore
      config.scales.y.max = 1;
    }
    if ( rwthAachenDataKey === "RWTH_AACHEN_BATTERY_MEAN_CAPACITY" ) {
      config.scales.x.stacked = false;
      config.scales.y.stacked = false;
    }
    if ( rwthAachenDataKey === "RWTH_AACHEN_BATTERY_CAPACITY_PLANNED" ) {
      // @ts-ignore
      config.scales = {};
      config.interaction = {  intersect: false, mode: 'point' },
      config.plugins.datalabels.display = true;
      // @ts-ignore
      config.plugins.datalabels.formatter = (v) => new DecimalPipe('de').transform(v, '1.1-1') + " kWh";
    }
    return config;
  }

  getRWTHAachenMeanBatteryPowerAndCapacityData() {
    return this.dataService.getRWTHAachenMeanBatteryPowerAndCapacityData();
  }

  getRWTHAachenPlannedCapacityData() {
    return this.dataService.getRWTHAachenPlannedCapacityData();
  }

  getXLargestStorageCapacityLabel(dataset: string, index: number) : any {

    const largestValues = [
      { 'key'   : "Großspeicher",
        'value' : this.chartData[dataset].data.datasets[0].data.slice(-1)[0] / 1000 },
      { 'key'   : "Gewerbespeicher",
        'value' : this.chartData[dataset].data.datasets[1].data.slice(-1)[0] / 1000},
      { 'key'   : "Heimspeicher",
        'value' : (this.chartData[dataset].data.datasets[2].data.slice(-1)[0] / 1000) }
    ].sort((a, b) => b.value - a.value);

    return largestValues[index];
  }

  getNumberOfMonthsForDoubledGrowth(data : number[]) {
    const currentValue = data[data.length - 1];
    let monthsCounter = 1;

    for ( let i = data.length - 2; i >= 0; i--) {
      if ( data[i] <= currentValue / 2 ) break;
      else monthsCounter++;
    }

    return monthsCounter;
  }

  getTotalSumOfBatteryCapacity(): number {
    return this.chartData.RWTH_AACHEN_BATTERY_CAPACITY.data.datasets[0].data.slice(-1)[0] +
      this.chartData.RWTH_AACHEN_BATTERY_CAPACITY.data.datasets[1].data.slice(-1)[0] +
      this.chartData.RWTH_AACHEN_BATTERY_CAPACITY.data.datasets[2].data.slice(-1)[0];
  }
}
