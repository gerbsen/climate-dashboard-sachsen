import { Component, OnInit } from '@angular/core';
import { CHARTJS_COLORS, COLOR_ELECTRICITY, COLOR_RAMPS } from "../../../config/colors.constants";
import {DatePipe, DecimalPipe} from "@angular/common";
import {DataService} from "../../../shared/service/data.service";
import {ITocEntry, TocEntry} from "../../../entities/models/toc-entry.model";
import {SOURCES, STATE_CONFIGS, StateConfig, StateName} from "../../../config/state.constants";
import {IAdministrationUnit} from "../../../entities/models/administration-unit.model";
import {AdministrationUnitService} from "../../../entities/services/administration-unit.service";
import {IStatistic} from "../../../entities/models/statistic.model";
import {IChargingStationsOperatorDTO} from "../../../entities/models/charging-stations-operator-dto.model";
import { ChargingStationService } from 'app/entities/services/charging-station.service';
import { AlertService } from 'app/core/util/alert.service';
import { IChargingStationsStatisticsForIntervalDTO } from 'app/entities/models/charging-stations-statistics-for-interval-dto.model';
import { IntervalFormat } from 'app/entities/models/mastr-unit-for-interval-statistics.model';
import { IChargingStation } from 'app/entities/models/charging-station.model';
import dayjs from 'dayjs/esm';

@Component({
  selector: 'jhi-emobility',
  templateUrl: './emobility.component.html',
  styleUrls: ['./emobility.component.scss']
})
export class EmobilityComponent implements OnInit {

  STATE_CONFIG : StateConfig = STATE_CONFIGS[process.env.STATE as StateName || "Sachsen"];
  
  timeUnits: { key: IntervalFormat; label: string; }[] = [{ key: "YEAR", label: "Jahr" }, { key: "MONTH", label: "Monat" }, { key: "WEEK", label: "Woche" }];
  selectedTimeUnit = this.timeUnits[0];

  bnetzaChargingPointsChartData: any;
  bnetzaChargingPointsChartOptions: any;
  bnetzaChargingPowerChartData: any;
  bnetzaChargingPowerChartOptions: any;
  timeUnitChargingPointsYearData: number[] = [];
  totalChargingPowerYearData: number[] = [];
  timeUnitChargingPowerYearData: number[] = [];
  totalChargingPointsYearData: number[] = [];
  chartData: any = {};
  COLOR_ELECTRICITY = COLOR_ELECTRICITY;
  yearLabels: number[] = [];
  administrationUnits: IAdministrationUnit[] = [];
  BNETZA_SOURCE = SOURCES.BNETZA;

  possibleColors = COLOR_RAMPS;
  chargingPowerStatistics : IStatistic[] = [
    { id: -1, key: "sum_nominal_power", name: "Nennleistung Ladeeinrichtung", unit: "kW" },
    { id: -1, key: "charging_station_count", name: "Anzahl Ladepunkte", unit: "" }
  ];

  chargingPowerMapAttribution: string = '<a href="' + this.BNETZA_SOURCE.url + '">'+ this.BNETZA_SOURCE.label +'</a> | ' + '<a href="https://gdz.bkg.bund.de/index.php/default/digitale-geodaten/verwaltungsgebiete/verwaltungsgebiete-1-2-500-000-stand-31-12-vg2500-12-31.html">Bundesamt für Kartographie und Geodäsie</a>';
  entries: ITocEntry[] = [];
  chargingStationStatisticsForInterval: IChargingStationsStatisticsForIntervalDTO | undefined;
  latestChargingStation!: IChargingStation;
  CHARGING_STATIONS_OPERATORS!: IChargingStationsOperatorDTO;
  CHARGING_STATIONS_OPERATORS_CURRENT_YEAR! : IChargingStationsOperatorDTO;

  constructor(private dataService: DataService,
              private alertService: AlertService,
              private chargingStationService: ChargingStationService,
              private administrationUnitService: AdministrationUnitService) {

    this.entries.push(new TocEntry("top", "Einleitung", []));
    this.entries.push(new TocEntry("zubau-ladepunkte", "Zubau an Ladepunkten", []));
    this.entries.push(new TocEntry("zubau-ladepunktleistung", "Leistung der Ladepunkte", []));
    this.entries.push(new TocEntry("betreibende", "Betreibende", []));
    this.entries.push(new TocEntry("kreise-und-kreisfreie-staedte", "Kreise und kreisfreie Städte", [
      new TocEntry("kreise-und-kreisfreie-staedte-ladeleistung", "Ladeleistung", []),
      new TocEntry("kreise-und-kreisfreie-staedte-ladepunkte", "Ladepunkte", [])
    ]));
    this.entries.push(new TocEntry("kraftfahrtbundesamt", "Kraftfahrtbundesamt", [
      new TocEntry("kraftfahrtbundesamt-batterieelektrische-vs-plugin-hybride", "Antriebsart", [])
    ]));
    this.entries.push(new TocEntry("methodik", "Methodik", []));
  }

  ngOnInit(): void {

    this.chargingStationService.query({ 'inbetriebnahmeDatum.specified': true, sort: ['inbetriebnahmeDatum,desc'], size: 1 })
      .subscribe(value => {
        this.latestChargingStation = value.body![0]
      });

    this.chargingStationService.getOperators().subscribe(operators => {
      this.CHARGING_STATIONS_OPERATORS = operators;
    })

    this.chargingStationService.getOperators(dayjs().subtract(1, 'year'), dayjs()).subscribe(operators => {
      this.CHARGING_STATIONS_OPERATORS_CURRENT_YEAR = operators;
    })

    // data
    this.chargingStationService.statisticsForInterval().subscribe({
      next: (res) => {
        this.chargingStationStatisticsForInterval = res;
        
        // dataset
        this.bnetzaChargingPointsChartData = this.getBNetzAChartData("ChargingPoints");
        this.timeUnitChargingPointsYearData = this.chargingStationStatisticsForInterval?.intervalStatistics['YEAR'].map((e : any) => e['timeUnitChargingPoints']);
        this.totalChargingPointsYearData = this.chargingStationStatisticsForInterval?.intervalStatistics['YEAR'].map((e : any) => e['totalChargingPoints']);
        this.bnetzaChargingPowerChartData = this.getBNetzAChartData("Power");
        this.timeUnitChargingPowerYearData = this.chargingStationStatisticsForInterval?.intervalStatistics['YEAR'].map((e : any) => e['timeUnitPower']),
        this.totalChargingPowerYearData = this.chargingStationStatisticsForInterval?.intervalStatistics['YEAR'].map((e : any) => e['totalPower']),
        // options
        this.bnetzaChargingPointsChartOptions = this.getBNetzAChartOptions("timeUnitChargingPoints");
        this.bnetzaChargingPowerChartOptions = this.getBNetzAChartOptions("timeUnitPower");
        // labels
        this.yearLabels = this.chargingStationStatisticsForInterval?.intervalStatistics['YEAR'].map((e : any) => e['timeUnit']);
      },
      error: () => {
        this.alertService.addAlert({
          type: "warning",
          message : "Die statistischen Daten für die Ladesäulen konnten nicht abgerufen werden." })
      }
    });

    for ( const key of ['MOBILITY_CHARTS_CAR_TYPE']) {
      this.chartData[key] = {};
      this.chartData[key].config = this.getMastrChartOptions(key)
      this.chartData[key].data   = this.dataService.getRWTHAachenMobilityData(key)
    }
    
    this.administrationUnitService.query({
      "land.equals" : this.STATE_CONFIG.agsId,
      "ade.equals": 4, size : 1000 }).subscribe(res => {
      if ( res.body !== null ) {
        this.administrationUnits = res.body;
      }
    });
  }

  getBNetzAChartData(variableToDisplay: "ChargingPoints" | "Power") {
    return {
      labels: this.chargingStationStatisticsForInterval?.intervalStatistics[this.selectedTimeUnit.key].map((e : any) => e.timeUnit),
      datasets: [
        {
          label: this.getChargingStationStatisticLabel('timeUnit' + variableToDisplay),
          data: this.chargingStationStatisticsForInterval?.intervalStatistics[this.selectedTimeUnit.key].map((e : any) => e['timeUnit' + variableToDisplay]),
          borderColor: COLOR_ELECTRICITY,
          backgroundColor: COLOR_ELECTRICITY
        },
        {
          label: this.getChargingStationStatisticLabel('total' + variableToDisplay),
          data: this.chargingStationStatisticsForInterval?.intervalStatistics[this.selectedTimeUnit.key].map((e : any) => e['total' + variableToDisplay]),
          borderColor: CHARTJS_COLORS[7],
          backgroundColor: CHARTJS_COLORS[7],
          hidden: true,
        }
      ]
    };
  }

  getChargingStationStatisticLabel(variableToDisplay: string): string {
    if ( variableToDisplay === "timeUnitChargingPoints" ) {
      return "Anzahl der Ladepunkte im Zeitraum";
    }
    else if ( variableToDisplay === "timeUnitPower" ) {
      return "Anzahl der Leistung im Zeitraum";
    }
    else if ( variableToDisplay ===  "totalChargingPoints" )  {
      return "Anzahl der Ladepunkte (gesamt)";
    }
    else if ( variableToDisplay === "totalPower" ) {
      return "Anzahl der Leistung (gesamt)";
    }
    else return "unbekannt";
  }

  getBNetzAChartOptions(variableToDisplay: "timeUnitChargingPoints" | "timeUnitPower" | "totalChargingPoints" | "totalPower" ): any {
    return {
      locale: "de-DE",
      plugins : {
        legend: { display: true, position: 'bottom' },
        datalabels: {
          color: '#36A2EB',
          display: false
        },
        tooltip: { position: 'nearest',
          callbacks: {
            label(context : any) {
              return `${context.dataset.label ?? ''}: ${
                new DecimalPipe('de').transform(context.raw, variableToDisplay === "timeUnitChargingPoints" ? "1.0" : "1.1-1") ?? 'N/A'
              } ${variableToDisplay === "timeUnitChargingPoints" ? "" : "kW"}`;
            }
          }
        }
      },
      parsing: {
        xAxisKey: 'timeUnit',
        yAxisKey: variableToDisplay
      },
      interaction: {
        intersect: false,
        mode: 'index',
      }
    };
  }

  getMastrChartOptions(rwthAachenDataKey: string) {
    const unit = `${rwthAachenDataKey === 'TimeUnitChargingPoints' ? "" : ( rwthAachenDataKey === 'TimeUnitPowerRating' ? "kW"
      : ( rwthAachenDataKey === 'XXX' ? "" : "") ) }`;

    let config = {
      interaction: {  intersect: false, mode: 'index' },
      scales: { x: { stacked: true, type: 'time', time : { displayFormats: { month: 'yyyy-MM' }} }, y: { stacked: true } },
      elements: { point:{ radius: 0 } },
      plugins: {
        legend: { display: true, position: 'top' },
        datalabels: { display: false },
        tooltip: { position: 'nearest', callbacks: {
            title(context : any) {
              return new DatePipe('de').transform(context[0].parsed.x, "d. MMMM y");
            },
            label(context : any) {
              return context.dataset.label + `: ${new DecimalPipe('de').transform(context.raw, '1.0')?.trim() ?? 'N/A'} ${unit.trim()}`;
            }
          }}
      }
    }
    return config;
  }
}
