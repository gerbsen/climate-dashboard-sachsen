import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../shared/service/data.service";
import {DecimalPipe} from "@angular/common";
import {ITocEntry, TocEntry} from "../../../entities/models/toc-entry.model";
import {STATE_CONFIGS, StateConfig, StateName} from "../../../config/state.constants";

@Component({
  selector: 'jhi-greenhousegas',
  templateUrl: './greenhousegas.component.html',
  styleUrls: ['./greenhousegas.component.scss']
})
export class GreenhousegasComponent implements OnInit {
  entries: ITocEntry[] = [];
  STATE_CONFIG : StateConfig = STATE_CONFIGS[process.env.STATE as StateName || "Sachsen"];

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.entries.push(new TocEntry("einfuehrung", "Einführung", []));
    this.entries.push(new TocEntry("emissionen-pro-jahr", "Emissionen pro Jahr", []));
    this.entries.push(new TocEntry("co2-in-atmosphaere", "CO<sub>2</sub>-Konzentration in Atmosphäre", []));
  }

  getChartData(): any {
    return this.dataService.getGreenHouseGasData(this.STATE_CONFIG.label as StateName);
  }

  getChartOptions(type: StateName | "atmosphere"): any {
    const options = {
      scales: { x: { stacked: true }, y: { stacked: true } },
      interaction: {  intersect: false, mode: 'index' },
      plugins: {
        legend: { display: true, position: 'top' },
        datalabels: { display: false },
        tooltip: { position: 'nearest', callbacks: {
            label(context : any) {
              return `${context.dataset.label ?? ''}: ${
                new DecimalPipe('de').transform(context.raw, '1.1-1') ?? 'N/A'
              } ${(type === 'atmosphere' ) ? 'ppm' : ' t'}`;
            }
          }}
      }
    };
    if ( type !== "atmosphere" ) {
      // @ts-ignore
      options.plugins.tooltip.callbacks.footer = (tooltipItems: any) => {
        let sum = 0;
        tooltipItems.forEach(function (tooltipItem: any) { sum += tooltipItem.parsed.y; });
        return 'Summe: ' + new DecimalPipe('de').transform(sum * 1000, '1.1-1') + " t";
      };
    }
    return options;
  }

  getCo2InAtmosphereData(): any {
    return this.dataService.getCo2InAtmosphereData();
  }

  getCo2InAtmosphereOptions(): any {
    return {
      plugins : {
        legend: {
          position : 'top'
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      }
    }
  }
}
