import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GreenhousegasComponent } from './greenhousegas.component';

describe('GreenhousegasComponent', () => {
  let component: GreenhousegasComponent;
  let fixture: ComponentFixture<GreenhousegasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GreenhousegasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GreenhousegasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
