import {Component, OnInit} from '@angular/core';
import {DataService} from "../../../shared/service/data.service";
import {DecimalPipe} from "@angular/common";
import {COLOR_BIO, COLOR_RAMPS, COLOR_STORAGE, COLOR_SUN, COLOR_WATER, COLOR_WIND} from 'app/config/colors.constants';
import {ITocEntry, TocEntry} from "../../../entities/models/toc-entry.model";
import {STATE_CONFIGS, StateConfig, StateName} from "../../../config/state.constants";
import {IAdministrationUnit} from "../../../entities/models/administration-unit.model";
import {AdministrationUnitService} from "../../../entities/services/administration-unit.service";
import {IStatistic} from "../../../entities/models/statistic.model";
import { MastrUnitService } from 'app/entities/services/mastr-unit.service';
import {IMastrUnitForIntervalStatistics } from "../../../entities/models/mastr-unit-for-interval-statistics.model";
import { IMastrUnit } from 'app/entities/models/mastr-unit.model';

@Component({
  selector: 'jhi-renewables',
  templateUrl: './renewables.component.html',
  styleUrls: ['./renewables.component.scss']
})
export class RenewablesComponent implements OnInit {

  possibleColors = COLOR_RAMPS;
  STATE_CONFIG : StateConfig = STATE_CONFIGS[process.env.STATE as StateName || "Sachsen"];
  today: Date = new Date();
  timeUnits: any[] = [];
  latestMastrUnit: IMastrUnit | undefined;
  selectedTimeUnit: "YEAR" | "MONTH" | "WEEK" = "YEAR";
  sunValue!: number;
  sunValueLast!: number;
  windValue!: number;
  windValueLast!: number;
  bioValue!: number;
  bioValueLast!: number;
  waterValue!: number;
  waterValueLast!: number;
  mostRecentYear!: number;
  lastYearGrossContribution!: number;
  administrationUnits: IAdministrationUnit[] = [];
  mastrChartData: any;
  mastrChartOptions: any;
  workChartData: any;
  workChartOptions: any;
  powerChartData: any;
  powerChartOptions: any;
  entries: ITocEntry[] = [];
  energySources: any[] = [{
    key: 'pv',
    mastrKey: 'Solare Strahlungsenergie',
    energySource: "SOLAR_POWER",
    statistic: { 'id': -1, 'value': 'Solare Strahlungsenergie', 'name': 'Solare Strahlungsenergie', 'key': 'Solare Strahlungsenergie_sum', 'unit': "MW" },
    label: 'Photovoltaik',
    labelNewUnitsChart: 'Zubau an Photovoltaik in MW',
    selectedTimeUnit: "YEAR",
    mastrChartData: {},
    interpolate: "orRd",
    datalabelBackgroundColor: "#ffffff",
    borderColor: COLOR_SUN,
    backgroundColor: COLOR_SUN
  },{
    key: 'wind',
    mastrKey: 'Wind',
    energySource: "WIND",
    statistic: { 'id': -1, 'value': 'Wind', 'name': 'Wind', 'key': 'Wind_sum', 'unit': "MW" },
    label: 'Windkraftanlagen',
    labelNewUnitsChart: 'Zubau an Windkraft in MW',
    selectedTimeUnit: "YEAR",
    mastrChartData: {},
    interpolate: "orRd",
    datalabelBackgroundColor: "#ffffff",
    borderColor: COLOR_WIND,
    backgroundColor: COLOR_WIND
  },{
    key: 'speicher',
    mastrKey: 'Speicher',
    energySource: "STORAGE",
    statistic: { 'id': -1, 'value': 'Speicher', 'name': 'Speicher', 'key': 'Speicher_sum', 'unit': "MW" },
    label: 'Speicher',
    labelNewUnitsChart: 'Zubau an Speicher in MW',
    selectedTimeUnit: "YEAR",
    mastrChartData: {},
    interpolate: "orRd",
    datalabelBackgroundColor: "#ffffff",
    borderColor: COLOR_STORAGE,
    backgroundColor: COLOR_STORAGE
  },{
    key: 'bio',
    mastrKey: 'Biomasse',
    energySource: "BIOMASS",
    statistic: { 'id': -1, 'value': 'Biomasse', 'name': 'Biomasse', 'key': 'Biomasse_sum', 'unit': "MW" },
    label: 'Biomasse',
    labelNewUnitsChart: 'Zubau an Biomasse in MW',
    selectedTimeUnit: "YEAR",
    mastrChartData: {},
    interpolate: "orRd",
    datalabelBackgroundColor: "#ffffff",
    borderColor: COLOR_BIO,
    backgroundColor: COLOR_BIO
  },{
    key: 'water',
    mastrKey: 'Wasser',
    energySource: "WATER",
    statistic: { 'id': -1, 'value': 'Wasser', 'name': 'Wasser', 'key': 'Wasser_sum', 'unit': "MW" },
    label: 'Wasserkraft',
    labelNewUnitsChart: 'Zubau an Wasserkraft in MW',
    selectedTimeUnit: "YEAR",
    mastrChartData: {},
    interpolate: "orRd",
    datalabelBackgroundColor: "#ffffff",
    borderColor: COLOR_WATER,
    backgroundColor: COLOR_WATER
  }
];

  energySourcesStatistics : IStatistic[] = this.energySources.map((energySource) => energySource.statistic);

  constructor(private dataService: DataService,
              private administrationUnitService: AdministrationUnitService,
              private mastrUnitService: MastrUnitService) {
    this.entries.push(new TocEntry("aktuelle-daten", "Aktuelle Daten", []));

    this.energySources.forEach(energySource => this.entries.push(new TocEntry(energySource.key, energySource.label, [
      new TocEntry(energySource.key + "-neue-anlagen-leistung", "Zubau pro Zeiteinheit", []),
      new TocEntry(energySource.key + "-landkreise-und-kreisfreie-staedte", "Landkreise und kreisfreie Städte", [])
    ])))

    const subs = [];
    if (this.dataService.isEnabled(this.STATE_CONFIG.label, "work"))
      subs.push(new TocEntry("beitrag-stromerzeugung", "Beitrag zur Stromerzeugung", []));
    if (this.dataService.isEnabled(this.STATE_CONFIG.label, "power"))
      subs.push(new TocEntry("installierte-leistung", "Installierte Leistung", []))

    this.entries.push(new TocEntry("historische-daten", "Historische Daten", subs));
  }

  ngOnInit(): void {

    for ( const energySource of this.energySources ) {
      this.mastrUnitService.getMastrUnitStateStatistics("ACTIVE", energySource.energySource).subscribe((result) => {
        energySource.data = result;
      });
      this.getMastrStatisticsForIntervalForState(energySource);
    }

    this.sunValue = this.dataService.getLatestValue(this.STATE_CONFIG.label, "work", "sun");
    this.sunValueLast = this.dataService.getLastValue(this.STATE_CONFIG.label, "work", "sun");;
    this.windValue = this.dataService.getLatestValue(this.STATE_CONFIG.label, "work", "wind");
    this.windValueLast = this.dataService.getLastValue(this.STATE_CONFIG.label, "work", "wind");;
    this.bioValue = this.dataService.getLatestValue(this.STATE_CONFIG.label, "work", "bio");
    this.bioValueLast = this.dataService.getLastValue(this.STATE_CONFIG.label, "work", "bio");;
    this.waterValue = this.dataService.getLatestValue(this.STATE_CONFIG.label, "work", "water");
    this.waterValueLast = this.dataService.getLastValue(this.STATE_CONFIG.label, "work", "water");;
    this.mostRecentYear = this.dataService.getMostRecentYear(this.STATE_CONFIG.label, "work");
    this.lastYearGrossContribution = this.dataService.getStateData(this.STATE_CONFIG.label).lastYearGrossContribution;
    this.timeUnits = [{ key: "YEAR", value: "Jahr" }, { key: "MONTH", value: "Monat" }, { key: "WEEK", value: "Woche" }];
    this.selectedTimeUnit = this.timeUnits[0].key;
    this.workChartData = this.getChartData("work");
    this.workChartOptions = this.getChartOptions("work");
    this.powerChartData = this.getChartData("power");
    this.powerChartOptions = this.getChartOptions("power");
    this.mastrChartOptions = this.getMastrChartOptions();
    this.administrationUnitService.query({
      "land.equals" : this.STATE_CONFIG.agsId,
      "ade.equals": 4, size : 1000 }).subscribe(res => {
      if ( res.body !== null ) {
        this.administrationUnits = res.body;
      }
    });

    this.mastrUnitService.query({ 'datumLetzteAktualisierung.specified': true, sort: ['datumLetzteAktualisierung,desc'], size: 1 })
        .subscribe(value => {
          this.latestMastrUnit = value.body![0]
        });
  }

  getMastrStatisticsForIntervalForState(energySource: any) {
    this.mastrUnitService.getMastrStatisticsForIntervalForState(energySource.selectedTimeUnit, "GROSS_POWER", energySource.energySource)
      .subscribe((statisitics) => {
        this.setNewMastrUnitsChartData(energySource, statisitics!);
      });
  }

  getSumForYear(physicalType: "work" | "power", year: number) {
    return this.dataService.getSumForYear(this.STATE_CONFIG.label, physicalType, year);
  }

  getChartOptions(physicalType: "power" | "work"): any {
    return {
      scales: { x: { stacked: true }, y: { stacked: true } },
      interaction: {  intersect: false, mode: 'index' },
      plugins: {
        legend: { display: true, position: 'top' },
        datalabels: {
          color: '#36A2EB',
          display: false
        },
        tooltip: { position: 'nearest', callbacks: {
          label(context : any) {
            return `${context.dataset.label ?? ''}: ${
              new DecimalPipe('de').transform(context.raw, '1.1-1') ?? 'N/A'
            } ${physicalType === 'work' ? 'GWh' : 'MW'}`;
          },
          footer: (tooltipItems : any) => {
            let sum = 0;
            tooltipItems.forEach(function(tooltipItem : any) { sum += tooltipItem.parsed.y; });
            return 'Summe: ' + sum.toFixed(1) + (physicalType === 'work' ? ' GWh' : ' MW');
          }
        }}
      }
    };
  }

  getChartData(physicalType: "power" | "work") {
    return {
      labels: this.dataService.getLabels(this.STATE_CONFIG.label, physicalType),
      enabled: this.dataService.isEnabled(this.STATE_CONFIG.label, physicalType),
      source: this.dataService.getStateData(this.STATE_CONFIG.label)[physicalType].source,
      datasets: [
        { label: 'Biomasse/Gas', data: this.dataService.getData(this.STATE_CONFIG.label, physicalType, "bio"), borderColor: COLOR_BIO, backgroundColor: COLOR_BIO},
        { label: 'Wasser', data: this.dataService.getData(this.STATE_CONFIG.label, physicalType, "water"), backgroundColor: COLOR_WATER },
        { label: 'Sonne', data: this.dataService.getData(this.STATE_CONFIG.label, physicalType, "sun"), backgroundColor: COLOR_SUN },
        { label: 'Wind', data: this.dataService.getData(this.STATE_CONFIG.label, physicalType, "wind"), backgroundColor: COLOR_WIND }
      ]
    };
  }

  setNewMastrUnitsChartData(energySource: any, statisitics : IMastrUnitForIntervalStatistics) {
    energySource.mastrChartData = {
      labels: statisitics.results.map(x => x.date),
      datasets: [
        {
          label: energySource.label,
          data: statisitics.results.map(entry => ({ x: entry.date, y: entry.aggregate / 1000 })),
          borderColor: energySource.borderColor,
          backgroundColor: energySource.backgroundColor,
          order: 1
        },
      ]
    }
    const delta = {
      label: "Veränderung zum Vorjahr",
      data: [{ x: 1999, y: 0}], // we nede to add this since the first/zero does not have data
      borderColor: "#c2c2c2",
      backgroundColor: "#c2c2c2",
      pointBackgroundColor: energySource.borderColor,
      type: 'line',
      order: 0,
      cubicInterpolationMode: 'monotone',
      tension: 0.4
    }
    for (let i = 1; i <= energySource.mastrChartData.datasets[0].data.length - 1; i++) {
      delta.data.push({ x: energySource.mastrChartData.labels[i], y: energySource.mastrChartData.datasets[0].data[i].y - energySource.mastrChartData.datasets[0].data[i - 1].y});
    }
    energySource.mastrChartData.datasets.push(delta)
  }

  getMastrChartOptions(): any {
    return {
      locale: "de-DE",
      plugins : {
        legend: { display: true, position: 'bottom' },
        datalabels: {
          color: '#36A2EB',
          display: false
        },
        tooltip: { position: 'nearest',
          callbacks: {
            label(context : any) {
              return `${context.dataset.label ?? ''}: ${
                new DecimalPipe('de').transform(Number(context.raw.y), '1.1-1') ?? 'N/A'
              } MW`;
            }
          }
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      }
    };
  }
}
