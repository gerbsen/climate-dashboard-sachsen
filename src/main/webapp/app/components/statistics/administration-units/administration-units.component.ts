import {Component, OnInit, ViewChild} from '@angular/core';
import {Table} from "primeng/table";
import {ITocEntry, TocEntry} from "../../../entities/models/toc-entry.model";
import {SOURCES, STATE_CONFIGS, StateConfig, StateName} from "../../../config/state.constants";
import {AdministrationUnitService} from 'app/entities/services/administration-unit.service';
import {IAdministrationUnit} from 'app/entities/models/administration-unit.model';
import {COLOR_RAMPS} from "../../../config/colors.constants";
import {IStatistic} from "../../../entities/models/statistic.model";
import {ChargingStationService} from "../../../entities/services/charging-station.service";
import {IChargingStation} from "../../../entities/models/charging-station.model";
import { SortEvent } from 'primeng/api';
import { MastrUnitService } from 'app/entities/services/mastr-unit.service';
import { IMastrUnit } from 'app/entities/models/mastr-unit.model';
import _ from "lodash";

@Component({
  selector: 'jhi-administrationUnits',
  templateUrl: './administration-units.component.html',
  styleUrls: ['./administration-units.component.scss']
})
export class AdministrationUnitsComponent implements OnInit {
  @ViewChild('sunTable') sunTable: Table | undefined;
  @ViewChild('storageTable') storageTable: Table | undefined;
  entries: ITocEntry[] = [];
  ALL_SOURCES = SOURCES;
  latestChargingStation: IChargingStation | null = null;
  STATE_CONFIG : StateConfig = STATE_CONFIGS[process.env.STATE as StateName || "Sachsen"];
  MASTR = SOURCES.MASTR;
  possibleColors = COLOR_RAMPS;
  currentDate = new Date();
  communitySizes = [
    { minInhabitants : 0, maxInhabitants : 10000, label : "unter 10000 Einwohnende"},
    { minInhabitants : 10000, maxInhabitants : 100000, label : "zwischen 10.000 und 100.000 Einwohnende"},
    { minInhabitants : 100000, maxInhabitants : 1000000000, label : "über 100.000 Einwohnende"},
    { minInhabitants : 0, maxInhabitants : 1000000000, label : "Alle Größenklassen"}];
  selectedCommunitySizeStorage: any = this.communitySizes[3];
  selectedCommunitySizeForSun: any = this.communitySizes[3];
  sortedValues: any = { sunTable: {}, storageTable: {} };
  administrationUnits: IAdministrationUnit[] = [];
  communityMapAttribution: string = '<a href="' + SOURCES.MASTR.url + '">' + SOURCES.MASTR.label +'</a> | ' +
    '<a href="https://gdz.bkg.bund.de/index.php/default/digitale-geodaten/verwaltungsgebiete/verwaltungsgebiete-1-2-500-000-stand-31-12-vg2500-12-31.html">Bundesamt für Kartographie und Geodäsie</a>';
  eMobilitycommunityMapAttribution: string = '<a href="' + SOURCES.BNETZA.url + '">'
      + SOURCES.BNETZA.label +'</a> | ' +
      '<a href="https://gdz.bkg.bund.de/index.php/default/digitale-geodaten/verwaltungsgebiete/verwaltungsgebiete-1-2-500-000-stand-31-12-vg2500-12-31.html">Bundesamt für Kartographie und Geodäsie</a>';
  
  chargingStationStatistics : IStatistic[] = [
    { id: -1, key: "charging_station_count", name: `Anzahl Ladepunkte`, unit: "" },
    { id: -1, key: "normal_charging_station_count", name: `Anzahl Normalladeeinrichtung`, unit: "" },
    { id: -1, key: "fast_charging_station_count", name: `Anzahl Schnellladeeinrichtung`, unit: "" },
    { id: -1, key: "charging_station_count_per_population", name: "Anzahl Ladepunkte / Bevölkerung", unit: "" },
    { id: -1, key: "charging_station_count_per_area", name: "Anzahl Ladepunkte / Fläche", unit: "" },
    { id: -1, key: "nominal_power_per_population", name: `Nennleistung Ladeeinrichtung / Bevölkerung`, unit: "kW pro Pers." },
    { id: -1, key: "nominal_power_per_area", name: "Nennleistung Ladeeinrichtung / Fläche", unit: "kW / km²" },
    { id: -1, key: "company_count", name: "Anzahl Firmen", unit: "" }];
  
    pvMapPropertyValues : IStatistic[] = [
      { id: -1, key: "population_total", name: "Bevölkerung (2022)", unit: "Personen" },
      { id: -1, key: "Solare Strahlungsenergie_sum", name: "Bruttoleistung", unit: "kW" },
      { id: -1, key: "Solare Strahlungsenergie per Inhabitant this year", name: `Leistung pro Person (${new Date().getFullYear()})`, unit: "kW / 1000 Personen" },
      { id: -1, key: "Solare Strahlungsenergie per Inhabitant", name: "Leistung pro Person (gesamt)", unit: "kW / Person" },
    ];
  
    batteryMapPropertyValues : IStatistic[] = [
    { id: -1, key: "Speicher per Inhabitant this year", name: `Leistung pro Person (${new Date().getFullYear()})`, unit: "kWh / 1000 Personen" },
    { id: -1, key: "Speicher_sum", name: "Bruttoleistung", unit: "kWh" },
    { id: -1, key: "Speicher per Inhabitant", name: "Leistung pro Person (gesamt)", unit: "kWh / Person" },
    { id: -1, key: "Speicher_capacity_sum", name: "Nutzbare Speicherkapazität", unit: "kWh" },
    { id: -1, key: "Speicher_capacity per Inhabitant", name: "Nutzbare Speicherkapazität / Person", unit: "kWh / Person" },
    { id: -1, key: "population_total", name: "Bevölkerung (2022)", unit: "Personen" }];
  latestMastrUnit: IMastrUnit | undefined;

  constructor(protected administrationUnitService: AdministrationUnitService, 
              protected chargingStationService: ChargingStationService,
              protected mastrUnitService: MastrUnitService) {}

  getRank(table: Table, tableName: string, adminUnit: IAdministrationUnit): number {
    return table.sortField === "name" ? null : this.sortedValues[tableName][table.sortField]
    .findIndex((c : number) => c === adminUnit.statistics?.find(s => s.key === table.sortField)?.value) + 1;
  }

  ngOnInit(): void {

    this.entries.push(new TocEntry("top", "Kommunen", [
      new TocEntry("photovoltaik", "Fotovoltaik", []),
      new TocEntry("stromspeicher", "Speicher", []),
      new TocEntry("ladesaeulen", "Ladeäsulen", [])
    ]));

    this.administrationUnitService.query({
      "land.equals": this.STATE_CONFIG.agsId,
      "ade.equals" : 6,
      sort: ['id,asc'], size : 3000 }).subscribe(res => {
        if ( res.body !== null ) {
          this.administrationUnits = res.body;
          // we have to remove all unneeded statistics here, since the statistic.find function 
          // takes extremely much time, other more complicated solutions
          // - refactor statistics in IAdministrationUnit to { 'key': IStatistic } or
          // - make it possible to give query params to the query service above (might be slow though)
          this.administrationUnits.forEach(au => {
            _.remove(au.statistics!, function(statistic : IStatistic) {
              return !["population_total", "Solare Strahlungsenergie_sum", "Solare Strahlungsenergie_sum_this_year", "Speicher_sum", 
              "Speicher_sum_this_year", "Speicher_capacity_sum", "charging_station_count", "normal_charging_station_count", 
              "fast_charging_station_count", "charging_station_count_per_population", "charging_station_count_per_area", 
              "nominal_power_per_population", "nominal_power_per_area", "company_count"].includes(statistic.key!);
            });
          })

          // calculate the rank for the table
          this.sortedValues['sunTable']["name"] = this.administrationUnits.map((unit : any) => this.getAdministrationUnitName(unit)).sort((a: any, b: any) => b! - a!);
          this.sortedValues['sunTable']["population_total"] = this.administrationUnits.map((unit : any) => this.getAdministrationUnitPopulation(unit)).sort((a: any, b: any) => b! - a!);
          this.sortedValues['sunTable']["Solare Strahlungsenergie_sum"] = this.administrationUnits.map((unit : any) => this.getAdministrationUnitSolarPower(unit)).sort((a: any, b: any) => b! - a!);
          this.sortedValues['sunTable']["Solare Strahlungsenergie per Inhabitant"] = this.administrationUnits.map((unit : any) => this.getAdministrationUnitSolarPowerPerInhabitants(unit)).sort((a: any, b: any) => b! - a!);
          this.sortedValues['sunTable']["Solare Strahlungsenergie per Inhabitant this year"] = this.administrationUnits.map((unit : any) => this.getAdministrationUnitSolarPowerPerInhabitantsThisYear(unit)).sort((a: any, b: any) => b! - a!);
          this.sortedValues['storageTable']["name"] = this.administrationUnits.map((unit : any) => this.getAdministrationUnitName(unit)).sort((a: any, b: any) => b! - a!);
          this.sortedValues['storageTable']["population_total"] = this.administrationUnits.map((unit : any) => this.getAdministrationUnitPopulation(unit)).sort((a: any, b: any) => b! - a!);
          this.sortedValues['storageTable']["Speicher_sum"] = this.administrationUnits.map((unit : any) => this.getAdministrationUnitSpeicher(unit)).sort((a: any, b: any) => b! - a!);
          this.sortedValues['storageTable']["Speicher per Inhabitant"] = this.administrationUnits.map((unit : any) => this.getAdministrationUnitSpeicherPerInhabitants(unit)).sort((a: any, b: any) => b! - a!);
          this.sortedValues['storageTable']["Speicher per Inhabitant this year"] = this.administrationUnits.map((unit : any) => this.getAdministrationUnitSpeicherPerInhabitantsThisYear(unit)).sort((a: any, b: any) => b! - a!);
        }
    });

    // get the most recent data from the database
    this.mastrUnitService.query({ 'datumLetzteAktualisierung.specified': true, sort: ['datumLetzteAktualisierung,desc'], size: 1 })
        .subscribe(value => {
          this.latestMastrUnit = value.body![0]
        });

    // get the most recent data from the database
    this.chargingStationService.query({ 'inbetriebnahmeDatum.specified': true, sort: ['inbetriebnahmeDatum,desc'], size: 1 })
        .subscribe(value => {
          this.latestChargingStation = value.body![0]
        });
  }

  getStatisticByKey(unit: IAdministrationUnit, key: string) : IStatistic | undefined {
    return unit.statistics?.find((statistic) => statistic.key == key);
  }

  filterCommunitySize(administrationUnit: IAdministrationUnit) {
    return this.getAdministrationUnitPopulation(administrationUnit)! >= this.selectedCommunitySizeForSun.minInhabitants && 
           this.getAdministrationUnitPopulation(administrationUnit)! < this.selectedCommunitySizeForSun.maxInhabitants;
  }

  applyFilterGlobal(table: Table, $event: Event, stringVal: string) {
    table!.filterGlobal(($event.target as HTMLInputElement).value, stringVal);
  }

  getAdministrationUnitName(administrationUnit: IAdministrationUnit) {
    return administrationUnit!.nbd ? 
      administrationUnit!.bezeichnung + " " + administrationUnit!.geografischernameGen : 
      administrationUnit!.geografischernameGen;
  }

  getAdministrationUnitPopulation(administrationUnit: IAdministrationUnit) {
    return administrationUnit.statistics?.find((statistic) => statistic.key === 'population_total')?.value;
  }
  getAdministrationUnitSolarPower(administrationUnit: IAdministrationUnit) {
    return administrationUnit.statistics?.find((statistic) => statistic.key === 'Solare Strahlungsenergie_sum')?.value!;
  }
  getAdministrationUnitSolarPowerPerInhabitants(administrationUnit: IAdministrationUnit) {
    const value = this.getAdministrationUnitSolarPower(administrationUnit) / (this.getAdministrationUnitPopulation(administrationUnit)!);
    administrationUnit.statistics!.push({key: "Solare Strahlungsenergie per Inhabitant", value, unit: "kW pro Einwohner"} as IStatistic);
    return value;
  }
  getAdministrationUnitSolarPowerPerInhabitantsThisYear(administrationUnit: IAdministrationUnit){
    const value = administrationUnit.statistics?.find((statistic) => statistic.key === 'Solare Strahlungsenergie_sum_this_year')?.value! / (this.getAdministrationUnitPopulation(administrationUnit)! / 1000);
    administrationUnit.statistics!.push({key: "Solare Strahlungsenergie per Inhabitant this year", unit: "kW pro 1.000 Einwohner", value} as IStatistic);
    return value;
  }
  getAdministrationUnitSpeicher(administrationUnit: IAdministrationUnit) {
    return administrationUnit.statistics?.find((statistic) => statistic.key === 'Speicher_sum')?.value!;
  }
  getAdministrationUnitSpeicherPerInhabitants(administrationUnit: IAdministrationUnit) {
    const value = this.getAdministrationUnitSpeicher(administrationUnit) / (this.getAdministrationUnitPopulation(administrationUnit)!);
    administrationUnit.statistics!.push({key: "Speicher per Inhabitant", value, unit: "kW pro Einwohner"} as IStatistic);
    return value;
  }
  getAdministrationUnitSpeicherCapacityPerInhabitants(administrationUnit: IAdministrationUnit) {
    const value = this.getAdministrationUnitSpeicher(administrationUnit) / (this.getAdministrationUnitPopulation(administrationUnit)!);
    administrationUnit.statistics!.push({key: "Speicher_capacity per Inhabitant", value, unit: "kWh pro Einwohner"} as IStatistic);
    return value;
  }
  getAdministrationUnitSpeicherPerInhabitantsThisYear(administrationUnit: IAdministrationUnit){
    const value = administrationUnit.statistics?.find((statistic) => statistic.key === 'Speicher_sum_this_year')?.value! / (this.getAdministrationUnitPopulation(administrationUnit)! / 1000);
    administrationUnit.statistics!.push({key: "Speicher per Inhabitant this year", unit: "kW pro 1.000 Einwohner", value} as IStatistic);
    return value;
  }

  customSort(event: SortEvent) {
    event.data!.sort((data1, data2) => {
      let value1 = this.getTableData(data1, event.field!);
      let value2 = this.getTableData(data2, event.field!);
      let result = null;
  
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1! < value2!) ? -1 : (value1! > value2!) ? 1 : 0;
  
      return (event.order! * result);
    });
  }

  getTableData(administrationUnit: IAdministrationUnit, key: string) {
    if ( key === "population_total" ) {
      return this.getAdministrationUnitPopulation(administrationUnit);
    }
    if ( key === "name" ) {
      return this.getAdministrationUnitName(administrationUnit);
    }
    if ( key === "Solare Strahlungsenergie_sum") {
      return this.getAdministrationUnitSolarPower(administrationUnit);
    }
    if ( key === "Solare Strahlungsenergie per Inhabitant") {
      return this.getAdministrationUnitSolarPowerPerInhabitants(administrationUnit);
    }
    if ( key === "Solare Strahlungsenergie per Inhabitant this year" ) {
      return this.getAdministrationUnitSolarPowerPerInhabitantsThisYear(administrationUnit);
    }
    if ( key === "Speicher_sum") {
      return this.getAdministrationUnitSpeicher(administrationUnit);
    }
    if ( key === "Speicher per Inhabitant") {
      return this.getAdministrationUnitSpeicherPerInhabitants(administrationUnit);
    }
    if ( key === "Speicher_capacity per Inhabitant") {
      return this.getAdministrationUnitSpeicherPerInhabitants(administrationUnit);
    }
    if ( key === "Speicher per Inhabitant this year" ) {
      return this.getAdministrationUnitSpeicherPerInhabitantsThisYear(administrationUnit);
    }
    
    return "N/A";
  }
}



