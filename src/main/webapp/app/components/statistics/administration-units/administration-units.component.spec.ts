import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrationUnitsComponent } from './administration-units.component';

describe('CommunitiesComponent', () => {
  let component: AdministrationUnitsComponent;
  let fixture: ComponentFixture<AdministrationUnitsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministrationUnitsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdministrationUnitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    // @ts-ignore
    expect(component).toBeTruthy();
  });
});
