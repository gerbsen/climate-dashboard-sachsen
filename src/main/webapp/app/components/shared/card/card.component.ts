import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'jhi-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnChanges {

  @Input() threshold = 1.5;
  @Input() valueNumberFormat = "1.1-1";
  @Input() currentValue!: number;
  @Input() previousValue!: number;
  @Input() name!: string;
  @Input() unit!: string;
  @Input() icon!: string;
  // indicates that a positive diff is a bad thing
  // e.g. more car crashes or more co2 production
  @Input() upIsBad: boolean = false;
  @Input() iconBackgroundColorClass!: string;
  @Input() backgroundColor!: string;
  @Input() comparisonText: string = "Im Vergleich zum Vorjahr";
  diff!: number;
  textColor =  "text-warning";

  ngOnInit(): void {
    this.diff = ((this.currentValue * 100) / this.previousValue) - 100;
    this.textColor = this.getTextColor();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.currentValue && this.previousValue) {
      this.ngOnInit();
    }
  }

  getTextColor() {
    if ( this.diff >= this.threshold )
      return !this.upIsBad ? "text-success" : "text-danger";
    else if ( this.diff <= -this.threshold )
      return !this.upIsBad ? "text-danger" : "text-success";
    else
      return "text-warning";
  }

  getArrowIcon() {
    if ( this.diff >= this.threshold )
      return 'circle-arrow-up';
    else if ( this.diff <= -this.threshold )
      return 'circle-arrow-down';
    else
      return 'circle-arrow-right';
  }

  isFinite(diff: number): any {
    return isFinite(diff);
  }    
}
