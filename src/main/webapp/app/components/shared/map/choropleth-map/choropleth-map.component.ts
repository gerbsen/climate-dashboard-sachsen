import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {MAP_KEY, MAP_STYLES} from 'app/config/maps.constants';
import { LngLat, MapLayerMouseEvent } from "maplibre-gl";
import {STATE_CONFIGS, StateConfig, StateName} from "../../../../config/state.constants";
import {ckmeans, equalIntervalBreaks} from 'simple-statistics'
import {pairs} from "d3";
import Gradient from "javascript-color-gradient";
import { GeoJsonProperties } from 'geojson';
import {IAdministrationUnit} from "../../../../entities/models/administration-unit.model";
import {IStatistic} from "../../../../entities/models/statistic.model";
import {SortEvent} from "primeng/api";
import {Table} from "primeng/table";

@Component({
  selector: 'jhi-choropleth-map',
  templateUrl: './choropleth-map.component.html',
  styleUrls: ['./choropleth-map.component.scss']
})
export class ChoroplethMapComponent implements OnInit, OnChanges {

  STATE_CONFIG : StateConfig = STATE_CONFIGS[process.env.STATE as StateName || "Sachsen"];
  @Input() administrationUnits: IAdministrationUnit[] = [];
  @Input() clustersCount : number = 5;
  @Input() possibleColors : string[][] = [];
  @Input() selectedColors : string[] = [];
  @Input() clusterBreaks: number[] = [];
  @Input() possibleMapPropertyValues: IStatistic[] = [];
  @Input() attribution: string = '';
  @Input() selectedClusterStatistic: IStatistic = { id: -1, value: 0, key: "key", name: "label", unit: "unit"};
  possibleClustersCount : number[] = [2,3,4,5,6,7,8,9,10];
  selectedElement: GeoJsonProperties | undefined;
  mapDivId: string = (Math.random() + 1).toString(36).substring(2);
  selectedLngLat: LngLat | undefined;
  fillColors : any ;
  cursorStyle = '';
  legend : any[] = [];
  @Input() transparency = 0.8;
  predicate: string = "value";
  ascending: boolean = true;
  @Input() showText: boolean = false;
  @Input() digitsInfo: string = '1.0-2';
  @Input() prefix : string = "PV";

  mapOptions = {
    fitBounds : {} as any,
    options : {
      zoom: this.STATE_CONFIG.mapInitOptions.zoom,
      center: [this.STATE_CONFIG.mapInitOptions.center.lat, this.STATE_CONFIG.mapInitOptions.center.lng] as number[],
      attribution : this.attribution,
      tileUrl : `https://api.maptiler.com/maps/${MAP_STYLES[0].key}/style.json?key=${MAP_KEY}`
    },
  }

  ngOnInit(): void {
    for ( let unit of this.administrationUnits ) {
      unit.properties = { 
        name : unit.nbd ? unit.bezeichnung + " " + unit.geografischernameGen : unit.geografischernameGen,
        ags: unit.gemeindeschluesselaufgefuellt,
        isLandkreis: unit.ade === 4
      };
      for ( let prop of unit.statistics!){
        unit.properties[prop.key!] = prop.value!;
        unit.properties[prop.key + "Unit"] = prop.unit!;
      }
    }
  }

  getStatisticByKey(unit: IAdministrationUnit, key: string) : IStatistic | undefined {
    return unit.statistics?.find((statistic) => statistic.key == key);
  }

  getSumOfSelectedValue() : number {
    return this.administrationUnits.reduce(
        (accumulator, currentValue) => {
          const value = this.getStatisticByKey(currentValue, this.selectedClusterStatistic.key + "")?.value! as number;
          return accumulator + (Number.isNaN(value) || !Number.isFinite(value) ||  value === undefined ? 0 : value);
        }
        , 0
    );
  }

  setFillColors(property: IStatistic, hexColors : string[], clusters: number) : void {
    // thanks to https://observablehq.com/@visionscarto/natural-breaks
    const roundBreaks = (numbers : number[], k: number) =>
      pairs(ckmeans(numbers, k)).map(([low, hi]) => {
        const p = 10 ** Math.floor(1 - Math.log10(hi.at(0)! - low.at(-1)!));
        return Math.floor(((hi.at(0)! + low.at(-1)!) / 2) * p) / p;
      })

    const statValues = this.administrationUnits.map((unit) => {
      const ret = Number(this.getStatisticByKey(unit, this.selectedClusterStatistic.key!)?.value);
      return isNaN(ret) ? 0 : ret;
    });
    this.clusterBreaks = [...new Set(roundBreaks(statValues, clusters).sort(function(a, b){return a-b}))];
    this.clusterBreaks.splice(0, 0, 0);
    this.legend = [];

    const colors = new Gradient().setColorGradient(...hexColors).setMidpoint(clusters).getColors();
    const fillColors : any = [ 'step', ['get', property.key]]

    for ( let i = 0 ; i < this.clusterBreaks.length ; i++) {
      if ( i !== 0 ) fillColors.push(this.clusterBreaks[i]);
      fillColors.push(colors[i]);
      this.legend.push({ stop: this.clusterBreaks[i], color: colors[i], unit: this.selectedClusterStatistic.unit});
    }
    this.fillColors = ["case", ["==", ["get", property.key], null], "#ffffff", fillColors];
  }

  public compareFn(a : string[], b: string[]): boolean {
    return a == b;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.setFillColors(this.selectedClusterStatistic, this.selectedColors, this.clustersCount);
  }

  onClick(evt: MapLayerMouseEvent) {
    this.selectedLngLat = evt.lngLat;
    this.selectedElement = evt.features?.[0].properties as GeoJsonProperties;
  }
  customSort(event: SortEvent) {
    event.data!.sort((data1: IAdministrationUnit, data2: IAdministrationUnit) => {

      let value1 = undefined, value2 = undefined;
      if (event.field == 'name'){
        value1 = data1.nbd ? data1.bezeichnung + " " + data1.geografischernameGen : data1.geografischernameGen;
        value2 = data2.nbd ? data2.bezeichnung + " " + data2.geografischernameGen : data2.geografischernameGen;
      }
      if (event.field == 'value'){
        value1 = this.getStatisticByKey(data1, this.selectedClusterStatistic.value! + "")?.value!;
        value2 = this.getStatisticByKey(data2, this.selectedClusterStatistic.value! + "")?.value!;
      }

      let result = null;

      if (value1 == null && value2 != null) result = -1;
      else if (value1 != null && value2 == null) result = 1;
      else if (value1 == null && value2 == null) result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string') result = value1.localeCompare(value2);
      else result = value1! < value2! ? -1 : value1! > value2! ? 1 : 0;

      return event?.order! * result;
    });
  }

  applyFilterGlobal(table: Table, $event: Event, stringVal: string) {
    table!.filterGlobal(($event.target as HTMLInputElement).value, stringVal);
  }
}
