import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ISource} from "../../../../entities/models/source.model";
import {ChartType} from "chart.js";
import {
  AggregateFunction, EnergySource, IMastrUnitForIntervalStatistics, IntervalFormat,
  MastrUnitProperty
} from "../../../../entities/models/mastr-unit-for-interval-statistics.model";
import {MastrUnitService} from "../../../../entities/services/mastr-unit.service";
import {IAdministrationUnit} from "../../../../entities/models/administration-unit.model";
import {CHARTJS_COLORS} from "../../../../config/colors.constants";
import {SOURCES} from "../../../../config/state.constants";
import {IMastrUnit} from "../../../../entities/models/mastr-unit.model";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'jhi-statistics-for-timeinterval',
  templateUrl: './statistics-for-timeinterval.component.html',
  styleUrls: ['./statistics-for-timeinterval.component.scss']
})
export class StatisticsForTimeintervalComponent implements OnInit {

  @Input() property: MastrUnitProperty = "GROSS_POWER";
  @Input() aggregateFunction: AggregateFunction = "SUM";
  @Input() intervalFormat: IntervalFormat = "YEAR";
  @Input() energySource: EnergySource = "SOLAR_POWER";
  @Input() administrationUnit: IAdministrationUnit | null = null;
  @Input() chartType: ChartType = "bar";
  @Input() height: number = 100;
  @Input() sources: ISource[] = [SOURCES.MASTR];
  @Input() unit: string | undefined;
  @Input() id: string | undefined;
  timeUnits = [{ key: "YEAR", label: "Jahr" }, { key: "MONTH", label: "Monat" }, { key: "WEEK", label: "Woche" }];
  selectedTimeUnit: any = this.timeUnits[0];
  chartData: any;
  chartConfig: any;
  intervalStatistics: IMastrUnitForIntervalStatistics | null = null;
  latestMastrUnit: IMastrUnit | null = null;
  divId: string = (Math.random() + 1).toString(36).substring(2);
  @Output() newChartDataEvent = new EventEmitter<string>();

  addNewChartDataEvent(value: any) {
    this.newChartDataEvent.emit(value);
  }

  constructor(protected mastrUnitService: MastrUnitService,
              protected translateService: TranslateService) { }

  ngOnInit(): void {
    this.mastrUnitService.getMastrStatisticsForIntervalForCommunity(
        this.administrationUnit?.gemeindeschluesselaufgefuellt!, this.intervalFormat, this.property, this.energySource, this.aggregateFunction)
        .subscribe(value => {
          this.intervalStatistics = value;
          this.chartData = {
            labels: this.intervalStatistics?.results.map(entry => entry.date),
            datasets: [
              {
                label: this.translateService.instant("energytransitiondashboardApp.mastrUnit.property." + this.property),
                data: this.intervalStatistics?.results.map(entry => ({ x: entry.date, y: entry.aggregate })),
                borderColor: CHARTJS_COLORS[0],
                backgroundColor: CHARTJS_COLORS[0],
                pointBackgroundColor: CHARTJS_COLORS[0],
                pointRadius: 0,
                hidden: true
              },
              {
                label: "Kommulative Summe",
                data: this.getCommulativeSum(),
                borderColor: CHARTJS_COLORS[1],
                backgroundColor: CHARTJS_COLORS[1],
                pointBackgroundColor: CHARTJS_COLORS[1],
                pointRadius: 0
              },
              {
                label: "Veränderung zum Vorjahr",
                style: 'bar',
                data: this.getDeltaToPreviousYear(),
                borderColor: CHARTJS_COLORS[2],
                backgroundColor: CHARTJS_COLORS[2],
                pointBackgroundColor: CHARTJS_COLORS[2],
                order: 0,
                cubicInterpolationMode: 'monotone',
                tension: 0.4,
                pointRadius: 0,
                hidden: true
              }
            ]
          };
          this.chartConfig = {
            interaction: {
              intersect: false,
              mode: 'nearest'
            },
          };
          this.newChartDataEvent.emit(this.chartData);
        })

    this.mastrUnitService.query({ 'datumLetzteAktualisierung.specified': true, sort: ['datumLetzteAktualisierung,desc'], size: 1 })
        .subscribe(value => {
          this.latestMastrUnit = value.body![0]
        });
  }

  getDeltaToPreviousYear() : any[] {

    const delta = [];
    for (let i = 1; i <= this.intervalStatistics?.results.length! - 1; i++) {
      delta.push({ x: this.intervalStatistics?.results[i].date, y: this.intervalStatistics?.results[i].aggregate! - this.intervalStatistics?.results[i - 1].aggregate!});
    }
    return delta;
  }

  getCommulativeSum() : any[]{

    const cumulativeSum: any[] = [];
    this.intervalStatistics?.results.reduce((sum, entry) => {
      sum = sum + entry.aggregate;
      cumulativeSum.push({ x: entry.date, y: sum});
      return sum
    }, 0);

    return cumulativeSum;
  }

  update() {
    this.intervalFormat = this.selectedTimeUnit.key;
    this.ngOnInit();
  }
}
