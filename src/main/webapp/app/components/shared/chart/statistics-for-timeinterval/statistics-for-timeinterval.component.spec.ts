import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticsForTimeintervalComponent } from './statistics-for-timeinterval.component';

describe('StatisticsForTimeintervalComponent', () => {
  let component: StatisticsForTimeintervalComponent;
  let fixture: ComponentFixture<StatisticsForTimeintervalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatisticsForTimeintervalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StatisticsForTimeintervalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
