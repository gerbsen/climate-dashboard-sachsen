import {Component, Input} from '@angular/core';
import {ITocEntry} from "../../entities/models/toc-entry.model";

@Component({
  selector: 'jhi-on-this-page',
  templateUrl: './on-this-page.component.html',
  styleUrls: ['./on-this-page.component.scss']
})
export class OnThisPageComponent {
  @Input() entries: ITocEntry[] = [];
}
