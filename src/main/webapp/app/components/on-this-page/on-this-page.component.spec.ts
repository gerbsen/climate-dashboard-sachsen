import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnThisPageComponent } from './on-this-page.component';

describe('OnThisPageComponent', () => {
  let component: OnThisPageComponent;
  let fixture: ComponentFixture<OnThisPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnThisPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OnThisPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
