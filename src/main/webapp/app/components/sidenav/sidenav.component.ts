import {Component, OnInit } from '@angular/core';
import {navbarData} from './sidenav-data';
import {STATE_CONFIGS, StateConfig, StateName} from "../../config/state.constants";

@Component({
  selector: 'jhi-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  navData = navbarData;
  STATE_CONFIG : StateConfig = STATE_CONFIGS[process.env.STATE as StateName || "Sachsen"];

  ngOnInit(): void {
  }
}
