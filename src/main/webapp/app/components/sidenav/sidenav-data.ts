import {faHome} from "@fortawesome/free-solid-svg-icons";
import {RouteLink} from "../../config/state.constants";

export const navbarData = [
  {
    routeLink: 'home' as RouteLink,
    icon: faHome,
    label: 'Home'
  },
  {
    routeLink: 'batteriespeicher' as RouteLink,
    icon: 'battery-three-quarters',
    label: 'Batteriespeicher'
  },
  {
    routeLink: 'e-mobilität' as RouteLink,
    icon: 'car',
    label: 'E-Mobilität'
  },
  {
    routeLink: 'erneuerbare-energien' as RouteLink,
    icon: 'solar-panel',
    label: 'Erneuerbare'
  },
  {
    routeLink: 'klima' as RouteLink,
    icon: 'temperature-arrow-up',
    label: 'Klima'
  },
  {
    routeLink: 'kommunen' as RouteLink,
    icon: 'tree-city',
    label: 'Kommunen'
  },
  {
    routeLink: 'treibhausgase' as RouteLink,
    icon: 'fire',
    label: 'Treibhausgase'
  },
  {
    routeLink: 'waldzustand' as RouteLink,
    icon: 'tree',
    label: 'Waldzustand'
  },
  {
    routeLink: 'wasserhaushalt' as RouteLink,
    icon: 'water',
    label: 'Wasserhaushalt'
  },
  // {
  //   routeLink: 'einstellungen',
  //   icon: faGear,
  //   label: 'Einstellungen'
  // }
]
