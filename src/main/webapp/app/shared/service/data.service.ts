import { Injectable } from '@angular/core';
import { ENERGY_DATA } from "../../config/data/energy_data";
import {GREEN_HOUSE_GAS_DATA, GREEN_HOUSE_GAS_IN_ATMOSPHERE} from "../../config/data/greenhousegas.constants";
import {RWTH_AACHEN_BATTERY_CAPACITY} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_capacity";
import {RWTH_AACHEN_BATTERY_CAPACITY_INCREASE} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_capacity_increase";
import {RWTH_AACHEN_BATTERY_POWER} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_power";
import {RWTH_AACHEN_BATTERY_POWER_INCREASE} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_power_increase";
import {RWTH_AACHEN_BATTERY_COUNT} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_count";
import {RWTH_AACHEN_BATTERY_MEAN_CAPACITY} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_mean_capacity";
import {RWTH_AACHEN_BATTERY_CAPACITY_PLANNED} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_planned";
import {MOBILITY_CHARTS_CAR_TYPE} from "../../../app/config/data/rwth_aachen/rwth_mobility_car_type";
import {CHARTJS_COLORS} from "../../config/colors.constants";
import {WALDZUSTANDSBERICHT_DATA} from "../../config/data/smekul/waldzustandsbericht.constants";
import {StateName} from "../../config/state.constants";
import chroma from "chroma-js";
import _ from "lodash";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import { IDWDMeasurementDTO } from 'app/entities/models/dwd-measurement-dto';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  RWTH_AACHEN_BATTERY_POWER = RWTH_AACHEN_BATTERY_POWER;
  RWTH_AACHEN_BATTERY_CAPACITY = RWTH_AACHEN_BATTERY_CAPACITY;
  RWTH_AACHEN_BATTERY_CAPACITY_INCREASE = RWTH_AACHEN_BATTERY_CAPACITY_INCREASE;
  RWTH_AACHEN_BATTERY_POWER_INCREASE = RWTH_AACHEN_BATTERY_POWER_INCREASE;
  RWTH_AACHEN_BATTERY_COUNT = RWTH_AACHEN_BATTERY_COUNT;
  RWTH_AACHEN_BATTERY_MEAN_CAPACITY = RWTH_AACHEN_BATTERY_MEAN_CAPACITY;
  RWTH_AACHEN_BATTERY_CAPACITY_PLANNED = RWTH_AACHEN_BATTERY_CAPACITY_PLANNED;
  MOBILITY_CHARTS_CAR_TYPE = MOBILITY_CHARTS_CAR_TYPE;

  constructor(
    private http: HttpClient
  ) {}

  getLabels(state: StateName, physicalType: "work" | "power"){
    return ENERGY_DATA[state][physicalType]['years'];
  }

  getStateData(state: StateName){
    return ENERGY_DATA[state];
  }

  isEnabled(state: StateName, physicalType: "work" | "power"){
    return ENERGY_DATA[state][physicalType].enabled;
  }

  getData(state: StateName, physicalType: "work" | "power", eeType: "sun" | "wind" | "bio" | "water") {
    return ENERGY_DATA[state][physicalType][eeType];
  }

  getLatestValue(state: StateName, physicalType: "work" | "power", eeType: "sun" | "wind" | "bio" | "water") {
    return ENERGY_DATA[state][physicalType][eeType][ENERGY_DATA[state][physicalType][eeType].length - 1];
  }

  getLastValue(state: StateName, physicalType: "work" | "power", eeType: "sun" | "wind" | "bio" | "water") {
    return ENERGY_DATA[state][physicalType][eeType][ENERGY_DATA[state][physicalType][eeType].length - 2];
  }

  getMostRecentYear(state: StateName, physicalType: "work" | "power") {
    return ENERGY_DATA[state][physicalType]['years'][ENERGY_DATA[state][physicalType]['years'].length - 1];
  }

  getSumForYear(state: StateName, physicalType: "work" | "power", year: number) {
    const index = ENERGY_DATA[state][physicalType]['years'].findIndex((element : number) => element === year);
    return ENERGY_DATA[state][physicalType]['sun'][index] +
      ENERGY_DATA[state][physicalType]['water'][index] +
      ENERGY_DATA[state][physicalType]['bio'][index] +
      ENERGY_DATA[state][physicalType]['wind'][index];
  }

  getGreenHouseGasData(state: StateName) {
    return GREEN_HOUSE_GAS_DATA[state];
  }

  getWaldzustandsBerichtData(treeType: "alleBaumarten") {
    return {
      source: WALDZUSTANDSBERICHT_DATA.source,
      labels: WALDZUSTANDSBERICHT_DATA.labels,
      datasets: WALDZUSTANDSBERICHT_DATA.datasets[treeType]
    }
  }

  getCo2InAtmosphereData() {
    return GREEN_HOUSE_GAS_IN_ATMOSPHERE;
  }

  getAverageTemperaturData(data: IDWDMeasurementDTO) {
    return {
      labels: data.years,
      datasets: [
        {
          label: 'Jahresdurchschnittstemperatur',
          data: data.airTemperatureMean,
          backgroundColor: 'rgba(255, 99, 132, 1)',
          fill: false,
          borderColor: 'rgba(255, 99, 132, 1)'
        },{
          label: 'Durchschnittstemperatur (30 Jahre Mittel)',
            data: data.airTemperatureMeanRolling30,
            backgroundColor: 'rgba(25, 99, 132, 1)',
            borderColor: 'rgba(25, 99, 132, 1)',
            fill: false
        }]
      };
  }

  getRWTHAachenMobilityData(rwthAachenDataKey: string): any {
    return {
      labels: (this as any)[rwthAachenDataKey].results.PHEV.frames[0].data.values[0],
      datasets: [
        {
          label: 'Batterieelektrisch',
          data: (this as any)[rwthAachenDataKey].results.BEV.frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[0]
        },
        {
          label: 'Plugin-Hybrid',
          data: (this as any)[rwthAachenDataKey].results.PHEV.frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[1]
        }
      ]
    };
  }

  getRWTHAachenData(rwthAachenDataKey: string): any {
    return {
      labels: (this as any)[rwthAachenDataKey].results.Gewerbespeicher.frames[0].data.values[0],
      datasets: [
        {
          label: 'Großspeicher',
          data: (this as any)[rwthAachenDataKey].results['Großspeicher'].frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[0]
        },
        {
          label: 'Gewerbespeicher',
          data: (this as any)[rwthAachenDataKey].results.Gewerbespeicher.frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[1]
        },
        {
          label: 'Heimspeicher',
          data: (this as any)[rwthAachenDataKey].results.Heimspeicher.frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[2]
        }
      ]
    };
  }

  getRWTHAachenMeanBatteryPowerAndCapacityData() {
    return {
      labels: RWTH_AACHEN_BATTERY_MEAN_CAPACITY.results['Speicherkapazität'].frames[0].data.values[0],
      datasets: [
        {
          label: 'Speicherleistung',
          data: RWTH_AACHEN_BATTERY_MEAN_CAPACITY.results['Speicherkapazität'].frames[0].data.values[1],
        },
        {
          label: 'Speicherkapazität',
          data: RWTH_AACHEN_BATTERY_MEAN_CAPACITY.results['Speicherkapazität'].frames[0].data.values[2],
        }
      ]
    }
  }

  getRWTHAachenPlannedCapacityData() {
    return {
      labels: ["Großspeicher", "Gewerbespeicher", "Heimspeicher"],
      datasets: [
        {
          data: [
            RWTH_AACHEN_BATTERY_CAPACITY_PLANNED.results['Großspeicher'].frames[0].data.values[1][0],
            RWTH_AACHEN_BATTERY_CAPACITY_PLANNED.results.Gewerbespeicher.frames[0].data.values[1][0],
            RWTH_AACHEN_BATTERY_CAPACITY_PLANNED.results.Heimspeicher.frames[0].data.values[1][0]
          ],
          backgroundColor: [
            CHARTJS_COLORS[0],
            CHARTJS_COLORS[1],
            CHARTJS_COLORS[2]
          ],
        },
      ]
    }
  }


  getAverageMonthData(data: IDWDMeasurementDTO, dataSource :  "temperaturMean" | "percipitation" | "sunshineDuration") {

    let source = undefined;
    if ( dataSource == "temperaturMean" ) source = data.regionalAverageAirTemperatureMean;
    else if ( dataSource == "percipitation" ) source = data.regionalAveragePercipitation;
    else source = data.regionalAverageSunshineDuration;

    const average1950To1980 = {
      "Januar" : [] as number[], "Februar": [] as number[],  "März": [] as number[],
      "April": [] as number[],   "Mai": [] as number[],      "Juni": [] as number[],
      "Juli" : [] as number[],   "August": [] as number[],   "September": [] as number[],
      "Oktober": [] as number[], "November": [] as number[], "Dezember": [] as number[]
    };
    const average1980To2010 = {
      "Januar" : [] as number[], "Februar": [] as number[],  "März": [] as number[],
      "April": [] as number[],   "Mai": [] as number[],      "Juni": [] as number[],
      "Juli" : [] as number[],   "August": [] as number[],   "September": [] as number[],
      "Oktober": [] as number[], "November": [] as number[], "Dezember": [] as number[]
    };

    const color = chroma.scale(['blue', 'red']).colors(Object.entries(source).length);
    const datasets : any [] = [];

    for (const [index, [key, element]] of Object.entries(Object.entries(source))) {

      // @ts-ignore
      let aColor = chroma(color[index]).rgb();
      const dataset = {
        label: element.year,
        data: [element.january, element.february, element.march, element.april, element.may, element.june,
          element.july, element.august, element.september, element.october, element.november, element.december],
        backgroundColor: `rgba(${aColor[0]}, ${aColor[1]}, ${aColor[2]}, 1)`,
        borderColor: `rgba(${aColor[0]}, ${aColor[1]}, ${aColor[2]}, 1)`,
        pointBackgroundColor: `rgba(${aColor[0]}, ${aColor[1]}, ${aColor[2]}, 1)`,
        hidden: true,
        fill: false,
        tension: 0.2,
        pointStyle: 'circle',
        pointRadius: 6,
        pointHoverRadius: 8
      }

      if ( element.year >= 1950 && element.year <= 1980 ) {
        average1950To1980.Januar.push(element.january!);
        average1950To1980.Februar.push(element.february!);
        average1950To1980.März.push(element.march!);
        average1950To1980.April.push(element.april!);
        average1950To1980.Mai.push(element.may!);
        average1950To1980.Juni.push(element.june!);
        average1950To1980.Juli.push(element.july!);
        average1950To1980.August.push(element.august!);
        average1950To1980.September.push(element.september!);
        average1950To1980.Oktober.push(element.october!);
        average1950To1980.November.push(element.november!);
        average1950To1980.Dezember.push(element.december!);
      }
      if ( element.year >= 1980 && element.year <= 2010 ) {
        average1980To2010.Januar.push(element.january!);
        average1980To2010.Februar.push(element.february!);
        average1980To2010.März.push(element.march!);
        average1980To2010.April.push(element.april!);
        average1980To2010.Mai.push(element.may!);
        average1980To2010.Juni.push(element.june!);
        average1980To2010.Juli.push(element.july!);
        average1980To2010.August.push(element.august!);
        average1980To2010.September.push(element.september!);
        average1980To2010.Oktober.push(element.october!);
        average1980To2010.November.push(element.november!);
        average1980To2010.Dezember.push(element.december!);
      }

      datasets.push(dataset)
    }

    datasets.push({
      label: "Mittelwerte 1950 - 1980",
      data: [_.mean(average1950To1980.Januar), _.mean(average1950To1980.Februar), _.mean(average1950To1980.März),
        _.mean(average1950To1980.April), _.mean(average1950To1980.Mai), _.mean(average1950To1980.Juni),
        _.mean(average1950To1980.Juli), _.mean(average1950To1980.August), _.mean(average1950To1980.September),
        _.mean(average1950To1980.Oktober), _.mean(average1950To1980.November), _.mean(average1950To1980.Dezember)],
      backgroundColor: `green`,
      borderColor: `green`,
      pointBackgroundColor: `green`,
      fill: false,
      tension: 0.2,
      pointStyle: 'circle',
      pointRadius: 6,
      pointHoverRadius: 8
    })
    datasets.push({
      label: "Mittelwerte 1980 - 2010",
      data: [_.mean(average1980To2010.Januar), _.mean(average1980To2010.Februar), _.mean(average1980To2010.März),
        _.mean(average1980To2010.April), _.mean(average1980To2010.Mai), _.mean(average1980To2010.Juni),
        _.mean(average1980To2010.Juli), _.mean(average1980To2010.August), _.mean(average1980To2010.September),
        _.mean(average1980To2010.Oktober), _.mean(average1980To2010.November), _.mean(average1980To2010.Dezember)],
      backgroundColor: `black`,
      borderColor: `black`,
      pointBackgroundColor: `black`,
      fill: false,
      tension: 0.2,
      pointStyle: 'circle',
      pointRadius: 6,
      pointHoverRadius: 8
    })

    return {
      labels: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
      datasets: datasets
    };
  }

  fetchGlobal2mData(): Observable<any[]> {
    return this.http.get<any[]>("https://climatereanalyzer.org/clim/t2_daily/json/era5_world_t2_day.json");
  }
}
