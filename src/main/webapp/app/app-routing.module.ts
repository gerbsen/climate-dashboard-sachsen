import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';
import { Authority } from 'app/config/authority.constants';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import {HomeComponent} from "./components/home/home.component";
import {RenewablesComponent} from "./components/statistics/renewables/renewables.component";
import {GreenhousegasComponent} from "./components/statistics/greenhousegas/greenhousegas.component";
import {ClimateComponent} from "./components/statistics/climate/climate.component";
import { StorageComponent } from './components/statistics/storage/storage.component';
import {EmobilityComponent} from "./components/statistics/emobility/emobility.component";
import {WaterComponent} from "./components/statistics/water/water.component";
import {ForestConditionComponent} from "./components/statistics/forest-condition/forest-condition.component";
import { AdministrationUnitsComponent } from './components/statistics/administration-units/administration-units.component';
import {
  AdministrationUnitByAgsRoutingResolveService
} from "./components/statistics/administration-unit/resolver/administration-unit-by-ags-routing-resolve.service";
import {AdministrationUnitComponent} from "./components/statistics/administration-unit/administration-unit.component";

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          data: {
            authorities: [Authority.ADMIN],
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule),
        },
        {
          path: 'account',
          loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
        },
        {
          path: 'login',
          loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
        },
        // TODO
        { path: 'home', component: HomeComponent },
        { path: 'erneuerbare-energien', component: RenewablesComponent },
        { path: 'treibhausgase', component: GreenhousegasComponent },
        { path: 'klima', component: ClimateComponent },
        { path: 'batteriespeicher', component: StorageComponent },
        { path: 'e-mobilität', component: EmobilityComponent },
        { path: 'wasserhaushalt', component: WaterComponent },
        { path: 'waldzustand', component: ForestConditionComponent },
        { path: 'kommunen', component: AdministrationUnitsComponent },
        { path: 'kommunen/:gemeindeschluesselaufgefuellt/:name',
          component: AdministrationUnitComponent,
          resolve: {
              administrationUnit: AdministrationUnitByAgsRoutingResolveService,
          },
        },
        { path: '', redirectTo: 'home', pathMatch: 'full' },

        // {
        //   path: '',
        //   loadChildren: () => import(`./entities/entity-routing.module`).then(m => m.EntityRoutingModule),
        // },
        //
        // navbarRoute,
        // ...errorRoute,
      ],
      {
        enableTracing: false, // DEBUG_INFO_ENABLED,
        scrollPositionRestoration: 'enabled',
        anchorScrolling: 'enabled',
        scrollOffset: [0, 75] // [x, y]
      }
    ),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
