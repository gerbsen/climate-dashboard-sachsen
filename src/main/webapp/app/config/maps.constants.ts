export interface MapStyle {
  key: "hybrid" | "basic-v2" | "bright-v2" | "topo-v2";
  label: string;
}

export const MAP_KEY: string = 'ggn8oCY37Ax1DKly8JOI';
export const MAP_STYLE_HYBRID : MapStyle = { key : 'hybrid',    label : 'Satellite'};
export const MAP_STYLE_BASIC : MapStyle  = { key : 'basic-v2',  label : 'Einfach'};
export const MAP_STYLE_BRIGHT : MapStyle = { key : 'bright-v2', label : 'Hell'};
export const MAP_STYLE_TOPO : MapStyle   = { key : 'topo-v2',   label : 'Topografie'};
export const MAP_STYLES : MapStyle[]     = [MAP_STYLE_BASIC, MAP_STYLE_BRIGHT, MAP_STYLE_HYBRID, MAP_STYLE_TOPO]


