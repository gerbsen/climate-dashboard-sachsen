import {IAdministrationUnit} from "../../../entities/models/administration-unit.model";
import dayjs from "dayjs/esm";
import { IStatistic } from "app/entities/models/statistic.model";
// population from https://statistik.sachsen-anhalt.de/themen/bevoelkerung-mikrozensus-freiwillige-haushaltserhebungen/bevoelkerung/tabellen-bevoelkerungsstand
export const BURGENLANDKREIS : IAdministrationUnit =  { gemeindeschluesselaufgefuellt: "15084000", id: -1, statistics: [] };
export const BOERDE : IAdministrationUnit=  { gemeindeschluesselaufgefuellt: "15083000", id: -1, statistics: [] };
export const WITTENBERG : IAdministrationUnit=  { gemeindeschluesselaufgefuellt: "15091000", id: -1, statistics: [] };
export const HARZ : IAdministrationUnit=  { gemeindeschluesselaufgefuellt: "15085000", id: -1, statistics: [] };
export const ALTMARKKREIS_SALZWEDEL : IAdministrationUnit=  { gemeindeschluesselaufgefuellt: "15081000", id: -1, statistics: [] };
export const JERICHOWER_LAND : IAdministrationUnit=  { gemeindeschluesselaufgefuellt: "15086000", id: -1, statistics: [] };
export const MANSFELD_SUEDHARZ : IAdministrationUnit=  { gemeindeschluesselaufgefuellt: "15087000", id: -1, statistics: [] };
export const SAALEKREIS : IAdministrationUnit=  { gemeindeschluesselaufgefuellt: "15088000", id: -1, statistics: [] };
export const STENDAL : IAdministrationUnit=  { gemeindeschluesselaufgefuellt: "15090000", id: -1, statistics: [] };
export const ANHALT_BITTERFELD : IAdministrationUnit=  { gemeindeschluesselaufgefuellt: "15082000", id: -1, statistics: [] };
export const HALLE_SAALE : IAdministrationUnit=  { gemeindeschluesselaufgefuellt: "15002000", id: -1, statistics: [] };
export const SALZLANDKREIS : IAdministrationUnit=  { gemeindeschluesselaufgefuellt: "15089000", id: -1, statistics: [] };
export const DESSAU_ROSSLAU : IAdministrationUnit=  { gemeindeschluesselaufgefuellt: "15001000", id: -1, statistics: [] };
export const MAGDEBURG : IAdministrationUnit =  { gemeindeschluesselaufgefuellt: "15003000", id: -1, statistics: [] };

export const DISTRICTS = [BURGENLANDKREIS, BOERDE, WITTENBERG, HARZ, ALTMARKKREIS_SALZWEDEL, JERICHOWER_LAND,
  MANSFELD_SUEDHARZ, SAALEKREIS, STENDAL, ANHALT_BITTERFELD, HALLE_SAALE, SALZLANDKREIS, DESSAU_ROSSLAU, MAGDEBURG];

// data generation import needle
