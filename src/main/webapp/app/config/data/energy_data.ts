import {Source} from "../../entities/models/source.model";

export const ENERGY_DATA = {
  "Sachsen" : {
    lastYearGrossContribution: 0.228,
    work: {
      enabled: true,
      years: [2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019],
      wind: [740.25, 815.77, 1133.30, 1155.46, 1261.25, 1548.00, 1457.21, 1362.78, 1335.72, 1653.37, 1716.14, 1559.01, 1576.84, 1939.24, 1695.70, 2200.68, 1999.08, 2249.33],
      sun: [1.58, 2.66, 6.57, 15.76, 43.96, 58.51, 111.18, 196.87, 336.63, 645.26, 955.15, 1180.30, 1417.72, 1562.82, 1463.32, 1509.35, 1782.14, 1876],
      water: [277.18, 171.18, 268.30, 299.46, 236.98, 323.92, 261.03, 274.53, 324.62, 268.77, 239.23, 314.84, 194.74, 236.50, 266.12, 283.58, 192.67, 227.99],
      bio: [125.69, 223.93, 387.32, 511.45, 657.89, 921.04, 1074.18, 1175.04, 1234.61, 1397.65, 1489.16, 1676.00, 1850.14, 1960.71, 1934.58, 1923.33, 1884.84, 1831.27],
      source: new Source(
        "https://www.energie.sachsen.de/zahlen-und-fakten-3971.html",
        "Sächsisches Staatsministerium für Energie, Klimaschutz, Umwelt und Landwirtschaft"
      )
    },
    power : {
      enabled: true,
      years: [2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019],
      wind : [531.3, 658.8, 706.5, 747.8, 788.9, 826.4, 844.97, 912.84, 962.7, 988.8, 1030.94, 1056.95, 1101.86, 1153.59, 1183.13, 1203.55, 1212.02, 1265.48],
      sun : [3, 4, 14.29, 27.22, 49.9, 92.65, 166.75, 289.85, 509.86, 764.33, 1178.89, 1381.76, 1451.96, 1512.77, 1635.61, 1714.18, 1803.03, 2012.94],
      water : [77.01, 77.99, 72.94, 79.75, 79.84, 82.17, 82.17, 84.25, 87.99, 88.75, 94.62, 93.38, 92.8, 89.28, 92.05, 93.34, 94.28, 94.54],
      bio : [36.61, 73.15, 96.15, 92.61, 142.37, 177.69, 192.4, 213.68, 233.25, 236.53, 259.89, 280.65, 299.37, 301.83, 305.15, 289.35, 299.81, 308.95],
      source: new Source(
        "https://www.energie.sachsen.de/zahlen-und-fakten-3971.html",
        "Sächsisches Staatsministerium für Energie, Klimaschutz, Umwelt und Landwirtschaft"
      )
    }
  },
  "Sachsen-Anhalt" : {
    lastYearGrossContribution: 15078 / 25730,
    work: {
      enabled: true,
      years: [1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019],
      wind: [0, 0, 0, 3, 2, 8, 24, 62, 132, 240, 636, 881, 1458, 2113, 2285, 2372, 2710, 4425, 5063, 4818, 4846, 5834, 6238, 5992, 6115, 7784, 6970, 8964, 8350, 9378],
      sun: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 4, 12, 19, 32, 60, 110, 245, 529, 950, 1311, 1608, 1817, 1880, 1970, 2419, 2493],
      water: [0, 7, 8, 11, 18, 25, 26, 39, 42, 48, 61, 62, 80, 60, 67, 69, 68, 120, 80, 77, 94, 85, 87, 97, 105, 102, 104, 108, 90, 88],
      bio: [0, 0, 0, 0, 0, 0, 1, 4, 13, 14, 44, 72, 122, 159, 284, 922, 1317, 1658, 1977, 2035, 2090, 2502, 2624, 2842, 3102, 3175, 3223, 3189, 2950, 3061],
      source: new Source(
        "https://statistik.sachsen-anhalt.de/themen/wirtschaftsbereiche/energie-und-wasserversorgung/tabellen-stromerzeugung-insgesamt",
        "Statistisches Landesamt Sachsen-Anhalt"
      ),
    },
    power : {
      enabled: false,
      years: [],
      wind : [],
      sun : [],
      water : [],
      bio : [],
      source: new Source(
        "",
        ""
      ),
    }
  },
  "Bayern" : { 
    lastYearGrossContribution: 40.2 / 68.5, // 2022
    work: {
      enabled: true,
      years: [1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021],
      wind: [75, 92, 115, 135, 131, 169, 232, 239, 354, 524, 547, 557, 601, 789, 1123, 1348, 1803, 2784, 3235, 4624, 4601, 4995, 4878, 4074],
      sun: [6, 20, 34, 48, 58, 122, 225, 560, 962, 1283, 1808, 2555, 4451, 7101, 8530, 9043, 10382, 11026, 10765, 11247, 11755, 12064, 12968, 13166],
      water: [12325, 13863, 14144, 14176, 15880, 11965, 12495, 11779, 12031, 12837, 12577, 11987, 12531, 10747, 13112, 13143, 11260, 11206, 12140, 12160, 10640, 11925, 11129, 11420],
      bio: [865, 926, 972, 834, 776, 1451, 1864, 2397, 3362, 4337, 4845, 5657, 5954, 6519, 7334, 7781, 8105, 8704, 8726, 8946, 9173, 9200, 9400, 9225],
      source: new Source(
        "https://www.stmwi.bayern.de/energie/energiedaten/",
        "Bayerisches Staatsministerium für Wirtschaft, Landesentwicklung und Energie"
      )
    },
    power : {
      enabled: false,
      years: [],
      wind : [],
      sun : [],
      water : [],
      bio : [],
      source: null,
    }
  }
};
