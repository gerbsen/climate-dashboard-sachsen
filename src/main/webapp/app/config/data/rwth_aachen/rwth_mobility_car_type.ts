export const MOBILITY_CHARTS_CAR_TYPE = {
  "results": {
    "BEV": {
      "frames": [
        {
          "schema": {
            "refId": "BEV",
            "meta": {
              "executedQueryString": "SELECT\n  time AS \"time\",\n  correct_number/1000 AS \"batterieelektrische PKW\"\nFROM fz2\nWHERE\n  Ort = 'Sachsen' AND\n  Typ = \"batterieelektrische PKW\"\nORDER BY time"
            },
            "fields": [
              {
                "name": "Time",
                "type": "time",
                "typeInfo": {
                  "frame": "time.Time",
                  "nullable": true
                }
              },
              {
                "name": "batterieelektrische PKW",
                "type": "number",
                "typeInfo": {
                  "frame": "float64",
                  "nullable": true
                }
              }
            ]
          },
          "data": {
            "values": [
              [
                1514764800000,
                1546300800000,
                1577836800000,
                1609459200000,
                1640995200000,
                1672531200000
              ],
              [
                1.501,
                2.43,
                3.438,
                7.978,
                15.589,
                23.8
              ]
            ]
          }
        }
      ]
    },
    "PHEV": {
      "frames": [
        {
          "schema": {
            "refId": "PHEV",
            "meta": {
              "executedQueryString": "SELECT\n  time AS \"time\",\n  correct_number/1000 AS \"Plug-in-Hybride\"\nFROM fz2\nWHERE\n  Ort = 'Sachsen' AND\n  Typ = 'Plug-in-Hybrid'\nORDER BY time"
            },
            "fields": [
              {
                "name": "Time",
                "type": "time",
                "typeInfo": {
                  "frame": "time.Time",
                  "nullable": true
                }
              },
              {
                "name": "Plug-in-Hybride",
                "type": "number",
                "typeInfo": {
                  "frame": "float64",
                  "nullable": true
                }
              }
            ]
          },
          "data": {
            "values": [
              [
                1514764800000,
                1546300800000,
                1577836800000,
                1609459200000,
                1640995200000,
                1672531200000
              ],
              [
                1.174,
                1.64,
                2.52,
                7.163,
                15.464,
                23.959
              ]
            ]
          }
        }
      ]
    }
  }
};