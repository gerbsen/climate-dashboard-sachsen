export const RWTH_AACHEN_BATTERY_COUNT = {
  "results": {
    "Gewerbespeicher": {
      "frames": [
        {
          "schema": {
            "refId": "Gewerbespeicher",
            "meta": {
              "executedQueryString": "SELECT\n  time AS \"time\",\n  anzahl AS \"Gewerbespeicher\"\nFROM speicher\nWHERE\n  bundesland = 'Sachsen' AND\n  speicherart = \"Gewerbespeicher\" AND\n  batterietyp = 'Alle Batterietechnologien'\nORDER BY time"
            },
            "fields": [
              {
                "name": "Time",
                "type": "time",
                "typeInfo": {
                  "frame": "time.Time",
                  "nullable": true
                }
              },
              {
                "name": "Gewerbespeicher",
                "type": "number",
                "typeInfo": {
                  "frame": "float64",
                  "nullable": true
                }
              }
            ]
          },
          "data": {
            "values": [
              [
                1554076800000,
                1556668800000,
                1559347200000,
                1561939200000,
                1564617600000,
                1567296000000,
                1569888000000,
                1572566400000,
                1575158400000,
                1577836800000,
                1580515200000,
                1583020800000,
                1585699200000,
                1588291200000,
                1590969600000,
                1593561600000,
                1596240000000,
                1598918400000,
                1601510400000,
                1604188800000,
                1606780800000,
                1609459200000,
                1612137600000,
                1614556800000,
                1617235200000,
                1619827200000,
                1622505600000,
                1625097600000,
                1627776000000,
                1630454400000,
                1633046400000,
                1635724800000,
                1638316800000,
                1640995200000,
                1643673600000,
                1646092800000,
                1648771200000,
                1651363200000,
                1654041600000,
                1656633600000,
                1659312000000,
                1661990400000,
                1664582400000,
                1667260800000,
                1669852800000,
                1672531200000,
                1675209600000,
                1677628800000,
                1680307200000,
                1682899200000,
                1685577600000,
                1688169600000,
                1690848000000,
                1693526400000,
                1696118400000,
                1698796800000,
                1701388800000,
                1704067200000,
                1706745600000,
                1709251200000,
                1709683200000
              ],
              [
                34,
                34,
                36,
                36,
                38,
                40,
                43,
                43,
                43,
                47,
                47,
                51,
                52,
                55,
                59,
                64,
                65,
                67,
                71,
                73,
                75,
                77,
                80,
                89,
                93,
                94,
                96,
                100,
                105,
                110,
                112,
                116,
                118,
                124,
                127,
                127,
                130,
                135,
                139,
                146,
                156,
                163,
                169,
                175,
                183,
                187,
                198,
                206,
                213,
                222,
                232,
                243,
                255,
                272,
                287,
                308,
                322,
                331,
                342,
                349,
                349
              ]
            ]
          }
        }
      ]
    },
    "Großspeicher": {
      "frames": [
        {
          "schema": {
            "refId": "Großspeicher",
            "meta": {
              "executedQueryString": "SELECT\n  time AS \"time\",\n  anzahl AS \"Großspeicher\"\nFROM speicher\nWHERE\n  bundesland = 'Sachsen' AND\n  speicherart = \"Großspeicher\" AND\n  batterietyp = 'Alle Batterietechnologien'\nORDER BY time"
            },
            "fields": [
              {
                "name": "Time",
                "type": "time",
                "typeInfo": {
                  "frame": "time.Time",
                  "nullable": true
                }
              },
              {
                "name": "Großspeicher",
                "type": "number",
                "typeInfo": {
                  "frame": "float64",
                  "nullable": true
                }
              }
            ]
          },
          "data": {
            "values": [
              [
                1554076800000,
                1556668800000,
                1559347200000,
                1561939200000,
                1564617600000,
                1567296000000,
                1569888000000,
                1572566400000,
                1575158400000,
                1577836800000,
                1580515200000,
                1583020800000,
                1585699200000,
                1588291200000,
                1590969600000,
                1593561600000,
                1596240000000,
                1598918400000,
                1601510400000,
                1604188800000,
                1606780800000,
                1609459200000,
                1612137600000,
                1614556800000,
                1617235200000,
                1619827200000,
                1622505600000,
                1625097600000,
                1627776000000,
                1630454400000,
                1633046400000,
                1635724800000,
                1638316800000,
                1640995200000,
                1643673600000,
                1646092800000,
                1648771200000,
                1651363200000,
                1654041600000,
                1656633600000,
                1659312000000,
                1661990400000,
                1664582400000,
                1667260800000,
                1669852800000,
                1672531200000,
                1675209600000,
                1677628800000,
                1680307200000,
                1682899200000,
                1685577600000,
                1688169600000,
                1690848000000,
                1693526400000,
                1696118400000,
                1698796800000,
                1701388800000,
                1704067200000,
                1706745600000,
                1709251200000,
                1709683200000
              ],
              [
                11,
                11,
                11,
                11,
                12,
                12,
                12,
                12,
                12,
                12,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                13,
                14,
                14,
                15,
                15,
                15,
                15,
                16,
                16,
                17,
                17,
                17,
                18,
                18,
                18,
                18,
                18,
                18,
                18,
                18,
                18,
                19,
                19,
                21,
                21
              ]
            ]
          }
        }
      ]
    },
    "Heimspeicher": {
      "frames": [
        {
          "schema": {
            "refId": "Heimspeicher",
            "meta": {
              "executedQueryString": "SELECT\n  time AS \"time\",\n  anzahl AS \"Heimspeicher\"\nFROM speicher\nWHERE\n  bundesland = 'Sachsen' AND\n  speicherart = \"Heimspeicher\" AND\n  batterietyp = 'Alle Batterietechnologien'\nORDER BY time"
            },
            "fields": [
              {
                "name": "Time",
                "type": "time",
                "typeInfo": {
                  "frame": "time.Time",
                  "nullable": true
                }
              },
              {
                "name": "Heimspeicher",
                "type": "number",
                "typeInfo": {
                  "frame": "float64",
                  "nullable": true
                }
              }
            ]
          },
          "data": {
            "values": [
              [
                1554076800000,
                1556668800000,
                1559347200000,
                1561939200000,
                1564617600000,
                1567296000000,
                1569888000000,
                1572566400000,
                1575158400000,
                1577836800000,
                1580515200000,
                1583020800000,
                1585699200000,
                1588291200000,
                1590969600000,
                1593561600000,
                1596240000000,
                1598918400000,
                1601510400000,
                1604188800000,
                1606780800000,
                1609459200000,
                1612137600000,
                1614556800000,
                1617235200000,
                1619827200000,
                1622505600000,
                1625097600000,
                1627776000000,
                1630454400000,
                1633046400000,
                1635724800000,
                1638316800000,
                1640995200000,
                1643673600000,
                1646092800000,
                1648771200000,
                1651363200000,
                1654041600000,
                1656633600000,
                1659312000000,
                1661990400000,
                1664582400000,
                1667260800000,
                1669852800000,
                1672531200000,
                1675209600000,
                1677628800000,
                1680307200000,
                1682899200000,
                1685577600000,
                1688169600000,
                1690848000000,
                1693526400000,
                1696118400000,
                1698796800000,
                1701388800000,
                1704067200000,
                1706745600000,
                1709251200000,
                1709683200000
              ],
              [
                2883,
                3007,
                3157,
                3285,
                3436,
                3575,
                3720,
                3892,
                4058,
                4166,
                4368,
                4527,
                4733,
                4954,
                5200,
                5455,
                5805,
                6084,
                6459,
                6819,
                7190,
                7500,
                7728,
                7924,
                8181,
                8444,
                8734,
                9155,
                9590,
                9991,
                10422,
                10908,
                11380,
                11776,
                12135,
                12616,
                13218,
                13774,
                14442,
                15136,
                15758,
                16618,
                17534,
                18303,
                19077,
                19566,
                20919,
                22117,
                23865,
                25349,
                27123,
                29189,
                31018,
                32947,
                34797,
                36499,
                38199,
                39393,
                40550,
                41312,
                41366
              ]
            ]
          }
        }
      ]
    }
  }
};