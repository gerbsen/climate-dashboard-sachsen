export const RWTH_AACHEN_BATTERY_CAPACITY_PLANNED = {
  "results": {
    "Gewerbespeicher": {
      "frames": [
        {
          "schema": {
            "refId": "Gewerbespeicher",
            "meta": {
              "executedQueryString": "SELECT\n  time AS \"time\",\n  batteriekapazitaet AS \"Gewerbespeicher\"\nFROM zukunft\nWHERE\n  batterietyp = 'Alle Batterietechnologien' AND\n  bundesland = 'Sachsen' AND\n  speicherart = 'Gewerbespeicher'\nORDER BY time"
            },
            "fields": [
              {
                "name": "Time",
                "type": "time",
                "typeInfo": {
                  "frame": "time.Time",
                  "nullable": true
                }
              },
              {
                "name": "Gewerbespeicher",
                "type": "number",
                "typeInfo": {
                  "frame": "float64",
                  "nullable": true
                }
              }
            ]
          },
          "data": {
            "values": [
              [
                1709251200000
              ],
              [
                2156.82
              ]
            ]
          }
        }
      ]
    },
    "Großspeicher": {
      "frames": [
        {
          "schema": {
            "refId": "Großspeicher",
            "meta": {
              "executedQueryString": "SELECT\n  time AS \"time\",\n  batteriekapazitaet AS \"Großspeicher\"\nFROM zukunft\nWHERE\n  batterietyp = 'Alle Batterietechnologien' AND\n  bundesland = 'Sachsen' AND\n  speicherart = 'Großspeicher'\nORDER BY time"
            },
            "fields": [
              {
                "name": "Time",
                "type": "time",
                "typeInfo": {
                  "frame": "time.Time",
                  "nullable": true
                }
              },
              {
                "name": "Großspeicher",
                "type": "number",
                "typeInfo": {
                  "frame": "float64",
                  "nullable": true
                }
              }
            ]
          },
          "data": {
            "values": [
              [
                1709251200000
              ],
              [
                21610
              ]
            ]
          }
        }
      ]
    },
    "Heimspeicher": {
      "frames": [
        {
          "schema": {
            "refId": "Heimspeicher",
            "meta": {
              "executedQueryString": "SELECT\n  time AS \"time\",\n  batteriekapazitaet AS \"Heimspeicher\"\nFROM zukunft\nWHERE\n  batterietyp = 'Alle Batterietechnologien' AND\n  bundesland = 'Sachsen' AND\n  speicherart = 'Heimspeicher'\nORDER BY time"
            },
            "fields": [
              {
                "name": "Time",
                "type": "time",
                "typeInfo": {
                  "frame": "time.Time",
                  "nullable": true
                }
              },
              {
                "name": "Heimspeicher",
                "type": "number",
                "typeInfo": {
                  "frame": "float64",
                  "nullable": true
                }
              }
            ]
          },
          "data": {
            "values": [
              [
                1709251200000
              ],
              [
                5703.16
              ]
            ]
          }
        }
      ]
    }
  }
};