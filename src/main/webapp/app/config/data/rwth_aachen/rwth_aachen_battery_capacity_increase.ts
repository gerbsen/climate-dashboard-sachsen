export const RWTH_AACHEN_BATTERY_CAPACITY_INCREASE = {
  "results": {
    "Gewerbespeicher": {
      "frames": [
        {
          "schema": {
            "refId": "Gewerbespeicher",
            "meta": {
              "executedQueryString": "SELECT\n  time AS \"time\",\n  batteriekapazitaet AS \"Gewerbespeicher\"\nFROM wachstum\nWHERE\n  bundesland = 'Sachsen' AND\n  batterietyp = 'Alle Batterietechnologien' AND\n  speicherart = 'Gewerbespeicher' AND\n  batteriekapazitaet >= 0\nORDER BY time"
            },
            "fields": [
              {
                "name": "Time",
                "type": "time",
                "typeInfo": {
                  "frame": "time.Time",
                  "nullable": true
                }
              },
              {
                "name": "Gewerbespeicher",
                "type": "number",
                "typeInfo": {
                  "frame": "float64",
                  "nullable": true
                }
              }
            ]
          },
          "data": {
            "values": [
              [
                1553990400000,
                1556582400000,
                1559260800000,
                1561852800000,
                1564531200000,
                1567209600000,
                1569801600000,
                1572480000000,
                1575072000000,
                1577750400000,
                1580428800000,
                1582934400000,
                1585612800000,
                1588204800000,
                1590883200000,
                1593475200000,
                1596153600000,
                1598832000000,
                1601424000000,
                1604102400000,
                1606694400000,
                1609372800000,
                1612051200000,
                1614470400000,
                1617148800000,
                1619740800000,
                1622419200000,
                1625011200000,
                1627689600000,
                1630368000000,
                1632960000000,
                1635638400000,
                1638230400000,
                1640908800000,
                1643587200000,
                1646006400000,
                1648684800000,
                1651276800000,
                1653955200000,
                1656547200000,
                1659225600000,
                1661904000000,
                1664496000000,
                1667174400000,
                1669766400000,
                1672444800000,
                1675123200000,
                1677542400000,
                1680220800000,
                1682812800000,
                1685491200000,
                1688083200000,
                1690761600000,
                1693440000000,
                1696032000000,
                1698710400000,
                1701302400000,
                1703980800000,
                1706659200000,
                1709164800000
              ],
              [
                75.1,
                0,
                81,
                0,
                75,
                280.5,
                119.52,
                0,
                0,
                247.6,
                0,
                154.4,
                30.6,
                109.4,
                294.18,
                416.6,
                34.2,
                172,
                249.5,
                71,
                79.26,
                74.1,
                226.96,
                371.93,
                204.6,
                33.6,
                69.72,
                204.1,
                286.92,
                308.5,
                86.4,
                159.2,
                258.72,
                955.82,
                959,
                0,
                134.78,
                321.88,
                217.7,
                541.8,
                999.3,
                470.9,
                321.3,
                439.5,
                462.8,
                132.8,
                1001.9,
                659.9,
                435.1,
                481,
                542.9,
                804.5,
                568.2,
                1146.8,
                979.6,
                1344,
                1033,
                839.5,
                916,
                455
              ]
            ]
          }
        }
      ]
    },
    "Großspeicher": {
      "frames": [
        {
          "schema": {
            "refId": "Großspeicher",
            "meta": {
              "executedQueryString": "SELECT\n  time AS \"time\",\n  batteriekapazitaet AS \"Großspeicher\"\nFROM wachstum\nWHERE\n  bundesland = 'Sachsen' AND\n  batterietyp = 'Alle Batterietechnologien' AND\n  speicherart = 'Großspeicher' AND\n  batteriekapazitaet >= 0\nORDER BY time"
            },
            "fields": [
              {
                "name": "Time",
                "type": "time",
                "typeInfo": {
                  "frame": "time.Time",
                  "nullable": true
                }
              },
              {
                "name": "Großspeicher",
                "type": "number",
                "typeInfo": {
                  "frame": "float64",
                  "nullable": true
                }
              }
            ]
          },
          "data": {
            "values": [
              [
                1553990400000,
                1556582400000,
                1559260800000,
                1561852800000,
                1564531200000,
                1567209600000,
                1569801600000,
                1572480000000,
                1575072000000,
                1577750400000,
                1580428800000,
                1582934400000,
                1585612800000,
                1588204800000,
                1590883200000,
                1593475200000,
                1596153600000,
                1598832000000,
                1601424000000,
                1604102400000,
                1606694400000,
                1609372800000,
                1612051200000,
                1614470400000,
                1617148800000,
                1619740800000,
                1622419200000,
                1625011200000,
                1627689600000,
                1630368000000,
                1632960000000,
                1635638400000,
                1638230400000,
                1640908800000,
                1643587200000,
                1646006400000,
                1648684800000,
                1651276800000,
                1653955200000,
                1656547200000,
                1659225600000,
                1661904000000,
                1664496000000,
                1667174400000,
                1669766400000,
                1672444800000,
                1675123200000,
                1677542400000,
                1680220800000,
                1682812800000,
                1685491200000,
                1688083200000,
                1690761600000,
                1693440000000,
                1696032000000,
                1698710400000,
                1701302400000,
                1703980800000,
                1706659200000,
                1709164800000
              ],
              [
                0,
                0,
                0,
                0,
                25500,
                0,
                0,
                0,
                0,
                0,
                1600,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                3680,
                0,
                3840,
                0,
                0,
                0,
                16028,
                0,
                6000,
                0,
                0,
                11100,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                2500,
                0,
                25060
              ]
            ]
          }
        }
      ]
    },
    "Heimspeicher": {
      "frames": [
        {
          "schema": {
            "refId": "Heimspeicher",
            "meta": {
              "executedQueryString": "SELECT\n  time AS \"time\",\n  batteriekapazitaet AS \"Heimspeicher\"\nFROM wachstum\nWHERE\n  bundesland = 'Sachsen' AND\n  batterietyp = 'Alle Batterietechnologien' AND\n  speicherart = 'Heimspeicher' AND\n  batteriekapazitaet >= 0\nORDER BY time"
            },
            "fields": [
              {
                "name": "Time",
                "type": "time",
                "typeInfo": {
                  "frame": "time.Time",
                  "nullable": true
                }
              },
              {
                "name": "Heimspeicher",
                "type": "number",
                "typeInfo": {
                  "frame": "float64",
                  "nullable": true
                }
              }
            ]
          },
          "data": {
            "values": [
              [
                1553990400000,
                1556582400000,
                1559260800000,
                1561852800000,
                1564531200000,
                1567209600000,
                1569801600000,
                1572480000000,
                1575072000000,
                1577750400000,
                1580428800000,
                1582934400000,
                1585612800000,
                1588204800000,
                1590883200000,
                1593475200000,
                1596153600000,
                1598832000000,
                1601424000000,
                1604102400000,
                1606694400000,
                1609372800000,
                1612051200000,
                1614470400000,
                1617148800000,
                1619740800000,
                1622419200000,
                1625011200000,
                1627689600000,
                1630368000000,
                1632960000000,
                1635638400000,
                1638230400000,
                1640908800000,
                1643587200000,
                1646006400000,
                1648684800000,
                1651276800000,
                1653955200000,
                1656547200000,
                1659225600000,
                1661904000000,
                1664496000000,
                1667174400000,
                1669766400000,
                1672444800000,
                1675123200000,
                1677542400000,
                1680220800000,
                1682812800000,
                1685491200000,
                1688083200000,
                1690761600000,
                1693440000000,
                1696032000000,
                1698710400000,
                1701302400000,
                1703980800000,
                1706659200000,
                1709164800000
              ],
              [
                660.8,
                971,
                1132.3,
                991.7,
                1199.4,
                1147.8,
                1115.2,
                1395.8,
                1392,
                979.6,
                1682.9,
                1373.4,
                1737.6,
                2035.9,
                2170.1,
                2275.6,
                3140.8,
                2360.4,
                3335.2,
                3340.1,
                3027.1,
                2651.2,
                1866.5,
                1782.1,
                2225.1,
                2264.6,
                2508.2,
                3748.4,
                3919.1,
                3598.4,
                4000.5,
                4186.6,
                4046.8,
                3594,
                3132.3,
                4054,
                5621,
                5250,
                6056,
                6379,
                5848,
                7997,
                8659,
                7303,
                7201,
                4677,
                13021,
                11302,
                16781,
                13730,
                16701,
                19591,
                17043,
                18145,
                17578,
                16165,
                16311,
                11535,
                10993,
                6939
              ]
            ]
          }
        }
      ]
    }
  }
};