import {IAdministrationUnit} from "../../../entities/models/administration-unit.model";
import dayjs from "dayjs/esm";
import { IStatistic } from "app/entities/models/statistic.model";
export const BAUTZEN: IAdministrationUnit = {  id: -1, gemeindeschluesselaufgefuellt: "14625000", statistics: [] }
export const CHEMNITZ: IAdministrationUnit = {  id: -1, gemeindeschluesselaufgefuellt: "14511000", statistics: [] }
export const DRESDEN: IAdministrationUnit = {  id: -1, gemeindeschluesselaufgefuellt: "14612000", statistics: [] }
export const ERZGEBIRGSKREIS: IAdministrationUnit = {  id: -1, gemeindeschluesselaufgefuellt: "14521000", statistics: [] }
export const GOERLITZ: IAdministrationUnit = {  id: -1, gemeindeschluesselaufgefuellt: "14626000", statistics: [] }
export const LANDKREIS_LEIPZIG: IAdministrationUnit = {  id: -1, gemeindeschluesselaufgefuellt: "14729000", statistics: [] }
export const LEIPZIG: IAdministrationUnit = {  id: -1, gemeindeschluesselaufgefuellt: "14713000", statistics: [] }
export const MEISSEN: IAdministrationUnit = {  id: -1, gemeindeschluesselaufgefuellt: "14627000", statistics: [] }
export const MITTELSACHSEN: IAdministrationUnit = { id: -1,  gemeindeschluesselaufgefuellt: "14522000", statistics: [] }
export const NORDSACHSEN: IAdministrationUnit = {  id: -1, gemeindeschluesselaufgefuellt: "14730000", statistics: [] }
export const SSOE: IAdministrationUnit = {  id: -1, gemeindeschluesselaufgefuellt: "14628000", statistics: [] }
export const VOGTLANDKREIS: IAdministrationUnit = {  id: -1, gemeindeschluesselaufgefuellt: "14523000", statistics: [] }
export const ZWICKAU: IAdministrationUnit = {  id: -1, gemeindeschluesselaufgefuellt: "14524000", statistics: [] }

export const DISTRICTS = [BAUTZEN, CHEMNITZ, DRESDEN, ERZGEBIRGSKREIS, GOERLITZ, LANDKREIS_LEIPZIG, LEIPZIG,
  MEISSEN, MITTELSACHSEN, NORDSACHSEN, SSOE, VOGTLANDKREIS, ZWICKAU
]

// data generation import needle
