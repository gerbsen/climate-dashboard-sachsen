import { LngLat } from "maplibre-gl";
import {Source} from "../entities/models/source.model";
import { MAP_STYLE_BRIGHT, MapStyle } from "./maps.constants";

export type DWDState = "brandenburg_berlin" | "brandenburg" | "baden_wuerttemberg" | "bayern" | "mecklenburg_vorpommern" | "niedersachsen" | "niedersachsen_hamburg_bremen" | "nordrhein_westfalen" | "rheinland_pfalz" | "schleswig_holstein" | "saarland" | "sachsen" | "sachsen_anhalt" | "thueringen_sachsen_anhalt" | "thueringen" | "deutschland";

export interface Sponsor {
  url : string;
  imageName: string;
  textHtml: string;
}

export interface StateConfig {
  label: StateName;
  agsId: string;
  stripeImageName: string;
  social: any;
  sources: Source[];
  administrationUnitGeometrySource: Source;
  routeLinks: RouteLink[];
  mapInitOptions: any,
  dwdStateKey: DWDState,
  defaultMapStyle: MapStyle,
  imprintLink: string;
  dataProtectionLink: string;
  sponsors?: Sponsor[];
}

export type StateName = "Sachsen" | "Sachsen-Anhalt" | "Bayern";
export type RouteLink = "home" | "batteriespeicher" | "e-mobilität" | "erneuerbare-energien" | "klima" | "kommunen" |
  "treibhausgase" | "waldzustand" | "wasserhaushalt";

export const SOURCES = {
  COPERNICUS_REF_TEMP: new Source(
    "https://climate.copernicus.eu/sites/default/files/custom-uploads/Page%20Uploads/daily_global_t2m.csv",
    "Referenztemperatur 1850-1900",
    "Daten des Copernicus Programms der EU, die die globale Durchschnittstemperatur pro Tag in der Referenzperiode 1850 - 1900 (in °C) wiedergeben. (Bildquelle: Wikiepdia)",
    "copernicus.png"
  ),
  SMEKUL : new Source(
    "https://www.energie.sachsen.de/zahlen-und-fakten-3971.html",
    "Energiedaten 2019",
    "Auf Grundlage der Energiebilanz von Energieträgern in Sachsen werden Energiedaten über die " +
    "Energieversorgung in Sachsen jährlich veröffentlicht.",
    'smekul.jpg'
  ),
  MASTR : new Source(
    "https://www.marktstammdatenregister.de/MaStR",
    "Markstammdatenregister (MaStR)",
    "Im Marktstammdatenregister werden Stammdaten von Strom- und Gaserzeugungsanlagen " +
    "bundesweit erfasst und in maschinenlesbarer Form zur Verfügung gestellt.",
    "mastr.jpg"
  ),
  DWD: new Source(
    "https://www.dwd.de/DE/leistungen/cdc/climate-data-center.html;jsessionid=89EA878A93F01B14A0464DBFB10D08D4.live21061",
    "Deutscher Wetterdienst (DWD)",
    "Der Deutsche Wetterdienst stellt seinen umfangreichen Klimadatenbestand im Climate Data " +
    "Center bereit, der frei genutzt werden kann.",
    "dwd.jpg"
  ),
  UFZ: new Source(
    "https://www.ufz.de/index.php?de=37937",
    "Helmholtz-Zentrum für Umweltforschung (UFZ)",
    "Das Helmholtz-Zentrum für Umweltforschung stellt seine Daten zum deutschlandweiten Dürremonitor im NetCFD Format zur Vergügung.",
    "ufz.png"
  ),
  BNETZA: new Source(
      'https://data.bundesnetzagentur.de/Bundesnetzagentur/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/E_Mobilitaet/ladesaeulenregister.xlsx',
      "Bundesnetzagentur (BNetzA)",
      "Die BNetzA stellt unter anderem die Daten aller öffentlich verfügbaren Ladesäulen im Ladesäulenregister zur Verfügung",
      "bnetza.jpg"
  )
}

export const STATE_CONFIGS: Record<StateName, StateConfig> = {
  "Sachsen": {
    label : "Sachsen",
    agsId: "14",
    stripeImageName: "EUROPE-Germany-Sachsen-1881-2022-DW.png",
    social : {
      mastodon: "https://mastodon.social/@klimadashboard_sachsen",
    },
    mapInitOptions: { zoom: 7, center: new LngLat(13.45, 50.9), maxZoom: 11 },
    administrationUnitGeometrySource: new Source("https://gdz.bkg.bund.de/index.php/default/digitale-geodaten/verwaltungsgebiete/verwaltungsgebiete-1-2-500-000-stand-31-12-vg2500-12-31.html", "Bundesamt für Kartographie und Geodäsie"),
    sources: [SOURCES.SMEKUL, SOURCES.MASTR, SOURCES.DWD, SOURCES.UFZ, SOURCES.BNETZA],
    routeLinks: ["home", "batteriespeicher", "e-mobilität", "erneuerbare-energien", "klima", "kommunen",
      "treibhausgase", "waldzustand", "wasserhaushalt"],
    dwdStateKey: "sachsen",
    defaultMapStyle: MAP_STYLE_BRIGHT,
    imprintLink: "https://danielgerber.eu/impressum/",
    dataProtectionLink: "https://danielgerber.eu/datenschutzerklaerung/",
    sponsors : [{
      url: "https://www.vee-sachsen.de",
      imageName: "vee-sachsen.png",
      textHtml: "Das Klimadashboard Sachsen wird vom Sächsischen Landesverband für Erneuerbare Energien, der <a href='https://www.vee-sachsen.de' target='_blank'>VEE Sachsen e.V.</a>, unterstützt. Der Verband engagiert sich seit vielen Jahren für die Förderung Erneuerbarer Energien und den Klimaschutz."
    }]
  },
  "Sachsen-Anhalt" : {
    label: "Sachsen-Anhalt",
    agsId: "15",
    stripeImageName: "EUROPE-Germany-Sachsen_Anhalt-1881-2022-DW.png",
    social : {
      mastodon: "",
    },
    mapInitOptions: { zoom: 6.5, center: new LngLat(11.616667, 52.0), maxZoom: 11 },
    administrationUnitGeometrySource: new Source("https://www.lvermgeo.sachsen-anhalt.de/de/dvg-lsa.html", "Landesamt für Vermessung und Geoinformation Sachsen-Anhalt"),
    sources : [SOURCES.MASTR, SOURCES.DWD, SOURCES.UFZ, SOURCES.BNETZA],
    routeLinks: ["home", "batteriespeicher", "e-mobilität", "erneuerbare-energien", "klima", "kommunen",
      "treibhausgase", "wasserhaushalt"],
    dwdStateKey: "sachsen_anhalt",
    defaultMapStyle: MAP_STYLE_BRIGHT,
    imprintLink: "https://danielgerber.eu/impressum/",
    dataProtectionLink: "https://danielgerber.eu/datenschutzerklaerung/"
  },
  "Bayern": {
    label : "Bayern",
    agsId: "09",
    stripeImageName: "EUROPE-Germany-Bayern-1881-2022-DW.png",
    social : {
      mastodon: "https://mastodon.social/@klimadashboard_sachsen",
    },
    mapInitOptions: { zoom: 6.1, center: new LngLat(11.404167, 48.946389), maxZoom: 18 },
    administrationUnitGeometrySource: new Source("https://gdz.bkg.bund.de/index.php/default/digitale-geodaten/verwaltungsgebiete/verwaltungsgebiete-1-2-500-000-stand-31-12-vg2500-12-31.html", "Bundesamt für Kartographie und Geodäsie"),
    sources: [SOURCES.MASTR, SOURCES.DWD, SOURCES.UFZ, SOURCES.BNETZA],
    routeLinks: ["home", "batteriespeicher", "e-mobilität", "erneuerbare-energien", "klima", "kommunen",
      "treibhausgase", "wasserhaushalt"],
    dwdStateKey: "bayern",
    defaultMapStyle: MAP_STYLE_BRIGHT,
    imprintLink: "https://danielgerber.eu/impressum/",
    dataProtectionLink: "https://danielgerber.eu/datenschutzerklaerung/"
  }
};
