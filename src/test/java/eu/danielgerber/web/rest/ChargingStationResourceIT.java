package eu.danielgerber.web.rest;

import static org.geolatte.geom.builder.DSL.g;
import static org.geolatte.geom.builder.DSL.point;
import static org.geolatte.geom.crs.CoordinateReferenceSystems.WGS84;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eu.danielgerber.IntegrationTest;
import eu.danielgerber.domain.AdministrationUnit;
import eu.danielgerber.domain.ChargingStation;
import eu.danielgerber.domain.Statistic;
import eu.danielgerber.repository.ChargingStationRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import jakarta.persistence.EntityManager;

import org.geolatte.geom.jts.JTS;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.Geometry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ChargingStationResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ChargingStationResourceIT {

    private static final String DEFAULT_BETREIBER = "AAAAAAAAAA";
    private static final String UPDATED_BETREIBER = "BBBBBBBBBB";

    private static final String DEFAULT_STRASSE = "AAAAAAAAAA";
    private static final String UPDATED_STRASSE = "BBBBBBBBBB";

    private static final String DEFAULT_HAUSNUMMER = "AAAAAAAAAA";
    private static final String UPDATED_HAUSNUMMER = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSZUSATZ = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSZUSATZ = "BBBBBBBBBB";

    private static final String DEFAULT_POSTLEITZAHL = "AAAAAAAAAA";
    private static final String UPDATED_POSTLEITZAHL = "BBBBBBBBBB";

    private static final String DEFAULT_ORT = "AAAAAAAAAA";
    private static final String UPDATED_ORT = "BBBBBBBBBB";

    private static final String DEFAULT_BUNDESLAND = "AAAAAAAAAA";
    private static final String UPDATED_BUNDESLAND = "BBBBBBBBBB";

    private static final String DEFAULT_KREIS_KREISFREIE_STADT = "AAAAAAAAAA";
    private static final String UPDATED_KREIS_KREISFREIE_STADT = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_INBETRIEBNAHME_DATUM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_INBETRIEBNAHME_DATUM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_INBETRIEBNAHME_DATUM = LocalDate.ofEpochDay(-1L);

    private static final Double DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG = 1D;
    private static final Double UPDATED_NENNLEISTUNG_LADEEINRICHTUNG = 2D;
    private static final Double SMALLER_NENNLEISTUNG_LADEEINRICHTUNG = 1D - 1D;

    private static final String DEFAULT_ART_DER_LADEEINRICHTUNG = "AAAAAAAAAA";
    private static final String UPDATED_ART_DER_LADEEINRICHTUNG = "BBBBBBBBBB";

    private static final Integer DEFAULT_ANZAHL_LADEPUNKTE = 1;
    private static final Integer UPDATED_ANZAHL_LADEPUNKTE = 2;
    private static final Integer SMALLER_ANZAHL_LADEPUNKTE = 1 - 1;

    private static final String DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN = "AAAAAAAAAA";
    private static final String UPDATED_ERSTER_LADEPUNKT_STECKERTYPEN = "BBBBBBBBBB";

    private static final Double DEFAULT_ERSTER_LADEPUNKT_LEISTUNG = 1D;
    private static final Double UPDATED_ERSTER_LADEPUNKT_LEISTUNG = 2D;
    private static final Double SMALLER_ERSTER_LADEPUNKT_LEISTUNG = 1D - 1D;

    private static final String DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN = "AAAAAAAAAA";
    private static final String UPDATED_ZWEITER_LADEPUNKT_STECKERTYPEN = "BBBBBBBBBB";

    private static final Double DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG = 1D;
    private static final Double UPDATED_ZWEITER_LADEPUNKT_LEISTUNG = 2D;
    private static final Double SMALLER_ZWEITER_LADEPUNKT_LEISTUNG = 1D - 1D;

    private static final String DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN = "AAAAAAAAAA";
    private static final String UPDATED_DRITTER_LADEPUNKT_STECKERTYPEN = "BBBBBBBBBB";

    private static final Double DEFAULT_DRITTER_LADEPUNKT_LEISTUNG = 1D;
    private static final Double UPDATED_DRITTER_LADEPUNKT_LEISTUNG = 2D;
    private static final Double SMALLER_DRITTER_LADEPUNKT_LEISTUNG = 1D - 1D;

    private static final String DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN = "AAAAAAAAAA";
    private static final String UPDATED_VIERTER_LADEPUNKT_STECKERTYPEN = "BBBBBBBBBB";

    private static final Double DEFAULT_VIERTER_LADEPUNKT_LEISTUNG = 1D;
    private static final Double UPDATED_VIERTER_LADEPUNKT_LEISTUNG = 2D;
    private static final Double SMALLER_VIERTER_LADEPUNKT_LEISTUNG = 1D - 1D;

    private static final Geometry DEFAULT_GEOMETRY = JTS.to(point(WGS84,g(4.33,53.21)));
    private static final Geometry UPDATED_GEOMETRY = JTS.to(point(WGS84,g(5.33,54.21)));

    private static final String ENTITY_API_URL = "/api/charging-stations";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private ChargingStationRepository chargingStationRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restChargingStationMockMvc;

    private ChargingStation chargingStation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChargingStation createEntity(EntityManager em) {
        ChargingStation chargingStation = new ChargingStation()
            .betreiber(DEFAULT_BETREIBER)
            .strasse(DEFAULT_STRASSE)
            .hausnummer(DEFAULT_HAUSNUMMER)
            .adresszusatz(DEFAULT_ADRESSZUSATZ)
            .postleitzahl(DEFAULT_POSTLEITZAHL)
            .ort(DEFAULT_ORT)
            .bundesland(DEFAULT_BUNDESLAND)
            .kreisKreisfreieStadt(DEFAULT_KREIS_KREISFREIE_STADT)
            .inbetriebnahmeDatum(DEFAULT_INBETRIEBNAHME_DATUM)
            .nennleistungLadeeinrichtung(DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG)
            .artDerLadeeinrichtung(DEFAULT_ART_DER_LADEEINRICHTUNG)
            .anzahlLadepunkte(DEFAULT_ANZAHL_LADEPUNKTE)
            .ersterLadepunktSteckertypen(DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN)
            .ersterLadepunktLeistung(DEFAULT_ERSTER_LADEPUNKT_LEISTUNG)
            .zweiterLadepunktSteckertypen(DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN)
            .zweiterLadepunktLeistung(DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG)
            .dritterLadepunktSteckertypen(DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN)
            .dritterLadepunktLeistung(DEFAULT_DRITTER_LADEPUNKT_LEISTUNG)
            .vierterLadepunktSteckertypen(DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN)
            .vierterLadepunktLeistung(DEFAULT_VIERTER_LADEPUNKT_LEISTUNG)
            .geometry(DEFAULT_GEOMETRY);
        return chargingStation;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChargingStation createUpdatedEntity(EntityManager em) {
        ChargingStation chargingStation = new ChargingStation()
            .betreiber(UPDATED_BETREIBER)
            .strasse(UPDATED_STRASSE)
            .hausnummer(UPDATED_HAUSNUMMER)
            .adresszusatz(UPDATED_ADRESSZUSATZ)
            .postleitzahl(UPDATED_POSTLEITZAHL)
            .ort(UPDATED_ORT)
            .bundesland(UPDATED_BUNDESLAND)
            .kreisKreisfreieStadt(UPDATED_KREIS_KREISFREIE_STADT)
            .inbetriebnahmeDatum(UPDATED_INBETRIEBNAHME_DATUM)
            .nennleistungLadeeinrichtung(UPDATED_NENNLEISTUNG_LADEEINRICHTUNG)
            .artDerLadeeinrichtung(UPDATED_ART_DER_LADEEINRICHTUNG)
            .anzahlLadepunkte(UPDATED_ANZAHL_LADEPUNKTE)
            .ersterLadepunktSteckertypen(UPDATED_ERSTER_LADEPUNKT_STECKERTYPEN)
            .ersterLadepunktLeistung(UPDATED_ERSTER_LADEPUNKT_LEISTUNG)
            .zweiterLadepunktSteckertypen(UPDATED_ZWEITER_LADEPUNKT_STECKERTYPEN)
            .zweiterLadepunktLeistung(UPDATED_ZWEITER_LADEPUNKT_LEISTUNG)
            .dritterLadepunktSteckertypen(UPDATED_DRITTER_LADEPUNKT_STECKERTYPEN)
            .dritterLadepunktLeistung(UPDATED_DRITTER_LADEPUNKT_LEISTUNG)
            .vierterLadepunktSteckertypen(UPDATED_VIERTER_LADEPUNKT_STECKERTYPEN)
            .vierterLadepunktLeistung(UPDATED_VIERTER_LADEPUNKT_LEISTUNG)
            .geometry(UPDATED_GEOMETRY);
        return chargingStation;
    }

    @BeforeEach
    public void initTest() {
        chargingStation = createEntity(em);
    }

    @Test
    @Transactional
    void getAllChargingStations() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList
        restChargingStationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chargingStation.getId().intValue())))
            .andExpect(jsonPath("$.[*].betreiber").value(hasItem(DEFAULT_BETREIBER)))
            .andExpect(jsonPath("$.[*].strasse").value(hasItem(DEFAULT_STRASSE)))
            .andExpect(jsonPath("$.[*].hausnummer").value(hasItem(DEFAULT_HAUSNUMMER)))
            .andExpect(jsonPath("$.[*].adresszusatz").value(hasItem(DEFAULT_ADRESSZUSATZ)))
            .andExpect(jsonPath("$.[*].postleitzahl").value(hasItem(DEFAULT_POSTLEITZAHL)))
            .andExpect(jsonPath("$.[*].ort").value(hasItem(DEFAULT_ORT)))
            .andExpect(jsonPath("$.[*].bundesland").value(hasItem(DEFAULT_BUNDESLAND)))
            .andExpect(jsonPath("$.[*].kreisKreisfreieStadt").value(hasItem(DEFAULT_KREIS_KREISFREIE_STADT)))
            .andExpect(jsonPath("$.[*].inbetriebnahmeDatum").value(hasItem(DEFAULT_INBETRIEBNAHME_DATUM.toString())))
            .andExpect(jsonPath("$.[*].nennleistungLadeeinrichtung").value(hasItem(DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].artDerLadeeinrichtung").value(hasItem(DEFAULT_ART_DER_LADEEINRICHTUNG)))
            .andExpect(jsonPath("$.[*].anzahlLadepunkte").value(hasItem(DEFAULT_ANZAHL_LADEPUNKTE)))
            .andExpect(jsonPath("$.[*].ersterLadepunktSteckertypen").value(hasItem(DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN)))
            .andExpect(jsonPath("$.[*].ersterLadepunktLeistung").value(hasItem(DEFAULT_ERSTER_LADEPUNKT_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].zweiterLadepunktSteckertypen").value(hasItem(DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN)))
            .andExpect(jsonPath("$.[*].zweiterLadepunktLeistung").value(hasItem(DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].dritterLadepunktSteckertypen").value(hasItem(DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN)))
            .andExpect(jsonPath("$.[*].dritterLadepunktLeistung").value(hasItem(DEFAULT_DRITTER_LADEPUNKT_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].vierterLadepunktSteckertypen").value(hasItem(DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN)))
            .andExpect(jsonPath("$.[*].vierterLadepunktLeistung").value(hasItem(DEFAULT_VIERTER_LADEPUNKT_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].geometry").value(hasItem(DEFAULT_GEOMETRY)));
    }

    @Test
    @Transactional
    void getChargingStation() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get the chargingStation
        restChargingStationMockMvc
            .perform(get(ENTITY_API_URL_ID, chargingStation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(chargingStation.getId().intValue()))
            .andExpect(jsonPath("$.betreiber").value(DEFAULT_BETREIBER))
            .andExpect(jsonPath("$.strasse").value(DEFAULT_STRASSE))
            .andExpect(jsonPath("$.hausnummer").value(DEFAULT_HAUSNUMMER))
            .andExpect(jsonPath("$.adresszusatz").value(DEFAULT_ADRESSZUSATZ))
            .andExpect(jsonPath("$.postleitzahl").value(DEFAULT_POSTLEITZAHL))
            .andExpect(jsonPath("$.ort").value(DEFAULT_ORT))
            .andExpect(jsonPath("$.bundesland").value(DEFAULT_BUNDESLAND))
            .andExpect(jsonPath("$.kreisKreisfreieStadt").value(DEFAULT_KREIS_KREISFREIE_STADT))
            .andExpect(jsonPath("$.inbetriebnahmeDatum").value(DEFAULT_INBETRIEBNAHME_DATUM.toString()))
            .andExpect(jsonPath("$.nennleistungLadeeinrichtung").value(DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG.doubleValue()))
            .andExpect(jsonPath("$.artDerLadeeinrichtung").value(DEFAULT_ART_DER_LADEEINRICHTUNG))
            .andExpect(jsonPath("$.anzahlLadepunkte").value(DEFAULT_ANZAHL_LADEPUNKTE))
            .andExpect(jsonPath("$.ersterLadepunktSteckertypen").value(DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN))
            .andExpect(jsonPath("$.ersterLadepunktLeistung").value(DEFAULT_ERSTER_LADEPUNKT_LEISTUNG.doubleValue()))
            .andExpect(jsonPath("$.zweiterLadepunktSteckertypen").value(DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN))
            .andExpect(jsonPath("$.zweiterLadepunktLeistung").value(DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG.doubleValue()))
            .andExpect(jsonPath("$.dritterLadepunktSteckertypen").value(DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN))
            .andExpect(jsonPath("$.dritterLadepunktLeistung").value(DEFAULT_DRITTER_LADEPUNKT_LEISTUNG.doubleValue()))
            .andExpect(jsonPath("$.vierterLadepunktSteckertypen").value(DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN))
            .andExpect(jsonPath("$.vierterLadepunktLeistung").value(DEFAULT_VIERTER_LADEPUNKT_LEISTUNG.doubleValue()))
            .andExpect(jsonPath("$.geometry").value(DEFAULT_GEOMETRY));
    }

    @Test
    @Transactional
    void getChargingStationsByIdFiltering() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        Long id = chargingStation.getId();

        defaultChargingStationShouldBeFound("id.equals=" + id);
        defaultChargingStationShouldNotBeFound("id.notEquals=" + id);

        defaultChargingStationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultChargingStationShouldNotBeFound("id.greaterThan=" + id);

        defaultChargingStationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultChargingStationShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllChargingStationsByBetreiberIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where betreiber equals to DEFAULT_BETREIBER
        defaultChargingStationShouldBeFound("betreiber.equals=" + DEFAULT_BETREIBER);

        // Get all the chargingStationList where betreiber equals to UPDATED_BETREIBER
        defaultChargingStationShouldNotBeFound("betreiber.equals=" + UPDATED_BETREIBER);
    }

    @Test
    @Transactional
    void getAllChargingStationsByBetreiberIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where betreiber in DEFAULT_BETREIBER or UPDATED_BETREIBER
        defaultChargingStationShouldBeFound("betreiber.in=" + DEFAULT_BETREIBER + "," + UPDATED_BETREIBER);

        // Get all the chargingStationList where betreiber equals to UPDATED_BETREIBER
        defaultChargingStationShouldNotBeFound("betreiber.in=" + UPDATED_BETREIBER);
    }

    @Test
    @Transactional
    void getAllChargingStationsByBetreiberIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where betreiber is not null
        defaultChargingStationShouldBeFound("betreiber.specified=true");

        // Get all the chargingStationList where betreiber is null
        defaultChargingStationShouldNotBeFound("betreiber.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByBetreiberContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where betreiber contains DEFAULT_BETREIBER
        defaultChargingStationShouldBeFound("betreiber.contains=" + DEFAULT_BETREIBER);

        // Get all the chargingStationList where betreiber contains UPDATED_BETREIBER
        defaultChargingStationShouldNotBeFound("betreiber.contains=" + UPDATED_BETREIBER);
    }

    @Test
    @Transactional
    void getAllChargingStationsByBetreiberNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where betreiber does not contain DEFAULT_BETREIBER
        defaultChargingStationShouldNotBeFound("betreiber.doesNotContain=" + DEFAULT_BETREIBER);

        // Get all the chargingStationList where betreiber does not contain UPDATED_BETREIBER
        defaultChargingStationShouldBeFound("betreiber.doesNotContain=" + UPDATED_BETREIBER);
    }

    @Test
    @Transactional
    void getAllChargingStationsByStrasseIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where strasse equals to DEFAULT_STRASSE
        defaultChargingStationShouldBeFound("strasse.equals=" + DEFAULT_STRASSE);

        // Get all the chargingStationList where strasse equals to UPDATED_STRASSE
        defaultChargingStationShouldNotBeFound("strasse.equals=" + UPDATED_STRASSE);
    }

    @Test
    @Transactional
    void getAllChargingStationsByStrasseIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where strasse in DEFAULT_STRASSE or UPDATED_STRASSE
        defaultChargingStationShouldBeFound("strasse.in=" + DEFAULT_STRASSE + "," + UPDATED_STRASSE);

        // Get all the chargingStationList where strasse equals to UPDATED_STRASSE
        defaultChargingStationShouldNotBeFound("strasse.in=" + UPDATED_STRASSE);
    }

    @Test
    @Transactional
    void getAllChargingStationsByStrasseIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where strasse is not null
        defaultChargingStationShouldBeFound("strasse.specified=true");

        // Get all the chargingStationList where strasse is null
        defaultChargingStationShouldNotBeFound("strasse.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByStrasseContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where strasse contains DEFAULT_STRASSE
        defaultChargingStationShouldBeFound("strasse.contains=" + DEFAULT_STRASSE);

        // Get all the chargingStationList where strasse contains UPDATED_STRASSE
        defaultChargingStationShouldNotBeFound("strasse.contains=" + UPDATED_STRASSE);
    }

    @Test
    @Transactional
    void getAllChargingStationsByStrasseNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where strasse does not contain DEFAULT_STRASSE
        defaultChargingStationShouldNotBeFound("strasse.doesNotContain=" + DEFAULT_STRASSE);

        // Get all the chargingStationList where strasse does not contain UPDATED_STRASSE
        defaultChargingStationShouldBeFound("strasse.doesNotContain=" + UPDATED_STRASSE);
    }

    @Test
    @Transactional
    void getAllChargingStationsByHausnummerIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where hausnummer equals to DEFAULT_HAUSNUMMER
        defaultChargingStationShouldBeFound("hausnummer.equals=" + DEFAULT_HAUSNUMMER);

        // Get all the chargingStationList where hausnummer equals to UPDATED_HAUSNUMMER
        defaultChargingStationShouldNotBeFound("hausnummer.equals=" + UPDATED_HAUSNUMMER);
    }

    @Test
    @Transactional
    void getAllChargingStationsByHausnummerIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where hausnummer in DEFAULT_HAUSNUMMER or UPDATED_HAUSNUMMER
        defaultChargingStationShouldBeFound("hausnummer.in=" + DEFAULT_HAUSNUMMER + "," + UPDATED_HAUSNUMMER);

        // Get all the chargingStationList where hausnummer equals to UPDATED_HAUSNUMMER
        defaultChargingStationShouldNotBeFound("hausnummer.in=" + UPDATED_HAUSNUMMER);
    }

    @Test
    @Transactional
    void getAllChargingStationsByHausnummerIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where hausnummer is not null
        defaultChargingStationShouldBeFound("hausnummer.specified=true");

        // Get all the chargingStationList where hausnummer is null
        defaultChargingStationShouldNotBeFound("hausnummer.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByHausnummerContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where hausnummer contains DEFAULT_HAUSNUMMER
        defaultChargingStationShouldBeFound("hausnummer.contains=" + DEFAULT_HAUSNUMMER);

        // Get all the chargingStationList where hausnummer contains UPDATED_HAUSNUMMER
        defaultChargingStationShouldNotBeFound("hausnummer.contains=" + UPDATED_HAUSNUMMER);
    }

    @Test
    @Transactional
    void getAllChargingStationsByHausnummerNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where hausnummer does not contain DEFAULT_HAUSNUMMER
        defaultChargingStationShouldNotBeFound("hausnummer.doesNotContain=" + DEFAULT_HAUSNUMMER);

        // Get all the chargingStationList where hausnummer does not contain UPDATED_HAUSNUMMER
        defaultChargingStationShouldBeFound("hausnummer.doesNotContain=" + UPDATED_HAUSNUMMER);
    }

    @Test
    @Transactional
    void getAllChargingStationsByAdresszusatzIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where adresszusatz equals to DEFAULT_ADRESSZUSATZ
        defaultChargingStationShouldBeFound("adresszusatz.equals=" + DEFAULT_ADRESSZUSATZ);

        // Get all the chargingStationList where adresszusatz equals to UPDATED_ADRESSZUSATZ
        defaultChargingStationShouldNotBeFound("adresszusatz.equals=" + UPDATED_ADRESSZUSATZ);
    }

    @Test
    @Transactional
    void getAllChargingStationsByAdresszusatzIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where adresszusatz in DEFAULT_ADRESSZUSATZ or UPDATED_ADRESSZUSATZ
        defaultChargingStationShouldBeFound("adresszusatz.in=" + DEFAULT_ADRESSZUSATZ + "," + UPDATED_ADRESSZUSATZ);

        // Get all the chargingStationList where adresszusatz equals to UPDATED_ADRESSZUSATZ
        defaultChargingStationShouldNotBeFound("adresszusatz.in=" + UPDATED_ADRESSZUSATZ);
    }

    @Test
    @Transactional
    void getAllChargingStationsByAdresszusatzIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where adresszusatz is not null
        defaultChargingStationShouldBeFound("adresszusatz.specified=true");

        // Get all the chargingStationList where adresszusatz is null
        defaultChargingStationShouldNotBeFound("adresszusatz.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByAdresszusatzContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where adresszusatz contains DEFAULT_ADRESSZUSATZ
        defaultChargingStationShouldBeFound("adresszusatz.contains=" + DEFAULT_ADRESSZUSATZ);

        // Get all the chargingStationList where adresszusatz contains UPDATED_ADRESSZUSATZ
        defaultChargingStationShouldNotBeFound("adresszusatz.contains=" + UPDATED_ADRESSZUSATZ);
    }

    @Test
    @Transactional
    void getAllChargingStationsByAdresszusatzNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where adresszusatz does not contain DEFAULT_ADRESSZUSATZ
        defaultChargingStationShouldNotBeFound("adresszusatz.doesNotContain=" + DEFAULT_ADRESSZUSATZ);

        // Get all the chargingStationList where adresszusatz does not contain UPDATED_ADRESSZUSATZ
        defaultChargingStationShouldBeFound("adresszusatz.doesNotContain=" + UPDATED_ADRESSZUSATZ);
    }

    @Test
    @Transactional
    void getAllChargingStationsByPostleitzahlIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where postleitzahl equals to DEFAULT_POSTLEITZAHL
        defaultChargingStationShouldBeFound("postleitzahl.equals=" + DEFAULT_POSTLEITZAHL);

        // Get all the chargingStationList where postleitzahl equals to UPDATED_POSTLEITZAHL
        defaultChargingStationShouldNotBeFound("postleitzahl.equals=" + UPDATED_POSTLEITZAHL);
    }

    @Test
    @Transactional
    void getAllChargingStationsByPostleitzahlIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where postleitzahl in DEFAULT_POSTLEITZAHL or UPDATED_POSTLEITZAHL
        defaultChargingStationShouldBeFound("postleitzahl.in=" + DEFAULT_POSTLEITZAHL + "," + UPDATED_POSTLEITZAHL);

        // Get all the chargingStationList where postleitzahl equals to UPDATED_POSTLEITZAHL
        defaultChargingStationShouldNotBeFound("postleitzahl.in=" + UPDATED_POSTLEITZAHL);
    }

    @Test
    @Transactional
    void getAllChargingStationsByPostleitzahlIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where postleitzahl is not null
        defaultChargingStationShouldBeFound("postleitzahl.specified=true");

        // Get all the chargingStationList where postleitzahl is null
        defaultChargingStationShouldNotBeFound("postleitzahl.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByPostleitzahlContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where postleitzahl contains DEFAULT_POSTLEITZAHL
        defaultChargingStationShouldBeFound("postleitzahl.contains=" + DEFAULT_POSTLEITZAHL);

        // Get all the chargingStationList where postleitzahl contains UPDATED_POSTLEITZAHL
        defaultChargingStationShouldNotBeFound("postleitzahl.contains=" + UPDATED_POSTLEITZAHL);
    }

    @Test
    @Transactional
    void getAllChargingStationsByPostleitzahlNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where postleitzahl does not contain DEFAULT_POSTLEITZAHL
        defaultChargingStationShouldNotBeFound("postleitzahl.doesNotContain=" + DEFAULT_POSTLEITZAHL);

        // Get all the chargingStationList where postleitzahl does not contain UPDATED_POSTLEITZAHL
        defaultChargingStationShouldBeFound("postleitzahl.doesNotContain=" + UPDATED_POSTLEITZAHL);
    }

    @Test
    @Transactional
    void getAllChargingStationsByOrtIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ort equals to DEFAULT_ORT
        defaultChargingStationShouldBeFound("ort.equals=" + DEFAULT_ORT);

        // Get all the chargingStationList where ort equals to UPDATED_ORT
        defaultChargingStationShouldNotBeFound("ort.equals=" + UPDATED_ORT);
    }

    @Test
    @Transactional
    void getAllChargingStationsByOrtIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ort in DEFAULT_ORT or UPDATED_ORT
        defaultChargingStationShouldBeFound("ort.in=" + DEFAULT_ORT + "," + UPDATED_ORT);

        // Get all the chargingStationList where ort equals to UPDATED_ORT
        defaultChargingStationShouldNotBeFound("ort.in=" + UPDATED_ORT);
    }

    @Test
    @Transactional
    void getAllChargingStationsByOrtIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ort is not null
        defaultChargingStationShouldBeFound("ort.specified=true");

        // Get all the chargingStationList where ort is null
        defaultChargingStationShouldNotBeFound("ort.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByOrtContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ort contains DEFAULT_ORT
        defaultChargingStationShouldBeFound("ort.contains=" + DEFAULT_ORT);

        // Get all the chargingStationList where ort contains UPDATED_ORT
        defaultChargingStationShouldNotBeFound("ort.contains=" + UPDATED_ORT);
    }

    @Test
    @Transactional
    void getAllChargingStationsByOrtNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ort does not contain DEFAULT_ORT
        defaultChargingStationShouldNotBeFound("ort.doesNotContain=" + DEFAULT_ORT);

        // Get all the chargingStationList where ort does not contain UPDATED_ORT
        defaultChargingStationShouldBeFound("ort.doesNotContain=" + UPDATED_ORT);
    }

    @Test
    @Transactional
    void getAllChargingStationsByBundeslandIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where bundesland equals to DEFAULT_BUNDESLAND
        defaultChargingStationShouldBeFound("bundesland.equals=" + DEFAULT_BUNDESLAND);

        // Get all the chargingStationList where bundesland equals to UPDATED_BUNDESLAND
        defaultChargingStationShouldNotBeFound("bundesland.equals=" + UPDATED_BUNDESLAND);
    }

    @Test
    @Transactional
    void getAllChargingStationsByBundeslandIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where bundesland in DEFAULT_BUNDESLAND or UPDATED_BUNDESLAND
        defaultChargingStationShouldBeFound("bundesland.in=" + DEFAULT_BUNDESLAND + "," + UPDATED_BUNDESLAND);

        // Get all the chargingStationList where bundesland equals to UPDATED_BUNDESLAND
        defaultChargingStationShouldNotBeFound("bundesland.in=" + UPDATED_BUNDESLAND);
    }

    @Test
    @Transactional
    void getAllChargingStationsByBundeslandIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where bundesland is not null
        defaultChargingStationShouldBeFound("bundesland.specified=true");

        // Get all the chargingStationList where bundesland is null
        defaultChargingStationShouldNotBeFound("bundesland.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByBundeslandContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where bundesland contains DEFAULT_BUNDESLAND
        defaultChargingStationShouldBeFound("bundesland.contains=" + DEFAULT_BUNDESLAND);

        // Get all the chargingStationList where bundesland contains UPDATED_BUNDESLAND
        defaultChargingStationShouldNotBeFound("bundesland.contains=" + UPDATED_BUNDESLAND);
    }

    @Test
    @Transactional
    void getAllChargingStationsByBundeslandNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where bundesland does not contain DEFAULT_BUNDESLAND
        defaultChargingStationShouldNotBeFound("bundesland.doesNotContain=" + DEFAULT_BUNDESLAND);

        // Get all the chargingStationList where bundesland does not contain UPDATED_BUNDESLAND
        defaultChargingStationShouldBeFound("bundesland.doesNotContain=" + UPDATED_BUNDESLAND);
    }

    @Test
    @Transactional
    void getAllChargingStationsByKreisKreisfreieStadtIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where kreisKreisfreieStadt equals to DEFAULT_KREIS_KREISFREIE_STADT
        defaultChargingStationShouldBeFound("kreisKreisfreieStadt.equals=" + DEFAULT_KREIS_KREISFREIE_STADT);

        // Get all the chargingStationList where kreisKreisfreieStadt equals to UPDATED_KREIS_KREISFREIE_STADT
        defaultChargingStationShouldNotBeFound("kreisKreisfreieStadt.equals=" + UPDATED_KREIS_KREISFREIE_STADT);
    }

    @Test
    @Transactional
    void getAllChargingStationsByKreisKreisfreieStadtIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where kreisKreisfreieStadt in DEFAULT_KREIS_KREISFREIE_STADT or UPDATED_KREIS_KREISFREIE_STADT
        defaultChargingStationShouldBeFound(
            "kreisKreisfreieStadt.in=" + DEFAULT_KREIS_KREISFREIE_STADT + "," + UPDATED_KREIS_KREISFREIE_STADT
        );

        // Get all the chargingStationList where kreisKreisfreieStadt equals to UPDATED_KREIS_KREISFREIE_STADT
        defaultChargingStationShouldNotBeFound("kreisKreisfreieStadt.in=" + UPDATED_KREIS_KREISFREIE_STADT);
    }

    @Test
    @Transactional
    void getAllChargingStationsByKreisKreisfreieStadtIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where kreisKreisfreieStadt is not null
        defaultChargingStationShouldBeFound("kreisKreisfreieStadt.specified=true");

        // Get all the chargingStationList where kreisKreisfreieStadt is null
        defaultChargingStationShouldNotBeFound("kreisKreisfreieStadt.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByKreisKreisfreieStadtContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where kreisKreisfreieStadt contains DEFAULT_KREIS_KREISFREIE_STADT
        defaultChargingStationShouldBeFound("kreisKreisfreieStadt.contains=" + DEFAULT_KREIS_KREISFREIE_STADT);

        // Get all the chargingStationList where kreisKreisfreieStadt contains UPDATED_KREIS_KREISFREIE_STADT
        defaultChargingStationShouldNotBeFound("kreisKreisfreieStadt.contains=" + UPDATED_KREIS_KREISFREIE_STADT);
    }

    @Test
    @Transactional
    void getAllChargingStationsByKreisKreisfreieStadtNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where kreisKreisfreieStadt does not contain DEFAULT_KREIS_KREISFREIE_STADT
        defaultChargingStationShouldNotBeFound("kreisKreisfreieStadt.doesNotContain=" + DEFAULT_KREIS_KREISFREIE_STADT);

        // Get all the chargingStationList where kreisKreisfreieStadt does not contain UPDATED_KREIS_KREISFREIE_STADT
        defaultChargingStationShouldBeFound("kreisKreisfreieStadt.doesNotContain=" + UPDATED_KREIS_KREISFREIE_STADT);
    }

    @Test
    @Transactional
    void getAllChargingStationsByInbetriebnahmeDatumIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where inbetriebnahmeDatum equals to DEFAULT_INBETRIEBNAHME_DATUM
        defaultChargingStationShouldBeFound("inbetriebnahmeDatum.equals=" + DEFAULT_INBETRIEBNAHME_DATUM);

        // Get all the chargingStationList where inbetriebnahmeDatum equals to UPDATED_INBETRIEBNAHME_DATUM
        defaultChargingStationShouldNotBeFound("inbetriebnahmeDatum.equals=" + UPDATED_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllChargingStationsByInbetriebnahmeDatumIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where inbetriebnahmeDatum in DEFAULT_INBETRIEBNAHME_DATUM or UPDATED_INBETRIEBNAHME_DATUM
        defaultChargingStationShouldBeFound("inbetriebnahmeDatum.in=" + DEFAULT_INBETRIEBNAHME_DATUM + "," + UPDATED_INBETRIEBNAHME_DATUM);

        // Get all the chargingStationList where inbetriebnahmeDatum equals to UPDATED_INBETRIEBNAHME_DATUM
        defaultChargingStationShouldNotBeFound("inbetriebnahmeDatum.in=" + UPDATED_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllChargingStationsByInbetriebnahmeDatumIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where inbetriebnahmeDatum is not null
        defaultChargingStationShouldBeFound("inbetriebnahmeDatum.specified=true");

        // Get all the chargingStationList where inbetriebnahmeDatum is null
        defaultChargingStationShouldNotBeFound("inbetriebnahmeDatum.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByInbetriebnahmeDatumIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where inbetriebnahmeDatum is greater than or equal to DEFAULT_INBETRIEBNAHME_DATUM
        defaultChargingStationShouldBeFound("inbetriebnahmeDatum.greaterThanOrEqual=" + DEFAULT_INBETRIEBNAHME_DATUM);

        // Get all the chargingStationList where inbetriebnahmeDatum is greater than or equal to UPDATED_INBETRIEBNAHME_DATUM
        defaultChargingStationShouldNotBeFound("inbetriebnahmeDatum.greaterThanOrEqual=" + UPDATED_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllChargingStationsByInbetriebnahmeDatumIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where inbetriebnahmeDatum is less than or equal to DEFAULT_INBETRIEBNAHME_DATUM
        defaultChargingStationShouldBeFound("inbetriebnahmeDatum.lessThanOrEqual=" + DEFAULT_INBETRIEBNAHME_DATUM);

        // Get all the chargingStationList where inbetriebnahmeDatum is less than or equal to SMALLER_INBETRIEBNAHME_DATUM
        defaultChargingStationShouldNotBeFound("inbetriebnahmeDatum.lessThanOrEqual=" + SMALLER_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllChargingStationsByInbetriebnahmeDatumIsLessThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where inbetriebnahmeDatum is less than DEFAULT_INBETRIEBNAHME_DATUM
        defaultChargingStationShouldNotBeFound("inbetriebnahmeDatum.lessThan=" + DEFAULT_INBETRIEBNAHME_DATUM);

        // Get all the chargingStationList where inbetriebnahmeDatum is less than UPDATED_INBETRIEBNAHME_DATUM
        defaultChargingStationShouldBeFound("inbetriebnahmeDatum.lessThan=" + UPDATED_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllChargingStationsByInbetriebnahmeDatumIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where inbetriebnahmeDatum is greater than DEFAULT_INBETRIEBNAHME_DATUM
        defaultChargingStationShouldNotBeFound("inbetriebnahmeDatum.greaterThan=" + DEFAULT_INBETRIEBNAHME_DATUM);

        // Get all the chargingStationList where inbetriebnahmeDatum is greater than SMALLER_INBETRIEBNAHME_DATUM
        defaultChargingStationShouldBeFound("inbetriebnahmeDatum.greaterThan=" + SMALLER_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllChargingStationsByNennleistungLadeeinrichtungIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where nennleistungLadeeinrichtung equals to DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG
        defaultChargingStationShouldBeFound("nennleistungLadeeinrichtung.equals=" + DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG);

        // Get all the chargingStationList where nennleistungLadeeinrichtung equals to UPDATED_NENNLEISTUNG_LADEEINRICHTUNG
        defaultChargingStationShouldNotBeFound("nennleistungLadeeinrichtung.equals=" + UPDATED_NENNLEISTUNG_LADEEINRICHTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByNennleistungLadeeinrichtungIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where nennleistungLadeeinrichtung in DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG or UPDATED_NENNLEISTUNG_LADEEINRICHTUNG
        defaultChargingStationShouldBeFound(
            "nennleistungLadeeinrichtung.in=" + DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG + "," + UPDATED_NENNLEISTUNG_LADEEINRICHTUNG
        );

        // Get all the chargingStationList where nennleistungLadeeinrichtung equals to UPDATED_NENNLEISTUNG_LADEEINRICHTUNG
        defaultChargingStationShouldNotBeFound("nennleistungLadeeinrichtung.in=" + UPDATED_NENNLEISTUNG_LADEEINRICHTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByNennleistungLadeeinrichtungIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where nennleistungLadeeinrichtung is not null
        defaultChargingStationShouldBeFound("nennleistungLadeeinrichtung.specified=true");

        // Get all the chargingStationList where nennleistungLadeeinrichtung is null
        defaultChargingStationShouldNotBeFound("nennleistungLadeeinrichtung.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByNennleistungLadeeinrichtungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where nennleistungLadeeinrichtung is greater than or equal to DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG
        defaultChargingStationShouldBeFound("nennleistungLadeeinrichtung.greaterThanOrEqual=" + DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG);

        // Get all the chargingStationList where nennleistungLadeeinrichtung is greater than or equal to UPDATED_NENNLEISTUNG_LADEEINRICHTUNG
        defaultChargingStationShouldNotBeFound("nennleistungLadeeinrichtung.greaterThanOrEqual=" + UPDATED_NENNLEISTUNG_LADEEINRICHTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByNennleistungLadeeinrichtungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where nennleistungLadeeinrichtung is less than or equal to DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG
        defaultChargingStationShouldBeFound("nennleistungLadeeinrichtung.lessThanOrEqual=" + DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG);

        // Get all the chargingStationList where nennleistungLadeeinrichtung is less than or equal to SMALLER_NENNLEISTUNG_LADEEINRICHTUNG
        defaultChargingStationShouldNotBeFound("nennleistungLadeeinrichtung.lessThanOrEqual=" + SMALLER_NENNLEISTUNG_LADEEINRICHTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByNennleistungLadeeinrichtungIsLessThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where nennleistungLadeeinrichtung is less than DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG
        defaultChargingStationShouldNotBeFound("nennleistungLadeeinrichtung.lessThan=" + DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG);

        // Get all the chargingStationList where nennleistungLadeeinrichtung is less than UPDATED_NENNLEISTUNG_LADEEINRICHTUNG
        defaultChargingStationShouldBeFound("nennleistungLadeeinrichtung.lessThan=" + UPDATED_NENNLEISTUNG_LADEEINRICHTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByNennleistungLadeeinrichtungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where nennleistungLadeeinrichtung is greater than DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG
        defaultChargingStationShouldNotBeFound("nennleistungLadeeinrichtung.greaterThan=" + DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG);

        // Get all the chargingStationList where nennleistungLadeeinrichtung is greater than SMALLER_NENNLEISTUNG_LADEEINRICHTUNG
        defaultChargingStationShouldBeFound("nennleistungLadeeinrichtung.greaterThan=" + SMALLER_NENNLEISTUNG_LADEEINRICHTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByArtDerLadeeinrichtungIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where artDerLadeeinrichtung equals to DEFAULT_ART_DER_LADEEINRICHTUNG
        defaultChargingStationShouldBeFound("artDerLadeeinrichtung.equals=" + DEFAULT_ART_DER_LADEEINRICHTUNG);

        // Get all the chargingStationList where artDerLadeeinrichtung equals to UPDATED_ART_DER_LADEEINRICHTUNG
        defaultChargingStationShouldNotBeFound("artDerLadeeinrichtung.equals=" + UPDATED_ART_DER_LADEEINRICHTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByArtDerLadeeinrichtungIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where artDerLadeeinrichtung in DEFAULT_ART_DER_LADEEINRICHTUNG or UPDATED_ART_DER_LADEEINRICHTUNG
        defaultChargingStationShouldBeFound(
            "artDerLadeeinrichtung.in=" + DEFAULT_ART_DER_LADEEINRICHTUNG + "," + UPDATED_ART_DER_LADEEINRICHTUNG
        );

        // Get all the chargingStationList where artDerLadeeinrichtung equals to UPDATED_ART_DER_LADEEINRICHTUNG
        defaultChargingStationShouldNotBeFound("artDerLadeeinrichtung.in=" + UPDATED_ART_DER_LADEEINRICHTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByArtDerLadeeinrichtungIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where artDerLadeeinrichtung is not null
        defaultChargingStationShouldBeFound("artDerLadeeinrichtung.specified=true");

        // Get all the chargingStationList where artDerLadeeinrichtung is null
        defaultChargingStationShouldNotBeFound("artDerLadeeinrichtung.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByArtDerLadeeinrichtungContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where artDerLadeeinrichtung contains DEFAULT_ART_DER_LADEEINRICHTUNG
        defaultChargingStationShouldBeFound("artDerLadeeinrichtung.contains=" + DEFAULT_ART_DER_LADEEINRICHTUNG);

        // Get all the chargingStationList where artDerLadeeinrichtung contains UPDATED_ART_DER_LADEEINRICHTUNG
        defaultChargingStationShouldNotBeFound("artDerLadeeinrichtung.contains=" + UPDATED_ART_DER_LADEEINRICHTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByArtDerLadeeinrichtungNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where artDerLadeeinrichtung does not contain DEFAULT_ART_DER_LADEEINRICHTUNG
        defaultChargingStationShouldNotBeFound("artDerLadeeinrichtung.doesNotContain=" + DEFAULT_ART_DER_LADEEINRICHTUNG);

        // Get all the chargingStationList where artDerLadeeinrichtung does not contain UPDATED_ART_DER_LADEEINRICHTUNG
        defaultChargingStationShouldBeFound("artDerLadeeinrichtung.doesNotContain=" + UPDATED_ART_DER_LADEEINRICHTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByAnzahlLadepunkteIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where anzahlLadepunkte equals to DEFAULT_ANZAHL_LADEPUNKTE
        defaultChargingStationShouldBeFound("anzahlLadepunkte.equals=" + DEFAULT_ANZAHL_LADEPUNKTE);

        // Get all the chargingStationList where anzahlLadepunkte equals to UPDATED_ANZAHL_LADEPUNKTE
        defaultChargingStationShouldNotBeFound("anzahlLadepunkte.equals=" + UPDATED_ANZAHL_LADEPUNKTE);
    }

    @Test
    @Transactional
    void getAllChargingStationsByAnzahlLadepunkteIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where anzahlLadepunkte in DEFAULT_ANZAHL_LADEPUNKTE or UPDATED_ANZAHL_LADEPUNKTE
        defaultChargingStationShouldBeFound("anzahlLadepunkte.in=" + DEFAULT_ANZAHL_LADEPUNKTE + "," + UPDATED_ANZAHL_LADEPUNKTE);

        // Get all the chargingStationList where anzahlLadepunkte equals to UPDATED_ANZAHL_LADEPUNKTE
        defaultChargingStationShouldNotBeFound("anzahlLadepunkte.in=" + UPDATED_ANZAHL_LADEPUNKTE);
    }

    @Test
    @Transactional
    void getAllChargingStationsByAnzahlLadepunkteIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where anzahlLadepunkte is not null
        defaultChargingStationShouldBeFound("anzahlLadepunkte.specified=true");

        // Get all the chargingStationList where anzahlLadepunkte is null
        defaultChargingStationShouldNotBeFound("anzahlLadepunkte.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByAnzahlLadepunkteIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where anzahlLadepunkte is greater than or equal to DEFAULT_ANZAHL_LADEPUNKTE
        defaultChargingStationShouldBeFound("anzahlLadepunkte.greaterThanOrEqual=" + DEFAULT_ANZAHL_LADEPUNKTE);

        // Get all the chargingStationList where anzahlLadepunkte is greater than or equal to UPDATED_ANZAHL_LADEPUNKTE
        defaultChargingStationShouldNotBeFound("anzahlLadepunkte.greaterThanOrEqual=" + UPDATED_ANZAHL_LADEPUNKTE);
    }

    @Test
    @Transactional
    void getAllChargingStationsByAnzahlLadepunkteIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where anzahlLadepunkte is less than or equal to DEFAULT_ANZAHL_LADEPUNKTE
        defaultChargingStationShouldBeFound("anzahlLadepunkte.lessThanOrEqual=" + DEFAULT_ANZAHL_LADEPUNKTE);

        // Get all the chargingStationList where anzahlLadepunkte is less than or equal to SMALLER_ANZAHL_LADEPUNKTE
        defaultChargingStationShouldNotBeFound("anzahlLadepunkte.lessThanOrEqual=" + SMALLER_ANZAHL_LADEPUNKTE);
    }

    @Test
    @Transactional
    void getAllChargingStationsByAnzahlLadepunkteIsLessThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where anzahlLadepunkte is less than DEFAULT_ANZAHL_LADEPUNKTE
        defaultChargingStationShouldNotBeFound("anzahlLadepunkte.lessThan=" + DEFAULT_ANZAHL_LADEPUNKTE);

        // Get all the chargingStationList where anzahlLadepunkte is less than UPDATED_ANZAHL_LADEPUNKTE
        defaultChargingStationShouldBeFound("anzahlLadepunkte.lessThan=" + UPDATED_ANZAHL_LADEPUNKTE);
    }

    @Test
    @Transactional
    void getAllChargingStationsByAnzahlLadepunkteIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where anzahlLadepunkte is greater than DEFAULT_ANZAHL_LADEPUNKTE
        defaultChargingStationShouldNotBeFound("anzahlLadepunkte.greaterThan=" + DEFAULT_ANZAHL_LADEPUNKTE);

        // Get all the chargingStationList where anzahlLadepunkte is greater than SMALLER_ANZAHL_LADEPUNKTE
        defaultChargingStationShouldBeFound("anzahlLadepunkte.greaterThan=" + SMALLER_ANZAHL_LADEPUNKTE);
    }

    @Test
    @Transactional
    void getAllChargingStationsByErsterLadepunktSteckertypenIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ersterLadepunktSteckertypen equals to DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound("ersterLadepunktSteckertypen.equals=" + DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN);

        // Get all the chargingStationList where ersterLadepunktSteckertypen equals to UPDATED_ERSTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("ersterLadepunktSteckertypen.equals=" + UPDATED_ERSTER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByErsterLadepunktSteckertypenIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ersterLadepunktSteckertypen in DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN or UPDATED_ERSTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound(
            "ersterLadepunktSteckertypen.in=" + DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN + "," + UPDATED_ERSTER_LADEPUNKT_STECKERTYPEN
        );

        // Get all the chargingStationList where ersterLadepunktSteckertypen equals to UPDATED_ERSTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("ersterLadepunktSteckertypen.in=" + UPDATED_ERSTER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByErsterLadepunktSteckertypenIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ersterLadepunktSteckertypen is not null
        defaultChargingStationShouldBeFound("ersterLadepunktSteckertypen.specified=true");

        // Get all the chargingStationList where ersterLadepunktSteckertypen is null
        defaultChargingStationShouldNotBeFound("ersterLadepunktSteckertypen.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByErsterLadepunktSteckertypenContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ersterLadepunktSteckertypen contains DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound("ersterLadepunktSteckertypen.contains=" + DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN);

        // Get all the chargingStationList where ersterLadepunktSteckertypen contains UPDATED_ERSTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("ersterLadepunktSteckertypen.contains=" + UPDATED_ERSTER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByErsterLadepunktSteckertypenNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ersterLadepunktSteckertypen does not contain DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("ersterLadepunktSteckertypen.doesNotContain=" + DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN);

        // Get all the chargingStationList where ersterLadepunktSteckertypen does not contain UPDATED_ERSTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound("ersterLadepunktSteckertypen.doesNotContain=" + UPDATED_ERSTER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByErsterLadepunktLeistungIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ersterLadepunktLeistung equals to DEFAULT_ERSTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("ersterLadepunktLeistung.equals=" + DEFAULT_ERSTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where ersterLadepunktLeistung equals to UPDATED_ERSTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("ersterLadepunktLeistung.equals=" + UPDATED_ERSTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByErsterLadepunktLeistungIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ersterLadepunktLeistung in DEFAULT_ERSTER_LADEPUNKT_LEISTUNG or UPDATED_ERSTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound(
            "ersterLadepunktLeistung.in=" + DEFAULT_ERSTER_LADEPUNKT_LEISTUNG + "," + UPDATED_ERSTER_LADEPUNKT_LEISTUNG
        );

        // Get all the chargingStationList where ersterLadepunktLeistung equals to UPDATED_ERSTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("ersterLadepunktLeistung.in=" + UPDATED_ERSTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByErsterLadepunktLeistungIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ersterLadepunktLeistung is not null
        defaultChargingStationShouldBeFound("ersterLadepunktLeistung.specified=true");

        // Get all the chargingStationList where ersterLadepunktLeistung is null
        defaultChargingStationShouldNotBeFound("ersterLadepunktLeistung.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByErsterLadepunktLeistungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ersterLadepunktLeistung is greater than or equal to DEFAULT_ERSTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("ersterLadepunktLeistung.greaterThanOrEqual=" + DEFAULT_ERSTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where ersterLadepunktLeistung is greater than or equal to UPDATED_ERSTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("ersterLadepunktLeistung.greaterThanOrEqual=" + UPDATED_ERSTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByErsterLadepunktLeistungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ersterLadepunktLeistung is less than or equal to DEFAULT_ERSTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("ersterLadepunktLeistung.lessThanOrEqual=" + DEFAULT_ERSTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where ersterLadepunktLeistung is less than or equal to SMALLER_ERSTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("ersterLadepunktLeistung.lessThanOrEqual=" + SMALLER_ERSTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByErsterLadepunktLeistungIsLessThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ersterLadepunktLeistung is less than DEFAULT_ERSTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("ersterLadepunktLeistung.lessThan=" + DEFAULT_ERSTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where ersterLadepunktLeistung is less than UPDATED_ERSTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("ersterLadepunktLeistung.lessThan=" + UPDATED_ERSTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByErsterLadepunktLeistungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where ersterLadepunktLeistung is greater than DEFAULT_ERSTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("ersterLadepunktLeistung.greaterThan=" + DEFAULT_ERSTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where ersterLadepunktLeistung is greater than SMALLER_ERSTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("ersterLadepunktLeistung.greaterThan=" + SMALLER_ERSTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByZweiterLadepunktSteckertypenIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where zweiterLadepunktSteckertypen equals to DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound("zweiterLadepunktSteckertypen.equals=" + DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN);

        // Get all the chargingStationList where zweiterLadepunktSteckertypen equals to UPDATED_ZWEITER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("zweiterLadepunktSteckertypen.equals=" + UPDATED_ZWEITER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByZweiterLadepunktSteckertypenIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where zweiterLadepunktSteckertypen in DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN or UPDATED_ZWEITER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound(
            "zweiterLadepunktSteckertypen.in=" + DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN + "," + UPDATED_ZWEITER_LADEPUNKT_STECKERTYPEN
        );

        // Get all the chargingStationList where zweiterLadepunktSteckertypen equals to UPDATED_ZWEITER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("zweiterLadepunktSteckertypen.in=" + UPDATED_ZWEITER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByZweiterLadepunktSteckertypenIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where zweiterLadepunktSteckertypen is not null
        defaultChargingStationShouldBeFound("zweiterLadepunktSteckertypen.specified=true");

        // Get all the chargingStationList where zweiterLadepunktSteckertypen is null
        defaultChargingStationShouldNotBeFound("zweiterLadepunktSteckertypen.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByZweiterLadepunktSteckertypenContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where zweiterLadepunktSteckertypen contains DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound("zweiterLadepunktSteckertypen.contains=" + DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN);

        // Get all the chargingStationList where zweiterLadepunktSteckertypen contains UPDATED_ZWEITER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("zweiterLadepunktSteckertypen.contains=" + UPDATED_ZWEITER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByZweiterLadepunktSteckertypenNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where zweiterLadepunktSteckertypen does not contain DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("zweiterLadepunktSteckertypen.doesNotContain=" + DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN);

        // Get all the chargingStationList where zweiterLadepunktSteckertypen does not contain UPDATED_ZWEITER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound("zweiterLadepunktSteckertypen.doesNotContain=" + UPDATED_ZWEITER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByZweiterLadepunktLeistungIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where zweiterLadepunktLeistung equals to DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("zweiterLadepunktLeistung.equals=" + DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where zweiterLadepunktLeistung equals to UPDATED_ZWEITER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("zweiterLadepunktLeistung.equals=" + UPDATED_ZWEITER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByZweiterLadepunktLeistungIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where zweiterLadepunktLeistung in DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG or UPDATED_ZWEITER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound(
            "zweiterLadepunktLeistung.in=" + DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG + "," + UPDATED_ZWEITER_LADEPUNKT_LEISTUNG
        );

        // Get all the chargingStationList where zweiterLadepunktLeistung equals to UPDATED_ZWEITER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("zweiterLadepunktLeistung.in=" + UPDATED_ZWEITER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByZweiterLadepunktLeistungIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where zweiterLadepunktLeistung is not null
        defaultChargingStationShouldBeFound("zweiterLadepunktLeistung.specified=true");

        // Get all the chargingStationList where zweiterLadepunktLeistung is null
        defaultChargingStationShouldNotBeFound("zweiterLadepunktLeistung.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByZweiterLadepunktLeistungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where zweiterLadepunktLeistung is greater than or equal to DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("zweiterLadepunktLeistung.greaterThanOrEqual=" + DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where zweiterLadepunktLeistung is greater than or equal to UPDATED_ZWEITER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("zweiterLadepunktLeistung.greaterThanOrEqual=" + UPDATED_ZWEITER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByZweiterLadepunktLeistungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where zweiterLadepunktLeistung is less than or equal to DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("zweiterLadepunktLeistung.lessThanOrEqual=" + DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where zweiterLadepunktLeistung is less than or equal to SMALLER_ZWEITER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("zweiterLadepunktLeistung.lessThanOrEqual=" + SMALLER_ZWEITER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByZweiterLadepunktLeistungIsLessThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where zweiterLadepunktLeistung is less than DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("zweiterLadepunktLeistung.lessThan=" + DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where zweiterLadepunktLeistung is less than UPDATED_ZWEITER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("zweiterLadepunktLeistung.lessThan=" + UPDATED_ZWEITER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByZweiterLadepunktLeistungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where zweiterLadepunktLeistung is greater than DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("zweiterLadepunktLeistung.greaterThan=" + DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where zweiterLadepunktLeistung is greater than SMALLER_ZWEITER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("zweiterLadepunktLeistung.greaterThan=" + SMALLER_ZWEITER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByDritterLadepunktSteckertypenIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where dritterLadepunktSteckertypen equals to DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound("dritterLadepunktSteckertypen.equals=" + DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN);

        // Get all the chargingStationList where dritterLadepunktSteckertypen equals to UPDATED_DRITTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("dritterLadepunktSteckertypen.equals=" + UPDATED_DRITTER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByDritterLadepunktSteckertypenIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where dritterLadepunktSteckertypen in DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN or UPDATED_DRITTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound(
            "dritterLadepunktSteckertypen.in=" + DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN + "," + UPDATED_DRITTER_LADEPUNKT_STECKERTYPEN
        );

        // Get all the chargingStationList where dritterLadepunktSteckertypen equals to UPDATED_DRITTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("dritterLadepunktSteckertypen.in=" + UPDATED_DRITTER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByDritterLadepunktSteckertypenIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where dritterLadepunktSteckertypen is not null
        defaultChargingStationShouldBeFound("dritterLadepunktSteckertypen.specified=true");

        // Get all the chargingStationList where dritterLadepunktSteckertypen is null
        defaultChargingStationShouldNotBeFound("dritterLadepunktSteckertypen.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByDritterLadepunktSteckertypenContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where dritterLadepunktSteckertypen contains DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound("dritterLadepunktSteckertypen.contains=" + DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN);

        // Get all the chargingStationList where dritterLadepunktSteckertypen contains UPDATED_DRITTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("dritterLadepunktSteckertypen.contains=" + UPDATED_DRITTER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByDritterLadepunktSteckertypenNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where dritterLadepunktSteckertypen does not contain DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("dritterLadepunktSteckertypen.doesNotContain=" + DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN);

        // Get all the chargingStationList where dritterLadepunktSteckertypen does not contain UPDATED_DRITTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound("dritterLadepunktSteckertypen.doesNotContain=" + UPDATED_DRITTER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByDritterLadepunktLeistungIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where dritterLadepunktLeistung equals to DEFAULT_DRITTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("dritterLadepunktLeistung.equals=" + DEFAULT_DRITTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where dritterLadepunktLeistung equals to UPDATED_DRITTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("dritterLadepunktLeistung.equals=" + UPDATED_DRITTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByDritterLadepunktLeistungIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where dritterLadepunktLeistung in DEFAULT_DRITTER_LADEPUNKT_LEISTUNG or UPDATED_DRITTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound(
            "dritterLadepunktLeistung.in=" + DEFAULT_DRITTER_LADEPUNKT_LEISTUNG + "," + UPDATED_DRITTER_LADEPUNKT_LEISTUNG
        );

        // Get all the chargingStationList where dritterLadepunktLeistung equals to UPDATED_DRITTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("dritterLadepunktLeistung.in=" + UPDATED_DRITTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByDritterLadepunktLeistungIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where dritterLadepunktLeistung is not null
        defaultChargingStationShouldBeFound("dritterLadepunktLeistung.specified=true");

        // Get all the chargingStationList where dritterLadepunktLeistung is null
        defaultChargingStationShouldNotBeFound("dritterLadepunktLeistung.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByDritterLadepunktLeistungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where dritterLadepunktLeistung is greater than or equal to DEFAULT_DRITTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("dritterLadepunktLeistung.greaterThanOrEqual=" + DEFAULT_DRITTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where dritterLadepunktLeistung is greater than or equal to UPDATED_DRITTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("dritterLadepunktLeistung.greaterThanOrEqual=" + UPDATED_DRITTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByDritterLadepunktLeistungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where dritterLadepunktLeistung is less than or equal to DEFAULT_DRITTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("dritterLadepunktLeistung.lessThanOrEqual=" + DEFAULT_DRITTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where dritterLadepunktLeistung is less than or equal to SMALLER_DRITTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("dritterLadepunktLeistung.lessThanOrEqual=" + SMALLER_DRITTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByDritterLadepunktLeistungIsLessThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where dritterLadepunktLeistung is less than DEFAULT_DRITTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("dritterLadepunktLeistung.lessThan=" + DEFAULT_DRITTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where dritterLadepunktLeistung is less than UPDATED_DRITTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("dritterLadepunktLeistung.lessThan=" + UPDATED_DRITTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByDritterLadepunktLeistungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where dritterLadepunktLeistung is greater than DEFAULT_DRITTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("dritterLadepunktLeistung.greaterThan=" + DEFAULT_DRITTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where dritterLadepunktLeistung is greater than SMALLER_DRITTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("dritterLadepunktLeistung.greaterThan=" + SMALLER_DRITTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByVierterLadepunktSteckertypenIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where vierterLadepunktSteckertypen equals to DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound("vierterLadepunktSteckertypen.equals=" + DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN);

        // Get all the chargingStationList where vierterLadepunktSteckertypen equals to UPDATED_VIERTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("vierterLadepunktSteckertypen.equals=" + UPDATED_VIERTER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByVierterLadepunktSteckertypenIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where vierterLadepunktSteckertypen in DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN or UPDATED_VIERTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound(
            "vierterLadepunktSteckertypen.in=" + DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN + "," + UPDATED_VIERTER_LADEPUNKT_STECKERTYPEN
        );

        // Get all the chargingStationList where vierterLadepunktSteckertypen equals to UPDATED_VIERTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("vierterLadepunktSteckertypen.in=" + UPDATED_VIERTER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByVierterLadepunktSteckertypenIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where vierterLadepunktSteckertypen is not null
        defaultChargingStationShouldBeFound("vierterLadepunktSteckertypen.specified=true");

        // Get all the chargingStationList where vierterLadepunktSteckertypen is null
        defaultChargingStationShouldNotBeFound("vierterLadepunktSteckertypen.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByVierterLadepunktSteckertypenContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where vierterLadepunktSteckertypen contains DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound("vierterLadepunktSteckertypen.contains=" + DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN);

        // Get all the chargingStationList where vierterLadepunktSteckertypen contains UPDATED_VIERTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("vierterLadepunktSteckertypen.contains=" + UPDATED_VIERTER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByVierterLadepunktSteckertypenNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where vierterLadepunktSteckertypen does not contain DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldNotBeFound("vierterLadepunktSteckertypen.doesNotContain=" + DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN);

        // Get all the chargingStationList where vierterLadepunktSteckertypen does not contain UPDATED_VIERTER_LADEPUNKT_STECKERTYPEN
        defaultChargingStationShouldBeFound("vierterLadepunktSteckertypen.doesNotContain=" + UPDATED_VIERTER_LADEPUNKT_STECKERTYPEN);
    }

    @Test
    @Transactional
    void getAllChargingStationsByVierterLadepunktLeistungIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where vierterLadepunktLeistung equals to DEFAULT_VIERTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("vierterLadepunktLeistung.equals=" + DEFAULT_VIERTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where vierterLadepunktLeistung equals to UPDATED_VIERTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("vierterLadepunktLeistung.equals=" + UPDATED_VIERTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByVierterLadepunktLeistungIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where vierterLadepunktLeistung in DEFAULT_VIERTER_LADEPUNKT_LEISTUNG or UPDATED_VIERTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound(
            "vierterLadepunktLeistung.in=" + DEFAULT_VIERTER_LADEPUNKT_LEISTUNG + "," + UPDATED_VIERTER_LADEPUNKT_LEISTUNG
        );

        // Get all the chargingStationList where vierterLadepunktLeistung equals to UPDATED_VIERTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("vierterLadepunktLeistung.in=" + UPDATED_VIERTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByVierterLadepunktLeistungIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where vierterLadepunktLeistung is not null
        defaultChargingStationShouldBeFound("vierterLadepunktLeistung.specified=true");

        // Get all the chargingStationList where vierterLadepunktLeistung is null
        defaultChargingStationShouldNotBeFound("vierterLadepunktLeistung.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByVierterLadepunktLeistungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where vierterLadepunktLeistung is greater than or equal to DEFAULT_VIERTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("vierterLadepunktLeistung.greaterThanOrEqual=" + DEFAULT_VIERTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where vierterLadepunktLeistung is greater than or equal to UPDATED_VIERTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("vierterLadepunktLeistung.greaterThanOrEqual=" + UPDATED_VIERTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByVierterLadepunktLeistungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where vierterLadepunktLeistung is less than or equal to DEFAULT_VIERTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("vierterLadepunktLeistung.lessThanOrEqual=" + DEFAULT_VIERTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where vierterLadepunktLeistung is less than or equal to SMALLER_VIERTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("vierterLadepunktLeistung.lessThanOrEqual=" + SMALLER_VIERTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByVierterLadepunktLeistungIsLessThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where vierterLadepunktLeistung is less than DEFAULT_VIERTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("vierterLadepunktLeistung.lessThan=" + DEFAULT_VIERTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where vierterLadepunktLeistung is less than UPDATED_VIERTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("vierterLadepunktLeistung.lessThan=" + UPDATED_VIERTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByVierterLadepunktLeistungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where vierterLadepunktLeistung is greater than DEFAULT_VIERTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldNotBeFound("vierterLadepunktLeistung.greaterThan=" + DEFAULT_VIERTER_LADEPUNKT_LEISTUNG);

        // Get all the chargingStationList where vierterLadepunktLeistung is greater than SMALLER_VIERTER_LADEPUNKT_LEISTUNG
        defaultChargingStationShouldBeFound("vierterLadepunktLeistung.greaterThan=" + SMALLER_VIERTER_LADEPUNKT_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllChargingStationsByGeometryIsEqualToSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where geometry equals to DEFAULT_GEOMETRY
        defaultChargingStationShouldBeFound("geometry.equals=" + DEFAULT_GEOMETRY);

        // Get all the chargingStationList where geometry equals to UPDATED_GEOMETRY
        defaultChargingStationShouldNotBeFound("geometry.equals=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllChargingStationsByGeometryIsInShouldWork() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where geometry in DEFAULT_GEOMETRY or UPDATED_GEOMETRY
        defaultChargingStationShouldBeFound("geometry.in=" + DEFAULT_GEOMETRY + "," + UPDATED_GEOMETRY);

        // Get all the chargingStationList where geometry equals to UPDATED_GEOMETRY
        defaultChargingStationShouldNotBeFound("geometry.in=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllChargingStationsByGeometryIsNullOrNotNull() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where geometry is not null
        defaultChargingStationShouldBeFound("geometry.specified=true");

        // Get all the chargingStationList where geometry is null
        defaultChargingStationShouldNotBeFound("geometry.specified=false");
    }

    @Test
    @Transactional
    void getAllChargingStationsByGeometryContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where geometry contains DEFAULT_GEOMETRY
        defaultChargingStationShouldBeFound("geometry.contains=" + DEFAULT_GEOMETRY);

        // Get all the chargingStationList where geometry contains UPDATED_GEOMETRY
        defaultChargingStationShouldNotBeFound("geometry.contains=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllChargingStationsByGeometryNotContainsSomething() throws Exception {
        // Initialize the database
        chargingStationRepository.saveAndFlush(chargingStation);

        // Get all the chargingStationList where geometry does not contain DEFAULT_GEOMETRY
        defaultChargingStationShouldNotBeFound("geometry.doesNotContain=" + DEFAULT_GEOMETRY);

        // Get all the chargingStationList where geometry does not contain UPDATED_GEOMETRY
        defaultChargingStationShouldBeFound("geometry.doesNotContain=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllChargingStationsByAdministrationUnitIsEqualToSomething() throws Exception {
        AdministrationUnit administrationUnit;
        if (TestUtil.findAll(em, AdministrationUnit.class).isEmpty()) {
            chargingStationRepository.saveAndFlush(chargingStation);
            administrationUnit = AdministrationUnitResourceIT.createEntity(em);
        } else {
            administrationUnit = TestUtil.findAll(em, AdministrationUnit.class).get(0);
        }
        em.persist(administrationUnit);
        em.flush();
        chargingStation.setAdministrationUnit(administrationUnit);
        chargingStationRepository.saveAndFlush(chargingStation);
        Long administrationUnitId = administrationUnit.getId();

        // Get all the chargingStationList where administrationUnit equals to administrationUnitId
        defaultChargingStationShouldBeFound("administrationUnitId.equals=" + administrationUnitId);

        // Get all the chargingStationList where administrationUnit equals to (administrationUnitId + 1)
        defaultChargingStationShouldNotBeFound("administrationUnitId.equals=" + (administrationUnitId + 1));
    }

    @Test
    @Transactional
    void getAllChargingStationsByStatisticIsEqualToSomething() throws Exception {
        Statistic statistic;
        if (TestUtil.findAll(em, Statistic.class).isEmpty()) {
            chargingStationRepository.saveAndFlush(chargingStation);
            statistic = StatisticResourceIT.createEntity(em);
        } else {
            statistic = TestUtil.findAll(em, Statistic.class).get(0);
        }
        em.persist(statistic);
        em.flush();
        chargingStation.addStatistic(statistic);
        chargingStationRepository.saveAndFlush(chargingStation);
        Long statisticId = statistic.getId();

        // Get all the chargingStationList where statistic equals to statisticId
        defaultChargingStationShouldBeFound("statisticId.equals=" + statisticId);

        // Get all the chargingStationList where statistic equals to (statisticId + 1)
        defaultChargingStationShouldNotBeFound("statisticId.equals=" + (statisticId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultChargingStationShouldBeFound(String filter) throws Exception {
        restChargingStationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chargingStation.getId().intValue())))
            .andExpect(jsonPath("$.[*].betreiber").value(hasItem(DEFAULT_BETREIBER)))
            .andExpect(jsonPath("$.[*].strasse").value(hasItem(DEFAULT_STRASSE)))
            .andExpect(jsonPath("$.[*].hausnummer").value(hasItem(DEFAULT_HAUSNUMMER)))
            .andExpect(jsonPath("$.[*].adresszusatz").value(hasItem(DEFAULT_ADRESSZUSATZ)))
            .andExpect(jsonPath("$.[*].postleitzahl").value(hasItem(DEFAULT_POSTLEITZAHL)))
            .andExpect(jsonPath("$.[*].ort").value(hasItem(DEFAULT_ORT)))
            .andExpect(jsonPath("$.[*].bundesland").value(hasItem(DEFAULT_BUNDESLAND)))
            .andExpect(jsonPath("$.[*].kreisKreisfreieStadt").value(hasItem(DEFAULT_KREIS_KREISFREIE_STADT)))
            .andExpect(jsonPath("$.[*].inbetriebnahmeDatum").value(hasItem(DEFAULT_INBETRIEBNAHME_DATUM.toString())))
            .andExpect(jsonPath("$.[*].nennleistungLadeeinrichtung").value(hasItem(DEFAULT_NENNLEISTUNG_LADEEINRICHTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].artDerLadeeinrichtung").value(hasItem(DEFAULT_ART_DER_LADEEINRICHTUNG)))
            .andExpect(jsonPath("$.[*].anzahlLadepunkte").value(hasItem(DEFAULT_ANZAHL_LADEPUNKTE)))
            .andExpect(jsonPath("$.[*].ersterLadepunktSteckertypen").value(hasItem(DEFAULT_ERSTER_LADEPUNKT_STECKERTYPEN)))
            .andExpect(jsonPath("$.[*].ersterLadepunktLeistung").value(hasItem(DEFAULT_ERSTER_LADEPUNKT_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].zweiterLadepunktSteckertypen").value(hasItem(DEFAULT_ZWEITER_LADEPUNKT_STECKERTYPEN)))
            .andExpect(jsonPath("$.[*].zweiterLadepunktLeistung").value(hasItem(DEFAULT_ZWEITER_LADEPUNKT_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].dritterLadepunktSteckertypen").value(hasItem(DEFAULT_DRITTER_LADEPUNKT_STECKERTYPEN)))
            .andExpect(jsonPath("$.[*].dritterLadepunktLeistung").value(hasItem(DEFAULT_DRITTER_LADEPUNKT_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].vierterLadepunktSteckertypen").value(hasItem(DEFAULT_VIERTER_LADEPUNKT_STECKERTYPEN)))
            .andExpect(jsonPath("$.[*].vierterLadepunktLeistung").value(hasItem(DEFAULT_VIERTER_LADEPUNKT_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].geometry").value(hasItem(DEFAULT_GEOMETRY)));

        // Check, that the count call also returns 1
        restChargingStationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultChargingStationShouldNotBeFound(String filter) throws Exception {
        restChargingStationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restChargingStationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingChargingStation() throws Exception {
        // Get the chargingStation
        restChargingStationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }
}
