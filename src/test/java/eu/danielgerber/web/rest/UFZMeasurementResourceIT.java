package eu.danielgerber.web.rest;

import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eu.danielgerber.IntegrationTest;
import eu.danielgerber.domain.UFZMeasurement;
import eu.danielgerber.domain.UFZRasterPoint;
import eu.danielgerber.repository.UFZMeasurementRepository;
import jakarta.persistence.EntityManager;

import java.time.LocalDate;
import java.time.ZoneId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link UFZMeasurementResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class UFZMeasurementResourceIT {

    private static final Double DEFAULT_SMI_GESAMTBODEN = 1D;
    private static final Double UPDATED_SMI_GESAMTBODEN = 2D;
    private static final Double SMALLER_SMI_GESAMTBODEN = 1D - 1D;

    private static final Double DEFAULT_SMI_OBERBODEN = 1D;
    private static final Double UPDATED_SMI_OBERBODEN = 2D;
    private static final Double SMALLER_SMI_OBERBODEN = 1D - 1D;

    private static final Double DEFAULT_NUTZBARE_FELDKAPAZITAET = 1D;
    private static final Double UPDATED_NUTZBARE_FELDKAPAZITAET = 2D;
    private static final Double SMALLER_NUTZBARE_FELDKAPAZITAET = 1D - 1D;

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE = LocalDate.ofEpochDay(-1L);

    private static final String ENTITY_API_URL = "/api/ufz-measurements";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private UFZMeasurementRepository uFZMeasurementRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUFZMeasurementMockMvc;

    private UFZMeasurement uFZMeasurement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UFZMeasurement createEntity(EntityManager em) {
        UFZMeasurement uFZMeasurement = new UFZMeasurement()
            .smiGesamtboden(DEFAULT_SMI_GESAMTBODEN)
            .smiOberboden(DEFAULT_SMI_OBERBODEN)
            .nutzbareFeldkapazitaet(DEFAULT_NUTZBARE_FELDKAPAZITAET)
            .date(DEFAULT_DATE);
        return uFZMeasurement;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UFZMeasurement createUpdatedEntity(EntityManager em) {
        UFZMeasurement uFZMeasurement = new UFZMeasurement()
            .smiGesamtboden(UPDATED_SMI_GESAMTBODEN)
            .smiOberboden(UPDATED_SMI_OBERBODEN)
            .nutzbareFeldkapazitaet(UPDATED_NUTZBARE_FELDKAPAZITAET)
            .date(UPDATED_DATE);
        return uFZMeasurement;
    }

    @BeforeEach
    public void initTest() {
        uFZMeasurement = createEntity(em);
    }

    @Test
    @Transactional
    void getAllUFZMeasurements() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList
        restUFZMeasurementMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(uFZMeasurement.getId().intValue())))
            .andExpect(jsonPath("$.[*].smiGesamtboden").value(hasItem(DEFAULT_SMI_GESAMTBODEN.doubleValue())))
            .andExpect(jsonPath("$.[*].smiOberboden").value(hasItem(DEFAULT_SMI_OBERBODEN.doubleValue())))
            .andExpect(jsonPath("$.[*].nutzbareFeldkapazitaet").value(hasItem(DEFAULT_NUTZBARE_FELDKAPAZITAET.doubleValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));
    }

    @Test
    @Transactional
    void getUFZMeasurement() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get the uFZMeasurement
        restUFZMeasurementMockMvc
            .perform(get(ENTITY_API_URL_ID, uFZMeasurement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(uFZMeasurement.getId().intValue()))
            .andExpect(jsonPath("$.smiGesamtboden").value(DEFAULT_SMI_GESAMTBODEN.doubleValue()))
            .andExpect(jsonPath("$.smiOberboden").value(DEFAULT_SMI_OBERBODEN.doubleValue()))
            .andExpect(jsonPath("$.nutzbareFeldkapazitaet").value(DEFAULT_NUTZBARE_FELDKAPAZITAET.doubleValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()));
    }

    @Test
    @Transactional
    void getUFZMeasurementsByIdFiltering() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        Long id = uFZMeasurement.getId();

        defaultUFZMeasurementShouldBeFound("id.equals=" + id);
        defaultUFZMeasurementShouldNotBeFound("id.notEquals=" + id);

        defaultUFZMeasurementShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultUFZMeasurementShouldNotBeFound("id.greaterThan=" + id);

        defaultUFZMeasurementShouldBeFound("id.lessThanOrEqual=" + id);
        defaultUFZMeasurementShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiGesamtbodenIsEqualToSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiGesamtboden equals to DEFAULT_SMI_GESAMTBODEN
        defaultUFZMeasurementShouldBeFound("smiGesamtboden.equals=" + DEFAULT_SMI_GESAMTBODEN);

        // Get all the uFZMeasurementList where smiGesamtboden equals to UPDATED_SMI_GESAMTBODEN
        defaultUFZMeasurementShouldNotBeFound("smiGesamtboden.equals=" + UPDATED_SMI_GESAMTBODEN);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiGesamtbodenIsInShouldWork() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiGesamtboden in DEFAULT_SMI_GESAMTBODEN or UPDATED_SMI_GESAMTBODEN
        defaultUFZMeasurementShouldBeFound("smiGesamtboden.in=" + DEFAULT_SMI_GESAMTBODEN + "," + UPDATED_SMI_GESAMTBODEN);

        // Get all the uFZMeasurementList where smiGesamtboden equals to UPDATED_SMI_GESAMTBODEN
        defaultUFZMeasurementShouldNotBeFound("smiGesamtboden.in=" + UPDATED_SMI_GESAMTBODEN);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiGesamtbodenIsNullOrNotNull() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiGesamtboden is not null
        defaultUFZMeasurementShouldBeFound("smiGesamtboden.specified=true");

        // Get all the uFZMeasurementList where smiGesamtboden is null
        defaultUFZMeasurementShouldNotBeFound("smiGesamtboden.specified=false");
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiGesamtbodenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiGesamtboden is greater than or equal to DEFAULT_SMI_GESAMTBODEN
        defaultUFZMeasurementShouldBeFound("smiGesamtboden.greaterThanOrEqual=" + DEFAULT_SMI_GESAMTBODEN);

        // Get all the uFZMeasurementList where smiGesamtboden is greater than or equal to UPDATED_SMI_GESAMTBODEN
        defaultUFZMeasurementShouldNotBeFound("smiGesamtboden.greaterThanOrEqual=" + UPDATED_SMI_GESAMTBODEN);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiGesamtbodenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiGesamtboden is less than or equal to DEFAULT_SMI_GESAMTBODEN
        defaultUFZMeasurementShouldBeFound("smiGesamtboden.lessThanOrEqual=" + DEFAULT_SMI_GESAMTBODEN);

        // Get all the uFZMeasurementList where smiGesamtboden is less than or equal to SMALLER_SMI_GESAMTBODEN
        defaultUFZMeasurementShouldNotBeFound("smiGesamtboden.lessThanOrEqual=" + SMALLER_SMI_GESAMTBODEN);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiGesamtbodenIsLessThanSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiGesamtboden is less than DEFAULT_SMI_GESAMTBODEN
        defaultUFZMeasurementShouldNotBeFound("smiGesamtboden.lessThan=" + DEFAULT_SMI_GESAMTBODEN);

        // Get all the uFZMeasurementList where smiGesamtboden is less than UPDATED_SMI_GESAMTBODEN
        defaultUFZMeasurementShouldBeFound("smiGesamtboden.lessThan=" + UPDATED_SMI_GESAMTBODEN);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiGesamtbodenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiGesamtboden is greater than DEFAULT_SMI_GESAMTBODEN
        defaultUFZMeasurementShouldNotBeFound("smiGesamtboden.greaterThan=" + DEFAULT_SMI_GESAMTBODEN);

        // Get all the uFZMeasurementList where smiGesamtboden is greater than SMALLER_SMI_GESAMTBODEN
        defaultUFZMeasurementShouldBeFound("smiGesamtboden.greaterThan=" + SMALLER_SMI_GESAMTBODEN);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiOberbodenIsEqualToSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiOberboden equals to DEFAULT_SMI_OBERBODEN
        defaultUFZMeasurementShouldBeFound("smiOberboden.equals=" + DEFAULT_SMI_OBERBODEN);

        // Get all the uFZMeasurementList where smiOberboden equals to UPDATED_SMI_OBERBODEN
        defaultUFZMeasurementShouldNotBeFound("smiOberboden.equals=" + UPDATED_SMI_OBERBODEN);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiOberbodenIsInShouldWork() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiOberboden in DEFAULT_SMI_OBERBODEN or UPDATED_SMI_OBERBODEN
        defaultUFZMeasurementShouldBeFound("smiOberboden.in=" + DEFAULT_SMI_OBERBODEN + "," + UPDATED_SMI_OBERBODEN);

        // Get all the uFZMeasurementList where smiOberboden equals to UPDATED_SMI_OBERBODEN
        defaultUFZMeasurementShouldNotBeFound("smiOberboden.in=" + UPDATED_SMI_OBERBODEN);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiOberbodenIsNullOrNotNull() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiOberboden is not null
        defaultUFZMeasurementShouldBeFound("smiOberboden.specified=true");

        // Get all the uFZMeasurementList where smiOberboden is null
        defaultUFZMeasurementShouldNotBeFound("smiOberboden.specified=false");
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiOberbodenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiOberboden is greater than or equal to DEFAULT_SMI_OBERBODEN
        defaultUFZMeasurementShouldBeFound("smiOberboden.greaterThanOrEqual=" + DEFAULT_SMI_OBERBODEN);

        // Get all the uFZMeasurementList where smiOberboden is greater than or equal to UPDATED_SMI_OBERBODEN
        defaultUFZMeasurementShouldNotBeFound("smiOberboden.greaterThanOrEqual=" + UPDATED_SMI_OBERBODEN);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiOberbodenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiOberboden is less than or equal to DEFAULT_SMI_OBERBODEN
        defaultUFZMeasurementShouldBeFound("smiOberboden.lessThanOrEqual=" + DEFAULT_SMI_OBERBODEN);

        // Get all the uFZMeasurementList where smiOberboden is less than or equal to SMALLER_SMI_OBERBODEN
        defaultUFZMeasurementShouldNotBeFound("smiOberboden.lessThanOrEqual=" + SMALLER_SMI_OBERBODEN);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiOberbodenIsLessThanSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiOberboden is less than DEFAULT_SMI_OBERBODEN
        defaultUFZMeasurementShouldNotBeFound("smiOberboden.lessThan=" + DEFAULT_SMI_OBERBODEN);

        // Get all the uFZMeasurementList where smiOberboden is less than UPDATED_SMI_OBERBODEN
        defaultUFZMeasurementShouldBeFound("smiOberboden.lessThan=" + UPDATED_SMI_OBERBODEN);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsBySmiOberbodenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where smiOberboden is greater than DEFAULT_SMI_OBERBODEN
        defaultUFZMeasurementShouldNotBeFound("smiOberboden.greaterThan=" + DEFAULT_SMI_OBERBODEN);

        // Get all the uFZMeasurementList where smiOberboden is greater than SMALLER_SMI_OBERBODEN
        defaultUFZMeasurementShouldBeFound("smiOberboden.greaterThan=" + SMALLER_SMI_OBERBODEN);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByNutzbareFeldkapazitaetIsEqualToSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet equals to DEFAULT_NUTZBARE_FELDKAPAZITAET
        defaultUFZMeasurementShouldBeFound("nutzbareFeldkapazitaet.equals=" + DEFAULT_NUTZBARE_FELDKAPAZITAET);

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet equals to UPDATED_NUTZBARE_FELDKAPAZITAET
        defaultUFZMeasurementShouldNotBeFound("nutzbareFeldkapazitaet.equals=" + UPDATED_NUTZBARE_FELDKAPAZITAET);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByNutzbareFeldkapazitaetIsInShouldWork() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet in DEFAULT_NUTZBARE_FELDKAPAZITAET or UPDATED_NUTZBARE_FELDKAPAZITAET
        defaultUFZMeasurementShouldBeFound(
            "nutzbareFeldkapazitaet.in=" + DEFAULT_NUTZBARE_FELDKAPAZITAET + "," + UPDATED_NUTZBARE_FELDKAPAZITAET
        );

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet equals to UPDATED_NUTZBARE_FELDKAPAZITAET
        defaultUFZMeasurementShouldNotBeFound("nutzbareFeldkapazitaet.in=" + UPDATED_NUTZBARE_FELDKAPAZITAET);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByNutzbareFeldkapazitaetIsNullOrNotNull() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet is not null
        defaultUFZMeasurementShouldBeFound("nutzbareFeldkapazitaet.specified=true");

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet is null
        defaultUFZMeasurementShouldNotBeFound("nutzbareFeldkapazitaet.specified=false");
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByNutzbareFeldkapazitaetIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet is greater than or equal to DEFAULT_NUTZBARE_FELDKAPAZITAET
        defaultUFZMeasurementShouldBeFound("nutzbareFeldkapazitaet.greaterThanOrEqual=" + DEFAULT_NUTZBARE_FELDKAPAZITAET);

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet is greater than or equal to UPDATED_NUTZBARE_FELDKAPAZITAET
        defaultUFZMeasurementShouldNotBeFound("nutzbareFeldkapazitaet.greaterThanOrEqual=" + UPDATED_NUTZBARE_FELDKAPAZITAET);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByNutzbareFeldkapazitaetIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet is less than or equal to DEFAULT_NUTZBARE_FELDKAPAZITAET
        defaultUFZMeasurementShouldBeFound("nutzbareFeldkapazitaet.lessThanOrEqual=" + DEFAULT_NUTZBARE_FELDKAPAZITAET);

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet is less than or equal to SMALLER_NUTZBARE_FELDKAPAZITAET
        defaultUFZMeasurementShouldNotBeFound("nutzbareFeldkapazitaet.lessThanOrEqual=" + SMALLER_NUTZBARE_FELDKAPAZITAET);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByNutzbareFeldkapazitaetIsLessThanSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet is less than DEFAULT_NUTZBARE_FELDKAPAZITAET
        defaultUFZMeasurementShouldNotBeFound("nutzbareFeldkapazitaet.lessThan=" + DEFAULT_NUTZBARE_FELDKAPAZITAET);

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet is less than UPDATED_NUTZBARE_FELDKAPAZITAET
        defaultUFZMeasurementShouldBeFound("nutzbareFeldkapazitaet.lessThan=" + UPDATED_NUTZBARE_FELDKAPAZITAET);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByNutzbareFeldkapazitaetIsGreaterThanSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet is greater than DEFAULT_NUTZBARE_FELDKAPAZITAET
        defaultUFZMeasurementShouldNotBeFound("nutzbareFeldkapazitaet.greaterThan=" + DEFAULT_NUTZBARE_FELDKAPAZITAET);

        // Get all the uFZMeasurementList where nutzbareFeldkapazitaet is greater than SMALLER_NUTZBARE_FELDKAPAZITAET
        defaultUFZMeasurementShouldBeFound("nutzbareFeldkapazitaet.greaterThan=" + SMALLER_NUTZBARE_FELDKAPAZITAET);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByDateIsEqualToSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where date equals to DEFAULT_DATE
        defaultUFZMeasurementShouldBeFound("date.equals=" + DEFAULT_DATE);

        // Get all the uFZMeasurementList where date equals to UPDATED_DATE
        defaultUFZMeasurementShouldNotBeFound("date.equals=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByDateIsInShouldWork() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where date in DEFAULT_DATE or UPDATED_DATE
        defaultUFZMeasurementShouldBeFound("date.in=" + DEFAULT_DATE + "," + UPDATED_DATE);

        // Get all the uFZMeasurementList where date equals to UPDATED_DATE
        defaultUFZMeasurementShouldNotBeFound("date.in=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where date is not null
        defaultUFZMeasurementShouldBeFound("date.specified=true");

        // Get all the uFZMeasurementList where date is null
        defaultUFZMeasurementShouldNotBeFound("date.specified=false");
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where date is greater than or equal to DEFAULT_DATE
        defaultUFZMeasurementShouldBeFound("date.greaterThanOrEqual=" + DEFAULT_DATE);

        // Get all the uFZMeasurementList where date is greater than or equal to UPDATED_DATE
        defaultUFZMeasurementShouldNotBeFound("date.greaterThanOrEqual=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where date is less than or equal to DEFAULT_DATE
        defaultUFZMeasurementShouldBeFound("date.lessThanOrEqual=" + DEFAULT_DATE);

        // Get all the uFZMeasurementList where date is less than or equal to SMALLER_DATE
        defaultUFZMeasurementShouldNotBeFound("date.lessThanOrEqual=" + SMALLER_DATE);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByDateIsLessThanSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where date is less than DEFAULT_DATE
        defaultUFZMeasurementShouldNotBeFound("date.lessThan=" + DEFAULT_DATE);

        // Get all the uFZMeasurementList where date is less than UPDATED_DATE
        defaultUFZMeasurementShouldBeFound("date.lessThan=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);

        // Get all the uFZMeasurementList where date is greater than DEFAULT_DATE
        defaultUFZMeasurementShouldNotBeFound("date.greaterThan=" + DEFAULT_DATE);

        // Get all the uFZMeasurementList where date is greater than SMALLER_DATE
        defaultUFZMeasurementShouldBeFound("date.greaterThan=" + SMALLER_DATE);
    }

    @Test
    @Transactional
    void getAllUFZMeasurementsByUfzRasterPointIsEqualToSomething() throws Exception {
        UFZRasterPoint ufzRasterPoint;
        if (TestUtil.findAll(em, UFZRasterPoint.class).isEmpty()) {
            uFZMeasurementRepository.saveAndFlush(uFZMeasurement);
            ufzRasterPoint = UFZRasterPointResourceIT.createEntity(em);
        } else {
            ufzRasterPoint = TestUtil.findAll(em, UFZRasterPoint.class).get(0);
        }
        em.persist(ufzRasterPoint);
        em.flush();
        uFZMeasurement.setUfzRasterPoint(ufzRasterPoint);
        uFZMeasurementRepository.saveAndFlush(uFZMeasurement);
        Long ufzRasterPointId = ufzRasterPoint.getId();

        // Get all the uFZMeasurementList where ufzRasterPoint equals to ufzRasterPointId
        defaultUFZMeasurementShouldBeFound("ufzRasterPointId.equals=" + ufzRasterPointId);

        // Get all the uFZMeasurementList where ufzRasterPoint equals to (ufzRasterPointId + 1)
        defaultUFZMeasurementShouldNotBeFound("ufzRasterPointId.equals=" + (ufzRasterPointId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUFZMeasurementShouldBeFound(String filter) throws Exception {
        restUFZMeasurementMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(uFZMeasurement.getId().intValue())))
            .andExpect(jsonPath("$.[*].smiGesamtboden").value(hasItem(DEFAULT_SMI_GESAMTBODEN.doubleValue())))
            .andExpect(jsonPath("$.[*].smiOberboden").value(hasItem(DEFAULT_SMI_OBERBODEN.doubleValue())))
            .andExpect(jsonPath("$.[*].nutzbareFeldkapazitaet").value(hasItem(DEFAULT_NUTZBARE_FELDKAPAZITAET.doubleValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())));

        // Check, that the count call also returns 1
        restUFZMeasurementMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUFZMeasurementShouldNotBeFound(String filter) throws Exception {
        restUFZMeasurementMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUFZMeasurementMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingUFZMeasurement() throws Exception {
        // Get the uFZMeasurement
        restUFZMeasurementMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }
}
