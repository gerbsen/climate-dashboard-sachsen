package eu.danielgerber.web.rest;

import static org.geolatte.geom.builder.DSL.g;
import static org.geolatte.geom.builder.DSL.point;
import static org.geolatte.geom.crs.CoordinateReferenceSystems.WGS84;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eu.danielgerber.IntegrationTest;
import eu.danielgerber.domain.UFZMeasurement;
import eu.danielgerber.domain.UFZRasterPoint;
import eu.danielgerber.repository.UFZRasterPointRepository;
import jakarta.persistence.EntityManager;
import org.geolatte.geom.jts.JTS;
import org.locationtech.jts.geom.Geometry;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link UFZRasterPointResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class UFZRasterPointResourceIT {

    private static final Double DEFAULT_LAT = 1D;
    private static final Double UPDATED_LAT = 2D;
    private static final Double SMALLER_LAT = 1D - 1D;

    private static final Double DEFAULT_LNG = 1D;
    private static final Double UPDATED_LNG = 2D;
    private static final Double SMALLER_LNG = 1D - 1D;

    private static final Geometry DEFAULT_GEOMETRY = JTS.to(point(WGS84, g(4.33,53.21)));
    private static final Geometry UPDATED_GEOMETRY = JTS.to(point(WGS84, g(5.33,54.21)));

    private static final String ENTITY_API_URL = "/api/ufz-raster-points";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private UFZRasterPointRepository uFZRasterPointRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUFZRasterPointMockMvc;

    private UFZRasterPoint uFZRasterPoint;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UFZRasterPoint createEntity(EntityManager em) {
        UFZRasterPoint uFZRasterPoint = new UFZRasterPoint().lat(DEFAULT_LAT).lng(DEFAULT_LNG).geometry(DEFAULT_GEOMETRY);
        return uFZRasterPoint;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UFZRasterPoint createUpdatedEntity(EntityManager em) {
        UFZRasterPoint uFZRasterPoint = new UFZRasterPoint().lat(UPDATED_LAT).lng(UPDATED_LNG).geometry(UPDATED_GEOMETRY);
        return uFZRasterPoint;
    }

    @BeforeEach
    public void initTest() {
        uFZRasterPoint = createEntity(em);
    }

    @Test
    @Transactional
    void getAllUFZRasterPoints() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList
        restUFZRasterPointMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(uFZRasterPoint.getId().intValue())))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.doubleValue())))
            .andExpect(jsonPath("$.[*].lng").value(hasItem(DEFAULT_LNG.doubleValue())))
            .andExpect(jsonPath("$.[*].geometry").value(hasItem(DEFAULT_GEOMETRY)));
    }

    @Test
    @Transactional
    void getUFZRasterPoint() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get the uFZRasterPoint
        restUFZRasterPointMockMvc
            .perform(get(ENTITY_API_URL_ID, uFZRasterPoint.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(uFZRasterPoint.getId().intValue()))
            .andExpect(jsonPath("$.lat").value(DEFAULT_LAT.doubleValue()))
            .andExpect(jsonPath("$.lng").value(DEFAULT_LNG.doubleValue()))
            .andExpect(jsonPath("$.geometry").value(DEFAULT_GEOMETRY));
    }

    @Test
    @Transactional
    void getUFZRasterPointsByIdFiltering() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        Long id = uFZRasterPoint.getId();

        defaultUFZRasterPointShouldBeFound("id.equals=" + id);
        defaultUFZRasterPointShouldNotBeFound("id.notEquals=" + id);

        defaultUFZRasterPointShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultUFZRasterPointShouldNotBeFound("id.greaterThan=" + id);

        defaultUFZRasterPointShouldBeFound("id.lessThanOrEqual=" + id);
        defaultUFZRasterPointShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLatIsEqualToSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lat equals to DEFAULT_LAT
        defaultUFZRasterPointShouldBeFound("lat.equals=" + DEFAULT_LAT);

        // Get all the uFZRasterPointList where lat equals to UPDATED_LAT
        defaultUFZRasterPointShouldNotBeFound("lat.equals=" + UPDATED_LAT);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLatIsInShouldWork() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lat in DEFAULT_LAT or UPDATED_LAT
        defaultUFZRasterPointShouldBeFound("lat.in=" + DEFAULT_LAT + "," + UPDATED_LAT);

        // Get all the uFZRasterPointList where lat equals to UPDATED_LAT
        defaultUFZRasterPointShouldNotBeFound("lat.in=" + UPDATED_LAT);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLatIsNullOrNotNull() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lat is not null
        defaultUFZRasterPointShouldBeFound("lat.specified=true");

        // Get all the uFZRasterPointList where lat is null
        defaultUFZRasterPointShouldNotBeFound("lat.specified=false");
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLatIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lat is greater than or equal to DEFAULT_LAT
        defaultUFZRasterPointShouldBeFound("lat.greaterThanOrEqual=" + DEFAULT_LAT);

        // Get all the uFZRasterPointList where lat is greater than or equal to UPDATED_LAT
        defaultUFZRasterPointShouldNotBeFound("lat.greaterThanOrEqual=" + UPDATED_LAT);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLatIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lat is less than or equal to DEFAULT_LAT
        defaultUFZRasterPointShouldBeFound("lat.lessThanOrEqual=" + DEFAULT_LAT);

        // Get all the uFZRasterPointList where lat is less than or equal to SMALLER_LAT
        defaultUFZRasterPointShouldNotBeFound("lat.lessThanOrEqual=" + SMALLER_LAT);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLatIsLessThanSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lat is less than DEFAULT_LAT
        defaultUFZRasterPointShouldNotBeFound("lat.lessThan=" + DEFAULT_LAT);

        // Get all the uFZRasterPointList where lat is less than UPDATED_LAT
        defaultUFZRasterPointShouldBeFound("lat.lessThan=" + UPDATED_LAT);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLatIsGreaterThanSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lat is greater than DEFAULT_LAT
        defaultUFZRasterPointShouldNotBeFound("lat.greaterThan=" + DEFAULT_LAT);

        // Get all the uFZRasterPointList where lat is greater than SMALLER_LAT
        defaultUFZRasterPointShouldBeFound("lat.greaterThan=" + SMALLER_LAT);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLngIsEqualToSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lng equals to DEFAULT_LNG
        defaultUFZRasterPointShouldBeFound("lng.equals=" + DEFAULT_LNG);

        // Get all the uFZRasterPointList where lng equals to UPDATED_LNG
        defaultUFZRasterPointShouldNotBeFound("lng.equals=" + UPDATED_LNG);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLngIsInShouldWork() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lng in DEFAULT_LNG or UPDATED_LNG
        defaultUFZRasterPointShouldBeFound("lng.in=" + DEFAULT_LNG + "," + UPDATED_LNG);

        // Get all the uFZRasterPointList where lng equals to UPDATED_LNG
        defaultUFZRasterPointShouldNotBeFound("lng.in=" + UPDATED_LNG);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLngIsNullOrNotNull() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lng is not null
        defaultUFZRasterPointShouldBeFound("lng.specified=true");

        // Get all the uFZRasterPointList where lng is null
        defaultUFZRasterPointShouldNotBeFound("lng.specified=false");
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLngIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lng is greater than or equal to DEFAULT_LNG
        defaultUFZRasterPointShouldBeFound("lng.greaterThanOrEqual=" + DEFAULT_LNG);

        // Get all the uFZRasterPointList where lng is greater than or equal to UPDATED_LNG
        defaultUFZRasterPointShouldNotBeFound("lng.greaterThanOrEqual=" + UPDATED_LNG);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLngIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lng is less than or equal to DEFAULT_LNG
        defaultUFZRasterPointShouldBeFound("lng.lessThanOrEqual=" + DEFAULT_LNG);

        // Get all the uFZRasterPointList where lng is less than or equal to SMALLER_LNG
        defaultUFZRasterPointShouldNotBeFound("lng.lessThanOrEqual=" + SMALLER_LNG);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLngIsLessThanSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lng is less than DEFAULT_LNG
        defaultUFZRasterPointShouldNotBeFound("lng.lessThan=" + DEFAULT_LNG);

        // Get all the uFZRasterPointList where lng is less than UPDATED_LNG
        defaultUFZRasterPointShouldBeFound("lng.lessThan=" + UPDATED_LNG);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByLngIsGreaterThanSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where lng is greater than DEFAULT_LNG
        defaultUFZRasterPointShouldNotBeFound("lng.greaterThan=" + DEFAULT_LNG);

        // Get all the uFZRasterPointList where lng is greater than SMALLER_LNG
        defaultUFZRasterPointShouldBeFound("lng.greaterThan=" + SMALLER_LNG);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByGeometryIsEqualToSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where geometry equals to DEFAULT_GEOMETRY
        defaultUFZRasterPointShouldBeFound("geometry.equals=" + DEFAULT_GEOMETRY);

        // Get all the uFZRasterPointList where geometry equals to UPDATED_GEOMETRY
        defaultUFZRasterPointShouldNotBeFound("geometry.equals=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByGeometryIsInShouldWork() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where geometry in DEFAULT_GEOMETRY or UPDATED_GEOMETRY
        defaultUFZRasterPointShouldBeFound("geometry.in=" + DEFAULT_GEOMETRY + "," + UPDATED_GEOMETRY);

        // Get all the uFZRasterPointList where geometry equals to UPDATED_GEOMETRY
        defaultUFZRasterPointShouldNotBeFound("geometry.in=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByGeometryIsNullOrNotNull() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where geometry is not null
        defaultUFZRasterPointShouldBeFound("geometry.specified=true");

        // Get all the uFZRasterPointList where geometry is null
        defaultUFZRasterPointShouldNotBeFound("geometry.specified=false");
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByGeometryContainsSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where geometry contains DEFAULT_GEOMETRY
        defaultUFZRasterPointShouldBeFound("geometry.contains=" + DEFAULT_GEOMETRY);

        // Get all the uFZRasterPointList where geometry contains UPDATED_GEOMETRY
        defaultUFZRasterPointShouldNotBeFound("geometry.contains=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByGeometryNotContainsSomething() throws Exception {
        // Initialize the database
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);

        // Get all the uFZRasterPointList where geometry does not contain DEFAULT_GEOMETRY
        defaultUFZRasterPointShouldNotBeFound("geometry.doesNotContain=" + DEFAULT_GEOMETRY);

        // Get all the uFZRasterPointList where geometry does not contain UPDATED_GEOMETRY
        defaultUFZRasterPointShouldBeFound("geometry.doesNotContain=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllUFZRasterPointsByUfzMeasurementIsEqualToSomething() throws Exception {
        UFZMeasurement ufzMeasurement;
        if (TestUtil.findAll(em, UFZMeasurement.class).isEmpty()) {
            uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);
            ufzMeasurement = UFZMeasurementResourceIT.createEntity(em);
        } else {
            ufzMeasurement = TestUtil.findAll(em, UFZMeasurement.class).get(0);
        }
        em.persist(ufzMeasurement);
        em.flush();
        uFZRasterPoint.addUfzMeasurement(ufzMeasurement);
        uFZRasterPointRepository.saveAndFlush(uFZRasterPoint);
        Long ufzMeasurementId = ufzMeasurement.getId();

        // Get all the uFZRasterPointList where ufzMeasurement equals to ufzMeasurementId
        defaultUFZRasterPointShouldBeFound("ufzMeasurementId.equals=" + ufzMeasurementId);

        // Get all the uFZRasterPointList where ufzMeasurement equals to (ufzMeasurementId + 1)
        defaultUFZRasterPointShouldNotBeFound("ufzMeasurementId.equals=" + (ufzMeasurementId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUFZRasterPointShouldBeFound(String filter) throws Exception {
        restUFZRasterPointMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(uFZRasterPoint.getId().intValue())))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.doubleValue())))
            .andExpect(jsonPath("$.[*].lng").value(hasItem(DEFAULT_LNG.doubleValue())))
            .andExpect(jsonPath("$.[*].geometry").value(hasItem(DEFAULT_GEOMETRY)));

        // Check, that the count call also returns 1
        restUFZRasterPointMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUFZRasterPointShouldNotBeFound(String filter) throws Exception {
        restUFZRasterPointMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUFZRasterPointMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingUFZRasterPoint() throws Exception {
        // Get the uFZRasterPoint
        restUFZRasterPointMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }
}
