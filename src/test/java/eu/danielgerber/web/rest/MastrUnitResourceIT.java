package eu.danielgerber.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.geolatte.geom.builder.DSL.g;
import static org.geolatte.geom.builder.DSL.point;
import static org.geolatte.geom.crs.CoordinateReferenceSystems.WGS84;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eu.danielgerber.IntegrationTest;
import eu.danielgerber.domain.AdministrationUnit;
import eu.danielgerber.domain.MastrUnit;
import eu.danielgerber.repository.MastrUnitRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import jakarta.persistence.EntityManager;

import org.geolatte.geom.jts.JTS;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.Geometry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MastrUnitResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MastrUnitResourceIT {

    private static final String DEFAULT_MASTR_NUMMER = "AAAAAAAAAA";
    private static final String UPDATED_MASTR_NUMMER = "BBBBBBBBBB";

    private static final Long DEFAULT_ANLAGEN_BETREIBER_ID = 1L;
    private static final Long UPDATED_ANLAGEN_BETREIBER_ID = 2L;
    private static final Long SMALLER_ANLAGEN_BETREIBER_ID = 1L - 1L;

    private static final Long DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART = 1L;
    private static final Long UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART = 2L;
    private static final Long SMALLER_ANLAGEN_BETREIBER_PERSONEN_ART = 1L - 1L;

    private static final String DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ANLAGEN_BETREIBER_MASKED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER = "AAAAAAAAAA";
    private static final String UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER = "BBBBBBBBBB";

    private static final String DEFAULT_ANLAGEN_BETREIBER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ANLAGEN_BETREIBER_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_BETRIEBS_STATUS_ID = 1;
    private static final Integer UPDATED_BETRIEBS_STATUS_ID = 2;
    private static final Integer SMALLER_BETRIEBS_STATUS_ID = 1 - 1;

    private static final String DEFAULT_BETRIEBS_STATUS_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BETRIEBS_STATUS_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BUNDESLAND = "AAAAAAAAAA";
    private static final String UPDATED_BUNDESLAND = "BBBBBBBBBB";

    private static final Integer DEFAULT_BUNDESLAND_ID = 1;
    private static final Integer UPDATED_BUNDESLAND_ID = 2;
    private static final Integer SMALLER_BUNDESLAND_ID = 1 - 1;

    private static final Integer DEFAULT_LANDKREIS_ID = 1;
    private static final Integer UPDATED_LANDKREIS_ID = 2;
    private static final Integer SMALLER_LANDKREIS_ID = 1 - 1;

    private static final Integer DEFAULT_GEMEINDE_ID = 1;
    private static final Integer UPDATED_GEMEINDE_ID = 2;
    private static final Integer SMALLER_GEMEINDE_ID = 1 - 1;

    private static final LocalDate DEFAULT_DATUM_LETZTE_AKTUALISIERUNG = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATUM_LETZTE_AKTUALISIERUNG = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATUM_LETZTE_AKTUALISIERUNG = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EINHEIT_REGISTRIERUNGS_DATUM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_EINHEIT_REGISTRIERUNGS_DATUM = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_ENDGUELTIGE_STILLLEGUNG_DATUM = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_GEPLANTES_INBETRIEBSNAHME_DATUM = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_INBETRIEBNAHME_DATUM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_INBETRIEBNAHME_DATUM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_INBETRIEBNAHME_DATUM = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_KWK_ANLAGE_INBETRIEBNAHME_DATUM = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_KWK_ANLAGE_REGISTRIERUNGS_DATUM = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_EEG_INBETRIEBNAHME_DATUM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EEG_INBETRIEBNAHME_DATUM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_EEG_INBETRIEBNAHME_DATUM = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_EEG_ANLAGE_REGISTRIERUNGS_DATUM = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_GENEHMIGUNG_DATUM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_GENEHMIGUNG_DATUM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_GENEHMIGUNG_DATUM = LocalDate.ofEpochDay(-1L);

    private static final LocalDate DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_GENEHMIGUNG_REGISTRIERUNGS_DATUM = LocalDate.ofEpochDay(-1L);

    private static final Integer DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN = 1;
    private static final Integer UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN = 2;
    private static final Integer SMALLER_IS_NB_PRUEFUNG_ABGESCHLOSSEN = 1 - 1;

    private static final Boolean DEFAULT_IS_ANONYMISIERT = false;
    private static final Boolean UPDATED_IS_ANONYMISIERT = true;

    private static final Boolean DEFAULT_IS_BUERGER_ENERGIE = false;
    private static final Boolean UPDATED_IS_BUERGER_ENERGIE = true;

    private static final Boolean DEFAULT_IS_EINHEIT_NOTSTROMAGGREGAT = false;
    private static final Boolean UPDATED_IS_EINHEIT_NOTSTROMAGGREGAT = true;

    private static final Boolean DEFAULT_IS_MIETERSTROM_ANGEMELDET = false;
    private static final Boolean UPDATED_IS_MIETERSTROM_ANGEMELDET = true;

    private static final Boolean DEFAULT_IS_WASSERKRAFT_ERTUECHTIGUNG = false;
    private static final Boolean UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG = true;

    private static final Boolean DEFAULT_IS_PILOT_WINDANLAGE = false;
    private static final Boolean UPDATED_IS_PILOT_WINDANLAGE = true;

    private static final Boolean DEFAULT_IS_PROTOTYP_ANLAGE = false;
    private static final Boolean UPDATED_IS_PROTOTYP_ANLAGE = true;

    private static final Double DEFAULT_LAT = 1D;
    private static final Double UPDATED_LAT = 2D;
    private static final Double SMALLER_LAT = 1D - 1D;

    private static final Double DEFAULT_LNG = 1D;
    private static final Double UPDATED_LNG = 2D;
    private static final Double SMALLER_LNG = 1D - 1D;

    private static final String DEFAULT_ORT = "AAAAAAAAAA";
    private static final String UPDATED_ORT = "BBBBBBBBBB";

    private static final Integer DEFAULT_PLZ = 1;
    private static final Integer UPDATED_PLZ = 2;
    private static final Integer SMALLER_PLZ = 1 - 1;

    private static final String DEFAULT_STRASSE = "AAAAAAAAAA";
    private static final String UPDATED_STRASSE = "BBBBBBBBBB";

    private static final String DEFAULT_HAUSNUMMER = "AAAAAAAAAA";
    private static final String UPDATED_HAUSNUMMER = "BBBBBBBBBB";

    private static final String DEFAULT_EINHEITNAME = "AAAAAAAAAA";
    private static final String UPDATED_EINHEITNAME = "BBBBBBBBBB";

    private static final String DEFAULT_FLURSTUECK = "AAAAAAAAAA";
    private static final String UPDATED_FLURSTUECK = "BBBBBBBBBB";

    private static final String DEFAULT_GEMARKUNG = "AAAAAAAAAA";
    private static final String UPDATED_GEMARKUNG = "BBBBBBBBBB";

    private static final String DEFAULT_GEMEINDE = "AAAAAAAAAA";
    private static final String UPDATED_GEMEINDE = "BBBBBBBBBB";

    private static final Integer DEFAULT_LAND_ID = 1;
    private static final Integer UPDATED_LAND_ID = 2;
    private static final Integer SMALLER_LAND_ID = 1 - 1;

    private static final String DEFAULT_LANDKREIS = "AAAAAAAAAA";
    private static final String UPDATED_LANDKREIS = "BBBBBBBBBB";

    private static final Long DEFAULT_AGS = 1L;
    private static final Long UPDATED_AGS = 2L;
    private static final Long SMALLER_AGS = 1L - 1L;

    private static final Long DEFAULT_LOKATION_ID = 1L;
    private static final Long UPDATED_LOKATION_ID = 2L;
    private static final Long SMALLER_LOKATION_ID = 1L - 1L;

    private static final String DEFAULT_LOKATION_MASTR_NR = "AAAAAAAAAA";
    private static final String UPDATED_LOKATION_MASTR_NR = "BBBBBBBBBB";

    private static final String DEFAULT_NETZBETREIBER_ID = "AAAAAAAAAA";
    private static final String UPDATED_NETZBETREIBER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_NETZBETREIBER_MASKED_NAMEN = "AAAAAAAAAA";
    private static final String UPDATED_NETZBETREIBER_MASKED_NAMEN = "BBBBBBBBBB";

    private static final String DEFAULT_NETZBETREIBER_MASTR_NUMMER = "AAAAAAAAAA";
    private static final String UPDATED_NETZBETREIBER_MASTR_NUMMER = "BBBBBBBBBB";

    private static final String DEFAULT_NETZBETREIBER_NAMEN = "AAAAAAAAAA";
    private static final String UPDATED_NETZBETREIBER_NAMEN = "BBBBBBBBBB";

    private static final String DEFAULT_NETZBETREIBER_PERSONEN_ART = "AAAAAAAAAA";
    private static final String UPDATED_NETZBETREIBER_PERSONEN_ART = "BBBBBBBBBB";

    private static final Integer DEFAULT_SYSTEM_STATUS_ID = 1;
    private static final Integer UPDATED_SYSTEM_STATUS_ID = 2;
    private static final Integer SMALLER_SYSTEM_STATUS_ID = 1 - 1;

    private static final String DEFAULT_SYSTEM_STATUS_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SYSTEM_STATUS_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_TYP = 1;
    private static final Integer UPDATED_TYP = 2;
    private static final Integer SMALLER_TYP = 1 - 1;

    private static final String DEFAULT_AKTENZEICHEN_GENEHMIGUNG = "AAAAAAAAAA";
    private static final String UPDATED_AKTENZEICHEN_GENEHMIGUNG = "BBBBBBBBBB";

    private static final Integer DEFAULT_ANZAHL_SOLARMODULE = 1;
    private static final Integer UPDATED_ANZAHL_SOLARMODULE = 2;
    private static final Integer SMALLER_ANZAHL_SOLARMODULE = 1 - 1;

    private static final Integer DEFAULT_BATTERIE_TECHNOLOGIE = 1;
    private static final Integer UPDATED_BATTERIE_TECHNOLOGIE = 2;
    private static final Integer SMALLER_BATTERIE_TECHNOLOGIE = 1 - 1;

    private static final Double DEFAULT_BRUTTO_LEISTUNG = 1D;
    private static final Double UPDATED_BRUTTO_LEISTUNG = 2D;
    private static final Double SMALLER_BRUTTO_LEISTUNG = 1D - 1D;

    private static final Double DEFAULT_EEG_INSTALLIERTE_LEISTUNG = 1D;
    private static final Double UPDATED_EEG_INSTALLIERTE_LEISTUNG = 2D;
    private static final Double SMALLER_EEG_INSTALLIERTE_LEISTUNG = 1D - 1D;

    private static final String DEFAULT_EEG_ANLAGE_MASTR_NUMMER = "AAAAAAAAAA";
    private static final String UPDATED_EEG_ANLAGE_MASTR_NUMMER = "BBBBBBBBBB";

    private static final String DEFAULT_EEG_ANLAGEN_SCHLUESSEL = "AAAAAAAAAA";
    private static final String UPDATED_EEG_ANLAGEN_SCHLUESSEL = "BBBBBBBBBB";

    private static final String DEFAULT_EEG_ZUSCHLAG = "AAAAAAAAAA";
    private static final String UPDATED_EEG_ZUSCHLAG = "BBBBBBBBBB";

    private static final String DEFAULT_ZUSCHLAGS_NUMMERN = "AAAAAAAAAA";
    private static final String UPDATED_ZUSCHLAGS_NUMMERN = "BBBBBBBBBB";

    private static final Integer DEFAULT_ENERGIE_TRAEGER_ID = 1;
    private static final Integer UPDATED_ENERGIE_TRAEGER_ID = 2;
    private static final Integer SMALLER_ENERGIE_TRAEGER_ID = 1 - 1;

    private static final String DEFAULT_ENERGIE_TRAEGER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ENERGIE_TRAEGER_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_GEMEINSAMER_WECHSELRICHTER = 1;
    private static final Integer UPDATED_GEMEINSAMER_WECHSELRICHTER = 2;
    private static final Integer SMALLER_GEMEINSAMER_WECHSELRICHTER = 1 - 1;

    private static final String DEFAULT_GENEHMIGUNG_BEHOERDE = "AAAAAAAAAA";
    private static final String UPDATED_GENEHMIGUNG_BEHOERDE = "BBBBBBBBBB";

    private static final String DEFAULT_GENEHMIGUNGS_MASTR_NUMMER = "AAAAAAAAAA";
    private static final String UPDATED_GENEHMIGUNGS_MASTR_NUMMER = "BBBBBBBBBB";

    private static final String DEFAULT_GRUPPIERUNGS_OBJEKTE = "AAAAAAAAAA";
    private static final String UPDATED_GRUPPIERUNGS_OBJEKTE = "BBBBBBBBBB";

    private static final String DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS = "AAAAAAAAAA";
    private static final String UPDATED_GRUPPIERUNGS_OBJEKTE_IDS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_HAT_FLEXIBILITAETS_PRAEMIE = false;
    private static final Boolean UPDATED_HAT_FLEXIBILITAETS_PRAEMIE = true;

    private static final Integer DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE = 1;
    private static final Integer UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE = 2;
    private static final Integer SMALLER_HAUPT_AUSRICHTUNG_SOLARMODULE = 1 - 1;

    private static final String DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG = "AAAAAAAAAA";
    private static final String UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG = "BBBBBBBBBB";

    private static final Integer DEFAULT_HAUPT_BRENNSTOFF_ID = 1;
    private static final Integer UPDATED_HAUPT_BRENNSTOFF_ID = 2;
    private static final Integer SMALLER_HAUPT_BRENNSTOFF_ID = 1 - 1;

    private static final String DEFAULT_HAUPT_BRENNSTOFF_NAMEN = "AAAAAAAAAA";
    private static final String UPDATED_HAUPT_BRENNSTOFF_NAMEN = "BBBBBBBBBB";

    private static final Integer DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE = 1;
    private static final Integer UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE = 2;
    private static final Integer SMALLER_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE = 1 - 1;

    private static final Integer DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID = 1;
    private static final Integer UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID = 2;
    private static final Integer SMALLER_HERSTELLER_WINDENERGIEANLAGE_ID = 1 - 1;

    private static final String DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG = "AAAAAAAAAA";
    private static final String UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG = "BBBBBBBBBB";

    private static final Double DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG = 1D;
    private static final Double UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG = 2D;
    private static final Double SMALLER_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG = 1D - 1D;

    private static final String DEFAULT_KWK_ANLAGE_MASTR_NUMMER = "AAAAAAAAAA";
    private static final String UPDATED_KWK_ANLAGE_MASTR_NUMMER = "BBBBBBBBBB";

    private static final String DEFAULT_KWK_ZUSCHLAG = "AAAAAAAAAA";
    private static final String UPDATED_KWK_ZUSCHLAG = "BBBBBBBBBB";

    private static final Integer DEFAULT_LAGE_EINHEIT = 1;
    private static final Integer UPDATED_LAGE_EINHEIT = 2;
    private static final Integer SMALLER_LAGE_EINHEIT = 1 - 1;

    private static final String DEFAULT_LAGE_EINHEIT_BEZEICHNUNG = "AAAAAAAAAA";
    private static final String UPDATED_LAGE_EINHEIT_BEZEICHNUNG = "BBBBBBBBBB";

    private static final Integer DEFAULT_LEISTUNGS_BEGRENZUNG = 1;
    private static final Integer UPDATED_LEISTUNGS_BEGRENZUNG = 2;
    private static final Integer SMALLER_LEISTUNGS_BEGRENZUNG = 1 - 1;

    private static final Double DEFAULT_NABENHOEHE_WINDENERGIEANLAGE = 1D;
    private static final Double UPDATED_NABENHOEHE_WINDENERGIEANLAGE = 2D;
    private static final Double SMALLER_NABENHOEHE_WINDENERGIEANLAGE = 1D - 1D;

    private static final Double DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE = 1D;
    private static final Double UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE = 2D;
    private static final Double SMALLER_ROTOR_DURCHMESSER_WINDENERGIEANLAGE = 1D - 1D;

    private static final Double DEFAULT_NETTO_NENN_LEISTUNG = 1D;
    private static final Double UPDATED_NETTO_NENN_LEISTUNG = 2D;
    private static final Double SMALLER_NETTO_NENN_LEISTUNG = 1D - 1D;

    private static final Double DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET = 1D;
    private static final Double UPDATED_NUTZBARE_SPEICHER_KAPAZITAET = 2D;
    private static final Double SMALLER_NUTZBARE_SPEICHER_KAPAZITAET = 1D - 1D;

    private static final Integer DEFAULT_NUTZUNGS_BEREICH_GEBSA = 1;
    private static final Integer UPDATED_NUTZUNGS_BEREICH_GEBSA = 2;
    private static final Integer SMALLER_NUTZUNGS_BEREICH_GEBSA = 1 - 1;

    private static final String DEFAULT_STANDORT_ANONYMISIERT = "AAAAAAAAAA";
    private static final String UPDATED_STANDORT_ANONYMISIERT = "BBBBBBBBBB";

    private static final String DEFAULT_SPANNUNGS_EBENEN_ID = "AAAAAAAAAA";
    private static final String UPDATED_SPANNUNGS_EBENEN_ID = "BBBBBBBBBB";

    private static final String DEFAULT_SPANNUNGS_EBENEN_NAMEN = "AAAAAAAAAA";
    private static final String UPDATED_SPANNUNGS_EBENEN_NAMEN = "BBBBBBBBBB";

    private static final String DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER = "AAAAAAAAAA";
    private static final String UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER = "BBBBBBBBBB";

    private static final Integer DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID = 1;
    private static final Integer UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID = 2;
    private static final Integer SMALLER_TECHNOLOGIE_STROMERZEUGUNG_ID = 1 - 1;

    private static final String DEFAULT_TECHNOLOGIE_STROMERZEUGUNG = "AAAAAAAAAA";
    private static final String UPDATED_TECHNOLOGIE_STROMERZEUGUNG = "BBBBBBBBBB";

    private static final Double DEFAULT_THERMISCHE_NUTZ_LEISTUNG = 1D;
    private static final Double UPDATED_THERMISCHE_NUTZ_LEISTUNG = 2D;
    private static final Double SMALLER_THERMISCHE_NUTZ_LEISTUNG = 1D - 1D;

    private static final String DEFAULT_TYPEN_BEZEICHNUNG = "AAAAAAAAAA";
    private static final String UPDATED_TYPEN_BEZEICHNUNG = "BBBBBBBBBB";

    private static final Integer DEFAULT_VOLL_TEIL_EINSPEISUNG = 1;
    private static final Integer UPDATED_VOLL_TEIL_EINSPEISUNG = 2;
    private static final Integer SMALLER_VOLL_TEIL_EINSPEISUNG = 1 - 1;

    private static final String DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG = "AAAAAAAAAA";
    private static final String UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG = "BBBBBBBBBB";

    private static final String DEFAULT_WINDPARK_NAME = "AAAAAAAAAA";
    private static final String UPDATED_WINDPARK_NAME = "BBBBBBBBBB";

    private static final Geometry DEFAULT_GEOMETRY = JTS.to(point(WGS84,g(4.33,53.21)));
    private static final Geometry UPDATED_GEOMETRY = JTS.to(point(WGS84,g(5.33,54.21)));

    private static final String ENTITY_API_URL = "/api/mastr-units";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MastrUnitRepository mastrUnitRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMastrUnitMockMvc;

    private MastrUnit mastrUnit;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MastrUnit createEntity(EntityManager em) {
        MastrUnit mastrUnit = new MastrUnit()
            .mastrNummer(DEFAULT_MASTR_NUMMER)
            .anlagenBetreiberId(DEFAULT_ANLAGEN_BETREIBER_ID)
            .anlagenBetreiberPersonenArt(DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART)
            .anlagenBetreiberMaskedName(DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME)
            .anlagenBetreiberMastrNummer(DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER)
            .anlagenBetreiberName(DEFAULT_ANLAGEN_BETREIBER_NAME)
            .betriebsStatusId(DEFAULT_BETRIEBS_STATUS_ID)
            .betriebsStatusName(DEFAULT_BETRIEBS_STATUS_NAME)
            .bundesland(DEFAULT_BUNDESLAND)
            .bundeslandId(DEFAULT_BUNDESLAND_ID)
            .landkreisId(DEFAULT_LANDKREIS_ID)
            .gemeindeId(DEFAULT_GEMEINDE_ID)
            .datumLetzteAktualisierung(DEFAULT_DATUM_LETZTE_AKTUALISIERUNG)
            .einheitRegistrierungsDatum(DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM)
            .endgueltigeStilllegungDatum(DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM)
            .geplantesInbetriebsnahmeDatum(DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM)
            .inbetriebnahmeDatum(DEFAULT_INBETRIEBNAHME_DATUM)
            .kwkAnlageInbetriebnahmeDatum(DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM)
            .kwkAnlageRegistrierungsDatum(DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM)
            .eegInbetriebnahmeDatum(DEFAULT_EEG_INBETRIEBNAHME_DATUM)
            .eegAnlageRegistrierungsDatum(DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM)
            .genehmigungDatum(DEFAULT_GENEHMIGUNG_DATUM)
            .genehmigungRegistrierungsDatum(DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM)
            .isNbPruefungAbgeschlossen(DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN)
            .isAnonymisiert(DEFAULT_IS_ANONYMISIERT)
            .isBuergerEnergie(DEFAULT_IS_BUERGER_ENERGIE)
            .isEinheitNotstromaggregat(DEFAULT_IS_EINHEIT_NOTSTROMAGGREGAT)
            .isMieterstromAngemeldet(DEFAULT_IS_MIETERSTROM_ANGEMELDET)
            .isWasserkraftErtuechtigung(DEFAULT_IS_WASSERKRAFT_ERTUECHTIGUNG)
            .isPilotWindanlage(DEFAULT_IS_PILOT_WINDANLAGE)
            .isPrototypAnlage(DEFAULT_IS_PROTOTYP_ANLAGE)
            .lat(DEFAULT_LAT)
            .lng(DEFAULT_LNG)
            .ort(DEFAULT_ORT)
            .plz(DEFAULT_PLZ)
            .strasse(DEFAULT_STRASSE)
            .hausnummer(DEFAULT_HAUSNUMMER)
            .einheitname(DEFAULT_EINHEITNAME)
            .flurstueck(DEFAULT_FLURSTUECK)
            .gemarkung(DEFAULT_GEMARKUNG)
            .gemeinde(DEFAULT_GEMEINDE)
            .landId(DEFAULT_LAND_ID)
            .landkreis(DEFAULT_LANDKREIS)
            .ags(DEFAULT_AGS)
            .lokationId(DEFAULT_LOKATION_ID)
            .lokationMastrNr(DEFAULT_LOKATION_MASTR_NR)
            .netzbetreiberId(DEFAULT_NETZBETREIBER_ID)
            .netzbetreiberMaskedNamen(DEFAULT_NETZBETREIBER_MASKED_NAMEN)
            .netzbetreiberMastrNummer(DEFAULT_NETZBETREIBER_MASTR_NUMMER)
            .netzbetreiberNamen(DEFAULT_NETZBETREIBER_NAMEN)
            .netzbetreiberPersonenArt(DEFAULT_NETZBETREIBER_PERSONEN_ART)
            .systemStatusId(DEFAULT_SYSTEM_STATUS_ID)
            .systemStatusName(DEFAULT_SYSTEM_STATUS_NAME)
            .typ(DEFAULT_TYP)
            .aktenzeichenGenehmigung(DEFAULT_AKTENZEICHEN_GENEHMIGUNG)
            .anzahlSolarmodule(DEFAULT_ANZAHL_SOLARMODULE)
            .batterieTechnologie(DEFAULT_BATTERIE_TECHNOLOGIE)
            .bruttoLeistung(DEFAULT_BRUTTO_LEISTUNG)
            .eegInstallierteLeistung(DEFAULT_EEG_INSTALLIERTE_LEISTUNG)
            .eegAnlageMastrNummer(DEFAULT_EEG_ANLAGE_MASTR_NUMMER)
            .eegAnlagenSchluessel(DEFAULT_EEG_ANLAGEN_SCHLUESSEL)
            .eegZuschlag(DEFAULT_EEG_ZUSCHLAG)
            .zuschlagsNummern(DEFAULT_ZUSCHLAGS_NUMMERN)
            .energieTraegerId(DEFAULT_ENERGIE_TRAEGER_ID)
            .energieTraegerName(DEFAULT_ENERGIE_TRAEGER_NAME)
            .gemeinsamerWechselrichter(DEFAULT_GEMEINSAMER_WECHSELRICHTER)
            .genehmigungBehoerde(DEFAULT_GENEHMIGUNG_BEHOERDE)
            .genehmigungsMastrNummer(DEFAULT_GENEHMIGUNGS_MASTR_NUMMER)
            .gruppierungsObjekte(DEFAULT_GRUPPIERUNGS_OBJEKTE)
            .gruppierungsObjekteIds(DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS)
            .hatFlexibilitaetsPraemie(DEFAULT_HAT_FLEXIBILITAETS_PRAEMIE)
            .hauptAusrichtungSolarmodule(DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE)
            .hauptAusrichtungSolarmoduleBezeichnung(DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG)
            .hauptBrennstoffId(DEFAULT_HAUPT_BRENNSTOFF_ID)
            .hauptBrennstoffNamen(DEFAULT_HAUPT_BRENNSTOFF_NAMEN)
            .hauptNeigungswinkelSolarModule(DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE)
            .herstellerWindenergieanlageId(DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID)
            .herstellerWindenergieanlageBezeichnung(DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG)
            .kwkAnlageElektrischeLeistung(DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG)
            .kwkAnlageMastrNummer(DEFAULT_KWK_ANLAGE_MASTR_NUMMER)
            .kwkZuschlag(DEFAULT_KWK_ZUSCHLAG)
            .lageEinheit(DEFAULT_LAGE_EINHEIT)
            .lageEinheitBezeichnung(DEFAULT_LAGE_EINHEIT_BEZEICHNUNG)
            .leistungsBegrenzung(DEFAULT_LEISTUNGS_BEGRENZUNG)
            .nabenhoeheWindenergieanlage(DEFAULT_NABENHOEHE_WINDENERGIEANLAGE)
            .rotorDurchmesserWindenergieanlage(DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE)
            .nettoNennLeistung(DEFAULT_NETTO_NENN_LEISTUNG)
            .nutzbareSpeicherKapazitaet(DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET)
            .nutzungsBereichGebsa(DEFAULT_NUTZUNGS_BEREICH_GEBSA)
            .standortAnonymisiert(DEFAULT_STANDORT_ANONYMISIERT)
            .spannungsEbenenId(DEFAULT_SPANNUNGS_EBENEN_ID)
            .spannungsEbenenNamen(DEFAULT_SPANNUNGS_EBENEN_NAMEN)
            .speicherEinheitMastrNummer(DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER)
            .technologieStromerzeugungId(DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID)
            .technologieStromerzeugung(DEFAULT_TECHNOLOGIE_STROMERZEUGUNG)
            .thermischeNutzLeistung(DEFAULT_THERMISCHE_NUTZ_LEISTUNG)
            .typenBezeichnung(DEFAULT_TYPEN_BEZEICHNUNG)
            .vollTeilEinspeisung(DEFAULT_VOLL_TEIL_EINSPEISUNG)
            .vollTeilEinspeisungBezeichnung(DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG)
            .windparkName(DEFAULT_WINDPARK_NAME)
            .geometry(DEFAULT_GEOMETRY);
        return mastrUnit;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MastrUnit createUpdatedEntity(EntityManager em) {
        MastrUnit mastrUnit = new MastrUnit()
            .mastrNummer(UPDATED_MASTR_NUMMER)
            .anlagenBetreiberId(UPDATED_ANLAGEN_BETREIBER_ID)
            .anlagenBetreiberPersonenArt(UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART)
            .anlagenBetreiberMaskedName(UPDATED_ANLAGEN_BETREIBER_MASKED_NAME)
            .anlagenBetreiberMastrNummer(UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER)
            .anlagenBetreiberName(UPDATED_ANLAGEN_BETREIBER_NAME)
            .betriebsStatusId(UPDATED_BETRIEBS_STATUS_ID)
            .betriebsStatusName(UPDATED_BETRIEBS_STATUS_NAME)
            .bundesland(UPDATED_BUNDESLAND)
            .bundeslandId(UPDATED_BUNDESLAND_ID)
            .landkreisId(UPDATED_LANDKREIS_ID)
            .gemeindeId(UPDATED_GEMEINDE_ID)
            .datumLetzteAktualisierung(UPDATED_DATUM_LETZTE_AKTUALISIERUNG)
            .einheitRegistrierungsDatum(UPDATED_EINHEIT_REGISTRIERUNGS_DATUM)
            .endgueltigeStilllegungDatum(UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM)
            .geplantesInbetriebsnahmeDatum(UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM)
            .inbetriebnahmeDatum(UPDATED_INBETRIEBNAHME_DATUM)
            .kwkAnlageInbetriebnahmeDatum(UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM)
            .kwkAnlageRegistrierungsDatum(UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM)
            .eegInbetriebnahmeDatum(UPDATED_EEG_INBETRIEBNAHME_DATUM)
            .eegAnlageRegistrierungsDatum(UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM)
            .genehmigungDatum(UPDATED_GENEHMIGUNG_DATUM)
            .genehmigungRegistrierungsDatum(UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM)
            .isNbPruefungAbgeschlossen(UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN)
            .isAnonymisiert(UPDATED_IS_ANONYMISIERT)
            .isBuergerEnergie(UPDATED_IS_BUERGER_ENERGIE)
            .isEinheitNotstromaggregat(UPDATED_IS_EINHEIT_NOTSTROMAGGREGAT)
            .isMieterstromAngemeldet(UPDATED_IS_MIETERSTROM_ANGEMELDET)
            .isWasserkraftErtuechtigung(UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG)
            .isPilotWindanlage(UPDATED_IS_PILOT_WINDANLAGE)
            .isPrototypAnlage(UPDATED_IS_PROTOTYP_ANLAGE)
            .lat(UPDATED_LAT)
            .lng(UPDATED_LNG)
            .ort(UPDATED_ORT)
            .plz(UPDATED_PLZ)
            .strasse(UPDATED_STRASSE)
            .hausnummer(UPDATED_HAUSNUMMER)
            .einheitname(UPDATED_EINHEITNAME)
            .flurstueck(UPDATED_FLURSTUECK)
            .gemarkung(UPDATED_GEMARKUNG)
            .gemeinde(UPDATED_GEMEINDE)
            .landId(UPDATED_LAND_ID)
            .landkreis(UPDATED_LANDKREIS)
            .ags(UPDATED_AGS)
            .lokationId(UPDATED_LOKATION_ID)
            .lokationMastrNr(UPDATED_LOKATION_MASTR_NR)
            .netzbetreiberId(UPDATED_NETZBETREIBER_ID)
            .netzbetreiberMaskedNamen(UPDATED_NETZBETREIBER_MASKED_NAMEN)
            .netzbetreiberMastrNummer(UPDATED_NETZBETREIBER_MASTR_NUMMER)
            .netzbetreiberNamen(UPDATED_NETZBETREIBER_NAMEN)
            .netzbetreiberPersonenArt(UPDATED_NETZBETREIBER_PERSONEN_ART)
            .systemStatusId(UPDATED_SYSTEM_STATUS_ID)
            .systemStatusName(UPDATED_SYSTEM_STATUS_NAME)
            .typ(UPDATED_TYP)
            .aktenzeichenGenehmigung(UPDATED_AKTENZEICHEN_GENEHMIGUNG)
            .anzahlSolarmodule(UPDATED_ANZAHL_SOLARMODULE)
            .batterieTechnologie(UPDATED_BATTERIE_TECHNOLOGIE)
            .bruttoLeistung(UPDATED_BRUTTO_LEISTUNG)
            .eegInstallierteLeistung(UPDATED_EEG_INSTALLIERTE_LEISTUNG)
            .eegAnlageMastrNummer(UPDATED_EEG_ANLAGE_MASTR_NUMMER)
            .eegAnlagenSchluessel(UPDATED_EEG_ANLAGEN_SCHLUESSEL)
            .eegZuschlag(UPDATED_EEG_ZUSCHLAG)
            .zuschlagsNummern(UPDATED_ZUSCHLAGS_NUMMERN)
            .energieTraegerId(UPDATED_ENERGIE_TRAEGER_ID)
            .energieTraegerName(UPDATED_ENERGIE_TRAEGER_NAME)
            .gemeinsamerWechselrichter(UPDATED_GEMEINSAMER_WECHSELRICHTER)
            .genehmigungBehoerde(UPDATED_GENEHMIGUNG_BEHOERDE)
            .genehmigungsMastrNummer(UPDATED_GENEHMIGUNGS_MASTR_NUMMER)
            .gruppierungsObjekte(UPDATED_GRUPPIERUNGS_OBJEKTE)
            .gruppierungsObjekteIds(UPDATED_GRUPPIERUNGS_OBJEKTE_IDS)
            .hatFlexibilitaetsPraemie(UPDATED_HAT_FLEXIBILITAETS_PRAEMIE)
            .hauptAusrichtungSolarmodule(UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE)
            .hauptAusrichtungSolarmoduleBezeichnung(UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG)
            .hauptBrennstoffId(UPDATED_HAUPT_BRENNSTOFF_ID)
            .hauptBrennstoffNamen(UPDATED_HAUPT_BRENNSTOFF_NAMEN)
            .hauptNeigungswinkelSolarModule(UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE)
            .herstellerWindenergieanlageId(UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID)
            .herstellerWindenergieanlageBezeichnung(UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG)
            .kwkAnlageElektrischeLeistung(UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG)
            .kwkAnlageMastrNummer(UPDATED_KWK_ANLAGE_MASTR_NUMMER)
            .kwkZuschlag(UPDATED_KWK_ZUSCHLAG)
            .lageEinheit(UPDATED_LAGE_EINHEIT)
            .lageEinheitBezeichnung(UPDATED_LAGE_EINHEIT_BEZEICHNUNG)
            .leistungsBegrenzung(UPDATED_LEISTUNGS_BEGRENZUNG)
            .nabenhoeheWindenergieanlage(UPDATED_NABENHOEHE_WINDENERGIEANLAGE)
            .rotorDurchmesserWindenergieanlage(UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE)
            .nettoNennLeistung(UPDATED_NETTO_NENN_LEISTUNG)
            .nutzbareSpeicherKapazitaet(UPDATED_NUTZBARE_SPEICHER_KAPAZITAET)
            .nutzungsBereichGebsa(UPDATED_NUTZUNGS_BEREICH_GEBSA)
            .standortAnonymisiert(UPDATED_STANDORT_ANONYMISIERT)
            .spannungsEbenenId(UPDATED_SPANNUNGS_EBENEN_ID)
            .spannungsEbenenNamen(UPDATED_SPANNUNGS_EBENEN_NAMEN)
            .speicherEinheitMastrNummer(UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER)
            .technologieStromerzeugungId(UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID)
            .technologieStromerzeugung(UPDATED_TECHNOLOGIE_STROMERZEUGUNG)
            .thermischeNutzLeistung(UPDATED_THERMISCHE_NUTZ_LEISTUNG)
            .typenBezeichnung(UPDATED_TYPEN_BEZEICHNUNG)
            .vollTeilEinspeisung(UPDATED_VOLL_TEIL_EINSPEISUNG)
            .vollTeilEinspeisungBezeichnung(UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG)
            .windparkName(UPDATED_WINDPARK_NAME)
            .geometry(UPDATED_GEOMETRY);
        return mastrUnit;
    }

    @BeforeEach
    public void initTest() {
        mastrUnit = createEntity(em);
    }

    @Test
    @Transactional
    void createMastrUnit() throws Exception {
        int databaseSizeBeforeCreate = mastrUnitRepository.findAll().size();
        // Create the MastrUnit
        restMastrUnitMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mastrUnit)))
            .andExpect(status().isCreated());

        // Validate the MastrUnit in the database
        List<MastrUnit> mastrUnitList = mastrUnitRepository.findAll();
        assertThat(mastrUnitList).hasSize(databaseSizeBeforeCreate + 1);
        MastrUnit testMastrUnit = mastrUnitList.get(mastrUnitList.size() - 1);
        assertThat(testMastrUnit.getMastrNummer()).isEqualTo(DEFAULT_MASTR_NUMMER);
        assertThat(testMastrUnit.getAnlagenBetreiberId()).isEqualTo(DEFAULT_ANLAGEN_BETREIBER_ID);
        assertThat(testMastrUnit.getAnlagenBetreiberPersonenArt()).isEqualTo(DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART);
        assertThat(testMastrUnit.getAnlagenBetreiberMaskedName()).isEqualTo(DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME);
        assertThat(testMastrUnit.getAnlagenBetreiberMastrNummer()).isEqualTo(DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER);
        assertThat(testMastrUnit.getAnlagenBetreiberName()).isEqualTo(DEFAULT_ANLAGEN_BETREIBER_NAME);
        assertThat(testMastrUnit.getBetriebsStatusId()).isEqualTo(DEFAULT_BETRIEBS_STATUS_ID);
        assertThat(testMastrUnit.getBetriebsStatusName()).isEqualTo(DEFAULT_BETRIEBS_STATUS_NAME);
        assertThat(testMastrUnit.getBundesland()).isEqualTo(DEFAULT_BUNDESLAND);
        assertThat(testMastrUnit.getBundeslandId()).isEqualTo(DEFAULT_BUNDESLAND_ID);
        assertThat(testMastrUnit.getLandkreisId()).isEqualTo(DEFAULT_LANDKREIS_ID);
        assertThat(testMastrUnit.getGemeindeId()).isEqualTo(DEFAULT_GEMEINDE_ID);
        assertThat(testMastrUnit.getDatumLetzteAktualisierung()).isEqualTo(DEFAULT_DATUM_LETZTE_AKTUALISIERUNG);
        assertThat(testMastrUnit.getEinheitRegistrierungsDatum()).isEqualTo(DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getEndgueltigeStilllegungDatum()).isEqualTo(DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM);
        assertThat(testMastrUnit.getGeplantesInbetriebsnahmeDatum()).isEqualTo(DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM);
        assertThat(testMastrUnit.getInbetriebnahmeDatum()).isEqualTo(DEFAULT_INBETRIEBNAHME_DATUM);
        assertThat(testMastrUnit.getKwkAnlageInbetriebnahmeDatum()).isEqualTo(DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM);
        assertThat(testMastrUnit.getKwkAnlageRegistrierungsDatum()).isEqualTo(DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getEegInbetriebnahmeDatum()).isEqualTo(DEFAULT_EEG_INBETRIEBNAHME_DATUM);
        assertThat(testMastrUnit.getEegAnlageRegistrierungsDatum()).isEqualTo(DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getGenehmigungDatum()).isEqualTo(DEFAULT_GENEHMIGUNG_DATUM);
        assertThat(testMastrUnit.getGenehmigungRegistrierungsDatum()).isEqualTo(DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getIsNbPruefungAbgeschlossen()).isEqualTo(DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN);
        assertThat(testMastrUnit.getIsAnonymisiert()).isEqualTo(DEFAULT_IS_ANONYMISIERT);
        assertThat(testMastrUnit.getIsBuergerEnergie()).isEqualTo(DEFAULT_IS_BUERGER_ENERGIE);
        assertThat(testMastrUnit.getIsEinheitNotstromaggregat()).isEqualTo(DEFAULT_IS_EINHEIT_NOTSTROMAGGREGAT);
        assertThat(testMastrUnit.getIsMieterstromAngemeldet()).isEqualTo(DEFAULT_IS_MIETERSTROM_ANGEMELDET);
        assertThat(testMastrUnit.getIsWasserkraftErtuechtigung()).isEqualTo(DEFAULT_IS_WASSERKRAFT_ERTUECHTIGUNG);
        assertThat(testMastrUnit.getIsPilotWindanlage()).isEqualTo(DEFAULT_IS_PILOT_WINDANLAGE);
        assertThat(testMastrUnit.getIsPrototypAnlage()).isEqualTo(DEFAULT_IS_PROTOTYP_ANLAGE);
        assertThat(testMastrUnit.getLat()).isEqualTo(DEFAULT_LAT);
        assertThat(testMastrUnit.getLng()).isEqualTo(DEFAULT_LNG);
        assertThat(testMastrUnit.getOrt()).isEqualTo(DEFAULT_ORT);
        assertThat(testMastrUnit.getPlz()).isEqualTo(DEFAULT_PLZ);
        assertThat(testMastrUnit.getStrasse()).isEqualTo(DEFAULT_STRASSE);
        assertThat(testMastrUnit.getHausnummer()).isEqualTo(DEFAULT_HAUSNUMMER);
        assertThat(testMastrUnit.getEinheitname()).isEqualTo(DEFAULT_EINHEITNAME);
        assertThat(testMastrUnit.getFlurstueck()).isEqualTo(DEFAULT_FLURSTUECK);
        assertThat(testMastrUnit.getGemarkung()).isEqualTo(DEFAULT_GEMARKUNG);
        assertThat(testMastrUnit.getGemeinde()).isEqualTo(DEFAULT_GEMEINDE);
        assertThat(testMastrUnit.getLandId()).isEqualTo(DEFAULT_LAND_ID);
        assertThat(testMastrUnit.getLandkreis()).isEqualTo(DEFAULT_LANDKREIS);
        assertThat(testMastrUnit.getAgs()).isEqualTo(DEFAULT_AGS);
        assertThat(testMastrUnit.getLokationId()).isEqualTo(DEFAULT_LOKATION_ID);
        assertThat(testMastrUnit.getLokationMastrNr()).isEqualTo(DEFAULT_LOKATION_MASTR_NR);
        assertThat(testMastrUnit.getNetzbetreiberId()).isEqualTo(DEFAULT_NETZBETREIBER_ID);
        assertThat(testMastrUnit.getNetzbetreiberMaskedNamen()).isEqualTo(DEFAULT_NETZBETREIBER_MASKED_NAMEN);
        assertThat(testMastrUnit.getNetzbetreiberMastrNummer()).isEqualTo(DEFAULT_NETZBETREIBER_MASTR_NUMMER);
        assertThat(testMastrUnit.getNetzbetreiberNamen()).isEqualTo(DEFAULT_NETZBETREIBER_NAMEN);
        assertThat(testMastrUnit.getNetzbetreiberPersonenArt()).isEqualTo(DEFAULT_NETZBETREIBER_PERSONEN_ART);
        assertThat(testMastrUnit.getSystemStatusId()).isEqualTo(DEFAULT_SYSTEM_STATUS_ID);
        assertThat(testMastrUnit.getSystemStatusName()).isEqualTo(DEFAULT_SYSTEM_STATUS_NAME);
        assertThat(testMastrUnit.getTyp()).isEqualTo(DEFAULT_TYP);
        assertThat(testMastrUnit.getAktenzeichenGenehmigung()).isEqualTo(DEFAULT_AKTENZEICHEN_GENEHMIGUNG);
        assertThat(testMastrUnit.getAnzahlSolarmodule()).isEqualTo(DEFAULT_ANZAHL_SOLARMODULE);
        assertThat(testMastrUnit.getBatterieTechnologie()).isEqualTo(DEFAULT_BATTERIE_TECHNOLOGIE);
        assertThat(testMastrUnit.getBruttoLeistung()).isEqualTo(DEFAULT_BRUTTO_LEISTUNG);
        assertThat(testMastrUnit.getEegInstallierteLeistung()).isEqualTo(DEFAULT_EEG_INSTALLIERTE_LEISTUNG);
        assertThat(testMastrUnit.getEegAnlageMastrNummer()).isEqualTo(DEFAULT_EEG_ANLAGE_MASTR_NUMMER);
        assertThat(testMastrUnit.getEegAnlagenSchluessel()).isEqualTo(DEFAULT_EEG_ANLAGEN_SCHLUESSEL);
        assertThat(testMastrUnit.getEegZuschlag()).isEqualTo(DEFAULT_EEG_ZUSCHLAG);
        assertThat(testMastrUnit.getZuschlagsNummern()).isEqualTo(DEFAULT_ZUSCHLAGS_NUMMERN);
        assertThat(testMastrUnit.getEnergieTraegerId()).isEqualTo(DEFAULT_ENERGIE_TRAEGER_ID);
        assertThat(testMastrUnit.getEnergieTraegerName()).isEqualTo(DEFAULT_ENERGIE_TRAEGER_NAME);
        assertThat(testMastrUnit.getGemeinsamerWechselrichter()).isEqualTo(DEFAULT_GEMEINSAMER_WECHSELRICHTER);
        assertThat(testMastrUnit.getGenehmigungBehoerde()).isEqualTo(DEFAULT_GENEHMIGUNG_BEHOERDE);
        assertThat(testMastrUnit.getGenehmigungsMastrNummer()).isEqualTo(DEFAULT_GENEHMIGUNGS_MASTR_NUMMER);
        assertThat(testMastrUnit.getGruppierungsObjekte()).isEqualTo(DEFAULT_GRUPPIERUNGS_OBJEKTE);
        assertThat(testMastrUnit.getGruppierungsObjekteIds()).isEqualTo(DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS);
        assertThat(testMastrUnit.getHatFlexibilitaetsPraemie()).isEqualTo(DEFAULT_HAT_FLEXIBILITAETS_PRAEMIE);
        assertThat(testMastrUnit.getHauptAusrichtungSolarmodule()).isEqualTo(DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE);
        assertThat(testMastrUnit.getHauptAusrichtungSolarmoduleBezeichnung()).isEqualTo(DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG);
        assertThat(testMastrUnit.getHauptBrennstoffId()).isEqualTo(DEFAULT_HAUPT_BRENNSTOFF_ID);
        assertThat(testMastrUnit.getHauptBrennstoffNamen()).isEqualTo(DEFAULT_HAUPT_BRENNSTOFF_NAMEN);
        assertThat(testMastrUnit.getHauptNeigungswinkelSolarModule()).isEqualTo(DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);
        assertThat(testMastrUnit.getHerstellerWindenergieanlageId()).isEqualTo(DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID);
        assertThat(testMastrUnit.getHerstellerWindenergieanlageBezeichnung()).isEqualTo(DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG);
        assertThat(testMastrUnit.getKwkAnlageElektrischeLeistung()).isEqualTo(DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);
        assertThat(testMastrUnit.getKwkAnlageMastrNummer()).isEqualTo(DEFAULT_KWK_ANLAGE_MASTR_NUMMER);
        assertThat(testMastrUnit.getKwkZuschlag()).isEqualTo(DEFAULT_KWK_ZUSCHLAG);
        assertThat(testMastrUnit.getLageEinheit()).isEqualTo(DEFAULT_LAGE_EINHEIT);
        assertThat(testMastrUnit.getLageEinheitBezeichnung()).isEqualTo(DEFAULT_LAGE_EINHEIT_BEZEICHNUNG);
        assertThat(testMastrUnit.getLeistungsBegrenzung()).isEqualTo(DEFAULT_LEISTUNGS_BEGRENZUNG);
        assertThat(testMastrUnit.getNabenhoeheWindenergieanlage()).isEqualTo(DEFAULT_NABENHOEHE_WINDENERGIEANLAGE);
        assertThat(testMastrUnit.getRotorDurchmesserWindenergieanlage()).isEqualTo(DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE);
        assertThat(testMastrUnit.getNettoNennLeistung()).isEqualTo(DEFAULT_NETTO_NENN_LEISTUNG);
        assertThat(testMastrUnit.getNutzbareSpeicherKapazitaet()).isEqualTo(DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET);
        assertThat(testMastrUnit.getNutzungsBereichGebsa()).isEqualTo(DEFAULT_NUTZUNGS_BEREICH_GEBSA);
        assertThat(testMastrUnit.getStandortAnonymisiert()).isEqualTo(DEFAULT_STANDORT_ANONYMISIERT);
        assertThat(testMastrUnit.getSpannungsEbenenId()).isEqualTo(DEFAULT_SPANNUNGS_EBENEN_ID);
        assertThat(testMastrUnit.getSpannungsEbenenNamen()).isEqualTo(DEFAULT_SPANNUNGS_EBENEN_NAMEN);
        assertThat(testMastrUnit.getSpeicherEinheitMastrNummer()).isEqualTo(DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER);
        assertThat(testMastrUnit.getTechnologieStromerzeugungId()).isEqualTo(DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID);
        assertThat(testMastrUnit.getTechnologieStromerzeugung()).isEqualTo(DEFAULT_TECHNOLOGIE_STROMERZEUGUNG);
        assertThat(testMastrUnit.getThermischeNutzLeistung()).isEqualTo(DEFAULT_THERMISCHE_NUTZ_LEISTUNG);
        assertThat(testMastrUnit.getTypenBezeichnung()).isEqualTo(DEFAULT_TYPEN_BEZEICHNUNG);
        assertThat(testMastrUnit.getVollTeilEinspeisung()).isEqualTo(DEFAULT_VOLL_TEIL_EINSPEISUNG);
        assertThat(testMastrUnit.getVollTeilEinspeisungBezeichnung()).isEqualTo(DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG);
        assertThat(testMastrUnit.getWindparkName()).isEqualTo(DEFAULT_WINDPARK_NAME);
        assertThat(testMastrUnit.getGeometry()).isEqualTo(DEFAULT_GEOMETRY);
    }

    @Test
    @Transactional
    void createMastrUnitWithExistingId() throws Exception {
        // Create the MastrUnit with an existing ID
        mastrUnit.setId(1L);

        int databaseSizeBeforeCreate = mastrUnitRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMastrUnitMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mastrUnit)))
            .andExpect(status().isBadRequest());

        // Validate the MastrUnit in the database
        List<MastrUnit> mastrUnitList = mastrUnitRepository.findAll();
        assertThat(mastrUnitList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMastrUnits() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList
        restMastrUnitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mastrUnit.getId().intValue())))
            .andExpect(jsonPath("$.[*].mastrNummer").value(hasItem(DEFAULT_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].anlagenBetreiberId").value(hasItem(DEFAULT_ANLAGEN_BETREIBER_ID.intValue())))
            .andExpect(jsonPath("$.[*].anlagenBetreiberPersonenArt").value(hasItem(DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART.intValue())))
            .andExpect(jsonPath("$.[*].anlagenBetreiberMaskedName").value(hasItem(DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME)))
            .andExpect(jsonPath("$.[*].anlagenBetreiberMastrNummer").value(hasItem(DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].anlagenBetreiberName").value(hasItem(DEFAULT_ANLAGEN_BETREIBER_NAME)))
            .andExpect(jsonPath("$.[*].betriebsStatusId").value(hasItem(DEFAULT_BETRIEBS_STATUS_ID)))
            .andExpect(jsonPath("$.[*].betriebsStatusName").value(hasItem(DEFAULT_BETRIEBS_STATUS_NAME)))
            .andExpect(jsonPath("$.[*].bundesland").value(hasItem(DEFAULT_BUNDESLAND)))
            .andExpect(jsonPath("$.[*].bundeslandId").value(hasItem(DEFAULT_BUNDESLAND_ID)))
            .andExpect(jsonPath("$.[*].landkreisId").value(hasItem(DEFAULT_LANDKREIS_ID)))
            .andExpect(jsonPath("$.[*].gemeindeId").value(hasItem(DEFAULT_GEMEINDE_ID)))
            .andExpect(jsonPath("$.[*].datumLetzteAktualisierung").value(hasItem(DEFAULT_DATUM_LETZTE_AKTUALISIERUNG.toString())))
            .andExpect(jsonPath("$.[*].einheitRegistrierungsDatum").value(hasItem(DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM.toString())))
            .andExpect(jsonPath("$.[*].endgueltigeStilllegungDatum").value(hasItem(DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM.toString())))
            .andExpect(jsonPath("$.[*].geplantesInbetriebsnahmeDatum").value(hasItem(DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM.toString())))
            .andExpect(jsonPath("$.[*].inbetriebnahmeDatum").value(hasItem(DEFAULT_INBETRIEBNAHME_DATUM.toString())))
            .andExpect(jsonPath("$.[*].kwkAnlageInbetriebnahmeDatum").value(hasItem(DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM.toString())))
            .andExpect(jsonPath("$.[*].kwkAnlageRegistrierungsDatum").value(hasItem(DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM.toString())))
            .andExpect(jsonPath("$.[*].eegInbetriebnahmeDatum").value(hasItem(DEFAULT_EEG_INBETRIEBNAHME_DATUM.toString())))
            .andExpect(jsonPath("$.[*].eegAnlageRegistrierungsDatum").value(hasItem(DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM.toString())))
            .andExpect(jsonPath("$.[*].genehmigungDatum").value(hasItem(DEFAULT_GENEHMIGUNG_DATUM.toString())))
            .andExpect(jsonPath("$.[*].genehmigungRegistrierungsDatum").value(hasItem(DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM.toString())))
            .andExpect(jsonPath("$.[*].isNbPruefungAbgeschlossen").value(hasItem(DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN)))
            .andExpect(jsonPath("$.[*].isAnonymisiert").value(hasItem(DEFAULT_IS_ANONYMISIERT.booleanValue())))
            .andExpect(jsonPath("$.[*].isBuergerEnergie").value(hasItem(DEFAULT_IS_BUERGER_ENERGIE.booleanValue())))
            .andExpect(jsonPath("$.[*].isEinheitNotstromaggregat").value(hasItem(DEFAULT_IS_EINHEIT_NOTSTROMAGGREGAT.booleanValue())))
            .andExpect(jsonPath("$.[*].isMieterstromAngemeldet").value(hasItem(DEFAULT_IS_MIETERSTROM_ANGEMELDET.booleanValue())))
            .andExpect(jsonPath("$.[*].isWasserkraftErtuechtigung").value(hasItem(DEFAULT_IS_WASSERKRAFT_ERTUECHTIGUNG.booleanValue())))
            .andExpect(jsonPath("$.[*].isPilotWindanlage").value(hasItem(DEFAULT_IS_PILOT_WINDANLAGE.booleanValue())))
            .andExpect(jsonPath("$.[*].isPrototypAnlage").value(hasItem(DEFAULT_IS_PROTOTYP_ANLAGE.booleanValue())))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.doubleValue())))
            .andExpect(jsonPath("$.[*].lng").value(hasItem(DEFAULT_LNG.doubleValue())))
            .andExpect(jsonPath("$.[*].ort").value(hasItem(DEFAULT_ORT)))
            .andExpect(jsonPath("$.[*].plz").value(hasItem(DEFAULT_PLZ)))
            .andExpect(jsonPath("$.[*].strasse").value(hasItem(DEFAULT_STRASSE)))
            .andExpect(jsonPath("$.[*].hausnummer").value(hasItem(DEFAULT_HAUSNUMMER)))
            .andExpect(jsonPath("$.[*].einheitname").value(hasItem(DEFAULT_EINHEITNAME)))
            .andExpect(jsonPath("$.[*].flurstueck").value(hasItem(DEFAULT_FLURSTUECK)))
            .andExpect(jsonPath("$.[*].gemarkung").value(hasItem(DEFAULT_GEMARKUNG)))
            .andExpect(jsonPath("$.[*].gemeinde").value(hasItem(DEFAULT_GEMEINDE)))
            .andExpect(jsonPath("$.[*].landId").value(hasItem(DEFAULT_LAND_ID)))
            .andExpect(jsonPath("$.[*].landkreis").value(hasItem(DEFAULT_LANDKREIS)))
            .andExpect(jsonPath("$.[*].ags").value(hasItem(DEFAULT_AGS.intValue())))
            .andExpect(jsonPath("$.[*].lokationId").value(hasItem(DEFAULT_LOKATION_ID.intValue())))
            .andExpect(jsonPath("$.[*].lokationMastrNr").value(hasItem(DEFAULT_LOKATION_MASTR_NR)))
            .andExpect(jsonPath("$.[*].netzbetreiberId").value(hasItem(DEFAULT_NETZBETREIBER_ID)))
            .andExpect(jsonPath("$.[*].netzbetreiberMaskedNamen").value(hasItem(DEFAULT_NETZBETREIBER_MASKED_NAMEN)))
            .andExpect(jsonPath("$.[*].netzbetreiberMastrNummer").value(hasItem(DEFAULT_NETZBETREIBER_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].netzbetreiberNamen").value(hasItem(DEFAULT_NETZBETREIBER_NAMEN)))
            .andExpect(jsonPath("$.[*].netzbetreiberPersonenArt").value(hasItem(DEFAULT_NETZBETREIBER_PERSONEN_ART)))
            .andExpect(jsonPath("$.[*].systemStatusId").value(hasItem(DEFAULT_SYSTEM_STATUS_ID)))
            .andExpect(jsonPath("$.[*].systemStatusName").value(hasItem(DEFAULT_SYSTEM_STATUS_NAME)))
            .andExpect(jsonPath("$.[*].typ").value(hasItem(DEFAULT_TYP)))
            .andExpect(jsonPath("$.[*].aktenzeichenGenehmigung").value(hasItem(DEFAULT_AKTENZEICHEN_GENEHMIGUNG)))
            .andExpect(jsonPath("$.[*].anzahlSolarmodule").value(hasItem(DEFAULT_ANZAHL_SOLARMODULE)))
            .andExpect(jsonPath("$.[*].batterieTechnologie").value(hasItem(DEFAULT_BATTERIE_TECHNOLOGIE)))
            .andExpect(jsonPath("$.[*].bruttoLeistung").value(hasItem(DEFAULT_BRUTTO_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].eegInstallierteLeistung").value(hasItem(DEFAULT_EEG_INSTALLIERTE_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].eegAnlageMastrNummer").value(hasItem(DEFAULT_EEG_ANLAGE_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].eegAnlagenSchluessel").value(hasItem(DEFAULT_EEG_ANLAGEN_SCHLUESSEL)))
            .andExpect(jsonPath("$.[*].eegZuschlag").value(hasItem(DEFAULT_EEG_ZUSCHLAG)))
            .andExpect(jsonPath("$.[*].zuschlagsNummern").value(hasItem(DEFAULT_ZUSCHLAGS_NUMMERN)))
            .andExpect(jsonPath("$.[*].energieTraegerId").value(hasItem(DEFAULT_ENERGIE_TRAEGER_ID)))
            .andExpect(jsonPath("$.[*].energieTraegerName").value(hasItem(DEFAULT_ENERGIE_TRAEGER_NAME)))
            .andExpect(jsonPath("$.[*].gemeinsamerWechselrichter").value(hasItem(DEFAULT_GEMEINSAMER_WECHSELRICHTER)))
            .andExpect(jsonPath("$.[*].genehmigungBehoerde").value(hasItem(DEFAULT_GENEHMIGUNG_BEHOERDE)))
            .andExpect(jsonPath("$.[*].genehmigungsMastrNummer").value(hasItem(DEFAULT_GENEHMIGUNGS_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].gruppierungsObjekte").value(hasItem(DEFAULT_GRUPPIERUNGS_OBJEKTE)))
            .andExpect(jsonPath("$.[*].gruppierungsObjekteIds").value(hasItem(DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS)))
            .andExpect(jsonPath("$.[*].hatFlexibilitaetsPraemie").value(hasItem(DEFAULT_HAT_FLEXIBILITAETS_PRAEMIE.booleanValue())))
            .andExpect(jsonPath("$.[*].hauptAusrichtungSolarmodule").value(hasItem(DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE)))
            .andExpect(
                jsonPath("$.[*].hauptAusrichtungSolarmoduleBezeichnung").value(hasItem(DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG))
            )
            .andExpect(jsonPath("$.[*].hauptBrennstoffId").value(hasItem(DEFAULT_HAUPT_BRENNSTOFF_ID)))
            .andExpect(jsonPath("$.[*].hauptBrennstoffNamen").value(hasItem(DEFAULT_HAUPT_BRENNSTOFF_NAMEN)))
            .andExpect(jsonPath("$.[*].hauptNeigungswinkelSolarModule").value(hasItem(DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE)))
            .andExpect(jsonPath("$.[*].herstellerWindenergieanlageId").value(hasItem(DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID)))
            .andExpect(
                jsonPath("$.[*].herstellerWindenergieanlageBezeichnung").value(hasItem(DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG))
            )
            .andExpect(jsonPath("$.[*].kwkAnlageElektrischeLeistung").value(hasItem(DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].kwkAnlageMastrNummer").value(hasItem(DEFAULT_KWK_ANLAGE_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].kwkZuschlag").value(hasItem(DEFAULT_KWK_ZUSCHLAG)))
            .andExpect(jsonPath("$.[*].lageEinheit").value(hasItem(DEFAULT_LAGE_EINHEIT)))
            .andExpect(jsonPath("$.[*].lageEinheitBezeichnung").value(hasItem(DEFAULT_LAGE_EINHEIT_BEZEICHNUNG)))
            .andExpect(jsonPath("$.[*].leistungsBegrenzung").value(hasItem(DEFAULT_LEISTUNGS_BEGRENZUNG)))
            .andExpect(jsonPath("$.[*].nabenhoeheWindenergieanlage").value(hasItem(DEFAULT_NABENHOEHE_WINDENERGIEANLAGE.doubleValue())))
            .andExpect(
                jsonPath("$.[*].rotorDurchmesserWindenergieanlage")
                    .value(hasItem(DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE.doubleValue()))
            )
            .andExpect(jsonPath("$.[*].nettoNennLeistung").value(hasItem(DEFAULT_NETTO_NENN_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].nutzbareSpeicherKapazitaet").value(hasItem(DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET.doubleValue())))
            .andExpect(jsonPath("$.[*].nutzungsBereichGebsa").value(hasItem(DEFAULT_NUTZUNGS_BEREICH_GEBSA)))
            .andExpect(jsonPath("$.[*].standortAnonymisiert").value(hasItem(DEFAULT_STANDORT_ANONYMISIERT)))
            .andExpect(jsonPath("$.[*].spannungsEbenenId").value(hasItem(DEFAULT_SPANNUNGS_EBENEN_ID)))
            .andExpect(jsonPath("$.[*].spannungsEbenenNamen").value(hasItem(DEFAULT_SPANNUNGS_EBENEN_NAMEN)))
            .andExpect(jsonPath("$.[*].speicherEinheitMastrNummer").value(hasItem(DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].technologieStromerzeugungId").value(hasItem(DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID)))
            .andExpect(jsonPath("$.[*].technologieStromerzeugung").value(hasItem(DEFAULT_TECHNOLOGIE_STROMERZEUGUNG)))
            .andExpect(jsonPath("$.[*].thermischeNutzLeistung").value(hasItem(DEFAULT_THERMISCHE_NUTZ_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].typenBezeichnung").value(hasItem(DEFAULT_TYPEN_BEZEICHNUNG)))
            .andExpect(jsonPath("$.[*].vollTeilEinspeisung").value(hasItem(DEFAULT_VOLL_TEIL_EINSPEISUNG)))
            .andExpect(jsonPath("$.[*].vollTeilEinspeisungBezeichnung").value(hasItem(DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG)))
            .andExpect(jsonPath("$.[*].windparkName").value(hasItem(DEFAULT_WINDPARK_NAME)))
            .andExpect(jsonPath("$.[*].geometry").value(hasItem(DEFAULT_GEOMETRY)));
    }

    @Test
    @Transactional
    void getMastrUnit() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get the mastrUnit
        restMastrUnitMockMvc
            .perform(get(ENTITY_API_URL_ID, mastrUnit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(mastrUnit.getId().intValue()))
            .andExpect(jsonPath("$.mastrNummer").value(DEFAULT_MASTR_NUMMER))
            .andExpect(jsonPath("$.anlagenBetreiberId").value(DEFAULT_ANLAGEN_BETREIBER_ID.intValue()))
            .andExpect(jsonPath("$.anlagenBetreiberPersonenArt").value(DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART.intValue()))
            .andExpect(jsonPath("$.anlagenBetreiberMaskedName").value(DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME))
            .andExpect(jsonPath("$.anlagenBetreiberMastrNummer").value(DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER))
            .andExpect(jsonPath("$.anlagenBetreiberName").value(DEFAULT_ANLAGEN_BETREIBER_NAME))
            .andExpect(jsonPath("$.betriebsStatusId").value(DEFAULT_BETRIEBS_STATUS_ID))
            .andExpect(jsonPath("$.betriebsStatusName").value(DEFAULT_BETRIEBS_STATUS_NAME))
            .andExpect(jsonPath("$.bundesland").value(DEFAULT_BUNDESLAND))
            .andExpect(jsonPath("$.bundeslandId").value(DEFAULT_BUNDESLAND_ID))
            .andExpect(jsonPath("$.landkreisId").value(DEFAULT_LANDKREIS_ID))
            .andExpect(jsonPath("$.gemeindeId").value(DEFAULT_GEMEINDE_ID))
            .andExpect(jsonPath("$.datumLetzteAktualisierung").value(DEFAULT_DATUM_LETZTE_AKTUALISIERUNG.toString()))
            .andExpect(jsonPath("$.einheitRegistrierungsDatum").value(DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM.toString()))
            .andExpect(jsonPath("$.endgueltigeStilllegungDatum").value(DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM.toString()))
            .andExpect(jsonPath("$.geplantesInbetriebsnahmeDatum").value(DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM.toString()))
            .andExpect(jsonPath("$.inbetriebnahmeDatum").value(DEFAULT_INBETRIEBNAHME_DATUM.toString()))
            .andExpect(jsonPath("$.kwkAnlageInbetriebnahmeDatum").value(DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM.toString()))
            .andExpect(jsonPath("$.kwkAnlageRegistrierungsDatum").value(DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM.toString()))
            .andExpect(jsonPath("$.eegInbetriebnahmeDatum").value(DEFAULT_EEG_INBETRIEBNAHME_DATUM.toString()))
            .andExpect(jsonPath("$.eegAnlageRegistrierungsDatum").value(DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM.toString()))
            .andExpect(jsonPath("$.genehmigungDatum").value(DEFAULT_GENEHMIGUNG_DATUM.toString()))
            .andExpect(jsonPath("$.genehmigungRegistrierungsDatum").value(DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM.toString()))
            .andExpect(jsonPath("$.isNbPruefungAbgeschlossen").value(DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN))
            .andExpect(jsonPath("$.isAnonymisiert").value(DEFAULT_IS_ANONYMISIERT.booleanValue()))
            .andExpect(jsonPath("$.isBuergerEnergie").value(DEFAULT_IS_BUERGER_ENERGIE.booleanValue()))
            .andExpect(jsonPath("$.isEinheitNotstromaggregat").value(DEFAULT_IS_EINHEIT_NOTSTROMAGGREGAT.booleanValue()))
            .andExpect(jsonPath("$.isMieterstromAngemeldet").value(DEFAULT_IS_MIETERSTROM_ANGEMELDET.booleanValue()))
            .andExpect(jsonPath("$.isWasserkraftErtuechtigung").value(DEFAULT_IS_WASSERKRAFT_ERTUECHTIGUNG.booleanValue()))
            .andExpect(jsonPath("$.isPilotWindanlage").value(DEFAULT_IS_PILOT_WINDANLAGE.booleanValue()))
            .andExpect(jsonPath("$.isPrototypAnlage").value(DEFAULT_IS_PROTOTYP_ANLAGE.booleanValue()))
            .andExpect(jsonPath("$.lat").value(DEFAULT_LAT.doubleValue()))
            .andExpect(jsonPath("$.lng").value(DEFAULT_LNG.doubleValue()))
            .andExpect(jsonPath("$.ort").value(DEFAULT_ORT))
            .andExpect(jsonPath("$.plz").value(DEFAULT_PLZ))
            .andExpect(jsonPath("$.strasse").value(DEFAULT_STRASSE))
            .andExpect(jsonPath("$.hausnummer").value(DEFAULT_HAUSNUMMER))
            .andExpect(jsonPath("$.einheitname").value(DEFAULT_EINHEITNAME))
            .andExpect(jsonPath("$.flurstueck").value(DEFAULT_FLURSTUECK))
            .andExpect(jsonPath("$.gemarkung").value(DEFAULT_GEMARKUNG))
            .andExpect(jsonPath("$.gemeinde").value(DEFAULT_GEMEINDE))
            .andExpect(jsonPath("$.landId").value(DEFAULT_LAND_ID))
            .andExpect(jsonPath("$.landkreis").value(DEFAULT_LANDKREIS))
            .andExpect(jsonPath("$.ags").value(DEFAULT_AGS.intValue()))
            .andExpect(jsonPath("$.lokationId").value(DEFAULT_LOKATION_ID.intValue()))
            .andExpect(jsonPath("$.lokationMastrNr").value(DEFAULT_LOKATION_MASTR_NR))
            .andExpect(jsonPath("$.netzbetreiberId").value(DEFAULT_NETZBETREIBER_ID))
            .andExpect(jsonPath("$.netzbetreiberMaskedNamen").value(DEFAULT_NETZBETREIBER_MASKED_NAMEN))
            .andExpect(jsonPath("$.netzbetreiberMastrNummer").value(DEFAULT_NETZBETREIBER_MASTR_NUMMER))
            .andExpect(jsonPath("$.netzbetreiberNamen").value(DEFAULT_NETZBETREIBER_NAMEN))
            .andExpect(jsonPath("$.netzbetreiberPersonenArt").value(DEFAULT_NETZBETREIBER_PERSONEN_ART))
            .andExpect(jsonPath("$.systemStatusId").value(DEFAULT_SYSTEM_STATUS_ID))
            .andExpect(jsonPath("$.systemStatusName").value(DEFAULT_SYSTEM_STATUS_NAME))
            .andExpect(jsonPath("$.typ").value(DEFAULT_TYP))
            .andExpect(jsonPath("$.aktenzeichenGenehmigung").value(DEFAULT_AKTENZEICHEN_GENEHMIGUNG))
            .andExpect(jsonPath("$.anzahlSolarmodule").value(DEFAULT_ANZAHL_SOLARMODULE))
            .andExpect(jsonPath("$.batterieTechnologie").value(DEFAULT_BATTERIE_TECHNOLOGIE))
            .andExpect(jsonPath("$.bruttoLeistung").value(DEFAULT_BRUTTO_LEISTUNG.doubleValue()))
            .andExpect(jsonPath("$.eegInstallierteLeistung").value(DEFAULT_EEG_INSTALLIERTE_LEISTUNG.doubleValue()))
            .andExpect(jsonPath("$.eegAnlageMastrNummer").value(DEFAULT_EEG_ANLAGE_MASTR_NUMMER))
            .andExpect(jsonPath("$.eegAnlagenSchluessel").value(DEFAULT_EEG_ANLAGEN_SCHLUESSEL))
            .andExpect(jsonPath("$.eegZuschlag").value(DEFAULT_EEG_ZUSCHLAG))
            .andExpect(jsonPath("$.zuschlagsNummern").value(DEFAULT_ZUSCHLAGS_NUMMERN))
            .andExpect(jsonPath("$.energieTraegerId").value(DEFAULT_ENERGIE_TRAEGER_ID))
            .andExpect(jsonPath("$.energieTraegerName").value(DEFAULT_ENERGIE_TRAEGER_NAME))
            .andExpect(jsonPath("$.gemeinsamerWechselrichter").value(DEFAULT_GEMEINSAMER_WECHSELRICHTER))
            .andExpect(jsonPath("$.genehmigungBehoerde").value(DEFAULT_GENEHMIGUNG_BEHOERDE))
            .andExpect(jsonPath("$.genehmigungsMastrNummer").value(DEFAULT_GENEHMIGUNGS_MASTR_NUMMER))
            .andExpect(jsonPath("$.gruppierungsObjekte").value(DEFAULT_GRUPPIERUNGS_OBJEKTE))
            .andExpect(jsonPath("$.gruppierungsObjekteIds").value(DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS))
            .andExpect(jsonPath("$.hatFlexibilitaetsPraemie").value(DEFAULT_HAT_FLEXIBILITAETS_PRAEMIE.booleanValue()))
            .andExpect(jsonPath("$.hauptAusrichtungSolarmodule").value(DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE))
            .andExpect(jsonPath("$.hauptAusrichtungSolarmoduleBezeichnung").value(DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG))
            .andExpect(jsonPath("$.hauptBrennstoffId").value(DEFAULT_HAUPT_BRENNSTOFF_ID))
            .andExpect(jsonPath("$.hauptBrennstoffNamen").value(DEFAULT_HAUPT_BRENNSTOFF_NAMEN))
            .andExpect(jsonPath("$.hauptNeigungswinkelSolarModule").value(DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE))
            .andExpect(jsonPath("$.herstellerWindenergieanlageId").value(DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID))
            .andExpect(jsonPath("$.herstellerWindenergieanlageBezeichnung").value(DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG))
            .andExpect(jsonPath("$.kwkAnlageElektrischeLeistung").value(DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG.doubleValue()))
            .andExpect(jsonPath("$.kwkAnlageMastrNummer").value(DEFAULT_KWK_ANLAGE_MASTR_NUMMER))
            .andExpect(jsonPath("$.kwkZuschlag").value(DEFAULT_KWK_ZUSCHLAG))
            .andExpect(jsonPath("$.lageEinheit").value(DEFAULT_LAGE_EINHEIT))
            .andExpect(jsonPath("$.lageEinheitBezeichnung").value(DEFAULT_LAGE_EINHEIT_BEZEICHNUNG))
            .andExpect(jsonPath("$.leistungsBegrenzung").value(DEFAULT_LEISTUNGS_BEGRENZUNG))
            .andExpect(jsonPath("$.nabenhoeheWindenergieanlage").value(DEFAULT_NABENHOEHE_WINDENERGIEANLAGE.doubleValue()))
            .andExpect(jsonPath("$.rotorDurchmesserWindenergieanlage").value(DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE.doubleValue()))
            .andExpect(jsonPath("$.nettoNennLeistung").value(DEFAULT_NETTO_NENN_LEISTUNG.doubleValue()))
            .andExpect(jsonPath("$.nutzbareSpeicherKapazitaet").value(DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET.doubleValue()))
            .andExpect(jsonPath("$.nutzungsBereichGebsa").value(DEFAULT_NUTZUNGS_BEREICH_GEBSA))
            .andExpect(jsonPath("$.standortAnonymisiert").value(DEFAULT_STANDORT_ANONYMISIERT))
            .andExpect(jsonPath("$.spannungsEbenenId").value(DEFAULT_SPANNUNGS_EBENEN_ID))
            .andExpect(jsonPath("$.spannungsEbenenNamen").value(DEFAULT_SPANNUNGS_EBENEN_NAMEN))
            .andExpect(jsonPath("$.speicherEinheitMastrNummer").value(DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER))
            .andExpect(jsonPath("$.technologieStromerzeugungId").value(DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID))
            .andExpect(jsonPath("$.technologieStromerzeugung").value(DEFAULT_TECHNOLOGIE_STROMERZEUGUNG))
            .andExpect(jsonPath("$.thermischeNutzLeistung").value(DEFAULT_THERMISCHE_NUTZ_LEISTUNG.doubleValue()))
            .andExpect(jsonPath("$.typenBezeichnung").value(DEFAULT_TYPEN_BEZEICHNUNG))
            .andExpect(jsonPath("$.vollTeilEinspeisung").value(DEFAULT_VOLL_TEIL_EINSPEISUNG))
            .andExpect(jsonPath("$.vollTeilEinspeisungBezeichnung").value(DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG))
            .andExpect(jsonPath("$.windparkName").value(DEFAULT_WINDPARK_NAME))
            .andExpect(jsonPath("$.geometry").value(DEFAULT_GEOMETRY));
    }

    @Test
    @Transactional
    void getMastrUnitsByIdFiltering() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        Long id = mastrUnit.getId();

        defaultMastrUnitShouldBeFound("id.equals=" + id);
        defaultMastrUnitShouldNotBeFound("id.notEquals=" + id);

        defaultMastrUnitShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMastrUnitShouldNotBeFound("id.greaterThan=" + id);

        defaultMastrUnitShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMastrUnitShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByMastrNummerIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where mastrNummer equals to DEFAULT_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("mastrNummer.equals=" + DEFAULT_MASTR_NUMMER);

        // Get all the mastrUnitList where mastrNummer equals to UPDATED_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("mastrNummer.equals=" + UPDATED_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByMastrNummerIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where mastrNummer in DEFAULT_MASTR_NUMMER or UPDATED_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("mastrNummer.in=" + DEFAULT_MASTR_NUMMER + "," + UPDATED_MASTR_NUMMER);

        // Get all the mastrUnitList where mastrNummer equals to UPDATED_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("mastrNummer.in=" + UPDATED_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByMastrNummerIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where mastrNummer is not null
        defaultMastrUnitShouldBeFound("mastrNummer.specified=true");

        // Get all the mastrUnitList where mastrNummer is null
        defaultMastrUnitShouldNotBeFound("mastrNummer.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByMastrNummerContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where mastrNummer contains DEFAULT_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("mastrNummer.contains=" + DEFAULT_MASTR_NUMMER);

        // Get all the mastrUnitList where mastrNummer contains UPDATED_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("mastrNummer.contains=" + UPDATED_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByMastrNummerNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where mastrNummer does not contain DEFAULT_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("mastrNummer.doesNotContain=" + DEFAULT_MASTR_NUMMER);

        // Get all the mastrUnitList where mastrNummer does not contain UPDATED_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("mastrNummer.doesNotContain=" + UPDATED_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberId equals to DEFAULT_ANLAGEN_BETREIBER_ID
        defaultMastrUnitShouldBeFound("anlagenBetreiberId.equals=" + DEFAULT_ANLAGEN_BETREIBER_ID);

        // Get all the mastrUnitList where anlagenBetreiberId equals to UPDATED_ANLAGEN_BETREIBER_ID
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberId.equals=" + UPDATED_ANLAGEN_BETREIBER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberId in DEFAULT_ANLAGEN_BETREIBER_ID or UPDATED_ANLAGEN_BETREIBER_ID
        defaultMastrUnitShouldBeFound("anlagenBetreiberId.in=" + DEFAULT_ANLAGEN_BETREIBER_ID + "," + UPDATED_ANLAGEN_BETREIBER_ID);

        // Get all the mastrUnitList where anlagenBetreiberId equals to UPDATED_ANLAGEN_BETREIBER_ID
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberId.in=" + UPDATED_ANLAGEN_BETREIBER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberId is not null
        defaultMastrUnitShouldBeFound("anlagenBetreiberId.specified=true");

        // Get all the mastrUnitList where anlagenBetreiberId is null
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberId is greater than or equal to DEFAULT_ANLAGEN_BETREIBER_ID
        defaultMastrUnitShouldBeFound("anlagenBetreiberId.greaterThanOrEqual=" + DEFAULT_ANLAGEN_BETREIBER_ID);

        // Get all the mastrUnitList where anlagenBetreiberId is greater than or equal to UPDATED_ANLAGEN_BETREIBER_ID
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberId.greaterThanOrEqual=" + UPDATED_ANLAGEN_BETREIBER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberId is less than or equal to DEFAULT_ANLAGEN_BETREIBER_ID
        defaultMastrUnitShouldBeFound("anlagenBetreiberId.lessThanOrEqual=" + DEFAULT_ANLAGEN_BETREIBER_ID);

        // Get all the mastrUnitList where anlagenBetreiberId is less than or equal to SMALLER_ANLAGEN_BETREIBER_ID
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberId.lessThanOrEqual=" + SMALLER_ANLAGEN_BETREIBER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberIdIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberId is less than DEFAULT_ANLAGEN_BETREIBER_ID
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberId.lessThan=" + DEFAULT_ANLAGEN_BETREIBER_ID);

        // Get all the mastrUnitList where anlagenBetreiberId is less than UPDATED_ANLAGEN_BETREIBER_ID
        defaultMastrUnitShouldBeFound("anlagenBetreiberId.lessThan=" + UPDATED_ANLAGEN_BETREIBER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberId is greater than DEFAULT_ANLAGEN_BETREIBER_ID
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberId.greaterThan=" + DEFAULT_ANLAGEN_BETREIBER_ID);

        // Get all the mastrUnitList where anlagenBetreiberId is greater than SMALLER_ANLAGEN_BETREIBER_ID
        defaultMastrUnitShouldBeFound("anlagenBetreiberId.greaterThan=" + SMALLER_ANLAGEN_BETREIBER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberPersonenArtIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt equals to DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART
        defaultMastrUnitShouldBeFound("anlagenBetreiberPersonenArt.equals=" + DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART);

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt equals to UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberPersonenArt.equals=" + UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberPersonenArtIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt in DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART or UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART
        defaultMastrUnitShouldBeFound(
            "anlagenBetreiberPersonenArt.in=" + DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART + "," + UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART
        );

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt equals to UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberPersonenArt.in=" + UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberPersonenArtIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt is not null
        defaultMastrUnitShouldBeFound("anlagenBetreiberPersonenArt.specified=true");

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt is null
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberPersonenArt.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberPersonenArtIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt is greater than or equal to DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART
        defaultMastrUnitShouldBeFound("anlagenBetreiberPersonenArt.greaterThanOrEqual=" + DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART);

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt is greater than or equal to UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberPersonenArt.greaterThanOrEqual=" + UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberPersonenArtIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt is less than or equal to DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART
        defaultMastrUnitShouldBeFound("anlagenBetreiberPersonenArt.lessThanOrEqual=" + DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART);

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt is less than or equal to SMALLER_ANLAGEN_BETREIBER_PERSONEN_ART
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberPersonenArt.lessThanOrEqual=" + SMALLER_ANLAGEN_BETREIBER_PERSONEN_ART);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberPersonenArtIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt is less than DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberPersonenArt.lessThan=" + DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART);

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt is less than UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART
        defaultMastrUnitShouldBeFound("anlagenBetreiberPersonenArt.lessThan=" + UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberPersonenArtIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt is greater than DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberPersonenArt.greaterThan=" + DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART);

        // Get all the mastrUnitList where anlagenBetreiberPersonenArt is greater than SMALLER_ANLAGEN_BETREIBER_PERSONEN_ART
        defaultMastrUnitShouldBeFound("anlagenBetreiberPersonenArt.greaterThan=" + SMALLER_ANLAGEN_BETREIBER_PERSONEN_ART);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberMaskedNameIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberMaskedName equals to DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME
        defaultMastrUnitShouldBeFound("anlagenBetreiberMaskedName.equals=" + DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME);

        // Get all the mastrUnitList where anlagenBetreiberMaskedName equals to UPDATED_ANLAGEN_BETREIBER_MASKED_NAME
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberMaskedName.equals=" + UPDATED_ANLAGEN_BETREIBER_MASKED_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberMaskedNameIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberMaskedName in DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME or UPDATED_ANLAGEN_BETREIBER_MASKED_NAME
        defaultMastrUnitShouldBeFound(
            "anlagenBetreiberMaskedName.in=" + DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME + "," + UPDATED_ANLAGEN_BETREIBER_MASKED_NAME
        );

        // Get all the mastrUnitList where anlagenBetreiberMaskedName equals to UPDATED_ANLAGEN_BETREIBER_MASKED_NAME
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberMaskedName.in=" + UPDATED_ANLAGEN_BETREIBER_MASKED_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberMaskedNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberMaskedName is not null
        defaultMastrUnitShouldBeFound("anlagenBetreiberMaskedName.specified=true");

        // Get all the mastrUnitList where anlagenBetreiberMaskedName is null
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberMaskedName.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberMaskedNameContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberMaskedName contains DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME
        defaultMastrUnitShouldBeFound("anlagenBetreiberMaskedName.contains=" + DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME);

        // Get all the mastrUnitList where anlagenBetreiberMaskedName contains UPDATED_ANLAGEN_BETREIBER_MASKED_NAME
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberMaskedName.contains=" + UPDATED_ANLAGEN_BETREIBER_MASKED_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberMaskedNameNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberMaskedName does not contain DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberMaskedName.doesNotContain=" + DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME);

        // Get all the mastrUnitList where anlagenBetreiberMaskedName does not contain UPDATED_ANLAGEN_BETREIBER_MASKED_NAME
        defaultMastrUnitShouldBeFound("anlagenBetreiberMaskedName.doesNotContain=" + UPDATED_ANLAGEN_BETREIBER_MASKED_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberMastrNummerIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberMastrNummer equals to DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("anlagenBetreiberMastrNummer.equals=" + DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER);

        // Get all the mastrUnitList where anlagenBetreiberMastrNummer equals to UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberMastrNummer.equals=" + UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberMastrNummerIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberMastrNummer in DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER or UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldBeFound(
            "anlagenBetreiberMastrNummer.in=" + DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER + "," + UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER
        );

        // Get all the mastrUnitList where anlagenBetreiberMastrNummer equals to UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberMastrNummer.in=" + UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberMastrNummerIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberMastrNummer is not null
        defaultMastrUnitShouldBeFound("anlagenBetreiberMastrNummer.specified=true");

        // Get all the mastrUnitList where anlagenBetreiberMastrNummer is null
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberMastrNummer.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberMastrNummerContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberMastrNummer contains DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("anlagenBetreiberMastrNummer.contains=" + DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER);

        // Get all the mastrUnitList where anlagenBetreiberMastrNummer contains UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberMastrNummer.contains=" + UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberMastrNummerNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberMastrNummer does not contain DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberMastrNummer.doesNotContain=" + DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER);

        // Get all the mastrUnitList where anlagenBetreiberMastrNummer does not contain UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("anlagenBetreiberMastrNummer.doesNotContain=" + UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberNameIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberName equals to DEFAULT_ANLAGEN_BETREIBER_NAME
        defaultMastrUnitShouldBeFound("anlagenBetreiberName.equals=" + DEFAULT_ANLAGEN_BETREIBER_NAME);

        // Get all the mastrUnitList where anlagenBetreiberName equals to UPDATED_ANLAGEN_BETREIBER_NAME
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberName.equals=" + UPDATED_ANLAGEN_BETREIBER_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberNameIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberName in DEFAULT_ANLAGEN_BETREIBER_NAME or UPDATED_ANLAGEN_BETREIBER_NAME
        defaultMastrUnitShouldBeFound("anlagenBetreiberName.in=" + DEFAULT_ANLAGEN_BETREIBER_NAME + "," + UPDATED_ANLAGEN_BETREIBER_NAME);

        // Get all the mastrUnitList where anlagenBetreiberName equals to UPDATED_ANLAGEN_BETREIBER_NAME
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberName.in=" + UPDATED_ANLAGEN_BETREIBER_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberName is not null
        defaultMastrUnitShouldBeFound("anlagenBetreiberName.specified=true");

        // Get all the mastrUnitList where anlagenBetreiberName is null
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberName.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberNameContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberName contains DEFAULT_ANLAGEN_BETREIBER_NAME
        defaultMastrUnitShouldBeFound("anlagenBetreiberName.contains=" + DEFAULT_ANLAGEN_BETREIBER_NAME);

        // Get all the mastrUnitList where anlagenBetreiberName contains UPDATED_ANLAGEN_BETREIBER_NAME
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberName.contains=" + UPDATED_ANLAGEN_BETREIBER_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnlagenBetreiberNameNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anlagenBetreiberName does not contain DEFAULT_ANLAGEN_BETREIBER_NAME
        defaultMastrUnitShouldNotBeFound("anlagenBetreiberName.doesNotContain=" + DEFAULT_ANLAGEN_BETREIBER_NAME);

        // Get all the mastrUnitList where anlagenBetreiberName does not contain UPDATED_ANLAGEN_BETREIBER_NAME
        defaultMastrUnitShouldBeFound("anlagenBetreiberName.doesNotContain=" + UPDATED_ANLAGEN_BETREIBER_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBetriebsStatusIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where betriebsStatusId equals to DEFAULT_BETRIEBS_STATUS_ID
        defaultMastrUnitShouldBeFound("betriebsStatusId.equals=" + DEFAULT_BETRIEBS_STATUS_ID);

        // Get all the mastrUnitList where betriebsStatusId equals to UPDATED_BETRIEBS_STATUS_ID
        defaultMastrUnitShouldNotBeFound("betriebsStatusId.equals=" + UPDATED_BETRIEBS_STATUS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBetriebsStatusIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where betriebsStatusId in DEFAULT_BETRIEBS_STATUS_ID or UPDATED_BETRIEBS_STATUS_ID
        defaultMastrUnitShouldBeFound("betriebsStatusId.in=" + DEFAULT_BETRIEBS_STATUS_ID + "," + UPDATED_BETRIEBS_STATUS_ID);

        // Get all the mastrUnitList where betriebsStatusId equals to UPDATED_BETRIEBS_STATUS_ID
        defaultMastrUnitShouldNotBeFound("betriebsStatusId.in=" + UPDATED_BETRIEBS_STATUS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBetriebsStatusIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where betriebsStatusId is not null
        defaultMastrUnitShouldBeFound("betriebsStatusId.specified=true");

        // Get all the mastrUnitList where betriebsStatusId is null
        defaultMastrUnitShouldNotBeFound("betriebsStatusId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBetriebsStatusIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where betriebsStatusId is greater than or equal to DEFAULT_BETRIEBS_STATUS_ID
        defaultMastrUnitShouldBeFound("betriebsStatusId.greaterThanOrEqual=" + DEFAULT_BETRIEBS_STATUS_ID);

        // Get all the mastrUnitList where betriebsStatusId is greater than or equal to UPDATED_BETRIEBS_STATUS_ID
        defaultMastrUnitShouldNotBeFound("betriebsStatusId.greaterThanOrEqual=" + UPDATED_BETRIEBS_STATUS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBetriebsStatusIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where betriebsStatusId is less than or equal to DEFAULT_BETRIEBS_STATUS_ID
        defaultMastrUnitShouldBeFound("betriebsStatusId.lessThanOrEqual=" + DEFAULT_BETRIEBS_STATUS_ID);

        // Get all the mastrUnitList where betriebsStatusId is less than or equal to SMALLER_BETRIEBS_STATUS_ID
        defaultMastrUnitShouldNotBeFound("betriebsStatusId.lessThanOrEqual=" + SMALLER_BETRIEBS_STATUS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBetriebsStatusIdIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where betriebsStatusId is less than DEFAULT_BETRIEBS_STATUS_ID
        defaultMastrUnitShouldNotBeFound("betriebsStatusId.lessThan=" + DEFAULT_BETRIEBS_STATUS_ID);

        // Get all the mastrUnitList where betriebsStatusId is less than UPDATED_BETRIEBS_STATUS_ID
        defaultMastrUnitShouldBeFound("betriebsStatusId.lessThan=" + UPDATED_BETRIEBS_STATUS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBetriebsStatusIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where betriebsStatusId is greater than DEFAULT_BETRIEBS_STATUS_ID
        defaultMastrUnitShouldNotBeFound("betriebsStatusId.greaterThan=" + DEFAULT_BETRIEBS_STATUS_ID);

        // Get all the mastrUnitList where betriebsStatusId is greater than SMALLER_BETRIEBS_STATUS_ID
        defaultMastrUnitShouldBeFound("betriebsStatusId.greaterThan=" + SMALLER_BETRIEBS_STATUS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBetriebsStatusNameIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where betriebsStatusName equals to DEFAULT_BETRIEBS_STATUS_NAME
        defaultMastrUnitShouldBeFound("betriebsStatusName.equals=" + DEFAULT_BETRIEBS_STATUS_NAME);

        // Get all the mastrUnitList where betriebsStatusName equals to UPDATED_BETRIEBS_STATUS_NAME
        defaultMastrUnitShouldNotBeFound("betriebsStatusName.equals=" + UPDATED_BETRIEBS_STATUS_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBetriebsStatusNameIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where betriebsStatusName in DEFAULT_BETRIEBS_STATUS_NAME or UPDATED_BETRIEBS_STATUS_NAME
        defaultMastrUnitShouldBeFound("betriebsStatusName.in=" + DEFAULT_BETRIEBS_STATUS_NAME + "," + UPDATED_BETRIEBS_STATUS_NAME);

        // Get all the mastrUnitList where betriebsStatusName equals to UPDATED_BETRIEBS_STATUS_NAME
        defaultMastrUnitShouldNotBeFound("betriebsStatusName.in=" + UPDATED_BETRIEBS_STATUS_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBetriebsStatusNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where betriebsStatusName is not null
        defaultMastrUnitShouldBeFound("betriebsStatusName.specified=true");

        // Get all the mastrUnitList where betriebsStatusName is null
        defaultMastrUnitShouldNotBeFound("betriebsStatusName.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBetriebsStatusNameContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where betriebsStatusName contains DEFAULT_BETRIEBS_STATUS_NAME
        defaultMastrUnitShouldBeFound("betriebsStatusName.contains=" + DEFAULT_BETRIEBS_STATUS_NAME);

        // Get all the mastrUnitList where betriebsStatusName contains UPDATED_BETRIEBS_STATUS_NAME
        defaultMastrUnitShouldNotBeFound("betriebsStatusName.contains=" + UPDATED_BETRIEBS_STATUS_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBetriebsStatusNameNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where betriebsStatusName does not contain DEFAULT_BETRIEBS_STATUS_NAME
        defaultMastrUnitShouldNotBeFound("betriebsStatusName.doesNotContain=" + DEFAULT_BETRIEBS_STATUS_NAME);

        // Get all the mastrUnitList where betriebsStatusName does not contain UPDATED_BETRIEBS_STATUS_NAME
        defaultMastrUnitShouldBeFound("betriebsStatusName.doesNotContain=" + UPDATED_BETRIEBS_STATUS_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBundeslandIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bundesland equals to DEFAULT_BUNDESLAND
        defaultMastrUnitShouldBeFound("bundesland.equals=" + DEFAULT_BUNDESLAND);

        // Get all the mastrUnitList where bundesland equals to UPDATED_BUNDESLAND
        defaultMastrUnitShouldNotBeFound("bundesland.equals=" + UPDATED_BUNDESLAND);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBundeslandIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bundesland in DEFAULT_BUNDESLAND or UPDATED_BUNDESLAND
        defaultMastrUnitShouldBeFound("bundesland.in=" + DEFAULT_BUNDESLAND + "," + UPDATED_BUNDESLAND);

        // Get all the mastrUnitList where bundesland equals to UPDATED_BUNDESLAND
        defaultMastrUnitShouldNotBeFound("bundesland.in=" + UPDATED_BUNDESLAND);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBundeslandIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bundesland is not null
        defaultMastrUnitShouldBeFound("bundesland.specified=true");

        // Get all the mastrUnitList where bundesland is null
        defaultMastrUnitShouldNotBeFound("bundesland.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBundeslandContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bundesland contains DEFAULT_BUNDESLAND
        defaultMastrUnitShouldBeFound("bundesland.contains=" + DEFAULT_BUNDESLAND);

        // Get all the mastrUnitList where bundesland contains UPDATED_BUNDESLAND
        defaultMastrUnitShouldNotBeFound("bundesland.contains=" + UPDATED_BUNDESLAND);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBundeslandNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bundesland does not contain DEFAULT_BUNDESLAND
        defaultMastrUnitShouldNotBeFound("bundesland.doesNotContain=" + DEFAULT_BUNDESLAND);

        // Get all the mastrUnitList where bundesland does not contain UPDATED_BUNDESLAND
        defaultMastrUnitShouldBeFound("bundesland.doesNotContain=" + UPDATED_BUNDESLAND);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBundeslandIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bundeslandId equals to DEFAULT_BUNDESLAND_ID
        defaultMastrUnitShouldBeFound("bundeslandId.equals=" + DEFAULT_BUNDESLAND_ID);

        // Get all the mastrUnitList where bundeslandId equals to UPDATED_BUNDESLAND_ID
        defaultMastrUnitShouldNotBeFound("bundeslandId.equals=" + UPDATED_BUNDESLAND_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBundeslandIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bundeslandId in DEFAULT_BUNDESLAND_ID or UPDATED_BUNDESLAND_ID
        defaultMastrUnitShouldBeFound("bundeslandId.in=" + DEFAULT_BUNDESLAND_ID + "," + UPDATED_BUNDESLAND_ID);

        // Get all the mastrUnitList where bundeslandId equals to UPDATED_BUNDESLAND_ID
        defaultMastrUnitShouldNotBeFound("bundeslandId.in=" + UPDATED_BUNDESLAND_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBundeslandIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bundeslandId is not null
        defaultMastrUnitShouldBeFound("bundeslandId.specified=true");

        // Get all the mastrUnitList where bundeslandId is null
        defaultMastrUnitShouldNotBeFound("bundeslandId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBundeslandIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bundeslandId is greater than or equal to DEFAULT_BUNDESLAND_ID
        defaultMastrUnitShouldBeFound("bundeslandId.greaterThanOrEqual=" + DEFAULT_BUNDESLAND_ID);

        // Get all the mastrUnitList where bundeslandId is greater than or equal to UPDATED_BUNDESLAND_ID
        defaultMastrUnitShouldNotBeFound("bundeslandId.greaterThanOrEqual=" + UPDATED_BUNDESLAND_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBundeslandIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bundeslandId is less than or equal to DEFAULT_BUNDESLAND_ID
        defaultMastrUnitShouldBeFound("bundeslandId.lessThanOrEqual=" + DEFAULT_BUNDESLAND_ID);

        // Get all the mastrUnitList where bundeslandId is less than or equal to SMALLER_BUNDESLAND_ID
        defaultMastrUnitShouldNotBeFound("bundeslandId.lessThanOrEqual=" + SMALLER_BUNDESLAND_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBundeslandIdIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bundeslandId is less than DEFAULT_BUNDESLAND_ID
        defaultMastrUnitShouldNotBeFound("bundeslandId.lessThan=" + DEFAULT_BUNDESLAND_ID);

        // Get all the mastrUnitList where bundeslandId is less than UPDATED_BUNDESLAND_ID
        defaultMastrUnitShouldBeFound("bundeslandId.lessThan=" + UPDATED_BUNDESLAND_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBundeslandIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bundeslandId is greater than DEFAULT_BUNDESLAND_ID
        defaultMastrUnitShouldNotBeFound("bundeslandId.greaterThan=" + DEFAULT_BUNDESLAND_ID);

        // Get all the mastrUnitList where bundeslandId is greater than SMALLER_BUNDESLAND_ID
        defaultMastrUnitShouldBeFound("bundeslandId.greaterThan=" + SMALLER_BUNDESLAND_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandkreisIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landkreisId equals to DEFAULT_LANDKREIS_ID
        defaultMastrUnitShouldBeFound("landkreisId.equals=" + DEFAULT_LANDKREIS_ID);

        // Get all the mastrUnitList where landkreisId equals to UPDATED_LANDKREIS_ID
        defaultMastrUnitShouldNotBeFound("landkreisId.equals=" + UPDATED_LANDKREIS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandkreisIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landkreisId in DEFAULT_LANDKREIS_ID or UPDATED_LANDKREIS_ID
        defaultMastrUnitShouldBeFound("landkreisId.in=" + DEFAULT_LANDKREIS_ID + "," + UPDATED_LANDKREIS_ID);

        // Get all the mastrUnitList where landkreisId equals to UPDATED_LANDKREIS_ID
        defaultMastrUnitShouldNotBeFound("landkreisId.in=" + UPDATED_LANDKREIS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandkreisIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landkreisId is not null
        defaultMastrUnitShouldBeFound("landkreisId.specified=true");

        // Get all the mastrUnitList where landkreisId is null
        defaultMastrUnitShouldNotBeFound("landkreisId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandkreisIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landkreisId is greater than or equal to DEFAULT_LANDKREIS_ID
        defaultMastrUnitShouldBeFound("landkreisId.greaterThanOrEqual=" + DEFAULT_LANDKREIS_ID);

        // Get all the mastrUnitList where landkreisId is greater than or equal to UPDATED_LANDKREIS_ID
        defaultMastrUnitShouldNotBeFound("landkreisId.greaterThanOrEqual=" + UPDATED_LANDKREIS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandkreisIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landkreisId is less than or equal to DEFAULT_LANDKREIS_ID
        defaultMastrUnitShouldBeFound("landkreisId.lessThanOrEqual=" + DEFAULT_LANDKREIS_ID);

        // Get all the mastrUnitList where landkreisId is less than or equal to SMALLER_LANDKREIS_ID
        defaultMastrUnitShouldNotBeFound("landkreisId.lessThanOrEqual=" + SMALLER_LANDKREIS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandkreisIdIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landkreisId is less than DEFAULT_LANDKREIS_ID
        defaultMastrUnitShouldNotBeFound("landkreisId.lessThan=" + DEFAULT_LANDKREIS_ID);

        // Get all the mastrUnitList where landkreisId is less than UPDATED_LANDKREIS_ID
        defaultMastrUnitShouldBeFound("landkreisId.lessThan=" + UPDATED_LANDKREIS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandkreisIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landkreisId is greater than DEFAULT_LANDKREIS_ID
        defaultMastrUnitShouldNotBeFound("landkreisId.greaterThan=" + DEFAULT_LANDKREIS_ID);

        // Get all the mastrUnitList where landkreisId is greater than SMALLER_LANDKREIS_ID
        defaultMastrUnitShouldBeFound("landkreisId.greaterThan=" + SMALLER_LANDKREIS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeindeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeindeId equals to DEFAULT_GEMEINDE_ID
        defaultMastrUnitShouldBeFound("gemeindeId.equals=" + DEFAULT_GEMEINDE_ID);

        // Get all the mastrUnitList where gemeindeId equals to UPDATED_GEMEINDE_ID
        defaultMastrUnitShouldNotBeFound("gemeindeId.equals=" + UPDATED_GEMEINDE_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeindeIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeindeId in DEFAULT_GEMEINDE_ID or UPDATED_GEMEINDE_ID
        defaultMastrUnitShouldBeFound("gemeindeId.in=" + DEFAULT_GEMEINDE_ID + "," + UPDATED_GEMEINDE_ID);

        // Get all the mastrUnitList where gemeindeId equals to UPDATED_GEMEINDE_ID
        defaultMastrUnitShouldNotBeFound("gemeindeId.in=" + UPDATED_GEMEINDE_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeindeIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeindeId is not null
        defaultMastrUnitShouldBeFound("gemeindeId.specified=true");

        // Get all the mastrUnitList where gemeindeId is null
        defaultMastrUnitShouldNotBeFound("gemeindeId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeindeIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeindeId is greater than or equal to DEFAULT_GEMEINDE_ID
        defaultMastrUnitShouldBeFound("gemeindeId.greaterThanOrEqual=" + DEFAULT_GEMEINDE_ID);

        // Get all the mastrUnitList where gemeindeId is greater than or equal to UPDATED_GEMEINDE_ID
        defaultMastrUnitShouldNotBeFound("gemeindeId.greaterThanOrEqual=" + UPDATED_GEMEINDE_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeindeIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeindeId is less than or equal to DEFAULT_GEMEINDE_ID
        defaultMastrUnitShouldBeFound("gemeindeId.lessThanOrEqual=" + DEFAULT_GEMEINDE_ID);

        // Get all the mastrUnitList where gemeindeId is less than or equal to SMALLER_GEMEINDE_ID
        defaultMastrUnitShouldNotBeFound("gemeindeId.lessThanOrEqual=" + SMALLER_GEMEINDE_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeindeIdIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeindeId is less than DEFAULT_GEMEINDE_ID
        defaultMastrUnitShouldNotBeFound("gemeindeId.lessThan=" + DEFAULT_GEMEINDE_ID);

        // Get all the mastrUnitList where gemeindeId is less than UPDATED_GEMEINDE_ID
        defaultMastrUnitShouldBeFound("gemeindeId.lessThan=" + UPDATED_GEMEINDE_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeindeIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeindeId is greater than DEFAULT_GEMEINDE_ID
        defaultMastrUnitShouldNotBeFound("gemeindeId.greaterThan=" + DEFAULT_GEMEINDE_ID);

        // Get all the mastrUnitList where gemeindeId is greater than SMALLER_GEMEINDE_ID
        defaultMastrUnitShouldBeFound("gemeindeId.greaterThan=" + SMALLER_GEMEINDE_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByDatumLetzteAktualisierungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where datumLetzteAktualisierung equals to DEFAULT_DATUM_LETZTE_AKTUALISIERUNG
        defaultMastrUnitShouldBeFound("datumLetzteAktualisierung.equals=" + DEFAULT_DATUM_LETZTE_AKTUALISIERUNG);

        // Get all the mastrUnitList where datumLetzteAktualisierung equals to UPDATED_DATUM_LETZTE_AKTUALISIERUNG
        defaultMastrUnitShouldNotBeFound("datumLetzteAktualisierung.equals=" + UPDATED_DATUM_LETZTE_AKTUALISIERUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByDatumLetzteAktualisierungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where datumLetzteAktualisierung in DEFAULT_DATUM_LETZTE_AKTUALISIERUNG or UPDATED_DATUM_LETZTE_AKTUALISIERUNG
        defaultMastrUnitShouldBeFound(
            "datumLetzteAktualisierung.in=" + DEFAULT_DATUM_LETZTE_AKTUALISIERUNG + "," + UPDATED_DATUM_LETZTE_AKTUALISIERUNG
        );

        // Get all the mastrUnitList where datumLetzteAktualisierung equals to UPDATED_DATUM_LETZTE_AKTUALISIERUNG
        defaultMastrUnitShouldNotBeFound("datumLetzteAktualisierung.in=" + UPDATED_DATUM_LETZTE_AKTUALISIERUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByDatumLetzteAktualisierungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where datumLetzteAktualisierung is not null
        defaultMastrUnitShouldBeFound("datumLetzteAktualisierung.specified=true");

        // Get all the mastrUnitList where datumLetzteAktualisierung is null
        defaultMastrUnitShouldNotBeFound("datumLetzteAktualisierung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByDatumLetzteAktualisierungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where datumLetzteAktualisierung is greater than or equal to DEFAULT_DATUM_LETZTE_AKTUALISIERUNG
        defaultMastrUnitShouldBeFound("datumLetzteAktualisierung.greaterThanOrEqual=" + DEFAULT_DATUM_LETZTE_AKTUALISIERUNG);

        // Get all the mastrUnitList where datumLetzteAktualisierung is greater than or equal to UPDATED_DATUM_LETZTE_AKTUALISIERUNG
        defaultMastrUnitShouldNotBeFound("datumLetzteAktualisierung.greaterThanOrEqual=" + UPDATED_DATUM_LETZTE_AKTUALISIERUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByDatumLetzteAktualisierungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where datumLetzteAktualisierung is less than or equal to DEFAULT_DATUM_LETZTE_AKTUALISIERUNG
        defaultMastrUnitShouldBeFound("datumLetzteAktualisierung.lessThanOrEqual=" + DEFAULT_DATUM_LETZTE_AKTUALISIERUNG);

        // Get all the mastrUnitList where datumLetzteAktualisierung is less than or equal to SMALLER_DATUM_LETZTE_AKTUALISIERUNG
        defaultMastrUnitShouldNotBeFound("datumLetzteAktualisierung.lessThanOrEqual=" + SMALLER_DATUM_LETZTE_AKTUALISIERUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByDatumLetzteAktualisierungIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where datumLetzteAktualisierung is less than DEFAULT_DATUM_LETZTE_AKTUALISIERUNG
        defaultMastrUnitShouldNotBeFound("datumLetzteAktualisierung.lessThan=" + DEFAULT_DATUM_LETZTE_AKTUALISIERUNG);

        // Get all the mastrUnitList where datumLetzteAktualisierung is less than UPDATED_DATUM_LETZTE_AKTUALISIERUNG
        defaultMastrUnitShouldBeFound("datumLetzteAktualisierung.lessThan=" + UPDATED_DATUM_LETZTE_AKTUALISIERUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByDatumLetzteAktualisierungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where datumLetzteAktualisierung is greater than DEFAULT_DATUM_LETZTE_AKTUALISIERUNG
        defaultMastrUnitShouldNotBeFound("datumLetzteAktualisierung.greaterThan=" + DEFAULT_DATUM_LETZTE_AKTUALISIERUNG);

        // Get all the mastrUnitList where datumLetzteAktualisierung is greater than SMALLER_DATUM_LETZTE_AKTUALISIERUNG
        defaultMastrUnitShouldBeFound("datumLetzteAktualisierung.greaterThan=" + SMALLER_DATUM_LETZTE_AKTUALISIERUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEinheitRegistrierungsDatumIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where einheitRegistrierungsDatum equals to DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("einheitRegistrierungsDatum.equals=" + DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where einheitRegistrierungsDatum equals to UPDATED_EINHEIT_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("einheitRegistrierungsDatum.equals=" + UPDATED_EINHEIT_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEinheitRegistrierungsDatumIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where einheitRegistrierungsDatum in DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM or UPDATED_EINHEIT_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound(
            "einheitRegistrierungsDatum.in=" + DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM + "," + UPDATED_EINHEIT_REGISTRIERUNGS_DATUM
        );

        // Get all the mastrUnitList where einheitRegistrierungsDatum equals to UPDATED_EINHEIT_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("einheitRegistrierungsDatum.in=" + UPDATED_EINHEIT_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEinheitRegistrierungsDatumIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where einheitRegistrierungsDatum is not null
        defaultMastrUnitShouldBeFound("einheitRegistrierungsDatum.specified=true");

        // Get all the mastrUnitList where einheitRegistrierungsDatum is null
        defaultMastrUnitShouldNotBeFound("einheitRegistrierungsDatum.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEinheitRegistrierungsDatumIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where einheitRegistrierungsDatum is greater than or equal to DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("einheitRegistrierungsDatum.greaterThanOrEqual=" + DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where einheitRegistrierungsDatum is greater than or equal to UPDATED_EINHEIT_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("einheitRegistrierungsDatum.greaterThanOrEqual=" + UPDATED_EINHEIT_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEinheitRegistrierungsDatumIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where einheitRegistrierungsDatum is less than or equal to DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("einheitRegistrierungsDatum.lessThanOrEqual=" + DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where einheitRegistrierungsDatum is less than or equal to SMALLER_EINHEIT_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("einheitRegistrierungsDatum.lessThanOrEqual=" + SMALLER_EINHEIT_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEinheitRegistrierungsDatumIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where einheitRegistrierungsDatum is less than DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("einheitRegistrierungsDatum.lessThan=" + DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where einheitRegistrierungsDatum is less than UPDATED_EINHEIT_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("einheitRegistrierungsDatum.lessThan=" + UPDATED_EINHEIT_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEinheitRegistrierungsDatumIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where einheitRegistrierungsDatum is greater than DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("einheitRegistrierungsDatum.greaterThan=" + DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where einheitRegistrierungsDatum is greater than SMALLER_EINHEIT_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("einheitRegistrierungsDatum.greaterThan=" + SMALLER_EINHEIT_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEndgueltigeStilllegungDatumIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where endgueltigeStilllegungDatum equals to DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM
        defaultMastrUnitShouldBeFound("endgueltigeStilllegungDatum.equals=" + DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM);

        // Get all the mastrUnitList where endgueltigeStilllegungDatum equals to UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM
        defaultMastrUnitShouldNotBeFound("endgueltigeStilllegungDatum.equals=" + UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEndgueltigeStilllegungDatumIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where endgueltigeStilllegungDatum in DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM or UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM
        defaultMastrUnitShouldBeFound(
            "endgueltigeStilllegungDatum.in=" + DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM + "," + UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM
        );

        // Get all the mastrUnitList where endgueltigeStilllegungDatum equals to UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM
        defaultMastrUnitShouldNotBeFound("endgueltigeStilllegungDatum.in=" + UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEndgueltigeStilllegungDatumIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where endgueltigeStilllegungDatum is not null
        defaultMastrUnitShouldBeFound("endgueltigeStilllegungDatum.specified=true");

        // Get all the mastrUnitList where endgueltigeStilllegungDatum is null
        defaultMastrUnitShouldNotBeFound("endgueltigeStilllegungDatum.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEndgueltigeStilllegungDatumIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where endgueltigeStilllegungDatum is greater than or equal to DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM
        defaultMastrUnitShouldBeFound("endgueltigeStilllegungDatum.greaterThanOrEqual=" + DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM);

        // Get all the mastrUnitList where endgueltigeStilllegungDatum is greater than or equal to UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM
        defaultMastrUnitShouldNotBeFound("endgueltigeStilllegungDatum.greaterThanOrEqual=" + UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEndgueltigeStilllegungDatumIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where endgueltigeStilllegungDatum is less than or equal to DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM
        defaultMastrUnitShouldBeFound("endgueltigeStilllegungDatum.lessThanOrEqual=" + DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM);

        // Get all the mastrUnitList where endgueltigeStilllegungDatum is less than or equal to SMALLER_ENDGUELTIGE_STILLLEGUNG_DATUM
        defaultMastrUnitShouldNotBeFound("endgueltigeStilllegungDatum.lessThanOrEqual=" + SMALLER_ENDGUELTIGE_STILLLEGUNG_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEndgueltigeStilllegungDatumIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where endgueltigeStilllegungDatum is less than DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM
        defaultMastrUnitShouldNotBeFound("endgueltigeStilllegungDatum.lessThan=" + DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM);

        // Get all the mastrUnitList where endgueltigeStilllegungDatum is less than UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM
        defaultMastrUnitShouldBeFound("endgueltigeStilllegungDatum.lessThan=" + UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEndgueltigeStilllegungDatumIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where endgueltigeStilllegungDatum is greater than DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM
        defaultMastrUnitShouldNotBeFound("endgueltigeStilllegungDatum.greaterThan=" + DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM);

        // Get all the mastrUnitList where endgueltigeStilllegungDatum is greater than SMALLER_ENDGUELTIGE_STILLLEGUNG_DATUM
        defaultMastrUnitShouldBeFound("endgueltigeStilllegungDatum.greaterThan=" + SMALLER_ENDGUELTIGE_STILLLEGUNG_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGeplantesInbetriebsnahmeDatumIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum equals to DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM
        defaultMastrUnitShouldBeFound("geplantesInbetriebsnahmeDatum.equals=" + DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM);

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum equals to UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("geplantesInbetriebsnahmeDatum.equals=" + UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGeplantesInbetriebsnahmeDatumIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum in DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM or UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM
        defaultMastrUnitShouldBeFound(
            "geplantesInbetriebsnahmeDatum.in=" + DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM + "," + UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM
        );

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum equals to UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("geplantesInbetriebsnahmeDatum.in=" + UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGeplantesInbetriebsnahmeDatumIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum is not null
        defaultMastrUnitShouldBeFound("geplantesInbetriebsnahmeDatum.specified=true");

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum is null
        defaultMastrUnitShouldNotBeFound("geplantesInbetriebsnahmeDatum.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGeplantesInbetriebsnahmeDatumIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum is greater than or equal to DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM
        defaultMastrUnitShouldBeFound("geplantesInbetriebsnahmeDatum.greaterThanOrEqual=" + DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM);

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum is greater than or equal to UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("geplantesInbetriebsnahmeDatum.greaterThanOrEqual=" + UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGeplantesInbetriebsnahmeDatumIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum is less than or equal to DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM
        defaultMastrUnitShouldBeFound("geplantesInbetriebsnahmeDatum.lessThanOrEqual=" + DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM);

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum is less than or equal to SMALLER_GEPLANTES_INBETRIEBSNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("geplantesInbetriebsnahmeDatum.lessThanOrEqual=" + SMALLER_GEPLANTES_INBETRIEBSNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGeplantesInbetriebsnahmeDatumIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum is less than DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("geplantesInbetriebsnahmeDatum.lessThan=" + DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM);

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum is less than UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM
        defaultMastrUnitShouldBeFound("geplantesInbetriebsnahmeDatum.lessThan=" + UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGeplantesInbetriebsnahmeDatumIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum is greater than DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("geplantesInbetriebsnahmeDatum.greaterThan=" + DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM);

        // Get all the mastrUnitList where geplantesInbetriebsnahmeDatum is greater than SMALLER_GEPLANTES_INBETRIEBSNAHME_DATUM
        defaultMastrUnitShouldBeFound("geplantesInbetriebsnahmeDatum.greaterThan=" + SMALLER_GEPLANTES_INBETRIEBSNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByInbetriebnahmeDatumIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where inbetriebnahmeDatum equals to DEFAULT_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("inbetriebnahmeDatum.equals=" + DEFAULT_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where inbetriebnahmeDatum equals to UPDATED_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("inbetriebnahmeDatum.equals=" + UPDATED_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByInbetriebnahmeDatumIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where inbetriebnahmeDatum in DEFAULT_INBETRIEBNAHME_DATUM or UPDATED_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("inbetriebnahmeDatum.in=" + DEFAULT_INBETRIEBNAHME_DATUM + "," + UPDATED_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where inbetriebnahmeDatum equals to UPDATED_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("inbetriebnahmeDatum.in=" + UPDATED_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByInbetriebnahmeDatumIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where inbetriebnahmeDatum is not null
        defaultMastrUnitShouldBeFound("inbetriebnahmeDatum.specified=true");

        // Get all the mastrUnitList where inbetriebnahmeDatum is null
        defaultMastrUnitShouldNotBeFound("inbetriebnahmeDatum.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByInbetriebnahmeDatumIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where inbetriebnahmeDatum is greater than or equal to DEFAULT_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("inbetriebnahmeDatum.greaterThanOrEqual=" + DEFAULT_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where inbetriebnahmeDatum is greater than or equal to UPDATED_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("inbetriebnahmeDatum.greaterThanOrEqual=" + UPDATED_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByInbetriebnahmeDatumIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where inbetriebnahmeDatum is less than or equal to DEFAULT_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("inbetriebnahmeDatum.lessThanOrEqual=" + DEFAULT_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where inbetriebnahmeDatum is less than or equal to SMALLER_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("inbetriebnahmeDatum.lessThanOrEqual=" + SMALLER_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByInbetriebnahmeDatumIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where inbetriebnahmeDatum is less than DEFAULT_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("inbetriebnahmeDatum.lessThan=" + DEFAULT_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where inbetriebnahmeDatum is less than UPDATED_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("inbetriebnahmeDatum.lessThan=" + UPDATED_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByInbetriebnahmeDatumIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where inbetriebnahmeDatum is greater than DEFAULT_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("inbetriebnahmeDatum.greaterThan=" + DEFAULT_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where inbetriebnahmeDatum is greater than SMALLER_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("inbetriebnahmeDatum.greaterThan=" + SMALLER_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageInbetriebnahmeDatumIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum equals to DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("kwkAnlageInbetriebnahmeDatum.equals=" + DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum equals to UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("kwkAnlageInbetriebnahmeDatum.equals=" + UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageInbetriebnahmeDatumIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum in DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM or UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound(
            "kwkAnlageInbetriebnahmeDatum.in=" + DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM + "," + UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        );

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum equals to UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("kwkAnlageInbetriebnahmeDatum.in=" + UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageInbetriebnahmeDatumIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum is not null
        defaultMastrUnitShouldBeFound("kwkAnlageInbetriebnahmeDatum.specified=true");

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum is null
        defaultMastrUnitShouldNotBeFound("kwkAnlageInbetriebnahmeDatum.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageInbetriebnahmeDatumIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum is greater than or equal to DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("kwkAnlageInbetriebnahmeDatum.greaterThanOrEqual=" + DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum is greater than or equal to UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("kwkAnlageInbetriebnahmeDatum.greaterThanOrEqual=" + UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageInbetriebnahmeDatumIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum is less than or equal to DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("kwkAnlageInbetriebnahmeDatum.lessThanOrEqual=" + DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum is less than or equal to SMALLER_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("kwkAnlageInbetriebnahmeDatum.lessThanOrEqual=" + SMALLER_KWK_ANLAGE_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageInbetriebnahmeDatumIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum is less than DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("kwkAnlageInbetriebnahmeDatum.lessThan=" + DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum is less than UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("kwkAnlageInbetriebnahmeDatum.lessThan=" + UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageInbetriebnahmeDatumIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum is greater than DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("kwkAnlageInbetriebnahmeDatum.greaterThan=" + DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where kwkAnlageInbetriebnahmeDatum is greater than SMALLER_KWK_ANLAGE_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("kwkAnlageInbetriebnahmeDatum.greaterThan=" + SMALLER_KWK_ANLAGE_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageRegistrierungsDatumIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum equals to DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("kwkAnlageRegistrierungsDatum.equals=" + DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum equals to UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("kwkAnlageRegistrierungsDatum.equals=" + UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageRegistrierungsDatumIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum in DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM or UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound(
            "kwkAnlageRegistrierungsDatum.in=" + DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM + "," + UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        );

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum equals to UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("kwkAnlageRegistrierungsDatum.in=" + UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageRegistrierungsDatumIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum is not null
        defaultMastrUnitShouldBeFound("kwkAnlageRegistrierungsDatum.specified=true");

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum is null
        defaultMastrUnitShouldNotBeFound("kwkAnlageRegistrierungsDatum.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageRegistrierungsDatumIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum is greater than or equal to DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("kwkAnlageRegistrierungsDatum.greaterThanOrEqual=" + DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum is greater than or equal to UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("kwkAnlageRegistrierungsDatum.greaterThanOrEqual=" + UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageRegistrierungsDatumIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum is less than or equal to DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("kwkAnlageRegistrierungsDatum.lessThanOrEqual=" + DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum is less than or equal to SMALLER_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("kwkAnlageRegistrierungsDatum.lessThanOrEqual=" + SMALLER_KWK_ANLAGE_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageRegistrierungsDatumIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum is less than DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("kwkAnlageRegistrierungsDatum.lessThan=" + DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum is less than UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("kwkAnlageRegistrierungsDatum.lessThan=" + UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageRegistrierungsDatumIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum is greater than DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("kwkAnlageRegistrierungsDatum.greaterThan=" + DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where kwkAnlageRegistrierungsDatum is greater than SMALLER_KWK_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("kwkAnlageRegistrierungsDatum.greaterThan=" + SMALLER_KWK_ANLAGE_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInbetriebnahmeDatumIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInbetriebnahmeDatum equals to DEFAULT_EEG_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("eegInbetriebnahmeDatum.equals=" + DEFAULT_EEG_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where eegInbetriebnahmeDatum equals to UPDATED_EEG_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("eegInbetriebnahmeDatum.equals=" + UPDATED_EEG_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInbetriebnahmeDatumIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInbetriebnahmeDatum in DEFAULT_EEG_INBETRIEBNAHME_DATUM or UPDATED_EEG_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound(
            "eegInbetriebnahmeDatum.in=" + DEFAULT_EEG_INBETRIEBNAHME_DATUM + "," + UPDATED_EEG_INBETRIEBNAHME_DATUM
        );

        // Get all the mastrUnitList where eegInbetriebnahmeDatum equals to UPDATED_EEG_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("eegInbetriebnahmeDatum.in=" + UPDATED_EEG_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInbetriebnahmeDatumIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInbetriebnahmeDatum is not null
        defaultMastrUnitShouldBeFound("eegInbetriebnahmeDatum.specified=true");

        // Get all the mastrUnitList where eegInbetriebnahmeDatum is null
        defaultMastrUnitShouldNotBeFound("eegInbetriebnahmeDatum.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInbetriebnahmeDatumIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInbetriebnahmeDatum is greater than or equal to DEFAULT_EEG_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("eegInbetriebnahmeDatum.greaterThanOrEqual=" + DEFAULT_EEG_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where eegInbetriebnahmeDatum is greater than or equal to UPDATED_EEG_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("eegInbetriebnahmeDatum.greaterThanOrEqual=" + UPDATED_EEG_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInbetriebnahmeDatumIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInbetriebnahmeDatum is less than or equal to DEFAULT_EEG_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("eegInbetriebnahmeDatum.lessThanOrEqual=" + DEFAULT_EEG_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where eegInbetriebnahmeDatum is less than or equal to SMALLER_EEG_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("eegInbetriebnahmeDatum.lessThanOrEqual=" + SMALLER_EEG_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInbetriebnahmeDatumIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInbetriebnahmeDatum is less than DEFAULT_EEG_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("eegInbetriebnahmeDatum.lessThan=" + DEFAULT_EEG_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where eegInbetriebnahmeDatum is less than UPDATED_EEG_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("eegInbetriebnahmeDatum.lessThan=" + UPDATED_EEG_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInbetriebnahmeDatumIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInbetriebnahmeDatum is greater than DEFAULT_EEG_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldNotBeFound("eegInbetriebnahmeDatum.greaterThan=" + DEFAULT_EEG_INBETRIEBNAHME_DATUM);

        // Get all the mastrUnitList where eegInbetriebnahmeDatum is greater than SMALLER_EEG_INBETRIEBNAHME_DATUM
        defaultMastrUnitShouldBeFound("eegInbetriebnahmeDatum.greaterThan=" + SMALLER_EEG_INBETRIEBNAHME_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlageRegistrierungsDatumIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum equals to DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("eegAnlageRegistrierungsDatum.equals=" + DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum equals to UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("eegAnlageRegistrierungsDatum.equals=" + UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlageRegistrierungsDatumIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum in DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM or UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound(
            "eegAnlageRegistrierungsDatum.in=" + DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM + "," + UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        );

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum equals to UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("eegAnlageRegistrierungsDatum.in=" + UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlageRegistrierungsDatumIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum is not null
        defaultMastrUnitShouldBeFound("eegAnlageRegistrierungsDatum.specified=true");

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum is null
        defaultMastrUnitShouldNotBeFound("eegAnlageRegistrierungsDatum.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlageRegistrierungsDatumIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum is greater than or equal to DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("eegAnlageRegistrierungsDatum.greaterThanOrEqual=" + DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum is greater than or equal to UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("eegAnlageRegistrierungsDatum.greaterThanOrEqual=" + UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlageRegistrierungsDatumIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum is less than or equal to DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("eegAnlageRegistrierungsDatum.lessThanOrEqual=" + DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum is less than or equal to SMALLER_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("eegAnlageRegistrierungsDatum.lessThanOrEqual=" + SMALLER_EEG_ANLAGE_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlageRegistrierungsDatumIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum is less than DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("eegAnlageRegistrierungsDatum.lessThan=" + DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum is less than UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("eegAnlageRegistrierungsDatum.lessThan=" + UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlageRegistrierungsDatumIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum is greater than DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("eegAnlageRegistrierungsDatum.greaterThan=" + DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where eegAnlageRegistrierungsDatum is greater than SMALLER_EEG_ANLAGE_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("eegAnlageRegistrierungsDatum.greaterThan=" + SMALLER_EEG_ANLAGE_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungDatumIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungDatum equals to DEFAULT_GENEHMIGUNG_DATUM
        defaultMastrUnitShouldBeFound("genehmigungDatum.equals=" + DEFAULT_GENEHMIGUNG_DATUM);

        // Get all the mastrUnitList where genehmigungDatum equals to UPDATED_GENEHMIGUNG_DATUM
        defaultMastrUnitShouldNotBeFound("genehmigungDatum.equals=" + UPDATED_GENEHMIGUNG_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungDatumIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungDatum in DEFAULT_GENEHMIGUNG_DATUM or UPDATED_GENEHMIGUNG_DATUM
        defaultMastrUnitShouldBeFound("genehmigungDatum.in=" + DEFAULT_GENEHMIGUNG_DATUM + "," + UPDATED_GENEHMIGUNG_DATUM);

        // Get all the mastrUnitList where genehmigungDatum equals to UPDATED_GENEHMIGUNG_DATUM
        defaultMastrUnitShouldNotBeFound("genehmigungDatum.in=" + UPDATED_GENEHMIGUNG_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungDatumIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungDatum is not null
        defaultMastrUnitShouldBeFound("genehmigungDatum.specified=true");

        // Get all the mastrUnitList where genehmigungDatum is null
        defaultMastrUnitShouldNotBeFound("genehmigungDatum.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungDatumIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungDatum is greater than or equal to DEFAULT_GENEHMIGUNG_DATUM
        defaultMastrUnitShouldBeFound("genehmigungDatum.greaterThanOrEqual=" + DEFAULT_GENEHMIGUNG_DATUM);

        // Get all the mastrUnitList where genehmigungDatum is greater than or equal to UPDATED_GENEHMIGUNG_DATUM
        defaultMastrUnitShouldNotBeFound("genehmigungDatum.greaterThanOrEqual=" + UPDATED_GENEHMIGUNG_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungDatumIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungDatum is less than or equal to DEFAULT_GENEHMIGUNG_DATUM
        defaultMastrUnitShouldBeFound("genehmigungDatum.lessThanOrEqual=" + DEFAULT_GENEHMIGUNG_DATUM);

        // Get all the mastrUnitList where genehmigungDatum is less than or equal to SMALLER_GENEHMIGUNG_DATUM
        defaultMastrUnitShouldNotBeFound("genehmigungDatum.lessThanOrEqual=" + SMALLER_GENEHMIGUNG_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungDatumIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungDatum is less than DEFAULT_GENEHMIGUNG_DATUM
        defaultMastrUnitShouldNotBeFound("genehmigungDatum.lessThan=" + DEFAULT_GENEHMIGUNG_DATUM);

        // Get all the mastrUnitList where genehmigungDatum is less than UPDATED_GENEHMIGUNG_DATUM
        defaultMastrUnitShouldBeFound("genehmigungDatum.lessThan=" + UPDATED_GENEHMIGUNG_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungDatumIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungDatum is greater than DEFAULT_GENEHMIGUNG_DATUM
        defaultMastrUnitShouldNotBeFound("genehmigungDatum.greaterThan=" + DEFAULT_GENEHMIGUNG_DATUM);

        // Get all the mastrUnitList where genehmigungDatum is greater than SMALLER_GENEHMIGUNG_DATUM
        defaultMastrUnitShouldBeFound("genehmigungDatum.greaterThan=" + SMALLER_GENEHMIGUNG_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungRegistrierungsDatumIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum equals to DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("genehmigungRegistrierungsDatum.equals=" + DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum equals to UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("genehmigungRegistrierungsDatum.equals=" + UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungRegistrierungsDatumIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum in DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM or UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound(
            "genehmigungRegistrierungsDatum.in=" + DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM + "," + UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        );

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum equals to UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("genehmigungRegistrierungsDatum.in=" + UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungRegistrierungsDatumIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum is not null
        defaultMastrUnitShouldBeFound("genehmigungRegistrierungsDatum.specified=true");

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum is null
        defaultMastrUnitShouldNotBeFound("genehmigungRegistrierungsDatum.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungRegistrierungsDatumIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum is greater than or equal to DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("genehmigungRegistrierungsDatum.greaterThanOrEqual=" + DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum is greater than or equal to UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("genehmigungRegistrierungsDatum.greaterThanOrEqual=" + UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungRegistrierungsDatumIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum is less than or equal to DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("genehmigungRegistrierungsDatum.lessThanOrEqual=" + DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum is less than or equal to SMALLER_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("genehmigungRegistrierungsDatum.lessThanOrEqual=" + SMALLER_GENEHMIGUNG_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungRegistrierungsDatumIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum is less than DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("genehmigungRegistrierungsDatum.lessThan=" + DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum is less than UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("genehmigungRegistrierungsDatum.lessThan=" + UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungRegistrierungsDatumIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum is greater than DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldNotBeFound("genehmigungRegistrierungsDatum.greaterThan=" + DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM);

        // Get all the mastrUnitList where genehmigungRegistrierungsDatum is greater than SMALLER_GENEHMIGUNG_REGISTRIERUNGS_DATUM
        defaultMastrUnitShouldBeFound("genehmigungRegistrierungsDatum.greaterThan=" + SMALLER_GENEHMIGUNG_REGISTRIERUNGS_DATUM);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsNbPruefungAbgeschlossenIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen equals to DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        defaultMastrUnitShouldBeFound("isNbPruefungAbgeschlossen.equals=" + DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN);

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen equals to UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        defaultMastrUnitShouldNotBeFound("isNbPruefungAbgeschlossen.equals=" + UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsNbPruefungAbgeschlossenIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen in DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN or UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        defaultMastrUnitShouldBeFound(
            "isNbPruefungAbgeschlossen.in=" + DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN + "," + UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        );

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen equals to UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        defaultMastrUnitShouldNotBeFound("isNbPruefungAbgeschlossen.in=" + UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsNbPruefungAbgeschlossenIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen is not null
        defaultMastrUnitShouldBeFound("isNbPruefungAbgeschlossen.specified=true");

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen is null
        defaultMastrUnitShouldNotBeFound("isNbPruefungAbgeschlossen.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsNbPruefungAbgeschlossenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen is greater than or equal to DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        defaultMastrUnitShouldBeFound("isNbPruefungAbgeschlossen.greaterThanOrEqual=" + DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN);

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen is greater than or equal to UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        defaultMastrUnitShouldNotBeFound("isNbPruefungAbgeschlossen.greaterThanOrEqual=" + UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsNbPruefungAbgeschlossenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen is less than or equal to DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        defaultMastrUnitShouldBeFound("isNbPruefungAbgeschlossen.lessThanOrEqual=" + DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN);

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen is less than or equal to SMALLER_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        defaultMastrUnitShouldNotBeFound("isNbPruefungAbgeschlossen.lessThanOrEqual=" + SMALLER_IS_NB_PRUEFUNG_ABGESCHLOSSEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsNbPruefungAbgeschlossenIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen is less than DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        defaultMastrUnitShouldNotBeFound("isNbPruefungAbgeschlossen.lessThan=" + DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN);

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen is less than UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        defaultMastrUnitShouldBeFound("isNbPruefungAbgeschlossen.lessThan=" + UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsNbPruefungAbgeschlossenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen is greater than DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        defaultMastrUnitShouldNotBeFound("isNbPruefungAbgeschlossen.greaterThan=" + DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN);

        // Get all the mastrUnitList where isNbPruefungAbgeschlossen is greater than SMALLER_IS_NB_PRUEFUNG_ABGESCHLOSSEN
        defaultMastrUnitShouldBeFound("isNbPruefungAbgeschlossen.greaterThan=" + SMALLER_IS_NB_PRUEFUNG_ABGESCHLOSSEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsAnonymisiertIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isAnonymisiert equals to DEFAULT_IS_ANONYMISIERT
        defaultMastrUnitShouldBeFound("isAnonymisiert.equals=" + DEFAULT_IS_ANONYMISIERT);

        // Get all the mastrUnitList where isAnonymisiert equals to UPDATED_IS_ANONYMISIERT
        defaultMastrUnitShouldNotBeFound("isAnonymisiert.equals=" + UPDATED_IS_ANONYMISIERT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsAnonymisiertIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isAnonymisiert in DEFAULT_IS_ANONYMISIERT or UPDATED_IS_ANONYMISIERT
        defaultMastrUnitShouldBeFound("isAnonymisiert.in=" + DEFAULT_IS_ANONYMISIERT + "," + UPDATED_IS_ANONYMISIERT);

        // Get all the mastrUnitList where isAnonymisiert equals to UPDATED_IS_ANONYMISIERT
        defaultMastrUnitShouldNotBeFound("isAnonymisiert.in=" + UPDATED_IS_ANONYMISIERT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsAnonymisiertIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isAnonymisiert is not null
        defaultMastrUnitShouldBeFound("isAnonymisiert.specified=true");

        // Get all the mastrUnitList where isAnonymisiert is null
        defaultMastrUnitShouldNotBeFound("isAnonymisiert.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsBuergerEnergieIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isBuergerEnergie equals to DEFAULT_IS_BUERGER_ENERGIE
        defaultMastrUnitShouldBeFound("isBuergerEnergie.equals=" + DEFAULT_IS_BUERGER_ENERGIE);

        // Get all the mastrUnitList where isBuergerEnergie equals to UPDATED_IS_BUERGER_ENERGIE
        defaultMastrUnitShouldNotBeFound("isBuergerEnergie.equals=" + UPDATED_IS_BUERGER_ENERGIE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsBuergerEnergieIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isBuergerEnergie in DEFAULT_IS_BUERGER_ENERGIE or UPDATED_IS_BUERGER_ENERGIE
        defaultMastrUnitShouldBeFound("isBuergerEnergie.in=" + DEFAULT_IS_BUERGER_ENERGIE + "," + UPDATED_IS_BUERGER_ENERGIE);

        // Get all the mastrUnitList where isBuergerEnergie equals to UPDATED_IS_BUERGER_ENERGIE
        defaultMastrUnitShouldNotBeFound("isBuergerEnergie.in=" + UPDATED_IS_BUERGER_ENERGIE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsBuergerEnergieIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isBuergerEnergie is not null
        defaultMastrUnitShouldBeFound("isBuergerEnergie.specified=true");

        // Get all the mastrUnitList where isBuergerEnergie is null
        defaultMastrUnitShouldNotBeFound("isBuergerEnergie.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsEinheitNotstromaggregatIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isEinheitNotstromaggregat equals to DEFAULT_IS_EINHEIT_NOTSTROMAGGREGAT
        defaultMastrUnitShouldBeFound("isEinheitNotstromaggregat.equals=" + DEFAULT_IS_EINHEIT_NOTSTROMAGGREGAT);

        // Get all the mastrUnitList where isEinheitNotstromaggregat equals to UPDATED_IS_EINHEIT_NOTSTROMAGGREGAT
        defaultMastrUnitShouldNotBeFound("isEinheitNotstromaggregat.equals=" + UPDATED_IS_EINHEIT_NOTSTROMAGGREGAT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsEinheitNotstromaggregatIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isEinheitNotstromaggregat in DEFAULT_IS_EINHEIT_NOTSTROMAGGREGAT or UPDATED_IS_EINHEIT_NOTSTROMAGGREGAT
        defaultMastrUnitShouldBeFound(
            "isEinheitNotstromaggregat.in=" + DEFAULT_IS_EINHEIT_NOTSTROMAGGREGAT + "," + UPDATED_IS_EINHEIT_NOTSTROMAGGREGAT
        );

        // Get all the mastrUnitList where isEinheitNotstromaggregat equals to UPDATED_IS_EINHEIT_NOTSTROMAGGREGAT
        defaultMastrUnitShouldNotBeFound("isEinheitNotstromaggregat.in=" + UPDATED_IS_EINHEIT_NOTSTROMAGGREGAT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsEinheitNotstromaggregatIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isEinheitNotstromaggregat is not null
        defaultMastrUnitShouldBeFound("isEinheitNotstromaggregat.specified=true");

        // Get all the mastrUnitList where isEinheitNotstromaggregat is null
        defaultMastrUnitShouldNotBeFound("isEinheitNotstromaggregat.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsMieterstromAngemeldetIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isMieterstromAngemeldet equals to DEFAULT_IS_MIETERSTROM_ANGEMELDET
        defaultMastrUnitShouldBeFound("isMieterstromAngemeldet.equals=" + DEFAULT_IS_MIETERSTROM_ANGEMELDET);

        // Get all the mastrUnitList where isMieterstromAngemeldet equals to UPDATED_IS_MIETERSTROM_ANGEMELDET
        defaultMastrUnitShouldNotBeFound("isMieterstromAngemeldet.equals=" + UPDATED_IS_MIETERSTROM_ANGEMELDET);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsMieterstromAngemeldetIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isMieterstromAngemeldet in DEFAULT_IS_MIETERSTROM_ANGEMELDET or UPDATED_IS_MIETERSTROM_ANGEMELDET
        defaultMastrUnitShouldBeFound(
            "isMieterstromAngemeldet.in=" + DEFAULT_IS_MIETERSTROM_ANGEMELDET + "," + UPDATED_IS_MIETERSTROM_ANGEMELDET
        );

        // Get all the mastrUnitList where isMieterstromAngemeldet equals to UPDATED_IS_MIETERSTROM_ANGEMELDET
        defaultMastrUnitShouldNotBeFound("isMieterstromAngemeldet.in=" + UPDATED_IS_MIETERSTROM_ANGEMELDET);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsMieterstromAngemeldetIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isMieterstromAngemeldet is not null
        defaultMastrUnitShouldBeFound("isMieterstromAngemeldet.specified=true");

        // Get all the mastrUnitList where isMieterstromAngemeldet is null
        defaultMastrUnitShouldNotBeFound("isMieterstromAngemeldet.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsWasserkraftErtuechtigungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isWasserkraftErtuechtigung equals to DEFAULT_IS_WASSERKRAFT_ERTUECHTIGUNG
        defaultMastrUnitShouldBeFound("isWasserkraftErtuechtigung.equals=" + DEFAULT_IS_WASSERKRAFT_ERTUECHTIGUNG);

        // Get all the mastrUnitList where isWasserkraftErtuechtigung equals to UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG
        defaultMastrUnitShouldNotBeFound("isWasserkraftErtuechtigung.equals=" + UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsWasserkraftErtuechtigungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isWasserkraftErtuechtigung in DEFAULT_IS_WASSERKRAFT_ERTUECHTIGUNG or UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG
        defaultMastrUnitShouldBeFound(
            "isWasserkraftErtuechtigung.in=" + DEFAULT_IS_WASSERKRAFT_ERTUECHTIGUNG + "," + UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG
        );

        // Get all the mastrUnitList where isWasserkraftErtuechtigung equals to UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG
        defaultMastrUnitShouldNotBeFound("isWasserkraftErtuechtigung.in=" + UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsWasserkraftErtuechtigungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isWasserkraftErtuechtigung is not null
        defaultMastrUnitShouldBeFound("isWasserkraftErtuechtigung.specified=true");

        // Get all the mastrUnitList where isWasserkraftErtuechtigung is null
        defaultMastrUnitShouldNotBeFound("isWasserkraftErtuechtigung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsPilotWindanlageIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isPilotWindanlage equals to DEFAULT_IS_PILOT_WINDANLAGE
        defaultMastrUnitShouldBeFound("isPilotWindanlage.equals=" + DEFAULT_IS_PILOT_WINDANLAGE);

        // Get all the mastrUnitList where isPilotWindanlage equals to UPDATED_IS_PILOT_WINDANLAGE
        defaultMastrUnitShouldNotBeFound("isPilotWindanlage.equals=" + UPDATED_IS_PILOT_WINDANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsPilotWindanlageIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isPilotWindanlage in DEFAULT_IS_PILOT_WINDANLAGE or UPDATED_IS_PILOT_WINDANLAGE
        defaultMastrUnitShouldBeFound("isPilotWindanlage.in=" + DEFAULT_IS_PILOT_WINDANLAGE + "," + UPDATED_IS_PILOT_WINDANLAGE);

        // Get all the mastrUnitList where isPilotWindanlage equals to UPDATED_IS_PILOT_WINDANLAGE
        defaultMastrUnitShouldNotBeFound("isPilotWindanlage.in=" + UPDATED_IS_PILOT_WINDANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsPilotWindanlageIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isPilotWindanlage is not null
        defaultMastrUnitShouldBeFound("isPilotWindanlage.specified=true");

        // Get all the mastrUnitList where isPilotWindanlage is null
        defaultMastrUnitShouldNotBeFound("isPilotWindanlage.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsPrototypAnlageIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isPrototypAnlage equals to DEFAULT_IS_PROTOTYP_ANLAGE
        defaultMastrUnitShouldBeFound("isPrototypAnlage.equals=" + DEFAULT_IS_PROTOTYP_ANLAGE);

        // Get all the mastrUnitList where isPrototypAnlage equals to UPDATED_IS_PROTOTYP_ANLAGE
        defaultMastrUnitShouldNotBeFound("isPrototypAnlage.equals=" + UPDATED_IS_PROTOTYP_ANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsPrototypAnlageIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isPrototypAnlage in DEFAULT_IS_PROTOTYP_ANLAGE or UPDATED_IS_PROTOTYP_ANLAGE
        defaultMastrUnitShouldBeFound("isPrototypAnlage.in=" + DEFAULT_IS_PROTOTYP_ANLAGE + "," + UPDATED_IS_PROTOTYP_ANLAGE);

        // Get all the mastrUnitList where isPrototypAnlage equals to UPDATED_IS_PROTOTYP_ANLAGE
        defaultMastrUnitShouldNotBeFound("isPrototypAnlage.in=" + UPDATED_IS_PROTOTYP_ANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByIsPrototypAnlageIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where isPrototypAnlage is not null
        defaultMastrUnitShouldBeFound("isPrototypAnlage.specified=true");

        // Get all the mastrUnitList where isPrototypAnlage is null
        defaultMastrUnitShouldNotBeFound("isPrototypAnlage.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLatIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lat equals to DEFAULT_LAT
        defaultMastrUnitShouldBeFound("lat.equals=" + DEFAULT_LAT);

        // Get all the mastrUnitList where lat equals to UPDATED_LAT
        defaultMastrUnitShouldNotBeFound("lat.equals=" + UPDATED_LAT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLatIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lat in DEFAULT_LAT or UPDATED_LAT
        defaultMastrUnitShouldBeFound("lat.in=" + DEFAULT_LAT + "," + UPDATED_LAT);

        // Get all the mastrUnitList where lat equals to UPDATED_LAT
        defaultMastrUnitShouldNotBeFound("lat.in=" + UPDATED_LAT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLatIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lat is not null
        defaultMastrUnitShouldBeFound("lat.specified=true");

        // Get all the mastrUnitList where lat is null
        defaultMastrUnitShouldNotBeFound("lat.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLatIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lat is greater than or equal to DEFAULT_LAT
        defaultMastrUnitShouldBeFound("lat.greaterThanOrEqual=" + DEFAULT_LAT);

        // Get all the mastrUnitList where lat is greater than or equal to UPDATED_LAT
        defaultMastrUnitShouldNotBeFound("lat.greaterThanOrEqual=" + UPDATED_LAT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLatIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lat is less than or equal to DEFAULT_LAT
        defaultMastrUnitShouldBeFound("lat.lessThanOrEqual=" + DEFAULT_LAT);

        // Get all the mastrUnitList where lat is less than or equal to SMALLER_LAT
        defaultMastrUnitShouldNotBeFound("lat.lessThanOrEqual=" + SMALLER_LAT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLatIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lat is less than DEFAULT_LAT
        defaultMastrUnitShouldNotBeFound("lat.lessThan=" + DEFAULT_LAT);

        // Get all the mastrUnitList where lat is less than UPDATED_LAT
        defaultMastrUnitShouldBeFound("lat.lessThan=" + UPDATED_LAT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLatIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lat is greater than DEFAULT_LAT
        defaultMastrUnitShouldNotBeFound("lat.greaterThan=" + DEFAULT_LAT);

        // Get all the mastrUnitList where lat is greater than SMALLER_LAT
        defaultMastrUnitShouldBeFound("lat.greaterThan=" + SMALLER_LAT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLngIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lng equals to DEFAULT_LNG
        defaultMastrUnitShouldBeFound("lng.equals=" + DEFAULT_LNG);

        // Get all the mastrUnitList where lng equals to UPDATED_LNG
        defaultMastrUnitShouldNotBeFound("lng.equals=" + UPDATED_LNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLngIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lng in DEFAULT_LNG or UPDATED_LNG
        defaultMastrUnitShouldBeFound("lng.in=" + DEFAULT_LNG + "," + UPDATED_LNG);

        // Get all the mastrUnitList where lng equals to UPDATED_LNG
        defaultMastrUnitShouldNotBeFound("lng.in=" + UPDATED_LNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLngIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lng is not null
        defaultMastrUnitShouldBeFound("lng.specified=true");

        // Get all the mastrUnitList where lng is null
        defaultMastrUnitShouldNotBeFound("lng.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLngIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lng is greater than or equal to DEFAULT_LNG
        defaultMastrUnitShouldBeFound("lng.greaterThanOrEqual=" + DEFAULT_LNG);

        // Get all the mastrUnitList where lng is greater than or equal to UPDATED_LNG
        defaultMastrUnitShouldNotBeFound("lng.greaterThanOrEqual=" + UPDATED_LNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLngIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lng is less than or equal to DEFAULT_LNG
        defaultMastrUnitShouldBeFound("lng.lessThanOrEqual=" + DEFAULT_LNG);

        // Get all the mastrUnitList where lng is less than or equal to SMALLER_LNG
        defaultMastrUnitShouldNotBeFound("lng.lessThanOrEqual=" + SMALLER_LNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLngIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lng is less than DEFAULT_LNG
        defaultMastrUnitShouldNotBeFound("lng.lessThan=" + DEFAULT_LNG);

        // Get all the mastrUnitList where lng is less than UPDATED_LNG
        defaultMastrUnitShouldBeFound("lng.lessThan=" + UPDATED_LNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLngIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lng is greater than DEFAULT_LNG
        defaultMastrUnitShouldNotBeFound("lng.greaterThan=" + DEFAULT_LNG);

        // Get all the mastrUnitList where lng is greater than SMALLER_LNG
        defaultMastrUnitShouldBeFound("lng.greaterThan=" + SMALLER_LNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByOrtIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where ort equals to DEFAULT_ORT
        defaultMastrUnitShouldBeFound("ort.equals=" + DEFAULT_ORT);

        // Get all the mastrUnitList where ort equals to UPDATED_ORT
        defaultMastrUnitShouldNotBeFound("ort.equals=" + UPDATED_ORT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByOrtIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where ort in DEFAULT_ORT or UPDATED_ORT
        defaultMastrUnitShouldBeFound("ort.in=" + DEFAULT_ORT + "," + UPDATED_ORT);

        // Get all the mastrUnitList where ort equals to UPDATED_ORT
        defaultMastrUnitShouldNotBeFound("ort.in=" + UPDATED_ORT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByOrtIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where ort is not null
        defaultMastrUnitShouldBeFound("ort.specified=true");

        // Get all the mastrUnitList where ort is null
        defaultMastrUnitShouldNotBeFound("ort.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByOrtContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where ort contains DEFAULT_ORT
        defaultMastrUnitShouldBeFound("ort.contains=" + DEFAULT_ORT);

        // Get all the mastrUnitList where ort contains UPDATED_ORT
        defaultMastrUnitShouldNotBeFound("ort.contains=" + UPDATED_ORT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByOrtNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where ort does not contain DEFAULT_ORT
        defaultMastrUnitShouldNotBeFound("ort.doesNotContain=" + DEFAULT_ORT);

        // Get all the mastrUnitList where ort does not contain UPDATED_ORT
        defaultMastrUnitShouldBeFound("ort.doesNotContain=" + UPDATED_ORT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByPlzIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where plz equals to DEFAULT_PLZ
        defaultMastrUnitShouldBeFound("plz.equals=" + DEFAULT_PLZ);

        // Get all the mastrUnitList where plz equals to UPDATED_PLZ
        defaultMastrUnitShouldNotBeFound("plz.equals=" + UPDATED_PLZ);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByPlzIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where plz in DEFAULT_PLZ or UPDATED_PLZ
        defaultMastrUnitShouldBeFound("plz.in=" + DEFAULT_PLZ + "," + UPDATED_PLZ);

        // Get all the mastrUnitList where plz equals to UPDATED_PLZ
        defaultMastrUnitShouldNotBeFound("plz.in=" + UPDATED_PLZ);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByPlzIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where plz is not null
        defaultMastrUnitShouldBeFound("plz.specified=true");

        // Get all the mastrUnitList where plz is null
        defaultMastrUnitShouldNotBeFound("plz.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByPlzIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where plz is greater than or equal to DEFAULT_PLZ
        defaultMastrUnitShouldBeFound("plz.greaterThanOrEqual=" + DEFAULT_PLZ);

        // Get all the mastrUnitList where plz is greater than or equal to UPDATED_PLZ
        defaultMastrUnitShouldNotBeFound("plz.greaterThanOrEqual=" + UPDATED_PLZ);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByPlzIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where plz is less than or equal to DEFAULT_PLZ
        defaultMastrUnitShouldBeFound("plz.lessThanOrEqual=" + DEFAULT_PLZ);

        // Get all the mastrUnitList where plz is less than or equal to SMALLER_PLZ
        defaultMastrUnitShouldNotBeFound("plz.lessThanOrEqual=" + SMALLER_PLZ);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByPlzIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where plz is less than DEFAULT_PLZ
        defaultMastrUnitShouldNotBeFound("plz.lessThan=" + DEFAULT_PLZ);

        // Get all the mastrUnitList where plz is less than UPDATED_PLZ
        defaultMastrUnitShouldBeFound("plz.lessThan=" + UPDATED_PLZ);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByPlzIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where plz is greater than DEFAULT_PLZ
        defaultMastrUnitShouldNotBeFound("plz.greaterThan=" + DEFAULT_PLZ);

        // Get all the mastrUnitList where plz is greater than SMALLER_PLZ
        defaultMastrUnitShouldBeFound("plz.greaterThan=" + SMALLER_PLZ);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByStrasseIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where strasse equals to DEFAULT_STRASSE
        defaultMastrUnitShouldBeFound("strasse.equals=" + DEFAULT_STRASSE);

        // Get all the mastrUnitList where strasse equals to UPDATED_STRASSE
        defaultMastrUnitShouldNotBeFound("strasse.equals=" + UPDATED_STRASSE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByStrasseIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where strasse in DEFAULT_STRASSE or UPDATED_STRASSE
        defaultMastrUnitShouldBeFound("strasse.in=" + DEFAULT_STRASSE + "," + UPDATED_STRASSE);

        // Get all the mastrUnitList where strasse equals to UPDATED_STRASSE
        defaultMastrUnitShouldNotBeFound("strasse.in=" + UPDATED_STRASSE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByStrasseIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where strasse is not null
        defaultMastrUnitShouldBeFound("strasse.specified=true");

        // Get all the mastrUnitList where strasse is null
        defaultMastrUnitShouldNotBeFound("strasse.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByStrasseContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where strasse contains DEFAULT_STRASSE
        defaultMastrUnitShouldBeFound("strasse.contains=" + DEFAULT_STRASSE);

        // Get all the mastrUnitList where strasse contains UPDATED_STRASSE
        defaultMastrUnitShouldNotBeFound("strasse.contains=" + UPDATED_STRASSE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByStrasseNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where strasse does not contain DEFAULT_STRASSE
        defaultMastrUnitShouldNotBeFound("strasse.doesNotContain=" + DEFAULT_STRASSE);

        // Get all the mastrUnitList where strasse does not contain UPDATED_STRASSE
        defaultMastrUnitShouldBeFound("strasse.doesNotContain=" + UPDATED_STRASSE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHausnummerIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hausnummer equals to DEFAULT_HAUSNUMMER
        defaultMastrUnitShouldBeFound("hausnummer.equals=" + DEFAULT_HAUSNUMMER);

        // Get all the mastrUnitList where hausnummer equals to UPDATED_HAUSNUMMER
        defaultMastrUnitShouldNotBeFound("hausnummer.equals=" + UPDATED_HAUSNUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHausnummerIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hausnummer in DEFAULT_HAUSNUMMER or UPDATED_HAUSNUMMER
        defaultMastrUnitShouldBeFound("hausnummer.in=" + DEFAULT_HAUSNUMMER + "," + UPDATED_HAUSNUMMER);

        // Get all the mastrUnitList where hausnummer equals to UPDATED_HAUSNUMMER
        defaultMastrUnitShouldNotBeFound("hausnummer.in=" + UPDATED_HAUSNUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHausnummerIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hausnummer is not null
        defaultMastrUnitShouldBeFound("hausnummer.specified=true");

        // Get all the mastrUnitList where hausnummer is null
        defaultMastrUnitShouldNotBeFound("hausnummer.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHausnummerContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hausnummer contains DEFAULT_HAUSNUMMER
        defaultMastrUnitShouldBeFound("hausnummer.contains=" + DEFAULT_HAUSNUMMER);

        // Get all the mastrUnitList where hausnummer contains UPDATED_HAUSNUMMER
        defaultMastrUnitShouldNotBeFound("hausnummer.contains=" + UPDATED_HAUSNUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHausnummerNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hausnummer does not contain DEFAULT_HAUSNUMMER
        defaultMastrUnitShouldNotBeFound("hausnummer.doesNotContain=" + DEFAULT_HAUSNUMMER);

        // Get all the mastrUnitList where hausnummer does not contain UPDATED_HAUSNUMMER
        defaultMastrUnitShouldBeFound("hausnummer.doesNotContain=" + UPDATED_HAUSNUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEinheitnameIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where einheitname equals to DEFAULT_EINHEITNAME
        defaultMastrUnitShouldBeFound("einheitname.equals=" + DEFAULT_EINHEITNAME);

        // Get all the mastrUnitList where einheitname equals to UPDATED_EINHEITNAME
        defaultMastrUnitShouldNotBeFound("einheitname.equals=" + UPDATED_EINHEITNAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEinheitnameIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where einheitname in DEFAULT_EINHEITNAME or UPDATED_EINHEITNAME
        defaultMastrUnitShouldBeFound("einheitname.in=" + DEFAULT_EINHEITNAME + "," + UPDATED_EINHEITNAME);

        // Get all the mastrUnitList where einheitname equals to UPDATED_EINHEITNAME
        defaultMastrUnitShouldNotBeFound("einheitname.in=" + UPDATED_EINHEITNAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEinheitnameIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where einheitname is not null
        defaultMastrUnitShouldBeFound("einheitname.specified=true");

        // Get all the mastrUnitList where einheitname is null
        defaultMastrUnitShouldNotBeFound("einheitname.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEinheitnameContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where einheitname contains DEFAULT_EINHEITNAME
        defaultMastrUnitShouldBeFound("einheitname.contains=" + DEFAULT_EINHEITNAME);

        // Get all the mastrUnitList where einheitname contains UPDATED_EINHEITNAME
        defaultMastrUnitShouldNotBeFound("einheitname.contains=" + UPDATED_EINHEITNAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEinheitnameNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where einheitname does not contain DEFAULT_EINHEITNAME
        defaultMastrUnitShouldNotBeFound("einheitname.doesNotContain=" + DEFAULT_EINHEITNAME);

        // Get all the mastrUnitList where einheitname does not contain UPDATED_EINHEITNAME
        defaultMastrUnitShouldBeFound("einheitname.doesNotContain=" + UPDATED_EINHEITNAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByFlurstueckIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where flurstueck equals to DEFAULT_FLURSTUECK
        defaultMastrUnitShouldBeFound("flurstueck.equals=" + DEFAULT_FLURSTUECK);

        // Get all the mastrUnitList where flurstueck equals to UPDATED_FLURSTUECK
        defaultMastrUnitShouldNotBeFound("flurstueck.equals=" + UPDATED_FLURSTUECK);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByFlurstueckIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where flurstueck in DEFAULT_FLURSTUECK or UPDATED_FLURSTUECK
        defaultMastrUnitShouldBeFound("flurstueck.in=" + DEFAULT_FLURSTUECK + "," + UPDATED_FLURSTUECK);

        // Get all the mastrUnitList where flurstueck equals to UPDATED_FLURSTUECK
        defaultMastrUnitShouldNotBeFound("flurstueck.in=" + UPDATED_FLURSTUECK);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByFlurstueckIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where flurstueck is not null
        defaultMastrUnitShouldBeFound("flurstueck.specified=true");

        // Get all the mastrUnitList where flurstueck is null
        defaultMastrUnitShouldNotBeFound("flurstueck.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByFlurstueckContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where flurstueck contains DEFAULT_FLURSTUECK
        defaultMastrUnitShouldBeFound("flurstueck.contains=" + DEFAULT_FLURSTUECK);

        // Get all the mastrUnitList where flurstueck contains UPDATED_FLURSTUECK
        defaultMastrUnitShouldNotBeFound("flurstueck.contains=" + UPDATED_FLURSTUECK);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByFlurstueckNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where flurstueck does not contain DEFAULT_FLURSTUECK
        defaultMastrUnitShouldNotBeFound("flurstueck.doesNotContain=" + DEFAULT_FLURSTUECK);

        // Get all the mastrUnitList where flurstueck does not contain UPDATED_FLURSTUECK
        defaultMastrUnitShouldBeFound("flurstueck.doesNotContain=" + UPDATED_FLURSTUECK);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemarkungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemarkung equals to DEFAULT_GEMARKUNG
        defaultMastrUnitShouldBeFound("gemarkung.equals=" + DEFAULT_GEMARKUNG);

        // Get all the mastrUnitList where gemarkung equals to UPDATED_GEMARKUNG
        defaultMastrUnitShouldNotBeFound("gemarkung.equals=" + UPDATED_GEMARKUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemarkungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemarkung in DEFAULT_GEMARKUNG or UPDATED_GEMARKUNG
        defaultMastrUnitShouldBeFound("gemarkung.in=" + DEFAULT_GEMARKUNG + "," + UPDATED_GEMARKUNG);

        // Get all the mastrUnitList where gemarkung equals to UPDATED_GEMARKUNG
        defaultMastrUnitShouldNotBeFound("gemarkung.in=" + UPDATED_GEMARKUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemarkungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemarkung is not null
        defaultMastrUnitShouldBeFound("gemarkung.specified=true");

        // Get all the mastrUnitList where gemarkung is null
        defaultMastrUnitShouldNotBeFound("gemarkung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemarkungContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemarkung contains DEFAULT_GEMARKUNG
        defaultMastrUnitShouldBeFound("gemarkung.contains=" + DEFAULT_GEMARKUNG);

        // Get all the mastrUnitList where gemarkung contains UPDATED_GEMARKUNG
        defaultMastrUnitShouldNotBeFound("gemarkung.contains=" + UPDATED_GEMARKUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemarkungNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemarkung does not contain DEFAULT_GEMARKUNG
        defaultMastrUnitShouldNotBeFound("gemarkung.doesNotContain=" + DEFAULT_GEMARKUNG);

        // Get all the mastrUnitList where gemarkung does not contain UPDATED_GEMARKUNG
        defaultMastrUnitShouldBeFound("gemarkung.doesNotContain=" + UPDATED_GEMARKUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeindeIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeinde equals to DEFAULT_GEMEINDE
        defaultMastrUnitShouldBeFound("gemeinde.equals=" + DEFAULT_GEMEINDE);

        // Get all the mastrUnitList where gemeinde equals to UPDATED_GEMEINDE
        defaultMastrUnitShouldNotBeFound("gemeinde.equals=" + UPDATED_GEMEINDE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeindeIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeinde in DEFAULT_GEMEINDE or UPDATED_GEMEINDE
        defaultMastrUnitShouldBeFound("gemeinde.in=" + DEFAULT_GEMEINDE + "," + UPDATED_GEMEINDE);

        // Get all the mastrUnitList where gemeinde equals to UPDATED_GEMEINDE
        defaultMastrUnitShouldNotBeFound("gemeinde.in=" + UPDATED_GEMEINDE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeindeIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeinde is not null
        defaultMastrUnitShouldBeFound("gemeinde.specified=true");

        // Get all the mastrUnitList where gemeinde is null
        defaultMastrUnitShouldNotBeFound("gemeinde.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeindeContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeinde contains DEFAULT_GEMEINDE
        defaultMastrUnitShouldBeFound("gemeinde.contains=" + DEFAULT_GEMEINDE);

        // Get all the mastrUnitList where gemeinde contains UPDATED_GEMEINDE
        defaultMastrUnitShouldNotBeFound("gemeinde.contains=" + UPDATED_GEMEINDE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeindeNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeinde does not contain DEFAULT_GEMEINDE
        defaultMastrUnitShouldNotBeFound("gemeinde.doesNotContain=" + DEFAULT_GEMEINDE);

        // Get all the mastrUnitList where gemeinde does not contain UPDATED_GEMEINDE
        defaultMastrUnitShouldBeFound("gemeinde.doesNotContain=" + UPDATED_GEMEINDE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landId equals to DEFAULT_LAND_ID
        defaultMastrUnitShouldBeFound("landId.equals=" + DEFAULT_LAND_ID);

        // Get all the mastrUnitList where landId equals to UPDATED_LAND_ID
        defaultMastrUnitShouldNotBeFound("landId.equals=" + UPDATED_LAND_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landId in DEFAULT_LAND_ID or UPDATED_LAND_ID
        defaultMastrUnitShouldBeFound("landId.in=" + DEFAULT_LAND_ID + "," + UPDATED_LAND_ID);

        // Get all the mastrUnitList where landId equals to UPDATED_LAND_ID
        defaultMastrUnitShouldNotBeFound("landId.in=" + UPDATED_LAND_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landId is not null
        defaultMastrUnitShouldBeFound("landId.specified=true");

        // Get all the mastrUnitList where landId is null
        defaultMastrUnitShouldNotBeFound("landId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landId is greater than or equal to DEFAULT_LAND_ID
        defaultMastrUnitShouldBeFound("landId.greaterThanOrEqual=" + DEFAULT_LAND_ID);

        // Get all the mastrUnitList where landId is greater than or equal to UPDATED_LAND_ID
        defaultMastrUnitShouldNotBeFound("landId.greaterThanOrEqual=" + UPDATED_LAND_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landId is less than or equal to DEFAULT_LAND_ID
        defaultMastrUnitShouldBeFound("landId.lessThanOrEqual=" + DEFAULT_LAND_ID);

        // Get all the mastrUnitList where landId is less than or equal to SMALLER_LAND_ID
        defaultMastrUnitShouldNotBeFound("landId.lessThanOrEqual=" + SMALLER_LAND_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandIdIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landId is less than DEFAULT_LAND_ID
        defaultMastrUnitShouldNotBeFound("landId.lessThan=" + DEFAULT_LAND_ID);

        // Get all the mastrUnitList where landId is less than UPDATED_LAND_ID
        defaultMastrUnitShouldBeFound("landId.lessThan=" + UPDATED_LAND_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landId is greater than DEFAULT_LAND_ID
        defaultMastrUnitShouldNotBeFound("landId.greaterThan=" + DEFAULT_LAND_ID);

        // Get all the mastrUnitList where landId is greater than SMALLER_LAND_ID
        defaultMastrUnitShouldBeFound("landId.greaterThan=" + SMALLER_LAND_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandkreisIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landkreis equals to DEFAULT_LANDKREIS
        defaultMastrUnitShouldBeFound("landkreis.equals=" + DEFAULT_LANDKREIS);

        // Get all the mastrUnitList where landkreis equals to UPDATED_LANDKREIS
        defaultMastrUnitShouldNotBeFound("landkreis.equals=" + UPDATED_LANDKREIS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandkreisIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landkreis in DEFAULT_LANDKREIS or UPDATED_LANDKREIS
        defaultMastrUnitShouldBeFound("landkreis.in=" + DEFAULT_LANDKREIS + "," + UPDATED_LANDKREIS);

        // Get all the mastrUnitList where landkreis equals to UPDATED_LANDKREIS
        defaultMastrUnitShouldNotBeFound("landkreis.in=" + UPDATED_LANDKREIS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandkreisIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landkreis is not null
        defaultMastrUnitShouldBeFound("landkreis.specified=true");

        // Get all the mastrUnitList where landkreis is null
        defaultMastrUnitShouldNotBeFound("landkreis.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandkreisContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landkreis contains DEFAULT_LANDKREIS
        defaultMastrUnitShouldBeFound("landkreis.contains=" + DEFAULT_LANDKREIS);

        // Get all the mastrUnitList where landkreis contains UPDATED_LANDKREIS
        defaultMastrUnitShouldNotBeFound("landkreis.contains=" + UPDATED_LANDKREIS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLandkreisNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where landkreis does not contain DEFAULT_LANDKREIS
        defaultMastrUnitShouldNotBeFound("landkreis.doesNotContain=" + DEFAULT_LANDKREIS);

        // Get all the mastrUnitList where landkreis does not contain UPDATED_LANDKREIS
        defaultMastrUnitShouldBeFound("landkreis.doesNotContain=" + UPDATED_LANDKREIS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAgsIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where ags equals to DEFAULT_AGS
        defaultMastrUnitShouldBeFound("ags.equals=" + DEFAULT_AGS);

        // Get all the mastrUnitList where ags equals to UPDATED_AGS
        defaultMastrUnitShouldNotBeFound("ags.equals=" + UPDATED_AGS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAgsIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where ags in DEFAULT_AGS or UPDATED_AGS
        defaultMastrUnitShouldBeFound("ags.in=" + DEFAULT_AGS + "," + UPDATED_AGS);

        // Get all the mastrUnitList where ags equals to UPDATED_AGS
        defaultMastrUnitShouldNotBeFound("ags.in=" + UPDATED_AGS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAgsIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where ags is not null
        defaultMastrUnitShouldBeFound("ags.specified=true");

        // Get all the mastrUnitList where ags is null
        defaultMastrUnitShouldNotBeFound("ags.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAgsIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where ags is greater than or equal to DEFAULT_AGS
        defaultMastrUnitShouldBeFound("ags.greaterThanOrEqual=" + DEFAULT_AGS);

        // Get all the mastrUnitList where ags is greater than or equal to UPDATED_AGS
        defaultMastrUnitShouldNotBeFound("ags.greaterThanOrEqual=" + UPDATED_AGS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAgsIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where ags is less than or equal to DEFAULT_AGS
        defaultMastrUnitShouldBeFound("ags.lessThanOrEqual=" + DEFAULT_AGS);

        // Get all the mastrUnitList where ags is less than or equal to SMALLER_AGS
        defaultMastrUnitShouldNotBeFound("ags.lessThanOrEqual=" + SMALLER_AGS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAgsIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where ags is less than DEFAULT_AGS
        defaultMastrUnitShouldNotBeFound("ags.lessThan=" + DEFAULT_AGS);

        // Get all the mastrUnitList where ags is less than UPDATED_AGS
        defaultMastrUnitShouldBeFound("ags.lessThan=" + UPDATED_AGS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAgsIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where ags is greater than DEFAULT_AGS
        defaultMastrUnitShouldNotBeFound("ags.greaterThan=" + DEFAULT_AGS);

        // Get all the mastrUnitList where ags is greater than SMALLER_AGS
        defaultMastrUnitShouldBeFound("ags.greaterThan=" + SMALLER_AGS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLokationIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lokationId equals to DEFAULT_LOKATION_ID
        defaultMastrUnitShouldBeFound("lokationId.equals=" + DEFAULT_LOKATION_ID);

        // Get all the mastrUnitList where lokationId equals to UPDATED_LOKATION_ID
        defaultMastrUnitShouldNotBeFound("lokationId.equals=" + UPDATED_LOKATION_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLokationIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lokationId in DEFAULT_LOKATION_ID or UPDATED_LOKATION_ID
        defaultMastrUnitShouldBeFound("lokationId.in=" + DEFAULT_LOKATION_ID + "," + UPDATED_LOKATION_ID);

        // Get all the mastrUnitList where lokationId equals to UPDATED_LOKATION_ID
        defaultMastrUnitShouldNotBeFound("lokationId.in=" + UPDATED_LOKATION_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLokationIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lokationId is not null
        defaultMastrUnitShouldBeFound("lokationId.specified=true");

        // Get all the mastrUnitList where lokationId is null
        defaultMastrUnitShouldNotBeFound("lokationId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLokationIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lokationId is greater than or equal to DEFAULT_LOKATION_ID
        defaultMastrUnitShouldBeFound("lokationId.greaterThanOrEqual=" + DEFAULT_LOKATION_ID);

        // Get all the mastrUnitList where lokationId is greater than or equal to UPDATED_LOKATION_ID
        defaultMastrUnitShouldNotBeFound("lokationId.greaterThanOrEqual=" + UPDATED_LOKATION_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLokationIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lokationId is less than or equal to DEFAULT_LOKATION_ID
        defaultMastrUnitShouldBeFound("lokationId.lessThanOrEqual=" + DEFAULT_LOKATION_ID);

        // Get all the mastrUnitList where lokationId is less than or equal to SMALLER_LOKATION_ID
        defaultMastrUnitShouldNotBeFound("lokationId.lessThanOrEqual=" + SMALLER_LOKATION_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLokationIdIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lokationId is less than DEFAULT_LOKATION_ID
        defaultMastrUnitShouldNotBeFound("lokationId.lessThan=" + DEFAULT_LOKATION_ID);

        // Get all the mastrUnitList where lokationId is less than UPDATED_LOKATION_ID
        defaultMastrUnitShouldBeFound("lokationId.lessThan=" + UPDATED_LOKATION_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLokationIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lokationId is greater than DEFAULT_LOKATION_ID
        defaultMastrUnitShouldNotBeFound("lokationId.greaterThan=" + DEFAULT_LOKATION_ID);

        // Get all the mastrUnitList where lokationId is greater than SMALLER_LOKATION_ID
        defaultMastrUnitShouldBeFound("lokationId.greaterThan=" + SMALLER_LOKATION_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLokationMastrNrIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lokationMastrNr equals to DEFAULT_LOKATION_MASTR_NR
        defaultMastrUnitShouldBeFound("lokationMastrNr.equals=" + DEFAULT_LOKATION_MASTR_NR);

        // Get all the mastrUnitList where lokationMastrNr equals to UPDATED_LOKATION_MASTR_NR
        defaultMastrUnitShouldNotBeFound("lokationMastrNr.equals=" + UPDATED_LOKATION_MASTR_NR);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLokationMastrNrIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lokationMastrNr in DEFAULT_LOKATION_MASTR_NR or UPDATED_LOKATION_MASTR_NR
        defaultMastrUnitShouldBeFound("lokationMastrNr.in=" + DEFAULT_LOKATION_MASTR_NR + "," + UPDATED_LOKATION_MASTR_NR);

        // Get all the mastrUnitList where lokationMastrNr equals to UPDATED_LOKATION_MASTR_NR
        defaultMastrUnitShouldNotBeFound("lokationMastrNr.in=" + UPDATED_LOKATION_MASTR_NR);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLokationMastrNrIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lokationMastrNr is not null
        defaultMastrUnitShouldBeFound("lokationMastrNr.specified=true");

        // Get all the mastrUnitList where lokationMastrNr is null
        defaultMastrUnitShouldNotBeFound("lokationMastrNr.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLokationMastrNrContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lokationMastrNr contains DEFAULT_LOKATION_MASTR_NR
        defaultMastrUnitShouldBeFound("lokationMastrNr.contains=" + DEFAULT_LOKATION_MASTR_NR);

        // Get all the mastrUnitList where lokationMastrNr contains UPDATED_LOKATION_MASTR_NR
        defaultMastrUnitShouldNotBeFound("lokationMastrNr.contains=" + UPDATED_LOKATION_MASTR_NR);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLokationMastrNrNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lokationMastrNr does not contain DEFAULT_LOKATION_MASTR_NR
        defaultMastrUnitShouldNotBeFound("lokationMastrNr.doesNotContain=" + DEFAULT_LOKATION_MASTR_NR);

        // Get all the mastrUnitList where lokationMastrNr does not contain UPDATED_LOKATION_MASTR_NR
        defaultMastrUnitShouldBeFound("lokationMastrNr.doesNotContain=" + UPDATED_LOKATION_MASTR_NR);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberId equals to DEFAULT_NETZBETREIBER_ID
        defaultMastrUnitShouldBeFound("netzbetreiberId.equals=" + DEFAULT_NETZBETREIBER_ID);

        // Get all the mastrUnitList where netzbetreiberId equals to UPDATED_NETZBETREIBER_ID
        defaultMastrUnitShouldNotBeFound("netzbetreiberId.equals=" + UPDATED_NETZBETREIBER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberId in DEFAULT_NETZBETREIBER_ID or UPDATED_NETZBETREIBER_ID
        defaultMastrUnitShouldBeFound("netzbetreiberId.in=" + DEFAULT_NETZBETREIBER_ID + "," + UPDATED_NETZBETREIBER_ID);

        // Get all the mastrUnitList where netzbetreiberId equals to UPDATED_NETZBETREIBER_ID
        defaultMastrUnitShouldNotBeFound("netzbetreiberId.in=" + UPDATED_NETZBETREIBER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberId is not null
        defaultMastrUnitShouldBeFound("netzbetreiberId.specified=true");

        // Get all the mastrUnitList where netzbetreiberId is null
        defaultMastrUnitShouldNotBeFound("netzbetreiberId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberIdContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberId contains DEFAULT_NETZBETREIBER_ID
        defaultMastrUnitShouldBeFound("netzbetreiberId.contains=" + DEFAULT_NETZBETREIBER_ID);

        // Get all the mastrUnitList where netzbetreiberId contains UPDATED_NETZBETREIBER_ID
        defaultMastrUnitShouldNotBeFound("netzbetreiberId.contains=" + UPDATED_NETZBETREIBER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberIdNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberId does not contain DEFAULT_NETZBETREIBER_ID
        defaultMastrUnitShouldNotBeFound("netzbetreiberId.doesNotContain=" + DEFAULT_NETZBETREIBER_ID);

        // Get all the mastrUnitList where netzbetreiberId does not contain UPDATED_NETZBETREIBER_ID
        defaultMastrUnitShouldBeFound("netzbetreiberId.doesNotContain=" + UPDATED_NETZBETREIBER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberMaskedNamenIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberMaskedNamen equals to DEFAULT_NETZBETREIBER_MASKED_NAMEN
        defaultMastrUnitShouldBeFound("netzbetreiberMaskedNamen.equals=" + DEFAULT_NETZBETREIBER_MASKED_NAMEN);

        // Get all the mastrUnitList where netzbetreiberMaskedNamen equals to UPDATED_NETZBETREIBER_MASKED_NAMEN
        defaultMastrUnitShouldNotBeFound("netzbetreiberMaskedNamen.equals=" + UPDATED_NETZBETREIBER_MASKED_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberMaskedNamenIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberMaskedNamen in DEFAULT_NETZBETREIBER_MASKED_NAMEN or UPDATED_NETZBETREIBER_MASKED_NAMEN
        defaultMastrUnitShouldBeFound(
            "netzbetreiberMaskedNamen.in=" + DEFAULT_NETZBETREIBER_MASKED_NAMEN + "," + UPDATED_NETZBETREIBER_MASKED_NAMEN
        );

        // Get all the mastrUnitList where netzbetreiberMaskedNamen equals to UPDATED_NETZBETREIBER_MASKED_NAMEN
        defaultMastrUnitShouldNotBeFound("netzbetreiberMaskedNamen.in=" + UPDATED_NETZBETREIBER_MASKED_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberMaskedNamenIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberMaskedNamen is not null
        defaultMastrUnitShouldBeFound("netzbetreiberMaskedNamen.specified=true");

        // Get all the mastrUnitList where netzbetreiberMaskedNamen is null
        defaultMastrUnitShouldNotBeFound("netzbetreiberMaskedNamen.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberMaskedNamenContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberMaskedNamen contains DEFAULT_NETZBETREIBER_MASKED_NAMEN
        defaultMastrUnitShouldBeFound("netzbetreiberMaskedNamen.contains=" + DEFAULT_NETZBETREIBER_MASKED_NAMEN);

        // Get all the mastrUnitList where netzbetreiberMaskedNamen contains UPDATED_NETZBETREIBER_MASKED_NAMEN
        defaultMastrUnitShouldNotBeFound("netzbetreiberMaskedNamen.contains=" + UPDATED_NETZBETREIBER_MASKED_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberMaskedNamenNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberMaskedNamen does not contain DEFAULT_NETZBETREIBER_MASKED_NAMEN
        defaultMastrUnitShouldNotBeFound("netzbetreiberMaskedNamen.doesNotContain=" + DEFAULT_NETZBETREIBER_MASKED_NAMEN);

        // Get all the mastrUnitList where netzbetreiberMaskedNamen does not contain UPDATED_NETZBETREIBER_MASKED_NAMEN
        defaultMastrUnitShouldBeFound("netzbetreiberMaskedNamen.doesNotContain=" + UPDATED_NETZBETREIBER_MASKED_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberMastrNummerIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberMastrNummer equals to DEFAULT_NETZBETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("netzbetreiberMastrNummer.equals=" + DEFAULT_NETZBETREIBER_MASTR_NUMMER);

        // Get all the mastrUnitList where netzbetreiberMastrNummer equals to UPDATED_NETZBETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("netzbetreiberMastrNummer.equals=" + UPDATED_NETZBETREIBER_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberMastrNummerIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberMastrNummer in DEFAULT_NETZBETREIBER_MASTR_NUMMER or UPDATED_NETZBETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldBeFound(
            "netzbetreiberMastrNummer.in=" + DEFAULT_NETZBETREIBER_MASTR_NUMMER + "," + UPDATED_NETZBETREIBER_MASTR_NUMMER
        );

        // Get all the mastrUnitList where netzbetreiberMastrNummer equals to UPDATED_NETZBETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("netzbetreiberMastrNummer.in=" + UPDATED_NETZBETREIBER_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberMastrNummerIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberMastrNummer is not null
        defaultMastrUnitShouldBeFound("netzbetreiberMastrNummer.specified=true");

        // Get all the mastrUnitList where netzbetreiberMastrNummer is null
        defaultMastrUnitShouldNotBeFound("netzbetreiberMastrNummer.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberMastrNummerContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberMastrNummer contains DEFAULT_NETZBETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("netzbetreiberMastrNummer.contains=" + DEFAULT_NETZBETREIBER_MASTR_NUMMER);

        // Get all the mastrUnitList where netzbetreiberMastrNummer contains UPDATED_NETZBETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("netzbetreiberMastrNummer.contains=" + UPDATED_NETZBETREIBER_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberMastrNummerNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberMastrNummer does not contain DEFAULT_NETZBETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("netzbetreiberMastrNummer.doesNotContain=" + DEFAULT_NETZBETREIBER_MASTR_NUMMER);

        // Get all the mastrUnitList where netzbetreiberMastrNummer does not contain UPDATED_NETZBETREIBER_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("netzbetreiberMastrNummer.doesNotContain=" + UPDATED_NETZBETREIBER_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberNamenIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberNamen equals to DEFAULT_NETZBETREIBER_NAMEN
        defaultMastrUnitShouldBeFound("netzbetreiberNamen.equals=" + DEFAULT_NETZBETREIBER_NAMEN);

        // Get all the mastrUnitList where netzbetreiberNamen equals to UPDATED_NETZBETREIBER_NAMEN
        defaultMastrUnitShouldNotBeFound("netzbetreiberNamen.equals=" + UPDATED_NETZBETREIBER_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberNamenIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberNamen in DEFAULT_NETZBETREIBER_NAMEN or UPDATED_NETZBETREIBER_NAMEN
        defaultMastrUnitShouldBeFound("netzbetreiberNamen.in=" + DEFAULT_NETZBETREIBER_NAMEN + "," + UPDATED_NETZBETREIBER_NAMEN);

        // Get all the mastrUnitList where netzbetreiberNamen equals to UPDATED_NETZBETREIBER_NAMEN
        defaultMastrUnitShouldNotBeFound("netzbetreiberNamen.in=" + UPDATED_NETZBETREIBER_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberNamenIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberNamen is not null
        defaultMastrUnitShouldBeFound("netzbetreiberNamen.specified=true");

        // Get all the mastrUnitList where netzbetreiberNamen is null
        defaultMastrUnitShouldNotBeFound("netzbetreiberNamen.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberNamenContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberNamen contains DEFAULT_NETZBETREIBER_NAMEN
        defaultMastrUnitShouldBeFound("netzbetreiberNamen.contains=" + DEFAULT_NETZBETREIBER_NAMEN);

        // Get all the mastrUnitList where netzbetreiberNamen contains UPDATED_NETZBETREIBER_NAMEN
        defaultMastrUnitShouldNotBeFound("netzbetreiberNamen.contains=" + UPDATED_NETZBETREIBER_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberNamenNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberNamen does not contain DEFAULT_NETZBETREIBER_NAMEN
        defaultMastrUnitShouldNotBeFound("netzbetreiberNamen.doesNotContain=" + DEFAULT_NETZBETREIBER_NAMEN);

        // Get all the mastrUnitList where netzbetreiberNamen does not contain UPDATED_NETZBETREIBER_NAMEN
        defaultMastrUnitShouldBeFound("netzbetreiberNamen.doesNotContain=" + UPDATED_NETZBETREIBER_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberPersonenArtIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberPersonenArt equals to DEFAULT_NETZBETREIBER_PERSONEN_ART
        defaultMastrUnitShouldBeFound("netzbetreiberPersonenArt.equals=" + DEFAULT_NETZBETREIBER_PERSONEN_ART);

        // Get all the mastrUnitList where netzbetreiberPersonenArt equals to UPDATED_NETZBETREIBER_PERSONEN_ART
        defaultMastrUnitShouldNotBeFound("netzbetreiberPersonenArt.equals=" + UPDATED_NETZBETREIBER_PERSONEN_ART);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberPersonenArtIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberPersonenArt in DEFAULT_NETZBETREIBER_PERSONEN_ART or UPDATED_NETZBETREIBER_PERSONEN_ART
        defaultMastrUnitShouldBeFound(
            "netzbetreiberPersonenArt.in=" + DEFAULT_NETZBETREIBER_PERSONEN_ART + "," + UPDATED_NETZBETREIBER_PERSONEN_ART
        );

        // Get all the mastrUnitList where netzbetreiberPersonenArt equals to UPDATED_NETZBETREIBER_PERSONEN_ART
        defaultMastrUnitShouldNotBeFound("netzbetreiberPersonenArt.in=" + UPDATED_NETZBETREIBER_PERSONEN_ART);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberPersonenArtIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberPersonenArt is not null
        defaultMastrUnitShouldBeFound("netzbetreiberPersonenArt.specified=true");

        // Get all the mastrUnitList where netzbetreiberPersonenArt is null
        defaultMastrUnitShouldNotBeFound("netzbetreiberPersonenArt.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberPersonenArtContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberPersonenArt contains DEFAULT_NETZBETREIBER_PERSONEN_ART
        defaultMastrUnitShouldBeFound("netzbetreiberPersonenArt.contains=" + DEFAULT_NETZBETREIBER_PERSONEN_ART);

        // Get all the mastrUnitList where netzbetreiberPersonenArt contains UPDATED_NETZBETREIBER_PERSONEN_ART
        defaultMastrUnitShouldNotBeFound("netzbetreiberPersonenArt.contains=" + UPDATED_NETZBETREIBER_PERSONEN_ART);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNetzbetreiberPersonenArtNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where netzbetreiberPersonenArt does not contain DEFAULT_NETZBETREIBER_PERSONEN_ART
        defaultMastrUnitShouldNotBeFound("netzbetreiberPersonenArt.doesNotContain=" + DEFAULT_NETZBETREIBER_PERSONEN_ART);

        // Get all the mastrUnitList where netzbetreiberPersonenArt does not contain UPDATED_NETZBETREIBER_PERSONEN_ART
        defaultMastrUnitShouldBeFound("netzbetreiberPersonenArt.doesNotContain=" + UPDATED_NETZBETREIBER_PERSONEN_ART);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySystemStatusIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where systemStatusId equals to DEFAULT_SYSTEM_STATUS_ID
        defaultMastrUnitShouldBeFound("systemStatusId.equals=" + DEFAULT_SYSTEM_STATUS_ID);

        // Get all the mastrUnitList where systemStatusId equals to UPDATED_SYSTEM_STATUS_ID
        defaultMastrUnitShouldNotBeFound("systemStatusId.equals=" + UPDATED_SYSTEM_STATUS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySystemStatusIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where systemStatusId in DEFAULT_SYSTEM_STATUS_ID or UPDATED_SYSTEM_STATUS_ID
        defaultMastrUnitShouldBeFound("systemStatusId.in=" + DEFAULT_SYSTEM_STATUS_ID + "," + UPDATED_SYSTEM_STATUS_ID);

        // Get all the mastrUnitList where systemStatusId equals to UPDATED_SYSTEM_STATUS_ID
        defaultMastrUnitShouldNotBeFound("systemStatusId.in=" + UPDATED_SYSTEM_STATUS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySystemStatusIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where systemStatusId is not null
        defaultMastrUnitShouldBeFound("systemStatusId.specified=true");

        // Get all the mastrUnitList where systemStatusId is null
        defaultMastrUnitShouldNotBeFound("systemStatusId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySystemStatusIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where systemStatusId is greater than or equal to DEFAULT_SYSTEM_STATUS_ID
        defaultMastrUnitShouldBeFound("systemStatusId.greaterThanOrEqual=" + DEFAULT_SYSTEM_STATUS_ID);

        // Get all the mastrUnitList where systemStatusId is greater than or equal to UPDATED_SYSTEM_STATUS_ID
        defaultMastrUnitShouldNotBeFound("systemStatusId.greaterThanOrEqual=" + UPDATED_SYSTEM_STATUS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySystemStatusIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where systemStatusId is less than or equal to DEFAULT_SYSTEM_STATUS_ID
        defaultMastrUnitShouldBeFound("systemStatusId.lessThanOrEqual=" + DEFAULT_SYSTEM_STATUS_ID);

        // Get all the mastrUnitList where systemStatusId is less than or equal to SMALLER_SYSTEM_STATUS_ID
        defaultMastrUnitShouldNotBeFound("systemStatusId.lessThanOrEqual=" + SMALLER_SYSTEM_STATUS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySystemStatusIdIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where systemStatusId is less than DEFAULT_SYSTEM_STATUS_ID
        defaultMastrUnitShouldNotBeFound("systemStatusId.lessThan=" + DEFAULT_SYSTEM_STATUS_ID);

        // Get all the mastrUnitList where systemStatusId is less than UPDATED_SYSTEM_STATUS_ID
        defaultMastrUnitShouldBeFound("systemStatusId.lessThan=" + UPDATED_SYSTEM_STATUS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySystemStatusIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where systemStatusId is greater than DEFAULT_SYSTEM_STATUS_ID
        defaultMastrUnitShouldNotBeFound("systemStatusId.greaterThan=" + DEFAULT_SYSTEM_STATUS_ID);

        // Get all the mastrUnitList where systemStatusId is greater than SMALLER_SYSTEM_STATUS_ID
        defaultMastrUnitShouldBeFound("systemStatusId.greaterThan=" + SMALLER_SYSTEM_STATUS_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySystemStatusNameIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where systemStatusName equals to DEFAULT_SYSTEM_STATUS_NAME
        defaultMastrUnitShouldBeFound("systemStatusName.equals=" + DEFAULT_SYSTEM_STATUS_NAME);

        // Get all the mastrUnitList where systemStatusName equals to UPDATED_SYSTEM_STATUS_NAME
        defaultMastrUnitShouldNotBeFound("systemStatusName.equals=" + UPDATED_SYSTEM_STATUS_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySystemStatusNameIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where systemStatusName in DEFAULT_SYSTEM_STATUS_NAME or UPDATED_SYSTEM_STATUS_NAME
        defaultMastrUnitShouldBeFound("systemStatusName.in=" + DEFAULT_SYSTEM_STATUS_NAME + "," + UPDATED_SYSTEM_STATUS_NAME);

        // Get all the mastrUnitList where systemStatusName equals to UPDATED_SYSTEM_STATUS_NAME
        defaultMastrUnitShouldNotBeFound("systemStatusName.in=" + UPDATED_SYSTEM_STATUS_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySystemStatusNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where systemStatusName is not null
        defaultMastrUnitShouldBeFound("systemStatusName.specified=true");

        // Get all the mastrUnitList where systemStatusName is null
        defaultMastrUnitShouldNotBeFound("systemStatusName.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySystemStatusNameContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where systemStatusName contains DEFAULT_SYSTEM_STATUS_NAME
        defaultMastrUnitShouldBeFound("systemStatusName.contains=" + DEFAULT_SYSTEM_STATUS_NAME);

        // Get all the mastrUnitList where systemStatusName contains UPDATED_SYSTEM_STATUS_NAME
        defaultMastrUnitShouldNotBeFound("systemStatusName.contains=" + UPDATED_SYSTEM_STATUS_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySystemStatusNameNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where systemStatusName does not contain DEFAULT_SYSTEM_STATUS_NAME
        defaultMastrUnitShouldNotBeFound("systemStatusName.doesNotContain=" + DEFAULT_SYSTEM_STATUS_NAME);

        // Get all the mastrUnitList where systemStatusName does not contain UPDATED_SYSTEM_STATUS_NAME
        defaultMastrUnitShouldBeFound("systemStatusName.doesNotContain=" + UPDATED_SYSTEM_STATUS_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTypIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where typ equals to DEFAULT_TYP
        defaultMastrUnitShouldBeFound("typ.equals=" + DEFAULT_TYP);

        // Get all the mastrUnitList where typ equals to UPDATED_TYP
        defaultMastrUnitShouldNotBeFound("typ.equals=" + UPDATED_TYP);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTypIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where typ in DEFAULT_TYP or UPDATED_TYP
        defaultMastrUnitShouldBeFound("typ.in=" + DEFAULT_TYP + "," + UPDATED_TYP);

        // Get all the mastrUnitList where typ equals to UPDATED_TYP
        defaultMastrUnitShouldNotBeFound("typ.in=" + UPDATED_TYP);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTypIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where typ is not null
        defaultMastrUnitShouldBeFound("typ.specified=true");

        // Get all the mastrUnitList where typ is null
        defaultMastrUnitShouldNotBeFound("typ.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTypIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where typ is greater than or equal to DEFAULT_TYP
        defaultMastrUnitShouldBeFound("typ.greaterThanOrEqual=" + DEFAULT_TYP);

        // Get all the mastrUnitList where typ is greater than or equal to UPDATED_TYP
        defaultMastrUnitShouldNotBeFound("typ.greaterThanOrEqual=" + UPDATED_TYP);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTypIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where typ is less than or equal to DEFAULT_TYP
        defaultMastrUnitShouldBeFound("typ.lessThanOrEqual=" + DEFAULT_TYP);

        // Get all the mastrUnitList where typ is less than or equal to SMALLER_TYP
        defaultMastrUnitShouldNotBeFound("typ.lessThanOrEqual=" + SMALLER_TYP);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTypIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where typ is less than DEFAULT_TYP
        defaultMastrUnitShouldNotBeFound("typ.lessThan=" + DEFAULT_TYP);

        // Get all the mastrUnitList where typ is less than UPDATED_TYP
        defaultMastrUnitShouldBeFound("typ.lessThan=" + UPDATED_TYP);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTypIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where typ is greater than DEFAULT_TYP
        defaultMastrUnitShouldNotBeFound("typ.greaterThan=" + DEFAULT_TYP);

        // Get all the mastrUnitList where typ is greater than SMALLER_TYP
        defaultMastrUnitShouldBeFound("typ.greaterThan=" + SMALLER_TYP);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAktenzeichenGenehmigungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where aktenzeichenGenehmigung equals to DEFAULT_AKTENZEICHEN_GENEHMIGUNG
        defaultMastrUnitShouldBeFound("aktenzeichenGenehmigung.equals=" + DEFAULT_AKTENZEICHEN_GENEHMIGUNG);

        // Get all the mastrUnitList where aktenzeichenGenehmigung equals to UPDATED_AKTENZEICHEN_GENEHMIGUNG
        defaultMastrUnitShouldNotBeFound("aktenzeichenGenehmigung.equals=" + UPDATED_AKTENZEICHEN_GENEHMIGUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAktenzeichenGenehmigungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where aktenzeichenGenehmigung in DEFAULT_AKTENZEICHEN_GENEHMIGUNG or UPDATED_AKTENZEICHEN_GENEHMIGUNG
        defaultMastrUnitShouldBeFound(
            "aktenzeichenGenehmigung.in=" + DEFAULT_AKTENZEICHEN_GENEHMIGUNG + "," + UPDATED_AKTENZEICHEN_GENEHMIGUNG
        );

        // Get all the mastrUnitList where aktenzeichenGenehmigung equals to UPDATED_AKTENZEICHEN_GENEHMIGUNG
        defaultMastrUnitShouldNotBeFound("aktenzeichenGenehmigung.in=" + UPDATED_AKTENZEICHEN_GENEHMIGUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAktenzeichenGenehmigungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where aktenzeichenGenehmigung is not null
        defaultMastrUnitShouldBeFound("aktenzeichenGenehmigung.specified=true");

        // Get all the mastrUnitList where aktenzeichenGenehmigung is null
        defaultMastrUnitShouldNotBeFound("aktenzeichenGenehmigung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAktenzeichenGenehmigungContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where aktenzeichenGenehmigung contains DEFAULT_AKTENZEICHEN_GENEHMIGUNG
        defaultMastrUnitShouldBeFound("aktenzeichenGenehmigung.contains=" + DEFAULT_AKTENZEICHEN_GENEHMIGUNG);

        // Get all the mastrUnitList where aktenzeichenGenehmigung contains UPDATED_AKTENZEICHEN_GENEHMIGUNG
        defaultMastrUnitShouldNotBeFound("aktenzeichenGenehmigung.contains=" + UPDATED_AKTENZEICHEN_GENEHMIGUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAktenzeichenGenehmigungNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where aktenzeichenGenehmigung does not contain DEFAULT_AKTENZEICHEN_GENEHMIGUNG
        defaultMastrUnitShouldNotBeFound("aktenzeichenGenehmigung.doesNotContain=" + DEFAULT_AKTENZEICHEN_GENEHMIGUNG);

        // Get all the mastrUnitList where aktenzeichenGenehmigung does not contain UPDATED_AKTENZEICHEN_GENEHMIGUNG
        defaultMastrUnitShouldBeFound("aktenzeichenGenehmigung.doesNotContain=" + UPDATED_AKTENZEICHEN_GENEHMIGUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnzahlSolarmoduleIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anzahlSolarmodule equals to DEFAULT_ANZAHL_SOLARMODULE
        defaultMastrUnitShouldBeFound("anzahlSolarmodule.equals=" + DEFAULT_ANZAHL_SOLARMODULE);

        // Get all the mastrUnitList where anzahlSolarmodule equals to UPDATED_ANZAHL_SOLARMODULE
        defaultMastrUnitShouldNotBeFound("anzahlSolarmodule.equals=" + UPDATED_ANZAHL_SOLARMODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnzahlSolarmoduleIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anzahlSolarmodule in DEFAULT_ANZAHL_SOLARMODULE or UPDATED_ANZAHL_SOLARMODULE
        defaultMastrUnitShouldBeFound("anzahlSolarmodule.in=" + DEFAULT_ANZAHL_SOLARMODULE + "," + UPDATED_ANZAHL_SOLARMODULE);

        // Get all the mastrUnitList where anzahlSolarmodule equals to UPDATED_ANZAHL_SOLARMODULE
        defaultMastrUnitShouldNotBeFound("anzahlSolarmodule.in=" + UPDATED_ANZAHL_SOLARMODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnzahlSolarmoduleIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anzahlSolarmodule is not null
        defaultMastrUnitShouldBeFound("anzahlSolarmodule.specified=true");

        // Get all the mastrUnitList where anzahlSolarmodule is null
        defaultMastrUnitShouldNotBeFound("anzahlSolarmodule.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnzahlSolarmoduleIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anzahlSolarmodule is greater than or equal to DEFAULT_ANZAHL_SOLARMODULE
        defaultMastrUnitShouldBeFound("anzahlSolarmodule.greaterThanOrEqual=" + DEFAULT_ANZAHL_SOLARMODULE);

        // Get all the mastrUnitList where anzahlSolarmodule is greater than or equal to UPDATED_ANZAHL_SOLARMODULE
        defaultMastrUnitShouldNotBeFound("anzahlSolarmodule.greaterThanOrEqual=" + UPDATED_ANZAHL_SOLARMODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnzahlSolarmoduleIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anzahlSolarmodule is less than or equal to DEFAULT_ANZAHL_SOLARMODULE
        defaultMastrUnitShouldBeFound("anzahlSolarmodule.lessThanOrEqual=" + DEFAULT_ANZAHL_SOLARMODULE);

        // Get all the mastrUnitList where anzahlSolarmodule is less than or equal to SMALLER_ANZAHL_SOLARMODULE
        defaultMastrUnitShouldNotBeFound("anzahlSolarmodule.lessThanOrEqual=" + SMALLER_ANZAHL_SOLARMODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnzahlSolarmoduleIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anzahlSolarmodule is less than DEFAULT_ANZAHL_SOLARMODULE
        defaultMastrUnitShouldNotBeFound("anzahlSolarmodule.lessThan=" + DEFAULT_ANZAHL_SOLARMODULE);

        // Get all the mastrUnitList where anzahlSolarmodule is less than UPDATED_ANZAHL_SOLARMODULE
        defaultMastrUnitShouldBeFound("anzahlSolarmodule.lessThan=" + UPDATED_ANZAHL_SOLARMODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAnzahlSolarmoduleIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where anzahlSolarmodule is greater than DEFAULT_ANZAHL_SOLARMODULE
        defaultMastrUnitShouldNotBeFound("anzahlSolarmodule.greaterThan=" + DEFAULT_ANZAHL_SOLARMODULE);

        // Get all the mastrUnitList where anzahlSolarmodule is greater than SMALLER_ANZAHL_SOLARMODULE
        defaultMastrUnitShouldBeFound("anzahlSolarmodule.greaterThan=" + SMALLER_ANZAHL_SOLARMODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBatterieTechnologieIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where batterieTechnologie equals to DEFAULT_BATTERIE_TECHNOLOGIE
        defaultMastrUnitShouldBeFound("batterieTechnologie.equals=" + DEFAULT_BATTERIE_TECHNOLOGIE);

        // Get all the mastrUnitList where batterieTechnologie equals to UPDATED_BATTERIE_TECHNOLOGIE
        defaultMastrUnitShouldNotBeFound("batterieTechnologie.equals=" + UPDATED_BATTERIE_TECHNOLOGIE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBatterieTechnologieIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where batterieTechnologie in DEFAULT_BATTERIE_TECHNOLOGIE or UPDATED_BATTERIE_TECHNOLOGIE
        defaultMastrUnitShouldBeFound("batterieTechnologie.in=" + DEFAULT_BATTERIE_TECHNOLOGIE + "," + UPDATED_BATTERIE_TECHNOLOGIE);

        // Get all the mastrUnitList where batterieTechnologie equals to UPDATED_BATTERIE_TECHNOLOGIE
        defaultMastrUnitShouldNotBeFound("batterieTechnologie.in=" + UPDATED_BATTERIE_TECHNOLOGIE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBatterieTechnologieIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where batterieTechnologie is not null
        defaultMastrUnitShouldBeFound("batterieTechnologie.specified=true");

        // Get all the mastrUnitList where batterieTechnologie is null
        defaultMastrUnitShouldNotBeFound("batterieTechnologie.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBatterieTechnologieIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where batterieTechnologie is greater than or equal to DEFAULT_BATTERIE_TECHNOLOGIE
        defaultMastrUnitShouldBeFound("batterieTechnologie.greaterThanOrEqual=" + DEFAULT_BATTERIE_TECHNOLOGIE);

        // Get all the mastrUnitList where batterieTechnologie is greater than or equal to UPDATED_BATTERIE_TECHNOLOGIE
        defaultMastrUnitShouldNotBeFound("batterieTechnologie.greaterThanOrEqual=" + UPDATED_BATTERIE_TECHNOLOGIE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBatterieTechnologieIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where batterieTechnologie is less than or equal to DEFAULT_BATTERIE_TECHNOLOGIE
        defaultMastrUnitShouldBeFound("batterieTechnologie.lessThanOrEqual=" + DEFAULT_BATTERIE_TECHNOLOGIE);

        // Get all the mastrUnitList where batterieTechnologie is less than or equal to SMALLER_BATTERIE_TECHNOLOGIE
        defaultMastrUnitShouldNotBeFound("batterieTechnologie.lessThanOrEqual=" + SMALLER_BATTERIE_TECHNOLOGIE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBatterieTechnologieIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where batterieTechnologie is less than DEFAULT_BATTERIE_TECHNOLOGIE
        defaultMastrUnitShouldNotBeFound("batterieTechnologie.lessThan=" + DEFAULT_BATTERIE_TECHNOLOGIE);

        // Get all the mastrUnitList where batterieTechnologie is less than UPDATED_BATTERIE_TECHNOLOGIE
        defaultMastrUnitShouldBeFound("batterieTechnologie.lessThan=" + UPDATED_BATTERIE_TECHNOLOGIE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBatterieTechnologieIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where batterieTechnologie is greater than DEFAULT_BATTERIE_TECHNOLOGIE
        defaultMastrUnitShouldNotBeFound("batterieTechnologie.greaterThan=" + DEFAULT_BATTERIE_TECHNOLOGIE);

        // Get all the mastrUnitList where batterieTechnologie is greater than SMALLER_BATTERIE_TECHNOLOGIE
        defaultMastrUnitShouldBeFound("batterieTechnologie.greaterThan=" + SMALLER_BATTERIE_TECHNOLOGIE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBruttoLeistungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bruttoLeistung equals to DEFAULT_BRUTTO_LEISTUNG
        defaultMastrUnitShouldBeFound("bruttoLeistung.equals=" + DEFAULT_BRUTTO_LEISTUNG);

        // Get all the mastrUnitList where bruttoLeistung equals to UPDATED_BRUTTO_LEISTUNG
        defaultMastrUnitShouldNotBeFound("bruttoLeistung.equals=" + UPDATED_BRUTTO_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBruttoLeistungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bruttoLeistung in DEFAULT_BRUTTO_LEISTUNG or UPDATED_BRUTTO_LEISTUNG
        defaultMastrUnitShouldBeFound("bruttoLeistung.in=" + DEFAULT_BRUTTO_LEISTUNG + "," + UPDATED_BRUTTO_LEISTUNG);

        // Get all the mastrUnitList where bruttoLeistung equals to UPDATED_BRUTTO_LEISTUNG
        defaultMastrUnitShouldNotBeFound("bruttoLeistung.in=" + UPDATED_BRUTTO_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBruttoLeistungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bruttoLeistung is not null
        defaultMastrUnitShouldBeFound("bruttoLeistung.specified=true");

        // Get all the mastrUnitList where bruttoLeistung is null
        defaultMastrUnitShouldNotBeFound("bruttoLeistung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBruttoLeistungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bruttoLeistung is greater than or equal to DEFAULT_BRUTTO_LEISTUNG
        defaultMastrUnitShouldBeFound("bruttoLeistung.greaterThanOrEqual=" + DEFAULT_BRUTTO_LEISTUNG);

        // Get all the mastrUnitList where bruttoLeistung is greater than or equal to UPDATED_BRUTTO_LEISTUNG
        defaultMastrUnitShouldNotBeFound("bruttoLeistung.greaterThanOrEqual=" + UPDATED_BRUTTO_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBruttoLeistungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bruttoLeistung is less than or equal to DEFAULT_BRUTTO_LEISTUNG
        defaultMastrUnitShouldBeFound("bruttoLeistung.lessThanOrEqual=" + DEFAULT_BRUTTO_LEISTUNG);

        // Get all the mastrUnitList where bruttoLeistung is less than or equal to SMALLER_BRUTTO_LEISTUNG
        defaultMastrUnitShouldNotBeFound("bruttoLeistung.lessThanOrEqual=" + SMALLER_BRUTTO_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBruttoLeistungIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bruttoLeistung is less than DEFAULT_BRUTTO_LEISTUNG
        defaultMastrUnitShouldNotBeFound("bruttoLeistung.lessThan=" + DEFAULT_BRUTTO_LEISTUNG);

        // Get all the mastrUnitList where bruttoLeistung is less than UPDATED_BRUTTO_LEISTUNG
        defaultMastrUnitShouldBeFound("bruttoLeistung.lessThan=" + UPDATED_BRUTTO_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByBruttoLeistungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where bruttoLeistung is greater than DEFAULT_BRUTTO_LEISTUNG
        defaultMastrUnitShouldNotBeFound("bruttoLeistung.greaterThan=" + DEFAULT_BRUTTO_LEISTUNG);

        // Get all the mastrUnitList where bruttoLeistung is greater than SMALLER_BRUTTO_LEISTUNG
        defaultMastrUnitShouldBeFound("bruttoLeistung.greaterThan=" + SMALLER_BRUTTO_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInstallierteLeistungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInstallierteLeistung equals to DEFAULT_EEG_INSTALLIERTE_LEISTUNG
        defaultMastrUnitShouldBeFound("eegInstallierteLeistung.equals=" + DEFAULT_EEG_INSTALLIERTE_LEISTUNG);

        // Get all the mastrUnitList where eegInstallierteLeistung equals to UPDATED_EEG_INSTALLIERTE_LEISTUNG
        defaultMastrUnitShouldNotBeFound("eegInstallierteLeistung.equals=" + UPDATED_EEG_INSTALLIERTE_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInstallierteLeistungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInstallierteLeistung in DEFAULT_EEG_INSTALLIERTE_LEISTUNG or UPDATED_EEG_INSTALLIERTE_LEISTUNG
        defaultMastrUnitShouldBeFound(
            "eegInstallierteLeistung.in=" + DEFAULT_EEG_INSTALLIERTE_LEISTUNG + "," + UPDATED_EEG_INSTALLIERTE_LEISTUNG
        );

        // Get all the mastrUnitList where eegInstallierteLeistung equals to UPDATED_EEG_INSTALLIERTE_LEISTUNG
        defaultMastrUnitShouldNotBeFound("eegInstallierteLeistung.in=" + UPDATED_EEG_INSTALLIERTE_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInstallierteLeistungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInstallierteLeistung is not null
        defaultMastrUnitShouldBeFound("eegInstallierteLeistung.specified=true");

        // Get all the mastrUnitList where eegInstallierteLeistung is null
        defaultMastrUnitShouldNotBeFound("eegInstallierteLeistung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInstallierteLeistungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInstallierteLeistung is greater than or equal to DEFAULT_EEG_INSTALLIERTE_LEISTUNG
        defaultMastrUnitShouldBeFound("eegInstallierteLeistung.greaterThanOrEqual=" + DEFAULT_EEG_INSTALLIERTE_LEISTUNG);

        // Get all the mastrUnitList where eegInstallierteLeistung is greater than or equal to UPDATED_EEG_INSTALLIERTE_LEISTUNG
        defaultMastrUnitShouldNotBeFound("eegInstallierteLeistung.greaterThanOrEqual=" + UPDATED_EEG_INSTALLIERTE_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInstallierteLeistungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInstallierteLeistung is less than or equal to DEFAULT_EEG_INSTALLIERTE_LEISTUNG
        defaultMastrUnitShouldBeFound("eegInstallierteLeistung.lessThanOrEqual=" + DEFAULT_EEG_INSTALLIERTE_LEISTUNG);

        // Get all the mastrUnitList where eegInstallierteLeistung is less than or equal to SMALLER_EEG_INSTALLIERTE_LEISTUNG
        defaultMastrUnitShouldNotBeFound("eegInstallierteLeistung.lessThanOrEqual=" + SMALLER_EEG_INSTALLIERTE_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInstallierteLeistungIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInstallierteLeistung is less than DEFAULT_EEG_INSTALLIERTE_LEISTUNG
        defaultMastrUnitShouldNotBeFound("eegInstallierteLeistung.lessThan=" + DEFAULT_EEG_INSTALLIERTE_LEISTUNG);

        // Get all the mastrUnitList where eegInstallierteLeistung is less than UPDATED_EEG_INSTALLIERTE_LEISTUNG
        defaultMastrUnitShouldBeFound("eegInstallierteLeistung.lessThan=" + UPDATED_EEG_INSTALLIERTE_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegInstallierteLeistungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegInstallierteLeistung is greater than DEFAULT_EEG_INSTALLIERTE_LEISTUNG
        defaultMastrUnitShouldNotBeFound("eegInstallierteLeistung.greaterThan=" + DEFAULT_EEG_INSTALLIERTE_LEISTUNG);

        // Get all the mastrUnitList where eegInstallierteLeistung is greater than SMALLER_EEG_INSTALLIERTE_LEISTUNG
        defaultMastrUnitShouldBeFound("eegInstallierteLeistung.greaterThan=" + SMALLER_EEG_INSTALLIERTE_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlageMastrNummerIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlageMastrNummer equals to DEFAULT_EEG_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("eegAnlageMastrNummer.equals=" + DEFAULT_EEG_ANLAGE_MASTR_NUMMER);

        // Get all the mastrUnitList where eegAnlageMastrNummer equals to UPDATED_EEG_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("eegAnlageMastrNummer.equals=" + UPDATED_EEG_ANLAGE_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlageMastrNummerIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlageMastrNummer in DEFAULT_EEG_ANLAGE_MASTR_NUMMER or UPDATED_EEG_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("eegAnlageMastrNummer.in=" + DEFAULT_EEG_ANLAGE_MASTR_NUMMER + "," + UPDATED_EEG_ANLAGE_MASTR_NUMMER);

        // Get all the mastrUnitList where eegAnlageMastrNummer equals to UPDATED_EEG_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("eegAnlageMastrNummer.in=" + UPDATED_EEG_ANLAGE_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlageMastrNummerIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlageMastrNummer is not null
        defaultMastrUnitShouldBeFound("eegAnlageMastrNummer.specified=true");

        // Get all the mastrUnitList where eegAnlageMastrNummer is null
        defaultMastrUnitShouldNotBeFound("eegAnlageMastrNummer.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlageMastrNummerContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlageMastrNummer contains DEFAULT_EEG_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("eegAnlageMastrNummer.contains=" + DEFAULT_EEG_ANLAGE_MASTR_NUMMER);

        // Get all the mastrUnitList where eegAnlageMastrNummer contains UPDATED_EEG_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("eegAnlageMastrNummer.contains=" + UPDATED_EEG_ANLAGE_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlageMastrNummerNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlageMastrNummer does not contain DEFAULT_EEG_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("eegAnlageMastrNummer.doesNotContain=" + DEFAULT_EEG_ANLAGE_MASTR_NUMMER);

        // Get all the mastrUnitList where eegAnlageMastrNummer does not contain UPDATED_EEG_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("eegAnlageMastrNummer.doesNotContain=" + UPDATED_EEG_ANLAGE_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlagenSchluesselIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlagenSchluessel equals to DEFAULT_EEG_ANLAGEN_SCHLUESSEL
        defaultMastrUnitShouldBeFound("eegAnlagenSchluessel.equals=" + DEFAULT_EEG_ANLAGEN_SCHLUESSEL);

        // Get all the mastrUnitList where eegAnlagenSchluessel equals to UPDATED_EEG_ANLAGEN_SCHLUESSEL
        defaultMastrUnitShouldNotBeFound("eegAnlagenSchluessel.equals=" + UPDATED_EEG_ANLAGEN_SCHLUESSEL);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlagenSchluesselIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlagenSchluessel in DEFAULT_EEG_ANLAGEN_SCHLUESSEL or UPDATED_EEG_ANLAGEN_SCHLUESSEL
        defaultMastrUnitShouldBeFound("eegAnlagenSchluessel.in=" + DEFAULT_EEG_ANLAGEN_SCHLUESSEL + "," + UPDATED_EEG_ANLAGEN_SCHLUESSEL);

        // Get all the mastrUnitList where eegAnlagenSchluessel equals to UPDATED_EEG_ANLAGEN_SCHLUESSEL
        defaultMastrUnitShouldNotBeFound("eegAnlagenSchluessel.in=" + UPDATED_EEG_ANLAGEN_SCHLUESSEL);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlagenSchluesselIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlagenSchluessel is not null
        defaultMastrUnitShouldBeFound("eegAnlagenSchluessel.specified=true");

        // Get all the mastrUnitList where eegAnlagenSchluessel is null
        defaultMastrUnitShouldNotBeFound("eegAnlagenSchluessel.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlagenSchluesselContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlagenSchluessel contains DEFAULT_EEG_ANLAGEN_SCHLUESSEL
        defaultMastrUnitShouldBeFound("eegAnlagenSchluessel.contains=" + DEFAULT_EEG_ANLAGEN_SCHLUESSEL);

        // Get all the mastrUnitList where eegAnlagenSchluessel contains UPDATED_EEG_ANLAGEN_SCHLUESSEL
        defaultMastrUnitShouldNotBeFound("eegAnlagenSchluessel.contains=" + UPDATED_EEG_ANLAGEN_SCHLUESSEL);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegAnlagenSchluesselNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegAnlagenSchluessel does not contain DEFAULT_EEG_ANLAGEN_SCHLUESSEL
        defaultMastrUnitShouldNotBeFound("eegAnlagenSchluessel.doesNotContain=" + DEFAULT_EEG_ANLAGEN_SCHLUESSEL);

        // Get all the mastrUnitList where eegAnlagenSchluessel does not contain UPDATED_EEG_ANLAGEN_SCHLUESSEL
        defaultMastrUnitShouldBeFound("eegAnlagenSchluessel.doesNotContain=" + UPDATED_EEG_ANLAGEN_SCHLUESSEL);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegZuschlagIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegZuschlag equals to DEFAULT_EEG_ZUSCHLAG
        defaultMastrUnitShouldBeFound("eegZuschlag.equals=" + DEFAULT_EEG_ZUSCHLAG);

        // Get all the mastrUnitList where eegZuschlag equals to UPDATED_EEG_ZUSCHLAG
        defaultMastrUnitShouldNotBeFound("eegZuschlag.equals=" + UPDATED_EEG_ZUSCHLAG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegZuschlagIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegZuschlag in DEFAULT_EEG_ZUSCHLAG or UPDATED_EEG_ZUSCHLAG
        defaultMastrUnitShouldBeFound("eegZuschlag.in=" + DEFAULT_EEG_ZUSCHLAG + "," + UPDATED_EEG_ZUSCHLAG);

        // Get all the mastrUnitList where eegZuschlag equals to UPDATED_EEG_ZUSCHLAG
        defaultMastrUnitShouldNotBeFound("eegZuschlag.in=" + UPDATED_EEG_ZUSCHLAG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegZuschlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegZuschlag is not null
        defaultMastrUnitShouldBeFound("eegZuschlag.specified=true");

        // Get all the mastrUnitList where eegZuschlag is null
        defaultMastrUnitShouldNotBeFound("eegZuschlag.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegZuschlagContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegZuschlag contains DEFAULT_EEG_ZUSCHLAG
        defaultMastrUnitShouldBeFound("eegZuschlag.contains=" + DEFAULT_EEG_ZUSCHLAG);

        // Get all the mastrUnitList where eegZuschlag contains UPDATED_EEG_ZUSCHLAG
        defaultMastrUnitShouldNotBeFound("eegZuschlag.contains=" + UPDATED_EEG_ZUSCHLAG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEegZuschlagNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where eegZuschlag does not contain DEFAULT_EEG_ZUSCHLAG
        defaultMastrUnitShouldNotBeFound("eegZuschlag.doesNotContain=" + DEFAULT_EEG_ZUSCHLAG);

        // Get all the mastrUnitList where eegZuschlag does not contain UPDATED_EEG_ZUSCHLAG
        defaultMastrUnitShouldBeFound("eegZuschlag.doesNotContain=" + UPDATED_EEG_ZUSCHLAG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByZuschlagsNummernIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where zuschlagsNummern equals to DEFAULT_ZUSCHLAGS_NUMMERN
        defaultMastrUnitShouldBeFound("zuschlagsNummern.equals=" + DEFAULT_ZUSCHLAGS_NUMMERN);

        // Get all the mastrUnitList where zuschlagsNummern equals to UPDATED_ZUSCHLAGS_NUMMERN
        defaultMastrUnitShouldNotBeFound("zuschlagsNummern.equals=" + UPDATED_ZUSCHLAGS_NUMMERN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByZuschlagsNummernIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where zuschlagsNummern in DEFAULT_ZUSCHLAGS_NUMMERN or UPDATED_ZUSCHLAGS_NUMMERN
        defaultMastrUnitShouldBeFound("zuschlagsNummern.in=" + DEFAULT_ZUSCHLAGS_NUMMERN + "," + UPDATED_ZUSCHLAGS_NUMMERN);

        // Get all the mastrUnitList where zuschlagsNummern equals to UPDATED_ZUSCHLAGS_NUMMERN
        defaultMastrUnitShouldNotBeFound("zuschlagsNummern.in=" + UPDATED_ZUSCHLAGS_NUMMERN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByZuschlagsNummernIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where zuschlagsNummern is not null
        defaultMastrUnitShouldBeFound("zuschlagsNummern.specified=true");

        // Get all the mastrUnitList where zuschlagsNummern is null
        defaultMastrUnitShouldNotBeFound("zuschlagsNummern.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByZuschlagsNummernContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where zuschlagsNummern contains DEFAULT_ZUSCHLAGS_NUMMERN
        defaultMastrUnitShouldBeFound("zuschlagsNummern.contains=" + DEFAULT_ZUSCHLAGS_NUMMERN);

        // Get all the mastrUnitList where zuschlagsNummern contains UPDATED_ZUSCHLAGS_NUMMERN
        defaultMastrUnitShouldNotBeFound("zuschlagsNummern.contains=" + UPDATED_ZUSCHLAGS_NUMMERN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByZuschlagsNummernNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where zuschlagsNummern does not contain DEFAULT_ZUSCHLAGS_NUMMERN
        defaultMastrUnitShouldNotBeFound("zuschlagsNummern.doesNotContain=" + DEFAULT_ZUSCHLAGS_NUMMERN);

        // Get all the mastrUnitList where zuschlagsNummern does not contain UPDATED_ZUSCHLAGS_NUMMERN
        defaultMastrUnitShouldBeFound("zuschlagsNummern.doesNotContain=" + UPDATED_ZUSCHLAGS_NUMMERN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEnergieTraegerIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where energieTraegerId equals to DEFAULT_ENERGIE_TRAEGER_ID
        defaultMastrUnitShouldBeFound("energieTraegerId.equals=" + DEFAULT_ENERGIE_TRAEGER_ID);

        // Get all the mastrUnitList where energieTraegerId equals to UPDATED_ENERGIE_TRAEGER_ID
        defaultMastrUnitShouldNotBeFound("energieTraegerId.equals=" + UPDATED_ENERGIE_TRAEGER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEnergieTraegerIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where energieTraegerId in DEFAULT_ENERGIE_TRAEGER_ID or UPDATED_ENERGIE_TRAEGER_ID
        defaultMastrUnitShouldBeFound("energieTraegerId.in=" + DEFAULT_ENERGIE_TRAEGER_ID + "," + UPDATED_ENERGIE_TRAEGER_ID);

        // Get all the mastrUnitList where energieTraegerId equals to UPDATED_ENERGIE_TRAEGER_ID
        defaultMastrUnitShouldNotBeFound("energieTraegerId.in=" + UPDATED_ENERGIE_TRAEGER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEnergieTraegerIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where energieTraegerId is not null
        defaultMastrUnitShouldBeFound("energieTraegerId.specified=true");

        // Get all the mastrUnitList where energieTraegerId is null
        defaultMastrUnitShouldNotBeFound("energieTraegerId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEnergieTraegerIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where energieTraegerId is greater than or equal to DEFAULT_ENERGIE_TRAEGER_ID
        defaultMastrUnitShouldBeFound("energieTraegerId.greaterThanOrEqual=" + DEFAULT_ENERGIE_TRAEGER_ID);

        // Get all the mastrUnitList where energieTraegerId is greater than or equal to UPDATED_ENERGIE_TRAEGER_ID
        defaultMastrUnitShouldNotBeFound("energieTraegerId.greaterThanOrEqual=" + UPDATED_ENERGIE_TRAEGER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEnergieTraegerIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where energieTraegerId is less than or equal to DEFAULT_ENERGIE_TRAEGER_ID
        defaultMastrUnitShouldBeFound("energieTraegerId.lessThanOrEqual=" + DEFAULT_ENERGIE_TRAEGER_ID);

        // Get all the mastrUnitList where energieTraegerId is less than or equal to SMALLER_ENERGIE_TRAEGER_ID
        defaultMastrUnitShouldNotBeFound("energieTraegerId.lessThanOrEqual=" + SMALLER_ENERGIE_TRAEGER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEnergieTraegerIdIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where energieTraegerId is less than DEFAULT_ENERGIE_TRAEGER_ID
        defaultMastrUnitShouldNotBeFound("energieTraegerId.lessThan=" + DEFAULT_ENERGIE_TRAEGER_ID);

        // Get all the mastrUnitList where energieTraegerId is less than UPDATED_ENERGIE_TRAEGER_ID
        defaultMastrUnitShouldBeFound("energieTraegerId.lessThan=" + UPDATED_ENERGIE_TRAEGER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEnergieTraegerIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where energieTraegerId is greater than DEFAULT_ENERGIE_TRAEGER_ID
        defaultMastrUnitShouldNotBeFound("energieTraegerId.greaterThan=" + DEFAULT_ENERGIE_TRAEGER_ID);

        // Get all the mastrUnitList where energieTraegerId is greater than SMALLER_ENERGIE_TRAEGER_ID
        defaultMastrUnitShouldBeFound("energieTraegerId.greaterThan=" + SMALLER_ENERGIE_TRAEGER_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEnergieTraegerNameIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where energieTraegerName equals to DEFAULT_ENERGIE_TRAEGER_NAME
        defaultMastrUnitShouldBeFound("energieTraegerName.equals=" + DEFAULT_ENERGIE_TRAEGER_NAME);

        // Get all the mastrUnitList where energieTraegerName equals to UPDATED_ENERGIE_TRAEGER_NAME
        defaultMastrUnitShouldNotBeFound("energieTraegerName.equals=" + UPDATED_ENERGIE_TRAEGER_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEnergieTraegerNameIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where energieTraegerName in DEFAULT_ENERGIE_TRAEGER_NAME or UPDATED_ENERGIE_TRAEGER_NAME
        defaultMastrUnitShouldBeFound("energieTraegerName.in=" + DEFAULT_ENERGIE_TRAEGER_NAME + "," + UPDATED_ENERGIE_TRAEGER_NAME);

        // Get all the mastrUnitList where energieTraegerName equals to UPDATED_ENERGIE_TRAEGER_NAME
        defaultMastrUnitShouldNotBeFound("energieTraegerName.in=" + UPDATED_ENERGIE_TRAEGER_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEnergieTraegerNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where energieTraegerName is not null
        defaultMastrUnitShouldBeFound("energieTraegerName.specified=true");

        // Get all the mastrUnitList where energieTraegerName is null
        defaultMastrUnitShouldNotBeFound("energieTraegerName.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEnergieTraegerNameContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where energieTraegerName contains DEFAULT_ENERGIE_TRAEGER_NAME
        defaultMastrUnitShouldBeFound("energieTraegerName.contains=" + DEFAULT_ENERGIE_TRAEGER_NAME);

        // Get all the mastrUnitList where energieTraegerName contains UPDATED_ENERGIE_TRAEGER_NAME
        defaultMastrUnitShouldNotBeFound("energieTraegerName.contains=" + UPDATED_ENERGIE_TRAEGER_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByEnergieTraegerNameNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where energieTraegerName does not contain DEFAULT_ENERGIE_TRAEGER_NAME
        defaultMastrUnitShouldNotBeFound("energieTraegerName.doesNotContain=" + DEFAULT_ENERGIE_TRAEGER_NAME);

        // Get all the mastrUnitList where energieTraegerName does not contain UPDATED_ENERGIE_TRAEGER_NAME
        defaultMastrUnitShouldBeFound("energieTraegerName.doesNotContain=" + UPDATED_ENERGIE_TRAEGER_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeinsamerWechselrichterIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeinsamerWechselrichter equals to DEFAULT_GEMEINSAMER_WECHSELRICHTER
        defaultMastrUnitShouldBeFound("gemeinsamerWechselrichter.equals=" + DEFAULT_GEMEINSAMER_WECHSELRICHTER);

        // Get all the mastrUnitList where gemeinsamerWechselrichter equals to UPDATED_GEMEINSAMER_WECHSELRICHTER
        defaultMastrUnitShouldNotBeFound("gemeinsamerWechselrichter.equals=" + UPDATED_GEMEINSAMER_WECHSELRICHTER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeinsamerWechselrichterIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeinsamerWechselrichter in DEFAULT_GEMEINSAMER_WECHSELRICHTER or UPDATED_GEMEINSAMER_WECHSELRICHTER
        defaultMastrUnitShouldBeFound(
            "gemeinsamerWechselrichter.in=" + DEFAULT_GEMEINSAMER_WECHSELRICHTER + "," + UPDATED_GEMEINSAMER_WECHSELRICHTER
        );

        // Get all the mastrUnitList where gemeinsamerWechselrichter equals to UPDATED_GEMEINSAMER_WECHSELRICHTER
        defaultMastrUnitShouldNotBeFound("gemeinsamerWechselrichter.in=" + UPDATED_GEMEINSAMER_WECHSELRICHTER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeinsamerWechselrichterIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeinsamerWechselrichter is not null
        defaultMastrUnitShouldBeFound("gemeinsamerWechselrichter.specified=true");

        // Get all the mastrUnitList where gemeinsamerWechselrichter is null
        defaultMastrUnitShouldNotBeFound("gemeinsamerWechselrichter.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeinsamerWechselrichterIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeinsamerWechselrichter is greater than or equal to DEFAULT_GEMEINSAMER_WECHSELRICHTER
        defaultMastrUnitShouldBeFound("gemeinsamerWechselrichter.greaterThanOrEqual=" + DEFAULT_GEMEINSAMER_WECHSELRICHTER);

        // Get all the mastrUnitList where gemeinsamerWechselrichter is greater than or equal to UPDATED_GEMEINSAMER_WECHSELRICHTER
        defaultMastrUnitShouldNotBeFound("gemeinsamerWechselrichter.greaterThanOrEqual=" + UPDATED_GEMEINSAMER_WECHSELRICHTER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeinsamerWechselrichterIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeinsamerWechselrichter is less than or equal to DEFAULT_GEMEINSAMER_WECHSELRICHTER
        defaultMastrUnitShouldBeFound("gemeinsamerWechselrichter.lessThanOrEqual=" + DEFAULT_GEMEINSAMER_WECHSELRICHTER);

        // Get all the mastrUnitList where gemeinsamerWechselrichter is less than or equal to SMALLER_GEMEINSAMER_WECHSELRICHTER
        defaultMastrUnitShouldNotBeFound("gemeinsamerWechselrichter.lessThanOrEqual=" + SMALLER_GEMEINSAMER_WECHSELRICHTER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeinsamerWechselrichterIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeinsamerWechselrichter is less than DEFAULT_GEMEINSAMER_WECHSELRICHTER
        defaultMastrUnitShouldNotBeFound("gemeinsamerWechselrichter.lessThan=" + DEFAULT_GEMEINSAMER_WECHSELRICHTER);

        // Get all the mastrUnitList where gemeinsamerWechselrichter is less than UPDATED_GEMEINSAMER_WECHSELRICHTER
        defaultMastrUnitShouldBeFound("gemeinsamerWechselrichter.lessThan=" + UPDATED_GEMEINSAMER_WECHSELRICHTER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGemeinsamerWechselrichterIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gemeinsamerWechselrichter is greater than DEFAULT_GEMEINSAMER_WECHSELRICHTER
        defaultMastrUnitShouldNotBeFound("gemeinsamerWechselrichter.greaterThan=" + DEFAULT_GEMEINSAMER_WECHSELRICHTER);

        // Get all the mastrUnitList where gemeinsamerWechselrichter is greater than SMALLER_GEMEINSAMER_WECHSELRICHTER
        defaultMastrUnitShouldBeFound("gemeinsamerWechselrichter.greaterThan=" + SMALLER_GEMEINSAMER_WECHSELRICHTER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungBehoerdeIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungBehoerde equals to DEFAULT_GENEHMIGUNG_BEHOERDE
        defaultMastrUnitShouldBeFound("genehmigungBehoerde.equals=" + DEFAULT_GENEHMIGUNG_BEHOERDE);

        // Get all the mastrUnitList where genehmigungBehoerde equals to UPDATED_GENEHMIGUNG_BEHOERDE
        defaultMastrUnitShouldNotBeFound("genehmigungBehoerde.equals=" + UPDATED_GENEHMIGUNG_BEHOERDE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungBehoerdeIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungBehoerde in DEFAULT_GENEHMIGUNG_BEHOERDE or UPDATED_GENEHMIGUNG_BEHOERDE
        defaultMastrUnitShouldBeFound("genehmigungBehoerde.in=" + DEFAULT_GENEHMIGUNG_BEHOERDE + "," + UPDATED_GENEHMIGUNG_BEHOERDE);

        // Get all the mastrUnitList where genehmigungBehoerde equals to UPDATED_GENEHMIGUNG_BEHOERDE
        defaultMastrUnitShouldNotBeFound("genehmigungBehoerde.in=" + UPDATED_GENEHMIGUNG_BEHOERDE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungBehoerdeIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungBehoerde is not null
        defaultMastrUnitShouldBeFound("genehmigungBehoerde.specified=true");

        // Get all the mastrUnitList where genehmigungBehoerde is null
        defaultMastrUnitShouldNotBeFound("genehmigungBehoerde.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungBehoerdeContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungBehoerde contains DEFAULT_GENEHMIGUNG_BEHOERDE
        defaultMastrUnitShouldBeFound("genehmigungBehoerde.contains=" + DEFAULT_GENEHMIGUNG_BEHOERDE);

        // Get all the mastrUnitList where genehmigungBehoerde contains UPDATED_GENEHMIGUNG_BEHOERDE
        defaultMastrUnitShouldNotBeFound("genehmigungBehoerde.contains=" + UPDATED_GENEHMIGUNG_BEHOERDE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungBehoerdeNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungBehoerde does not contain DEFAULT_GENEHMIGUNG_BEHOERDE
        defaultMastrUnitShouldNotBeFound("genehmigungBehoerde.doesNotContain=" + DEFAULT_GENEHMIGUNG_BEHOERDE);

        // Get all the mastrUnitList where genehmigungBehoerde does not contain UPDATED_GENEHMIGUNG_BEHOERDE
        defaultMastrUnitShouldBeFound("genehmigungBehoerde.doesNotContain=" + UPDATED_GENEHMIGUNG_BEHOERDE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungsMastrNummerIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungsMastrNummer equals to DEFAULT_GENEHMIGUNGS_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("genehmigungsMastrNummer.equals=" + DEFAULT_GENEHMIGUNGS_MASTR_NUMMER);

        // Get all the mastrUnitList where genehmigungsMastrNummer equals to UPDATED_GENEHMIGUNGS_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("genehmigungsMastrNummer.equals=" + UPDATED_GENEHMIGUNGS_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungsMastrNummerIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungsMastrNummer in DEFAULT_GENEHMIGUNGS_MASTR_NUMMER or UPDATED_GENEHMIGUNGS_MASTR_NUMMER
        defaultMastrUnitShouldBeFound(
            "genehmigungsMastrNummer.in=" + DEFAULT_GENEHMIGUNGS_MASTR_NUMMER + "," + UPDATED_GENEHMIGUNGS_MASTR_NUMMER
        );

        // Get all the mastrUnitList where genehmigungsMastrNummer equals to UPDATED_GENEHMIGUNGS_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("genehmigungsMastrNummer.in=" + UPDATED_GENEHMIGUNGS_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungsMastrNummerIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungsMastrNummer is not null
        defaultMastrUnitShouldBeFound("genehmigungsMastrNummer.specified=true");

        // Get all the mastrUnitList where genehmigungsMastrNummer is null
        defaultMastrUnitShouldNotBeFound("genehmigungsMastrNummer.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungsMastrNummerContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungsMastrNummer contains DEFAULT_GENEHMIGUNGS_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("genehmigungsMastrNummer.contains=" + DEFAULT_GENEHMIGUNGS_MASTR_NUMMER);

        // Get all the mastrUnitList where genehmigungsMastrNummer contains UPDATED_GENEHMIGUNGS_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("genehmigungsMastrNummer.contains=" + UPDATED_GENEHMIGUNGS_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGenehmigungsMastrNummerNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where genehmigungsMastrNummer does not contain DEFAULT_GENEHMIGUNGS_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("genehmigungsMastrNummer.doesNotContain=" + DEFAULT_GENEHMIGUNGS_MASTR_NUMMER);

        // Get all the mastrUnitList where genehmigungsMastrNummer does not contain UPDATED_GENEHMIGUNGS_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("genehmigungsMastrNummer.doesNotContain=" + UPDATED_GENEHMIGUNGS_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGruppierungsObjekteIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gruppierungsObjekte equals to DEFAULT_GRUPPIERUNGS_OBJEKTE
        defaultMastrUnitShouldBeFound("gruppierungsObjekte.equals=" + DEFAULT_GRUPPIERUNGS_OBJEKTE);

        // Get all the mastrUnitList where gruppierungsObjekte equals to UPDATED_GRUPPIERUNGS_OBJEKTE
        defaultMastrUnitShouldNotBeFound("gruppierungsObjekte.equals=" + UPDATED_GRUPPIERUNGS_OBJEKTE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGruppierungsObjekteIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gruppierungsObjekte in DEFAULT_GRUPPIERUNGS_OBJEKTE or UPDATED_GRUPPIERUNGS_OBJEKTE
        defaultMastrUnitShouldBeFound("gruppierungsObjekte.in=" + DEFAULT_GRUPPIERUNGS_OBJEKTE + "," + UPDATED_GRUPPIERUNGS_OBJEKTE);

        // Get all the mastrUnitList where gruppierungsObjekte equals to UPDATED_GRUPPIERUNGS_OBJEKTE
        defaultMastrUnitShouldNotBeFound("gruppierungsObjekte.in=" + UPDATED_GRUPPIERUNGS_OBJEKTE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGruppierungsObjekteIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gruppierungsObjekte is not null
        defaultMastrUnitShouldBeFound("gruppierungsObjekte.specified=true");

        // Get all the mastrUnitList where gruppierungsObjekte is null
        defaultMastrUnitShouldNotBeFound("gruppierungsObjekte.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGruppierungsObjekteContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gruppierungsObjekte contains DEFAULT_GRUPPIERUNGS_OBJEKTE
        defaultMastrUnitShouldBeFound("gruppierungsObjekte.contains=" + DEFAULT_GRUPPIERUNGS_OBJEKTE);

        // Get all the mastrUnitList where gruppierungsObjekte contains UPDATED_GRUPPIERUNGS_OBJEKTE
        defaultMastrUnitShouldNotBeFound("gruppierungsObjekte.contains=" + UPDATED_GRUPPIERUNGS_OBJEKTE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGruppierungsObjekteNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gruppierungsObjekte does not contain DEFAULT_GRUPPIERUNGS_OBJEKTE
        defaultMastrUnitShouldNotBeFound("gruppierungsObjekte.doesNotContain=" + DEFAULT_GRUPPIERUNGS_OBJEKTE);

        // Get all the mastrUnitList where gruppierungsObjekte does not contain UPDATED_GRUPPIERUNGS_OBJEKTE
        defaultMastrUnitShouldBeFound("gruppierungsObjekte.doesNotContain=" + UPDATED_GRUPPIERUNGS_OBJEKTE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGruppierungsObjekteIdsIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gruppierungsObjekteIds equals to DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS
        defaultMastrUnitShouldBeFound("gruppierungsObjekteIds.equals=" + DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS);

        // Get all the mastrUnitList where gruppierungsObjekteIds equals to UPDATED_GRUPPIERUNGS_OBJEKTE_IDS
        defaultMastrUnitShouldNotBeFound("gruppierungsObjekteIds.equals=" + UPDATED_GRUPPIERUNGS_OBJEKTE_IDS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGruppierungsObjekteIdsIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gruppierungsObjekteIds in DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS or UPDATED_GRUPPIERUNGS_OBJEKTE_IDS
        defaultMastrUnitShouldBeFound(
            "gruppierungsObjekteIds.in=" + DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS + "," + UPDATED_GRUPPIERUNGS_OBJEKTE_IDS
        );

        // Get all the mastrUnitList where gruppierungsObjekteIds equals to UPDATED_GRUPPIERUNGS_OBJEKTE_IDS
        defaultMastrUnitShouldNotBeFound("gruppierungsObjekteIds.in=" + UPDATED_GRUPPIERUNGS_OBJEKTE_IDS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGruppierungsObjekteIdsIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gruppierungsObjekteIds is not null
        defaultMastrUnitShouldBeFound("gruppierungsObjekteIds.specified=true");

        // Get all the mastrUnitList where gruppierungsObjekteIds is null
        defaultMastrUnitShouldNotBeFound("gruppierungsObjekteIds.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGruppierungsObjekteIdsContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gruppierungsObjekteIds contains DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS
        defaultMastrUnitShouldBeFound("gruppierungsObjekteIds.contains=" + DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS);

        // Get all the mastrUnitList where gruppierungsObjekteIds contains UPDATED_GRUPPIERUNGS_OBJEKTE_IDS
        defaultMastrUnitShouldNotBeFound("gruppierungsObjekteIds.contains=" + UPDATED_GRUPPIERUNGS_OBJEKTE_IDS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGruppierungsObjekteIdsNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where gruppierungsObjekteIds does not contain DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS
        defaultMastrUnitShouldNotBeFound("gruppierungsObjekteIds.doesNotContain=" + DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS);

        // Get all the mastrUnitList where gruppierungsObjekteIds does not contain UPDATED_GRUPPIERUNGS_OBJEKTE_IDS
        defaultMastrUnitShouldBeFound("gruppierungsObjekteIds.doesNotContain=" + UPDATED_GRUPPIERUNGS_OBJEKTE_IDS);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHatFlexibilitaetsPraemieIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hatFlexibilitaetsPraemie equals to DEFAULT_HAT_FLEXIBILITAETS_PRAEMIE
        defaultMastrUnitShouldBeFound("hatFlexibilitaetsPraemie.equals=" + DEFAULT_HAT_FLEXIBILITAETS_PRAEMIE);

        // Get all the mastrUnitList where hatFlexibilitaetsPraemie equals to UPDATED_HAT_FLEXIBILITAETS_PRAEMIE
        defaultMastrUnitShouldNotBeFound("hatFlexibilitaetsPraemie.equals=" + UPDATED_HAT_FLEXIBILITAETS_PRAEMIE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHatFlexibilitaetsPraemieIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hatFlexibilitaetsPraemie in DEFAULT_HAT_FLEXIBILITAETS_PRAEMIE or UPDATED_HAT_FLEXIBILITAETS_PRAEMIE
        defaultMastrUnitShouldBeFound(
            "hatFlexibilitaetsPraemie.in=" + DEFAULT_HAT_FLEXIBILITAETS_PRAEMIE + "," + UPDATED_HAT_FLEXIBILITAETS_PRAEMIE
        );

        // Get all the mastrUnitList where hatFlexibilitaetsPraemie equals to UPDATED_HAT_FLEXIBILITAETS_PRAEMIE
        defaultMastrUnitShouldNotBeFound("hatFlexibilitaetsPraemie.in=" + UPDATED_HAT_FLEXIBILITAETS_PRAEMIE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHatFlexibilitaetsPraemieIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hatFlexibilitaetsPraemie is not null
        defaultMastrUnitShouldBeFound("hatFlexibilitaetsPraemie.specified=true");

        // Get all the mastrUnitList where hatFlexibilitaetsPraemie is null
        defaultMastrUnitShouldNotBeFound("hatFlexibilitaetsPraemie.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptAusrichtungSolarmoduleIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule equals to DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE
        defaultMastrUnitShouldBeFound("hauptAusrichtungSolarmodule.equals=" + DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE);

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule equals to UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE
        defaultMastrUnitShouldNotBeFound("hauptAusrichtungSolarmodule.equals=" + UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptAusrichtungSolarmoduleIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule in DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE or UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE
        defaultMastrUnitShouldBeFound(
            "hauptAusrichtungSolarmodule.in=" + DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE + "," + UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE
        );

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule equals to UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE
        defaultMastrUnitShouldNotBeFound("hauptAusrichtungSolarmodule.in=" + UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptAusrichtungSolarmoduleIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule is not null
        defaultMastrUnitShouldBeFound("hauptAusrichtungSolarmodule.specified=true");

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule is null
        defaultMastrUnitShouldNotBeFound("hauptAusrichtungSolarmodule.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptAusrichtungSolarmoduleIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule is greater than or equal to DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE
        defaultMastrUnitShouldBeFound("hauptAusrichtungSolarmodule.greaterThanOrEqual=" + DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE);

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule is greater than or equal to UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE
        defaultMastrUnitShouldNotBeFound("hauptAusrichtungSolarmodule.greaterThanOrEqual=" + UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptAusrichtungSolarmoduleIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule is less than or equal to DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE
        defaultMastrUnitShouldBeFound("hauptAusrichtungSolarmodule.lessThanOrEqual=" + DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE);

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule is less than or equal to SMALLER_HAUPT_AUSRICHTUNG_SOLARMODULE
        defaultMastrUnitShouldNotBeFound("hauptAusrichtungSolarmodule.lessThanOrEqual=" + SMALLER_HAUPT_AUSRICHTUNG_SOLARMODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptAusrichtungSolarmoduleIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule is less than DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE
        defaultMastrUnitShouldNotBeFound("hauptAusrichtungSolarmodule.lessThan=" + DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE);

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule is less than UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE
        defaultMastrUnitShouldBeFound("hauptAusrichtungSolarmodule.lessThan=" + UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptAusrichtungSolarmoduleIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule is greater than DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE
        defaultMastrUnitShouldNotBeFound("hauptAusrichtungSolarmodule.greaterThan=" + DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE);

        // Get all the mastrUnitList where hauptAusrichtungSolarmodule is greater than SMALLER_HAUPT_AUSRICHTUNG_SOLARMODULE
        defaultMastrUnitShouldBeFound("hauptAusrichtungSolarmodule.greaterThan=" + SMALLER_HAUPT_AUSRICHTUNG_SOLARMODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptAusrichtungSolarmoduleBezeichnungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptAusrichtungSolarmoduleBezeichnung equals to DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        defaultMastrUnitShouldBeFound("hauptAusrichtungSolarmoduleBezeichnung.equals=" + DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG);

        // Get all the mastrUnitList where hauptAusrichtungSolarmoduleBezeichnung equals to UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound(
            "hauptAusrichtungSolarmoduleBezeichnung.equals=" + UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        );
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptAusrichtungSolarmoduleBezeichnungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptAusrichtungSolarmoduleBezeichnung in DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG or UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        defaultMastrUnitShouldBeFound(
            "hauptAusrichtungSolarmoduleBezeichnung.in=" +
            DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG +
            "," +
            UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        );

        // Get all the mastrUnitList where hauptAusrichtungSolarmoduleBezeichnung equals to UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("hauptAusrichtungSolarmoduleBezeichnung.in=" + UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptAusrichtungSolarmoduleBezeichnungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptAusrichtungSolarmoduleBezeichnung is not null
        defaultMastrUnitShouldBeFound("hauptAusrichtungSolarmoduleBezeichnung.specified=true");

        // Get all the mastrUnitList where hauptAusrichtungSolarmoduleBezeichnung is null
        defaultMastrUnitShouldNotBeFound("hauptAusrichtungSolarmoduleBezeichnung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptAusrichtungSolarmoduleBezeichnungContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptAusrichtungSolarmoduleBezeichnung contains DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        defaultMastrUnitShouldBeFound(
            "hauptAusrichtungSolarmoduleBezeichnung.contains=" + DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        );

        // Get all the mastrUnitList where hauptAusrichtungSolarmoduleBezeichnung contains UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound(
            "hauptAusrichtungSolarmoduleBezeichnung.contains=" + UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        );
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptAusrichtungSolarmoduleBezeichnungNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptAusrichtungSolarmoduleBezeichnung does not contain DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound(
            "hauptAusrichtungSolarmoduleBezeichnung.doesNotContain=" + DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        );

        // Get all the mastrUnitList where hauptAusrichtungSolarmoduleBezeichnung does not contain UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        defaultMastrUnitShouldBeFound(
            "hauptAusrichtungSolarmoduleBezeichnung.doesNotContain=" + UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG
        );
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptBrennstoffIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptBrennstoffId equals to DEFAULT_HAUPT_BRENNSTOFF_ID
        defaultMastrUnitShouldBeFound("hauptBrennstoffId.equals=" + DEFAULT_HAUPT_BRENNSTOFF_ID);

        // Get all the mastrUnitList where hauptBrennstoffId equals to UPDATED_HAUPT_BRENNSTOFF_ID
        defaultMastrUnitShouldNotBeFound("hauptBrennstoffId.equals=" + UPDATED_HAUPT_BRENNSTOFF_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptBrennstoffIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptBrennstoffId in DEFAULT_HAUPT_BRENNSTOFF_ID or UPDATED_HAUPT_BRENNSTOFF_ID
        defaultMastrUnitShouldBeFound("hauptBrennstoffId.in=" + DEFAULT_HAUPT_BRENNSTOFF_ID + "," + UPDATED_HAUPT_BRENNSTOFF_ID);

        // Get all the mastrUnitList where hauptBrennstoffId equals to UPDATED_HAUPT_BRENNSTOFF_ID
        defaultMastrUnitShouldNotBeFound("hauptBrennstoffId.in=" + UPDATED_HAUPT_BRENNSTOFF_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptBrennstoffIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptBrennstoffId is not null
        defaultMastrUnitShouldBeFound("hauptBrennstoffId.specified=true");

        // Get all the mastrUnitList where hauptBrennstoffId is null
        defaultMastrUnitShouldNotBeFound("hauptBrennstoffId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptBrennstoffIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptBrennstoffId is greater than or equal to DEFAULT_HAUPT_BRENNSTOFF_ID
        defaultMastrUnitShouldBeFound("hauptBrennstoffId.greaterThanOrEqual=" + DEFAULT_HAUPT_BRENNSTOFF_ID);

        // Get all the mastrUnitList where hauptBrennstoffId is greater than or equal to UPDATED_HAUPT_BRENNSTOFF_ID
        defaultMastrUnitShouldNotBeFound("hauptBrennstoffId.greaterThanOrEqual=" + UPDATED_HAUPT_BRENNSTOFF_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptBrennstoffIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptBrennstoffId is less than or equal to DEFAULT_HAUPT_BRENNSTOFF_ID
        defaultMastrUnitShouldBeFound("hauptBrennstoffId.lessThanOrEqual=" + DEFAULT_HAUPT_BRENNSTOFF_ID);

        // Get all the mastrUnitList where hauptBrennstoffId is less than or equal to SMALLER_HAUPT_BRENNSTOFF_ID
        defaultMastrUnitShouldNotBeFound("hauptBrennstoffId.lessThanOrEqual=" + SMALLER_HAUPT_BRENNSTOFF_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptBrennstoffIdIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptBrennstoffId is less than DEFAULT_HAUPT_BRENNSTOFF_ID
        defaultMastrUnitShouldNotBeFound("hauptBrennstoffId.lessThan=" + DEFAULT_HAUPT_BRENNSTOFF_ID);

        // Get all the mastrUnitList where hauptBrennstoffId is less than UPDATED_HAUPT_BRENNSTOFF_ID
        defaultMastrUnitShouldBeFound("hauptBrennstoffId.lessThan=" + UPDATED_HAUPT_BRENNSTOFF_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptBrennstoffIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptBrennstoffId is greater than DEFAULT_HAUPT_BRENNSTOFF_ID
        defaultMastrUnitShouldNotBeFound("hauptBrennstoffId.greaterThan=" + DEFAULT_HAUPT_BRENNSTOFF_ID);

        // Get all the mastrUnitList where hauptBrennstoffId is greater than SMALLER_HAUPT_BRENNSTOFF_ID
        defaultMastrUnitShouldBeFound("hauptBrennstoffId.greaterThan=" + SMALLER_HAUPT_BRENNSTOFF_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptBrennstoffNamenIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptBrennstoffNamen equals to DEFAULT_HAUPT_BRENNSTOFF_NAMEN
        defaultMastrUnitShouldBeFound("hauptBrennstoffNamen.equals=" + DEFAULT_HAUPT_BRENNSTOFF_NAMEN);

        // Get all the mastrUnitList where hauptBrennstoffNamen equals to UPDATED_HAUPT_BRENNSTOFF_NAMEN
        defaultMastrUnitShouldNotBeFound("hauptBrennstoffNamen.equals=" + UPDATED_HAUPT_BRENNSTOFF_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptBrennstoffNamenIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptBrennstoffNamen in DEFAULT_HAUPT_BRENNSTOFF_NAMEN or UPDATED_HAUPT_BRENNSTOFF_NAMEN
        defaultMastrUnitShouldBeFound("hauptBrennstoffNamen.in=" + DEFAULT_HAUPT_BRENNSTOFF_NAMEN + "," + UPDATED_HAUPT_BRENNSTOFF_NAMEN);

        // Get all the mastrUnitList where hauptBrennstoffNamen equals to UPDATED_HAUPT_BRENNSTOFF_NAMEN
        defaultMastrUnitShouldNotBeFound("hauptBrennstoffNamen.in=" + UPDATED_HAUPT_BRENNSTOFF_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptBrennstoffNamenIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptBrennstoffNamen is not null
        defaultMastrUnitShouldBeFound("hauptBrennstoffNamen.specified=true");

        // Get all the mastrUnitList where hauptBrennstoffNamen is null
        defaultMastrUnitShouldNotBeFound("hauptBrennstoffNamen.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptBrennstoffNamenContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptBrennstoffNamen contains DEFAULT_HAUPT_BRENNSTOFF_NAMEN
        defaultMastrUnitShouldBeFound("hauptBrennstoffNamen.contains=" + DEFAULT_HAUPT_BRENNSTOFF_NAMEN);

        // Get all the mastrUnitList where hauptBrennstoffNamen contains UPDATED_HAUPT_BRENNSTOFF_NAMEN
        defaultMastrUnitShouldNotBeFound("hauptBrennstoffNamen.contains=" + UPDATED_HAUPT_BRENNSTOFF_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptBrennstoffNamenNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptBrennstoffNamen does not contain DEFAULT_HAUPT_BRENNSTOFF_NAMEN
        defaultMastrUnitShouldNotBeFound("hauptBrennstoffNamen.doesNotContain=" + DEFAULT_HAUPT_BRENNSTOFF_NAMEN);

        // Get all the mastrUnitList where hauptBrennstoffNamen does not contain UPDATED_HAUPT_BRENNSTOFF_NAMEN
        defaultMastrUnitShouldBeFound("hauptBrennstoffNamen.doesNotContain=" + UPDATED_HAUPT_BRENNSTOFF_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptNeigungswinkelSolarModuleIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule equals to DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        defaultMastrUnitShouldBeFound("hauptNeigungswinkelSolarModule.equals=" + DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule equals to UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        defaultMastrUnitShouldNotBeFound("hauptNeigungswinkelSolarModule.equals=" + UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptNeigungswinkelSolarModuleIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule in DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE or UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        defaultMastrUnitShouldBeFound(
            "hauptNeigungswinkelSolarModule.in=" +
            DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE +
            "," +
            UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        );

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule equals to UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        defaultMastrUnitShouldNotBeFound("hauptNeigungswinkelSolarModule.in=" + UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptNeigungswinkelSolarModuleIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule is not null
        defaultMastrUnitShouldBeFound("hauptNeigungswinkelSolarModule.specified=true");

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule is null
        defaultMastrUnitShouldNotBeFound("hauptNeigungswinkelSolarModule.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptNeigungswinkelSolarModuleIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule is greater than or equal to DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        defaultMastrUnitShouldBeFound("hauptNeigungswinkelSolarModule.greaterThanOrEqual=" + DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule is greater than or equal to UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        defaultMastrUnitShouldNotBeFound("hauptNeigungswinkelSolarModule.greaterThanOrEqual=" + UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptNeigungswinkelSolarModuleIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule is less than or equal to DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        defaultMastrUnitShouldBeFound("hauptNeigungswinkelSolarModule.lessThanOrEqual=" + DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule is less than or equal to SMALLER_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        defaultMastrUnitShouldNotBeFound("hauptNeigungswinkelSolarModule.lessThanOrEqual=" + SMALLER_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptNeigungswinkelSolarModuleIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule is less than DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        defaultMastrUnitShouldNotBeFound("hauptNeigungswinkelSolarModule.lessThan=" + DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule is less than UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        defaultMastrUnitShouldBeFound("hauptNeigungswinkelSolarModule.lessThan=" + UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHauptNeigungswinkelSolarModuleIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule is greater than DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        defaultMastrUnitShouldNotBeFound("hauptNeigungswinkelSolarModule.greaterThan=" + DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);

        // Get all the mastrUnitList where hauptNeigungswinkelSolarModule is greater than SMALLER_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE
        defaultMastrUnitShouldBeFound("hauptNeigungswinkelSolarModule.greaterThan=" + SMALLER_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHerstellerWindenergieanlageIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where herstellerWindenergieanlageId equals to DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID
        defaultMastrUnitShouldBeFound("herstellerWindenergieanlageId.equals=" + DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID);

        // Get all the mastrUnitList where herstellerWindenergieanlageId equals to UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID
        defaultMastrUnitShouldNotBeFound("herstellerWindenergieanlageId.equals=" + UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHerstellerWindenergieanlageIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where herstellerWindenergieanlageId in DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID or UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID
        defaultMastrUnitShouldBeFound(
            "herstellerWindenergieanlageId.in=" + DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID + "," + UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID
        );

        // Get all the mastrUnitList where herstellerWindenergieanlageId equals to UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID
        defaultMastrUnitShouldNotBeFound("herstellerWindenergieanlageId.in=" + UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHerstellerWindenergieanlageIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where herstellerWindenergieanlageId is not null
        defaultMastrUnitShouldBeFound("herstellerWindenergieanlageId.specified=true");

        // Get all the mastrUnitList where herstellerWindenergieanlageId is null
        defaultMastrUnitShouldNotBeFound("herstellerWindenergieanlageId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHerstellerWindenergieanlageIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where herstellerWindenergieanlageId is greater than or equal to DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID
        defaultMastrUnitShouldBeFound("herstellerWindenergieanlageId.greaterThanOrEqual=" + DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID);

        // Get all the mastrUnitList where herstellerWindenergieanlageId is greater than or equal to UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID
        defaultMastrUnitShouldNotBeFound("herstellerWindenergieanlageId.greaterThanOrEqual=" + UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHerstellerWindenergieanlageIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where herstellerWindenergieanlageId is less than or equal to DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID
        defaultMastrUnitShouldBeFound("herstellerWindenergieanlageId.lessThanOrEqual=" + DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID);

        // Get all the mastrUnitList where herstellerWindenergieanlageId is less than or equal to SMALLER_HERSTELLER_WINDENERGIEANLAGE_ID
        defaultMastrUnitShouldNotBeFound("herstellerWindenergieanlageId.lessThanOrEqual=" + SMALLER_HERSTELLER_WINDENERGIEANLAGE_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHerstellerWindenergieanlageIdIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where herstellerWindenergieanlageId is less than DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID
        defaultMastrUnitShouldNotBeFound("herstellerWindenergieanlageId.lessThan=" + DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID);

        // Get all the mastrUnitList where herstellerWindenergieanlageId is less than UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID
        defaultMastrUnitShouldBeFound("herstellerWindenergieanlageId.lessThan=" + UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHerstellerWindenergieanlageIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where herstellerWindenergieanlageId is greater than DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID
        defaultMastrUnitShouldNotBeFound("herstellerWindenergieanlageId.greaterThan=" + DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID);

        // Get all the mastrUnitList where herstellerWindenergieanlageId is greater than SMALLER_HERSTELLER_WINDENERGIEANLAGE_ID
        defaultMastrUnitShouldBeFound("herstellerWindenergieanlageId.greaterThan=" + SMALLER_HERSTELLER_WINDENERGIEANLAGE_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHerstellerWindenergieanlageBezeichnungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where herstellerWindenergieanlageBezeichnung equals to DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        defaultMastrUnitShouldBeFound("herstellerWindenergieanlageBezeichnung.equals=" + DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG);

        // Get all the mastrUnitList where herstellerWindenergieanlageBezeichnung equals to UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound(
            "herstellerWindenergieanlageBezeichnung.equals=" + UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        );
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHerstellerWindenergieanlageBezeichnungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where herstellerWindenergieanlageBezeichnung in DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG or UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        defaultMastrUnitShouldBeFound(
            "herstellerWindenergieanlageBezeichnung.in=" +
            DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG +
            "," +
            UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        );

        // Get all the mastrUnitList where herstellerWindenergieanlageBezeichnung equals to UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("herstellerWindenergieanlageBezeichnung.in=" + UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHerstellerWindenergieanlageBezeichnungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where herstellerWindenergieanlageBezeichnung is not null
        defaultMastrUnitShouldBeFound("herstellerWindenergieanlageBezeichnung.specified=true");

        // Get all the mastrUnitList where herstellerWindenergieanlageBezeichnung is null
        defaultMastrUnitShouldNotBeFound("herstellerWindenergieanlageBezeichnung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHerstellerWindenergieanlageBezeichnungContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where herstellerWindenergieanlageBezeichnung contains DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        defaultMastrUnitShouldBeFound(
            "herstellerWindenergieanlageBezeichnung.contains=" + DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        );

        // Get all the mastrUnitList where herstellerWindenergieanlageBezeichnung contains UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound(
            "herstellerWindenergieanlageBezeichnung.contains=" + UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        );
    }

    @Test
    @Transactional
    void getAllMastrUnitsByHerstellerWindenergieanlageBezeichnungNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where herstellerWindenergieanlageBezeichnung does not contain DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound(
            "herstellerWindenergieanlageBezeichnung.doesNotContain=" + DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        );

        // Get all the mastrUnitList where herstellerWindenergieanlageBezeichnung does not contain UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        defaultMastrUnitShouldBeFound(
            "herstellerWindenergieanlageBezeichnung.doesNotContain=" + UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG
        );
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageElektrischeLeistungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung equals to DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        defaultMastrUnitShouldBeFound("kwkAnlageElektrischeLeistung.equals=" + DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung equals to UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        defaultMastrUnitShouldNotBeFound("kwkAnlageElektrischeLeistung.equals=" + UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageElektrischeLeistungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung in DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG or UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        defaultMastrUnitShouldBeFound(
            "kwkAnlageElektrischeLeistung.in=" + DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG + "," + UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        );

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung equals to UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        defaultMastrUnitShouldNotBeFound("kwkAnlageElektrischeLeistung.in=" + UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageElektrischeLeistungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung is not null
        defaultMastrUnitShouldBeFound("kwkAnlageElektrischeLeistung.specified=true");

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung is null
        defaultMastrUnitShouldNotBeFound("kwkAnlageElektrischeLeistung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageElektrischeLeistungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung is greater than or equal to DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        defaultMastrUnitShouldBeFound("kwkAnlageElektrischeLeistung.greaterThanOrEqual=" + DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung is greater than or equal to UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        defaultMastrUnitShouldNotBeFound("kwkAnlageElektrischeLeistung.greaterThanOrEqual=" + UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageElektrischeLeistungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung is less than or equal to DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        defaultMastrUnitShouldBeFound("kwkAnlageElektrischeLeistung.lessThanOrEqual=" + DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung is less than or equal to SMALLER_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        defaultMastrUnitShouldNotBeFound("kwkAnlageElektrischeLeistung.lessThanOrEqual=" + SMALLER_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageElektrischeLeistungIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung is less than DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        defaultMastrUnitShouldNotBeFound("kwkAnlageElektrischeLeistung.lessThan=" + DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung is less than UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        defaultMastrUnitShouldBeFound("kwkAnlageElektrischeLeistung.lessThan=" + UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageElektrischeLeistungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung is greater than DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        defaultMastrUnitShouldNotBeFound("kwkAnlageElektrischeLeistung.greaterThan=" + DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);

        // Get all the mastrUnitList where kwkAnlageElektrischeLeistung is greater than SMALLER_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG
        defaultMastrUnitShouldBeFound("kwkAnlageElektrischeLeistung.greaterThan=" + SMALLER_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageMastrNummerIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageMastrNummer equals to DEFAULT_KWK_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("kwkAnlageMastrNummer.equals=" + DEFAULT_KWK_ANLAGE_MASTR_NUMMER);

        // Get all the mastrUnitList where kwkAnlageMastrNummer equals to UPDATED_KWK_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("kwkAnlageMastrNummer.equals=" + UPDATED_KWK_ANLAGE_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageMastrNummerIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageMastrNummer in DEFAULT_KWK_ANLAGE_MASTR_NUMMER or UPDATED_KWK_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("kwkAnlageMastrNummer.in=" + DEFAULT_KWK_ANLAGE_MASTR_NUMMER + "," + UPDATED_KWK_ANLAGE_MASTR_NUMMER);

        // Get all the mastrUnitList where kwkAnlageMastrNummer equals to UPDATED_KWK_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("kwkAnlageMastrNummer.in=" + UPDATED_KWK_ANLAGE_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageMastrNummerIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageMastrNummer is not null
        defaultMastrUnitShouldBeFound("kwkAnlageMastrNummer.specified=true");

        // Get all the mastrUnitList where kwkAnlageMastrNummer is null
        defaultMastrUnitShouldNotBeFound("kwkAnlageMastrNummer.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageMastrNummerContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageMastrNummer contains DEFAULT_KWK_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("kwkAnlageMastrNummer.contains=" + DEFAULT_KWK_ANLAGE_MASTR_NUMMER);

        // Get all the mastrUnitList where kwkAnlageMastrNummer contains UPDATED_KWK_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("kwkAnlageMastrNummer.contains=" + UPDATED_KWK_ANLAGE_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkAnlageMastrNummerNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkAnlageMastrNummer does not contain DEFAULT_KWK_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("kwkAnlageMastrNummer.doesNotContain=" + DEFAULT_KWK_ANLAGE_MASTR_NUMMER);

        // Get all the mastrUnitList where kwkAnlageMastrNummer does not contain UPDATED_KWK_ANLAGE_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("kwkAnlageMastrNummer.doesNotContain=" + UPDATED_KWK_ANLAGE_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkZuschlagIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkZuschlag equals to DEFAULT_KWK_ZUSCHLAG
        defaultMastrUnitShouldBeFound("kwkZuschlag.equals=" + DEFAULT_KWK_ZUSCHLAG);

        // Get all the mastrUnitList where kwkZuschlag equals to UPDATED_KWK_ZUSCHLAG
        defaultMastrUnitShouldNotBeFound("kwkZuschlag.equals=" + UPDATED_KWK_ZUSCHLAG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkZuschlagIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkZuschlag in DEFAULT_KWK_ZUSCHLAG or UPDATED_KWK_ZUSCHLAG
        defaultMastrUnitShouldBeFound("kwkZuschlag.in=" + DEFAULT_KWK_ZUSCHLAG + "," + UPDATED_KWK_ZUSCHLAG);

        // Get all the mastrUnitList where kwkZuschlag equals to UPDATED_KWK_ZUSCHLAG
        defaultMastrUnitShouldNotBeFound("kwkZuschlag.in=" + UPDATED_KWK_ZUSCHLAG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkZuschlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkZuschlag is not null
        defaultMastrUnitShouldBeFound("kwkZuschlag.specified=true");

        // Get all the mastrUnitList where kwkZuschlag is null
        defaultMastrUnitShouldNotBeFound("kwkZuschlag.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkZuschlagContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkZuschlag contains DEFAULT_KWK_ZUSCHLAG
        defaultMastrUnitShouldBeFound("kwkZuschlag.contains=" + DEFAULT_KWK_ZUSCHLAG);

        // Get all the mastrUnitList where kwkZuschlag contains UPDATED_KWK_ZUSCHLAG
        defaultMastrUnitShouldNotBeFound("kwkZuschlag.contains=" + UPDATED_KWK_ZUSCHLAG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByKwkZuschlagNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where kwkZuschlag does not contain DEFAULT_KWK_ZUSCHLAG
        defaultMastrUnitShouldNotBeFound("kwkZuschlag.doesNotContain=" + DEFAULT_KWK_ZUSCHLAG);

        // Get all the mastrUnitList where kwkZuschlag does not contain UPDATED_KWK_ZUSCHLAG
        defaultMastrUnitShouldBeFound("kwkZuschlag.doesNotContain=" + UPDATED_KWK_ZUSCHLAG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLageEinheitIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lageEinheit equals to DEFAULT_LAGE_EINHEIT
        defaultMastrUnitShouldBeFound("lageEinheit.equals=" + DEFAULT_LAGE_EINHEIT);

        // Get all the mastrUnitList where lageEinheit equals to UPDATED_LAGE_EINHEIT
        defaultMastrUnitShouldNotBeFound("lageEinheit.equals=" + UPDATED_LAGE_EINHEIT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLageEinheitIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lageEinheit in DEFAULT_LAGE_EINHEIT or UPDATED_LAGE_EINHEIT
        defaultMastrUnitShouldBeFound("lageEinheit.in=" + DEFAULT_LAGE_EINHEIT + "," + UPDATED_LAGE_EINHEIT);

        // Get all the mastrUnitList where lageEinheit equals to UPDATED_LAGE_EINHEIT
        defaultMastrUnitShouldNotBeFound("lageEinheit.in=" + UPDATED_LAGE_EINHEIT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLageEinheitIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lageEinheit is not null
        defaultMastrUnitShouldBeFound("lageEinheit.specified=true");

        // Get all the mastrUnitList where lageEinheit is null
        defaultMastrUnitShouldNotBeFound("lageEinheit.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLageEinheitIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lageEinheit is greater than or equal to DEFAULT_LAGE_EINHEIT
        defaultMastrUnitShouldBeFound("lageEinheit.greaterThanOrEqual=" + DEFAULT_LAGE_EINHEIT);

        // Get all the mastrUnitList where lageEinheit is greater than or equal to UPDATED_LAGE_EINHEIT
        defaultMastrUnitShouldNotBeFound("lageEinheit.greaterThanOrEqual=" + UPDATED_LAGE_EINHEIT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLageEinheitIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lageEinheit is less than or equal to DEFAULT_LAGE_EINHEIT
        defaultMastrUnitShouldBeFound("lageEinheit.lessThanOrEqual=" + DEFAULT_LAGE_EINHEIT);

        // Get all the mastrUnitList where lageEinheit is less than or equal to SMALLER_LAGE_EINHEIT
        defaultMastrUnitShouldNotBeFound("lageEinheit.lessThanOrEqual=" + SMALLER_LAGE_EINHEIT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLageEinheitIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lageEinheit is less than DEFAULT_LAGE_EINHEIT
        defaultMastrUnitShouldNotBeFound("lageEinheit.lessThan=" + DEFAULT_LAGE_EINHEIT);

        // Get all the mastrUnitList where lageEinheit is less than UPDATED_LAGE_EINHEIT
        defaultMastrUnitShouldBeFound("lageEinheit.lessThan=" + UPDATED_LAGE_EINHEIT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLageEinheitIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lageEinheit is greater than DEFAULT_LAGE_EINHEIT
        defaultMastrUnitShouldNotBeFound("lageEinheit.greaterThan=" + DEFAULT_LAGE_EINHEIT);

        // Get all the mastrUnitList where lageEinheit is greater than SMALLER_LAGE_EINHEIT
        defaultMastrUnitShouldBeFound("lageEinheit.greaterThan=" + SMALLER_LAGE_EINHEIT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLageEinheitBezeichnungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lageEinheitBezeichnung equals to DEFAULT_LAGE_EINHEIT_BEZEICHNUNG
        defaultMastrUnitShouldBeFound("lageEinheitBezeichnung.equals=" + DEFAULT_LAGE_EINHEIT_BEZEICHNUNG);

        // Get all the mastrUnitList where lageEinheitBezeichnung equals to UPDATED_LAGE_EINHEIT_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("lageEinheitBezeichnung.equals=" + UPDATED_LAGE_EINHEIT_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLageEinheitBezeichnungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lageEinheitBezeichnung in DEFAULT_LAGE_EINHEIT_BEZEICHNUNG or UPDATED_LAGE_EINHEIT_BEZEICHNUNG
        defaultMastrUnitShouldBeFound(
            "lageEinheitBezeichnung.in=" + DEFAULT_LAGE_EINHEIT_BEZEICHNUNG + "," + UPDATED_LAGE_EINHEIT_BEZEICHNUNG
        );

        // Get all the mastrUnitList where lageEinheitBezeichnung equals to UPDATED_LAGE_EINHEIT_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("lageEinheitBezeichnung.in=" + UPDATED_LAGE_EINHEIT_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLageEinheitBezeichnungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lageEinheitBezeichnung is not null
        defaultMastrUnitShouldBeFound("lageEinheitBezeichnung.specified=true");

        // Get all the mastrUnitList where lageEinheitBezeichnung is null
        defaultMastrUnitShouldNotBeFound("lageEinheitBezeichnung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLageEinheitBezeichnungContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lageEinheitBezeichnung contains DEFAULT_LAGE_EINHEIT_BEZEICHNUNG
        defaultMastrUnitShouldBeFound("lageEinheitBezeichnung.contains=" + DEFAULT_LAGE_EINHEIT_BEZEICHNUNG);

        // Get all the mastrUnitList where lageEinheitBezeichnung contains UPDATED_LAGE_EINHEIT_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("lageEinheitBezeichnung.contains=" + UPDATED_LAGE_EINHEIT_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLageEinheitBezeichnungNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where lageEinheitBezeichnung does not contain DEFAULT_LAGE_EINHEIT_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("lageEinheitBezeichnung.doesNotContain=" + DEFAULT_LAGE_EINHEIT_BEZEICHNUNG);

        // Get all the mastrUnitList where lageEinheitBezeichnung does not contain UPDATED_LAGE_EINHEIT_BEZEICHNUNG
        defaultMastrUnitShouldBeFound("lageEinheitBezeichnung.doesNotContain=" + UPDATED_LAGE_EINHEIT_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLeistungsBegrenzungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where leistungsBegrenzung equals to DEFAULT_LEISTUNGS_BEGRENZUNG
        defaultMastrUnitShouldBeFound("leistungsBegrenzung.equals=" + DEFAULT_LEISTUNGS_BEGRENZUNG);

        // Get all the mastrUnitList where leistungsBegrenzung equals to UPDATED_LEISTUNGS_BEGRENZUNG
        defaultMastrUnitShouldNotBeFound("leistungsBegrenzung.equals=" + UPDATED_LEISTUNGS_BEGRENZUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLeistungsBegrenzungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where leistungsBegrenzung in DEFAULT_LEISTUNGS_BEGRENZUNG or UPDATED_LEISTUNGS_BEGRENZUNG
        defaultMastrUnitShouldBeFound("leistungsBegrenzung.in=" + DEFAULT_LEISTUNGS_BEGRENZUNG + "," + UPDATED_LEISTUNGS_BEGRENZUNG);

        // Get all the mastrUnitList where leistungsBegrenzung equals to UPDATED_LEISTUNGS_BEGRENZUNG
        defaultMastrUnitShouldNotBeFound("leistungsBegrenzung.in=" + UPDATED_LEISTUNGS_BEGRENZUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLeistungsBegrenzungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where leistungsBegrenzung is not null
        defaultMastrUnitShouldBeFound("leistungsBegrenzung.specified=true");

        // Get all the mastrUnitList where leistungsBegrenzung is null
        defaultMastrUnitShouldNotBeFound("leistungsBegrenzung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLeistungsBegrenzungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where leistungsBegrenzung is greater than or equal to DEFAULT_LEISTUNGS_BEGRENZUNG
        defaultMastrUnitShouldBeFound("leistungsBegrenzung.greaterThanOrEqual=" + DEFAULT_LEISTUNGS_BEGRENZUNG);

        // Get all the mastrUnitList where leistungsBegrenzung is greater than or equal to UPDATED_LEISTUNGS_BEGRENZUNG
        defaultMastrUnitShouldNotBeFound("leistungsBegrenzung.greaterThanOrEqual=" + UPDATED_LEISTUNGS_BEGRENZUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLeistungsBegrenzungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where leistungsBegrenzung is less than or equal to DEFAULT_LEISTUNGS_BEGRENZUNG
        defaultMastrUnitShouldBeFound("leistungsBegrenzung.lessThanOrEqual=" + DEFAULT_LEISTUNGS_BEGRENZUNG);

        // Get all the mastrUnitList where leistungsBegrenzung is less than or equal to SMALLER_LEISTUNGS_BEGRENZUNG
        defaultMastrUnitShouldNotBeFound("leistungsBegrenzung.lessThanOrEqual=" + SMALLER_LEISTUNGS_BEGRENZUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLeistungsBegrenzungIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where leistungsBegrenzung is less than DEFAULT_LEISTUNGS_BEGRENZUNG
        defaultMastrUnitShouldNotBeFound("leistungsBegrenzung.lessThan=" + DEFAULT_LEISTUNGS_BEGRENZUNG);

        // Get all the mastrUnitList where leistungsBegrenzung is less than UPDATED_LEISTUNGS_BEGRENZUNG
        defaultMastrUnitShouldBeFound("leistungsBegrenzung.lessThan=" + UPDATED_LEISTUNGS_BEGRENZUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByLeistungsBegrenzungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where leistungsBegrenzung is greater than DEFAULT_LEISTUNGS_BEGRENZUNG
        defaultMastrUnitShouldNotBeFound("leistungsBegrenzung.greaterThan=" + DEFAULT_LEISTUNGS_BEGRENZUNG);

        // Get all the mastrUnitList where leistungsBegrenzung is greater than SMALLER_LEISTUNGS_BEGRENZUNG
        defaultMastrUnitShouldBeFound("leistungsBegrenzung.greaterThan=" + SMALLER_LEISTUNGS_BEGRENZUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNabenhoeheWindenergieanlageIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage equals to DEFAULT_NABENHOEHE_WINDENERGIEANLAGE
        defaultMastrUnitShouldBeFound("nabenhoeheWindenergieanlage.equals=" + DEFAULT_NABENHOEHE_WINDENERGIEANLAGE);

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage equals to UPDATED_NABENHOEHE_WINDENERGIEANLAGE
        defaultMastrUnitShouldNotBeFound("nabenhoeheWindenergieanlage.equals=" + UPDATED_NABENHOEHE_WINDENERGIEANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNabenhoeheWindenergieanlageIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage in DEFAULT_NABENHOEHE_WINDENERGIEANLAGE or UPDATED_NABENHOEHE_WINDENERGIEANLAGE
        defaultMastrUnitShouldBeFound(
            "nabenhoeheWindenergieanlage.in=" + DEFAULT_NABENHOEHE_WINDENERGIEANLAGE + "," + UPDATED_NABENHOEHE_WINDENERGIEANLAGE
        );

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage equals to UPDATED_NABENHOEHE_WINDENERGIEANLAGE
        defaultMastrUnitShouldNotBeFound("nabenhoeheWindenergieanlage.in=" + UPDATED_NABENHOEHE_WINDENERGIEANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNabenhoeheWindenergieanlageIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage is not null
        defaultMastrUnitShouldBeFound("nabenhoeheWindenergieanlage.specified=true");

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage is null
        defaultMastrUnitShouldNotBeFound("nabenhoeheWindenergieanlage.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNabenhoeheWindenergieanlageIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage is greater than or equal to DEFAULT_NABENHOEHE_WINDENERGIEANLAGE
        defaultMastrUnitShouldBeFound("nabenhoeheWindenergieanlage.greaterThanOrEqual=" + DEFAULT_NABENHOEHE_WINDENERGIEANLAGE);

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage is greater than or equal to UPDATED_NABENHOEHE_WINDENERGIEANLAGE
        defaultMastrUnitShouldNotBeFound("nabenhoeheWindenergieanlage.greaterThanOrEqual=" + UPDATED_NABENHOEHE_WINDENERGIEANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNabenhoeheWindenergieanlageIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage is less than or equal to DEFAULT_NABENHOEHE_WINDENERGIEANLAGE
        defaultMastrUnitShouldBeFound("nabenhoeheWindenergieanlage.lessThanOrEqual=" + DEFAULT_NABENHOEHE_WINDENERGIEANLAGE);

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage is less than or equal to SMALLER_NABENHOEHE_WINDENERGIEANLAGE
        defaultMastrUnitShouldNotBeFound("nabenhoeheWindenergieanlage.lessThanOrEqual=" + SMALLER_NABENHOEHE_WINDENERGIEANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNabenhoeheWindenergieanlageIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage is less than DEFAULT_NABENHOEHE_WINDENERGIEANLAGE
        defaultMastrUnitShouldNotBeFound("nabenhoeheWindenergieanlage.lessThan=" + DEFAULT_NABENHOEHE_WINDENERGIEANLAGE);

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage is less than UPDATED_NABENHOEHE_WINDENERGIEANLAGE
        defaultMastrUnitShouldBeFound("nabenhoeheWindenergieanlage.lessThan=" + UPDATED_NABENHOEHE_WINDENERGIEANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNabenhoeheWindenergieanlageIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage is greater than DEFAULT_NABENHOEHE_WINDENERGIEANLAGE
        defaultMastrUnitShouldNotBeFound("nabenhoeheWindenergieanlage.greaterThan=" + DEFAULT_NABENHOEHE_WINDENERGIEANLAGE);

        // Get all the mastrUnitList where nabenhoeheWindenergieanlage is greater than SMALLER_NABENHOEHE_WINDENERGIEANLAGE
        defaultMastrUnitShouldBeFound("nabenhoeheWindenergieanlage.greaterThan=" + SMALLER_NABENHOEHE_WINDENERGIEANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByRotorDurchmesserWindenergieanlageIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage equals to DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        defaultMastrUnitShouldBeFound("rotorDurchmesserWindenergieanlage.equals=" + DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE);

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage equals to UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        defaultMastrUnitShouldNotBeFound("rotorDurchmesserWindenergieanlage.equals=" + UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByRotorDurchmesserWindenergieanlageIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage in DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE or UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        defaultMastrUnitShouldBeFound(
            "rotorDurchmesserWindenergieanlage.in=" +
            DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE +
            "," +
            UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        );

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage equals to UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        defaultMastrUnitShouldNotBeFound("rotorDurchmesserWindenergieanlage.in=" + UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByRotorDurchmesserWindenergieanlageIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage is not null
        defaultMastrUnitShouldBeFound("rotorDurchmesserWindenergieanlage.specified=true");

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage is null
        defaultMastrUnitShouldNotBeFound("rotorDurchmesserWindenergieanlage.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByRotorDurchmesserWindenergieanlageIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage is greater than or equal to DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        defaultMastrUnitShouldBeFound(
            "rotorDurchmesserWindenergieanlage.greaterThanOrEqual=" + DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        );

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage is greater than or equal to UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        defaultMastrUnitShouldNotBeFound(
            "rotorDurchmesserWindenergieanlage.greaterThanOrEqual=" + UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        );
    }

    @Test
    @Transactional
    void getAllMastrUnitsByRotorDurchmesserWindenergieanlageIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage is less than or equal to DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        defaultMastrUnitShouldBeFound("rotorDurchmesserWindenergieanlage.lessThanOrEqual=" + DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE);

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage is less than or equal to SMALLER_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        defaultMastrUnitShouldNotBeFound(
            "rotorDurchmesserWindenergieanlage.lessThanOrEqual=" + SMALLER_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        );
    }

    @Test
    @Transactional
    void getAllMastrUnitsByRotorDurchmesserWindenergieanlageIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage is less than DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        defaultMastrUnitShouldNotBeFound("rotorDurchmesserWindenergieanlage.lessThan=" + DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE);

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage is less than UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        defaultMastrUnitShouldBeFound("rotorDurchmesserWindenergieanlage.lessThan=" + UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByRotorDurchmesserWindenergieanlageIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage is greater than DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        defaultMastrUnitShouldNotBeFound("rotorDurchmesserWindenergieanlage.greaterThan=" + DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE);

        // Get all the mastrUnitList where rotorDurchmesserWindenergieanlage is greater than SMALLER_ROTOR_DURCHMESSER_WINDENERGIEANLAGE
        defaultMastrUnitShouldBeFound("rotorDurchmesserWindenergieanlage.greaterThan=" + SMALLER_ROTOR_DURCHMESSER_WINDENERGIEANLAGE);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNettoNennLeistungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nettoNennLeistung equals to DEFAULT_NETTO_NENN_LEISTUNG
        defaultMastrUnitShouldBeFound("nettoNennLeistung.equals=" + DEFAULT_NETTO_NENN_LEISTUNG);

        // Get all the mastrUnitList where nettoNennLeistung equals to UPDATED_NETTO_NENN_LEISTUNG
        defaultMastrUnitShouldNotBeFound("nettoNennLeistung.equals=" + UPDATED_NETTO_NENN_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNettoNennLeistungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nettoNennLeistung in DEFAULT_NETTO_NENN_LEISTUNG or UPDATED_NETTO_NENN_LEISTUNG
        defaultMastrUnitShouldBeFound("nettoNennLeistung.in=" + DEFAULT_NETTO_NENN_LEISTUNG + "," + UPDATED_NETTO_NENN_LEISTUNG);

        // Get all the mastrUnitList where nettoNennLeistung equals to UPDATED_NETTO_NENN_LEISTUNG
        defaultMastrUnitShouldNotBeFound("nettoNennLeistung.in=" + UPDATED_NETTO_NENN_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNettoNennLeistungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nettoNennLeistung is not null
        defaultMastrUnitShouldBeFound("nettoNennLeistung.specified=true");

        // Get all the mastrUnitList where nettoNennLeistung is null
        defaultMastrUnitShouldNotBeFound("nettoNennLeistung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNettoNennLeistungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nettoNennLeistung is greater than or equal to DEFAULT_NETTO_NENN_LEISTUNG
        defaultMastrUnitShouldBeFound("nettoNennLeistung.greaterThanOrEqual=" + DEFAULT_NETTO_NENN_LEISTUNG);

        // Get all the mastrUnitList where nettoNennLeistung is greater than or equal to UPDATED_NETTO_NENN_LEISTUNG
        defaultMastrUnitShouldNotBeFound("nettoNennLeistung.greaterThanOrEqual=" + UPDATED_NETTO_NENN_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNettoNennLeistungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nettoNennLeistung is less than or equal to DEFAULT_NETTO_NENN_LEISTUNG
        defaultMastrUnitShouldBeFound("nettoNennLeistung.lessThanOrEqual=" + DEFAULT_NETTO_NENN_LEISTUNG);

        // Get all the mastrUnitList where nettoNennLeistung is less than or equal to SMALLER_NETTO_NENN_LEISTUNG
        defaultMastrUnitShouldNotBeFound("nettoNennLeistung.lessThanOrEqual=" + SMALLER_NETTO_NENN_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNettoNennLeistungIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nettoNennLeistung is less than DEFAULT_NETTO_NENN_LEISTUNG
        defaultMastrUnitShouldNotBeFound("nettoNennLeistung.lessThan=" + DEFAULT_NETTO_NENN_LEISTUNG);

        // Get all the mastrUnitList where nettoNennLeistung is less than UPDATED_NETTO_NENN_LEISTUNG
        defaultMastrUnitShouldBeFound("nettoNennLeistung.lessThan=" + UPDATED_NETTO_NENN_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNettoNennLeistungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nettoNennLeistung is greater than DEFAULT_NETTO_NENN_LEISTUNG
        defaultMastrUnitShouldNotBeFound("nettoNennLeistung.greaterThan=" + DEFAULT_NETTO_NENN_LEISTUNG);

        // Get all the mastrUnitList where nettoNennLeistung is greater than SMALLER_NETTO_NENN_LEISTUNG
        defaultMastrUnitShouldBeFound("nettoNennLeistung.greaterThan=" + SMALLER_NETTO_NENN_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzbareSpeicherKapazitaetIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet equals to DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET
        defaultMastrUnitShouldBeFound("nutzbareSpeicherKapazitaet.equals=" + DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET);

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet equals to UPDATED_NUTZBARE_SPEICHER_KAPAZITAET
        defaultMastrUnitShouldNotBeFound("nutzbareSpeicherKapazitaet.equals=" + UPDATED_NUTZBARE_SPEICHER_KAPAZITAET);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzbareSpeicherKapazitaetIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet in DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET or UPDATED_NUTZBARE_SPEICHER_KAPAZITAET
        defaultMastrUnitShouldBeFound(
            "nutzbareSpeicherKapazitaet.in=" + DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET + "," + UPDATED_NUTZBARE_SPEICHER_KAPAZITAET
        );

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet equals to UPDATED_NUTZBARE_SPEICHER_KAPAZITAET
        defaultMastrUnitShouldNotBeFound("nutzbareSpeicherKapazitaet.in=" + UPDATED_NUTZBARE_SPEICHER_KAPAZITAET);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzbareSpeicherKapazitaetIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet is not null
        defaultMastrUnitShouldBeFound("nutzbareSpeicherKapazitaet.specified=true");

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet is null
        defaultMastrUnitShouldNotBeFound("nutzbareSpeicherKapazitaet.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzbareSpeicherKapazitaetIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet is greater than or equal to DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET
        defaultMastrUnitShouldBeFound("nutzbareSpeicherKapazitaet.greaterThanOrEqual=" + DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET);

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet is greater than or equal to UPDATED_NUTZBARE_SPEICHER_KAPAZITAET
        defaultMastrUnitShouldNotBeFound("nutzbareSpeicherKapazitaet.greaterThanOrEqual=" + UPDATED_NUTZBARE_SPEICHER_KAPAZITAET);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzbareSpeicherKapazitaetIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet is less than or equal to DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET
        defaultMastrUnitShouldBeFound("nutzbareSpeicherKapazitaet.lessThanOrEqual=" + DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET);

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet is less than or equal to SMALLER_NUTZBARE_SPEICHER_KAPAZITAET
        defaultMastrUnitShouldNotBeFound("nutzbareSpeicherKapazitaet.lessThanOrEqual=" + SMALLER_NUTZBARE_SPEICHER_KAPAZITAET);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzbareSpeicherKapazitaetIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet is less than DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET
        defaultMastrUnitShouldNotBeFound("nutzbareSpeicherKapazitaet.lessThan=" + DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET);

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet is less than UPDATED_NUTZBARE_SPEICHER_KAPAZITAET
        defaultMastrUnitShouldBeFound("nutzbareSpeicherKapazitaet.lessThan=" + UPDATED_NUTZBARE_SPEICHER_KAPAZITAET);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzbareSpeicherKapazitaetIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet is greater than DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET
        defaultMastrUnitShouldNotBeFound("nutzbareSpeicherKapazitaet.greaterThan=" + DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET);

        // Get all the mastrUnitList where nutzbareSpeicherKapazitaet is greater than SMALLER_NUTZBARE_SPEICHER_KAPAZITAET
        defaultMastrUnitShouldBeFound("nutzbareSpeicherKapazitaet.greaterThan=" + SMALLER_NUTZBARE_SPEICHER_KAPAZITAET);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzungsBereichGebsaIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzungsBereichGebsa equals to DEFAULT_NUTZUNGS_BEREICH_GEBSA
        defaultMastrUnitShouldBeFound("nutzungsBereichGebsa.equals=" + DEFAULT_NUTZUNGS_BEREICH_GEBSA);

        // Get all the mastrUnitList where nutzungsBereichGebsa equals to UPDATED_NUTZUNGS_BEREICH_GEBSA
        defaultMastrUnitShouldNotBeFound("nutzungsBereichGebsa.equals=" + UPDATED_NUTZUNGS_BEREICH_GEBSA);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzungsBereichGebsaIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzungsBereichGebsa in DEFAULT_NUTZUNGS_BEREICH_GEBSA or UPDATED_NUTZUNGS_BEREICH_GEBSA
        defaultMastrUnitShouldBeFound("nutzungsBereichGebsa.in=" + DEFAULT_NUTZUNGS_BEREICH_GEBSA + "," + UPDATED_NUTZUNGS_BEREICH_GEBSA);

        // Get all the mastrUnitList where nutzungsBereichGebsa equals to UPDATED_NUTZUNGS_BEREICH_GEBSA
        defaultMastrUnitShouldNotBeFound("nutzungsBereichGebsa.in=" + UPDATED_NUTZUNGS_BEREICH_GEBSA);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzungsBereichGebsaIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzungsBereichGebsa is not null
        defaultMastrUnitShouldBeFound("nutzungsBereichGebsa.specified=true");

        // Get all the mastrUnitList where nutzungsBereichGebsa is null
        defaultMastrUnitShouldNotBeFound("nutzungsBereichGebsa.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzungsBereichGebsaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzungsBereichGebsa is greater than or equal to DEFAULT_NUTZUNGS_BEREICH_GEBSA
        defaultMastrUnitShouldBeFound("nutzungsBereichGebsa.greaterThanOrEqual=" + DEFAULT_NUTZUNGS_BEREICH_GEBSA);

        // Get all the mastrUnitList where nutzungsBereichGebsa is greater than or equal to UPDATED_NUTZUNGS_BEREICH_GEBSA
        defaultMastrUnitShouldNotBeFound("nutzungsBereichGebsa.greaterThanOrEqual=" + UPDATED_NUTZUNGS_BEREICH_GEBSA);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzungsBereichGebsaIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzungsBereichGebsa is less than or equal to DEFAULT_NUTZUNGS_BEREICH_GEBSA
        defaultMastrUnitShouldBeFound("nutzungsBereichGebsa.lessThanOrEqual=" + DEFAULT_NUTZUNGS_BEREICH_GEBSA);

        // Get all the mastrUnitList where nutzungsBereichGebsa is less than or equal to SMALLER_NUTZUNGS_BEREICH_GEBSA
        defaultMastrUnitShouldNotBeFound("nutzungsBereichGebsa.lessThanOrEqual=" + SMALLER_NUTZUNGS_BEREICH_GEBSA);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzungsBereichGebsaIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzungsBereichGebsa is less than DEFAULT_NUTZUNGS_BEREICH_GEBSA
        defaultMastrUnitShouldNotBeFound("nutzungsBereichGebsa.lessThan=" + DEFAULT_NUTZUNGS_BEREICH_GEBSA);

        // Get all the mastrUnitList where nutzungsBereichGebsa is less than UPDATED_NUTZUNGS_BEREICH_GEBSA
        defaultMastrUnitShouldBeFound("nutzungsBereichGebsa.lessThan=" + UPDATED_NUTZUNGS_BEREICH_GEBSA);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByNutzungsBereichGebsaIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where nutzungsBereichGebsa is greater than DEFAULT_NUTZUNGS_BEREICH_GEBSA
        defaultMastrUnitShouldNotBeFound("nutzungsBereichGebsa.greaterThan=" + DEFAULT_NUTZUNGS_BEREICH_GEBSA);

        // Get all the mastrUnitList where nutzungsBereichGebsa is greater than SMALLER_NUTZUNGS_BEREICH_GEBSA
        defaultMastrUnitShouldBeFound("nutzungsBereichGebsa.greaterThan=" + SMALLER_NUTZUNGS_BEREICH_GEBSA);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByStandortAnonymisiertIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where standortAnonymisiert equals to DEFAULT_STANDORT_ANONYMISIERT
        defaultMastrUnitShouldBeFound("standortAnonymisiert.equals=" + DEFAULT_STANDORT_ANONYMISIERT);

        // Get all the mastrUnitList where standortAnonymisiert equals to UPDATED_STANDORT_ANONYMISIERT
        defaultMastrUnitShouldNotBeFound("standortAnonymisiert.equals=" + UPDATED_STANDORT_ANONYMISIERT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByStandortAnonymisiertIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where standortAnonymisiert in DEFAULT_STANDORT_ANONYMISIERT or UPDATED_STANDORT_ANONYMISIERT
        defaultMastrUnitShouldBeFound("standortAnonymisiert.in=" + DEFAULT_STANDORT_ANONYMISIERT + "," + UPDATED_STANDORT_ANONYMISIERT);

        // Get all the mastrUnitList where standortAnonymisiert equals to UPDATED_STANDORT_ANONYMISIERT
        defaultMastrUnitShouldNotBeFound("standortAnonymisiert.in=" + UPDATED_STANDORT_ANONYMISIERT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByStandortAnonymisiertIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where standortAnonymisiert is not null
        defaultMastrUnitShouldBeFound("standortAnonymisiert.specified=true");

        // Get all the mastrUnitList where standortAnonymisiert is null
        defaultMastrUnitShouldNotBeFound("standortAnonymisiert.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByStandortAnonymisiertContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where standortAnonymisiert contains DEFAULT_STANDORT_ANONYMISIERT
        defaultMastrUnitShouldBeFound("standortAnonymisiert.contains=" + DEFAULT_STANDORT_ANONYMISIERT);

        // Get all the mastrUnitList where standortAnonymisiert contains UPDATED_STANDORT_ANONYMISIERT
        defaultMastrUnitShouldNotBeFound("standortAnonymisiert.contains=" + UPDATED_STANDORT_ANONYMISIERT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByStandortAnonymisiertNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where standortAnonymisiert does not contain DEFAULT_STANDORT_ANONYMISIERT
        defaultMastrUnitShouldNotBeFound("standortAnonymisiert.doesNotContain=" + DEFAULT_STANDORT_ANONYMISIERT);

        // Get all the mastrUnitList where standortAnonymisiert does not contain UPDATED_STANDORT_ANONYMISIERT
        defaultMastrUnitShouldBeFound("standortAnonymisiert.doesNotContain=" + UPDATED_STANDORT_ANONYMISIERT);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpannungsEbenenIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where spannungsEbenenId equals to DEFAULT_SPANNUNGS_EBENEN_ID
        defaultMastrUnitShouldBeFound("spannungsEbenenId.equals=" + DEFAULT_SPANNUNGS_EBENEN_ID);

        // Get all the mastrUnitList where spannungsEbenenId equals to UPDATED_SPANNUNGS_EBENEN_ID
        defaultMastrUnitShouldNotBeFound("spannungsEbenenId.equals=" + UPDATED_SPANNUNGS_EBENEN_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpannungsEbenenIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where spannungsEbenenId in DEFAULT_SPANNUNGS_EBENEN_ID or UPDATED_SPANNUNGS_EBENEN_ID
        defaultMastrUnitShouldBeFound("spannungsEbenenId.in=" + DEFAULT_SPANNUNGS_EBENEN_ID + "," + UPDATED_SPANNUNGS_EBENEN_ID);

        // Get all the mastrUnitList where spannungsEbenenId equals to UPDATED_SPANNUNGS_EBENEN_ID
        defaultMastrUnitShouldNotBeFound("spannungsEbenenId.in=" + UPDATED_SPANNUNGS_EBENEN_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpannungsEbenenIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where spannungsEbenenId is not null
        defaultMastrUnitShouldBeFound("spannungsEbenenId.specified=true");

        // Get all the mastrUnitList where spannungsEbenenId is null
        defaultMastrUnitShouldNotBeFound("spannungsEbenenId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpannungsEbenenIdContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where spannungsEbenenId contains DEFAULT_SPANNUNGS_EBENEN_ID
        defaultMastrUnitShouldBeFound("spannungsEbenenId.contains=" + DEFAULT_SPANNUNGS_EBENEN_ID);

        // Get all the mastrUnitList where spannungsEbenenId contains UPDATED_SPANNUNGS_EBENEN_ID
        defaultMastrUnitShouldNotBeFound("spannungsEbenenId.contains=" + UPDATED_SPANNUNGS_EBENEN_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpannungsEbenenIdNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where spannungsEbenenId does not contain DEFAULT_SPANNUNGS_EBENEN_ID
        defaultMastrUnitShouldNotBeFound("spannungsEbenenId.doesNotContain=" + DEFAULT_SPANNUNGS_EBENEN_ID);

        // Get all the mastrUnitList where spannungsEbenenId does not contain UPDATED_SPANNUNGS_EBENEN_ID
        defaultMastrUnitShouldBeFound("spannungsEbenenId.doesNotContain=" + UPDATED_SPANNUNGS_EBENEN_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpannungsEbenenNamenIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where spannungsEbenenNamen equals to DEFAULT_SPANNUNGS_EBENEN_NAMEN
        defaultMastrUnitShouldBeFound("spannungsEbenenNamen.equals=" + DEFAULT_SPANNUNGS_EBENEN_NAMEN);

        // Get all the mastrUnitList where spannungsEbenenNamen equals to UPDATED_SPANNUNGS_EBENEN_NAMEN
        defaultMastrUnitShouldNotBeFound("spannungsEbenenNamen.equals=" + UPDATED_SPANNUNGS_EBENEN_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpannungsEbenenNamenIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where spannungsEbenenNamen in DEFAULT_SPANNUNGS_EBENEN_NAMEN or UPDATED_SPANNUNGS_EBENEN_NAMEN
        defaultMastrUnitShouldBeFound("spannungsEbenenNamen.in=" + DEFAULT_SPANNUNGS_EBENEN_NAMEN + "," + UPDATED_SPANNUNGS_EBENEN_NAMEN);

        // Get all the mastrUnitList where spannungsEbenenNamen equals to UPDATED_SPANNUNGS_EBENEN_NAMEN
        defaultMastrUnitShouldNotBeFound("spannungsEbenenNamen.in=" + UPDATED_SPANNUNGS_EBENEN_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpannungsEbenenNamenIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where spannungsEbenenNamen is not null
        defaultMastrUnitShouldBeFound("spannungsEbenenNamen.specified=true");

        // Get all the mastrUnitList where spannungsEbenenNamen is null
        defaultMastrUnitShouldNotBeFound("spannungsEbenenNamen.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpannungsEbenenNamenContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where spannungsEbenenNamen contains DEFAULT_SPANNUNGS_EBENEN_NAMEN
        defaultMastrUnitShouldBeFound("spannungsEbenenNamen.contains=" + DEFAULT_SPANNUNGS_EBENEN_NAMEN);

        // Get all the mastrUnitList where spannungsEbenenNamen contains UPDATED_SPANNUNGS_EBENEN_NAMEN
        defaultMastrUnitShouldNotBeFound("spannungsEbenenNamen.contains=" + UPDATED_SPANNUNGS_EBENEN_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpannungsEbenenNamenNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where spannungsEbenenNamen does not contain DEFAULT_SPANNUNGS_EBENEN_NAMEN
        defaultMastrUnitShouldNotBeFound("spannungsEbenenNamen.doesNotContain=" + DEFAULT_SPANNUNGS_EBENEN_NAMEN);

        // Get all the mastrUnitList where spannungsEbenenNamen does not contain UPDATED_SPANNUNGS_EBENEN_NAMEN
        defaultMastrUnitShouldBeFound("spannungsEbenenNamen.doesNotContain=" + UPDATED_SPANNUNGS_EBENEN_NAMEN);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpeicherEinheitMastrNummerIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where speicherEinheitMastrNummer equals to DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("speicherEinheitMastrNummer.equals=" + DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER);

        // Get all the mastrUnitList where speicherEinheitMastrNummer equals to UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("speicherEinheitMastrNummer.equals=" + UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpeicherEinheitMastrNummerIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where speicherEinheitMastrNummer in DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER or UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER
        defaultMastrUnitShouldBeFound(
            "speicherEinheitMastrNummer.in=" + DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER + "," + UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER
        );

        // Get all the mastrUnitList where speicherEinheitMastrNummer equals to UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("speicherEinheitMastrNummer.in=" + UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpeicherEinheitMastrNummerIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where speicherEinheitMastrNummer is not null
        defaultMastrUnitShouldBeFound("speicherEinheitMastrNummer.specified=true");

        // Get all the mastrUnitList where speicherEinheitMastrNummer is null
        defaultMastrUnitShouldNotBeFound("speicherEinheitMastrNummer.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpeicherEinheitMastrNummerContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where speicherEinheitMastrNummer contains DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("speicherEinheitMastrNummer.contains=" + DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER);

        // Get all the mastrUnitList where speicherEinheitMastrNummer contains UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("speicherEinheitMastrNummer.contains=" + UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsBySpeicherEinheitMastrNummerNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where speicherEinheitMastrNummer does not contain DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER
        defaultMastrUnitShouldNotBeFound("speicherEinheitMastrNummer.doesNotContain=" + DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER);

        // Get all the mastrUnitList where speicherEinheitMastrNummer does not contain UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER
        defaultMastrUnitShouldBeFound("speicherEinheitMastrNummer.doesNotContain=" + UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTechnologieStromerzeugungIdIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where technologieStromerzeugungId equals to DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID
        defaultMastrUnitShouldBeFound("technologieStromerzeugungId.equals=" + DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID);

        // Get all the mastrUnitList where technologieStromerzeugungId equals to UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID
        defaultMastrUnitShouldNotBeFound("technologieStromerzeugungId.equals=" + UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTechnologieStromerzeugungIdIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where technologieStromerzeugungId in DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID or UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID
        defaultMastrUnitShouldBeFound(
            "technologieStromerzeugungId.in=" + DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID + "," + UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID
        );

        // Get all the mastrUnitList where technologieStromerzeugungId equals to UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID
        defaultMastrUnitShouldNotBeFound("technologieStromerzeugungId.in=" + UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTechnologieStromerzeugungIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where technologieStromerzeugungId is not null
        defaultMastrUnitShouldBeFound("technologieStromerzeugungId.specified=true");

        // Get all the mastrUnitList where technologieStromerzeugungId is null
        defaultMastrUnitShouldNotBeFound("technologieStromerzeugungId.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTechnologieStromerzeugungIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where technologieStromerzeugungId is greater than or equal to DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID
        defaultMastrUnitShouldBeFound("technologieStromerzeugungId.greaterThanOrEqual=" + DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID);

        // Get all the mastrUnitList where technologieStromerzeugungId is greater than or equal to UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID
        defaultMastrUnitShouldNotBeFound("technologieStromerzeugungId.greaterThanOrEqual=" + UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTechnologieStromerzeugungIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where technologieStromerzeugungId is less than or equal to DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID
        defaultMastrUnitShouldBeFound("technologieStromerzeugungId.lessThanOrEqual=" + DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID);

        // Get all the mastrUnitList where technologieStromerzeugungId is less than or equal to SMALLER_TECHNOLOGIE_STROMERZEUGUNG_ID
        defaultMastrUnitShouldNotBeFound("technologieStromerzeugungId.lessThanOrEqual=" + SMALLER_TECHNOLOGIE_STROMERZEUGUNG_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTechnologieStromerzeugungIdIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where technologieStromerzeugungId is less than DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID
        defaultMastrUnitShouldNotBeFound("technologieStromerzeugungId.lessThan=" + DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID);

        // Get all the mastrUnitList where technologieStromerzeugungId is less than UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID
        defaultMastrUnitShouldBeFound("technologieStromerzeugungId.lessThan=" + UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTechnologieStromerzeugungIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where technologieStromerzeugungId is greater than DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID
        defaultMastrUnitShouldNotBeFound("technologieStromerzeugungId.greaterThan=" + DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID);

        // Get all the mastrUnitList where technologieStromerzeugungId is greater than SMALLER_TECHNOLOGIE_STROMERZEUGUNG_ID
        defaultMastrUnitShouldBeFound("technologieStromerzeugungId.greaterThan=" + SMALLER_TECHNOLOGIE_STROMERZEUGUNG_ID);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTechnologieStromerzeugungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where technologieStromerzeugung equals to DEFAULT_TECHNOLOGIE_STROMERZEUGUNG
        defaultMastrUnitShouldBeFound("technologieStromerzeugung.equals=" + DEFAULT_TECHNOLOGIE_STROMERZEUGUNG);

        // Get all the mastrUnitList where technologieStromerzeugung equals to UPDATED_TECHNOLOGIE_STROMERZEUGUNG
        defaultMastrUnitShouldNotBeFound("technologieStromerzeugung.equals=" + UPDATED_TECHNOLOGIE_STROMERZEUGUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTechnologieStromerzeugungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where technologieStromerzeugung in DEFAULT_TECHNOLOGIE_STROMERZEUGUNG or UPDATED_TECHNOLOGIE_STROMERZEUGUNG
        defaultMastrUnitShouldBeFound(
            "technologieStromerzeugung.in=" + DEFAULT_TECHNOLOGIE_STROMERZEUGUNG + "," + UPDATED_TECHNOLOGIE_STROMERZEUGUNG
        );

        // Get all the mastrUnitList where technologieStromerzeugung equals to UPDATED_TECHNOLOGIE_STROMERZEUGUNG
        defaultMastrUnitShouldNotBeFound("technologieStromerzeugung.in=" + UPDATED_TECHNOLOGIE_STROMERZEUGUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTechnologieStromerzeugungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where technologieStromerzeugung is not null
        defaultMastrUnitShouldBeFound("technologieStromerzeugung.specified=true");

        // Get all the mastrUnitList where technologieStromerzeugung is null
        defaultMastrUnitShouldNotBeFound("technologieStromerzeugung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTechnologieStromerzeugungContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where technologieStromerzeugung contains DEFAULT_TECHNOLOGIE_STROMERZEUGUNG
        defaultMastrUnitShouldBeFound("technologieStromerzeugung.contains=" + DEFAULT_TECHNOLOGIE_STROMERZEUGUNG);

        // Get all the mastrUnitList where technologieStromerzeugung contains UPDATED_TECHNOLOGIE_STROMERZEUGUNG
        defaultMastrUnitShouldNotBeFound("technologieStromerzeugung.contains=" + UPDATED_TECHNOLOGIE_STROMERZEUGUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTechnologieStromerzeugungNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where technologieStromerzeugung does not contain DEFAULT_TECHNOLOGIE_STROMERZEUGUNG
        defaultMastrUnitShouldNotBeFound("technologieStromerzeugung.doesNotContain=" + DEFAULT_TECHNOLOGIE_STROMERZEUGUNG);

        // Get all the mastrUnitList where technologieStromerzeugung does not contain UPDATED_TECHNOLOGIE_STROMERZEUGUNG
        defaultMastrUnitShouldBeFound("technologieStromerzeugung.doesNotContain=" + UPDATED_TECHNOLOGIE_STROMERZEUGUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByThermischeNutzLeistungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where thermischeNutzLeistung equals to DEFAULT_THERMISCHE_NUTZ_LEISTUNG
        defaultMastrUnitShouldBeFound("thermischeNutzLeistung.equals=" + DEFAULT_THERMISCHE_NUTZ_LEISTUNG);

        // Get all the mastrUnitList where thermischeNutzLeistung equals to UPDATED_THERMISCHE_NUTZ_LEISTUNG
        defaultMastrUnitShouldNotBeFound("thermischeNutzLeistung.equals=" + UPDATED_THERMISCHE_NUTZ_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByThermischeNutzLeistungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where thermischeNutzLeistung in DEFAULT_THERMISCHE_NUTZ_LEISTUNG or UPDATED_THERMISCHE_NUTZ_LEISTUNG
        defaultMastrUnitShouldBeFound(
            "thermischeNutzLeistung.in=" + DEFAULT_THERMISCHE_NUTZ_LEISTUNG + "," + UPDATED_THERMISCHE_NUTZ_LEISTUNG
        );

        // Get all the mastrUnitList where thermischeNutzLeistung equals to UPDATED_THERMISCHE_NUTZ_LEISTUNG
        defaultMastrUnitShouldNotBeFound("thermischeNutzLeistung.in=" + UPDATED_THERMISCHE_NUTZ_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByThermischeNutzLeistungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where thermischeNutzLeistung is not null
        defaultMastrUnitShouldBeFound("thermischeNutzLeistung.specified=true");

        // Get all the mastrUnitList where thermischeNutzLeistung is null
        defaultMastrUnitShouldNotBeFound("thermischeNutzLeistung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByThermischeNutzLeistungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where thermischeNutzLeistung is greater than or equal to DEFAULT_THERMISCHE_NUTZ_LEISTUNG
        defaultMastrUnitShouldBeFound("thermischeNutzLeistung.greaterThanOrEqual=" + DEFAULT_THERMISCHE_NUTZ_LEISTUNG);

        // Get all the mastrUnitList where thermischeNutzLeistung is greater than or equal to UPDATED_THERMISCHE_NUTZ_LEISTUNG
        defaultMastrUnitShouldNotBeFound("thermischeNutzLeistung.greaterThanOrEqual=" + UPDATED_THERMISCHE_NUTZ_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByThermischeNutzLeistungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where thermischeNutzLeistung is less than or equal to DEFAULT_THERMISCHE_NUTZ_LEISTUNG
        defaultMastrUnitShouldBeFound("thermischeNutzLeistung.lessThanOrEqual=" + DEFAULT_THERMISCHE_NUTZ_LEISTUNG);

        // Get all the mastrUnitList where thermischeNutzLeistung is less than or equal to SMALLER_THERMISCHE_NUTZ_LEISTUNG
        defaultMastrUnitShouldNotBeFound("thermischeNutzLeistung.lessThanOrEqual=" + SMALLER_THERMISCHE_NUTZ_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByThermischeNutzLeistungIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where thermischeNutzLeistung is less than DEFAULT_THERMISCHE_NUTZ_LEISTUNG
        defaultMastrUnitShouldNotBeFound("thermischeNutzLeistung.lessThan=" + DEFAULT_THERMISCHE_NUTZ_LEISTUNG);

        // Get all the mastrUnitList where thermischeNutzLeistung is less than UPDATED_THERMISCHE_NUTZ_LEISTUNG
        defaultMastrUnitShouldBeFound("thermischeNutzLeistung.lessThan=" + UPDATED_THERMISCHE_NUTZ_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByThermischeNutzLeistungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where thermischeNutzLeistung is greater than DEFAULT_THERMISCHE_NUTZ_LEISTUNG
        defaultMastrUnitShouldNotBeFound("thermischeNutzLeistung.greaterThan=" + DEFAULT_THERMISCHE_NUTZ_LEISTUNG);

        // Get all the mastrUnitList where thermischeNutzLeistung is greater than SMALLER_THERMISCHE_NUTZ_LEISTUNG
        defaultMastrUnitShouldBeFound("thermischeNutzLeistung.greaterThan=" + SMALLER_THERMISCHE_NUTZ_LEISTUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTypenBezeichnungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where typenBezeichnung equals to DEFAULT_TYPEN_BEZEICHNUNG
        defaultMastrUnitShouldBeFound("typenBezeichnung.equals=" + DEFAULT_TYPEN_BEZEICHNUNG);

        // Get all the mastrUnitList where typenBezeichnung equals to UPDATED_TYPEN_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("typenBezeichnung.equals=" + UPDATED_TYPEN_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTypenBezeichnungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where typenBezeichnung in DEFAULT_TYPEN_BEZEICHNUNG or UPDATED_TYPEN_BEZEICHNUNG
        defaultMastrUnitShouldBeFound("typenBezeichnung.in=" + DEFAULT_TYPEN_BEZEICHNUNG + "," + UPDATED_TYPEN_BEZEICHNUNG);

        // Get all the mastrUnitList where typenBezeichnung equals to UPDATED_TYPEN_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("typenBezeichnung.in=" + UPDATED_TYPEN_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTypenBezeichnungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where typenBezeichnung is not null
        defaultMastrUnitShouldBeFound("typenBezeichnung.specified=true");

        // Get all the mastrUnitList where typenBezeichnung is null
        defaultMastrUnitShouldNotBeFound("typenBezeichnung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTypenBezeichnungContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where typenBezeichnung contains DEFAULT_TYPEN_BEZEICHNUNG
        defaultMastrUnitShouldBeFound("typenBezeichnung.contains=" + DEFAULT_TYPEN_BEZEICHNUNG);

        // Get all the mastrUnitList where typenBezeichnung contains UPDATED_TYPEN_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("typenBezeichnung.contains=" + UPDATED_TYPEN_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByTypenBezeichnungNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where typenBezeichnung does not contain DEFAULT_TYPEN_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("typenBezeichnung.doesNotContain=" + DEFAULT_TYPEN_BEZEICHNUNG);

        // Get all the mastrUnitList where typenBezeichnung does not contain UPDATED_TYPEN_BEZEICHNUNG
        defaultMastrUnitShouldBeFound("typenBezeichnung.doesNotContain=" + UPDATED_TYPEN_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByVollTeilEinspeisungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where vollTeilEinspeisung equals to DEFAULT_VOLL_TEIL_EINSPEISUNG
        defaultMastrUnitShouldBeFound("vollTeilEinspeisung.equals=" + DEFAULT_VOLL_TEIL_EINSPEISUNG);

        // Get all the mastrUnitList where vollTeilEinspeisung equals to UPDATED_VOLL_TEIL_EINSPEISUNG
        defaultMastrUnitShouldNotBeFound("vollTeilEinspeisung.equals=" + UPDATED_VOLL_TEIL_EINSPEISUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByVollTeilEinspeisungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where vollTeilEinspeisung in DEFAULT_VOLL_TEIL_EINSPEISUNG or UPDATED_VOLL_TEIL_EINSPEISUNG
        defaultMastrUnitShouldBeFound("vollTeilEinspeisung.in=" + DEFAULT_VOLL_TEIL_EINSPEISUNG + "," + UPDATED_VOLL_TEIL_EINSPEISUNG);

        // Get all the mastrUnitList where vollTeilEinspeisung equals to UPDATED_VOLL_TEIL_EINSPEISUNG
        defaultMastrUnitShouldNotBeFound("vollTeilEinspeisung.in=" + UPDATED_VOLL_TEIL_EINSPEISUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByVollTeilEinspeisungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where vollTeilEinspeisung is not null
        defaultMastrUnitShouldBeFound("vollTeilEinspeisung.specified=true");

        // Get all the mastrUnitList where vollTeilEinspeisung is null
        defaultMastrUnitShouldNotBeFound("vollTeilEinspeisung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByVollTeilEinspeisungIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where vollTeilEinspeisung is greater than or equal to DEFAULT_VOLL_TEIL_EINSPEISUNG
        defaultMastrUnitShouldBeFound("vollTeilEinspeisung.greaterThanOrEqual=" + DEFAULT_VOLL_TEIL_EINSPEISUNG);

        // Get all the mastrUnitList where vollTeilEinspeisung is greater than or equal to UPDATED_VOLL_TEIL_EINSPEISUNG
        defaultMastrUnitShouldNotBeFound("vollTeilEinspeisung.greaterThanOrEqual=" + UPDATED_VOLL_TEIL_EINSPEISUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByVollTeilEinspeisungIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where vollTeilEinspeisung is less than or equal to DEFAULT_VOLL_TEIL_EINSPEISUNG
        defaultMastrUnitShouldBeFound("vollTeilEinspeisung.lessThanOrEqual=" + DEFAULT_VOLL_TEIL_EINSPEISUNG);

        // Get all the mastrUnitList where vollTeilEinspeisung is less than or equal to SMALLER_VOLL_TEIL_EINSPEISUNG
        defaultMastrUnitShouldNotBeFound("vollTeilEinspeisung.lessThanOrEqual=" + SMALLER_VOLL_TEIL_EINSPEISUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByVollTeilEinspeisungIsLessThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where vollTeilEinspeisung is less than DEFAULT_VOLL_TEIL_EINSPEISUNG
        defaultMastrUnitShouldNotBeFound("vollTeilEinspeisung.lessThan=" + DEFAULT_VOLL_TEIL_EINSPEISUNG);

        // Get all the mastrUnitList where vollTeilEinspeisung is less than UPDATED_VOLL_TEIL_EINSPEISUNG
        defaultMastrUnitShouldBeFound("vollTeilEinspeisung.lessThan=" + UPDATED_VOLL_TEIL_EINSPEISUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByVollTeilEinspeisungIsGreaterThanSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where vollTeilEinspeisung is greater than DEFAULT_VOLL_TEIL_EINSPEISUNG
        defaultMastrUnitShouldNotBeFound("vollTeilEinspeisung.greaterThan=" + DEFAULT_VOLL_TEIL_EINSPEISUNG);

        // Get all the mastrUnitList where vollTeilEinspeisung is greater than SMALLER_VOLL_TEIL_EINSPEISUNG
        defaultMastrUnitShouldBeFound("vollTeilEinspeisung.greaterThan=" + SMALLER_VOLL_TEIL_EINSPEISUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByVollTeilEinspeisungBezeichnungIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where vollTeilEinspeisungBezeichnung equals to DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG
        defaultMastrUnitShouldBeFound("vollTeilEinspeisungBezeichnung.equals=" + DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG);

        // Get all the mastrUnitList where vollTeilEinspeisungBezeichnung equals to UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("vollTeilEinspeisungBezeichnung.equals=" + UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByVollTeilEinspeisungBezeichnungIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where vollTeilEinspeisungBezeichnung in DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG or UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG
        defaultMastrUnitShouldBeFound(
            "vollTeilEinspeisungBezeichnung.in=" +
            DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG +
            "," +
            UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG
        );

        // Get all the mastrUnitList where vollTeilEinspeisungBezeichnung equals to UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("vollTeilEinspeisungBezeichnung.in=" + UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByVollTeilEinspeisungBezeichnungIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where vollTeilEinspeisungBezeichnung is not null
        defaultMastrUnitShouldBeFound("vollTeilEinspeisungBezeichnung.specified=true");

        // Get all the mastrUnitList where vollTeilEinspeisungBezeichnung is null
        defaultMastrUnitShouldNotBeFound("vollTeilEinspeisungBezeichnung.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByVollTeilEinspeisungBezeichnungContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where vollTeilEinspeisungBezeichnung contains DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG
        defaultMastrUnitShouldBeFound("vollTeilEinspeisungBezeichnung.contains=" + DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG);

        // Get all the mastrUnitList where vollTeilEinspeisungBezeichnung contains UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("vollTeilEinspeisungBezeichnung.contains=" + UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByVollTeilEinspeisungBezeichnungNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where vollTeilEinspeisungBezeichnung does not contain DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG
        defaultMastrUnitShouldNotBeFound("vollTeilEinspeisungBezeichnung.doesNotContain=" + DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG);

        // Get all the mastrUnitList where vollTeilEinspeisungBezeichnung does not contain UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG
        defaultMastrUnitShouldBeFound("vollTeilEinspeisungBezeichnung.doesNotContain=" + UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByWindparkNameIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where windparkName equals to DEFAULT_WINDPARK_NAME
        defaultMastrUnitShouldBeFound("windparkName.equals=" + DEFAULT_WINDPARK_NAME);

        // Get all the mastrUnitList where windparkName equals to UPDATED_WINDPARK_NAME
        defaultMastrUnitShouldNotBeFound("windparkName.equals=" + UPDATED_WINDPARK_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByWindparkNameIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where windparkName in DEFAULT_WINDPARK_NAME or UPDATED_WINDPARK_NAME
        defaultMastrUnitShouldBeFound("windparkName.in=" + DEFAULT_WINDPARK_NAME + "," + UPDATED_WINDPARK_NAME);

        // Get all the mastrUnitList where windparkName equals to UPDATED_WINDPARK_NAME
        defaultMastrUnitShouldNotBeFound("windparkName.in=" + UPDATED_WINDPARK_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByWindparkNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where windparkName is not null
        defaultMastrUnitShouldBeFound("windparkName.specified=true");

        // Get all the mastrUnitList where windparkName is null
        defaultMastrUnitShouldNotBeFound("windparkName.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByWindparkNameContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where windparkName contains DEFAULT_WINDPARK_NAME
        defaultMastrUnitShouldBeFound("windparkName.contains=" + DEFAULT_WINDPARK_NAME);

        // Get all the mastrUnitList where windparkName contains UPDATED_WINDPARK_NAME
        defaultMastrUnitShouldNotBeFound("windparkName.contains=" + UPDATED_WINDPARK_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByWindparkNameNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where windparkName does not contain DEFAULT_WINDPARK_NAME
        defaultMastrUnitShouldNotBeFound("windparkName.doesNotContain=" + DEFAULT_WINDPARK_NAME);

        // Get all the mastrUnitList where windparkName does not contain UPDATED_WINDPARK_NAME
        defaultMastrUnitShouldBeFound("windparkName.doesNotContain=" + UPDATED_WINDPARK_NAME);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGeometryIsEqualToSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where geometry equals to DEFAULT_GEOMETRY
        defaultMastrUnitShouldBeFound("geometry.equals=" + DEFAULT_GEOMETRY);

        // Get all the mastrUnitList where geometry equals to UPDATED_GEOMETRY
        defaultMastrUnitShouldNotBeFound("geometry.equals=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGeometryIsInShouldWork() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where geometry in DEFAULT_GEOMETRY or UPDATED_GEOMETRY
        defaultMastrUnitShouldBeFound("geometry.in=" + DEFAULT_GEOMETRY + "," + UPDATED_GEOMETRY);

        // Get all the mastrUnitList where geometry equals to UPDATED_GEOMETRY
        defaultMastrUnitShouldNotBeFound("geometry.in=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGeometryIsNullOrNotNull() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where geometry is not null
        defaultMastrUnitShouldBeFound("geometry.specified=true");

        // Get all the mastrUnitList where geometry is null
        defaultMastrUnitShouldNotBeFound("geometry.specified=false");
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGeometryContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where geometry contains DEFAULT_GEOMETRY
        defaultMastrUnitShouldBeFound("geometry.contains=" + DEFAULT_GEOMETRY);

        // Get all the mastrUnitList where geometry contains UPDATED_GEOMETRY
        defaultMastrUnitShouldNotBeFound("geometry.contains=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByGeometryNotContainsSomething() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        // Get all the mastrUnitList where geometry does not contain DEFAULT_GEOMETRY
        defaultMastrUnitShouldNotBeFound("geometry.doesNotContain=" + DEFAULT_GEOMETRY);

        // Get all the mastrUnitList where geometry does not contain UPDATED_GEOMETRY
        defaultMastrUnitShouldBeFound("geometry.doesNotContain=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllMastrUnitsByAdministrationUnitIsEqualToSomething() throws Exception {
        AdministrationUnit administrationUnit;
        if (TestUtil.findAll(em, AdministrationUnit.class).isEmpty()) {
            mastrUnitRepository.saveAndFlush(mastrUnit);
            administrationUnit = AdministrationUnitResourceIT.createEntity(em);
        } else {
            administrationUnit = TestUtil.findAll(em, AdministrationUnit.class).get(0);
        }
        em.persist(administrationUnit);
        em.flush();
        mastrUnit.setAdministrationUnit(administrationUnit);
        mastrUnitRepository.saveAndFlush(mastrUnit);
        Long administrationUnitId = administrationUnit.getId();

        // Get all the mastrUnitList where administrationUnit equals to administrationUnitId
        defaultMastrUnitShouldBeFound("administrationUnitId.equals=" + administrationUnitId);

        // Get all the mastrUnitList where administrationUnit equals to (administrationUnitId + 1)
        defaultMastrUnitShouldNotBeFound("administrationUnitId.equals=" + (administrationUnitId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMastrUnitShouldBeFound(String filter) throws Exception {
        restMastrUnitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mastrUnit.getId().intValue())))
            .andExpect(jsonPath("$.[*].mastrNummer").value(hasItem(DEFAULT_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].anlagenBetreiberId").value(hasItem(DEFAULT_ANLAGEN_BETREIBER_ID.intValue())))
            .andExpect(jsonPath("$.[*].anlagenBetreiberPersonenArt").value(hasItem(DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART.intValue())))
            .andExpect(jsonPath("$.[*].anlagenBetreiberMaskedName").value(hasItem(DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME)))
            .andExpect(jsonPath("$.[*].anlagenBetreiberMastrNummer").value(hasItem(DEFAULT_ANLAGEN_BETREIBER_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].anlagenBetreiberName").value(hasItem(DEFAULT_ANLAGEN_BETREIBER_NAME)))
            .andExpect(jsonPath("$.[*].betriebsStatusId").value(hasItem(DEFAULT_BETRIEBS_STATUS_ID)))
            .andExpect(jsonPath("$.[*].betriebsStatusName").value(hasItem(DEFAULT_BETRIEBS_STATUS_NAME)))
            .andExpect(jsonPath("$.[*].bundesland").value(hasItem(DEFAULT_BUNDESLAND)))
            .andExpect(jsonPath("$.[*].bundeslandId").value(hasItem(DEFAULT_BUNDESLAND_ID)))
            .andExpect(jsonPath("$.[*].landkreisId").value(hasItem(DEFAULT_LANDKREIS_ID)))
            .andExpect(jsonPath("$.[*].gemeindeId").value(hasItem(DEFAULT_GEMEINDE_ID)))
            .andExpect(jsonPath("$.[*].datumLetzteAktualisierung").value(hasItem(DEFAULT_DATUM_LETZTE_AKTUALISIERUNG.toString())))
            .andExpect(jsonPath("$.[*].einheitRegistrierungsDatum").value(hasItem(DEFAULT_EINHEIT_REGISTRIERUNGS_DATUM.toString())))
            .andExpect(jsonPath("$.[*].endgueltigeStilllegungDatum").value(hasItem(DEFAULT_ENDGUELTIGE_STILLLEGUNG_DATUM.toString())))
            .andExpect(jsonPath("$.[*].geplantesInbetriebsnahmeDatum").value(hasItem(DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM.toString())))
            .andExpect(jsonPath("$.[*].inbetriebnahmeDatum").value(hasItem(DEFAULT_INBETRIEBNAHME_DATUM.toString())))
            .andExpect(jsonPath("$.[*].kwkAnlageInbetriebnahmeDatum").value(hasItem(DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM.toString())))
            .andExpect(jsonPath("$.[*].kwkAnlageRegistrierungsDatum").value(hasItem(DEFAULT_KWK_ANLAGE_REGISTRIERUNGS_DATUM.toString())))
            .andExpect(jsonPath("$.[*].eegInbetriebnahmeDatum").value(hasItem(DEFAULT_EEG_INBETRIEBNAHME_DATUM.toString())))
            .andExpect(jsonPath("$.[*].eegAnlageRegistrierungsDatum").value(hasItem(DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM.toString())))
            .andExpect(jsonPath("$.[*].genehmigungDatum").value(hasItem(DEFAULT_GENEHMIGUNG_DATUM.toString())))
            .andExpect(jsonPath("$.[*].genehmigungRegistrierungsDatum").value(hasItem(DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM.toString())))
            .andExpect(jsonPath("$.[*].isNbPruefungAbgeschlossen").value(hasItem(DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN)))
            .andExpect(jsonPath("$.[*].isAnonymisiert").value(hasItem(DEFAULT_IS_ANONYMISIERT.booleanValue())))
            .andExpect(jsonPath("$.[*].isBuergerEnergie").value(hasItem(DEFAULT_IS_BUERGER_ENERGIE.booleanValue())))
            .andExpect(jsonPath("$.[*].isEinheitNotstromaggregat").value(hasItem(DEFAULT_IS_EINHEIT_NOTSTROMAGGREGAT.booleanValue())))
            .andExpect(jsonPath("$.[*].isMieterstromAngemeldet").value(hasItem(DEFAULT_IS_MIETERSTROM_ANGEMELDET.booleanValue())))
            .andExpect(jsonPath("$.[*].isWasserkraftErtuechtigung").value(hasItem(DEFAULT_IS_WASSERKRAFT_ERTUECHTIGUNG.booleanValue())))
            .andExpect(jsonPath("$.[*].isPilotWindanlage").value(hasItem(DEFAULT_IS_PILOT_WINDANLAGE.booleanValue())))
            .andExpect(jsonPath("$.[*].isPrototypAnlage").value(hasItem(DEFAULT_IS_PROTOTYP_ANLAGE.booleanValue())))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.doubleValue())))
            .andExpect(jsonPath("$.[*].lng").value(hasItem(DEFAULT_LNG.doubleValue())))
            .andExpect(jsonPath("$.[*].ort").value(hasItem(DEFAULT_ORT)))
            .andExpect(jsonPath("$.[*].plz").value(hasItem(DEFAULT_PLZ)))
            .andExpect(jsonPath("$.[*].strasse").value(hasItem(DEFAULT_STRASSE)))
            .andExpect(jsonPath("$.[*].hausnummer").value(hasItem(DEFAULT_HAUSNUMMER)))
            .andExpect(jsonPath("$.[*].einheitname").value(hasItem(DEFAULT_EINHEITNAME)))
            .andExpect(jsonPath("$.[*].flurstueck").value(hasItem(DEFAULT_FLURSTUECK)))
            .andExpect(jsonPath("$.[*].gemarkung").value(hasItem(DEFAULT_GEMARKUNG)))
            .andExpect(jsonPath("$.[*].gemeinde").value(hasItem(DEFAULT_GEMEINDE)))
            .andExpect(jsonPath("$.[*].landId").value(hasItem(DEFAULT_LAND_ID)))
            .andExpect(jsonPath("$.[*].landkreis").value(hasItem(DEFAULT_LANDKREIS)))
            .andExpect(jsonPath("$.[*].ags").value(hasItem(DEFAULT_AGS.intValue())))
            .andExpect(jsonPath("$.[*].lokationId").value(hasItem(DEFAULT_LOKATION_ID.intValue())))
            .andExpect(jsonPath("$.[*].lokationMastrNr").value(hasItem(DEFAULT_LOKATION_MASTR_NR)))
            .andExpect(jsonPath("$.[*].netzbetreiberId").value(hasItem(DEFAULT_NETZBETREIBER_ID)))
            .andExpect(jsonPath("$.[*].netzbetreiberMaskedNamen").value(hasItem(DEFAULT_NETZBETREIBER_MASKED_NAMEN)))
            .andExpect(jsonPath("$.[*].netzbetreiberMastrNummer").value(hasItem(DEFAULT_NETZBETREIBER_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].netzbetreiberNamen").value(hasItem(DEFAULT_NETZBETREIBER_NAMEN)))
            .andExpect(jsonPath("$.[*].netzbetreiberPersonenArt").value(hasItem(DEFAULT_NETZBETREIBER_PERSONEN_ART)))
            .andExpect(jsonPath("$.[*].systemStatusId").value(hasItem(DEFAULT_SYSTEM_STATUS_ID)))
            .andExpect(jsonPath("$.[*].systemStatusName").value(hasItem(DEFAULT_SYSTEM_STATUS_NAME)))
            .andExpect(jsonPath("$.[*].typ").value(hasItem(DEFAULT_TYP)))
            .andExpect(jsonPath("$.[*].aktenzeichenGenehmigung").value(hasItem(DEFAULT_AKTENZEICHEN_GENEHMIGUNG)))
            .andExpect(jsonPath("$.[*].anzahlSolarmodule").value(hasItem(DEFAULT_ANZAHL_SOLARMODULE)))
            .andExpect(jsonPath("$.[*].batterieTechnologie").value(hasItem(DEFAULT_BATTERIE_TECHNOLOGIE)))
            .andExpect(jsonPath("$.[*].bruttoLeistung").value(hasItem(DEFAULT_BRUTTO_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].eegInstallierteLeistung").value(hasItem(DEFAULT_EEG_INSTALLIERTE_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].eegAnlageMastrNummer").value(hasItem(DEFAULT_EEG_ANLAGE_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].eegAnlagenSchluessel").value(hasItem(DEFAULT_EEG_ANLAGEN_SCHLUESSEL)))
            .andExpect(jsonPath("$.[*].eegZuschlag").value(hasItem(DEFAULT_EEG_ZUSCHLAG)))
            .andExpect(jsonPath("$.[*].zuschlagsNummern").value(hasItem(DEFAULT_ZUSCHLAGS_NUMMERN)))
            .andExpect(jsonPath("$.[*].energieTraegerId").value(hasItem(DEFAULT_ENERGIE_TRAEGER_ID)))
            .andExpect(jsonPath("$.[*].energieTraegerName").value(hasItem(DEFAULT_ENERGIE_TRAEGER_NAME)))
            .andExpect(jsonPath("$.[*].gemeinsamerWechselrichter").value(hasItem(DEFAULT_GEMEINSAMER_WECHSELRICHTER)))
            .andExpect(jsonPath("$.[*].genehmigungBehoerde").value(hasItem(DEFAULT_GENEHMIGUNG_BEHOERDE)))
            .andExpect(jsonPath("$.[*].genehmigungsMastrNummer").value(hasItem(DEFAULT_GENEHMIGUNGS_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].gruppierungsObjekte").value(hasItem(DEFAULT_GRUPPIERUNGS_OBJEKTE)))
            .andExpect(jsonPath("$.[*].gruppierungsObjekteIds").value(hasItem(DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS)))
            .andExpect(jsonPath("$.[*].hatFlexibilitaetsPraemie").value(hasItem(DEFAULT_HAT_FLEXIBILITAETS_PRAEMIE.booleanValue())))
            .andExpect(jsonPath("$.[*].hauptAusrichtungSolarmodule").value(hasItem(DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE)))
            .andExpect(
                jsonPath("$.[*].hauptAusrichtungSolarmoduleBezeichnung").value(hasItem(DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG))
            )
            .andExpect(jsonPath("$.[*].hauptBrennstoffId").value(hasItem(DEFAULT_HAUPT_BRENNSTOFF_ID)))
            .andExpect(jsonPath("$.[*].hauptBrennstoffNamen").value(hasItem(DEFAULT_HAUPT_BRENNSTOFF_NAMEN)))
            .andExpect(jsonPath("$.[*].hauptNeigungswinkelSolarModule").value(hasItem(DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE)))
            .andExpect(jsonPath("$.[*].herstellerWindenergieanlageId").value(hasItem(DEFAULT_HERSTELLER_WINDENERGIEANLAGE_ID)))
            .andExpect(
                jsonPath("$.[*].herstellerWindenergieanlageBezeichnung").value(hasItem(DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG))
            )
            .andExpect(jsonPath("$.[*].kwkAnlageElektrischeLeistung").value(hasItem(DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].kwkAnlageMastrNummer").value(hasItem(DEFAULT_KWK_ANLAGE_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].kwkZuschlag").value(hasItem(DEFAULT_KWK_ZUSCHLAG)))
            .andExpect(jsonPath("$.[*].lageEinheit").value(hasItem(DEFAULT_LAGE_EINHEIT)))
            .andExpect(jsonPath("$.[*].lageEinheitBezeichnung").value(hasItem(DEFAULT_LAGE_EINHEIT_BEZEICHNUNG)))
            .andExpect(jsonPath("$.[*].leistungsBegrenzung").value(hasItem(DEFAULT_LEISTUNGS_BEGRENZUNG)))
            .andExpect(jsonPath("$.[*].nabenhoeheWindenergieanlage").value(hasItem(DEFAULT_NABENHOEHE_WINDENERGIEANLAGE.doubleValue())))
            .andExpect(
                jsonPath("$.[*].rotorDurchmesserWindenergieanlage")
                    .value(hasItem(DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE.doubleValue()))
            )
            .andExpect(jsonPath("$.[*].nettoNennLeistung").value(hasItem(DEFAULT_NETTO_NENN_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].nutzbareSpeicherKapazitaet").value(hasItem(DEFAULT_NUTZBARE_SPEICHER_KAPAZITAET.doubleValue())))
            .andExpect(jsonPath("$.[*].nutzungsBereichGebsa").value(hasItem(DEFAULT_NUTZUNGS_BEREICH_GEBSA)))
            .andExpect(jsonPath("$.[*].standortAnonymisiert").value(hasItem(DEFAULT_STANDORT_ANONYMISIERT)))
            .andExpect(jsonPath("$.[*].spannungsEbenenId").value(hasItem(DEFAULT_SPANNUNGS_EBENEN_ID)))
            .andExpect(jsonPath("$.[*].spannungsEbenenNamen").value(hasItem(DEFAULT_SPANNUNGS_EBENEN_NAMEN)))
            .andExpect(jsonPath("$.[*].speicherEinheitMastrNummer").value(hasItem(DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER)))
            .andExpect(jsonPath("$.[*].technologieStromerzeugungId").value(hasItem(DEFAULT_TECHNOLOGIE_STROMERZEUGUNG_ID)))
            .andExpect(jsonPath("$.[*].technologieStromerzeugung").value(hasItem(DEFAULT_TECHNOLOGIE_STROMERZEUGUNG)))
            .andExpect(jsonPath("$.[*].thermischeNutzLeistung").value(hasItem(DEFAULT_THERMISCHE_NUTZ_LEISTUNG.doubleValue())))
            .andExpect(jsonPath("$.[*].typenBezeichnung").value(hasItem(DEFAULT_TYPEN_BEZEICHNUNG)))
            .andExpect(jsonPath("$.[*].vollTeilEinspeisung").value(hasItem(DEFAULT_VOLL_TEIL_EINSPEISUNG)))
            .andExpect(jsonPath("$.[*].vollTeilEinspeisungBezeichnung").value(hasItem(DEFAULT_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG)))
            .andExpect(jsonPath("$.[*].windparkName").value(hasItem(DEFAULT_WINDPARK_NAME)))
            .andExpect(jsonPath("$.[*].geometry").value(hasItem(DEFAULT_GEOMETRY)));

        // Check, that the count call also returns 1
        restMastrUnitMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMastrUnitShouldNotBeFound(String filter) throws Exception {
        restMastrUnitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMastrUnitMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingMastrUnit() throws Exception {
        // Get the mastrUnit
        restMastrUnitMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingMastrUnit() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        int databaseSizeBeforeUpdate = mastrUnitRepository.findAll().size();

        // Update the mastrUnit
        MastrUnit updatedMastrUnit = mastrUnitRepository.findById(mastrUnit.getId()).get();
        // Disconnect from session so that the updates on updatedMastrUnit are not directly saved in db
        em.detach(updatedMastrUnit);
        updatedMastrUnit
            .mastrNummer(UPDATED_MASTR_NUMMER)
            .anlagenBetreiberId(UPDATED_ANLAGEN_BETREIBER_ID)
            .anlagenBetreiberPersonenArt(UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART)
            .anlagenBetreiberMaskedName(UPDATED_ANLAGEN_BETREIBER_MASKED_NAME)
            .anlagenBetreiberMastrNummer(UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER)
            .anlagenBetreiberName(UPDATED_ANLAGEN_BETREIBER_NAME)
            .betriebsStatusId(UPDATED_BETRIEBS_STATUS_ID)
            .betriebsStatusName(UPDATED_BETRIEBS_STATUS_NAME)
            .bundesland(UPDATED_BUNDESLAND)
            .bundeslandId(UPDATED_BUNDESLAND_ID)
            .landkreisId(UPDATED_LANDKREIS_ID)
            .gemeindeId(UPDATED_GEMEINDE_ID)
            .datumLetzteAktualisierung(UPDATED_DATUM_LETZTE_AKTUALISIERUNG)
            .einheitRegistrierungsDatum(UPDATED_EINHEIT_REGISTRIERUNGS_DATUM)
            .endgueltigeStilllegungDatum(UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM)
            .geplantesInbetriebsnahmeDatum(UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM)
            .inbetriebnahmeDatum(UPDATED_INBETRIEBNAHME_DATUM)
            .kwkAnlageInbetriebnahmeDatum(UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM)
            .kwkAnlageRegistrierungsDatum(UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM)
            .eegInbetriebnahmeDatum(UPDATED_EEG_INBETRIEBNAHME_DATUM)
            .eegAnlageRegistrierungsDatum(UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM)
            .genehmigungDatum(UPDATED_GENEHMIGUNG_DATUM)
            .genehmigungRegistrierungsDatum(UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM)
            .isNbPruefungAbgeschlossen(UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN)
            .isAnonymisiert(UPDATED_IS_ANONYMISIERT)
            .isBuergerEnergie(UPDATED_IS_BUERGER_ENERGIE)
            .isEinheitNotstromaggregat(UPDATED_IS_EINHEIT_NOTSTROMAGGREGAT)
            .isMieterstromAngemeldet(UPDATED_IS_MIETERSTROM_ANGEMELDET)
            .isWasserkraftErtuechtigung(UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG)
            .isPilotWindanlage(UPDATED_IS_PILOT_WINDANLAGE)
            .isPrototypAnlage(UPDATED_IS_PROTOTYP_ANLAGE)
            .lat(UPDATED_LAT)
            .lng(UPDATED_LNG)
            .ort(UPDATED_ORT)
            .plz(UPDATED_PLZ)
            .strasse(UPDATED_STRASSE)
            .hausnummer(UPDATED_HAUSNUMMER)
            .einheitname(UPDATED_EINHEITNAME)
            .flurstueck(UPDATED_FLURSTUECK)
            .gemarkung(UPDATED_GEMARKUNG)
            .gemeinde(UPDATED_GEMEINDE)
            .landId(UPDATED_LAND_ID)
            .landkreis(UPDATED_LANDKREIS)
            .ags(UPDATED_AGS)
            .lokationId(UPDATED_LOKATION_ID)
            .lokationMastrNr(UPDATED_LOKATION_MASTR_NR)
            .netzbetreiberId(UPDATED_NETZBETREIBER_ID)
            .netzbetreiberMaskedNamen(UPDATED_NETZBETREIBER_MASKED_NAMEN)
            .netzbetreiberMastrNummer(UPDATED_NETZBETREIBER_MASTR_NUMMER)
            .netzbetreiberNamen(UPDATED_NETZBETREIBER_NAMEN)
            .netzbetreiberPersonenArt(UPDATED_NETZBETREIBER_PERSONEN_ART)
            .systemStatusId(UPDATED_SYSTEM_STATUS_ID)
            .systemStatusName(UPDATED_SYSTEM_STATUS_NAME)
            .typ(UPDATED_TYP)
            .aktenzeichenGenehmigung(UPDATED_AKTENZEICHEN_GENEHMIGUNG)
            .anzahlSolarmodule(UPDATED_ANZAHL_SOLARMODULE)
            .batterieTechnologie(UPDATED_BATTERIE_TECHNOLOGIE)
            .bruttoLeistung(UPDATED_BRUTTO_LEISTUNG)
            .eegInstallierteLeistung(UPDATED_EEG_INSTALLIERTE_LEISTUNG)
            .eegAnlageMastrNummer(UPDATED_EEG_ANLAGE_MASTR_NUMMER)
            .eegAnlagenSchluessel(UPDATED_EEG_ANLAGEN_SCHLUESSEL)
            .eegZuschlag(UPDATED_EEG_ZUSCHLAG)
            .zuschlagsNummern(UPDATED_ZUSCHLAGS_NUMMERN)
            .energieTraegerId(UPDATED_ENERGIE_TRAEGER_ID)
            .energieTraegerName(UPDATED_ENERGIE_TRAEGER_NAME)
            .gemeinsamerWechselrichter(UPDATED_GEMEINSAMER_WECHSELRICHTER)
            .genehmigungBehoerde(UPDATED_GENEHMIGUNG_BEHOERDE)
            .genehmigungsMastrNummer(UPDATED_GENEHMIGUNGS_MASTR_NUMMER)
            .gruppierungsObjekte(UPDATED_GRUPPIERUNGS_OBJEKTE)
            .gruppierungsObjekteIds(UPDATED_GRUPPIERUNGS_OBJEKTE_IDS)
            .hatFlexibilitaetsPraemie(UPDATED_HAT_FLEXIBILITAETS_PRAEMIE)
            .hauptAusrichtungSolarmodule(UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE)
            .hauptAusrichtungSolarmoduleBezeichnung(UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG)
            .hauptBrennstoffId(UPDATED_HAUPT_BRENNSTOFF_ID)
            .hauptBrennstoffNamen(UPDATED_HAUPT_BRENNSTOFF_NAMEN)
            .hauptNeigungswinkelSolarModule(UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE)
            .herstellerWindenergieanlageId(UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID)
            .herstellerWindenergieanlageBezeichnung(UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG)
            .kwkAnlageElektrischeLeistung(UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG)
            .kwkAnlageMastrNummer(UPDATED_KWK_ANLAGE_MASTR_NUMMER)
            .kwkZuschlag(UPDATED_KWK_ZUSCHLAG)
            .lageEinheit(UPDATED_LAGE_EINHEIT)
            .lageEinheitBezeichnung(UPDATED_LAGE_EINHEIT_BEZEICHNUNG)
            .leistungsBegrenzung(UPDATED_LEISTUNGS_BEGRENZUNG)
            .nabenhoeheWindenergieanlage(UPDATED_NABENHOEHE_WINDENERGIEANLAGE)
            .rotorDurchmesserWindenergieanlage(UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE)
            .nettoNennLeistung(UPDATED_NETTO_NENN_LEISTUNG)
            .nutzbareSpeicherKapazitaet(UPDATED_NUTZBARE_SPEICHER_KAPAZITAET)
            .nutzungsBereichGebsa(UPDATED_NUTZUNGS_BEREICH_GEBSA)
            .standortAnonymisiert(UPDATED_STANDORT_ANONYMISIERT)
            .spannungsEbenenId(UPDATED_SPANNUNGS_EBENEN_ID)
            .spannungsEbenenNamen(UPDATED_SPANNUNGS_EBENEN_NAMEN)
            .speicherEinheitMastrNummer(UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER)
            .technologieStromerzeugungId(UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID)
            .technologieStromerzeugung(UPDATED_TECHNOLOGIE_STROMERZEUGUNG)
            .thermischeNutzLeistung(UPDATED_THERMISCHE_NUTZ_LEISTUNG)
            .typenBezeichnung(UPDATED_TYPEN_BEZEICHNUNG)
            .vollTeilEinspeisung(UPDATED_VOLL_TEIL_EINSPEISUNG)
            .vollTeilEinspeisungBezeichnung(UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG)
            .windparkName(UPDATED_WINDPARK_NAME)
            .geometry(UPDATED_GEOMETRY);

        restMastrUnitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMastrUnit.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMastrUnit))
            )
            .andExpect(status().isOk());

        // Validate the MastrUnit in the database
        List<MastrUnit> mastrUnitList = mastrUnitRepository.findAll();
        assertThat(mastrUnitList).hasSize(databaseSizeBeforeUpdate);
        MastrUnit testMastrUnit = mastrUnitList.get(mastrUnitList.size() - 1);
        assertThat(testMastrUnit.getMastrNummer()).isEqualTo(UPDATED_MASTR_NUMMER);
        assertThat(testMastrUnit.getAnlagenBetreiberId()).isEqualTo(UPDATED_ANLAGEN_BETREIBER_ID);
        assertThat(testMastrUnit.getAnlagenBetreiberPersonenArt()).isEqualTo(UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART);
        assertThat(testMastrUnit.getAnlagenBetreiberMaskedName()).isEqualTo(UPDATED_ANLAGEN_BETREIBER_MASKED_NAME);
        assertThat(testMastrUnit.getAnlagenBetreiberMastrNummer()).isEqualTo(UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER);
        assertThat(testMastrUnit.getAnlagenBetreiberName()).isEqualTo(UPDATED_ANLAGEN_BETREIBER_NAME);
        assertThat(testMastrUnit.getBetriebsStatusId()).isEqualTo(UPDATED_BETRIEBS_STATUS_ID);
        assertThat(testMastrUnit.getBetriebsStatusName()).isEqualTo(UPDATED_BETRIEBS_STATUS_NAME);
        assertThat(testMastrUnit.getBundesland()).isEqualTo(UPDATED_BUNDESLAND);
        assertThat(testMastrUnit.getBundeslandId()).isEqualTo(UPDATED_BUNDESLAND_ID);
        assertThat(testMastrUnit.getLandkreisId()).isEqualTo(UPDATED_LANDKREIS_ID);
        assertThat(testMastrUnit.getGemeindeId()).isEqualTo(UPDATED_GEMEINDE_ID);
        assertThat(testMastrUnit.getDatumLetzteAktualisierung()).isEqualTo(UPDATED_DATUM_LETZTE_AKTUALISIERUNG);
        assertThat(testMastrUnit.getEinheitRegistrierungsDatum()).isEqualTo(UPDATED_EINHEIT_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getEndgueltigeStilllegungDatum()).isEqualTo(UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM);
        assertThat(testMastrUnit.getGeplantesInbetriebsnahmeDatum()).isEqualTo(UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM);
        assertThat(testMastrUnit.getInbetriebnahmeDatum()).isEqualTo(UPDATED_INBETRIEBNAHME_DATUM);
        assertThat(testMastrUnit.getKwkAnlageInbetriebnahmeDatum()).isEqualTo(UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM);
        assertThat(testMastrUnit.getKwkAnlageRegistrierungsDatum()).isEqualTo(UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getEegInbetriebnahmeDatum()).isEqualTo(UPDATED_EEG_INBETRIEBNAHME_DATUM);
        assertThat(testMastrUnit.getEegAnlageRegistrierungsDatum()).isEqualTo(UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getGenehmigungDatum()).isEqualTo(UPDATED_GENEHMIGUNG_DATUM);
        assertThat(testMastrUnit.getGenehmigungRegistrierungsDatum()).isEqualTo(UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getIsNbPruefungAbgeschlossen()).isEqualTo(UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN);
        assertThat(testMastrUnit.getIsAnonymisiert()).isEqualTo(UPDATED_IS_ANONYMISIERT);
        assertThat(testMastrUnit.getIsBuergerEnergie()).isEqualTo(UPDATED_IS_BUERGER_ENERGIE);
        assertThat(testMastrUnit.getIsEinheitNotstromaggregat()).isEqualTo(UPDATED_IS_EINHEIT_NOTSTROMAGGREGAT);
        assertThat(testMastrUnit.getIsMieterstromAngemeldet()).isEqualTo(UPDATED_IS_MIETERSTROM_ANGEMELDET);
        assertThat(testMastrUnit.getIsWasserkraftErtuechtigung()).isEqualTo(UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG);
        assertThat(testMastrUnit.getIsPilotWindanlage()).isEqualTo(UPDATED_IS_PILOT_WINDANLAGE);
        assertThat(testMastrUnit.getIsPrototypAnlage()).isEqualTo(UPDATED_IS_PROTOTYP_ANLAGE);
        assertThat(testMastrUnit.getLat()).isEqualTo(UPDATED_LAT);
        assertThat(testMastrUnit.getLng()).isEqualTo(UPDATED_LNG);
        assertThat(testMastrUnit.getOrt()).isEqualTo(UPDATED_ORT);
        assertThat(testMastrUnit.getPlz()).isEqualTo(UPDATED_PLZ);
        assertThat(testMastrUnit.getStrasse()).isEqualTo(UPDATED_STRASSE);
        assertThat(testMastrUnit.getHausnummer()).isEqualTo(UPDATED_HAUSNUMMER);
        assertThat(testMastrUnit.getEinheitname()).isEqualTo(UPDATED_EINHEITNAME);
        assertThat(testMastrUnit.getFlurstueck()).isEqualTo(UPDATED_FLURSTUECK);
        assertThat(testMastrUnit.getGemarkung()).isEqualTo(UPDATED_GEMARKUNG);
        assertThat(testMastrUnit.getGemeinde()).isEqualTo(UPDATED_GEMEINDE);
        assertThat(testMastrUnit.getLandId()).isEqualTo(UPDATED_LAND_ID);
        assertThat(testMastrUnit.getLandkreis()).isEqualTo(UPDATED_LANDKREIS);
        assertThat(testMastrUnit.getAgs()).isEqualTo(UPDATED_AGS);
        assertThat(testMastrUnit.getLokationId()).isEqualTo(UPDATED_LOKATION_ID);
        assertThat(testMastrUnit.getLokationMastrNr()).isEqualTo(UPDATED_LOKATION_MASTR_NR);
        assertThat(testMastrUnit.getNetzbetreiberId()).isEqualTo(UPDATED_NETZBETREIBER_ID);
        assertThat(testMastrUnit.getNetzbetreiberMaskedNamen()).isEqualTo(UPDATED_NETZBETREIBER_MASKED_NAMEN);
        assertThat(testMastrUnit.getNetzbetreiberMastrNummer()).isEqualTo(UPDATED_NETZBETREIBER_MASTR_NUMMER);
        assertThat(testMastrUnit.getNetzbetreiberNamen()).isEqualTo(UPDATED_NETZBETREIBER_NAMEN);
        assertThat(testMastrUnit.getNetzbetreiberPersonenArt()).isEqualTo(UPDATED_NETZBETREIBER_PERSONEN_ART);
        assertThat(testMastrUnit.getSystemStatusId()).isEqualTo(UPDATED_SYSTEM_STATUS_ID);
        assertThat(testMastrUnit.getSystemStatusName()).isEqualTo(UPDATED_SYSTEM_STATUS_NAME);
        assertThat(testMastrUnit.getTyp()).isEqualTo(UPDATED_TYP);
        assertThat(testMastrUnit.getAktenzeichenGenehmigung()).isEqualTo(UPDATED_AKTENZEICHEN_GENEHMIGUNG);
        assertThat(testMastrUnit.getAnzahlSolarmodule()).isEqualTo(UPDATED_ANZAHL_SOLARMODULE);
        assertThat(testMastrUnit.getBatterieTechnologie()).isEqualTo(UPDATED_BATTERIE_TECHNOLOGIE);
        assertThat(testMastrUnit.getBruttoLeistung()).isEqualTo(UPDATED_BRUTTO_LEISTUNG);
        assertThat(testMastrUnit.getEegInstallierteLeistung()).isEqualTo(UPDATED_EEG_INSTALLIERTE_LEISTUNG);
        assertThat(testMastrUnit.getEegAnlageMastrNummer()).isEqualTo(UPDATED_EEG_ANLAGE_MASTR_NUMMER);
        assertThat(testMastrUnit.getEegAnlagenSchluessel()).isEqualTo(UPDATED_EEG_ANLAGEN_SCHLUESSEL);
        assertThat(testMastrUnit.getEegZuschlag()).isEqualTo(UPDATED_EEG_ZUSCHLAG);
        assertThat(testMastrUnit.getZuschlagsNummern()).isEqualTo(UPDATED_ZUSCHLAGS_NUMMERN);
        assertThat(testMastrUnit.getEnergieTraegerId()).isEqualTo(UPDATED_ENERGIE_TRAEGER_ID);
        assertThat(testMastrUnit.getEnergieTraegerName()).isEqualTo(UPDATED_ENERGIE_TRAEGER_NAME);
        assertThat(testMastrUnit.getGemeinsamerWechselrichter()).isEqualTo(UPDATED_GEMEINSAMER_WECHSELRICHTER);
        assertThat(testMastrUnit.getGenehmigungBehoerde()).isEqualTo(UPDATED_GENEHMIGUNG_BEHOERDE);
        assertThat(testMastrUnit.getGenehmigungsMastrNummer()).isEqualTo(UPDATED_GENEHMIGUNGS_MASTR_NUMMER);
        assertThat(testMastrUnit.getGruppierungsObjekte()).isEqualTo(UPDATED_GRUPPIERUNGS_OBJEKTE);
        assertThat(testMastrUnit.getGruppierungsObjekteIds()).isEqualTo(UPDATED_GRUPPIERUNGS_OBJEKTE_IDS);
        assertThat(testMastrUnit.getHatFlexibilitaetsPraemie()).isEqualTo(UPDATED_HAT_FLEXIBILITAETS_PRAEMIE);
        assertThat(testMastrUnit.getHauptAusrichtungSolarmodule()).isEqualTo(UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE);
        assertThat(testMastrUnit.getHauptAusrichtungSolarmoduleBezeichnung()).isEqualTo(UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG);
        assertThat(testMastrUnit.getHauptBrennstoffId()).isEqualTo(UPDATED_HAUPT_BRENNSTOFF_ID);
        assertThat(testMastrUnit.getHauptBrennstoffNamen()).isEqualTo(UPDATED_HAUPT_BRENNSTOFF_NAMEN);
        assertThat(testMastrUnit.getHauptNeigungswinkelSolarModule()).isEqualTo(UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);
        assertThat(testMastrUnit.getHerstellerWindenergieanlageId()).isEqualTo(UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID);
        assertThat(testMastrUnit.getHerstellerWindenergieanlageBezeichnung()).isEqualTo(UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG);
        assertThat(testMastrUnit.getKwkAnlageElektrischeLeistung()).isEqualTo(UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);
        assertThat(testMastrUnit.getKwkAnlageMastrNummer()).isEqualTo(UPDATED_KWK_ANLAGE_MASTR_NUMMER);
        assertThat(testMastrUnit.getKwkZuschlag()).isEqualTo(UPDATED_KWK_ZUSCHLAG);
        assertThat(testMastrUnit.getLageEinheit()).isEqualTo(UPDATED_LAGE_EINHEIT);
        assertThat(testMastrUnit.getLageEinheitBezeichnung()).isEqualTo(UPDATED_LAGE_EINHEIT_BEZEICHNUNG);
        assertThat(testMastrUnit.getLeistungsBegrenzung()).isEqualTo(UPDATED_LEISTUNGS_BEGRENZUNG);
        assertThat(testMastrUnit.getNabenhoeheWindenergieanlage()).isEqualTo(UPDATED_NABENHOEHE_WINDENERGIEANLAGE);
        assertThat(testMastrUnit.getRotorDurchmesserWindenergieanlage()).isEqualTo(UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE);
        assertThat(testMastrUnit.getNettoNennLeistung()).isEqualTo(UPDATED_NETTO_NENN_LEISTUNG);
        assertThat(testMastrUnit.getNutzbareSpeicherKapazitaet()).isEqualTo(UPDATED_NUTZBARE_SPEICHER_KAPAZITAET);
        assertThat(testMastrUnit.getNutzungsBereichGebsa()).isEqualTo(UPDATED_NUTZUNGS_BEREICH_GEBSA);
        assertThat(testMastrUnit.getStandortAnonymisiert()).isEqualTo(UPDATED_STANDORT_ANONYMISIERT);
        assertThat(testMastrUnit.getSpannungsEbenenId()).isEqualTo(UPDATED_SPANNUNGS_EBENEN_ID);
        assertThat(testMastrUnit.getSpannungsEbenenNamen()).isEqualTo(UPDATED_SPANNUNGS_EBENEN_NAMEN);
        assertThat(testMastrUnit.getSpeicherEinheitMastrNummer()).isEqualTo(UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER);
        assertThat(testMastrUnit.getTechnologieStromerzeugungId()).isEqualTo(UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID);
        assertThat(testMastrUnit.getTechnologieStromerzeugung()).isEqualTo(UPDATED_TECHNOLOGIE_STROMERZEUGUNG);
        assertThat(testMastrUnit.getThermischeNutzLeistung()).isEqualTo(UPDATED_THERMISCHE_NUTZ_LEISTUNG);
        assertThat(testMastrUnit.getTypenBezeichnung()).isEqualTo(UPDATED_TYPEN_BEZEICHNUNG);
        assertThat(testMastrUnit.getVollTeilEinspeisung()).isEqualTo(UPDATED_VOLL_TEIL_EINSPEISUNG);
        assertThat(testMastrUnit.getVollTeilEinspeisungBezeichnung()).isEqualTo(UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG);
        assertThat(testMastrUnit.getWindparkName()).isEqualTo(UPDATED_WINDPARK_NAME);
        assertThat(testMastrUnit.getGeometry()).isEqualTo(UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void putNonExistingMastrUnit() throws Exception {
        int databaseSizeBeforeUpdate = mastrUnitRepository.findAll().size();
        mastrUnit.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMastrUnitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, mastrUnit.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(mastrUnit))
            )
            .andExpect(status().isBadRequest());

        // Validate the MastrUnit in the database
        List<MastrUnit> mastrUnitList = mastrUnitRepository.findAll();
        assertThat(mastrUnitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMastrUnit() throws Exception {
        int databaseSizeBeforeUpdate = mastrUnitRepository.findAll().size();
        mastrUnit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMastrUnitMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(mastrUnit))
            )
            .andExpect(status().isBadRequest());

        // Validate the MastrUnit in the database
        List<MastrUnit> mastrUnitList = mastrUnitRepository.findAll();
        assertThat(mastrUnitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMastrUnit() throws Exception {
        int databaseSizeBeforeUpdate = mastrUnitRepository.findAll().size();
        mastrUnit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMastrUnitMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mastrUnit)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the MastrUnit in the database
        List<MastrUnit> mastrUnitList = mastrUnitRepository.findAll();
        assertThat(mastrUnitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMastrUnitWithPatch() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        int databaseSizeBeforeUpdate = mastrUnitRepository.findAll().size();

        // Update the mastrUnit using partial update
        MastrUnit partialUpdatedMastrUnit = new MastrUnit();
        partialUpdatedMastrUnit.setId(mastrUnit.getId());

        partialUpdatedMastrUnit
            .anlagenBetreiberMastrNummer(UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER)
            .anlagenBetreiberName(UPDATED_ANLAGEN_BETREIBER_NAME)
            .betriebsStatusName(UPDATED_BETRIEBS_STATUS_NAME)
            .landkreisId(UPDATED_LANDKREIS_ID)
            .einheitRegistrierungsDatum(UPDATED_EINHEIT_REGISTRIERUNGS_DATUM)
            .endgueltigeStilllegungDatum(UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM)
            .kwkAnlageRegistrierungsDatum(UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM)
            .eegInbetriebnahmeDatum(UPDATED_EEG_INBETRIEBNAHME_DATUM)
            .genehmigungDatum(UPDATED_GENEHMIGUNG_DATUM)
            .isAnonymisiert(UPDATED_IS_ANONYMISIERT)
            .isMieterstromAngemeldet(UPDATED_IS_MIETERSTROM_ANGEMELDET)
            .isWasserkraftErtuechtigung(UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG)
            .lat(UPDATED_LAT)
            .ort(UPDATED_ORT)
            .strasse(UPDATED_STRASSE)
            .flurstueck(UPDATED_FLURSTUECK)
            .gemarkung(UPDATED_GEMARKUNG)
            .landId(UPDATED_LAND_ID)
            .landkreis(UPDATED_LANDKREIS)
            .ags(UPDATED_AGS)
            .netzbetreiberMaskedNamen(UPDATED_NETZBETREIBER_MASKED_NAMEN)
            .netzbetreiberMastrNummer(UPDATED_NETZBETREIBER_MASTR_NUMMER)
            .netzbetreiberPersonenArt(UPDATED_NETZBETREIBER_PERSONEN_ART)
            .systemStatusId(UPDATED_SYSTEM_STATUS_ID)
            .systemStatusName(UPDATED_SYSTEM_STATUS_NAME)
            .typ(UPDATED_TYP)
            .aktenzeichenGenehmigung(UPDATED_AKTENZEICHEN_GENEHMIGUNG)
            .batterieTechnologie(UPDATED_BATTERIE_TECHNOLOGIE)
            .eegInstallierteLeistung(UPDATED_EEG_INSTALLIERTE_LEISTUNG)
            .eegAnlageMastrNummer(UPDATED_EEG_ANLAGE_MASTR_NUMMER)
            .eegAnlagenSchluessel(UPDATED_EEG_ANLAGEN_SCHLUESSEL)
            .energieTraegerName(UPDATED_ENERGIE_TRAEGER_NAME)
            .genehmigungBehoerde(UPDATED_GENEHMIGUNG_BEHOERDE)
            .genehmigungsMastrNummer(UPDATED_GENEHMIGUNGS_MASTR_NUMMER)
            .gruppierungsObjekte(UPDATED_GRUPPIERUNGS_OBJEKTE)
            .hauptAusrichtungSolarmoduleBezeichnung(UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG)
            .hauptBrennstoffId(UPDATED_HAUPT_BRENNSTOFF_ID)
            .hauptBrennstoffNamen(UPDATED_HAUPT_BRENNSTOFF_NAMEN)
            .herstellerWindenergieanlageId(UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID)
            .kwkAnlageMastrNummer(UPDATED_KWK_ANLAGE_MASTR_NUMMER)
            .lageEinheit(UPDATED_LAGE_EINHEIT)
            .nabenhoeheWindenergieanlage(UPDATED_NABENHOEHE_WINDENERGIEANLAGE)
            .nutzbareSpeicherKapazitaet(UPDATED_NUTZBARE_SPEICHER_KAPAZITAET)
            .spannungsEbenenId(UPDATED_SPANNUNGS_EBENEN_ID)
            .technologieStromerzeugungId(UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID)
            .thermischeNutzLeistung(UPDATED_THERMISCHE_NUTZ_LEISTUNG)
            .vollTeilEinspeisungBezeichnung(UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG)
            .windparkName(UPDATED_WINDPARK_NAME);

        restMastrUnitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMastrUnit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMastrUnit))
            )
            .andExpect(status().isOk());

        // Validate the MastrUnit in the database
        List<MastrUnit> mastrUnitList = mastrUnitRepository.findAll();
        assertThat(mastrUnitList).hasSize(databaseSizeBeforeUpdate);
        MastrUnit testMastrUnit = mastrUnitList.get(mastrUnitList.size() - 1);
        assertThat(testMastrUnit.getMastrNummer()).isEqualTo(DEFAULT_MASTR_NUMMER);
        assertThat(testMastrUnit.getAnlagenBetreiberId()).isEqualTo(DEFAULT_ANLAGEN_BETREIBER_ID);
        assertThat(testMastrUnit.getAnlagenBetreiberPersonenArt()).isEqualTo(DEFAULT_ANLAGEN_BETREIBER_PERSONEN_ART);
        assertThat(testMastrUnit.getAnlagenBetreiberMaskedName()).isEqualTo(DEFAULT_ANLAGEN_BETREIBER_MASKED_NAME);
        assertThat(testMastrUnit.getAnlagenBetreiberMastrNummer()).isEqualTo(UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER);
        assertThat(testMastrUnit.getAnlagenBetreiberName()).isEqualTo(UPDATED_ANLAGEN_BETREIBER_NAME);
        assertThat(testMastrUnit.getBetriebsStatusId()).isEqualTo(DEFAULT_BETRIEBS_STATUS_ID);
        assertThat(testMastrUnit.getBetriebsStatusName()).isEqualTo(UPDATED_BETRIEBS_STATUS_NAME);
        assertThat(testMastrUnit.getBundesland()).isEqualTo(DEFAULT_BUNDESLAND);
        assertThat(testMastrUnit.getBundeslandId()).isEqualTo(DEFAULT_BUNDESLAND_ID);
        assertThat(testMastrUnit.getLandkreisId()).isEqualTo(UPDATED_LANDKREIS_ID);
        assertThat(testMastrUnit.getGemeindeId()).isEqualTo(DEFAULT_GEMEINDE_ID);
        assertThat(testMastrUnit.getDatumLetzteAktualisierung()).isEqualTo(DEFAULT_DATUM_LETZTE_AKTUALISIERUNG);
        assertThat(testMastrUnit.getEinheitRegistrierungsDatum()).isEqualTo(UPDATED_EINHEIT_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getEndgueltigeStilllegungDatum()).isEqualTo(UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM);
        assertThat(testMastrUnit.getGeplantesInbetriebsnahmeDatum()).isEqualTo(DEFAULT_GEPLANTES_INBETRIEBSNAHME_DATUM);
        assertThat(testMastrUnit.getInbetriebnahmeDatum()).isEqualTo(DEFAULT_INBETRIEBNAHME_DATUM);
        assertThat(testMastrUnit.getKwkAnlageInbetriebnahmeDatum()).isEqualTo(DEFAULT_KWK_ANLAGE_INBETRIEBNAHME_DATUM);
        assertThat(testMastrUnit.getKwkAnlageRegistrierungsDatum()).isEqualTo(UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getEegInbetriebnahmeDatum()).isEqualTo(UPDATED_EEG_INBETRIEBNAHME_DATUM);
        assertThat(testMastrUnit.getEegAnlageRegistrierungsDatum()).isEqualTo(DEFAULT_EEG_ANLAGE_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getGenehmigungDatum()).isEqualTo(UPDATED_GENEHMIGUNG_DATUM);
        assertThat(testMastrUnit.getGenehmigungRegistrierungsDatum()).isEqualTo(DEFAULT_GENEHMIGUNG_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getIsNbPruefungAbgeschlossen()).isEqualTo(DEFAULT_IS_NB_PRUEFUNG_ABGESCHLOSSEN);
        assertThat(testMastrUnit.getIsAnonymisiert()).isEqualTo(UPDATED_IS_ANONYMISIERT);
        assertThat(testMastrUnit.getIsBuergerEnergie()).isEqualTo(DEFAULT_IS_BUERGER_ENERGIE);
        assertThat(testMastrUnit.getIsEinheitNotstromaggregat()).isEqualTo(DEFAULT_IS_EINHEIT_NOTSTROMAGGREGAT);
        assertThat(testMastrUnit.getIsMieterstromAngemeldet()).isEqualTo(UPDATED_IS_MIETERSTROM_ANGEMELDET);
        assertThat(testMastrUnit.getIsWasserkraftErtuechtigung()).isEqualTo(UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG);
        assertThat(testMastrUnit.getIsPilotWindanlage()).isEqualTo(DEFAULT_IS_PILOT_WINDANLAGE);
        assertThat(testMastrUnit.getIsPrototypAnlage()).isEqualTo(DEFAULT_IS_PROTOTYP_ANLAGE);
        assertThat(testMastrUnit.getLat()).isEqualTo(UPDATED_LAT);
        assertThat(testMastrUnit.getLng()).isEqualTo(DEFAULT_LNG);
        assertThat(testMastrUnit.getOrt()).isEqualTo(UPDATED_ORT);
        assertThat(testMastrUnit.getPlz()).isEqualTo(DEFAULT_PLZ);
        assertThat(testMastrUnit.getStrasse()).isEqualTo(UPDATED_STRASSE);
        assertThat(testMastrUnit.getHausnummer()).isEqualTo(DEFAULT_HAUSNUMMER);
        assertThat(testMastrUnit.getEinheitname()).isEqualTo(DEFAULT_EINHEITNAME);
        assertThat(testMastrUnit.getFlurstueck()).isEqualTo(UPDATED_FLURSTUECK);
        assertThat(testMastrUnit.getGemarkung()).isEqualTo(UPDATED_GEMARKUNG);
        assertThat(testMastrUnit.getGemeinde()).isEqualTo(DEFAULT_GEMEINDE);
        assertThat(testMastrUnit.getLandId()).isEqualTo(UPDATED_LAND_ID);
        assertThat(testMastrUnit.getLandkreis()).isEqualTo(UPDATED_LANDKREIS);
        assertThat(testMastrUnit.getAgs()).isEqualTo(UPDATED_AGS);
        assertThat(testMastrUnit.getLokationId()).isEqualTo(DEFAULT_LOKATION_ID);
        assertThat(testMastrUnit.getLokationMastrNr()).isEqualTo(DEFAULT_LOKATION_MASTR_NR);
        assertThat(testMastrUnit.getNetzbetreiberId()).isEqualTo(DEFAULT_NETZBETREIBER_ID);
        assertThat(testMastrUnit.getNetzbetreiberMaskedNamen()).isEqualTo(UPDATED_NETZBETREIBER_MASKED_NAMEN);
        assertThat(testMastrUnit.getNetzbetreiberMastrNummer()).isEqualTo(UPDATED_NETZBETREIBER_MASTR_NUMMER);
        assertThat(testMastrUnit.getNetzbetreiberNamen()).isEqualTo(DEFAULT_NETZBETREIBER_NAMEN);
        assertThat(testMastrUnit.getNetzbetreiberPersonenArt()).isEqualTo(UPDATED_NETZBETREIBER_PERSONEN_ART);
        assertThat(testMastrUnit.getSystemStatusId()).isEqualTo(UPDATED_SYSTEM_STATUS_ID);
        assertThat(testMastrUnit.getSystemStatusName()).isEqualTo(UPDATED_SYSTEM_STATUS_NAME);
        assertThat(testMastrUnit.getTyp()).isEqualTo(UPDATED_TYP);
        assertThat(testMastrUnit.getAktenzeichenGenehmigung()).isEqualTo(UPDATED_AKTENZEICHEN_GENEHMIGUNG);
        assertThat(testMastrUnit.getAnzahlSolarmodule()).isEqualTo(DEFAULT_ANZAHL_SOLARMODULE);
        assertThat(testMastrUnit.getBatterieTechnologie()).isEqualTo(UPDATED_BATTERIE_TECHNOLOGIE);
        assertThat(testMastrUnit.getBruttoLeistung()).isEqualTo(DEFAULT_BRUTTO_LEISTUNG);
        assertThat(testMastrUnit.getEegInstallierteLeistung()).isEqualTo(UPDATED_EEG_INSTALLIERTE_LEISTUNG);
        assertThat(testMastrUnit.getEegAnlageMastrNummer()).isEqualTo(UPDATED_EEG_ANLAGE_MASTR_NUMMER);
        assertThat(testMastrUnit.getEegAnlagenSchluessel()).isEqualTo(UPDATED_EEG_ANLAGEN_SCHLUESSEL);
        assertThat(testMastrUnit.getEegZuschlag()).isEqualTo(DEFAULT_EEG_ZUSCHLAG);
        assertThat(testMastrUnit.getZuschlagsNummern()).isEqualTo(DEFAULT_ZUSCHLAGS_NUMMERN);
        assertThat(testMastrUnit.getEnergieTraegerId()).isEqualTo(DEFAULT_ENERGIE_TRAEGER_ID);
        assertThat(testMastrUnit.getEnergieTraegerName()).isEqualTo(UPDATED_ENERGIE_TRAEGER_NAME);
        assertThat(testMastrUnit.getGemeinsamerWechselrichter()).isEqualTo(DEFAULT_GEMEINSAMER_WECHSELRICHTER);
        assertThat(testMastrUnit.getGenehmigungBehoerde()).isEqualTo(UPDATED_GENEHMIGUNG_BEHOERDE);
        assertThat(testMastrUnit.getGenehmigungsMastrNummer()).isEqualTo(UPDATED_GENEHMIGUNGS_MASTR_NUMMER);
        assertThat(testMastrUnit.getGruppierungsObjekte()).isEqualTo(UPDATED_GRUPPIERUNGS_OBJEKTE);
        assertThat(testMastrUnit.getGruppierungsObjekteIds()).isEqualTo(DEFAULT_GRUPPIERUNGS_OBJEKTE_IDS);
        assertThat(testMastrUnit.getHatFlexibilitaetsPraemie()).isEqualTo(DEFAULT_HAT_FLEXIBILITAETS_PRAEMIE);
        assertThat(testMastrUnit.getHauptAusrichtungSolarmodule()).isEqualTo(DEFAULT_HAUPT_AUSRICHTUNG_SOLARMODULE);
        assertThat(testMastrUnit.getHauptAusrichtungSolarmoduleBezeichnung()).isEqualTo(UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG);
        assertThat(testMastrUnit.getHauptBrennstoffId()).isEqualTo(UPDATED_HAUPT_BRENNSTOFF_ID);
        assertThat(testMastrUnit.getHauptBrennstoffNamen()).isEqualTo(UPDATED_HAUPT_BRENNSTOFF_NAMEN);
        assertThat(testMastrUnit.getHauptNeigungswinkelSolarModule()).isEqualTo(DEFAULT_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);
        assertThat(testMastrUnit.getHerstellerWindenergieanlageId()).isEqualTo(UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID);
        assertThat(testMastrUnit.getHerstellerWindenergieanlageBezeichnung()).isEqualTo(DEFAULT_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG);
        assertThat(testMastrUnit.getKwkAnlageElektrischeLeistung()).isEqualTo(DEFAULT_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);
        assertThat(testMastrUnit.getKwkAnlageMastrNummer()).isEqualTo(UPDATED_KWK_ANLAGE_MASTR_NUMMER);
        assertThat(testMastrUnit.getKwkZuschlag()).isEqualTo(DEFAULT_KWK_ZUSCHLAG);
        assertThat(testMastrUnit.getLageEinheit()).isEqualTo(UPDATED_LAGE_EINHEIT);
        assertThat(testMastrUnit.getLageEinheitBezeichnung()).isEqualTo(DEFAULT_LAGE_EINHEIT_BEZEICHNUNG);
        assertThat(testMastrUnit.getLeistungsBegrenzung()).isEqualTo(DEFAULT_LEISTUNGS_BEGRENZUNG);
        assertThat(testMastrUnit.getNabenhoeheWindenergieanlage()).isEqualTo(UPDATED_NABENHOEHE_WINDENERGIEANLAGE);
        assertThat(testMastrUnit.getRotorDurchmesserWindenergieanlage()).isEqualTo(DEFAULT_ROTOR_DURCHMESSER_WINDENERGIEANLAGE);
        assertThat(testMastrUnit.getNettoNennLeistung()).isEqualTo(DEFAULT_NETTO_NENN_LEISTUNG);
        assertThat(testMastrUnit.getNutzbareSpeicherKapazitaet()).isEqualTo(UPDATED_NUTZBARE_SPEICHER_KAPAZITAET);
        assertThat(testMastrUnit.getNutzungsBereichGebsa()).isEqualTo(DEFAULT_NUTZUNGS_BEREICH_GEBSA);
        assertThat(testMastrUnit.getStandortAnonymisiert()).isEqualTo(DEFAULT_STANDORT_ANONYMISIERT);
        assertThat(testMastrUnit.getSpannungsEbenenId()).isEqualTo(UPDATED_SPANNUNGS_EBENEN_ID);
        assertThat(testMastrUnit.getSpannungsEbenenNamen()).isEqualTo(DEFAULT_SPANNUNGS_EBENEN_NAMEN);
        assertThat(testMastrUnit.getSpeicherEinheitMastrNummer()).isEqualTo(DEFAULT_SPEICHER_EINHEIT_MASTR_NUMMER);
        assertThat(testMastrUnit.getTechnologieStromerzeugungId()).isEqualTo(UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID);
        assertThat(testMastrUnit.getTechnologieStromerzeugung()).isEqualTo(DEFAULT_TECHNOLOGIE_STROMERZEUGUNG);
        assertThat(testMastrUnit.getThermischeNutzLeistung()).isEqualTo(UPDATED_THERMISCHE_NUTZ_LEISTUNG);
        assertThat(testMastrUnit.getTypenBezeichnung()).isEqualTo(DEFAULT_TYPEN_BEZEICHNUNG);
        assertThat(testMastrUnit.getVollTeilEinspeisung()).isEqualTo(DEFAULT_VOLL_TEIL_EINSPEISUNG);
        assertThat(testMastrUnit.getVollTeilEinspeisungBezeichnung()).isEqualTo(UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG);
        assertThat(testMastrUnit.getWindparkName()).isEqualTo(UPDATED_WINDPARK_NAME);
        assertThat(testMastrUnit.getGeometry()).isEqualTo(DEFAULT_GEOMETRY);
    }

    @Test
    @Transactional
    void fullUpdateMastrUnitWithPatch() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        int databaseSizeBeforeUpdate = mastrUnitRepository.findAll().size();

        // Update the mastrUnit using partial update
        MastrUnit partialUpdatedMastrUnit = new MastrUnit();
        partialUpdatedMastrUnit.setId(mastrUnit.getId());

        partialUpdatedMastrUnit
            .mastrNummer(UPDATED_MASTR_NUMMER)
            .anlagenBetreiberId(UPDATED_ANLAGEN_BETREIBER_ID)
            .anlagenBetreiberPersonenArt(UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART)
            .anlagenBetreiberMaskedName(UPDATED_ANLAGEN_BETREIBER_MASKED_NAME)
            .anlagenBetreiberMastrNummer(UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER)
            .anlagenBetreiberName(UPDATED_ANLAGEN_BETREIBER_NAME)
            .betriebsStatusId(UPDATED_BETRIEBS_STATUS_ID)
            .betriebsStatusName(UPDATED_BETRIEBS_STATUS_NAME)
            .bundesland(UPDATED_BUNDESLAND)
            .bundeslandId(UPDATED_BUNDESLAND_ID)
            .landkreisId(UPDATED_LANDKREIS_ID)
            .gemeindeId(UPDATED_GEMEINDE_ID)
            .datumLetzteAktualisierung(UPDATED_DATUM_LETZTE_AKTUALISIERUNG)
            .einheitRegistrierungsDatum(UPDATED_EINHEIT_REGISTRIERUNGS_DATUM)
            .endgueltigeStilllegungDatum(UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM)
            .geplantesInbetriebsnahmeDatum(UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM)
            .inbetriebnahmeDatum(UPDATED_INBETRIEBNAHME_DATUM)
            .kwkAnlageInbetriebnahmeDatum(UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM)
            .kwkAnlageRegistrierungsDatum(UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM)
            .eegInbetriebnahmeDatum(UPDATED_EEG_INBETRIEBNAHME_DATUM)
            .eegAnlageRegistrierungsDatum(UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM)
            .genehmigungDatum(UPDATED_GENEHMIGUNG_DATUM)
            .genehmigungRegistrierungsDatum(UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM)
            .isNbPruefungAbgeschlossen(UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN)
            .isAnonymisiert(UPDATED_IS_ANONYMISIERT)
            .isBuergerEnergie(UPDATED_IS_BUERGER_ENERGIE)
            .isEinheitNotstromaggregat(UPDATED_IS_EINHEIT_NOTSTROMAGGREGAT)
            .isMieterstromAngemeldet(UPDATED_IS_MIETERSTROM_ANGEMELDET)
            .isWasserkraftErtuechtigung(UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG)
            .isPilotWindanlage(UPDATED_IS_PILOT_WINDANLAGE)
            .isPrototypAnlage(UPDATED_IS_PROTOTYP_ANLAGE)
            .lat(UPDATED_LAT)
            .lng(UPDATED_LNG)
            .ort(UPDATED_ORT)
            .plz(UPDATED_PLZ)
            .strasse(UPDATED_STRASSE)
            .hausnummer(UPDATED_HAUSNUMMER)
            .einheitname(UPDATED_EINHEITNAME)
            .flurstueck(UPDATED_FLURSTUECK)
            .gemarkung(UPDATED_GEMARKUNG)
            .gemeinde(UPDATED_GEMEINDE)
            .landId(UPDATED_LAND_ID)
            .landkreis(UPDATED_LANDKREIS)
            .ags(UPDATED_AGS)
            .lokationId(UPDATED_LOKATION_ID)
            .lokationMastrNr(UPDATED_LOKATION_MASTR_NR)
            .netzbetreiberId(UPDATED_NETZBETREIBER_ID)
            .netzbetreiberMaskedNamen(UPDATED_NETZBETREIBER_MASKED_NAMEN)
            .netzbetreiberMastrNummer(UPDATED_NETZBETREIBER_MASTR_NUMMER)
            .netzbetreiberNamen(UPDATED_NETZBETREIBER_NAMEN)
            .netzbetreiberPersonenArt(UPDATED_NETZBETREIBER_PERSONEN_ART)
            .systemStatusId(UPDATED_SYSTEM_STATUS_ID)
            .systemStatusName(UPDATED_SYSTEM_STATUS_NAME)
            .typ(UPDATED_TYP)
            .aktenzeichenGenehmigung(UPDATED_AKTENZEICHEN_GENEHMIGUNG)
            .anzahlSolarmodule(UPDATED_ANZAHL_SOLARMODULE)
            .batterieTechnologie(UPDATED_BATTERIE_TECHNOLOGIE)
            .bruttoLeistung(UPDATED_BRUTTO_LEISTUNG)
            .eegInstallierteLeistung(UPDATED_EEG_INSTALLIERTE_LEISTUNG)
            .eegAnlageMastrNummer(UPDATED_EEG_ANLAGE_MASTR_NUMMER)
            .eegAnlagenSchluessel(UPDATED_EEG_ANLAGEN_SCHLUESSEL)
            .eegZuschlag(UPDATED_EEG_ZUSCHLAG)
            .zuschlagsNummern(UPDATED_ZUSCHLAGS_NUMMERN)
            .energieTraegerId(UPDATED_ENERGIE_TRAEGER_ID)
            .energieTraegerName(UPDATED_ENERGIE_TRAEGER_NAME)
            .gemeinsamerWechselrichter(UPDATED_GEMEINSAMER_WECHSELRICHTER)
            .genehmigungBehoerde(UPDATED_GENEHMIGUNG_BEHOERDE)
            .genehmigungsMastrNummer(UPDATED_GENEHMIGUNGS_MASTR_NUMMER)
            .gruppierungsObjekte(UPDATED_GRUPPIERUNGS_OBJEKTE)
            .gruppierungsObjekteIds(UPDATED_GRUPPIERUNGS_OBJEKTE_IDS)
            .hatFlexibilitaetsPraemie(UPDATED_HAT_FLEXIBILITAETS_PRAEMIE)
            .hauptAusrichtungSolarmodule(UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE)
            .hauptAusrichtungSolarmoduleBezeichnung(UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG)
            .hauptBrennstoffId(UPDATED_HAUPT_BRENNSTOFF_ID)
            .hauptBrennstoffNamen(UPDATED_HAUPT_BRENNSTOFF_NAMEN)
            .hauptNeigungswinkelSolarModule(UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE)
            .herstellerWindenergieanlageId(UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID)
            .herstellerWindenergieanlageBezeichnung(UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG)
            .kwkAnlageElektrischeLeistung(UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG)
            .kwkAnlageMastrNummer(UPDATED_KWK_ANLAGE_MASTR_NUMMER)
            .kwkZuschlag(UPDATED_KWK_ZUSCHLAG)
            .lageEinheit(UPDATED_LAGE_EINHEIT)
            .lageEinheitBezeichnung(UPDATED_LAGE_EINHEIT_BEZEICHNUNG)
            .leistungsBegrenzung(UPDATED_LEISTUNGS_BEGRENZUNG)
            .nabenhoeheWindenergieanlage(UPDATED_NABENHOEHE_WINDENERGIEANLAGE)
            .rotorDurchmesserWindenergieanlage(UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE)
            .nettoNennLeistung(UPDATED_NETTO_NENN_LEISTUNG)
            .nutzbareSpeicherKapazitaet(UPDATED_NUTZBARE_SPEICHER_KAPAZITAET)
            .nutzungsBereichGebsa(UPDATED_NUTZUNGS_BEREICH_GEBSA)
            .standortAnonymisiert(UPDATED_STANDORT_ANONYMISIERT)
            .spannungsEbenenId(UPDATED_SPANNUNGS_EBENEN_ID)
            .spannungsEbenenNamen(UPDATED_SPANNUNGS_EBENEN_NAMEN)
            .speicherEinheitMastrNummer(UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER)
            .technologieStromerzeugungId(UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID)
            .technologieStromerzeugung(UPDATED_TECHNOLOGIE_STROMERZEUGUNG)
            .thermischeNutzLeistung(UPDATED_THERMISCHE_NUTZ_LEISTUNG)
            .typenBezeichnung(UPDATED_TYPEN_BEZEICHNUNG)
            .vollTeilEinspeisung(UPDATED_VOLL_TEIL_EINSPEISUNG)
            .vollTeilEinspeisungBezeichnung(UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG)
            .windparkName(UPDATED_WINDPARK_NAME)
            .geometry(UPDATED_GEOMETRY);

        restMastrUnitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMastrUnit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMastrUnit))
            )
            .andExpect(status().isOk());

        // Validate the MastrUnit in the database
        List<MastrUnit> mastrUnitList = mastrUnitRepository.findAll();
        assertThat(mastrUnitList).hasSize(databaseSizeBeforeUpdate);
        MastrUnit testMastrUnit = mastrUnitList.get(mastrUnitList.size() - 1);
        assertThat(testMastrUnit.getMastrNummer()).isEqualTo(UPDATED_MASTR_NUMMER);
        assertThat(testMastrUnit.getAnlagenBetreiberId()).isEqualTo(UPDATED_ANLAGEN_BETREIBER_ID);
        assertThat(testMastrUnit.getAnlagenBetreiberPersonenArt()).isEqualTo(UPDATED_ANLAGEN_BETREIBER_PERSONEN_ART);
        assertThat(testMastrUnit.getAnlagenBetreiberMaskedName()).isEqualTo(UPDATED_ANLAGEN_BETREIBER_MASKED_NAME);
        assertThat(testMastrUnit.getAnlagenBetreiberMastrNummer()).isEqualTo(UPDATED_ANLAGEN_BETREIBER_MASTR_NUMMER);
        assertThat(testMastrUnit.getAnlagenBetreiberName()).isEqualTo(UPDATED_ANLAGEN_BETREIBER_NAME);
        assertThat(testMastrUnit.getBetriebsStatusId()).isEqualTo(UPDATED_BETRIEBS_STATUS_ID);
        assertThat(testMastrUnit.getBetriebsStatusName()).isEqualTo(UPDATED_BETRIEBS_STATUS_NAME);
        assertThat(testMastrUnit.getBundesland()).isEqualTo(UPDATED_BUNDESLAND);
        assertThat(testMastrUnit.getBundeslandId()).isEqualTo(UPDATED_BUNDESLAND_ID);
        assertThat(testMastrUnit.getLandkreisId()).isEqualTo(UPDATED_LANDKREIS_ID);
        assertThat(testMastrUnit.getGemeindeId()).isEqualTo(UPDATED_GEMEINDE_ID);
        assertThat(testMastrUnit.getDatumLetzteAktualisierung()).isEqualTo(UPDATED_DATUM_LETZTE_AKTUALISIERUNG);
        assertThat(testMastrUnit.getEinheitRegistrierungsDatum()).isEqualTo(UPDATED_EINHEIT_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getEndgueltigeStilllegungDatum()).isEqualTo(UPDATED_ENDGUELTIGE_STILLLEGUNG_DATUM);
        assertThat(testMastrUnit.getGeplantesInbetriebsnahmeDatum()).isEqualTo(UPDATED_GEPLANTES_INBETRIEBSNAHME_DATUM);
        assertThat(testMastrUnit.getInbetriebnahmeDatum()).isEqualTo(UPDATED_INBETRIEBNAHME_DATUM);
        assertThat(testMastrUnit.getKwkAnlageInbetriebnahmeDatum()).isEqualTo(UPDATED_KWK_ANLAGE_INBETRIEBNAHME_DATUM);
        assertThat(testMastrUnit.getKwkAnlageRegistrierungsDatum()).isEqualTo(UPDATED_KWK_ANLAGE_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getEegInbetriebnahmeDatum()).isEqualTo(UPDATED_EEG_INBETRIEBNAHME_DATUM);
        assertThat(testMastrUnit.getEegAnlageRegistrierungsDatum()).isEqualTo(UPDATED_EEG_ANLAGE_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getGenehmigungDatum()).isEqualTo(UPDATED_GENEHMIGUNG_DATUM);
        assertThat(testMastrUnit.getGenehmigungRegistrierungsDatum()).isEqualTo(UPDATED_GENEHMIGUNG_REGISTRIERUNGS_DATUM);
        assertThat(testMastrUnit.getIsNbPruefungAbgeschlossen()).isEqualTo(UPDATED_IS_NB_PRUEFUNG_ABGESCHLOSSEN);
        assertThat(testMastrUnit.getIsAnonymisiert()).isEqualTo(UPDATED_IS_ANONYMISIERT);
        assertThat(testMastrUnit.getIsBuergerEnergie()).isEqualTo(UPDATED_IS_BUERGER_ENERGIE);
        assertThat(testMastrUnit.getIsEinheitNotstromaggregat()).isEqualTo(UPDATED_IS_EINHEIT_NOTSTROMAGGREGAT);
        assertThat(testMastrUnit.getIsMieterstromAngemeldet()).isEqualTo(UPDATED_IS_MIETERSTROM_ANGEMELDET);
        assertThat(testMastrUnit.getIsWasserkraftErtuechtigung()).isEqualTo(UPDATED_IS_WASSERKRAFT_ERTUECHTIGUNG);
        assertThat(testMastrUnit.getIsPilotWindanlage()).isEqualTo(UPDATED_IS_PILOT_WINDANLAGE);
        assertThat(testMastrUnit.getIsPrototypAnlage()).isEqualTo(UPDATED_IS_PROTOTYP_ANLAGE);
        assertThat(testMastrUnit.getLat()).isEqualTo(UPDATED_LAT);
        assertThat(testMastrUnit.getLng()).isEqualTo(UPDATED_LNG);
        assertThat(testMastrUnit.getOrt()).isEqualTo(UPDATED_ORT);
        assertThat(testMastrUnit.getPlz()).isEqualTo(UPDATED_PLZ);
        assertThat(testMastrUnit.getStrasse()).isEqualTo(UPDATED_STRASSE);
        assertThat(testMastrUnit.getHausnummer()).isEqualTo(UPDATED_HAUSNUMMER);
        assertThat(testMastrUnit.getEinheitname()).isEqualTo(UPDATED_EINHEITNAME);
        assertThat(testMastrUnit.getFlurstueck()).isEqualTo(UPDATED_FLURSTUECK);
        assertThat(testMastrUnit.getGemarkung()).isEqualTo(UPDATED_GEMARKUNG);
        assertThat(testMastrUnit.getGemeinde()).isEqualTo(UPDATED_GEMEINDE);
        assertThat(testMastrUnit.getLandId()).isEqualTo(UPDATED_LAND_ID);
        assertThat(testMastrUnit.getLandkreis()).isEqualTo(UPDATED_LANDKREIS);
        assertThat(testMastrUnit.getAgs()).isEqualTo(UPDATED_AGS);
        assertThat(testMastrUnit.getLokationId()).isEqualTo(UPDATED_LOKATION_ID);
        assertThat(testMastrUnit.getLokationMastrNr()).isEqualTo(UPDATED_LOKATION_MASTR_NR);
        assertThat(testMastrUnit.getNetzbetreiberId()).isEqualTo(UPDATED_NETZBETREIBER_ID);
        assertThat(testMastrUnit.getNetzbetreiberMaskedNamen()).isEqualTo(UPDATED_NETZBETREIBER_MASKED_NAMEN);
        assertThat(testMastrUnit.getNetzbetreiberMastrNummer()).isEqualTo(UPDATED_NETZBETREIBER_MASTR_NUMMER);
        assertThat(testMastrUnit.getNetzbetreiberNamen()).isEqualTo(UPDATED_NETZBETREIBER_NAMEN);
        assertThat(testMastrUnit.getNetzbetreiberPersonenArt()).isEqualTo(UPDATED_NETZBETREIBER_PERSONEN_ART);
        assertThat(testMastrUnit.getSystemStatusId()).isEqualTo(UPDATED_SYSTEM_STATUS_ID);
        assertThat(testMastrUnit.getSystemStatusName()).isEqualTo(UPDATED_SYSTEM_STATUS_NAME);
        assertThat(testMastrUnit.getTyp()).isEqualTo(UPDATED_TYP);
        assertThat(testMastrUnit.getAktenzeichenGenehmigung()).isEqualTo(UPDATED_AKTENZEICHEN_GENEHMIGUNG);
        assertThat(testMastrUnit.getAnzahlSolarmodule()).isEqualTo(UPDATED_ANZAHL_SOLARMODULE);
        assertThat(testMastrUnit.getBatterieTechnologie()).isEqualTo(UPDATED_BATTERIE_TECHNOLOGIE);
        assertThat(testMastrUnit.getBruttoLeistung()).isEqualTo(UPDATED_BRUTTO_LEISTUNG);
        assertThat(testMastrUnit.getEegInstallierteLeistung()).isEqualTo(UPDATED_EEG_INSTALLIERTE_LEISTUNG);
        assertThat(testMastrUnit.getEegAnlageMastrNummer()).isEqualTo(UPDATED_EEG_ANLAGE_MASTR_NUMMER);
        assertThat(testMastrUnit.getEegAnlagenSchluessel()).isEqualTo(UPDATED_EEG_ANLAGEN_SCHLUESSEL);
        assertThat(testMastrUnit.getEegZuschlag()).isEqualTo(UPDATED_EEG_ZUSCHLAG);
        assertThat(testMastrUnit.getZuschlagsNummern()).isEqualTo(UPDATED_ZUSCHLAGS_NUMMERN);
        assertThat(testMastrUnit.getEnergieTraegerId()).isEqualTo(UPDATED_ENERGIE_TRAEGER_ID);
        assertThat(testMastrUnit.getEnergieTraegerName()).isEqualTo(UPDATED_ENERGIE_TRAEGER_NAME);
        assertThat(testMastrUnit.getGemeinsamerWechselrichter()).isEqualTo(UPDATED_GEMEINSAMER_WECHSELRICHTER);
        assertThat(testMastrUnit.getGenehmigungBehoerde()).isEqualTo(UPDATED_GENEHMIGUNG_BEHOERDE);
        assertThat(testMastrUnit.getGenehmigungsMastrNummer()).isEqualTo(UPDATED_GENEHMIGUNGS_MASTR_NUMMER);
        assertThat(testMastrUnit.getGruppierungsObjekte()).isEqualTo(UPDATED_GRUPPIERUNGS_OBJEKTE);
        assertThat(testMastrUnit.getGruppierungsObjekteIds()).isEqualTo(UPDATED_GRUPPIERUNGS_OBJEKTE_IDS);
        assertThat(testMastrUnit.getHatFlexibilitaetsPraemie()).isEqualTo(UPDATED_HAT_FLEXIBILITAETS_PRAEMIE);
        assertThat(testMastrUnit.getHauptAusrichtungSolarmodule()).isEqualTo(UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE);
        assertThat(testMastrUnit.getHauptAusrichtungSolarmoduleBezeichnung()).isEqualTo(UPDATED_HAUPT_AUSRICHTUNG_SOLARMODULE_BEZEICHNUNG);
        assertThat(testMastrUnit.getHauptBrennstoffId()).isEqualTo(UPDATED_HAUPT_BRENNSTOFF_ID);
        assertThat(testMastrUnit.getHauptBrennstoffNamen()).isEqualTo(UPDATED_HAUPT_BRENNSTOFF_NAMEN);
        assertThat(testMastrUnit.getHauptNeigungswinkelSolarModule()).isEqualTo(UPDATED_HAUPT_NEIGUNGSWINKEL_SOLAR_MODULE);
        assertThat(testMastrUnit.getHerstellerWindenergieanlageId()).isEqualTo(UPDATED_HERSTELLER_WINDENERGIEANLAGE_ID);
        assertThat(testMastrUnit.getHerstellerWindenergieanlageBezeichnung()).isEqualTo(UPDATED_HERSTELLER_WINDENERGIEANLAGE_BEZEICHNUNG);
        assertThat(testMastrUnit.getKwkAnlageElektrischeLeistung()).isEqualTo(UPDATED_KWK_ANLAGE_ELEKTRISCHE_LEISTUNG);
        assertThat(testMastrUnit.getKwkAnlageMastrNummer()).isEqualTo(UPDATED_KWK_ANLAGE_MASTR_NUMMER);
        assertThat(testMastrUnit.getKwkZuschlag()).isEqualTo(UPDATED_KWK_ZUSCHLAG);
        assertThat(testMastrUnit.getLageEinheit()).isEqualTo(UPDATED_LAGE_EINHEIT);
        assertThat(testMastrUnit.getLageEinheitBezeichnung()).isEqualTo(UPDATED_LAGE_EINHEIT_BEZEICHNUNG);
        assertThat(testMastrUnit.getLeistungsBegrenzung()).isEqualTo(UPDATED_LEISTUNGS_BEGRENZUNG);
        assertThat(testMastrUnit.getNabenhoeheWindenergieanlage()).isEqualTo(UPDATED_NABENHOEHE_WINDENERGIEANLAGE);
        assertThat(testMastrUnit.getRotorDurchmesserWindenergieanlage()).isEqualTo(UPDATED_ROTOR_DURCHMESSER_WINDENERGIEANLAGE);
        assertThat(testMastrUnit.getNettoNennLeistung()).isEqualTo(UPDATED_NETTO_NENN_LEISTUNG);
        assertThat(testMastrUnit.getNutzbareSpeicherKapazitaet()).isEqualTo(UPDATED_NUTZBARE_SPEICHER_KAPAZITAET);
        assertThat(testMastrUnit.getNutzungsBereichGebsa()).isEqualTo(UPDATED_NUTZUNGS_BEREICH_GEBSA);
        assertThat(testMastrUnit.getStandortAnonymisiert()).isEqualTo(UPDATED_STANDORT_ANONYMISIERT);
        assertThat(testMastrUnit.getSpannungsEbenenId()).isEqualTo(UPDATED_SPANNUNGS_EBENEN_ID);
        assertThat(testMastrUnit.getSpannungsEbenenNamen()).isEqualTo(UPDATED_SPANNUNGS_EBENEN_NAMEN);
        assertThat(testMastrUnit.getSpeicherEinheitMastrNummer()).isEqualTo(UPDATED_SPEICHER_EINHEIT_MASTR_NUMMER);
        assertThat(testMastrUnit.getTechnologieStromerzeugungId()).isEqualTo(UPDATED_TECHNOLOGIE_STROMERZEUGUNG_ID);
        assertThat(testMastrUnit.getTechnologieStromerzeugung()).isEqualTo(UPDATED_TECHNOLOGIE_STROMERZEUGUNG);
        assertThat(testMastrUnit.getThermischeNutzLeistung()).isEqualTo(UPDATED_THERMISCHE_NUTZ_LEISTUNG);
        assertThat(testMastrUnit.getTypenBezeichnung()).isEqualTo(UPDATED_TYPEN_BEZEICHNUNG);
        assertThat(testMastrUnit.getVollTeilEinspeisung()).isEqualTo(UPDATED_VOLL_TEIL_EINSPEISUNG);
        assertThat(testMastrUnit.getVollTeilEinspeisungBezeichnung()).isEqualTo(UPDATED_VOLL_TEIL_EINSPEISUNG_BEZEICHNUNG);
        assertThat(testMastrUnit.getWindparkName()).isEqualTo(UPDATED_WINDPARK_NAME);
        assertThat(testMastrUnit.getGeometry()).isEqualTo(UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void patchNonExistingMastrUnit() throws Exception {
        int databaseSizeBeforeUpdate = mastrUnitRepository.findAll().size();
        mastrUnit.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMastrUnitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, mastrUnit.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(mastrUnit))
            )
            .andExpect(status().isBadRequest());

        // Validate the MastrUnit in the database
        List<MastrUnit> mastrUnitList = mastrUnitRepository.findAll();
        assertThat(mastrUnitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMastrUnit() throws Exception {
        int databaseSizeBeforeUpdate = mastrUnitRepository.findAll().size();
        mastrUnit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMastrUnitMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(mastrUnit))
            )
            .andExpect(status().isBadRequest());

        // Validate the MastrUnit in the database
        List<MastrUnit> mastrUnitList = mastrUnitRepository.findAll();
        assertThat(mastrUnitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMastrUnit() throws Exception {
        int databaseSizeBeforeUpdate = mastrUnitRepository.findAll().size();
        mastrUnit.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMastrUnitMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(mastrUnit))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MastrUnit in the database
        List<MastrUnit> mastrUnitList = mastrUnitRepository.findAll();
        assertThat(mastrUnitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMastrUnit() throws Exception {
        // Initialize the database
        mastrUnitRepository.saveAndFlush(mastrUnit);

        int databaseSizeBeforeDelete = mastrUnitRepository.findAll().size();

        // Delete the mastrUnit
        restMastrUnitMockMvc
            .perform(delete(ENTITY_API_URL_ID, mastrUnit.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MastrUnit> mastrUnitList = mastrUnitRepository.findAll();
        assertThat(mastrUnitList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
