package eu.danielgerber.web.rest;

import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import eu.danielgerber.IntegrationTest;
import eu.danielgerber.domain.AdministrationUnit;
import eu.danielgerber.domain.ChargingStation;
import eu.danielgerber.domain.Statistic;
import eu.danielgerber.repository.StatisticRepository;
import jakarta.persistence.EntityManager;

/**
 * Integration tests for the {@link StatisticResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class StatisticResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_KEY = "AAAAAAAAAA";
    private static final String UPDATED_KEY = "BBBBBBBBBB";

    private static final Double DEFAULT_VALUE = 1D;
    private static final Double UPDATED_VALUE = 2D;
    private static final Double SMALLER_VALUE = 1D - 1D;

    private static final String DEFAULT_UNIT = "AAAAAAAAAA";
    private static final String UPDATED_UNIT = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_SOURCE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/statistics";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private StatisticRepository statisticRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restStatisticMockMvc;

    private Statistic statistic;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Statistic createEntity(EntityManager em) {
        Statistic statistic = new Statistic()
            .name(DEFAULT_NAME)
            .key(DEFAULT_KEY)
            .value(DEFAULT_VALUE)
            .unit(DEFAULT_UNIT)
            .date(DEFAULT_DATE)
            .source(DEFAULT_SOURCE);
        return statistic;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Statistic createUpdatedEntity(EntityManager em) {
        Statistic statistic = new Statistic()
            .name(UPDATED_NAME)
            .key(UPDATED_KEY)
            .value(UPDATED_VALUE)
            .unit(UPDATED_UNIT)
            .date(UPDATED_DATE)
            .source(UPDATED_SOURCE);
        return statistic;
    }

    @BeforeEach
    public void initTest() {
        statistic = createEntity(em);
    }

    @Test
    @Transactional
    void getAllStatistics() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList
        restStatisticMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(statistic.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].key").value(hasItem(DEFAULT_KEY)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE)));
    }

    @Test
    @Transactional
    void getStatistic() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get the statistic
        restStatisticMockMvc
            .perform(get(ENTITY_API_URL_ID, statistic.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(statistic.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.key").value(DEFAULT_KEY))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.doubleValue()))
            .andExpect(jsonPath("$.unit").value(DEFAULT_UNIT))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.source").value(DEFAULT_SOURCE));
    }

    @Test
    @Transactional
    void getStatisticsByIdFiltering() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        Long id = statistic.getId();

        defaultStatisticShouldBeFound("id.equals=" + id);
        defaultStatisticShouldNotBeFound("id.notEquals=" + id);

        defaultStatisticShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultStatisticShouldNotBeFound("id.greaterThan=" + id);

        defaultStatisticShouldBeFound("id.lessThanOrEqual=" + id);
        defaultStatisticShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllStatisticsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where name equals to DEFAULT_NAME
        defaultStatisticShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the statisticList where name equals to UPDATED_NAME
        defaultStatisticShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllStatisticsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where name in DEFAULT_NAME or UPDATED_NAME
        defaultStatisticShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the statisticList where name equals to UPDATED_NAME
        defaultStatisticShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllStatisticsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where name is not null
        defaultStatisticShouldBeFound("name.specified=true");

        // Get all the statisticList where name is null
        defaultStatisticShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllStatisticsByNameContainsSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where name contains DEFAULT_NAME
        defaultStatisticShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the statisticList where name contains UPDATED_NAME
        defaultStatisticShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllStatisticsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where name does not contain DEFAULT_NAME
        defaultStatisticShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the statisticList where name does not contain UPDATED_NAME
        defaultStatisticShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllStatisticsByKeyIsEqualToSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where key equals to DEFAULT_KEY
        defaultStatisticShouldBeFound("key.equals=" + DEFAULT_KEY);

        // Get all the statisticList where key equals to UPDATED_KEY
        defaultStatisticShouldNotBeFound("key.equals=" + UPDATED_KEY);
    }

    @Test
    @Transactional
    void getAllStatisticsByKeyIsInShouldWork() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where key in DEFAULT_KEY or UPDATED_KEY
        defaultStatisticShouldBeFound("key.in=" + DEFAULT_KEY + "," + UPDATED_KEY);

        // Get all the statisticList where key equals to UPDATED_KEY
        defaultStatisticShouldNotBeFound("key.in=" + UPDATED_KEY);
    }

    @Test
    @Transactional
    void getAllStatisticsByKeyIsNullOrNotNull() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where key is not null
        defaultStatisticShouldBeFound("key.specified=true");

        // Get all the statisticList where key is null
        defaultStatisticShouldNotBeFound("key.specified=false");
    }

    @Test
    @Transactional
    void getAllStatisticsByKeyContainsSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where key contains DEFAULT_KEY
        defaultStatisticShouldBeFound("key.contains=" + DEFAULT_KEY);

        // Get all the statisticList where key contains UPDATED_KEY
        defaultStatisticShouldNotBeFound("key.contains=" + UPDATED_KEY);
    }

    @Test
    @Transactional
    void getAllStatisticsByKeyNotContainsSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where key does not contain DEFAULT_KEY
        defaultStatisticShouldNotBeFound("key.doesNotContain=" + DEFAULT_KEY);

        // Get all the statisticList where key does not contain UPDATED_KEY
        defaultStatisticShouldBeFound("key.doesNotContain=" + UPDATED_KEY);
    }

    @Test
    @Transactional
    void getAllStatisticsByValueIsEqualToSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where value equals to DEFAULT_VALUE
        defaultStatisticShouldBeFound("value.equals=" + DEFAULT_VALUE);

        // Get all the statisticList where value equals to UPDATED_VALUE
        defaultStatisticShouldNotBeFound("value.equals=" + UPDATED_VALUE);
    }

    @Test
    @Transactional
    void getAllStatisticsByValueIsInShouldWork() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where value in DEFAULT_VALUE or UPDATED_VALUE
        defaultStatisticShouldBeFound("value.in=" + DEFAULT_VALUE + "," + UPDATED_VALUE);

        // Get all the statisticList where value equals to UPDATED_VALUE
        defaultStatisticShouldNotBeFound("value.in=" + UPDATED_VALUE);
    }

    @Test
    @Transactional
    void getAllStatisticsByValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where value is not null
        defaultStatisticShouldBeFound("value.specified=true");

        // Get all the statisticList where value is null
        defaultStatisticShouldNotBeFound("value.specified=false");
    }

    @Test
    @Transactional
    void getAllStatisticsByValueIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where value is greater than or equal to DEFAULT_VALUE
        defaultStatisticShouldBeFound("value.greaterThanOrEqual=" + DEFAULT_VALUE);

        // Get all the statisticList where value is greater than or equal to UPDATED_VALUE
        defaultStatisticShouldNotBeFound("value.greaterThanOrEqual=" + UPDATED_VALUE);
    }

    @Test
    @Transactional
    void getAllStatisticsByValueIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where value is less than or equal to DEFAULT_VALUE
        defaultStatisticShouldBeFound("value.lessThanOrEqual=" + DEFAULT_VALUE);

        // Get all the statisticList where value is less than or equal to SMALLER_VALUE
        defaultStatisticShouldNotBeFound("value.lessThanOrEqual=" + SMALLER_VALUE);
    }

    @Test
    @Transactional
    void getAllStatisticsByValueIsLessThanSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where value is less than DEFAULT_VALUE
        defaultStatisticShouldNotBeFound("value.lessThan=" + DEFAULT_VALUE);

        // Get all the statisticList where value is less than UPDATED_VALUE
        defaultStatisticShouldBeFound("value.lessThan=" + UPDATED_VALUE);
    }

    @Test
    @Transactional
    void getAllStatisticsByValueIsGreaterThanSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where value is greater than DEFAULT_VALUE
        defaultStatisticShouldNotBeFound("value.greaterThan=" + DEFAULT_VALUE);

        // Get all the statisticList where value is greater than SMALLER_VALUE
        defaultStatisticShouldBeFound("value.greaterThan=" + SMALLER_VALUE);
    }

    @Test
    @Transactional
    void getAllStatisticsByUnitIsEqualToSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where unit equals to DEFAULT_UNIT
        defaultStatisticShouldBeFound("unit.equals=" + DEFAULT_UNIT);

        // Get all the statisticList where unit equals to UPDATED_UNIT
        defaultStatisticShouldNotBeFound("unit.equals=" + UPDATED_UNIT);
    }

    @Test
    @Transactional
    void getAllStatisticsByUnitIsInShouldWork() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where unit in DEFAULT_UNIT or UPDATED_UNIT
        defaultStatisticShouldBeFound("unit.in=" + DEFAULT_UNIT + "," + UPDATED_UNIT);

        // Get all the statisticList where unit equals to UPDATED_UNIT
        defaultStatisticShouldNotBeFound("unit.in=" + UPDATED_UNIT);
    }

    @Test
    @Transactional
    void getAllStatisticsByUnitIsNullOrNotNull() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where unit is not null
        defaultStatisticShouldBeFound("unit.specified=true");

        // Get all the statisticList where unit is null
        defaultStatisticShouldNotBeFound("unit.specified=false");
    }

    @Test
    @Transactional
    void getAllStatisticsByUnitContainsSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where unit contains DEFAULT_UNIT
        defaultStatisticShouldBeFound("unit.contains=" + DEFAULT_UNIT);

        // Get all the statisticList where unit contains UPDATED_UNIT
        defaultStatisticShouldNotBeFound("unit.contains=" + UPDATED_UNIT);
    }

    @Test
    @Transactional
    void getAllStatisticsByUnitNotContainsSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where unit does not contain DEFAULT_UNIT
        defaultStatisticShouldNotBeFound("unit.doesNotContain=" + DEFAULT_UNIT);

        // Get all the statisticList where unit does not contain UPDATED_UNIT
        defaultStatisticShouldBeFound("unit.doesNotContain=" + UPDATED_UNIT);
    }

    @Test
    @Transactional
    void getAllStatisticsByDateIsEqualToSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where date equals to DEFAULT_DATE
        defaultStatisticShouldBeFound("date.equals=" + DEFAULT_DATE);

        // Get all the statisticList where date equals to UPDATED_DATE
        defaultStatisticShouldNotBeFound("date.equals=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllStatisticsByDateIsInShouldWork() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where date in DEFAULT_DATE or UPDATED_DATE
        defaultStatisticShouldBeFound("date.in=" + DEFAULT_DATE + "," + UPDATED_DATE);

        // Get all the statisticList where date equals to UPDATED_DATE
        defaultStatisticShouldNotBeFound("date.in=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllStatisticsByDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where date is not null
        defaultStatisticShouldBeFound("date.specified=true");

        // Get all the statisticList where date is null
        defaultStatisticShouldNotBeFound("date.specified=false");
    }

    @Test
    @Transactional
    void getAllStatisticsByDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where date is greater than or equal to DEFAULT_DATE
        defaultStatisticShouldBeFound("date.greaterThanOrEqual=" + DEFAULT_DATE);

        // Get all the statisticList where date is greater than or equal to UPDATED_DATE
        defaultStatisticShouldNotBeFound("date.greaterThanOrEqual=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllStatisticsByDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where date is less than or equal to DEFAULT_DATE
        defaultStatisticShouldBeFound("date.lessThanOrEqual=" + DEFAULT_DATE);

        // Get all the statisticList where date is less than or equal to SMALLER_DATE
        defaultStatisticShouldNotBeFound("date.lessThanOrEqual=" + SMALLER_DATE);
    }

    @Test
    @Transactional
    void getAllStatisticsByDateIsLessThanSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where date is less than DEFAULT_DATE
        defaultStatisticShouldNotBeFound("date.lessThan=" + DEFAULT_DATE);

        // Get all the statisticList where date is less than UPDATED_DATE
        defaultStatisticShouldBeFound("date.lessThan=" + UPDATED_DATE);
    }

    @Test
    @Transactional
    void getAllStatisticsByDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where date is greater than DEFAULT_DATE
        defaultStatisticShouldNotBeFound("date.greaterThan=" + DEFAULT_DATE);

        // Get all the statisticList where date is greater than SMALLER_DATE
        defaultStatisticShouldBeFound("date.greaterThan=" + SMALLER_DATE);
    }

    @Test
    @Transactional
    void getAllStatisticsBySourceIsEqualToSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where source equals to DEFAULT_SOURCE
        defaultStatisticShouldBeFound("source.equals=" + DEFAULT_SOURCE);

        // Get all the statisticList where source equals to UPDATED_SOURCE
        defaultStatisticShouldNotBeFound("source.equals=" + UPDATED_SOURCE);
    }

    @Test
    @Transactional
    void getAllStatisticsBySourceIsInShouldWork() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where source in DEFAULT_SOURCE or UPDATED_SOURCE
        defaultStatisticShouldBeFound("source.in=" + DEFAULT_SOURCE + "," + UPDATED_SOURCE);

        // Get all the statisticList where source equals to UPDATED_SOURCE
        defaultStatisticShouldNotBeFound("source.in=" + UPDATED_SOURCE);
    }

    @Test
    @Transactional
    void getAllStatisticsBySourceIsNullOrNotNull() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where source is not null
        defaultStatisticShouldBeFound("source.specified=true");

        // Get all the statisticList where source is null
        defaultStatisticShouldNotBeFound("source.specified=false");
    }

    @Test
    @Transactional
    void getAllStatisticsBySourceContainsSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where source contains DEFAULT_SOURCE
        defaultStatisticShouldBeFound("source.contains=" + DEFAULT_SOURCE);

        // Get all the statisticList where source contains UPDATED_SOURCE
        defaultStatisticShouldNotBeFound("source.contains=" + UPDATED_SOURCE);
    }

    @Test
    @Transactional
    void getAllStatisticsBySourceNotContainsSomething() throws Exception {
        // Initialize the database
        statisticRepository.saveAndFlush(statistic);

        // Get all the statisticList where source does not contain DEFAULT_SOURCE
        defaultStatisticShouldNotBeFound("source.doesNotContain=" + DEFAULT_SOURCE);

        // Get all the statisticList where source does not contain UPDATED_SOURCE
        defaultStatisticShouldBeFound("source.doesNotContain=" + UPDATED_SOURCE);
    }

    @Test
    @Transactional
    void getAllStatisticsByAdministrationUnitIsEqualToSomething() throws Exception {
        AdministrationUnit administrationUnit;
        if (TestUtil.findAll(em, AdministrationUnit.class).isEmpty()) {
            statisticRepository.saveAndFlush(statistic);
            administrationUnit = AdministrationUnitResourceIT.createEntity(em);
        } else {
            administrationUnit = TestUtil.findAll(em, AdministrationUnit.class).get(0);
        }
        em.persist(administrationUnit);
        em.flush();
        statistic.setAdministrationUnit(administrationUnit);
        statisticRepository.saveAndFlush(statistic);
        Long administrationUnitId = administrationUnit.getId();

        // Get all the statisticList where administrationUnit equals to administrationUnitId
        defaultStatisticShouldBeFound("administrationUnitId.equals=" + administrationUnitId);

        // Get all the statisticList where administrationUnit equals to (administrationUnitId + 1)
        defaultStatisticShouldNotBeFound("administrationUnitId.equals=" + (administrationUnitId + 1));
    }

    @Test
    @Transactional
    void getAllStatisticsByChargingStationIsEqualToSomething() throws Exception {
        ChargingStation chargingStation;
        if (TestUtil.findAll(em, ChargingStation.class).isEmpty()) {
            statisticRepository.saveAndFlush(statistic);
            chargingStation = ChargingStationResourceIT.createEntity(em);
        } else {
            chargingStation = TestUtil.findAll(em, ChargingStation.class).get(0);
        }
        em.persist(chargingStation);
        em.flush();
        statistic.setChargingStation(chargingStation);
        statisticRepository.saveAndFlush(statistic);
        Long chargingStationId = chargingStation.getId();

        // Get all the statisticList where chargingStation equals to chargingStationId
        defaultStatisticShouldBeFound("chargingStationId.equals=" + chargingStationId);

        // Get all the statisticList where chargingStation equals to (chargingStationId + 1)
        defaultStatisticShouldNotBeFound("chargingStationId.equals=" + (chargingStationId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultStatisticShouldBeFound(String filter) throws Exception {
        restStatisticMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(statistic.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].key").value(hasItem(DEFAULT_KEY)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].unit").value(hasItem(DEFAULT_UNIT)))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE)));

        // Check, that the count call also returns 1
        restStatisticMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultStatisticShouldNotBeFound(String filter) throws Exception {
        restStatisticMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restStatisticMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingStatistic() throws Exception {
        // Get the statistic
        restStatisticMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }
}
