package eu.danielgerber.web.rest;

import static org.geolatte.geom.builder.DSL.g;
import static org.geolatte.geom.builder.DSL.point;
import static org.geolatte.geom.crs.CoordinateReferenceSystems.WGS84;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import eu.danielgerber.IntegrationTest;
import eu.danielgerber.domain.AdministrationUnit;
import eu.danielgerber.domain.ChargingStation;
import eu.danielgerber.domain.MastrUnit;
import eu.danielgerber.domain.Statistic;
import eu.danielgerber.repository.AdministrationUnitRepository;
import org.geolatte.geom.jts.JTS;
import org.locationtech.jts.geom.Geometry;
import java.time.LocalDate;
import java.time.ZoneId;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AdministrationUnitResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AdministrationUnitResourceIT {

    private static final String DEFAULT_OBJEKTIDENTIFIKATOR = "AAAAAAAAAA";
    private static final String UPDATED_OBJEKTIDENTIFIKATOR = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_BEGINNLEBENSZEIT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BEGINNLEBENSZEIT = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_BEGINNLEBENSZEIT = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_ADMIN_EBENE_ADE = "AAAAAAAAAA";
    private static final String UPDATED_ADMIN_EBENE_ADE = "BBBBBBBBBB";

    private static final Integer DEFAULT_ADE = 1;
    private static final Integer UPDATED_ADE = 2;
    private static final Integer SMALLER_ADE = 1 - 1;

    private static final String DEFAULT_GEOFAKTOR_GF = "AAAAAAAAAA";
    private static final String UPDATED_GEOFAKTOR_GF = "BBBBBBBBBB";

    private static final Long DEFAULT_GF = 1L;
    private static final Long UPDATED_GF = 2L;
    private static final Long SMALLER_GF = 1L - 1L;

    private static final String DEFAULT_BESONDERE_GEBIETE_BSG = "AAAAAAAAAA";
    private static final String UPDATED_BESONDERE_GEBIETE_BSG = "BBBBBBBBBB";

    private static final Long DEFAULT_BSG = 1L;
    private static final Long UPDATED_BSG = 2L;
    private static final Long SMALLER_BSG = 1L - 1L;

    private static final String DEFAULT_REGIONALSCHLUESSEL_ARS = "AAAAAAAAAA";
    private static final String UPDATED_REGIONALSCHLUESSEL_ARS = "BBBBBBBBBB";

    private static final String DEFAULT_GEMEINDESCHLUESSEL_AGS = "AAAAAAAAAA";
    private static final String UPDATED_GEMEINDESCHLUESSEL_AGS = "BBBBBBBBBB";

    private static final String DEFAULT_VERWALTUNGSSITZ_SDV_ARS = "AAAAAAAAAA";
    private static final String UPDATED_VERWALTUNGSSITZ_SDV_ARS = "BBBBBBBBBB";

    private static final String DEFAULT_GEOGRAFISCHERNAME_GEN = "AAAAAAAAAA";
    private static final String UPDATED_GEOGRAFISCHERNAME_GEN = "BBBBBBBBBB";

    private static final String DEFAULT_BEZEICHNUNG = "AAAAAAAAAA";
    private static final String UPDATED_BEZEICHNUNG = "BBBBBBBBBB";

    private static final Long DEFAULT_IDENTIFIKATOR_IBZ = 1L;
    private static final Long UPDATED_IDENTIFIKATOR_IBZ = 2L;
    private static final Long SMALLER_IDENTIFIKATOR_IBZ = 1L - 1L;

    private static final String DEFAULT_BEMERKUNG = "AAAAAAAAAA";
    private static final String UPDATED_BEMERKUNG = "BBBBBBBBBB";

    private static final String DEFAULT_NAMENSBILDUNG_NBD = "AAAAAAAAAA";
    private static final String UPDATED_NAMENSBILDUNG_NBD = "BBBBBBBBBB";

    private static final Boolean DEFAULT_NBD = false;
    private static final Boolean UPDATED_NBD = true;

    private static final String DEFAULT_LAND = "AAAAAAAAAA";
    private static final String UPDATED_LAND = "BBBBBBBBBB";

    private static final String DEFAULT_REGIERUNGSBEZIRK = "AAAAAAAAAA";
    private static final String UPDATED_REGIERUNGSBEZIRK = "BBBBBBBBBB";

    private static final String DEFAULT_KREIS = "AAAAAAAAAA";
    private static final String UPDATED_KREIS = "BBBBBBBBBB";

    private static final String DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1 = "AAAAAAAAAA";
    private static final String UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_1 = "BBBBBBBBBB";

    private static final String DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2 = "AAAAAAAAAA";
    private static final String UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_2 = "BBBBBBBBBB";

    private static final String DEFAULT_GEMEINDE = "AAAAAAAAAA";
    private static final String UPDATED_GEMEINDE = "BBBBBBBBBB";

    private static final String DEFAULT_FUNK_SCHLUESSELSTELLE_3 = "AAAAAAAAAA";
    private static final String UPDATED_FUNK_SCHLUESSELSTELLE_3 = "BBBBBBBBBB";

    private static final String DEFAULT_FK_S_3 = "AAAAAAAAAA";
    private static final String UPDATED_FK_S_3 = "BBBBBBBBBB";

    private static final String DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS = "AAAAAAAAAA";
    private static final String UPDATED_EUROP_STATISTIKSCHLUESSEL_NUTS = "BBBBBBBBBB";

    private static final String DEFAULT_REGIOSCHLUESSELAUFGEFUELLT = "AAAAAAAAAA";
    private static final String UPDATED_REGIOSCHLUESSELAUFGEFUELLT = "BBBBBBBBBB";

    private static final String DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT = "AAAAAAAAAA";
    private static final String UPDATED_GEMEINDESCHLUESSELAUFGEFUELLT = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_WIRKSAMKEIT_WSK = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_WIRKSAMKEIT_WSK = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_WIRKSAMKEIT_WSK = LocalDate.ofEpochDay(-1L);

    private static final Geometry DEFAULT_GEOMETRY = JTS.to(point(WGS84,g(4.33,53.21)));
    private static final Geometry UPDATED_GEOMETRY = JTS.to(point(WGS84,g(5.33,54.21)));

    private static final String ENTITY_API_URL = "/api/administration-units";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private AdministrationUnitRepository administrationUnitRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAdministrationUnitMockMvc;

    private AdministrationUnit administrationUnit;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdministrationUnit createEntity(EntityManager em) {
        AdministrationUnit administrationUnit = new AdministrationUnit()
            .objektidentifikator(DEFAULT_OBJEKTIDENTIFIKATOR)
            .beginnlebenszeit(DEFAULT_BEGINNLEBENSZEIT)
            .adminEbeneAde(DEFAULT_ADMIN_EBENE_ADE)
            .ade(DEFAULT_ADE)
            .geofaktorGf(DEFAULT_GEOFAKTOR_GF)
            .gf(DEFAULT_GF)
            .besondereGebieteBsg(DEFAULT_BESONDERE_GEBIETE_BSG)
            .bsg(DEFAULT_BSG)
            .regionalschluesselArs(DEFAULT_REGIONALSCHLUESSEL_ARS)
            .gemeindeschluesselAgs(DEFAULT_GEMEINDESCHLUESSEL_AGS)
            .verwaltungssitzSdvArs(DEFAULT_VERWALTUNGSSITZ_SDV_ARS)
            .geografischernameGen(DEFAULT_GEOGRAFISCHERNAME_GEN)
            .bezeichnung(DEFAULT_BEZEICHNUNG)
            .identifikatorIbz(DEFAULT_IDENTIFIKATOR_IBZ)
            .bemerkung(DEFAULT_BEMERKUNG)
            .namensbildungNbd(DEFAULT_NAMENSBILDUNG_NBD)
            .nbd(DEFAULT_NBD)
            .land(DEFAULT_LAND)
            .regierungsbezirk(DEFAULT_REGIERUNGSBEZIRK)
            .kreis(DEFAULT_KREIS)
            .verwaltungsgemeinschaftteil1(DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1)
            .verwaltungsgemeinschaftteil2(DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2)
            .gemeinde(DEFAULT_GEMEINDE)
            .funkSchluesselstelle3(DEFAULT_FUNK_SCHLUESSELSTELLE_3)
            .fkS3(DEFAULT_FK_S_3)
            .europStatistikschluesselNuts(DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS)
            .regioschluesselaufgefuellt(DEFAULT_REGIOSCHLUESSELAUFGEFUELLT)
            .gemeindeschluesselaufgefuellt(DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT)
            .wirksamkeitWsk(DEFAULT_WIRKSAMKEIT_WSK)
            .geometry(DEFAULT_GEOMETRY);
        return administrationUnit;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdministrationUnit createUpdatedEntity(EntityManager em) {
        AdministrationUnit administrationUnit = new AdministrationUnit()
            .objektidentifikator(UPDATED_OBJEKTIDENTIFIKATOR)
            .beginnlebenszeit(UPDATED_BEGINNLEBENSZEIT)
            .adminEbeneAde(UPDATED_ADMIN_EBENE_ADE)
            .ade(UPDATED_ADE)
            .geofaktorGf(UPDATED_GEOFAKTOR_GF)
            .gf(UPDATED_GF)
            .besondereGebieteBsg(UPDATED_BESONDERE_GEBIETE_BSG)
            .bsg(UPDATED_BSG)
            .regionalschluesselArs(UPDATED_REGIONALSCHLUESSEL_ARS)
            .gemeindeschluesselAgs(UPDATED_GEMEINDESCHLUESSEL_AGS)
            .verwaltungssitzSdvArs(UPDATED_VERWALTUNGSSITZ_SDV_ARS)
            .geografischernameGen(UPDATED_GEOGRAFISCHERNAME_GEN)
            .bezeichnung(UPDATED_BEZEICHNUNG)
            .identifikatorIbz(UPDATED_IDENTIFIKATOR_IBZ)
            .bemerkung(UPDATED_BEMERKUNG)
            .namensbildungNbd(UPDATED_NAMENSBILDUNG_NBD)
            .nbd(UPDATED_NBD)
            .land(UPDATED_LAND)
            .regierungsbezirk(UPDATED_REGIERUNGSBEZIRK)
            .kreis(UPDATED_KREIS)
            .verwaltungsgemeinschaftteil1(UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_1)
            .verwaltungsgemeinschaftteil2(UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_2)
            .gemeinde(UPDATED_GEMEINDE)
            .funkSchluesselstelle3(UPDATED_FUNK_SCHLUESSELSTELLE_3)
            .fkS3(UPDATED_FK_S_3)
            .europStatistikschluesselNuts(UPDATED_EUROP_STATISTIKSCHLUESSEL_NUTS)
            .regioschluesselaufgefuellt(UPDATED_REGIOSCHLUESSELAUFGEFUELLT)
            .gemeindeschluesselaufgefuellt(UPDATED_GEMEINDESCHLUESSELAUFGEFUELLT)
            .wirksamkeitWsk(UPDATED_WIRKSAMKEIT_WSK)
            .geometry(UPDATED_GEOMETRY);
        return administrationUnit;
    }

    @BeforeEach
    public void initTest() {
        administrationUnit = createEntity(em);
    }

    @Test
    @Transactional
    void getAllAdministrationUnits() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList
        restAdministrationUnitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(administrationUnit.getId().intValue())))
            .andExpect(jsonPath("$.[*].objektidentifikator").value(hasItem(DEFAULT_OBJEKTIDENTIFIKATOR)))
            .andExpect(jsonPath("$.[*].beginnlebenszeit").value(hasItem(DEFAULT_BEGINNLEBENSZEIT.toString())))
            .andExpect(jsonPath("$.[*].adminEbeneAde").value(hasItem(DEFAULT_ADMIN_EBENE_ADE)))
            .andExpect(jsonPath("$.[*].ade").value(hasItem(DEFAULT_ADE)))
            .andExpect(jsonPath("$.[*].geofaktorGf").value(hasItem(DEFAULT_GEOFAKTOR_GF)))
            .andExpect(jsonPath("$.[*].gf").value(hasItem(DEFAULT_GF.intValue())))
            .andExpect(jsonPath("$.[*].besondereGebieteBsg").value(hasItem(DEFAULT_BESONDERE_GEBIETE_BSG)))
            .andExpect(jsonPath("$.[*].bsg").value(hasItem(DEFAULT_BSG.intValue())))
            .andExpect(jsonPath("$.[*].regionalschluesselArs").value(hasItem(DEFAULT_REGIONALSCHLUESSEL_ARS)))
            .andExpect(jsonPath("$.[*].gemeindeschluesselAgs").value(hasItem(DEFAULT_GEMEINDESCHLUESSEL_AGS)))
            .andExpect(jsonPath("$.[*].verwaltungssitzSdvArs").value(hasItem(DEFAULT_VERWALTUNGSSITZ_SDV_ARS)))
            .andExpect(jsonPath("$.[*].geografischernameGen").value(hasItem(DEFAULT_GEOGRAFISCHERNAME_GEN)))
            .andExpect(jsonPath("$.[*].bezeichnung").value(hasItem(DEFAULT_BEZEICHNUNG)))
            .andExpect(jsonPath("$.[*].identifikatorIbz").value(hasItem(DEFAULT_IDENTIFIKATOR_IBZ.intValue())))
            .andExpect(jsonPath("$.[*].bemerkung").value(hasItem(DEFAULT_BEMERKUNG)))
            .andExpect(jsonPath("$.[*].namensbildungNbd").value(hasItem(DEFAULT_NAMENSBILDUNG_NBD)))
            .andExpect(jsonPath("$.[*].nbd").value(hasItem(DEFAULT_NBD.booleanValue())))
            .andExpect(jsonPath("$.[*].land").value(hasItem(DEFAULT_LAND)))
            .andExpect(jsonPath("$.[*].regierungsbezirk").value(hasItem(DEFAULT_REGIERUNGSBEZIRK)))
            .andExpect(jsonPath("$.[*].kreis").value(hasItem(DEFAULT_KREIS)))
            .andExpect(jsonPath("$.[*].verwaltungsgemeinschaftteil1").value(hasItem(DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1)))
            .andExpect(jsonPath("$.[*].verwaltungsgemeinschaftteil2").value(hasItem(DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2)))
            .andExpect(jsonPath("$.[*].gemeinde").value(hasItem(DEFAULT_GEMEINDE)))
            .andExpect(jsonPath("$.[*].funkSchluesselstelle3").value(hasItem(DEFAULT_FUNK_SCHLUESSELSTELLE_3)))
            .andExpect(jsonPath("$.[*].fkS3").value(hasItem(DEFAULT_FK_S_3)))
            .andExpect(jsonPath("$.[*].europStatistikschluesselNuts").value(hasItem(DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS)))
            .andExpect(jsonPath("$.[*].regioschluesselaufgefuellt").value(hasItem(DEFAULT_REGIOSCHLUESSELAUFGEFUELLT)))
            .andExpect(jsonPath("$.[*].gemeindeschluesselaufgefuellt").value(hasItem(DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT)))
            .andExpect(jsonPath("$.[*].wirksamkeitWsk").value(hasItem(DEFAULT_WIRKSAMKEIT_WSK.toString())))
            .andExpect(jsonPath("$.[*].geometry").value(hasItem(DEFAULT_GEOMETRY)));
    }

    @Test
    @Transactional
    void getAdministrationUnit() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get the administrationUnit
        restAdministrationUnitMockMvc
            .perform(get(ENTITY_API_URL_ID, administrationUnit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(administrationUnit.getId().intValue()))
            .andExpect(jsonPath("$.objektidentifikator").value(DEFAULT_OBJEKTIDENTIFIKATOR))
            .andExpect(jsonPath("$.beginnlebenszeit").value(DEFAULT_BEGINNLEBENSZEIT.toString()))
            .andExpect(jsonPath("$.adminEbeneAde").value(DEFAULT_ADMIN_EBENE_ADE))
            .andExpect(jsonPath("$.ade").value(DEFAULT_ADE))
            .andExpect(jsonPath("$.geofaktorGf").value(DEFAULT_GEOFAKTOR_GF))
            .andExpect(jsonPath("$.gf").value(DEFAULT_GF.intValue()))
            .andExpect(jsonPath("$.besondereGebieteBsg").value(DEFAULT_BESONDERE_GEBIETE_BSG))
            .andExpect(jsonPath("$.bsg").value(DEFAULT_BSG.intValue()))
            .andExpect(jsonPath("$.regionalschluesselArs").value(DEFAULT_REGIONALSCHLUESSEL_ARS))
            .andExpect(jsonPath("$.gemeindeschluesselAgs").value(DEFAULT_GEMEINDESCHLUESSEL_AGS))
            .andExpect(jsonPath("$.verwaltungssitzSdvArs").value(DEFAULT_VERWALTUNGSSITZ_SDV_ARS))
            .andExpect(jsonPath("$.geografischernameGen").value(DEFAULT_GEOGRAFISCHERNAME_GEN))
            .andExpect(jsonPath("$.bezeichnung").value(DEFAULT_BEZEICHNUNG))
            .andExpect(jsonPath("$.identifikatorIbz").value(DEFAULT_IDENTIFIKATOR_IBZ.intValue()))
            .andExpect(jsonPath("$.bemerkung").value(DEFAULT_BEMERKUNG))
            .andExpect(jsonPath("$.namensbildungNbd").value(DEFAULT_NAMENSBILDUNG_NBD))
            .andExpect(jsonPath("$.nbd").value(DEFAULT_NBD.booleanValue()))
            .andExpect(jsonPath("$.land").value(DEFAULT_LAND))
            .andExpect(jsonPath("$.regierungsbezirk").value(DEFAULT_REGIERUNGSBEZIRK))
            .andExpect(jsonPath("$.kreis").value(DEFAULT_KREIS))
            .andExpect(jsonPath("$.verwaltungsgemeinschaftteil1").value(DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1))
            .andExpect(jsonPath("$.verwaltungsgemeinschaftteil2").value(DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2))
            .andExpect(jsonPath("$.gemeinde").value(DEFAULT_GEMEINDE))
            .andExpect(jsonPath("$.funkSchluesselstelle3").value(DEFAULT_FUNK_SCHLUESSELSTELLE_3))
            .andExpect(jsonPath("$.fkS3").value(DEFAULT_FK_S_3))
            .andExpect(jsonPath("$.europStatistikschluesselNuts").value(DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS))
            .andExpect(jsonPath("$.regioschluesselaufgefuellt").value(DEFAULT_REGIOSCHLUESSELAUFGEFUELLT))
            .andExpect(jsonPath("$.gemeindeschluesselaufgefuellt").value(DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT))
            .andExpect(jsonPath("$.wirksamkeitWsk").value(DEFAULT_WIRKSAMKEIT_WSK.toString()))
            .andExpect(jsonPath("$.geometry").value(DEFAULT_GEOMETRY));
    }

    @Test
    @Transactional
    void getAdministrationUnitsByIdFiltering() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        Long id = administrationUnit.getId();

        defaultAdministrationUnitShouldBeFound("id.equals=" + id);
        defaultAdministrationUnitShouldNotBeFound("id.notEquals=" + id);

        defaultAdministrationUnitShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAdministrationUnitShouldNotBeFound("id.greaterThan=" + id);

        defaultAdministrationUnitShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAdministrationUnitShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByObjektidentifikatorIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where objektidentifikator equals to DEFAULT_OBJEKTIDENTIFIKATOR
        defaultAdministrationUnitShouldBeFound("objektidentifikator.equals=" + DEFAULT_OBJEKTIDENTIFIKATOR);

        // Get all the administrationUnitList where objektidentifikator equals to UPDATED_OBJEKTIDENTIFIKATOR
        defaultAdministrationUnitShouldNotBeFound("objektidentifikator.equals=" + UPDATED_OBJEKTIDENTIFIKATOR);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByObjektidentifikatorIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where objektidentifikator in DEFAULT_OBJEKTIDENTIFIKATOR or UPDATED_OBJEKTIDENTIFIKATOR
        defaultAdministrationUnitShouldBeFound("objektidentifikator.in=" + DEFAULT_OBJEKTIDENTIFIKATOR + "," + UPDATED_OBJEKTIDENTIFIKATOR);

        // Get all the administrationUnitList where objektidentifikator equals to UPDATED_OBJEKTIDENTIFIKATOR
        defaultAdministrationUnitShouldNotBeFound("objektidentifikator.in=" + UPDATED_OBJEKTIDENTIFIKATOR);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByObjektidentifikatorIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where objektidentifikator is not null
        defaultAdministrationUnitShouldBeFound("objektidentifikator.specified=true");

        // Get all the administrationUnitList where objektidentifikator is null
        defaultAdministrationUnitShouldNotBeFound("objektidentifikator.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByObjektidentifikatorContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where objektidentifikator contains DEFAULT_OBJEKTIDENTIFIKATOR
        defaultAdministrationUnitShouldBeFound("objektidentifikator.contains=" + DEFAULT_OBJEKTIDENTIFIKATOR);

        // Get all the administrationUnitList where objektidentifikator contains UPDATED_OBJEKTIDENTIFIKATOR
        defaultAdministrationUnitShouldNotBeFound("objektidentifikator.contains=" + UPDATED_OBJEKTIDENTIFIKATOR);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByObjektidentifikatorNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where objektidentifikator does not contain DEFAULT_OBJEKTIDENTIFIKATOR
        defaultAdministrationUnitShouldNotBeFound("objektidentifikator.doesNotContain=" + DEFAULT_OBJEKTIDENTIFIKATOR);

        // Get all the administrationUnitList where objektidentifikator does not contain UPDATED_OBJEKTIDENTIFIKATOR
        defaultAdministrationUnitShouldBeFound("objektidentifikator.doesNotContain=" + UPDATED_OBJEKTIDENTIFIKATOR);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBeginnlebenszeitIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where beginnlebenszeit equals to DEFAULT_BEGINNLEBENSZEIT
        defaultAdministrationUnitShouldBeFound("beginnlebenszeit.equals=" + DEFAULT_BEGINNLEBENSZEIT);

        // Get all the administrationUnitList where beginnlebenszeit equals to UPDATED_BEGINNLEBENSZEIT
        defaultAdministrationUnitShouldNotBeFound("beginnlebenszeit.equals=" + UPDATED_BEGINNLEBENSZEIT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBeginnlebenszeitIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where beginnlebenszeit in DEFAULT_BEGINNLEBENSZEIT or UPDATED_BEGINNLEBENSZEIT
        defaultAdministrationUnitShouldBeFound("beginnlebenszeit.in=" + DEFAULT_BEGINNLEBENSZEIT + "," + UPDATED_BEGINNLEBENSZEIT);

        // Get all the administrationUnitList where beginnlebenszeit equals to UPDATED_BEGINNLEBENSZEIT
        defaultAdministrationUnitShouldNotBeFound("beginnlebenszeit.in=" + UPDATED_BEGINNLEBENSZEIT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBeginnlebenszeitIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where beginnlebenszeit is not null
        defaultAdministrationUnitShouldBeFound("beginnlebenszeit.specified=true");

        // Get all the administrationUnitList where beginnlebenszeit is null
        defaultAdministrationUnitShouldNotBeFound("beginnlebenszeit.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBeginnlebenszeitIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where beginnlebenszeit is greater than or equal to DEFAULT_BEGINNLEBENSZEIT
        defaultAdministrationUnitShouldBeFound("beginnlebenszeit.greaterThanOrEqual=" + DEFAULT_BEGINNLEBENSZEIT);

        // Get all the administrationUnitList where beginnlebenszeit is greater than or equal to UPDATED_BEGINNLEBENSZEIT
        defaultAdministrationUnitShouldNotBeFound("beginnlebenszeit.greaterThanOrEqual=" + UPDATED_BEGINNLEBENSZEIT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBeginnlebenszeitIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where beginnlebenszeit is less than or equal to DEFAULT_BEGINNLEBENSZEIT
        defaultAdministrationUnitShouldBeFound("beginnlebenszeit.lessThanOrEqual=" + DEFAULT_BEGINNLEBENSZEIT);

        // Get all the administrationUnitList where beginnlebenszeit is less than or equal to SMALLER_BEGINNLEBENSZEIT
        defaultAdministrationUnitShouldNotBeFound("beginnlebenszeit.lessThanOrEqual=" + SMALLER_BEGINNLEBENSZEIT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBeginnlebenszeitIsLessThanSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where beginnlebenszeit is less than DEFAULT_BEGINNLEBENSZEIT
        defaultAdministrationUnitShouldNotBeFound("beginnlebenszeit.lessThan=" + DEFAULT_BEGINNLEBENSZEIT);

        // Get all the administrationUnitList where beginnlebenszeit is less than UPDATED_BEGINNLEBENSZEIT
        defaultAdministrationUnitShouldBeFound("beginnlebenszeit.lessThan=" + UPDATED_BEGINNLEBENSZEIT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBeginnlebenszeitIsGreaterThanSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where beginnlebenszeit is greater than DEFAULT_BEGINNLEBENSZEIT
        defaultAdministrationUnitShouldNotBeFound("beginnlebenszeit.greaterThan=" + DEFAULT_BEGINNLEBENSZEIT);

        // Get all the administrationUnitList where beginnlebenszeit is greater than SMALLER_BEGINNLEBENSZEIT
        defaultAdministrationUnitShouldBeFound("beginnlebenszeit.greaterThan=" + SMALLER_BEGINNLEBENSZEIT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByAdminEbeneAdeIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where adminEbeneAde equals to DEFAULT_ADMIN_EBENE_ADE
        defaultAdministrationUnitShouldBeFound("adminEbeneAde.equals=" + DEFAULT_ADMIN_EBENE_ADE);

        // Get all the administrationUnitList where adminEbeneAde equals to UPDATED_ADMIN_EBENE_ADE
        defaultAdministrationUnitShouldNotBeFound("adminEbeneAde.equals=" + UPDATED_ADMIN_EBENE_ADE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByAdminEbeneAdeIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where adminEbeneAde in DEFAULT_ADMIN_EBENE_ADE or UPDATED_ADMIN_EBENE_ADE
        defaultAdministrationUnitShouldBeFound("adminEbeneAde.in=" + DEFAULT_ADMIN_EBENE_ADE + "," + UPDATED_ADMIN_EBENE_ADE);

        // Get all the administrationUnitList where adminEbeneAde equals to UPDATED_ADMIN_EBENE_ADE
        defaultAdministrationUnitShouldNotBeFound("adminEbeneAde.in=" + UPDATED_ADMIN_EBENE_ADE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByAdminEbeneAdeIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where adminEbeneAde is not null
        defaultAdministrationUnitShouldBeFound("adminEbeneAde.specified=true");

        // Get all the administrationUnitList where adminEbeneAde is null
        defaultAdministrationUnitShouldNotBeFound("adminEbeneAde.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByAdminEbeneAdeContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where adminEbeneAde contains DEFAULT_ADMIN_EBENE_ADE
        defaultAdministrationUnitShouldBeFound("adminEbeneAde.contains=" + DEFAULT_ADMIN_EBENE_ADE);

        // Get all the administrationUnitList where adminEbeneAde contains UPDATED_ADMIN_EBENE_ADE
        defaultAdministrationUnitShouldNotBeFound("adminEbeneAde.contains=" + UPDATED_ADMIN_EBENE_ADE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByAdminEbeneAdeNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where adminEbeneAde does not contain DEFAULT_ADMIN_EBENE_ADE
        defaultAdministrationUnitShouldNotBeFound("adminEbeneAde.doesNotContain=" + DEFAULT_ADMIN_EBENE_ADE);

        // Get all the administrationUnitList where adminEbeneAde does not contain UPDATED_ADMIN_EBENE_ADE
        defaultAdministrationUnitShouldBeFound("adminEbeneAde.doesNotContain=" + UPDATED_ADMIN_EBENE_ADE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByAdeIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where ade equals to DEFAULT_ADE
        defaultAdministrationUnitShouldBeFound("ade.equals=" + DEFAULT_ADE);

        // Get all the administrationUnitList where ade equals to UPDATED_ADE
        defaultAdministrationUnitShouldNotBeFound("ade.equals=" + UPDATED_ADE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByAdeIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where ade in DEFAULT_ADE or UPDATED_ADE
        defaultAdministrationUnitShouldBeFound("ade.in=" + DEFAULT_ADE + "," + UPDATED_ADE);

        // Get all the administrationUnitList where ade equals to UPDATED_ADE
        defaultAdministrationUnitShouldNotBeFound("ade.in=" + UPDATED_ADE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByAdeIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where ade is not null
        defaultAdministrationUnitShouldBeFound("ade.specified=true");

        // Get all the administrationUnitList where ade is null
        defaultAdministrationUnitShouldNotBeFound("ade.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByAdeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where ade is greater than or equal to DEFAULT_ADE
        defaultAdministrationUnitShouldBeFound("ade.greaterThanOrEqual=" + DEFAULT_ADE);

        // Get all the administrationUnitList where ade is greater than or equal to UPDATED_ADE
        defaultAdministrationUnitShouldNotBeFound("ade.greaterThanOrEqual=" + UPDATED_ADE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByAdeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where ade is less than or equal to DEFAULT_ADE
        defaultAdministrationUnitShouldBeFound("ade.lessThanOrEqual=" + DEFAULT_ADE);

        // Get all the administrationUnitList where ade is less than or equal to SMALLER_ADE
        defaultAdministrationUnitShouldNotBeFound("ade.lessThanOrEqual=" + SMALLER_ADE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByAdeIsLessThanSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where ade is less than DEFAULT_ADE
        defaultAdministrationUnitShouldNotBeFound("ade.lessThan=" + DEFAULT_ADE);

        // Get all the administrationUnitList where ade is less than UPDATED_ADE
        defaultAdministrationUnitShouldBeFound("ade.lessThan=" + UPDATED_ADE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByAdeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where ade is greater than DEFAULT_ADE
        defaultAdministrationUnitShouldNotBeFound("ade.greaterThan=" + DEFAULT_ADE);

        // Get all the administrationUnitList where ade is greater than SMALLER_ADE
        defaultAdministrationUnitShouldBeFound("ade.greaterThan=" + SMALLER_ADE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeofaktorGfIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geofaktorGf equals to DEFAULT_GEOFAKTOR_GF
        defaultAdministrationUnitShouldBeFound("geofaktorGf.equals=" + DEFAULT_GEOFAKTOR_GF);

        // Get all the administrationUnitList where geofaktorGf equals to UPDATED_GEOFAKTOR_GF
        defaultAdministrationUnitShouldNotBeFound("geofaktorGf.equals=" + UPDATED_GEOFAKTOR_GF);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeofaktorGfIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geofaktorGf in DEFAULT_GEOFAKTOR_GF or UPDATED_GEOFAKTOR_GF
        defaultAdministrationUnitShouldBeFound("geofaktorGf.in=" + DEFAULT_GEOFAKTOR_GF + "," + UPDATED_GEOFAKTOR_GF);

        // Get all the administrationUnitList where geofaktorGf equals to UPDATED_GEOFAKTOR_GF
        defaultAdministrationUnitShouldNotBeFound("geofaktorGf.in=" + UPDATED_GEOFAKTOR_GF);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeofaktorGfIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geofaktorGf is not null
        defaultAdministrationUnitShouldBeFound("geofaktorGf.specified=true");

        // Get all the administrationUnitList where geofaktorGf is null
        defaultAdministrationUnitShouldNotBeFound("geofaktorGf.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeofaktorGfContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geofaktorGf contains DEFAULT_GEOFAKTOR_GF
        defaultAdministrationUnitShouldBeFound("geofaktorGf.contains=" + DEFAULT_GEOFAKTOR_GF);

        // Get all the administrationUnitList where geofaktorGf contains UPDATED_GEOFAKTOR_GF
        defaultAdministrationUnitShouldNotBeFound("geofaktorGf.contains=" + UPDATED_GEOFAKTOR_GF);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeofaktorGfNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geofaktorGf does not contain DEFAULT_GEOFAKTOR_GF
        defaultAdministrationUnitShouldNotBeFound("geofaktorGf.doesNotContain=" + DEFAULT_GEOFAKTOR_GF);

        // Get all the administrationUnitList where geofaktorGf does not contain UPDATED_GEOFAKTOR_GF
        defaultAdministrationUnitShouldBeFound("geofaktorGf.doesNotContain=" + UPDATED_GEOFAKTOR_GF);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGfIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gf equals to DEFAULT_GF
        defaultAdministrationUnitShouldBeFound("gf.equals=" + DEFAULT_GF);

        // Get all the administrationUnitList where gf equals to UPDATED_GF
        defaultAdministrationUnitShouldNotBeFound("gf.equals=" + UPDATED_GF);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGfIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gf in DEFAULT_GF or UPDATED_GF
        defaultAdministrationUnitShouldBeFound("gf.in=" + DEFAULT_GF + "," + UPDATED_GF);

        // Get all the administrationUnitList where gf equals to UPDATED_GF
        defaultAdministrationUnitShouldNotBeFound("gf.in=" + UPDATED_GF);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGfIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gf is not null
        defaultAdministrationUnitShouldBeFound("gf.specified=true");

        // Get all the administrationUnitList where gf is null
        defaultAdministrationUnitShouldNotBeFound("gf.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGfIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gf is greater than or equal to DEFAULT_GF
        defaultAdministrationUnitShouldBeFound("gf.greaterThanOrEqual=" + DEFAULT_GF);

        // Get all the administrationUnitList where gf is greater than or equal to UPDATED_GF
        defaultAdministrationUnitShouldNotBeFound("gf.greaterThanOrEqual=" + UPDATED_GF);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGfIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gf is less than or equal to DEFAULT_GF
        defaultAdministrationUnitShouldBeFound("gf.lessThanOrEqual=" + DEFAULT_GF);

        // Get all the administrationUnitList where gf is less than or equal to SMALLER_GF
        defaultAdministrationUnitShouldNotBeFound("gf.lessThanOrEqual=" + SMALLER_GF);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGfIsLessThanSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gf is less than DEFAULT_GF
        defaultAdministrationUnitShouldNotBeFound("gf.lessThan=" + DEFAULT_GF);

        // Get all the administrationUnitList where gf is less than UPDATED_GF
        defaultAdministrationUnitShouldBeFound("gf.lessThan=" + UPDATED_GF);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGfIsGreaterThanSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gf is greater than DEFAULT_GF
        defaultAdministrationUnitShouldNotBeFound("gf.greaterThan=" + DEFAULT_GF);

        // Get all the administrationUnitList where gf is greater than SMALLER_GF
        defaultAdministrationUnitShouldBeFound("gf.greaterThan=" + SMALLER_GF);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBesondereGebieteBsgIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where besondereGebieteBsg equals to DEFAULT_BESONDERE_GEBIETE_BSG
        defaultAdministrationUnitShouldBeFound("besondereGebieteBsg.equals=" + DEFAULT_BESONDERE_GEBIETE_BSG);

        // Get all the administrationUnitList where besondereGebieteBsg equals to UPDATED_BESONDERE_GEBIETE_BSG
        defaultAdministrationUnitShouldNotBeFound("besondereGebieteBsg.equals=" + UPDATED_BESONDERE_GEBIETE_BSG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBesondereGebieteBsgIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where besondereGebieteBsg in DEFAULT_BESONDERE_GEBIETE_BSG or UPDATED_BESONDERE_GEBIETE_BSG
        defaultAdministrationUnitShouldBeFound(
            "besondereGebieteBsg.in=" + DEFAULT_BESONDERE_GEBIETE_BSG + "," + UPDATED_BESONDERE_GEBIETE_BSG
        );

        // Get all the administrationUnitList where besondereGebieteBsg equals to UPDATED_BESONDERE_GEBIETE_BSG
        defaultAdministrationUnitShouldNotBeFound("besondereGebieteBsg.in=" + UPDATED_BESONDERE_GEBIETE_BSG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBesondereGebieteBsgIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where besondereGebieteBsg is not null
        defaultAdministrationUnitShouldBeFound("besondereGebieteBsg.specified=true");

        // Get all the administrationUnitList where besondereGebieteBsg is null
        defaultAdministrationUnitShouldNotBeFound("besondereGebieteBsg.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBesondereGebieteBsgContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where besondereGebieteBsg contains DEFAULT_BESONDERE_GEBIETE_BSG
        defaultAdministrationUnitShouldBeFound("besondereGebieteBsg.contains=" + DEFAULT_BESONDERE_GEBIETE_BSG);

        // Get all the administrationUnitList where besondereGebieteBsg contains UPDATED_BESONDERE_GEBIETE_BSG
        defaultAdministrationUnitShouldNotBeFound("besondereGebieteBsg.contains=" + UPDATED_BESONDERE_GEBIETE_BSG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBesondereGebieteBsgNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where besondereGebieteBsg does not contain DEFAULT_BESONDERE_GEBIETE_BSG
        defaultAdministrationUnitShouldNotBeFound("besondereGebieteBsg.doesNotContain=" + DEFAULT_BESONDERE_GEBIETE_BSG);

        // Get all the administrationUnitList where besondereGebieteBsg does not contain UPDATED_BESONDERE_GEBIETE_BSG
        defaultAdministrationUnitShouldBeFound("besondereGebieteBsg.doesNotContain=" + UPDATED_BESONDERE_GEBIETE_BSG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBsgIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bsg equals to DEFAULT_BSG
        defaultAdministrationUnitShouldBeFound("bsg.equals=" + DEFAULT_BSG);

        // Get all the administrationUnitList where bsg equals to UPDATED_BSG
        defaultAdministrationUnitShouldNotBeFound("bsg.equals=" + UPDATED_BSG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBsgIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bsg in DEFAULT_BSG or UPDATED_BSG
        defaultAdministrationUnitShouldBeFound("bsg.in=" + DEFAULT_BSG + "," + UPDATED_BSG);

        // Get all the administrationUnitList where bsg equals to UPDATED_BSG
        defaultAdministrationUnitShouldNotBeFound("bsg.in=" + UPDATED_BSG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBsgIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bsg is not null
        defaultAdministrationUnitShouldBeFound("bsg.specified=true");

        // Get all the administrationUnitList where bsg is null
        defaultAdministrationUnitShouldNotBeFound("bsg.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBsgIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bsg is greater than or equal to DEFAULT_BSG
        defaultAdministrationUnitShouldBeFound("bsg.greaterThanOrEqual=" + DEFAULT_BSG);

        // Get all the administrationUnitList where bsg is greater than or equal to UPDATED_BSG
        defaultAdministrationUnitShouldNotBeFound("bsg.greaterThanOrEqual=" + UPDATED_BSG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBsgIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bsg is less than or equal to DEFAULT_BSG
        defaultAdministrationUnitShouldBeFound("bsg.lessThanOrEqual=" + DEFAULT_BSG);

        // Get all the administrationUnitList where bsg is less than or equal to SMALLER_BSG
        defaultAdministrationUnitShouldNotBeFound("bsg.lessThanOrEqual=" + SMALLER_BSG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBsgIsLessThanSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bsg is less than DEFAULT_BSG
        defaultAdministrationUnitShouldNotBeFound("bsg.lessThan=" + DEFAULT_BSG);

        // Get all the administrationUnitList where bsg is less than UPDATED_BSG
        defaultAdministrationUnitShouldBeFound("bsg.lessThan=" + UPDATED_BSG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBsgIsGreaterThanSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bsg is greater than DEFAULT_BSG
        defaultAdministrationUnitShouldNotBeFound("bsg.greaterThan=" + DEFAULT_BSG);

        // Get all the administrationUnitList where bsg is greater than SMALLER_BSG
        defaultAdministrationUnitShouldBeFound("bsg.greaterThan=" + SMALLER_BSG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegionalschluesselArsIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regionalschluesselArs equals to DEFAULT_REGIONALSCHLUESSEL_ARS
        defaultAdministrationUnitShouldBeFound("regionalschluesselArs.equals=" + DEFAULT_REGIONALSCHLUESSEL_ARS);

        // Get all the administrationUnitList where regionalschluesselArs equals to UPDATED_REGIONALSCHLUESSEL_ARS
        defaultAdministrationUnitShouldNotBeFound("regionalschluesselArs.equals=" + UPDATED_REGIONALSCHLUESSEL_ARS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegionalschluesselArsIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regionalschluesselArs in DEFAULT_REGIONALSCHLUESSEL_ARS or UPDATED_REGIONALSCHLUESSEL_ARS
        defaultAdministrationUnitShouldBeFound(
            "regionalschluesselArs.in=" + DEFAULT_REGIONALSCHLUESSEL_ARS + "," + UPDATED_REGIONALSCHLUESSEL_ARS
        );

        // Get all the administrationUnitList where regionalschluesselArs equals to UPDATED_REGIONALSCHLUESSEL_ARS
        defaultAdministrationUnitShouldNotBeFound("regionalschluesselArs.in=" + UPDATED_REGIONALSCHLUESSEL_ARS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegionalschluesselArsIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regionalschluesselArs is not null
        defaultAdministrationUnitShouldBeFound("regionalschluesselArs.specified=true");

        // Get all the administrationUnitList where regionalschluesselArs is null
        defaultAdministrationUnitShouldNotBeFound("regionalschluesselArs.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegionalschluesselArsContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regionalschluesselArs contains DEFAULT_REGIONALSCHLUESSEL_ARS
        defaultAdministrationUnitShouldBeFound("regionalschluesselArs.contains=" + DEFAULT_REGIONALSCHLUESSEL_ARS);

        // Get all the administrationUnitList where regionalschluesselArs contains UPDATED_REGIONALSCHLUESSEL_ARS
        defaultAdministrationUnitShouldNotBeFound("regionalschluesselArs.contains=" + UPDATED_REGIONALSCHLUESSEL_ARS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegionalschluesselArsNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regionalschluesselArs does not contain DEFAULT_REGIONALSCHLUESSEL_ARS
        defaultAdministrationUnitShouldNotBeFound("regionalschluesselArs.doesNotContain=" + DEFAULT_REGIONALSCHLUESSEL_ARS);

        // Get all the administrationUnitList where regionalschluesselArs does not contain UPDATED_REGIONALSCHLUESSEL_ARS
        defaultAdministrationUnitShouldBeFound("regionalschluesselArs.doesNotContain=" + UPDATED_REGIONALSCHLUESSEL_ARS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeschluesselAgsIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeindeschluesselAgs equals to DEFAULT_GEMEINDESCHLUESSEL_AGS
        defaultAdministrationUnitShouldBeFound("gemeindeschluesselAgs.equals=" + DEFAULT_GEMEINDESCHLUESSEL_AGS);

        // Get all the administrationUnitList where gemeindeschluesselAgs equals to UPDATED_GEMEINDESCHLUESSEL_AGS
        defaultAdministrationUnitShouldNotBeFound("gemeindeschluesselAgs.equals=" + UPDATED_GEMEINDESCHLUESSEL_AGS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeschluesselAgsIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeindeschluesselAgs in DEFAULT_GEMEINDESCHLUESSEL_AGS or UPDATED_GEMEINDESCHLUESSEL_AGS
        defaultAdministrationUnitShouldBeFound(
            "gemeindeschluesselAgs.in=" + DEFAULT_GEMEINDESCHLUESSEL_AGS + "," + UPDATED_GEMEINDESCHLUESSEL_AGS
        );

        // Get all the administrationUnitList where gemeindeschluesselAgs equals to UPDATED_GEMEINDESCHLUESSEL_AGS
        defaultAdministrationUnitShouldNotBeFound("gemeindeschluesselAgs.in=" + UPDATED_GEMEINDESCHLUESSEL_AGS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeschluesselAgsIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeindeschluesselAgs is not null
        defaultAdministrationUnitShouldBeFound("gemeindeschluesselAgs.specified=true");

        // Get all the administrationUnitList where gemeindeschluesselAgs is null
        defaultAdministrationUnitShouldNotBeFound("gemeindeschluesselAgs.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeschluesselAgsContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeindeschluesselAgs contains DEFAULT_GEMEINDESCHLUESSEL_AGS
        defaultAdministrationUnitShouldBeFound("gemeindeschluesselAgs.contains=" + DEFAULT_GEMEINDESCHLUESSEL_AGS);

        // Get all the administrationUnitList where gemeindeschluesselAgs contains UPDATED_GEMEINDESCHLUESSEL_AGS
        defaultAdministrationUnitShouldNotBeFound("gemeindeschluesselAgs.contains=" + UPDATED_GEMEINDESCHLUESSEL_AGS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeschluesselAgsNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeindeschluesselAgs does not contain DEFAULT_GEMEINDESCHLUESSEL_AGS
        defaultAdministrationUnitShouldNotBeFound("gemeindeschluesselAgs.doesNotContain=" + DEFAULT_GEMEINDESCHLUESSEL_AGS);

        // Get all the administrationUnitList where gemeindeschluesselAgs does not contain UPDATED_GEMEINDESCHLUESSEL_AGS
        defaultAdministrationUnitShouldBeFound("gemeindeschluesselAgs.doesNotContain=" + UPDATED_GEMEINDESCHLUESSEL_AGS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungssitzSdvArsIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungssitzSdvArs equals to DEFAULT_VERWALTUNGSSITZ_SDV_ARS
        defaultAdministrationUnitShouldBeFound("verwaltungssitzSdvArs.equals=" + DEFAULT_VERWALTUNGSSITZ_SDV_ARS);

        // Get all the administrationUnitList where verwaltungssitzSdvArs equals to UPDATED_VERWALTUNGSSITZ_SDV_ARS
        defaultAdministrationUnitShouldNotBeFound("verwaltungssitzSdvArs.equals=" + UPDATED_VERWALTUNGSSITZ_SDV_ARS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungssitzSdvArsIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungssitzSdvArs in DEFAULT_VERWALTUNGSSITZ_SDV_ARS or UPDATED_VERWALTUNGSSITZ_SDV_ARS
        defaultAdministrationUnitShouldBeFound(
            "verwaltungssitzSdvArs.in=" + DEFAULT_VERWALTUNGSSITZ_SDV_ARS + "," + UPDATED_VERWALTUNGSSITZ_SDV_ARS
        );

        // Get all the administrationUnitList where verwaltungssitzSdvArs equals to UPDATED_VERWALTUNGSSITZ_SDV_ARS
        defaultAdministrationUnitShouldNotBeFound("verwaltungssitzSdvArs.in=" + UPDATED_VERWALTUNGSSITZ_SDV_ARS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungssitzSdvArsIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungssitzSdvArs is not null
        defaultAdministrationUnitShouldBeFound("verwaltungssitzSdvArs.specified=true");

        // Get all the administrationUnitList where verwaltungssitzSdvArs is null
        defaultAdministrationUnitShouldNotBeFound("verwaltungssitzSdvArs.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungssitzSdvArsContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungssitzSdvArs contains DEFAULT_VERWALTUNGSSITZ_SDV_ARS
        defaultAdministrationUnitShouldBeFound("verwaltungssitzSdvArs.contains=" + DEFAULT_VERWALTUNGSSITZ_SDV_ARS);

        // Get all the administrationUnitList where verwaltungssitzSdvArs contains UPDATED_VERWALTUNGSSITZ_SDV_ARS
        defaultAdministrationUnitShouldNotBeFound("verwaltungssitzSdvArs.contains=" + UPDATED_VERWALTUNGSSITZ_SDV_ARS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungssitzSdvArsNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungssitzSdvArs does not contain DEFAULT_VERWALTUNGSSITZ_SDV_ARS
        defaultAdministrationUnitShouldNotBeFound("verwaltungssitzSdvArs.doesNotContain=" + DEFAULT_VERWALTUNGSSITZ_SDV_ARS);

        // Get all the administrationUnitList where verwaltungssitzSdvArs does not contain UPDATED_VERWALTUNGSSITZ_SDV_ARS
        defaultAdministrationUnitShouldBeFound("verwaltungssitzSdvArs.doesNotContain=" + UPDATED_VERWALTUNGSSITZ_SDV_ARS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeografischernameGenIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geografischernameGen equals to DEFAULT_GEOGRAFISCHERNAME_GEN
        defaultAdministrationUnitShouldBeFound("geografischernameGen.equals=" + DEFAULT_GEOGRAFISCHERNAME_GEN);

        // Get all the administrationUnitList where geografischernameGen equals to UPDATED_GEOGRAFISCHERNAME_GEN
        defaultAdministrationUnitShouldNotBeFound("geografischernameGen.equals=" + UPDATED_GEOGRAFISCHERNAME_GEN);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeografischernameGenIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geografischernameGen in DEFAULT_GEOGRAFISCHERNAME_GEN or UPDATED_GEOGRAFISCHERNAME_GEN
        defaultAdministrationUnitShouldBeFound(
            "geografischernameGen.in=" + DEFAULT_GEOGRAFISCHERNAME_GEN + "," + UPDATED_GEOGRAFISCHERNAME_GEN
        );

        // Get all the administrationUnitList where geografischernameGen equals to UPDATED_GEOGRAFISCHERNAME_GEN
        defaultAdministrationUnitShouldNotBeFound("geografischernameGen.in=" + UPDATED_GEOGRAFISCHERNAME_GEN);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeografischernameGenIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geografischernameGen is not null
        defaultAdministrationUnitShouldBeFound("geografischernameGen.specified=true");

        // Get all the administrationUnitList where geografischernameGen is null
        defaultAdministrationUnitShouldNotBeFound("geografischernameGen.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeografischernameGenContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geografischernameGen contains DEFAULT_GEOGRAFISCHERNAME_GEN
        defaultAdministrationUnitShouldBeFound("geografischernameGen.contains=" + DEFAULT_GEOGRAFISCHERNAME_GEN);

        // Get all the administrationUnitList where geografischernameGen contains UPDATED_GEOGRAFISCHERNAME_GEN
        defaultAdministrationUnitShouldNotBeFound("geografischernameGen.contains=" + UPDATED_GEOGRAFISCHERNAME_GEN);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeografischernameGenNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geografischernameGen does not contain DEFAULT_GEOGRAFISCHERNAME_GEN
        defaultAdministrationUnitShouldNotBeFound("geografischernameGen.doesNotContain=" + DEFAULT_GEOGRAFISCHERNAME_GEN);

        // Get all the administrationUnitList where geografischernameGen does not contain UPDATED_GEOGRAFISCHERNAME_GEN
        defaultAdministrationUnitShouldBeFound("geografischernameGen.doesNotContain=" + UPDATED_GEOGRAFISCHERNAME_GEN);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBezeichnungIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bezeichnung equals to DEFAULT_BEZEICHNUNG
        defaultAdministrationUnitShouldBeFound("bezeichnung.equals=" + DEFAULT_BEZEICHNUNG);

        // Get all the administrationUnitList where bezeichnung equals to UPDATED_BEZEICHNUNG
        defaultAdministrationUnitShouldNotBeFound("bezeichnung.equals=" + UPDATED_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBezeichnungIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bezeichnung in DEFAULT_BEZEICHNUNG or UPDATED_BEZEICHNUNG
        defaultAdministrationUnitShouldBeFound("bezeichnung.in=" + DEFAULT_BEZEICHNUNG + "," + UPDATED_BEZEICHNUNG);

        // Get all the administrationUnitList where bezeichnung equals to UPDATED_BEZEICHNUNG
        defaultAdministrationUnitShouldNotBeFound("bezeichnung.in=" + UPDATED_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBezeichnungIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bezeichnung is not null
        defaultAdministrationUnitShouldBeFound("bezeichnung.specified=true");

        // Get all the administrationUnitList where bezeichnung is null
        defaultAdministrationUnitShouldNotBeFound("bezeichnung.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBezeichnungContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bezeichnung contains DEFAULT_BEZEICHNUNG
        defaultAdministrationUnitShouldBeFound("bezeichnung.contains=" + DEFAULT_BEZEICHNUNG);

        // Get all the administrationUnitList where bezeichnung contains UPDATED_BEZEICHNUNG
        defaultAdministrationUnitShouldNotBeFound("bezeichnung.contains=" + UPDATED_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBezeichnungNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bezeichnung does not contain DEFAULT_BEZEICHNUNG
        defaultAdministrationUnitShouldNotBeFound("bezeichnung.doesNotContain=" + DEFAULT_BEZEICHNUNG);

        // Get all the administrationUnitList where bezeichnung does not contain UPDATED_BEZEICHNUNG
        defaultAdministrationUnitShouldBeFound("bezeichnung.doesNotContain=" + UPDATED_BEZEICHNUNG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByIdentifikatorIbzIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where identifikatorIbz equals to DEFAULT_IDENTIFIKATOR_IBZ
        defaultAdministrationUnitShouldBeFound("identifikatorIbz.equals=" + DEFAULT_IDENTIFIKATOR_IBZ);

        // Get all the administrationUnitList where identifikatorIbz equals to UPDATED_IDENTIFIKATOR_IBZ
        defaultAdministrationUnitShouldNotBeFound("identifikatorIbz.equals=" + UPDATED_IDENTIFIKATOR_IBZ);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByIdentifikatorIbzIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where identifikatorIbz in DEFAULT_IDENTIFIKATOR_IBZ or UPDATED_IDENTIFIKATOR_IBZ
        defaultAdministrationUnitShouldBeFound("identifikatorIbz.in=" + DEFAULT_IDENTIFIKATOR_IBZ + "," + UPDATED_IDENTIFIKATOR_IBZ);

        // Get all the administrationUnitList where identifikatorIbz equals to UPDATED_IDENTIFIKATOR_IBZ
        defaultAdministrationUnitShouldNotBeFound("identifikatorIbz.in=" + UPDATED_IDENTIFIKATOR_IBZ);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByIdentifikatorIbzIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where identifikatorIbz is not null
        defaultAdministrationUnitShouldBeFound("identifikatorIbz.specified=true");

        // Get all the administrationUnitList where identifikatorIbz is null
        defaultAdministrationUnitShouldNotBeFound("identifikatorIbz.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByIdentifikatorIbzIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where identifikatorIbz is greater than or equal to DEFAULT_IDENTIFIKATOR_IBZ
        defaultAdministrationUnitShouldBeFound("identifikatorIbz.greaterThanOrEqual=" + DEFAULT_IDENTIFIKATOR_IBZ);

        // Get all the administrationUnitList where identifikatorIbz is greater than or equal to UPDATED_IDENTIFIKATOR_IBZ
        defaultAdministrationUnitShouldNotBeFound("identifikatorIbz.greaterThanOrEqual=" + UPDATED_IDENTIFIKATOR_IBZ);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByIdentifikatorIbzIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where identifikatorIbz is less than or equal to DEFAULT_IDENTIFIKATOR_IBZ
        defaultAdministrationUnitShouldBeFound("identifikatorIbz.lessThanOrEqual=" + DEFAULT_IDENTIFIKATOR_IBZ);

        // Get all the administrationUnitList where identifikatorIbz is less than or equal to SMALLER_IDENTIFIKATOR_IBZ
        defaultAdministrationUnitShouldNotBeFound("identifikatorIbz.lessThanOrEqual=" + SMALLER_IDENTIFIKATOR_IBZ);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByIdentifikatorIbzIsLessThanSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where identifikatorIbz is less than DEFAULT_IDENTIFIKATOR_IBZ
        defaultAdministrationUnitShouldNotBeFound("identifikatorIbz.lessThan=" + DEFAULT_IDENTIFIKATOR_IBZ);

        // Get all the administrationUnitList where identifikatorIbz is less than UPDATED_IDENTIFIKATOR_IBZ
        defaultAdministrationUnitShouldBeFound("identifikatorIbz.lessThan=" + UPDATED_IDENTIFIKATOR_IBZ);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByIdentifikatorIbzIsGreaterThanSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where identifikatorIbz is greater than DEFAULT_IDENTIFIKATOR_IBZ
        defaultAdministrationUnitShouldNotBeFound("identifikatorIbz.greaterThan=" + DEFAULT_IDENTIFIKATOR_IBZ);

        // Get all the administrationUnitList where identifikatorIbz is greater than SMALLER_IDENTIFIKATOR_IBZ
        defaultAdministrationUnitShouldBeFound("identifikatorIbz.greaterThan=" + SMALLER_IDENTIFIKATOR_IBZ);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBemerkungIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bemerkung equals to DEFAULT_BEMERKUNG
        defaultAdministrationUnitShouldBeFound("bemerkung.equals=" + DEFAULT_BEMERKUNG);

        // Get all the administrationUnitList where bemerkung equals to UPDATED_BEMERKUNG
        defaultAdministrationUnitShouldNotBeFound("bemerkung.equals=" + UPDATED_BEMERKUNG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBemerkungIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bemerkung in DEFAULT_BEMERKUNG or UPDATED_BEMERKUNG
        defaultAdministrationUnitShouldBeFound("bemerkung.in=" + DEFAULT_BEMERKUNG + "," + UPDATED_BEMERKUNG);

        // Get all the administrationUnitList where bemerkung equals to UPDATED_BEMERKUNG
        defaultAdministrationUnitShouldNotBeFound("bemerkung.in=" + UPDATED_BEMERKUNG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBemerkungIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bemerkung is not null
        defaultAdministrationUnitShouldBeFound("bemerkung.specified=true");

        // Get all the administrationUnitList where bemerkung is null
        defaultAdministrationUnitShouldNotBeFound("bemerkung.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBemerkungContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bemerkung contains DEFAULT_BEMERKUNG
        defaultAdministrationUnitShouldBeFound("bemerkung.contains=" + DEFAULT_BEMERKUNG);

        // Get all the administrationUnitList where bemerkung contains UPDATED_BEMERKUNG
        defaultAdministrationUnitShouldNotBeFound("bemerkung.contains=" + UPDATED_BEMERKUNG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByBemerkungNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where bemerkung does not contain DEFAULT_BEMERKUNG
        defaultAdministrationUnitShouldNotBeFound("bemerkung.doesNotContain=" + DEFAULT_BEMERKUNG);

        // Get all the administrationUnitList where bemerkung does not contain UPDATED_BEMERKUNG
        defaultAdministrationUnitShouldBeFound("bemerkung.doesNotContain=" + UPDATED_BEMERKUNG);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByNamensbildungNbdIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where namensbildungNbd equals to DEFAULT_NAMENSBILDUNG_NBD
        defaultAdministrationUnitShouldBeFound("namensbildungNbd.equals=" + DEFAULT_NAMENSBILDUNG_NBD);

        // Get all the administrationUnitList where namensbildungNbd equals to UPDATED_NAMENSBILDUNG_NBD
        defaultAdministrationUnitShouldNotBeFound("namensbildungNbd.equals=" + UPDATED_NAMENSBILDUNG_NBD);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByNamensbildungNbdIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where namensbildungNbd in DEFAULT_NAMENSBILDUNG_NBD or UPDATED_NAMENSBILDUNG_NBD
        defaultAdministrationUnitShouldBeFound("namensbildungNbd.in=" + DEFAULT_NAMENSBILDUNG_NBD + "," + UPDATED_NAMENSBILDUNG_NBD);

        // Get all the administrationUnitList where namensbildungNbd equals to UPDATED_NAMENSBILDUNG_NBD
        defaultAdministrationUnitShouldNotBeFound("namensbildungNbd.in=" + UPDATED_NAMENSBILDUNG_NBD);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByNamensbildungNbdIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where namensbildungNbd is not null
        defaultAdministrationUnitShouldBeFound("namensbildungNbd.specified=true");

        // Get all the administrationUnitList where namensbildungNbd is null
        defaultAdministrationUnitShouldNotBeFound("namensbildungNbd.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByNamensbildungNbdContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where namensbildungNbd contains DEFAULT_NAMENSBILDUNG_NBD
        defaultAdministrationUnitShouldBeFound("namensbildungNbd.contains=" + DEFAULT_NAMENSBILDUNG_NBD);

        // Get all the administrationUnitList where namensbildungNbd contains UPDATED_NAMENSBILDUNG_NBD
        defaultAdministrationUnitShouldNotBeFound("namensbildungNbd.contains=" + UPDATED_NAMENSBILDUNG_NBD);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByNamensbildungNbdNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where namensbildungNbd does not contain DEFAULT_NAMENSBILDUNG_NBD
        defaultAdministrationUnitShouldNotBeFound("namensbildungNbd.doesNotContain=" + DEFAULT_NAMENSBILDUNG_NBD);

        // Get all the administrationUnitList where namensbildungNbd does not contain UPDATED_NAMENSBILDUNG_NBD
        defaultAdministrationUnitShouldBeFound("namensbildungNbd.doesNotContain=" + UPDATED_NAMENSBILDUNG_NBD);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByNbdIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where nbd equals to DEFAULT_NBD
        defaultAdministrationUnitShouldBeFound("nbd.equals=" + DEFAULT_NBD);

        // Get all the administrationUnitList where nbd equals to UPDATED_NBD
        defaultAdministrationUnitShouldNotBeFound("nbd.equals=" + UPDATED_NBD);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByNbdIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where nbd in DEFAULT_NBD or UPDATED_NBD
        defaultAdministrationUnitShouldBeFound("nbd.in=" + DEFAULT_NBD + "," + UPDATED_NBD);

        // Get all the administrationUnitList where nbd equals to UPDATED_NBD
        defaultAdministrationUnitShouldNotBeFound("nbd.in=" + UPDATED_NBD);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByNbdIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where nbd is not null
        defaultAdministrationUnitShouldBeFound("nbd.specified=true");

        // Get all the administrationUnitList where nbd is null
        defaultAdministrationUnitShouldNotBeFound("nbd.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByLandIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where land equals to DEFAULT_LAND
        defaultAdministrationUnitShouldBeFound("land.equals=" + DEFAULT_LAND);

        // Get all the administrationUnitList where land equals to UPDATED_LAND
        defaultAdministrationUnitShouldNotBeFound("land.equals=" + UPDATED_LAND);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByLandIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where land in DEFAULT_LAND or UPDATED_LAND
        defaultAdministrationUnitShouldBeFound("land.in=" + DEFAULT_LAND + "," + UPDATED_LAND);

        // Get all the administrationUnitList where land equals to UPDATED_LAND
        defaultAdministrationUnitShouldNotBeFound("land.in=" + UPDATED_LAND);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByLandIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where land is not null
        defaultAdministrationUnitShouldBeFound("land.specified=true");

        // Get all the administrationUnitList where land is null
        defaultAdministrationUnitShouldNotBeFound("land.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByLandContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where land contains DEFAULT_LAND
        defaultAdministrationUnitShouldBeFound("land.contains=" + DEFAULT_LAND);

        // Get all the administrationUnitList where land contains UPDATED_LAND
        defaultAdministrationUnitShouldNotBeFound("land.contains=" + UPDATED_LAND);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByLandNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where land does not contain DEFAULT_LAND
        defaultAdministrationUnitShouldNotBeFound("land.doesNotContain=" + DEFAULT_LAND);

        // Get all the administrationUnitList where land does not contain UPDATED_LAND
        defaultAdministrationUnitShouldBeFound("land.doesNotContain=" + UPDATED_LAND);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegierungsbezirkIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regierungsbezirk equals to DEFAULT_REGIERUNGSBEZIRK
        defaultAdministrationUnitShouldBeFound("regierungsbezirk.equals=" + DEFAULT_REGIERUNGSBEZIRK);

        // Get all the administrationUnitList where regierungsbezirk equals to UPDATED_REGIERUNGSBEZIRK
        defaultAdministrationUnitShouldNotBeFound("regierungsbezirk.equals=" + UPDATED_REGIERUNGSBEZIRK);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegierungsbezirkIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regierungsbezirk in DEFAULT_REGIERUNGSBEZIRK or UPDATED_REGIERUNGSBEZIRK
        defaultAdministrationUnitShouldBeFound("regierungsbezirk.in=" + DEFAULT_REGIERUNGSBEZIRK + "," + UPDATED_REGIERUNGSBEZIRK);

        // Get all the administrationUnitList where regierungsbezirk equals to UPDATED_REGIERUNGSBEZIRK
        defaultAdministrationUnitShouldNotBeFound("regierungsbezirk.in=" + UPDATED_REGIERUNGSBEZIRK);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegierungsbezirkIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regierungsbezirk is not null
        defaultAdministrationUnitShouldBeFound("regierungsbezirk.specified=true");

        // Get all the administrationUnitList where regierungsbezirk is null
        defaultAdministrationUnitShouldNotBeFound("regierungsbezirk.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegierungsbezirkContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regierungsbezirk contains DEFAULT_REGIERUNGSBEZIRK
        defaultAdministrationUnitShouldBeFound("regierungsbezirk.contains=" + DEFAULT_REGIERUNGSBEZIRK);

        // Get all the administrationUnitList where regierungsbezirk contains UPDATED_REGIERUNGSBEZIRK
        defaultAdministrationUnitShouldNotBeFound("regierungsbezirk.contains=" + UPDATED_REGIERUNGSBEZIRK);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegierungsbezirkNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regierungsbezirk does not contain DEFAULT_REGIERUNGSBEZIRK
        defaultAdministrationUnitShouldNotBeFound("regierungsbezirk.doesNotContain=" + DEFAULT_REGIERUNGSBEZIRK);

        // Get all the administrationUnitList where regierungsbezirk does not contain UPDATED_REGIERUNGSBEZIRK
        defaultAdministrationUnitShouldBeFound("regierungsbezirk.doesNotContain=" + UPDATED_REGIERUNGSBEZIRK);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByKreisIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where kreis equals to DEFAULT_KREIS
        defaultAdministrationUnitShouldBeFound("kreis.equals=" + DEFAULT_KREIS);

        // Get all the administrationUnitList where kreis equals to UPDATED_KREIS
        defaultAdministrationUnitShouldNotBeFound("kreis.equals=" + UPDATED_KREIS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByKreisIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where kreis in DEFAULT_KREIS or UPDATED_KREIS
        defaultAdministrationUnitShouldBeFound("kreis.in=" + DEFAULT_KREIS + "," + UPDATED_KREIS);

        // Get all the administrationUnitList where kreis equals to UPDATED_KREIS
        defaultAdministrationUnitShouldNotBeFound("kreis.in=" + UPDATED_KREIS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByKreisIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where kreis is not null
        defaultAdministrationUnitShouldBeFound("kreis.specified=true");

        // Get all the administrationUnitList where kreis is null
        defaultAdministrationUnitShouldNotBeFound("kreis.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByKreisContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where kreis contains DEFAULT_KREIS
        defaultAdministrationUnitShouldBeFound("kreis.contains=" + DEFAULT_KREIS);

        // Get all the administrationUnitList where kreis contains UPDATED_KREIS
        defaultAdministrationUnitShouldNotBeFound("kreis.contains=" + UPDATED_KREIS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByKreisNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where kreis does not contain DEFAULT_KREIS
        defaultAdministrationUnitShouldNotBeFound("kreis.doesNotContain=" + DEFAULT_KREIS);

        // Get all the administrationUnitList where kreis does not contain UPDATED_KREIS
        defaultAdministrationUnitShouldBeFound("kreis.doesNotContain=" + UPDATED_KREIS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungsgemeinschaftteil1IsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil1 equals to DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1
        defaultAdministrationUnitShouldBeFound("verwaltungsgemeinschaftteil1.equals=" + DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil1 equals to UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_1
        defaultAdministrationUnitShouldNotBeFound("verwaltungsgemeinschaftteil1.equals=" + UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_1);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungsgemeinschaftteil1IsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil1 in DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1 or UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_1
        defaultAdministrationUnitShouldBeFound(
            "verwaltungsgemeinschaftteil1.in=" + DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1 + "," + UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_1
        );

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil1 equals to UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_1
        defaultAdministrationUnitShouldNotBeFound("verwaltungsgemeinschaftteil1.in=" + UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_1);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungsgemeinschaftteil1IsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil1 is not null
        defaultAdministrationUnitShouldBeFound("verwaltungsgemeinschaftteil1.specified=true");

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil1 is null
        defaultAdministrationUnitShouldNotBeFound("verwaltungsgemeinschaftteil1.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungsgemeinschaftteil1ContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil1 contains DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1
        defaultAdministrationUnitShouldBeFound("verwaltungsgemeinschaftteil1.contains=" + DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil1 contains UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_1
        defaultAdministrationUnitShouldNotBeFound("verwaltungsgemeinschaftteil1.contains=" + UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_1);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungsgemeinschaftteil1NotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil1 does not contain DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1
        defaultAdministrationUnitShouldNotBeFound("verwaltungsgemeinschaftteil1.doesNotContain=" + DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil1 does not contain UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_1
        defaultAdministrationUnitShouldBeFound("verwaltungsgemeinschaftteil1.doesNotContain=" + UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_1);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungsgemeinschaftteil2IsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil2 equals to DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2
        defaultAdministrationUnitShouldBeFound("verwaltungsgemeinschaftteil2.equals=" + DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil2 equals to UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_2
        defaultAdministrationUnitShouldNotBeFound("verwaltungsgemeinschaftteil2.equals=" + UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_2);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungsgemeinschaftteil2IsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil2 in DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2 or UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_2
        defaultAdministrationUnitShouldBeFound(
            "verwaltungsgemeinschaftteil2.in=" + DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2 + "," + UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_2
        );

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil2 equals to UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_2
        defaultAdministrationUnitShouldNotBeFound("verwaltungsgemeinschaftteil2.in=" + UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_2);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungsgemeinschaftteil2IsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil2 is not null
        defaultAdministrationUnitShouldBeFound("verwaltungsgemeinschaftteil2.specified=true");

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil2 is null
        defaultAdministrationUnitShouldNotBeFound("verwaltungsgemeinschaftteil2.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungsgemeinschaftteil2ContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil2 contains DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2
        defaultAdministrationUnitShouldBeFound("verwaltungsgemeinschaftteil2.contains=" + DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil2 contains UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_2
        defaultAdministrationUnitShouldNotBeFound("verwaltungsgemeinschaftteil2.contains=" + UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_2);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByVerwaltungsgemeinschaftteil2NotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil2 does not contain DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2
        defaultAdministrationUnitShouldNotBeFound("verwaltungsgemeinschaftteil2.doesNotContain=" + DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2);

        // Get all the administrationUnitList where verwaltungsgemeinschaftteil2 does not contain UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_2
        defaultAdministrationUnitShouldBeFound("verwaltungsgemeinschaftteil2.doesNotContain=" + UPDATED_VERWALTUNGSGEMEINSCHAFTTEIL_2);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeinde equals to DEFAULT_GEMEINDE
        defaultAdministrationUnitShouldBeFound("gemeinde.equals=" + DEFAULT_GEMEINDE);

        // Get all the administrationUnitList where gemeinde equals to UPDATED_GEMEINDE
        defaultAdministrationUnitShouldNotBeFound("gemeinde.equals=" + UPDATED_GEMEINDE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeinde in DEFAULT_GEMEINDE or UPDATED_GEMEINDE
        defaultAdministrationUnitShouldBeFound("gemeinde.in=" + DEFAULT_GEMEINDE + "," + UPDATED_GEMEINDE);

        // Get all the administrationUnitList where gemeinde equals to UPDATED_GEMEINDE
        defaultAdministrationUnitShouldNotBeFound("gemeinde.in=" + UPDATED_GEMEINDE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeinde is not null
        defaultAdministrationUnitShouldBeFound("gemeinde.specified=true");

        // Get all the administrationUnitList where gemeinde is null
        defaultAdministrationUnitShouldNotBeFound("gemeinde.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeinde contains DEFAULT_GEMEINDE
        defaultAdministrationUnitShouldBeFound("gemeinde.contains=" + DEFAULT_GEMEINDE);

        // Get all the administrationUnitList where gemeinde contains UPDATED_GEMEINDE
        defaultAdministrationUnitShouldNotBeFound("gemeinde.contains=" + UPDATED_GEMEINDE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeinde does not contain DEFAULT_GEMEINDE
        defaultAdministrationUnitShouldNotBeFound("gemeinde.doesNotContain=" + DEFAULT_GEMEINDE);

        // Get all the administrationUnitList where gemeinde does not contain UPDATED_GEMEINDE
        defaultAdministrationUnitShouldBeFound("gemeinde.doesNotContain=" + UPDATED_GEMEINDE);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByFunkSchluesselstelle3IsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where funkSchluesselstelle3 equals to DEFAULT_FUNK_SCHLUESSELSTELLE_3
        defaultAdministrationUnitShouldBeFound("funkSchluesselstelle3.equals=" + DEFAULT_FUNK_SCHLUESSELSTELLE_3);

        // Get all the administrationUnitList where funkSchluesselstelle3 equals to UPDATED_FUNK_SCHLUESSELSTELLE_3
        defaultAdministrationUnitShouldNotBeFound("funkSchluesselstelle3.equals=" + UPDATED_FUNK_SCHLUESSELSTELLE_3);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByFunkSchluesselstelle3IsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where funkSchluesselstelle3 in DEFAULT_FUNK_SCHLUESSELSTELLE_3 or UPDATED_FUNK_SCHLUESSELSTELLE_3
        defaultAdministrationUnitShouldBeFound(
            "funkSchluesselstelle3.in=" + DEFAULT_FUNK_SCHLUESSELSTELLE_3 + "," + UPDATED_FUNK_SCHLUESSELSTELLE_3
        );

        // Get all the administrationUnitList where funkSchluesselstelle3 equals to UPDATED_FUNK_SCHLUESSELSTELLE_3
        defaultAdministrationUnitShouldNotBeFound("funkSchluesselstelle3.in=" + UPDATED_FUNK_SCHLUESSELSTELLE_3);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByFunkSchluesselstelle3IsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where funkSchluesselstelle3 is not null
        defaultAdministrationUnitShouldBeFound("funkSchluesselstelle3.specified=true");

        // Get all the administrationUnitList where funkSchluesselstelle3 is null
        defaultAdministrationUnitShouldNotBeFound("funkSchluesselstelle3.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByFunkSchluesselstelle3ContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where funkSchluesselstelle3 contains DEFAULT_FUNK_SCHLUESSELSTELLE_3
        defaultAdministrationUnitShouldBeFound("funkSchluesselstelle3.contains=" + DEFAULT_FUNK_SCHLUESSELSTELLE_3);

        // Get all the administrationUnitList where funkSchluesselstelle3 contains UPDATED_FUNK_SCHLUESSELSTELLE_3
        defaultAdministrationUnitShouldNotBeFound("funkSchluesselstelle3.contains=" + UPDATED_FUNK_SCHLUESSELSTELLE_3);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByFunkSchluesselstelle3NotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where funkSchluesselstelle3 does not contain DEFAULT_FUNK_SCHLUESSELSTELLE_3
        defaultAdministrationUnitShouldNotBeFound("funkSchluesselstelle3.doesNotContain=" + DEFAULT_FUNK_SCHLUESSELSTELLE_3);

        // Get all the administrationUnitList where funkSchluesselstelle3 does not contain UPDATED_FUNK_SCHLUESSELSTELLE_3
        defaultAdministrationUnitShouldBeFound("funkSchluesselstelle3.doesNotContain=" + UPDATED_FUNK_SCHLUESSELSTELLE_3);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByFkS3IsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where fkS3 equals to DEFAULT_FK_S_3
        defaultAdministrationUnitShouldBeFound("fkS3.equals=" + DEFAULT_FK_S_3);

        // Get all the administrationUnitList where fkS3 equals to UPDATED_FK_S_3
        defaultAdministrationUnitShouldNotBeFound("fkS3.equals=" + UPDATED_FK_S_3);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByFkS3IsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where fkS3 in DEFAULT_FK_S_3 or UPDATED_FK_S_3
        defaultAdministrationUnitShouldBeFound("fkS3.in=" + DEFAULT_FK_S_3 + "," + UPDATED_FK_S_3);

        // Get all the administrationUnitList where fkS3 equals to UPDATED_FK_S_3
        defaultAdministrationUnitShouldNotBeFound("fkS3.in=" + UPDATED_FK_S_3);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByFkS3IsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where fkS3 is not null
        defaultAdministrationUnitShouldBeFound("fkS3.specified=true");

        // Get all the administrationUnitList where fkS3 is null
        defaultAdministrationUnitShouldNotBeFound("fkS3.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByFkS3ContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where fkS3 contains DEFAULT_FK_S_3
        defaultAdministrationUnitShouldBeFound("fkS3.contains=" + DEFAULT_FK_S_3);

        // Get all the administrationUnitList where fkS3 contains UPDATED_FK_S_3
        defaultAdministrationUnitShouldNotBeFound("fkS3.contains=" + UPDATED_FK_S_3);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByFkS3NotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where fkS3 does not contain DEFAULT_FK_S_3
        defaultAdministrationUnitShouldNotBeFound("fkS3.doesNotContain=" + DEFAULT_FK_S_3);

        // Get all the administrationUnitList where fkS3 does not contain UPDATED_FK_S_3
        defaultAdministrationUnitShouldBeFound("fkS3.doesNotContain=" + UPDATED_FK_S_3);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByEuropStatistikschluesselNutsIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where europStatistikschluesselNuts equals to DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS
        defaultAdministrationUnitShouldBeFound("europStatistikschluesselNuts.equals=" + DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS);

        // Get all the administrationUnitList where europStatistikschluesselNuts equals to UPDATED_EUROP_STATISTIKSCHLUESSEL_NUTS
        defaultAdministrationUnitShouldNotBeFound("europStatistikschluesselNuts.equals=" + UPDATED_EUROP_STATISTIKSCHLUESSEL_NUTS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByEuropStatistikschluesselNutsIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where europStatistikschluesselNuts in DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS or UPDATED_EUROP_STATISTIKSCHLUESSEL_NUTS
        defaultAdministrationUnitShouldBeFound(
            "europStatistikschluesselNuts.in=" + DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS + "," + UPDATED_EUROP_STATISTIKSCHLUESSEL_NUTS
        );

        // Get all the administrationUnitList where europStatistikschluesselNuts equals to UPDATED_EUROP_STATISTIKSCHLUESSEL_NUTS
        defaultAdministrationUnitShouldNotBeFound("europStatistikschluesselNuts.in=" + UPDATED_EUROP_STATISTIKSCHLUESSEL_NUTS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByEuropStatistikschluesselNutsIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where europStatistikschluesselNuts is not null
        defaultAdministrationUnitShouldBeFound("europStatistikschluesselNuts.specified=true");

        // Get all the administrationUnitList where europStatistikschluesselNuts is null
        defaultAdministrationUnitShouldNotBeFound("europStatistikschluesselNuts.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByEuropStatistikschluesselNutsContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where europStatistikschluesselNuts contains DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS
        defaultAdministrationUnitShouldBeFound("europStatistikschluesselNuts.contains=" + DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS);

        // Get all the administrationUnitList where europStatistikschluesselNuts contains UPDATED_EUROP_STATISTIKSCHLUESSEL_NUTS
        defaultAdministrationUnitShouldNotBeFound("europStatistikschluesselNuts.contains=" + UPDATED_EUROP_STATISTIKSCHLUESSEL_NUTS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByEuropStatistikschluesselNutsNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where europStatistikschluesselNuts does not contain DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS
        defaultAdministrationUnitShouldNotBeFound("europStatistikschluesselNuts.doesNotContain=" + DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS);

        // Get all the administrationUnitList where europStatistikschluesselNuts does not contain UPDATED_EUROP_STATISTIKSCHLUESSEL_NUTS
        defaultAdministrationUnitShouldBeFound("europStatistikschluesselNuts.doesNotContain=" + UPDATED_EUROP_STATISTIKSCHLUESSEL_NUTS);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegioschluesselaufgefuelltIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regioschluesselaufgefuellt equals to DEFAULT_REGIOSCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldBeFound("regioschluesselaufgefuellt.equals=" + DEFAULT_REGIOSCHLUESSELAUFGEFUELLT);

        // Get all the administrationUnitList where regioschluesselaufgefuellt equals to UPDATED_REGIOSCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldNotBeFound("regioschluesselaufgefuellt.equals=" + UPDATED_REGIOSCHLUESSELAUFGEFUELLT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegioschluesselaufgefuelltIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regioschluesselaufgefuellt in DEFAULT_REGIOSCHLUESSELAUFGEFUELLT or UPDATED_REGIOSCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldBeFound(
            "regioschluesselaufgefuellt.in=" + DEFAULT_REGIOSCHLUESSELAUFGEFUELLT + "," + UPDATED_REGIOSCHLUESSELAUFGEFUELLT
        );

        // Get all the administrationUnitList where regioschluesselaufgefuellt equals to UPDATED_REGIOSCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldNotBeFound("regioschluesselaufgefuellt.in=" + UPDATED_REGIOSCHLUESSELAUFGEFUELLT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegioschluesselaufgefuelltIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regioschluesselaufgefuellt is not null
        defaultAdministrationUnitShouldBeFound("regioschluesselaufgefuellt.specified=true");

        // Get all the administrationUnitList where regioschluesselaufgefuellt is null
        defaultAdministrationUnitShouldNotBeFound("regioschluesselaufgefuellt.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegioschluesselaufgefuelltContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regioschluesselaufgefuellt contains DEFAULT_REGIOSCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldBeFound("regioschluesselaufgefuellt.contains=" + DEFAULT_REGIOSCHLUESSELAUFGEFUELLT);

        // Get all the administrationUnitList where regioschluesselaufgefuellt contains UPDATED_REGIOSCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldNotBeFound("regioschluesselaufgefuellt.contains=" + UPDATED_REGIOSCHLUESSELAUFGEFUELLT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByRegioschluesselaufgefuelltNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where regioschluesselaufgefuellt does not contain DEFAULT_REGIOSCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldNotBeFound("regioschluesselaufgefuellt.doesNotContain=" + DEFAULT_REGIOSCHLUESSELAUFGEFUELLT);

        // Get all the administrationUnitList where regioschluesselaufgefuellt does not contain UPDATED_REGIOSCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldBeFound("regioschluesselaufgefuellt.doesNotContain=" + UPDATED_REGIOSCHLUESSELAUFGEFUELLT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeschluesselaufgefuelltIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeindeschluesselaufgefuellt equals to DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldBeFound("gemeindeschluesselaufgefuellt.equals=" + DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT);

        // Get all the administrationUnitList where gemeindeschluesselaufgefuellt equals to UPDATED_GEMEINDESCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldNotBeFound("gemeindeschluesselaufgefuellt.equals=" + UPDATED_GEMEINDESCHLUESSELAUFGEFUELLT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeschluesselaufgefuelltIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeindeschluesselaufgefuellt in DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT or UPDATED_GEMEINDESCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldBeFound(
            "gemeindeschluesselaufgefuellt.in=" + DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT + "," + UPDATED_GEMEINDESCHLUESSELAUFGEFUELLT
        );

        // Get all the administrationUnitList where gemeindeschluesselaufgefuellt equals to UPDATED_GEMEINDESCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldNotBeFound("gemeindeschluesselaufgefuellt.in=" + UPDATED_GEMEINDESCHLUESSELAUFGEFUELLT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeschluesselaufgefuelltIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeindeschluesselaufgefuellt is not null
        defaultAdministrationUnitShouldBeFound("gemeindeschluesselaufgefuellt.specified=true");

        // Get all the administrationUnitList where gemeindeschluesselaufgefuellt is null
        defaultAdministrationUnitShouldNotBeFound("gemeindeschluesselaufgefuellt.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeschluesselaufgefuelltContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeindeschluesselaufgefuellt contains DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldBeFound("gemeindeschluesselaufgefuellt.contains=" + DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT);

        // Get all the administrationUnitList where gemeindeschluesselaufgefuellt contains UPDATED_GEMEINDESCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldNotBeFound("gemeindeschluesselaufgefuellt.contains=" + UPDATED_GEMEINDESCHLUESSELAUFGEFUELLT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGemeindeschluesselaufgefuelltNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where gemeindeschluesselaufgefuellt does not contain DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldNotBeFound("gemeindeschluesselaufgefuellt.doesNotContain=" + DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT);

        // Get all the administrationUnitList where gemeindeschluesselaufgefuellt does not contain UPDATED_GEMEINDESCHLUESSELAUFGEFUELLT
        defaultAdministrationUnitShouldBeFound("gemeindeschluesselaufgefuellt.doesNotContain=" + UPDATED_GEMEINDESCHLUESSELAUFGEFUELLT);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByWirksamkeitWskIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where wirksamkeitWsk equals to DEFAULT_WIRKSAMKEIT_WSK
        defaultAdministrationUnitShouldBeFound("wirksamkeitWsk.equals=" + DEFAULT_WIRKSAMKEIT_WSK);

        // Get all the administrationUnitList where wirksamkeitWsk equals to UPDATED_WIRKSAMKEIT_WSK
        defaultAdministrationUnitShouldNotBeFound("wirksamkeitWsk.equals=" + UPDATED_WIRKSAMKEIT_WSK);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByWirksamkeitWskIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where wirksamkeitWsk in DEFAULT_WIRKSAMKEIT_WSK or UPDATED_WIRKSAMKEIT_WSK
        defaultAdministrationUnitShouldBeFound("wirksamkeitWsk.in=" + DEFAULT_WIRKSAMKEIT_WSK + "," + UPDATED_WIRKSAMKEIT_WSK);

        // Get all the administrationUnitList where wirksamkeitWsk equals to UPDATED_WIRKSAMKEIT_WSK
        defaultAdministrationUnitShouldNotBeFound("wirksamkeitWsk.in=" + UPDATED_WIRKSAMKEIT_WSK);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByWirksamkeitWskIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where wirksamkeitWsk is not null
        defaultAdministrationUnitShouldBeFound("wirksamkeitWsk.specified=true");

        // Get all the administrationUnitList where wirksamkeitWsk is null
        defaultAdministrationUnitShouldNotBeFound("wirksamkeitWsk.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByWirksamkeitWskIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where wirksamkeitWsk is greater than or equal to DEFAULT_WIRKSAMKEIT_WSK
        defaultAdministrationUnitShouldBeFound("wirksamkeitWsk.greaterThanOrEqual=" + DEFAULT_WIRKSAMKEIT_WSK);

        // Get all the administrationUnitList where wirksamkeitWsk is greater than or equal to UPDATED_WIRKSAMKEIT_WSK
        defaultAdministrationUnitShouldNotBeFound("wirksamkeitWsk.greaterThanOrEqual=" + UPDATED_WIRKSAMKEIT_WSK);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByWirksamkeitWskIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where wirksamkeitWsk is less than or equal to DEFAULT_WIRKSAMKEIT_WSK
        defaultAdministrationUnitShouldBeFound("wirksamkeitWsk.lessThanOrEqual=" + DEFAULT_WIRKSAMKEIT_WSK);

        // Get all the administrationUnitList where wirksamkeitWsk is less than or equal to SMALLER_WIRKSAMKEIT_WSK
        defaultAdministrationUnitShouldNotBeFound("wirksamkeitWsk.lessThanOrEqual=" + SMALLER_WIRKSAMKEIT_WSK);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByWirksamkeitWskIsLessThanSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where wirksamkeitWsk is less than DEFAULT_WIRKSAMKEIT_WSK
        defaultAdministrationUnitShouldNotBeFound("wirksamkeitWsk.lessThan=" + DEFAULT_WIRKSAMKEIT_WSK);

        // Get all the administrationUnitList where wirksamkeitWsk is less than UPDATED_WIRKSAMKEIT_WSK
        defaultAdministrationUnitShouldBeFound("wirksamkeitWsk.lessThan=" + UPDATED_WIRKSAMKEIT_WSK);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByWirksamkeitWskIsGreaterThanSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where wirksamkeitWsk is greater than DEFAULT_WIRKSAMKEIT_WSK
        defaultAdministrationUnitShouldNotBeFound("wirksamkeitWsk.greaterThan=" + DEFAULT_WIRKSAMKEIT_WSK);

        // Get all the administrationUnitList where wirksamkeitWsk is greater than SMALLER_WIRKSAMKEIT_WSK
        defaultAdministrationUnitShouldBeFound("wirksamkeitWsk.greaterThan=" + SMALLER_WIRKSAMKEIT_WSK);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeometryIsEqualToSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geometry equals to DEFAULT_GEOMETRY
        defaultAdministrationUnitShouldBeFound("geometry.equals=" + DEFAULT_GEOMETRY);

        // Get all the administrationUnitList where geometry equals to UPDATED_GEOMETRY
        defaultAdministrationUnitShouldNotBeFound("geometry.equals=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeometryIsInShouldWork() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geometry in DEFAULT_GEOMETRY or UPDATED_GEOMETRY
        defaultAdministrationUnitShouldBeFound("geometry.in=" + DEFAULT_GEOMETRY + "," + UPDATED_GEOMETRY);

        // Get all the administrationUnitList where geometry equals to UPDATED_GEOMETRY
        defaultAdministrationUnitShouldNotBeFound("geometry.in=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeometryIsNullOrNotNull() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geometry is not null
        defaultAdministrationUnitShouldBeFound("geometry.specified=true");

        // Get all the administrationUnitList where geometry is null
        defaultAdministrationUnitShouldNotBeFound("geometry.specified=false");
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeometryContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geometry contains DEFAULT_GEOMETRY
        defaultAdministrationUnitShouldBeFound("geometry.contains=" + DEFAULT_GEOMETRY);

        // Get all the administrationUnitList where geometry contains UPDATED_GEOMETRY
        defaultAdministrationUnitShouldNotBeFound("geometry.contains=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByGeometryNotContainsSomething() throws Exception {
        // Initialize the database
        administrationUnitRepository.saveAndFlush(administrationUnit);

        // Get all the administrationUnitList where geometry does not contain DEFAULT_GEOMETRY
        defaultAdministrationUnitShouldNotBeFound("geometry.doesNotContain=" + DEFAULT_GEOMETRY);

        // Get all the administrationUnitList where geometry does not contain UPDATED_GEOMETRY
        defaultAdministrationUnitShouldBeFound("geometry.doesNotContain=" + UPDATED_GEOMETRY);
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByDistrictIsEqualToSomething() throws Exception {
        AdministrationUnit district;
        if (TestUtil.findAll(em, AdministrationUnit.class).isEmpty()) {
            administrationUnitRepository.saveAndFlush(administrationUnit);
            district = AdministrationUnitResourceIT.createEntity(em);
        } else {
            district = TestUtil.findAll(em, AdministrationUnit.class).get(0);
        }
        em.persist(district);
        em.flush();
        administrationUnit.setDistrict(district);
        administrationUnitRepository.saveAndFlush(administrationUnit);
        Long districtId = district.getId();

        // Get all the administrationUnitList where district equals to districtId
        defaultAdministrationUnitShouldBeFound("districtId.equals=" + districtId);

        // Get all the administrationUnitList where district equals to (districtId + 1)
        defaultAdministrationUnitShouldNotBeFound("districtId.equals=" + (districtId + 1));
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByMastrUnitIsEqualToSomething() throws Exception {
        MastrUnit mastrUnit;
        if (TestUtil.findAll(em, MastrUnit.class).isEmpty()) {
            administrationUnitRepository.saveAndFlush(administrationUnit);
            mastrUnit = MastrUnitResourceIT.createEntity(em);
        } else {
            mastrUnit = TestUtil.findAll(em, MastrUnit.class).get(0);
        }
        em.persist(mastrUnit);
        em.flush();
        administrationUnit.addMastrUnit(mastrUnit);
        administrationUnitRepository.saveAndFlush(administrationUnit);
        Long mastrUnitId = mastrUnit.getId();

        // Get all the administrationUnitList where mastrUnit equals to mastrUnitId
        defaultAdministrationUnitShouldBeFound("mastrUnitId.equals=" + mastrUnitId);

        // Get all the administrationUnitList where mastrUnit equals to (mastrUnitId + 1)
        defaultAdministrationUnitShouldNotBeFound("mastrUnitId.equals=" + (mastrUnitId + 1));
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByChargingStationIsEqualToSomething() throws Exception {
        ChargingStation chargingStation;
        if (TestUtil.findAll(em, ChargingStation.class).isEmpty()) {
            administrationUnitRepository.saveAndFlush(administrationUnit);
            chargingStation = ChargingStationResourceIT.createEntity(em);
        } else {
            chargingStation = TestUtil.findAll(em, ChargingStation.class).get(0);
        }
        em.persist(chargingStation);
        em.flush();
        administrationUnit.addChargingStation(chargingStation);
        administrationUnitRepository.saveAndFlush(administrationUnit);
        Long chargingStationId = chargingStation.getId();

        // Get all the administrationUnitList where chargingStation equals to chargingStationId
        defaultAdministrationUnitShouldBeFound("chargingStationId.equals=" + chargingStationId);

        // Get all the administrationUnitList where chargingStation equals to (chargingStationId + 1)
        defaultAdministrationUnitShouldNotBeFound("chargingStationId.equals=" + (chargingStationId + 1));
    }

    @Test
    @Transactional
    void getAllAdministrationUnitsByStatisticIsEqualToSomething() throws Exception {
        Statistic statistic;
        if (TestUtil.findAll(em, Statistic.class).isEmpty()) {
            administrationUnitRepository.saveAndFlush(administrationUnit);
            statistic = StatisticResourceIT.createEntity(em);
        } else {
            statistic = TestUtil.findAll(em, Statistic.class).get(0);
        }
        em.persist(statistic);
        em.flush();
        administrationUnit.addStatistic(statistic);
        administrationUnitRepository.saveAndFlush(administrationUnit);
        Long statisticId = statistic.getId();

        // Get all the administrationUnitList where statistic equals to statisticId
        defaultAdministrationUnitShouldBeFound("statisticId.equals=" + statisticId);

        // Get all the administrationUnitList where statistic equals to (statisticId + 1)
        defaultAdministrationUnitShouldNotBeFound("statisticId.equals=" + (statisticId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAdministrationUnitShouldBeFound(String filter) throws Exception {
        restAdministrationUnitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(administrationUnit.getId().intValue())))
            .andExpect(jsonPath("$.[*].objektidentifikator").value(hasItem(DEFAULT_OBJEKTIDENTIFIKATOR)))
            .andExpect(jsonPath("$.[*].beginnlebenszeit").value(hasItem(DEFAULT_BEGINNLEBENSZEIT.toString())))
            .andExpect(jsonPath("$.[*].adminEbeneAde").value(hasItem(DEFAULT_ADMIN_EBENE_ADE)))
            .andExpect(jsonPath("$.[*].ade").value(hasItem(DEFAULT_ADE)))
            .andExpect(jsonPath("$.[*].geofaktorGf").value(hasItem(DEFAULT_GEOFAKTOR_GF)))
            .andExpect(jsonPath("$.[*].gf").value(hasItem(DEFAULT_GF.intValue())))
            .andExpect(jsonPath("$.[*].besondereGebieteBsg").value(hasItem(DEFAULT_BESONDERE_GEBIETE_BSG)))
            .andExpect(jsonPath("$.[*].bsg").value(hasItem(DEFAULT_BSG.intValue())))
            .andExpect(jsonPath("$.[*].regionalschluesselArs").value(hasItem(DEFAULT_REGIONALSCHLUESSEL_ARS)))
            .andExpect(jsonPath("$.[*].gemeindeschluesselAgs").value(hasItem(DEFAULT_GEMEINDESCHLUESSEL_AGS)))
            .andExpect(jsonPath("$.[*].verwaltungssitzSdvArs").value(hasItem(DEFAULT_VERWALTUNGSSITZ_SDV_ARS)))
            .andExpect(jsonPath("$.[*].geografischernameGen").value(hasItem(DEFAULT_GEOGRAFISCHERNAME_GEN)))
            .andExpect(jsonPath("$.[*].bezeichnung").value(hasItem(DEFAULT_BEZEICHNUNG)))
            .andExpect(jsonPath("$.[*].identifikatorIbz").value(hasItem(DEFAULT_IDENTIFIKATOR_IBZ.intValue())))
            .andExpect(jsonPath("$.[*].bemerkung").value(hasItem(DEFAULT_BEMERKUNG)))
            .andExpect(jsonPath("$.[*].namensbildungNbd").value(hasItem(DEFAULT_NAMENSBILDUNG_NBD)))
            .andExpect(jsonPath("$.[*].nbd").value(hasItem(DEFAULT_NBD.booleanValue())))
            .andExpect(jsonPath("$.[*].land").value(hasItem(DEFAULT_LAND)))
            .andExpect(jsonPath("$.[*].regierungsbezirk").value(hasItem(DEFAULT_REGIERUNGSBEZIRK)))
            .andExpect(jsonPath("$.[*].kreis").value(hasItem(DEFAULT_KREIS)))
            .andExpect(jsonPath("$.[*].verwaltungsgemeinschaftteil1").value(hasItem(DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_1)))
            .andExpect(jsonPath("$.[*].verwaltungsgemeinschaftteil2").value(hasItem(DEFAULT_VERWALTUNGSGEMEINSCHAFTTEIL_2)))
            .andExpect(jsonPath("$.[*].gemeinde").value(hasItem(DEFAULT_GEMEINDE)))
            .andExpect(jsonPath("$.[*].funkSchluesselstelle3").value(hasItem(DEFAULT_FUNK_SCHLUESSELSTELLE_3)))
            .andExpect(jsonPath("$.[*].fkS3").value(hasItem(DEFAULT_FK_S_3)))
            .andExpect(jsonPath("$.[*].europStatistikschluesselNuts").value(hasItem(DEFAULT_EUROP_STATISTIKSCHLUESSEL_NUTS)))
            .andExpect(jsonPath("$.[*].regioschluesselaufgefuellt").value(hasItem(DEFAULT_REGIOSCHLUESSELAUFGEFUELLT)))
            .andExpect(jsonPath("$.[*].gemeindeschluesselaufgefuellt").value(hasItem(DEFAULT_GEMEINDESCHLUESSELAUFGEFUELLT)))
            .andExpect(jsonPath("$.[*].wirksamkeitWsk").value(hasItem(DEFAULT_WIRKSAMKEIT_WSK.toString())))
            .andExpect(jsonPath("$.[*].geometry").value(hasItem(DEFAULT_GEOMETRY)));

        // Check, that the count call also returns 1
        restAdministrationUnitMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAdministrationUnitShouldNotBeFound(String filter) throws Exception {
        restAdministrationUnitMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAdministrationUnitMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingAdministrationUnit() throws Exception {
        // Get the administrationUnit
        restAdministrationUnitMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }
}
