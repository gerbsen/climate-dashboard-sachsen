package eu.danielgerber.web.rest;

import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import eu.danielgerber.IntegrationTest;
import eu.danielgerber.domain.DWDMeasurement;
import eu.danielgerber.repository.DWDMeasurementRepository;

/**
 * Integration tests for the {@link DWDMeasurementResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DWDMeasurementResourceIT {

    private static final Long DEFAULT_YEAR = 1L;
    private static final Long UPDATED_YEAR = 2L;
    private static final Long SMALLER_YEAR = 1L - 1L;

    private static final String DEFAULT_STATISTIC = "AAAAAAAAAA";
    private static final String UPDATED_STATISTIC = "BBBBBBBBBB";

    private static final Double DEFAULT_BRANDENBURG_BERLIN = 1D;
    private static final Double UPDATED_BRANDENBURG_BERLIN = 2D;
    private static final Double SMALLER_BRANDENBURG_BERLIN = 1D - 1D;

    private static final Double DEFAULT_BRANDENBURG = 1D;
    private static final Double UPDATED_BRANDENBURG = 2D;
    private static final Double SMALLER_BRANDENBURG = 1D - 1D;

    private static final Double DEFAULT_BADEN_WUERTTEMBERG = 1D;
    private static final Double UPDATED_BADEN_WUERTTEMBERG = 2D;
    private static final Double SMALLER_BADEN_WUERTTEMBERG = 1D - 1D;

    private static final Double DEFAULT_BAYERN = 1D;
    private static final Double UPDATED_BAYERN = 2D;
    private static final Double SMALLER_BAYERN = 1D - 1D;

    private static final Double DEFAULT_HESSEN = 1D;
    private static final Double UPDATED_HESSEN = 2D;
    private static final Double SMALLER_HESSEN = 1D - 1D;

    private static final Double DEFAULT_MECKLENBURG_VORPOMMERN = 1D;
    private static final Double UPDATED_MECKLENBURG_VORPOMMERN = 2D;
    private static final Double SMALLER_MECKLENBURG_VORPOMMERN = 1D - 1D;

    private static final Double DEFAULT_NIEDERSACHSEN = 1D;
    private static final Double UPDATED_NIEDERSACHSEN = 2D;
    private static final Double SMALLER_NIEDERSACHSEN = 1D - 1D;

    private static final Double DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN = 1D;
    private static final Double UPDATED_NIEDERSACHSEN_HAMBURG_BREMEN = 2D;
    private static final Double SMALLER_NIEDERSACHSEN_HAMBURG_BREMEN = 1D - 1D;

    private static final Double DEFAULT_NORDRHEIN_WESTFALEN = 1D;
    private static final Double UPDATED_NORDRHEIN_WESTFALEN = 2D;
    private static final Double SMALLER_NORDRHEIN_WESTFALEN = 1D - 1D;

    private static final Double DEFAULT_RHEINLAND_PFALZ = 1D;
    private static final Double UPDATED_RHEINLAND_PFALZ = 2D;
    private static final Double SMALLER_RHEINLAND_PFALZ = 1D - 1D;

    private static final Double DEFAULT_SCHLESWIG_HOLSTEIN = 1D;
    private static final Double UPDATED_SCHLESWIG_HOLSTEIN = 2D;
    private static final Double SMALLER_SCHLESWIG_HOLSTEIN = 1D - 1D;

    private static final Double DEFAULT_SAARLAND = 1D;
    private static final Double UPDATED_SAARLAND = 2D;
    private static final Double SMALLER_SAARLAND = 1D - 1D;

    private static final Double DEFAULT_SACHSEN = 1D;
    private static final Double UPDATED_SACHSEN = 2D;
    private static final Double SMALLER_SACHSEN = 1D - 1D;

    private static final Double DEFAULT_SACHSEN_ANHALT = 1D;
    private static final Double UPDATED_SACHSEN_ANHALT = 2D;
    private static final Double SMALLER_SACHSEN_ANHALT = 1D - 1D;

    private static final Double DEFAULT_THUERINGEN_SACHSEN_ANHALT = 1D;
    private static final Double UPDATED_THUERINGEN_SACHSEN_ANHALT = 2D;
    private static final Double SMALLER_THUERINGEN_SACHSEN_ANHALT = 1D - 1D;

    private static final Double DEFAULT_THUERINGEN = 1D;
    private static final Double UPDATED_THUERINGEN = 2D;
    private static final Double SMALLER_THUERINGEN = 1D - 1D;

    private static final Double DEFAULT_DEUTSCHLAND = 1D;
    private static final Double UPDATED_DEUTSCHLAND = 2D;
    private static final Double SMALLER_DEUTSCHLAND = 1D - 1D;

    private static final String ENTITY_API_URL = "/api/dwd-measurements";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private DWDMeasurementRepository dWDMeasurementRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDWDMeasurementMockMvc;

    private DWDMeasurement dWDMeasurement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DWDMeasurement createEntity(EntityManager em) {
        DWDMeasurement dWDMeasurement = new DWDMeasurement()
            .year(DEFAULT_YEAR)
            .statistic(DEFAULT_STATISTIC)
            .brandenburgBerlin(DEFAULT_BRANDENBURG_BERLIN)
            .brandenburg(DEFAULT_BRANDENBURG)
            .badenWuerttemberg(DEFAULT_BADEN_WUERTTEMBERG)
            .bayern(DEFAULT_BAYERN)
            .hessen(DEFAULT_HESSEN)
            .mecklenburgVorpommern(DEFAULT_MECKLENBURG_VORPOMMERN)
            .niedersachsen(DEFAULT_NIEDERSACHSEN)
            .niedersachsenHamburgBremen(DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN)
            .nordrheinWestfalen(DEFAULT_NORDRHEIN_WESTFALEN)
            .rheinlandPfalz(DEFAULT_RHEINLAND_PFALZ)
            .schleswigHolstein(DEFAULT_SCHLESWIG_HOLSTEIN)
            .saarland(DEFAULT_SAARLAND)
            .sachsen(DEFAULT_SACHSEN)
            .sachsenAnhalt(DEFAULT_SACHSEN_ANHALT)
            .thueringenSachsenAnhalt(DEFAULT_THUERINGEN_SACHSEN_ANHALT)
            .thueringen(DEFAULT_THUERINGEN)
            .deutschland(DEFAULT_DEUTSCHLAND);
        return dWDMeasurement;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DWDMeasurement createUpdatedEntity(EntityManager em) {
        DWDMeasurement dWDMeasurement = new DWDMeasurement()
            .year(UPDATED_YEAR)
            .statistic(UPDATED_STATISTIC)
            .brandenburgBerlin(UPDATED_BRANDENBURG_BERLIN)
            .brandenburg(UPDATED_BRANDENBURG)
            .badenWuerttemberg(UPDATED_BADEN_WUERTTEMBERG)
            .bayern(UPDATED_BAYERN)
            .hessen(UPDATED_HESSEN)
            .mecklenburgVorpommern(UPDATED_MECKLENBURG_VORPOMMERN)
            .niedersachsen(UPDATED_NIEDERSACHSEN)
            .niedersachsenHamburgBremen(UPDATED_NIEDERSACHSEN_HAMBURG_BREMEN)
            .nordrheinWestfalen(UPDATED_NORDRHEIN_WESTFALEN)
            .rheinlandPfalz(UPDATED_RHEINLAND_PFALZ)
            .schleswigHolstein(UPDATED_SCHLESWIG_HOLSTEIN)
            .saarland(UPDATED_SAARLAND)
            .sachsen(UPDATED_SACHSEN)
            .sachsenAnhalt(UPDATED_SACHSEN_ANHALT)
            .thueringenSachsenAnhalt(UPDATED_THUERINGEN_SACHSEN_ANHALT)
            .thueringen(UPDATED_THUERINGEN)
            .deutschland(UPDATED_DEUTSCHLAND);
        return dWDMeasurement;
    }

    @BeforeEach
    public void initTest() {
        dWDMeasurement = createEntity(em);
    }

    @Test
    @Transactional
    void getAllDWDMeasurements() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList
        restDWDMeasurementMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dWDMeasurement.getId().intValue())))
            .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR.intValue())))
            .andExpect(jsonPath("$.[*].statistic").value(hasItem(DEFAULT_STATISTIC)))
            .andExpect(jsonPath("$.[*].brandenburgBerlin").value(hasItem(DEFAULT_BRANDENBURG_BERLIN.doubleValue())))
            .andExpect(jsonPath("$.[*].brandenburg").value(hasItem(DEFAULT_BRANDENBURG.doubleValue())))
            .andExpect(jsonPath("$.[*].badenWuerttemberg").value(hasItem(DEFAULT_BADEN_WUERTTEMBERG.doubleValue())))
            .andExpect(jsonPath("$.[*].bayern").value(hasItem(DEFAULT_BAYERN.doubleValue())))
            .andExpect(jsonPath("$.[*].hessen").value(hasItem(DEFAULT_HESSEN.doubleValue())))
            .andExpect(jsonPath("$.[*].mecklenburgVorpommern").value(hasItem(DEFAULT_MECKLENBURG_VORPOMMERN.doubleValue())))
            .andExpect(jsonPath("$.[*].niedersachsen").value(hasItem(DEFAULT_NIEDERSACHSEN.doubleValue())))
            .andExpect(jsonPath("$.[*].niedersachsenHamburgBremen").value(hasItem(DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN.doubleValue())))
            .andExpect(jsonPath("$.[*].nordrheinWestfalen").value(hasItem(DEFAULT_NORDRHEIN_WESTFALEN.doubleValue())))
            .andExpect(jsonPath("$.[*].rheinlandPfalz").value(hasItem(DEFAULT_RHEINLAND_PFALZ.doubleValue())))
            .andExpect(jsonPath("$.[*].schleswigHolstein").value(hasItem(DEFAULT_SCHLESWIG_HOLSTEIN.doubleValue())))
            .andExpect(jsonPath("$.[*].saarland").value(hasItem(DEFAULT_SAARLAND.doubleValue())))
            .andExpect(jsonPath("$.[*].sachsen").value(hasItem(DEFAULT_SACHSEN.doubleValue())))
            .andExpect(jsonPath("$.[*].sachsenAnhalt").value(hasItem(DEFAULT_SACHSEN_ANHALT.doubleValue())))
            .andExpect(jsonPath("$.[*].thueringenSachsenAnhalt").value(hasItem(DEFAULT_THUERINGEN_SACHSEN_ANHALT.doubleValue())))
            .andExpect(jsonPath("$.[*].thueringen").value(hasItem(DEFAULT_THUERINGEN.doubleValue())))
            .andExpect(jsonPath("$.[*].deutschland").value(hasItem(DEFAULT_DEUTSCHLAND.doubleValue())));
    }

    @Test
    @Transactional
    void getDWDMeasurement() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get the dWDMeasurement
        restDWDMeasurementMockMvc
            .perform(get(ENTITY_API_URL_ID, dWDMeasurement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(dWDMeasurement.getId().intValue()))
            .andExpect(jsonPath("$.year").value(DEFAULT_YEAR.intValue()))
            .andExpect(jsonPath("$.statistic").value(DEFAULT_STATISTIC))
            .andExpect(jsonPath("$.brandenburgBerlin").value(DEFAULT_BRANDENBURG_BERLIN.doubleValue()))
            .andExpect(jsonPath("$.brandenburg").value(DEFAULT_BRANDENBURG.doubleValue()))
            .andExpect(jsonPath("$.badenWuerttemberg").value(DEFAULT_BADEN_WUERTTEMBERG.doubleValue()))
            .andExpect(jsonPath("$.bayern").value(DEFAULT_BAYERN.doubleValue()))
            .andExpect(jsonPath("$.hessen").value(DEFAULT_HESSEN.doubleValue()))
            .andExpect(jsonPath("$.mecklenburgVorpommern").value(DEFAULT_MECKLENBURG_VORPOMMERN.doubleValue()))
            .andExpect(jsonPath("$.niedersachsen").value(DEFAULT_NIEDERSACHSEN.doubleValue()))
            .andExpect(jsonPath("$.niedersachsenHamburgBremen").value(DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN.doubleValue()))
            .andExpect(jsonPath("$.nordrheinWestfalen").value(DEFAULT_NORDRHEIN_WESTFALEN.doubleValue()))
            .andExpect(jsonPath("$.rheinlandPfalz").value(DEFAULT_RHEINLAND_PFALZ.doubleValue()))
            .andExpect(jsonPath("$.schleswigHolstein").value(DEFAULT_SCHLESWIG_HOLSTEIN.doubleValue()))
            .andExpect(jsonPath("$.saarland").value(DEFAULT_SAARLAND.doubleValue()))
            .andExpect(jsonPath("$.sachsen").value(DEFAULT_SACHSEN.doubleValue()))
            .andExpect(jsonPath("$.sachsenAnhalt").value(DEFAULT_SACHSEN_ANHALT.doubleValue()))
            .andExpect(jsonPath("$.thueringenSachsenAnhalt").value(DEFAULT_THUERINGEN_SACHSEN_ANHALT.doubleValue()))
            .andExpect(jsonPath("$.thueringen").value(DEFAULT_THUERINGEN.doubleValue()))
            .andExpect(jsonPath("$.deutschland").value(DEFAULT_DEUTSCHLAND.doubleValue()));
    }

    @Test
    @Transactional
    void getDWDMeasurementsByIdFiltering() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        Long id = dWDMeasurement.getId();

        defaultDWDMeasurementShouldBeFound("id.equals=" + id);
        defaultDWDMeasurementShouldNotBeFound("id.notEquals=" + id);

        defaultDWDMeasurementShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultDWDMeasurementShouldNotBeFound("id.greaterThan=" + id);

        defaultDWDMeasurementShouldBeFound("id.lessThanOrEqual=" + id);
        defaultDWDMeasurementShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByYearIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where year equals to DEFAULT_YEAR
        defaultDWDMeasurementShouldBeFound("year.equals=" + DEFAULT_YEAR);

        // Get all the dWDMeasurementList where year equals to UPDATED_YEAR
        defaultDWDMeasurementShouldNotBeFound("year.equals=" + UPDATED_YEAR);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByYearIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where year in DEFAULT_YEAR or UPDATED_YEAR
        defaultDWDMeasurementShouldBeFound("year.in=" + DEFAULT_YEAR + "," + UPDATED_YEAR);

        // Get all the dWDMeasurementList where year equals to UPDATED_YEAR
        defaultDWDMeasurementShouldNotBeFound("year.in=" + UPDATED_YEAR);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByYearIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where year is not null
        defaultDWDMeasurementShouldBeFound("year.specified=true");

        // Get all the dWDMeasurementList where year is null
        defaultDWDMeasurementShouldNotBeFound("year.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByYearIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where year is greater than or equal to DEFAULT_YEAR
        defaultDWDMeasurementShouldBeFound("year.greaterThanOrEqual=" + DEFAULT_YEAR);

        // Get all the dWDMeasurementList where year is greater than or equal to UPDATED_YEAR
        defaultDWDMeasurementShouldNotBeFound("year.greaterThanOrEqual=" + UPDATED_YEAR);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByYearIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where year is less than or equal to DEFAULT_YEAR
        defaultDWDMeasurementShouldBeFound("year.lessThanOrEqual=" + DEFAULT_YEAR);

        // Get all the dWDMeasurementList where year is less than or equal to SMALLER_YEAR
        defaultDWDMeasurementShouldNotBeFound("year.lessThanOrEqual=" + SMALLER_YEAR);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByYearIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where year is less than DEFAULT_YEAR
        defaultDWDMeasurementShouldNotBeFound("year.lessThan=" + DEFAULT_YEAR);

        // Get all the dWDMeasurementList where year is less than UPDATED_YEAR
        defaultDWDMeasurementShouldBeFound("year.lessThan=" + UPDATED_YEAR);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByYearIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where year is greater than DEFAULT_YEAR
        defaultDWDMeasurementShouldNotBeFound("year.greaterThan=" + DEFAULT_YEAR);

        // Get all the dWDMeasurementList where year is greater than SMALLER_YEAR
        defaultDWDMeasurementShouldBeFound("year.greaterThan=" + SMALLER_YEAR);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByStatisticIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where statistic equals to DEFAULT_STATISTIC
        defaultDWDMeasurementShouldBeFound("statistic.equals=" + DEFAULT_STATISTIC);

        // Get all the dWDMeasurementList where statistic equals to UPDATED_STATISTIC
        defaultDWDMeasurementShouldNotBeFound("statistic.equals=" + UPDATED_STATISTIC);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByStatisticIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where statistic in DEFAULT_STATISTIC or UPDATED_STATISTIC
        defaultDWDMeasurementShouldBeFound("statistic.in=" + DEFAULT_STATISTIC + "," + UPDATED_STATISTIC);

        // Get all the dWDMeasurementList where statistic equals to UPDATED_STATISTIC
        defaultDWDMeasurementShouldNotBeFound("statistic.in=" + UPDATED_STATISTIC);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByStatisticIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where statistic is not null
        defaultDWDMeasurementShouldBeFound("statistic.specified=true");

        // Get all the dWDMeasurementList where statistic is null
        defaultDWDMeasurementShouldNotBeFound("statistic.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByStatisticContainsSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where statistic contains DEFAULT_STATISTIC
        defaultDWDMeasurementShouldBeFound("statistic.contains=" + DEFAULT_STATISTIC);

        // Get all the dWDMeasurementList where statistic contains UPDATED_STATISTIC
        defaultDWDMeasurementShouldNotBeFound("statistic.contains=" + UPDATED_STATISTIC);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByStatisticNotContainsSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where statistic does not contain DEFAULT_STATISTIC
        defaultDWDMeasurementShouldNotBeFound("statistic.doesNotContain=" + DEFAULT_STATISTIC);

        // Get all the dWDMeasurementList where statistic does not contain UPDATED_STATISTIC
        defaultDWDMeasurementShouldBeFound("statistic.doesNotContain=" + UPDATED_STATISTIC);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgBerlinIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburgBerlin equals to DEFAULT_BRANDENBURG_BERLIN
        defaultDWDMeasurementShouldBeFound("brandenburgBerlin.equals=" + DEFAULT_BRANDENBURG_BERLIN);

        // Get all the dWDMeasurementList where brandenburgBerlin equals to UPDATED_BRANDENBURG_BERLIN
        defaultDWDMeasurementShouldNotBeFound("brandenburgBerlin.equals=" + UPDATED_BRANDENBURG_BERLIN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgBerlinIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburgBerlin in DEFAULT_BRANDENBURG_BERLIN or UPDATED_BRANDENBURG_BERLIN
        defaultDWDMeasurementShouldBeFound("brandenburgBerlin.in=" + DEFAULT_BRANDENBURG_BERLIN + "," + UPDATED_BRANDENBURG_BERLIN);

        // Get all the dWDMeasurementList where brandenburgBerlin equals to UPDATED_BRANDENBURG_BERLIN
        defaultDWDMeasurementShouldNotBeFound("brandenburgBerlin.in=" + UPDATED_BRANDENBURG_BERLIN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgBerlinIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburgBerlin is not null
        defaultDWDMeasurementShouldBeFound("brandenburgBerlin.specified=true");

        // Get all the dWDMeasurementList where brandenburgBerlin is null
        defaultDWDMeasurementShouldNotBeFound("brandenburgBerlin.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgBerlinIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburgBerlin is greater than or equal to DEFAULT_BRANDENBURG_BERLIN
        defaultDWDMeasurementShouldBeFound("brandenburgBerlin.greaterThanOrEqual=" + DEFAULT_BRANDENBURG_BERLIN);

        // Get all the dWDMeasurementList where brandenburgBerlin is greater than or equal to UPDATED_BRANDENBURG_BERLIN
        defaultDWDMeasurementShouldNotBeFound("brandenburgBerlin.greaterThanOrEqual=" + UPDATED_BRANDENBURG_BERLIN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgBerlinIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburgBerlin is less than or equal to DEFAULT_BRANDENBURG_BERLIN
        defaultDWDMeasurementShouldBeFound("brandenburgBerlin.lessThanOrEqual=" + DEFAULT_BRANDENBURG_BERLIN);

        // Get all the dWDMeasurementList where brandenburgBerlin is less than or equal to SMALLER_BRANDENBURG_BERLIN
        defaultDWDMeasurementShouldNotBeFound("brandenburgBerlin.lessThanOrEqual=" + SMALLER_BRANDENBURG_BERLIN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgBerlinIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburgBerlin is less than DEFAULT_BRANDENBURG_BERLIN
        defaultDWDMeasurementShouldNotBeFound("brandenburgBerlin.lessThan=" + DEFAULT_BRANDENBURG_BERLIN);

        // Get all the dWDMeasurementList where brandenburgBerlin is less than UPDATED_BRANDENBURG_BERLIN
        defaultDWDMeasurementShouldBeFound("brandenburgBerlin.lessThan=" + UPDATED_BRANDENBURG_BERLIN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgBerlinIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburgBerlin is greater than DEFAULT_BRANDENBURG_BERLIN
        defaultDWDMeasurementShouldNotBeFound("brandenburgBerlin.greaterThan=" + DEFAULT_BRANDENBURG_BERLIN);

        // Get all the dWDMeasurementList where brandenburgBerlin is greater than SMALLER_BRANDENBURG_BERLIN
        defaultDWDMeasurementShouldBeFound("brandenburgBerlin.greaterThan=" + SMALLER_BRANDENBURG_BERLIN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburg equals to DEFAULT_BRANDENBURG
        defaultDWDMeasurementShouldBeFound("brandenburg.equals=" + DEFAULT_BRANDENBURG);

        // Get all the dWDMeasurementList where brandenburg equals to UPDATED_BRANDENBURG
        defaultDWDMeasurementShouldNotBeFound("brandenburg.equals=" + UPDATED_BRANDENBURG);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburg in DEFAULT_BRANDENBURG or UPDATED_BRANDENBURG
        defaultDWDMeasurementShouldBeFound("brandenburg.in=" + DEFAULT_BRANDENBURG + "," + UPDATED_BRANDENBURG);

        // Get all the dWDMeasurementList where brandenburg equals to UPDATED_BRANDENBURG
        defaultDWDMeasurementShouldNotBeFound("brandenburg.in=" + UPDATED_BRANDENBURG);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburg is not null
        defaultDWDMeasurementShouldBeFound("brandenburg.specified=true");

        // Get all the dWDMeasurementList where brandenburg is null
        defaultDWDMeasurementShouldNotBeFound("brandenburg.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburg is greater than or equal to DEFAULT_BRANDENBURG
        defaultDWDMeasurementShouldBeFound("brandenburg.greaterThanOrEqual=" + DEFAULT_BRANDENBURG);

        // Get all the dWDMeasurementList where brandenburg is greater than or equal to UPDATED_BRANDENBURG
        defaultDWDMeasurementShouldNotBeFound("brandenburg.greaterThanOrEqual=" + UPDATED_BRANDENBURG);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburg is less than or equal to DEFAULT_BRANDENBURG
        defaultDWDMeasurementShouldBeFound("brandenburg.lessThanOrEqual=" + DEFAULT_BRANDENBURG);

        // Get all the dWDMeasurementList where brandenburg is less than or equal to SMALLER_BRANDENBURG
        defaultDWDMeasurementShouldNotBeFound("brandenburg.lessThanOrEqual=" + SMALLER_BRANDENBURG);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburg is less than DEFAULT_BRANDENBURG
        defaultDWDMeasurementShouldNotBeFound("brandenburg.lessThan=" + DEFAULT_BRANDENBURG);

        // Get all the dWDMeasurementList where brandenburg is less than UPDATED_BRANDENBURG
        defaultDWDMeasurementShouldBeFound("brandenburg.lessThan=" + UPDATED_BRANDENBURG);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBrandenburgIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where brandenburg is greater than DEFAULT_BRANDENBURG
        defaultDWDMeasurementShouldNotBeFound("brandenburg.greaterThan=" + DEFAULT_BRANDENBURG);

        // Get all the dWDMeasurementList where brandenburg is greater than SMALLER_BRANDENBURG
        defaultDWDMeasurementShouldBeFound("brandenburg.greaterThan=" + SMALLER_BRANDENBURG);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBadenWuerttembergIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where badenWuerttemberg equals to DEFAULT_BADEN_WUERTTEMBERG
        defaultDWDMeasurementShouldBeFound("badenWuerttemberg.equals=" + DEFAULT_BADEN_WUERTTEMBERG);

        // Get all the dWDMeasurementList where badenWuerttemberg equals to UPDATED_BADEN_WUERTTEMBERG
        defaultDWDMeasurementShouldNotBeFound("badenWuerttemberg.equals=" + UPDATED_BADEN_WUERTTEMBERG);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBadenWuerttembergIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where badenWuerttemberg in DEFAULT_BADEN_WUERTTEMBERG or UPDATED_BADEN_WUERTTEMBERG
        defaultDWDMeasurementShouldBeFound("badenWuerttemberg.in=" + DEFAULT_BADEN_WUERTTEMBERG + "," + UPDATED_BADEN_WUERTTEMBERG);

        // Get all the dWDMeasurementList where badenWuerttemberg equals to UPDATED_BADEN_WUERTTEMBERG
        defaultDWDMeasurementShouldNotBeFound("badenWuerttemberg.in=" + UPDATED_BADEN_WUERTTEMBERG);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBadenWuerttembergIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where badenWuerttemberg is not null
        defaultDWDMeasurementShouldBeFound("badenWuerttemberg.specified=true");

        // Get all the dWDMeasurementList where badenWuerttemberg is null
        defaultDWDMeasurementShouldNotBeFound("badenWuerttemberg.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBadenWuerttembergIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where badenWuerttemberg is greater than or equal to DEFAULT_BADEN_WUERTTEMBERG
        defaultDWDMeasurementShouldBeFound("badenWuerttemberg.greaterThanOrEqual=" + DEFAULT_BADEN_WUERTTEMBERG);

        // Get all the dWDMeasurementList where badenWuerttemberg is greater than or equal to UPDATED_BADEN_WUERTTEMBERG
        defaultDWDMeasurementShouldNotBeFound("badenWuerttemberg.greaterThanOrEqual=" + UPDATED_BADEN_WUERTTEMBERG);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBadenWuerttembergIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where badenWuerttemberg is less than or equal to DEFAULT_BADEN_WUERTTEMBERG
        defaultDWDMeasurementShouldBeFound("badenWuerttemberg.lessThanOrEqual=" + DEFAULT_BADEN_WUERTTEMBERG);

        // Get all the dWDMeasurementList where badenWuerttemberg is less than or equal to SMALLER_BADEN_WUERTTEMBERG
        defaultDWDMeasurementShouldNotBeFound("badenWuerttemberg.lessThanOrEqual=" + SMALLER_BADEN_WUERTTEMBERG);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBadenWuerttembergIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where badenWuerttemberg is less than DEFAULT_BADEN_WUERTTEMBERG
        defaultDWDMeasurementShouldNotBeFound("badenWuerttemberg.lessThan=" + DEFAULT_BADEN_WUERTTEMBERG);

        // Get all the dWDMeasurementList where badenWuerttemberg is less than UPDATED_BADEN_WUERTTEMBERG
        defaultDWDMeasurementShouldBeFound("badenWuerttemberg.lessThan=" + UPDATED_BADEN_WUERTTEMBERG);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBadenWuerttembergIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where badenWuerttemberg is greater than DEFAULT_BADEN_WUERTTEMBERG
        defaultDWDMeasurementShouldNotBeFound("badenWuerttemberg.greaterThan=" + DEFAULT_BADEN_WUERTTEMBERG);

        // Get all the dWDMeasurementList where badenWuerttemberg is greater than SMALLER_BADEN_WUERTTEMBERG
        defaultDWDMeasurementShouldBeFound("badenWuerttemberg.greaterThan=" + SMALLER_BADEN_WUERTTEMBERG);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBayernIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where bayern equals to DEFAULT_BAYERN
        defaultDWDMeasurementShouldBeFound("bayern.equals=" + DEFAULT_BAYERN);

        // Get all the dWDMeasurementList where bayern equals to UPDATED_BAYERN
        defaultDWDMeasurementShouldNotBeFound("bayern.equals=" + UPDATED_BAYERN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBayernIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where bayern in DEFAULT_BAYERN or UPDATED_BAYERN
        defaultDWDMeasurementShouldBeFound("bayern.in=" + DEFAULT_BAYERN + "," + UPDATED_BAYERN);

        // Get all the dWDMeasurementList where bayern equals to UPDATED_BAYERN
        defaultDWDMeasurementShouldNotBeFound("bayern.in=" + UPDATED_BAYERN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBayernIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where bayern is not null
        defaultDWDMeasurementShouldBeFound("bayern.specified=true");

        // Get all the dWDMeasurementList where bayern is null
        defaultDWDMeasurementShouldNotBeFound("bayern.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBayernIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where bayern is greater than or equal to DEFAULT_BAYERN
        defaultDWDMeasurementShouldBeFound("bayern.greaterThanOrEqual=" + DEFAULT_BAYERN);

        // Get all the dWDMeasurementList where bayern is greater than or equal to UPDATED_BAYERN
        defaultDWDMeasurementShouldNotBeFound("bayern.greaterThanOrEqual=" + UPDATED_BAYERN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBayernIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where bayern is less than or equal to DEFAULT_BAYERN
        defaultDWDMeasurementShouldBeFound("bayern.lessThanOrEqual=" + DEFAULT_BAYERN);

        // Get all the dWDMeasurementList where bayern is less than or equal to SMALLER_BAYERN
        defaultDWDMeasurementShouldNotBeFound("bayern.lessThanOrEqual=" + SMALLER_BAYERN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBayernIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where bayern is less than DEFAULT_BAYERN
        defaultDWDMeasurementShouldNotBeFound("bayern.lessThan=" + DEFAULT_BAYERN);

        // Get all the dWDMeasurementList where bayern is less than UPDATED_BAYERN
        defaultDWDMeasurementShouldBeFound("bayern.lessThan=" + UPDATED_BAYERN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByBayernIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where bayern is greater than DEFAULT_BAYERN
        defaultDWDMeasurementShouldNotBeFound("bayern.greaterThan=" + DEFAULT_BAYERN);

        // Get all the dWDMeasurementList where bayern is greater than SMALLER_BAYERN
        defaultDWDMeasurementShouldBeFound("bayern.greaterThan=" + SMALLER_BAYERN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByHessenIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where hessen equals to DEFAULT_HESSEN
        defaultDWDMeasurementShouldBeFound("hessen.equals=" + DEFAULT_HESSEN);

        // Get all the dWDMeasurementList where hessen equals to UPDATED_HESSEN
        defaultDWDMeasurementShouldNotBeFound("hessen.equals=" + UPDATED_HESSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByHessenIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where hessen in DEFAULT_HESSEN or UPDATED_HESSEN
        defaultDWDMeasurementShouldBeFound("hessen.in=" + DEFAULT_HESSEN + "," + UPDATED_HESSEN);

        // Get all the dWDMeasurementList where hessen equals to UPDATED_HESSEN
        defaultDWDMeasurementShouldNotBeFound("hessen.in=" + UPDATED_HESSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByHessenIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where hessen is not null
        defaultDWDMeasurementShouldBeFound("hessen.specified=true");

        // Get all the dWDMeasurementList where hessen is null
        defaultDWDMeasurementShouldNotBeFound("hessen.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByHessenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where hessen is greater than or equal to DEFAULT_HESSEN
        defaultDWDMeasurementShouldBeFound("hessen.greaterThanOrEqual=" + DEFAULT_HESSEN);

        // Get all the dWDMeasurementList where hessen is greater than or equal to UPDATED_HESSEN
        defaultDWDMeasurementShouldNotBeFound("hessen.greaterThanOrEqual=" + UPDATED_HESSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByHessenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where hessen is less than or equal to DEFAULT_HESSEN
        defaultDWDMeasurementShouldBeFound("hessen.lessThanOrEqual=" + DEFAULT_HESSEN);

        // Get all the dWDMeasurementList where hessen is less than or equal to SMALLER_HESSEN
        defaultDWDMeasurementShouldNotBeFound("hessen.lessThanOrEqual=" + SMALLER_HESSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByHessenIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where hessen is less than DEFAULT_HESSEN
        defaultDWDMeasurementShouldNotBeFound("hessen.lessThan=" + DEFAULT_HESSEN);

        // Get all the dWDMeasurementList where hessen is less than UPDATED_HESSEN
        defaultDWDMeasurementShouldBeFound("hessen.lessThan=" + UPDATED_HESSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByHessenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where hessen is greater than DEFAULT_HESSEN
        defaultDWDMeasurementShouldNotBeFound("hessen.greaterThan=" + DEFAULT_HESSEN);

        // Get all the dWDMeasurementList where hessen is greater than SMALLER_HESSEN
        defaultDWDMeasurementShouldBeFound("hessen.greaterThan=" + SMALLER_HESSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByMecklenburgVorpommernIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where mecklenburgVorpommern equals to DEFAULT_MECKLENBURG_VORPOMMERN
        defaultDWDMeasurementShouldBeFound("mecklenburgVorpommern.equals=" + DEFAULT_MECKLENBURG_VORPOMMERN);

        // Get all the dWDMeasurementList where mecklenburgVorpommern equals to UPDATED_MECKLENBURG_VORPOMMERN
        defaultDWDMeasurementShouldNotBeFound("mecklenburgVorpommern.equals=" + UPDATED_MECKLENBURG_VORPOMMERN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByMecklenburgVorpommernIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where mecklenburgVorpommern in DEFAULT_MECKLENBURG_VORPOMMERN or UPDATED_MECKLENBURG_VORPOMMERN
        defaultDWDMeasurementShouldBeFound(
            "mecklenburgVorpommern.in=" + DEFAULT_MECKLENBURG_VORPOMMERN + "," + UPDATED_MECKLENBURG_VORPOMMERN
        );

        // Get all the dWDMeasurementList where mecklenburgVorpommern equals to UPDATED_MECKLENBURG_VORPOMMERN
        defaultDWDMeasurementShouldNotBeFound("mecklenburgVorpommern.in=" + UPDATED_MECKLENBURG_VORPOMMERN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByMecklenburgVorpommernIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where mecklenburgVorpommern is not null
        defaultDWDMeasurementShouldBeFound("mecklenburgVorpommern.specified=true");

        // Get all the dWDMeasurementList where mecklenburgVorpommern is null
        defaultDWDMeasurementShouldNotBeFound("mecklenburgVorpommern.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByMecklenburgVorpommernIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where mecklenburgVorpommern is greater than or equal to DEFAULT_MECKLENBURG_VORPOMMERN
        defaultDWDMeasurementShouldBeFound("mecklenburgVorpommern.greaterThanOrEqual=" + DEFAULT_MECKLENBURG_VORPOMMERN);

        // Get all the dWDMeasurementList where mecklenburgVorpommern is greater than or equal to UPDATED_MECKLENBURG_VORPOMMERN
        defaultDWDMeasurementShouldNotBeFound("mecklenburgVorpommern.greaterThanOrEqual=" + UPDATED_MECKLENBURG_VORPOMMERN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByMecklenburgVorpommernIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where mecklenburgVorpommern is less than or equal to DEFAULT_MECKLENBURG_VORPOMMERN
        defaultDWDMeasurementShouldBeFound("mecklenburgVorpommern.lessThanOrEqual=" + DEFAULT_MECKLENBURG_VORPOMMERN);

        // Get all the dWDMeasurementList where mecklenburgVorpommern is less than or equal to SMALLER_MECKLENBURG_VORPOMMERN
        defaultDWDMeasurementShouldNotBeFound("mecklenburgVorpommern.lessThanOrEqual=" + SMALLER_MECKLENBURG_VORPOMMERN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByMecklenburgVorpommernIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where mecklenburgVorpommern is less than DEFAULT_MECKLENBURG_VORPOMMERN
        defaultDWDMeasurementShouldNotBeFound("mecklenburgVorpommern.lessThan=" + DEFAULT_MECKLENBURG_VORPOMMERN);

        // Get all the dWDMeasurementList where mecklenburgVorpommern is less than UPDATED_MECKLENBURG_VORPOMMERN
        defaultDWDMeasurementShouldBeFound("mecklenburgVorpommern.lessThan=" + UPDATED_MECKLENBURG_VORPOMMERN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByMecklenburgVorpommernIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where mecklenburgVorpommern is greater than DEFAULT_MECKLENBURG_VORPOMMERN
        defaultDWDMeasurementShouldNotBeFound("mecklenburgVorpommern.greaterThan=" + DEFAULT_MECKLENBURG_VORPOMMERN);

        // Get all the dWDMeasurementList where mecklenburgVorpommern is greater than SMALLER_MECKLENBURG_VORPOMMERN
        defaultDWDMeasurementShouldBeFound("mecklenburgVorpommern.greaterThan=" + SMALLER_MECKLENBURG_VORPOMMERN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsen equals to DEFAULT_NIEDERSACHSEN
        defaultDWDMeasurementShouldBeFound("niedersachsen.equals=" + DEFAULT_NIEDERSACHSEN);

        // Get all the dWDMeasurementList where niedersachsen equals to UPDATED_NIEDERSACHSEN
        defaultDWDMeasurementShouldNotBeFound("niedersachsen.equals=" + UPDATED_NIEDERSACHSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsen in DEFAULT_NIEDERSACHSEN or UPDATED_NIEDERSACHSEN
        defaultDWDMeasurementShouldBeFound("niedersachsen.in=" + DEFAULT_NIEDERSACHSEN + "," + UPDATED_NIEDERSACHSEN);

        // Get all the dWDMeasurementList where niedersachsen equals to UPDATED_NIEDERSACHSEN
        defaultDWDMeasurementShouldNotBeFound("niedersachsen.in=" + UPDATED_NIEDERSACHSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsen is not null
        defaultDWDMeasurementShouldBeFound("niedersachsen.specified=true");

        // Get all the dWDMeasurementList where niedersachsen is null
        defaultDWDMeasurementShouldNotBeFound("niedersachsen.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsen is greater than or equal to DEFAULT_NIEDERSACHSEN
        defaultDWDMeasurementShouldBeFound("niedersachsen.greaterThanOrEqual=" + DEFAULT_NIEDERSACHSEN);

        // Get all the dWDMeasurementList where niedersachsen is greater than or equal to UPDATED_NIEDERSACHSEN
        defaultDWDMeasurementShouldNotBeFound("niedersachsen.greaterThanOrEqual=" + UPDATED_NIEDERSACHSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsen is less than or equal to DEFAULT_NIEDERSACHSEN
        defaultDWDMeasurementShouldBeFound("niedersachsen.lessThanOrEqual=" + DEFAULT_NIEDERSACHSEN);

        // Get all the dWDMeasurementList where niedersachsen is less than or equal to SMALLER_NIEDERSACHSEN
        defaultDWDMeasurementShouldNotBeFound("niedersachsen.lessThanOrEqual=" + SMALLER_NIEDERSACHSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsen is less than DEFAULT_NIEDERSACHSEN
        defaultDWDMeasurementShouldNotBeFound("niedersachsen.lessThan=" + DEFAULT_NIEDERSACHSEN);

        // Get all the dWDMeasurementList where niedersachsen is less than UPDATED_NIEDERSACHSEN
        defaultDWDMeasurementShouldBeFound("niedersachsen.lessThan=" + UPDATED_NIEDERSACHSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsen is greater than DEFAULT_NIEDERSACHSEN
        defaultDWDMeasurementShouldNotBeFound("niedersachsen.greaterThan=" + DEFAULT_NIEDERSACHSEN);

        // Get all the dWDMeasurementList where niedersachsen is greater than SMALLER_NIEDERSACHSEN
        defaultDWDMeasurementShouldBeFound("niedersachsen.greaterThan=" + SMALLER_NIEDERSACHSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenHamburgBremenIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen equals to DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN
        defaultDWDMeasurementShouldBeFound("niedersachsenHamburgBremen.equals=" + DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN);

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen equals to UPDATED_NIEDERSACHSEN_HAMBURG_BREMEN
        defaultDWDMeasurementShouldNotBeFound("niedersachsenHamburgBremen.equals=" + UPDATED_NIEDERSACHSEN_HAMBURG_BREMEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenHamburgBremenIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen in DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN or UPDATED_NIEDERSACHSEN_HAMBURG_BREMEN
        defaultDWDMeasurementShouldBeFound(
            "niedersachsenHamburgBremen.in=" + DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN + "," + UPDATED_NIEDERSACHSEN_HAMBURG_BREMEN
        );

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen equals to UPDATED_NIEDERSACHSEN_HAMBURG_BREMEN
        defaultDWDMeasurementShouldNotBeFound("niedersachsenHamburgBremen.in=" + UPDATED_NIEDERSACHSEN_HAMBURG_BREMEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenHamburgBremenIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen is not null
        defaultDWDMeasurementShouldBeFound("niedersachsenHamburgBremen.specified=true");

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen is null
        defaultDWDMeasurementShouldNotBeFound("niedersachsenHamburgBremen.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenHamburgBremenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen is greater than or equal to DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN
        defaultDWDMeasurementShouldBeFound("niedersachsenHamburgBremen.greaterThanOrEqual=" + DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN);

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen is greater than or equal to UPDATED_NIEDERSACHSEN_HAMBURG_BREMEN
        defaultDWDMeasurementShouldNotBeFound("niedersachsenHamburgBremen.greaterThanOrEqual=" + UPDATED_NIEDERSACHSEN_HAMBURG_BREMEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenHamburgBremenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen is less than or equal to DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN
        defaultDWDMeasurementShouldBeFound("niedersachsenHamburgBremen.lessThanOrEqual=" + DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN);

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen is less than or equal to SMALLER_NIEDERSACHSEN_HAMBURG_BREMEN
        defaultDWDMeasurementShouldNotBeFound("niedersachsenHamburgBremen.lessThanOrEqual=" + SMALLER_NIEDERSACHSEN_HAMBURG_BREMEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenHamburgBremenIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen is less than DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN
        defaultDWDMeasurementShouldNotBeFound("niedersachsenHamburgBremen.lessThan=" + DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN);

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen is less than UPDATED_NIEDERSACHSEN_HAMBURG_BREMEN
        defaultDWDMeasurementShouldBeFound("niedersachsenHamburgBremen.lessThan=" + UPDATED_NIEDERSACHSEN_HAMBURG_BREMEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNiedersachsenHamburgBremenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen is greater than DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN
        defaultDWDMeasurementShouldNotBeFound("niedersachsenHamburgBremen.greaterThan=" + DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN);

        // Get all the dWDMeasurementList where niedersachsenHamburgBremen is greater than SMALLER_NIEDERSACHSEN_HAMBURG_BREMEN
        defaultDWDMeasurementShouldBeFound("niedersachsenHamburgBremen.greaterThan=" + SMALLER_NIEDERSACHSEN_HAMBURG_BREMEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNordrheinWestfalenIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where nordrheinWestfalen equals to DEFAULT_NORDRHEIN_WESTFALEN
        defaultDWDMeasurementShouldBeFound("nordrheinWestfalen.equals=" + DEFAULT_NORDRHEIN_WESTFALEN);

        // Get all the dWDMeasurementList where nordrheinWestfalen equals to UPDATED_NORDRHEIN_WESTFALEN
        defaultDWDMeasurementShouldNotBeFound("nordrheinWestfalen.equals=" + UPDATED_NORDRHEIN_WESTFALEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNordrheinWestfalenIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where nordrheinWestfalen in DEFAULT_NORDRHEIN_WESTFALEN or UPDATED_NORDRHEIN_WESTFALEN
        defaultDWDMeasurementShouldBeFound("nordrheinWestfalen.in=" + DEFAULT_NORDRHEIN_WESTFALEN + "," + UPDATED_NORDRHEIN_WESTFALEN);

        // Get all the dWDMeasurementList where nordrheinWestfalen equals to UPDATED_NORDRHEIN_WESTFALEN
        defaultDWDMeasurementShouldNotBeFound("nordrheinWestfalen.in=" + UPDATED_NORDRHEIN_WESTFALEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNordrheinWestfalenIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where nordrheinWestfalen is not null
        defaultDWDMeasurementShouldBeFound("nordrheinWestfalen.specified=true");

        // Get all the dWDMeasurementList where nordrheinWestfalen is null
        defaultDWDMeasurementShouldNotBeFound("nordrheinWestfalen.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNordrheinWestfalenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where nordrheinWestfalen is greater than or equal to DEFAULT_NORDRHEIN_WESTFALEN
        defaultDWDMeasurementShouldBeFound("nordrheinWestfalen.greaterThanOrEqual=" + DEFAULT_NORDRHEIN_WESTFALEN);

        // Get all the dWDMeasurementList where nordrheinWestfalen is greater than or equal to UPDATED_NORDRHEIN_WESTFALEN
        defaultDWDMeasurementShouldNotBeFound("nordrheinWestfalen.greaterThanOrEqual=" + UPDATED_NORDRHEIN_WESTFALEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNordrheinWestfalenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where nordrheinWestfalen is less than or equal to DEFAULT_NORDRHEIN_WESTFALEN
        defaultDWDMeasurementShouldBeFound("nordrheinWestfalen.lessThanOrEqual=" + DEFAULT_NORDRHEIN_WESTFALEN);

        // Get all the dWDMeasurementList where nordrheinWestfalen is less than or equal to SMALLER_NORDRHEIN_WESTFALEN
        defaultDWDMeasurementShouldNotBeFound("nordrheinWestfalen.lessThanOrEqual=" + SMALLER_NORDRHEIN_WESTFALEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNordrheinWestfalenIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where nordrheinWestfalen is less than DEFAULT_NORDRHEIN_WESTFALEN
        defaultDWDMeasurementShouldNotBeFound("nordrheinWestfalen.lessThan=" + DEFAULT_NORDRHEIN_WESTFALEN);

        // Get all the dWDMeasurementList where nordrheinWestfalen is less than UPDATED_NORDRHEIN_WESTFALEN
        defaultDWDMeasurementShouldBeFound("nordrheinWestfalen.lessThan=" + UPDATED_NORDRHEIN_WESTFALEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByNordrheinWestfalenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where nordrheinWestfalen is greater than DEFAULT_NORDRHEIN_WESTFALEN
        defaultDWDMeasurementShouldNotBeFound("nordrheinWestfalen.greaterThan=" + DEFAULT_NORDRHEIN_WESTFALEN);

        // Get all the dWDMeasurementList where nordrheinWestfalen is greater than SMALLER_NORDRHEIN_WESTFALEN
        defaultDWDMeasurementShouldBeFound("nordrheinWestfalen.greaterThan=" + SMALLER_NORDRHEIN_WESTFALEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByRheinlandPfalzIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where rheinlandPfalz equals to DEFAULT_RHEINLAND_PFALZ
        defaultDWDMeasurementShouldBeFound("rheinlandPfalz.equals=" + DEFAULT_RHEINLAND_PFALZ);

        // Get all the dWDMeasurementList where rheinlandPfalz equals to UPDATED_RHEINLAND_PFALZ
        defaultDWDMeasurementShouldNotBeFound("rheinlandPfalz.equals=" + UPDATED_RHEINLAND_PFALZ);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByRheinlandPfalzIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where rheinlandPfalz in DEFAULT_RHEINLAND_PFALZ or UPDATED_RHEINLAND_PFALZ
        defaultDWDMeasurementShouldBeFound("rheinlandPfalz.in=" + DEFAULT_RHEINLAND_PFALZ + "," + UPDATED_RHEINLAND_PFALZ);

        // Get all the dWDMeasurementList where rheinlandPfalz equals to UPDATED_RHEINLAND_PFALZ
        defaultDWDMeasurementShouldNotBeFound("rheinlandPfalz.in=" + UPDATED_RHEINLAND_PFALZ);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByRheinlandPfalzIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where rheinlandPfalz is not null
        defaultDWDMeasurementShouldBeFound("rheinlandPfalz.specified=true");

        // Get all the dWDMeasurementList where rheinlandPfalz is null
        defaultDWDMeasurementShouldNotBeFound("rheinlandPfalz.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByRheinlandPfalzIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where rheinlandPfalz is greater than or equal to DEFAULT_RHEINLAND_PFALZ
        defaultDWDMeasurementShouldBeFound("rheinlandPfalz.greaterThanOrEqual=" + DEFAULT_RHEINLAND_PFALZ);

        // Get all the dWDMeasurementList where rheinlandPfalz is greater than or equal to UPDATED_RHEINLAND_PFALZ
        defaultDWDMeasurementShouldNotBeFound("rheinlandPfalz.greaterThanOrEqual=" + UPDATED_RHEINLAND_PFALZ);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByRheinlandPfalzIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where rheinlandPfalz is less than or equal to DEFAULT_RHEINLAND_PFALZ
        defaultDWDMeasurementShouldBeFound("rheinlandPfalz.lessThanOrEqual=" + DEFAULT_RHEINLAND_PFALZ);

        // Get all the dWDMeasurementList where rheinlandPfalz is less than or equal to SMALLER_RHEINLAND_PFALZ
        defaultDWDMeasurementShouldNotBeFound("rheinlandPfalz.lessThanOrEqual=" + SMALLER_RHEINLAND_PFALZ);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByRheinlandPfalzIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where rheinlandPfalz is less than DEFAULT_RHEINLAND_PFALZ
        defaultDWDMeasurementShouldNotBeFound("rheinlandPfalz.lessThan=" + DEFAULT_RHEINLAND_PFALZ);

        // Get all the dWDMeasurementList where rheinlandPfalz is less than UPDATED_RHEINLAND_PFALZ
        defaultDWDMeasurementShouldBeFound("rheinlandPfalz.lessThan=" + UPDATED_RHEINLAND_PFALZ);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByRheinlandPfalzIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where rheinlandPfalz is greater than DEFAULT_RHEINLAND_PFALZ
        defaultDWDMeasurementShouldNotBeFound("rheinlandPfalz.greaterThan=" + DEFAULT_RHEINLAND_PFALZ);

        // Get all the dWDMeasurementList where rheinlandPfalz is greater than SMALLER_RHEINLAND_PFALZ
        defaultDWDMeasurementShouldBeFound("rheinlandPfalz.greaterThan=" + SMALLER_RHEINLAND_PFALZ);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySchleswigHolsteinIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where schleswigHolstein equals to DEFAULT_SCHLESWIG_HOLSTEIN
        defaultDWDMeasurementShouldBeFound("schleswigHolstein.equals=" + DEFAULT_SCHLESWIG_HOLSTEIN);

        // Get all the dWDMeasurementList where schleswigHolstein equals to UPDATED_SCHLESWIG_HOLSTEIN
        defaultDWDMeasurementShouldNotBeFound("schleswigHolstein.equals=" + UPDATED_SCHLESWIG_HOLSTEIN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySchleswigHolsteinIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where schleswigHolstein in DEFAULT_SCHLESWIG_HOLSTEIN or UPDATED_SCHLESWIG_HOLSTEIN
        defaultDWDMeasurementShouldBeFound("schleswigHolstein.in=" + DEFAULT_SCHLESWIG_HOLSTEIN + "," + UPDATED_SCHLESWIG_HOLSTEIN);

        // Get all the dWDMeasurementList where schleswigHolstein equals to UPDATED_SCHLESWIG_HOLSTEIN
        defaultDWDMeasurementShouldNotBeFound("schleswigHolstein.in=" + UPDATED_SCHLESWIG_HOLSTEIN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySchleswigHolsteinIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where schleswigHolstein is not null
        defaultDWDMeasurementShouldBeFound("schleswigHolstein.specified=true");

        // Get all the dWDMeasurementList where schleswigHolstein is null
        defaultDWDMeasurementShouldNotBeFound("schleswigHolstein.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySchleswigHolsteinIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where schleswigHolstein is greater than or equal to DEFAULT_SCHLESWIG_HOLSTEIN
        defaultDWDMeasurementShouldBeFound("schleswigHolstein.greaterThanOrEqual=" + DEFAULT_SCHLESWIG_HOLSTEIN);

        // Get all the dWDMeasurementList where schleswigHolstein is greater than or equal to UPDATED_SCHLESWIG_HOLSTEIN
        defaultDWDMeasurementShouldNotBeFound("schleswigHolstein.greaterThanOrEqual=" + UPDATED_SCHLESWIG_HOLSTEIN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySchleswigHolsteinIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where schleswigHolstein is less than or equal to DEFAULT_SCHLESWIG_HOLSTEIN
        defaultDWDMeasurementShouldBeFound("schleswigHolstein.lessThanOrEqual=" + DEFAULT_SCHLESWIG_HOLSTEIN);

        // Get all the dWDMeasurementList where schleswigHolstein is less than or equal to SMALLER_SCHLESWIG_HOLSTEIN
        defaultDWDMeasurementShouldNotBeFound("schleswigHolstein.lessThanOrEqual=" + SMALLER_SCHLESWIG_HOLSTEIN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySchleswigHolsteinIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where schleswigHolstein is less than DEFAULT_SCHLESWIG_HOLSTEIN
        defaultDWDMeasurementShouldNotBeFound("schleswigHolstein.lessThan=" + DEFAULT_SCHLESWIG_HOLSTEIN);

        // Get all the dWDMeasurementList where schleswigHolstein is less than UPDATED_SCHLESWIG_HOLSTEIN
        defaultDWDMeasurementShouldBeFound("schleswigHolstein.lessThan=" + UPDATED_SCHLESWIG_HOLSTEIN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySchleswigHolsteinIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where schleswigHolstein is greater than DEFAULT_SCHLESWIG_HOLSTEIN
        defaultDWDMeasurementShouldNotBeFound("schleswigHolstein.greaterThan=" + DEFAULT_SCHLESWIG_HOLSTEIN);

        // Get all the dWDMeasurementList where schleswigHolstein is greater than SMALLER_SCHLESWIG_HOLSTEIN
        defaultDWDMeasurementShouldBeFound("schleswigHolstein.greaterThan=" + SMALLER_SCHLESWIG_HOLSTEIN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySaarlandIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where saarland equals to DEFAULT_SAARLAND
        defaultDWDMeasurementShouldBeFound("saarland.equals=" + DEFAULT_SAARLAND);

        // Get all the dWDMeasurementList where saarland equals to UPDATED_SAARLAND
        defaultDWDMeasurementShouldNotBeFound("saarland.equals=" + UPDATED_SAARLAND);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySaarlandIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where saarland in DEFAULT_SAARLAND or UPDATED_SAARLAND
        defaultDWDMeasurementShouldBeFound("saarland.in=" + DEFAULT_SAARLAND + "," + UPDATED_SAARLAND);

        // Get all the dWDMeasurementList where saarland equals to UPDATED_SAARLAND
        defaultDWDMeasurementShouldNotBeFound("saarland.in=" + UPDATED_SAARLAND);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySaarlandIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where saarland is not null
        defaultDWDMeasurementShouldBeFound("saarland.specified=true");

        // Get all the dWDMeasurementList where saarland is null
        defaultDWDMeasurementShouldNotBeFound("saarland.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySaarlandIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where saarland is greater than or equal to DEFAULT_SAARLAND
        defaultDWDMeasurementShouldBeFound("saarland.greaterThanOrEqual=" + DEFAULT_SAARLAND);

        // Get all the dWDMeasurementList where saarland is greater than or equal to UPDATED_SAARLAND
        defaultDWDMeasurementShouldNotBeFound("saarland.greaterThanOrEqual=" + UPDATED_SAARLAND);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySaarlandIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where saarland is less than or equal to DEFAULT_SAARLAND
        defaultDWDMeasurementShouldBeFound("saarland.lessThanOrEqual=" + DEFAULT_SAARLAND);

        // Get all the dWDMeasurementList where saarland is less than or equal to SMALLER_SAARLAND
        defaultDWDMeasurementShouldNotBeFound("saarland.lessThanOrEqual=" + SMALLER_SAARLAND);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySaarlandIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where saarland is less than DEFAULT_SAARLAND
        defaultDWDMeasurementShouldNotBeFound("saarland.lessThan=" + DEFAULT_SAARLAND);

        // Get all the dWDMeasurementList where saarland is less than UPDATED_SAARLAND
        defaultDWDMeasurementShouldBeFound("saarland.lessThan=" + UPDATED_SAARLAND);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySaarlandIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where saarland is greater than DEFAULT_SAARLAND
        defaultDWDMeasurementShouldNotBeFound("saarland.greaterThan=" + DEFAULT_SAARLAND);

        // Get all the dWDMeasurementList where saarland is greater than SMALLER_SAARLAND
        defaultDWDMeasurementShouldBeFound("saarland.greaterThan=" + SMALLER_SAARLAND);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsen equals to DEFAULT_SACHSEN
        defaultDWDMeasurementShouldBeFound("sachsen.equals=" + DEFAULT_SACHSEN);

        // Get all the dWDMeasurementList where sachsen equals to UPDATED_SACHSEN
        defaultDWDMeasurementShouldNotBeFound("sachsen.equals=" + UPDATED_SACHSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsen in DEFAULT_SACHSEN or UPDATED_SACHSEN
        defaultDWDMeasurementShouldBeFound("sachsen.in=" + DEFAULT_SACHSEN + "," + UPDATED_SACHSEN);

        // Get all the dWDMeasurementList where sachsen equals to UPDATED_SACHSEN
        defaultDWDMeasurementShouldNotBeFound("sachsen.in=" + UPDATED_SACHSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsen is not null
        defaultDWDMeasurementShouldBeFound("sachsen.specified=true");

        // Get all the dWDMeasurementList where sachsen is null
        defaultDWDMeasurementShouldNotBeFound("sachsen.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsen is greater than or equal to DEFAULT_SACHSEN
        defaultDWDMeasurementShouldBeFound("sachsen.greaterThanOrEqual=" + DEFAULT_SACHSEN);

        // Get all the dWDMeasurementList where sachsen is greater than or equal to UPDATED_SACHSEN
        defaultDWDMeasurementShouldNotBeFound("sachsen.greaterThanOrEqual=" + UPDATED_SACHSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsen is less than or equal to DEFAULT_SACHSEN
        defaultDWDMeasurementShouldBeFound("sachsen.lessThanOrEqual=" + DEFAULT_SACHSEN);

        // Get all the dWDMeasurementList where sachsen is less than or equal to SMALLER_SACHSEN
        defaultDWDMeasurementShouldNotBeFound("sachsen.lessThanOrEqual=" + SMALLER_SACHSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsen is less than DEFAULT_SACHSEN
        defaultDWDMeasurementShouldNotBeFound("sachsen.lessThan=" + DEFAULT_SACHSEN);

        // Get all the dWDMeasurementList where sachsen is less than UPDATED_SACHSEN
        defaultDWDMeasurementShouldBeFound("sachsen.lessThan=" + UPDATED_SACHSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsen is greater than DEFAULT_SACHSEN
        defaultDWDMeasurementShouldNotBeFound("sachsen.greaterThan=" + DEFAULT_SACHSEN);

        // Get all the dWDMeasurementList where sachsen is greater than SMALLER_SACHSEN
        defaultDWDMeasurementShouldBeFound("sachsen.greaterThan=" + SMALLER_SACHSEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenAnhaltIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsenAnhalt equals to DEFAULT_SACHSEN_ANHALT
        defaultDWDMeasurementShouldBeFound("sachsenAnhalt.equals=" + DEFAULT_SACHSEN_ANHALT);

        // Get all the dWDMeasurementList where sachsenAnhalt equals to UPDATED_SACHSEN_ANHALT
        defaultDWDMeasurementShouldNotBeFound("sachsenAnhalt.equals=" + UPDATED_SACHSEN_ANHALT);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenAnhaltIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsenAnhalt in DEFAULT_SACHSEN_ANHALT or UPDATED_SACHSEN_ANHALT
        defaultDWDMeasurementShouldBeFound("sachsenAnhalt.in=" + DEFAULT_SACHSEN_ANHALT + "," + UPDATED_SACHSEN_ANHALT);

        // Get all the dWDMeasurementList where sachsenAnhalt equals to UPDATED_SACHSEN_ANHALT
        defaultDWDMeasurementShouldNotBeFound("sachsenAnhalt.in=" + UPDATED_SACHSEN_ANHALT);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenAnhaltIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsenAnhalt is not null
        defaultDWDMeasurementShouldBeFound("sachsenAnhalt.specified=true");

        // Get all the dWDMeasurementList where sachsenAnhalt is null
        defaultDWDMeasurementShouldNotBeFound("sachsenAnhalt.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenAnhaltIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsenAnhalt is greater than or equal to DEFAULT_SACHSEN_ANHALT
        defaultDWDMeasurementShouldBeFound("sachsenAnhalt.greaterThanOrEqual=" + DEFAULT_SACHSEN_ANHALT);

        // Get all the dWDMeasurementList where sachsenAnhalt is greater than or equal to UPDATED_SACHSEN_ANHALT
        defaultDWDMeasurementShouldNotBeFound("sachsenAnhalt.greaterThanOrEqual=" + UPDATED_SACHSEN_ANHALT);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenAnhaltIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsenAnhalt is less than or equal to DEFAULT_SACHSEN_ANHALT
        defaultDWDMeasurementShouldBeFound("sachsenAnhalt.lessThanOrEqual=" + DEFAULT_SACHSEN_ANHALT);

        // Get all the dWDMeasurementList where sachsenAnhalt is less than or equal to SMALLER_SACHSEN_ANHALT
        defaultDWDMeasurementShouldNotBeFound("sachsenAnhalt.lessThanOrEqual=" + SMALLER_SACHSEN_ANHALT);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenAnhaltIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsenAnhalt is less than DEFAULT_SACHSEN_ANHALT
        defaultDWDMeasurementShouldNotBeFound("sachsenAnhalt.lessThan=" + DEFAULT_SACHSEN_ANHALT);

        // Get all the dWDMeasurementList where sachsenAnhalt is less than UPDATED_SACHSEN_ANHALT
        defaultDWDMeasurementShouldBeFound("sachsenAnhalt.lessThan=" + UPDATED_SACHSEN_ANHALT);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsBySachsenAnhaltIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where sachsenAnhalt is greater than DEFAULT_SACHSEN_ANHALT
        defaultDWDMeasurementShouldNotBeFound("sachsenAnhalt.greaterThan=" + DEFAULT_SACHSEN_ANHALT);

        // Get all the dWDMeasurementList where sachsenAnhalt is greater than SMALLER_SACHSEN_ANHALT
        defaultDWDMeasurementShouldBeFound("sachsenAnhalt.greaterThan=" + SMALLER_SACHSEN_ANHALT);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenSachsenAnhaltIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt equals to DEFAULT_THUERINGEN_SACHSEN_ANHALT
        defaultDWDMeasurementShouldBeFound("thueringenSachsenAnhalt.equals=" + DEFAULT_THUERINGEN_SACHSEN_ANHALT);

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt equals to UPDATED_THUERINGEN_SACHSEN_ANHALT
        defaultDWDMeasurementShouldNotBeFound("thueringenSachsenAnhalt.equals=" + UPDATED_THUERINGEN_SACHSEN_ANHALT);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenSachsenAnhaltIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt in DEFAULT_THUERINGEN_SACHSEN_ANHALT or UPDATED_THUERINGEN_SACHSEN_ANHALT
        defaultDWDMeasurementShouldBeFound(
            "thueringenSachsenAnhalt.in=" + DEFAULT_THUERINGEN_SACHSEN_ANHALT + "," + UPDATED_THUERINGEN_SACHSEN_ANHALT
        );

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt equals to UPDATED_THUERINGEN_SACHSEN_ANHALT
        defaultDWDMeasurementShouldNotBeFound("thueringenSachsenAnhalt.in=" + UPDATED_THUERINGEN_SACHSEN_ANHALT);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenSachsenAnhaltIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt is not null
        defaultDWDMeasurementShouldBeFound("thueringenSachsenAnhalt.specified=true");

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt is null
        defaultDWDMeasurementShouldNotBeFound("thueringenSachsenAnhalt.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenSachsenAnhaltIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt is greater than or equal to DEFAULT_THUERINGEN_SACHSEN_ANHALT
        defaultDWDMeasurementShouldBeFound("thueringenSachsenAnhalt.greaterThanOrEqual=" + DEFAULT_THUERINGEN_SACHSEN_ANHALT);

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt is greater than or equal to UPDATED_THUERINGEN_SACHSEN_ANHALT
        defaultDWDMeasurementShouldNotBeFound("thueringenSachsenAnhalt.greaterThanOrEqual=" + UPDATED_THUERINGEN_SACHSEN_ANHALT);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenSachsenAnhaltIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt is less than or equal to DEFAULT_THUERINGEN_SACHSEN_ANHALT
        defaultDWDMeasurementShouldBeFound("thueringenSachsenAnhalt.lessThanOrEqual=" + DEFAULT_THUERINGEN_SACHSEN_ANHALT);

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt is less than or equal to SMALLER_THUERINGEN_SACHSEN_ANHALT
        defaultDWDMeasurementShouldNotBeFound("thueringenSachsenAnhalt.lessThanOrEqual=" + SMALLER_THUERINGEN_SACHSEN_ANHALT);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenSachsenAnhaltIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt is less than DEFAULT_THUERINGEN_SACHSEN_ANHALT
        defaultDWDMeasurementShouldNotBeFound("thueringenSachsenAnhalt.lessThan=" + DEFAULT_THUERINGEN_SACHSEN_ANHALT);

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt is less than UPDATED_THUERINGEN_SACHSEN_ANHALT
        defaultDWDMeasurementShouldBeFound("thueringenSachsenAnhalt.lessThan=" + UPDATED_THUERINGEN_SACHSEN_ANHALT);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenSachsenAnhaltIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt is greater than DEFAULT_THUERINGEN_SACHSEN_ANHALT
        defaultDWDMeasurementShouldNotBeFound("thueringenSachsenAnhalt.greaterThan=" + DEFAULT_THUERINGEN_SACHSEN_ANHALT);

        // Get all the dWDMeasurementList where thueringenSachsenAnhalt is greater than SMALLER_THUERINGEN_SACHSEN_ANHALT
        defaultDWDMeasurementShouldBeFound("thueringenSachsenAnhalt.greaterThan=" + SMALLER_THUERINGEN_SACHSEN_ANHALT);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringen equals to DEFAULT_THUERINGEN
        defaultDWDMeasurementShouldBeFound("thueringen.equals=" + DEFAULT_THUERINGEN);

        // Get all the dWDMeasurementList where thueringen equals to UPDATED_THUERINGEN
        defaultDWDMeasurementShouldNotBeFound("thueringen.equals=" + UPDATED_THUERINGEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringen in DEFAULT_THUERINGEN or UPDATED_THUERINGEN
        defaultDWDMeasurementShouldBeFound("thueringen.in=" + DEFAULT_THUERINGEN + "," + UPDATED_THUERINGEN);

        // Get all the dWDMeasurementList where thueringen equals to UPDATED_THUERINGEN
        defaultDWDMeasurementShouldNotBeFound("thueringen.in=" + UPDATED_THUERINGEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringen is not null
        defaultDWDMeasurementShouldBeFound("thueringen.specified=true");

        // Get all the dWDMeasurementList where thueringen is null
        defaultDWDMeasurementShouldNotBeFound("thueringen.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringen is greater than or equal to DEFAULT_THUERINGEN
        defaultDWDMeasurementShouldBeFound("thueringen.greaterThanOrEqual=" + DEFAULT_THUERINGEN);

        // Get all the dWDMeasurementList where thueringen is greater than or equal to UPDATED_THUERINGEN
        defaultDWDMeasurementShouldNotBeFound("thueringen.greaterThanOrEqual=" + UPDATED_THUERINGEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringen is less than or equal to DEFAULT_THUERINGEN
        defaultDWDMeasurementShouldBeFound("thueringen.lessThanOrEqual=" + DEFAULT_THUERINGEN);

        // Get all the dWDMeasurementList where thueringen is less than or equal to SMALLER_THUERINGEN
        defaultDWDMeasurementShouldNotBeFound("thueringen.lessThanOrEqual=" + SMALLER_THUERINGEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringen is less than DEFAULT_THUERINGEN
        defaultDWDMeasurementShouldNotBeFound("thueringen.lessThan=" + DEFAULT_THUERINGEN);

        // Get all the dWDMeasurementList where thueringen is less than UPDATED_THUERINGEN
        defaultDWDMeasurementShouldBeFound("thueringen.lessThan=" + UPDATED_THUERINGEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByThueringenIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where thueringen is greater than DEFAULT_THUERINGEN
        defaultDWDMeasurementShouldNotBeFound("thueringen.greaterThan=" + DEFAULT_THUERINGEN);

        // Get all the dWDMeasurementList where thueringen is greater than SMALLER_THUERINGEN
        defaultDWDMeasurementShouldBeFound("thueringen.greaterThan=" + SMALLER_THUERINGEN);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByDeutschlandIsEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where deutschland equals to DEFAULT_DEUTSCHLAND
        defaultDWDMeasurementShouldBeFound("deutschland.equals=" + DEFAULT_DEUTSCHLAND);

        // Get all the dWDMeasurementList where deutschland equals to UPDATED_DEUTSCHLAND
        defaultDWDMeasurementShouldNotBeFound("deutschland.equals=" + UPDATED_DEUTSCHLAND);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByDeutschlandIsInShouldWork() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where deutschland in DEFAULT_DEUTSCHLAND or UPDATED_DEUTSCHLAND
        defaultDWDMeasurementShouldBeFound("deutschland.in=" + DEFAULT_DEUTSCHLAND + "," + UPDATED_DEUTSCHLAND);

        // Get all the dWDMeasurementList where deutschland equals to UPDATED_DEUTSCHLAND
        defaultDWDMeasurementShouldNotBeFound("deutschland.in=" + UPDATED_DEUTSCHLAND);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByDeutschlandIsNullOrNotNull() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where deutschland is not null
        defaultDWDMeasurementShouldBeFound("deutschland.specified=true");

        // Get all the dWDMeasurementList where deutschland is null
        defaultDWDMeasurementShouldNotBeFound("deutschland.specified=false");
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByDeutschlandIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where deutschland is greater than or equal to DEFAULT_DEUTSCHLAND
        defaultDWDMeasurementShouldBeFound("deutschland.greaterThanOrEqual=" + DEFAULT_DEUTSCHLAND);

        // Get all the dWDMeasurementList where deutschland is greater than or equal to UPDATED_DEUTSCHLAND
        defaultDWDMeasurementShouldNotBeFound("deutschland.greaterThanOrEqual=" + UPDATED_DEUTSCHLAND);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByDeutschlandIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where deutschland is less than or equal to DEFAULT_DEUTSCHLAND
        defaultDWDMeasurementShouldBeFound("deutschland.lessThanOrEqual=" + DEFAULT_DEUTSCHLAND);

        // Get all the dWDMeasurementList where deutschland is less than or equal to SMALLER_DEUTSCHLAND
        defaultDWDMeasurementShouldNotBeFound("deutschland.lessThanOrEqual=" + SMALLER_DEUTSCHLAND);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByDeutschlandIsLessThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where deutschland is less than DEFAULT_DEUTSCHLAND
        defaultDWDMeasurementShouldNotBeFound("deutschland.lessThan=" + DEFAULT_DEUTSCHLAND);

        // Get all the dWDMeasurementList where deutschland is less than UPDATED_DEUTSCHLAND
        defaultDWDMeasurementShouldBeFound("deutschland.lessThan=" + UPDATED_DEUTSCHLAND);
    }

    @Test
    @Transactional
    void getAllDWDMeasurementsByDeutschlandIsGreaterThanSomething() throws Exception {
        // Initialize the database
        dWDMeasurementRepository.saveAndFlush(dWDMeasurement);

        // Get all the dWDMeasurementList where deutschland is greater than DEFAULT_DEUTSCHLAND
        defaultDWDMeasurementShouldNotBeFound("deutschland.greaterThan=" + DEFAULT_DEUTSCHLAND);

        // Get all the dWDMeasurementList where deutschland is greater than SMALLER_DEUTSCHLAND
        defaultDWDMeasurementShouldBeFound("deutschland.greaterThan=" + SMALLER_DEUTSCHLAND);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultDWDMeasurementShouldBeFound(String filter) throws Exception {
        restDWDMeasurementMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dWDMeasurement.getId().intValue())))
            .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR.intValue())))
            .andExpect(jsonPath("$.[*].statistic").value(hasItem(DEFAULT_STATISTIC)))
            .andExpect(jsonPath("$.[*].brandenburgBerlin").value(hasItem(DEFAULT_BRANDENBURG_BERLIN.doubleValue())))
            .andExpect(jsonPath("$.[*].brandenburg").value(hasItem(DEFAULT_BRANDENBURG.doubleValue())))
            .andExpect(jsonPath("$.[*].badenWuerttemberg").value(hasItem(DEFAULT_BADEN_WUERTTEMBERG.doubleValue())))
            .andExpect(jsonPath("$.[*].bayern").value(hasItem(DEFAULT_BAYERN.doubleValue())))
            .andExpect(jsonPath("$.[*].hessen").value(hasItem(DEFAULT_HESSEN.doubleValue())))
            .andExpect(jsonPath("$.[*].mecklenburgVorpommern").value(hasItem(DEFAULT_MECKLENBURG_VORPOMMERN.doubleValue())))
            .andExpect(jsonPath("$.[*].niedersachsen").value(hasItem(DEFAULT_NIEDERSACHSEN.doubleValue())))
            .andExpect(jsonPath("$.[*].niedersachsenHamburgBremen").value(hasItem(DEFAULT_NIEDERSACHSEN_HAMBURG_BREMEN.doubleValue())))
            .andExpect(jsonPath("$.[*].nordrheinWestfalen").value(hasItem(DEFAULT_NORDRHEIN_WESTFALEN.doubleValue())))
            .andExpect(jsonPath("$.[*].rheinlandPfalz").value(hasItem(DEFAULT_RHEINLAND_PFALZ.doubleValue())))
            .andExpect(jsonPath("$.[*].schleswigHolstein").value(hasItem(DEFAULT_SCHLESWIG_HOLSTEIN.doubleValue())))
            .andExpect(jsonPath("$.[*].saarland").value(hasItem(DEFAULT_SAARLAND.doubleValue())))
            .andExpect(jsonPath("$.[*].sachsen").value(hasItem(DEFAULT_SACHSEN.doubleValue())))
            .andExpect(jsonPath("$.[*].sachsenAnhalt").value(hasItem(DEFAULT_SACHSEN_ANHALT.doubleValue())))
            .andExpect(jsonPath("$.[*].thueringenSachsenAnhalt").value(hasItem(DEFAULT_THUERINGEN_SACHSEN_ANHALT.doubleValue())))
            .andExpect(jsonPath("$.[*].thueringen").value(hasItem(DEFAULT_THUERINGEN.doubleValue())))
            .andExpect(jsonPath("$.[*].deutschland").value(hasItem(DEFAULT_DEUTSCHLAND.doubleValue())));

        // Check, that the count call also returns 1
        restDWDMeasurementMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultDWDMeasurementShouldNotBeFound(String filter) throws Exception {
        restDWDMeasurementMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restDWDMeasurementMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingDWDMeasurement() throws Exception {
        // Get the dWDMeasurement
        restDWDMeasurementMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }
}
