package eu.danielgerber.util;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.junit.jupiter.api.Test;

class MovingAverageTest {

    @Test
    void nullAndSizeTest() throws Exception {
        // if we put null in, null should come out without NPE
        assertThat(MovingAverage.createMovingAverage(null, 0)).isNull();
        // empty list should create null result
        assertThat(MovingAverage.createMovingAverage(Arrays.asList(), 0)).isNull();
        // it only makes sense to create a moving average if we have more elements then the window is large
        assertThat(MovingAverage.createMovingAverage(Arrays.asList(1D, 2D, 3D), 4)).isNull();
        // if the window size is smaller then the list is long, there needs to be a returned list
        assertThat(MovingAverage.createMovingAverage(Arrays.asList(1D, 2D, 3D), 3)).isNotNull();
    }

    @Test
    void assertCalculation() throws Exception {
        
        // simple test
        assertThat(MovingAverage.createMovingAverage(Arrays.asList(1D, 2D, 3D), 3)).isEqualTo(Arrays.asList(2D));

        // more complex test
        List<Double> actual   = MovingAverage.createMovingAverage(Arrays.asList(6.71, 8.12, 7.46, 8.21, 7.73, 7.85, 6.72, 6.66, 7.11, 7.2, 7.36, 7.36, 7.63, 7.94, 7.3, 7.39, 7.85, 8.63, 7.91, 8.26, 7.5, 6.91, 8.43, 8.29, 7.82, 8.22, 7.71, 7.36, 7.3, 8.36, 9.02, 7.66, 8.4, 8.39, 7.74, 8.44, 7.38, 8.53, 7.09, 8.48, 8.75, 6.91, 7.88, 7.46, 8.34, 8.63, 7.8, 8.03, 7.14, 8.6, 7.42, 8.13, 7.15, 9.57, 8.22, 8.25, 8.42, 8.46, 8.07, 6.17, 6.68, 7.05, 8.79, 8.14, 8.92, 8.17, 8.16, 9.11, 8.93, 8.46, 8.87, 7.78, 9.06, 7.48, 7.23, 6.38, 8.49, 8.17, 8.84, 8.21, 8.89, 7.1, 7.22, 8.0, 7.39, 8.51, 8.9, 8.05, 7.53, 7.45, 8.33, 7.82, 8.0, 8.66, 8.88, 8.14, 8.49, 7.64, 7.67, 7.23, 8.02, 9.0, 9.03, 7.72, 7.33, 7.79, 7.01, 8.78, 9.52, 9.42, 8.16, 9.3, 8.26, 9.45, 8.54, 6.63, 8.46, 8.89, 9.18, 9.82, 8.62, 9.2, 9.01, 8.62, 8.59, 9.24, 9.68, 9.41, 8.91, 7.46, 9.37, 8.86, 8.39, 10.14, 9.93, 9.37, 9.39, 10.29, 10.35, 10.27, 8.86, 10.17, 10.44), 30);
        List<Double> expected = Arrays.asList(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 7.6433333333333335, 7.720333333333333, 7.705, 7.7363333333333335, 7.742333333333334, 7.742666666666667, 7.762333333333332, 7.7843333333333335, 7.846666666666666, 7.846, 7.8886666666666665, 7.9350000000000005, 7.92, 7.928333333333333, 7.912333333333334, 7.947, 7.988333333333333, 7.986666666666666, 7.966666666666667, 7.941, 7.952333333333333, 7.949666666666666, 7.990333333333334, 7.947666666666666, 7.990333333333334, 8.003666666666668, 8.004666666666667, 8.028333333333332, 8.065000000000001, 8.090666666666667, 8.017666666666667, 7.939666666666667, 7.919333333333333, 7.932333333333333, 7.924, 7.963333333333334, 7.954333333333334, 7.980333333333333, 7.999666666666666, 8.061, 8.060333333333334, 8.064333333333334, 8.093333333333334, 8.132666666666667, 8.133333333333333, 8.096333333333332, 8.021333333333333, 8.044333333333334, 8.049000000000001, 8.105666666666666, 8.092666666666666, 8.141666666666667, 8.107333333333333, 8.109666666666667, 8.057333333333334, 8.029666666666666, 8.038333333333334, 8.054333333333334, 8.040666666666667, 8.022666666666668, 8.065333333333333, 8.120333333333333, 8.145999999999999, 8.119666666666667, 8.136999999999999, 8.135666666666667, 8.134666666666666, 8.145666666666667, 8.096666666666668, 8.054666666666668, 8.013666666666667, 7.985333333333333, 8.026, 8.025, 8.033, 8.036333333333333, 8.083333333333334, 8.034, 8.054333333333334, 8.077, 8.117333333333333, 8.093, 8.166333333333334, 8.201, 8.249333333333333, 8.287666666666668, 8.225, 8.210333333333333, 8.238333333333333, 8.293333333333333, 8.372333333333334, 8.382, 8.428, 8.461666666666668, 8.460333333333333, 8.450666666666667, 8.487333333333334, 8.527, 8.586, 8.627333333333333, 8.635, 8.68, 8.675333333333333, 8.654, 8.734666666666666, 8.821333333333333, 8.873999999999999, 8.953333333333331, 9.003666666666668, 9.031333333333333, 9.059666666666667, 9.083, 9.111999999999998, 9.184666666666665);

        assertThat(actual.size()).isEqualTo(expected.size());

        for (int i = 0; i < actual.size(); i++) {

            if ( expected.get(i) == null )
                assertThat(actual.get(i)).isNull();
            else {

                assertThat(actual.get(i))
                    .isEqualTo(expected.get(i), within(0.0001));
            }
        }
    }
}
