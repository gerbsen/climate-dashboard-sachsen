package eu.danielgerber.domain;

import static org.assertj.core.api.Assertions.assertThat;

import eu.danielgerber.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MastrUnitTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MastrUnit.class);
        MastrUnit mastrUnit1 = new MastrUnit();
        mastrUnit1.setId(1L);
        MastrUnit mastrUnit2 = new MastrUnit();
        mastrUnit2.setId(mastrUnit1.getId());
        assertThat(mastrUnit1).isEqualTo(mastrUnit2);
        mastrUnit2.setId(2L);
        assertThat(mastrUnit1).isNotEqualTo(mastrUnit2);
        mastrUnit1.setId(null);
        assertThat(mastrUnit1).isNotEqualTo(mastrUnit2);
    }
}
