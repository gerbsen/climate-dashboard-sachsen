package eu.danielgerber.domain;

import static org.assertj.core.api.Assertions.assertThat;

import eu.danielgerber.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class UFZRasterPointTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UFZRasterPoint.class);
        UFZRasterPoint uFZRasterPoint1 = new UFZRasterPoint();
        uFZRasterPoint1.setId(1L);
        UFZRasterPoint uFZRasterPoint2 = new UFZRasterPoint();
        uFZRasterPoint2.setId(uFZRasterPoint1.getId());
        assertThat(uFZRasterPoint1).isEqualTo(uFZRasterPoint2);
        uFZRasterPoint2.setId(2L);
        assertThat(uFZRasterPoint1).isNotEqualTo(uFZRasterPoint2);
        uFZRasterPoint1.setId(null);
        assertThat(uFZRasterPoint1).isNotEqualTo(uFZRasterPoint2);
    }
}
