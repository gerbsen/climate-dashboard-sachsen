package eu.danielgerber.domain;

import static org.assertj.core.api.Assertions.assertThat;

import eu.danielgerber.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ChargingStationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChargingStation.class);
        ChargingStation chargingStation1 = new ChargingStation();
        chargingStation1.setId(1L);
        ChargingStation chargingStation2 = new ChargingStation();
        chargingStation2.setId(chargingStation1.getId());
        assertThat(chargingStation1).isEqualTo(chargingStation2);
        chargingStation2.setId(2L);
        assertThat(chargingStation1).isNotEqualTo(chargingStation2);
        chargingStation1.setId(null);
        assertThat(chargingStation1).isNotEqualTo(chargingStation2);
    }
}
