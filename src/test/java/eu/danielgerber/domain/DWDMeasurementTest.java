package eu.danielgerber.domain;

import static org.assertj.core.api.Assertions.assertThat;

import eu.danielgerber.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DWDMeasurementTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DWDMeasurement.class);
        DWDMeasurement dWDMeasurement1 = new DWDMeasurement();
        dWDMeasurement1.setId(1L);
        DWDMeasurement dWDMeasurement2 = new DWDMeasurement();
        dWDMeasurement2.setId(dWDMeasurement1.getId());
        assertThat(dWDMeasurement1).isEqualTo(dWDMeasurement2);
        dWDMeasurement2.setId(2L);
        assertThat(dWDMeasurement1).isNotEqualTo(dWDMeasurement2);
        dWDMeasurement1.setId(null);
        assertThat(dWDMeasurement1).isNotEqualTo(dWDMeasurement2);
    }
}
