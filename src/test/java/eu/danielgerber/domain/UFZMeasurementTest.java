package eu.danielgerber.domain;

import static org.assertj.core.api.Assertions.assertThat;

import eu.danielgerber.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class UFZMeasurementTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UFZMeasurement.class);
        UFZMeasurement uFZMeasurement1 = new UFZMeasurement();
        uFZMeasurement1.setId(1L);
        UFZMeasurement uFZMeasurement2 = new UFZMeasurement();
        uFZMeasurement2.setId(uFZMeasurement1.getId());
        assertThat(uFZMeasurement1).isEqualTo(uFZMeasurement2);
        uFZMeasurement2.setId(2L);
        assertThat(uFZMeasurement1).isNotEqualTo(uFZMeasurement2);
        uFZMeasurement1.setId(null);
        assertThat(uFZMeasurement1).isNotEqualTo(uFZMeasurement2);
    }
}
