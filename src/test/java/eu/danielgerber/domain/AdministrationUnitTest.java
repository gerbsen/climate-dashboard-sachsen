package eu.danielgerber.domain;

import static org.assertj.core.api.Assertions.assertThat;

import eu.danielgerber.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AdministrationUnitTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AdministrationUnit.class);
        AdministrationUnit administrationUnit1 = new AdministrationUnit();
        administrationUnit1.setId(1L);
        AdministrationUnit administrationUnit2 = new AdministrationUnit();
        administrationUnit2.setId(administrationUnit1.getId());
        assertThat(administrationUnit1).isEqualTo(administrationUnit2);
        administrationUnit2.setId(2L);
        assertThat(administrationUnit1).isNotEqualTo(administrationUnit2);
        administrationUnit1.setId(null);
        assertThat(administrationUnit1).isNotEqualTo(administrationUnit2);
    }
}
