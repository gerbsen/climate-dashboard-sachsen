import { entityTableSelector, entityDetailsButtonSelector, entityDetailsBackButtonSelector } from '../../support/entity';

describe('AdministrationUnit e2e test', () => {
  const administrationUnitPageUrl = '/administration-unit';
  const administrationUnitPageUrlPattern = new RegExp('/administration-unit(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const administrationUnitSample = {};

  let administrationUnit;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/administration-units+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/administration-units').as('postEntityRequest');
    cy.intercept('DELETE', '/api/administration-units/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (administrationUnit) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/administration-units/${administrationUnit.id}`,
      }).then(() => {
        administrationUnit = undefined;
      });
    }
  });

  it('AdministrationUnits menu should load AdministrationUnits page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('administration-unit');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('AdministrationUnit').should('exist');
    cy.url().should('match', administrationUnitPageUrlPattern);
  });

  describe('AdministrationUnit page', () => {
    describe('with existing value', () => {
      beforeEach(function () {
        cy.visit(administrationUnitPageUrl);

        cy.wait('@entitiesRequest').then(({ response }) => {
          if (response.body.length === 0) {
            this.skip();
          }
        });
      });

      it('detail button click should load details AdministrationUnit page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('administrationUnit');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', administrationUnitPageUrlPattern);
      });
    });
  });
});
