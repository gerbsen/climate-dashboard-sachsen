import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Community e2e test', () => {
  const communityPageUrl = '/community';
  const communityPageUrlPattern = new RegExp('/community(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const communitySample = { lastModified: '2023-02-16T22:50:27.681Z', dateCreated: '2023-02-17T11:04:16.989Z' };

  let community;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/communities+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/communities').as('postEntityRequest');
    cy.intercept('DELETE', '/api/communities/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (community) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/communities/${community.id}`,
      }).then(() => {
        community = undefined;
      });
    }
  });

  it('Communities menu should load Communities page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('community');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Community').should('exist');
    cy.url().should('match', communityPageUrlPattern);
  });

  describe('Community page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(communityPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Community page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/community/new$'));
        cy.getEntityCreateUpdateHeading('Community');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', communityPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/communities',
          body: communitySample,
        }).then(({ body }) => {
          community = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/communities+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/communities?page=0&size=20>; rel="last",<http://localhost/api/communities?page=0&size=20>; rel="first"',
              },
              body: [community],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(communityPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Community page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('community');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', communityPageUrlPattern);
      });

      it('edit button click should load edit Community page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Community');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', communityPageUrlPattern);
      });

      it.skip('edit button click should load edit Community page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Community');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', communityPageUrlPattern);
      });

      it('last delete button click should delete instance of Community', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('community').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', communityPageUrlPattern);

        community = undefined;
      });
    });
  });

  describe('new Community page', () => {
    beforeEach(() => {
      cy.visit(`${communityPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Community');
    });

    it('should create an instance of Community', () => {
      cy.get(`[data-cy="lastModified"]`).type('2023-02-16T20:18').blur().should('have.value', '2023-02-16T20:18');

      cy.get(`[data-cy="dateCreated"]`).type('2023-02-16T23:26').blur().should('have.value', '2023-02-16T23:26');

      cy.get(`[data-cy="name"]`).type('Kongo').should('have.value', 'Kongo');

      cy.get(`[data-cy="ags"]`).type('50962').should('have.value', '50962');

      cy.get(`[data-cy="district"]`).type('payment').should('have.value', 'payment');

      cy.get(`[data-cy="type"]`).type('policy Buckinghamshire Manager').should('have.value', 'policy Buckinghamshire Manager');

      cy.get(`[data-cy="hasAdminOffice"]`).should('not.be.checked');
      cy.get(`[data-cy="hasAdminOffice"]`).click().should('be.checked');

      cy.get(`[data-cy="geometry"]`).type('Specialist system withdrawal').should('have.value', 'Specialist system withdrawal');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        community = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', communityPageUrlPattern);
    });
  });
});
