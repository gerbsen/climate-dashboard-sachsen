import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('MastrUnit e2e test', () => {
  const mastrUnitPageUrl = '/mastr-unit';
  const mastrUnitPageUrlPattern = new RegExp('/mastr-unit(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const mastrUnitSample = {};

  let mastrUnit;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/mastr-units+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/mastr-units').as('postEntityRequest');
    cy.intercept('DELETE', '/api/mastr-units/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (mastrUnit) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/mastr-units/${mastrUnit.id}`,
      }).then(() => {
        mastrUnit = undefined;
      });
    }
  });

  it('MastrUnits menu should load MastrUnits page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('mastr-unit');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('MastrUnit').should('exist');
    cy.url().should('match', mastrUnitPageUrlPattern);
  });

  describe('MastrUnit page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(mastrUnitPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create MastrUnit page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/mastr-unit/new$'));
        cy.getEntityCreateUpdateHeading('MastrUnit');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', mastrUnitPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/mastr-units',
          body: mastrUnitSample,
        }).then(({ body }) => {
          mastrUnit = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/mastr-units+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/mastr-units?page=0&size=20>; rel="last",<http://localhost/api/mastr-units?page=0&size=20>; rel="first"',
              },
              body: [mastrUnit],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(mastrUnitPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details MastrUnit page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('mastrUnit');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', mastrUnitPageUrlPattern);
      });

      it('edit button click should load edit MastrUnit page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('MastrUnit');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', mastrUnitPageUrlPattern);
      });

      it.skip('edit button click should load edit MastrUnit page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('MastrUnit');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', mastrUnitPageUrlPattern);
      });

      it('last delete button click should delete instance of MastrUnit', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('mastrUnit').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', mastrUnitPageUrlPattern);

        mastrUnit = undefined;
      });
    });
  });

  describe('new MastrUnit page', () => {
    beforeEach(() => {
      cy.visit(`${mastrUnitPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('MastrUnit');
    });

    it('should create an instance of MastrUnit', () => {
      cy.get(`[data-cy="mastrNummer"]`).type('Handmade bypassing').should('have.value', 'Handmade bypassing');

      cy.get(`[data-cy="anlagenBetreiberId"]`).type('81423').should('have.value', '81423');

      cy.get(`[data-cy="anlagenBetreiberPersonenArt"]`).type('34573').should('have.value', '34573');

      cy.get(`[data-cy="anlagenBetreiberMaskedName"]`)
        .type('generate withdrawal sticky')
        .should('have.value', 'generate withdrawal sticky');

      cy.get(`[data-cy="anlagenBetreiberMastrNummer"]`).type('sticky Account').should('have.value', 'sticky Account');

      cy.get(`[data-cy="anlagenBetreiberName"]`).type('Auto Sachsen').should('have.value', 'Auto Sachsen');

      cy.get(`[data-cy="betriebsStatusId"]`).type('62465').should('have.value', '62465');

      cy.get(`[data-cy="betriebsStatusName"]`).type('Account Gorgeous').should('have.value', 'Account Gorgeous');

      cy.get(`[data-cy="bundesland"]`).type('regional concept Concrete').should('have.value', 'regional concept Concrete');

      cy.get(`[data-cy="bundeslandId"]`).type('16803').should('have.value', '16803');

      cy.get(`[data-cy="landkreisId"]`).type('81854').should('have.value', '81854');

      cy.get(`[data-cy="gemeindeId"]`).type('4977').should('have.value', '4977');

      cy.get(`[data-cy="datumLetzteAktualisierung"]`).type('2023-02-23').blur().should('have.value', '2023-02-23');

      cy.get(`[data-cy="einheitRegistrierungsDatum"]`).type('2023-02-22').blur().should('have.value', '2023-02-22');

      cy.get(`[data-cy="endgueltigeStilllegungDatum"]`).type('2023-02-22').blur().should('have.value', '2023-02-22');

      cy.get(`[data-cy="geplantesInbetriebsnahmeDatum"]`).type('2023-02-22').blur().should('have.value', '2023-02-22');

      cy.get(`[data-cy="inbetriebnahmeDatum"]`).type('2023-02-22').blur().should('have.value', '2023-02-22');

      cy.get(`[data-cy="kwkAnlageInbetriebnahmeDatum"]`).type('2023-02-23').blur().should('have.value', '2023-02-23');

      cy.get(`[data-cy="kwkAnlageRegistrierungsDatum"]`).type('2023-02-23').blur().should('have.value', '2023-02-23');

      cy.get(`[data-cy="eegInbetriebnahmeDatum"]`).type('2023-02-22').blur().should('have.value', '2023-02-22');

      cy.get(`[data-cy="eegAnlageRegistrierungsDatum"]`).type('2023-02-23').blur().should('have.value', '2023-02-23');

      cy.get(`[data-cy="genehmigungDatum"]`).type('2023-02-23').blur().should('have.value', '2023-02-23');

      cy.get(`[data-cy="genehmigungRegistrierungsDatum"]`).type('2023-02-22').blur().should('have.value', '2023-02-22');

      cy.get(`[data-cy="isNbPruefungAbgeschlossen"]`).type('67750').should('have.value', '67750');

      cy.get(`[data-cy="isAnonymisiert"]`).should('not.be.checked');
      cy.get(`[data-cy="isAnonymisiert"]`).click().should('be.checked');

      cy.get(`[data-cy="isBuergerEnergie"]`).should('not.be.checked');
      cy.get(`[data-cy="isBuergerEnergie"]`).click().should('be.checked');

      cy.get(`[data-cy="isEinheitNotstromaggregat"]`).should('not.be.checked');
      cy.get(`[data-cy="isEinheitNotstromaggregat"]`).click().should('be.checked');

      cy.get(`[data-cy="isMieterstromAngemeldet"]`).should('not.be.checked');
      cy.get(`[data-cy="isMieterstromAngemeldet"]`).click().should('be.checked');

      cy.get(`[data-cy="isWasserkraftErtuechtigung"]`).should('not.be.checked');
      cy.get(`[data-cy="isWasserkraftErtuechtigung"]`).click().should('be.checked');

      cy.get(`[data-cy="isPilotWindanlage"]`).should('not.be.checked');
      cy.get(`[data-cy="isPilotWindanlage"]`).click().should('be.checked');

      cy.get(`[data-cy="isPrototypAnlage"]`).should('not.be.checked');
      cy.get(`[data-cy="isPrototypAnlage"]`).click().should('be.checked');

      cy.get(`[data-cy="lat"]`).type('75328').should('have.value', '75328');

      cy.get(`[data-cy="lng"]`).type('98977').should('have.value', '98977');

      cy.get(`[data-cy="ort"]`).type('Mouse turquoise back-end').should('have.value', 'Mouse turquoise back-end');

      cy.get(`[data-cy="plz"]`).type('2080').should('have.value', '2080');

      cy.get(`[data-cy="strasse"]`).type('Savings connecting Group').should('have.value', 'Savings connecting Group');

      cy.get(`[data-cy="hausnummer"]`).type('metrics').should('have.value', 'metrics');

      cy.get(`[data-cy="einheitname"]`).type('Advanced extensible Soft').should('have.value', 'Advanced extensible Soft');

      cy.get(`[data-cy="flurstueck"]`).type('digital morph Assimilated').should('have.value', 'digital morph Assimilated');

      cy.get(`[data-cy="gemarkung"]`).type('Senior Computer multi-state').should('have.value', 'Senior Computer multi-state');

      cy.get(`[data-cy="gemeinde"]`)
        .type('Account contextually-based synthesizing')
        .should('have.value', 'Account contextually-based synthesizing');

      cy.get(`[data-cy="landId"]`).type('43760').should('have.value', '43760');

      cy.get(`[data-cy="landkreis"]`).type('capacitor').should('have.value', 'capacitor');

      cy.get(`[data-cy="ags"]`).type('74582').should('have.value', '74582');

      cy.get(`[data-cy="lokationId"]`).type('78091').should('have.value', '78091');

      cy.get(`[data-cy="lokationMastrNr"]`).type('schemas Cambridgeshire payment').should('have.value', 'schemas Cambridgeshire payment');

      cy.get(`[data-cy="netzbetreiberId"]`).type('digital').should('have.value', 'digital');

      cy.get(`[data-cy="netzbetreiberMaskedNamen"]`).type('Plastic generating').should('have.value', 'Plastic generating');

      cy.get(`[data-cy="netzbetreiberMastrNummer"]`)
        .type('hacking Analyst Applications')
        .should('have.value', 'hacking Analyst Applications');

      cy.get(`[data-cy="netzbetreiberNamen"]`).type('online Salomonen synthesizing').should('have.value', 'online Salomonen synthesizing');

      cy.get(`[data-cy="netzbetreiberPersonenArt"]`).type('Bedfordshire').should('have.value', 'Bedfordshire');

      cy.get(`[data-cy="systemStatusId"]`).type('51649').should('have.value', '51649');

      cy.get(`[data-cy="systemStatusName"]`).type('scalable').should('have.value', 'scalable');

      cy.get(`[data-cy="typ"]`).type('29114').should('have.value', '29114');

      cy.get(`[data-cy="aktenzeichenGenehmigung"]`).type('Quality').should('have.value', 'Quality');

      cy.get(`[data-cy="anzahlSolarmodule"]`).type('28149').should('have.value', '28149');

      cy.get(`[data-cy="batterieTechnologie"]`).type('31277').should('have.value', '31277');

      cy.get(`[data-cy="bruttoLeistung"]`).type('17035').should('have.value', '17035');

      cy.get(`[data-cy="eegInstallierteLeistung"]`).type('95600').should('have.value', '95600');

      cy.get(`[data-cy="eegAnlageMastrNummer"]`).type('Licensed').should('have.value', 'Licensed');

      cy.get(`[data-cy="eegAnlagenSchluessel"]`).type('leading-edge payment SQL').should('have.value', 'leading-edge payment SQL');

      cy.get(`[data-cy="eegZuschlag"]`).type('Cotton Tools').should('have.value', 'Cotton Tools');

      cy.get(`[data-cy="zuschlagsNummern"]`).type('Ergonomic').should('have.value', 'Ergonomic');

      cy.get(`[data-cy="energieTraegerId"]`).type('68055').should('have.value', '68055');

      cy.get(`[data-cy="energieTraegerName"]`).type('Mecklenburg-Vorpommern').should('have.value', 'Mecklenburg-Vorpommern');

      cy.get(`[data-cy="gemeinsamerWechselrichter"]`).type('75038').should('have.value', '75038');

      cy.get(`[data-cy="genehmigungBehoerde"]`).type('Manat lime').should('have.value', 'Manat lime');

      cy.get(`[data-cy="genehmigungsMastrNummer"]`).type('Berkshire program').should('have.value', 'Berkshire program');

      cy.get(`[data-cy="gruppierungsObjekte"]`).type('Unbranded generate maroon').should('have.value', 'Unbranded generate maroon');

      cy.get(`[data-cy="gruppierungsObjekteIds"]`).type('bi-directional').should('have.value', 'bi-directional');

      cy.get(`[data-cy="hatFlexibilitaetsPraemie"]`).should('not.be.checked');
      cy.get(`[data-cy="hatFlexibilitaetsPraemie"]`).click().should('be.checked');

      cy.get(`[data-cy="hauptAusrichtungSolarmodule"]`).type('31952').should('have.value', '31952');

      cy.get(`[data-cy="hauptAusrichtungSolarmoduleBezeichnung"]`)
        .type('disintermediate Peso Global')
        .should('have.value', 'disintermediate Peso Global');

      cy.get(`[data-cy="hauptBrennstoffId"]`).type('99632').should('have.value', '99632');

      cy.get(`[data-cy="hauptBrennstoffNamen"]`)
        .type('Clothing Metal disintermediate')
        .should('have.value', 'Clothing Metal disintermediate');

      cy.get(`[data-cy="hauptNeigungswinkelSolarModule"]`).type('71889').should('have.value', '71889');

      cy.get(`[data-cy="herstellerWindenergieanlageId"]`).type('98068').should('have.value', '98068');

      cy.get(`[data-cy="herstellerWindenergieanlageBezeichnung"]`).type('driver').should('have.value', 'driver');

      cy.get(`[data-cy="kwkAnlageElektrischeLeistung"]`).type('1122').should('have.value', '1122');

      cy.get(`[data-cy="kwkAnlageMastrNummer"]`).type('interfaces').should('have.value', 'interfaces');

      cy.get(`[data-cy="kwkZuschlag"]`).type('Belarussian Crest Chicken').should('have.value', 'Belarussian Crest Chicken');

      cy.get(`[data-cy="lageEinheit"]`).type('1539').should('have.value', '1539');

      cy.get(`[data-cy="lageEinheitBezeichnung"]`).type('withdrawal Account Research').should('have.value', 'withdrawal Account Research');

      cy.get(`[data-cy="leistungsBegrenzung"]`).type('91174').should('have.value', '91174');

      cy.get(`[data-cy="nabenhoeheWindenergieanlage"]`).type('97603').should('have.value', '97603');

      cy.get(`[data-cy="rotorDurchmesserWindenergieanlage"]`).type('88042').should('have.value', '88042');

      cy.get(`[data-cy="nettoNennLeistung"]`).type('91098').should('have.value', '91098');

      cy.get(`[data-cy="nutzbareSpeicherKapazitaet"]`).type('44644').should('have.value', '44644');

      cy.get(`[data-cy="nutzungsBereichGebsa"]`).type('50459').should('have.value', '50459');

      cy.get(`[data-cy="standortAnonymisiert"]`).type('Buckinghamshire').should('have.value', 'Buckinghamshire');

      cy.get(`[data-cy="spannungsEbenenId"]`).type('CSS Metrics synergistic').should('have.value', 'CSS Metrics synergistic');

      cy.get(`[data-cy="spannungsEbenenNamen"]`).type('overriding Kasachstan').should('have.value', 'overriding Kasachstan');

      cy.get(`[data-cy="speicherEinheitMastrNummer"]`).type('portals').should('have.value', 'portals');

      cy.get(`[data-cy="technologieStromerzeugungId"]`).type('21275').should('have.value', '21275');

      cy.get(`[data-cy="technologieStromerzeugung"]`).type('navigate').should('have.value', 'navigate');

      cy.get(`[data-cy="thermischeNutzLeistung"]`).type('7228').should('have.value', '7228');

      cy.get(`[data-cy="typenBezeichnung"]`).type('protocol').should('have.value', 'protocol');

      cy.get(`[data-cy="vollTeilEinspeisung"]`).type('15594').should('have.value', '15594');

      cy.get(`[data-cy="vollTeilEinspeisungBezeichnung"]`)
        .type('algorithm Assimilated Sleek')
        .should('have.value', 'algorithm Assimilated Sleek');

      cy.get(`[data-cy="windparkName"]`).type('Sachsen Cheese Account').should('have.value', 'Sachsen Cheese Account');

      cy.get(`[data-cy="geometry"]`).type('Avon').should('have.value', 'Avon');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        mastrUnit = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', mastrUnitPageUrlPattern);
    });
  });
});
