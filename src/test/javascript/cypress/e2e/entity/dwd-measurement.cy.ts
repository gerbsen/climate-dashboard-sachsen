import { entityTableSelector, entityDetailsButtonSelector, entityDetailsBackButtonSelector } from '../../support/entity';

describe('DWDMeasurement e2e test', () => {
  const dWDMeasurementPageUrl = '/dwd-measurement';
  const dWDMeasurementPageUrlPattern = new RegExp('/dwd-measurement(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const dWDMeasurementSample = {};

  let dWDMeasurement;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/dwd-measurements+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/dwd-measurements').as('postEntityRequest');
    cy.intercept('DELETE', '/api/dwd-measurements/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (dWDMeasurement) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/dwd-measurements/${dWDMeasurement.id}`,
      }).then(() => {
        dWDMeasurement = undefined;
      });
    }
  });

  it('DWDMeasurements menu should load DWDMeasurements page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('dwd-measurement');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('DWDMeasurement').should('exist');
    cy.url().should('match', dWDMeasurementPageUrlPattern);
  });

  describe('DWDMeasurement page', () => {
    describe('with existing value', () => {
      beforeEach(function () {
        cy.visit(dWDMeasurementPageUrl);

        cy.wait('@entitiesRequest').then(({ response }) => {
          if (response.body.length === 0) {
            this.skip();
          }
        });
      });

      it('detail button click should load details DWDMeasurement page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('dWDMeasurement');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', dWDMeasurementPageUrlPattern);
      });
    });
  });
});
