import { entityTableSelector, entityDetailsButtonSelector, entityDetailsBackButtonSelector } from '../../support/entity';

describe('ChargingStation e2e test', () => {
  const chargingStationPageUrl = '/charging-station';
  const chargingStationPageUrlPattern = new RegExp('/charging-station(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const chargingStationSample = {};

  let chargingStation;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/charging-stations+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/charging-stations').as('postEntityRequest');
    cy.intercept('DELETE', '/api/charging-stations/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (chargingStation) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/charging-stations/${chargingStation.id}`,
      }).then(() => {
        chargingStation = undefined;
      });
    }
  });

  it('ChargingStations menu should load ChargingStations page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('charging-station');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('ChargingStation').should('exist');
    cy.url().should('match', chargingStationPageUrlPattern);
  });

  describe('ChargingStation page', () => {
    describe('with existing value', () => {
      beforeEach(function () {
        cy.visit(chargingStationPageUrl);

        cy.wait('@entitiesRequest').then(({ response }) => {
          if (response.body.length === 0) {
            this.skip();
          }
        });
      });

      it('detail button click should load details ChargingStation page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('chargingStation');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', chargingStationPageUrlPattern);
      });
    });
  });
});
