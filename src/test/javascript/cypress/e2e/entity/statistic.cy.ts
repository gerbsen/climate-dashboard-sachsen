import { entityTableSelector, entityDetailsButtonSelector, entityDetailsBackButtonSelector } from '../../support/entity';

describe('Statistic e2e test', () => {
  const statisticPageUrl = '/statistic';
  const statisticPageUrlPattern = new RegExp('/statistic(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const statisticSample = {};

  let statistic;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/statistics+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/statistics').as('postEntityRequest');
    cy.intercept('DELETE', '/api/statistics/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (statistic) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/statistics/${statistic.id}`,
      }).then(() => {
        statistic = undefined;
      });
    }
  });

  it('Statistics menu should load Statistics page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('statistic');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Statistic').should('exist');
    cy.url().should('match', statisticPageUrlPattern);
  });

  describe('Statistic page', () => {
    describe('with existing value', () => {
      beforeEach(function () {
        cy.visit(statisticPageUrl);

        cy.wait('@entitiesRequest').then(({ response }) => {
          if (response.body.length === 0) {
            this.skip();
          }
        });
      });

      it('detail button click should load details Statistic page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('statistic');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', statisticPageUrlPattern);
      });
    });
  });
});
