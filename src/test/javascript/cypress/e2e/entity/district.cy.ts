import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('District e2e test', () => {
  const districtPageUrl = '/district';
  const districtPageUrlPattern = new RegExp('/district(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const districtSample = {};

  let district;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/districts+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/districts').as('postEntityRequest');
    cy.intercept('DELETE', '/api/districts/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (district) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/districts/${district.id}`,
      }).then(() => {
        district = undefined;
      });
    }
  });

  it('Districts menu should load Districts page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('district');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('District').should('exist');
    cy.url().should('match', districtPageUrlPattern);
  });

  describe('District page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(districtPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create District page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/district/new$'));
        cy.getEntityCreateUpdateHeading('District');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', districtPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/districts',
          body: districtSample,
        }).then(({ body }) => {
          district = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/districts+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/districts?page=0&size=20>; rel="last",<http://localhost/api/districts?page=0&size=20>; rel="first"',
              },
              body: [district],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(districtPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details District page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('district');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', districtPageUrlPattern);
      });

      it('edit button click should load edit District page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('District');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', districtPageUrlPattern);
      });

      it.skip('edit button click should load edit District page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('District');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', districtPageUrlPattern);
      });

      it('last delete button click should delete instance of District', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('district').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', districtPageUrlPattern);

        district = undefined;
      });
    });
  });

  describe('new District page', () => {
    beforeEach(() => {
      cy.visit(`${districtPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('District');
    });

    it('should create an instance of District', () => {
      cy.get(`[data-cy="objektidentifikator"]`).type('aspernatur siegreich männlich');
      cy.get(`[data-cy="objektidentifikator"]`).should('have.value', 'aspernatur siegreich männlich');

      cy.get(`[data-cy="beginnlebenszeit"]`).type('2023-10-31');
      cy.get(`[data-cy="beginnlebenszeit"]`).blur();
      cy.get(`[data-cy="beginnlebenszeit"]`).should('have.value', '2023-10-31');

      cy.get(`[data-cy="adminEbeneAde"]`).type('laborum dank');
      cy.get(`[data-cy="adminEbeneAde"]`).should('have.value', 'laborum dank');

      cy.get(`[data-cy="ade"]`).type('19300');
      cy.get(`[data-cy="ade"]`).should('have.value', '19300');

      cy.get(`[data-cy="geofaktorGf"]`).type('Sachsen Indigo vielseitig');
      cy.get(`[data-cy="geofaktorGf"]`).should('have.value', 'Sachsen Indigo vielseitig');

      cy.get(`[data-cy="gf"]`).type('17760');
      cy.get(`[data-cy="gf"]`).should('have.value', '17760');

      cy.get(`[data-cy="besondereGebieteBsg"]`).type('porro männlich');
      cy.get(`[data-cy="besondereGebieteBsg"]`).should('have.value', 'porro männlich');

      cy.get(`[data-cy="bsg"]`).type('30913');
      cy.get(`[data-cy="bsg"]`).should('have.value', '30913');

      cy.get(`[data-cy="regionalschluesselArs"]`).type('für Sudan');
      cy.get(`[data-cy="regionalschluesselArs"]`).should('have.value', 'für Sudan');

      cy.get(`[data-cy="gemeindeschluesselAgs"]`).type('Ohr transpirieren männlich');
      cy.get(`[data-cy="gemeindeschluesselAgs"]`).should('have.value', 'Ohr transpirieren männlich');

      cy.get(`[data-cy="verwaltungssitzSdvArs"]`).type('Berlin');
      cy.get(`[data-cy="verwaltungssitzSdvArs"]`).should('have.value', 'Berlin');

      cy.get(`[data-cy="geografischernameGen"]`).type('weiblich Silber');
      cy.get(`[data-cy="geografischernameGen"]`).should('have.value', 'weiblich Silber');

      cy.get(`[data-cy="bezeichnung"]`).type('Österreich Würfel');
      cy.get(`[data-cy="bezeichnung"]`).should('have.value', 'Österreich Würfel');

      cy.get(`[data-cy="identifikatorIbz"]`).type('30091');
      cy.get(`[data-cy="identifikatorIbz"]`).should('have.value', '30091');

      cy.get(`[data-cy="bemerkung"]`).type('schuldenhalber buhlen');
      cy.get(`[data-cy="bemerkung"]`).should('have.value', 'schuldenhalber buhlen');

      cy.get(`[data-cy="namensbildungNbd"]`).type('Dunkelblau');
      cy.get(`[data-cy="namensbildungNbd"]`).should('have.value', 'Dunkelblau');

      cy.get(`[data-cy="nbd"]`).should('not.be.checked');
      cy.get(`[data-cy="nbd"]`).click();
      cy.get(`[data-cy="nbd"]`).should('be.checked');

      cy.get(`[data-cy="land"]`).type('Junkernkamp Polen schwerlich');
      cy.get(`[data-cy="land"]`).should('have.value', 'Junkernkamp Polen schwerlich');

      cy.get(`[data-cy="regierungsbezirk"]`).type('Magenta');
      cy.get(`[data-cy="regierungsbezirk"]`).should('have.value', 'Magenta');

      cy.get(`[data-cy="kreis"]`).type('Hellgrün abher');
      cy.get(`[data-cy="kreis"]`).should('have.value', 'Hellgrün abher');

      cy.get(`[data-cy="verwaltungsgemeinschaftteil1"]`).type('Am Wind');
      cy.get(`[data-cy="verwaltungsgemeinschaftteil1"]`).should('have.value', 'Am Wind');

      cy.get(`[data-cy="verwaltungsgemeinschaftteil2"]`).type('selbviert Hamburg Martinique');
      cy.get(`[data-cy="verwaltungsgemeinschaftteil2"]`).should('have.value', 'selbviert Hamburg Martinique');

      cy.get(`[data-cy="gemeinde"]`).type('Vietnam consectetur');
      cy.get(`[data-cy="gemeinde"]`).should('have.value', 'Vietnam consectetur');

      cy.get(`[data-cy="funkSchluesselstelle3"]`).type('bar Lurchenweg Eisblau');
      cy.get(`[data-cy="funkSchluesselstelle3"]`).should('have.value', 'bar Lurchenweg Eisblau');

      cy.get(`[data-cy="fkS3"]`).type('Mintgrün sunt weiblich');
      cy.get(`[data-cy="fkS3"]`).should('have.value', 'Mintgrün sunt weiblich');

      cy.get(`[data-cy="europStatistikschluesselNuts"]`).type('Pferd Gott');
      cy.get(`[data-cy="europStatistikschluesselNuts"]`).should('have.value', 'Pferd Gott');

      cy.get(`[data-cy="regioschluesselaufgefuellt"]`).type('prachtvoll flaggen Bordeauxrot');
      cy.get(`[data-cy="regioschluesselaufgefuellt"]`).should('have.value', 'prachtvoll flaggen Bordeauxrot');

      cy.get(`[data-cy="gemeindeschluesselaufgefuellt"]`).type('männlich');
      cy.get(`[data-cy="gemeindeschluesselaufgefuellt"]`).should('have.value', 'männlich');

      cy.get(`[data-cy="wirksamkeitWsk"]`).type('2023-10-30');
      cy.get(`[data-cy="wirksamkeitWsk"]`).blur();
      cy.get(`[data-cy="wirksamkeitWsk"]`).should('have.value', '2023-10-30');

      cy.get(`[data-cy="geometry"]`).type('Bordeauxrot Giftgrün');
      cy.get(`[data-cy="geometry"]`).should('have.value', 'Bordeauxrot Giftgrün');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        district = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', districtPageUrlPattern);
    });
  });
});
