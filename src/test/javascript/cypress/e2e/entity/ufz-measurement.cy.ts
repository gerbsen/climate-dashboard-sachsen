import { entityTableSelector, entityDetailsButtonSelector, entityDetailsBackButtonSelector } from '../../support/entity';

describe('UFZMeasurement e2e test', () => {
  const uFZMeasurementPageUrl = '/ufz-measurement';
  const uFZMeasurementPageUrlPattern = new RegExp('/ufz-measurement(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const uFZMeasurementSample = {};

  let uFZMeasurement;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/ufz-measurements+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/ufz-measurements').as('postEntityRequest');
    cy.intercept('DELETE', '/api/ufz-measurements/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (uFZMeasurement) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/ufz-measurements/${uFZMeasurement.id}`,
      }).then(() => {
        uFZMeasurement = undefined;
      });
    }
  });

  it('UFZMeasurements menu should load UFZMeasurements page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('ufz-measurement');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('UFZMeasurement').should('exist');
    cy.url().should('match', uFZMeasurementPageUrlPattern);
  });

  describe('UFZMeasurement page', () => {
    describe('with existing value', () => {
      beforeEach(function () {
        cy.visit(uFZMeasurementPageUrl);

        cy.wait('@entitiesRequest').then(({ response }) => {
          if (response.body.length === 0) {
            this.skip();
          }
        });
      });

      it('detail button click should load details UFZMeasurement page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('uFZMeasurement');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', uFZMeasurementPageUrlPattern);
      });
    });
  });
});
