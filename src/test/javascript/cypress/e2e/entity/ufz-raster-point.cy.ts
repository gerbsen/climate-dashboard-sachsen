import { entityTableSelector, entityDetailsButtonSelector, entityDetailsBackButtonSelector } from '../../support/entity';

describe('UFZRasterPoint e2e test', () => {
  const uFZRasterPointPageUrl = '/ufz-raster-point';
  const uFZRasterPointPageUrlPattern = new RegExp('/ufz-raster-point(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const uFZRasterPointSample = {};

  let uFZRasterPoint;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/ufz-raster-points+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/ufz-raster-points').as('postEntityRequest');
    cy.intercept('DELETE', '/api/ufz-raster-points/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (uFZRasterPoint) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/ufz-raster-points/${uFZRasterPoint.id}`,
      }).then(() => {
        uFZRasterPoint = undefined;
      });
    }
  });

  it('UFZRasterPoints menu should load UFZRasterPoints page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('ufz-raster-point');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('UFZRasterPoint').should('exist');
    cy.url().should('match', uFZRasterPointPageUrlPattern);
  });

  describe('UFZRasterPoint page', () => {
    describe('with existing value', () => {
      beforeEach(function () {
        cy.visit(uFZRasterPointPageUrl);

        cy.wait('@entitiesRequest').then(({ response }) => {
          if (response.body.length === 0) {
            this.skip();
          }
        });
      });

      it('detail button click should load details UFZRasterPoint page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('uFZRasterPoint');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', uFZRasterPointPageUrlPattern);
      });
    });
  });
});
