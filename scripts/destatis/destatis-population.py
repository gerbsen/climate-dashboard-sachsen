import pandas as pd
import math
import psycopg2
pd.set_option('display.max_rows', 10)
pd.set_option('display.min_rows', 500)

# This script is a helper script which only needs to be run after DESTATIS releases 
# a new population XLS file which should only happen once a year, and even then it is not 
# needed. 

# IMPORTANT: you need to have a running database connection with all administration units imported
path = "./31122022_Auszug_GV.xlsx"
path = "/app/scripts/destatis/31122022_Auszug_GV.xlsx"
columns = ["Satzart", "Textkennzeichen", "Land", "RB", "Kreis", "VB", "Gem", "Name", "Fläche", "pop_total", "pop_male", "pop_female", "pop_area", "PLZ", "lng", "lat", "Reisegebiete_Schlüssel", "Reisegebiete_Bezeichnung", "Grad_Verstädterung_Schlüssel", "Grad_Verstädterung_Bezeichnung"]
df = pd.read_excel(path, "Onlineprodukt_Gemeinden", header=None, names=columns, skiprows=5, skipfooter=12, converters={3:int, 2:int, 4:str, 5:int, 6:int})
df = df[df["Textkennzeichen"].notna()]
df = df.drop(columns=["Satzart", "Textkennzeichen", "Reisegebiete_Schlüssel", "Reisegebiete_Bezeichnung", "Grad_Verstädterung_Schlüssel", "Grad_Verstädterung_Bezeichnung"])
df["Land"] = pd.to_numeric(df["Land"])
df["RB"] = pd.to_numeric(df["RB"])
df["Kreis"] = pd.to_numeric(df["Kreis"])
df["VB"] = pd.to_numeric(df["VB"])
df["Gem"] = pd.to_numeric(df["Gem"])
df["Land"] = pd.to_numeric(df["Land"])
date = '2022-12-31'
inserts = []
# change this line to run stuff outside docker (faster queries)
# conn = psycopg2.connect("host=localhost dbname=energytransitiondashboard user=energytransitiondashboard")
conn = psycopg2.connect("host=host.docker.internal dbname=energytransitiondashboard user=energytransitiondashboard")
cur = conn.cursor()
id = -100000;
population_total = 0;
population_male = 0;
population_female = 0;
population_foo = ""
area = 0;
adminUnitName = ''
foundAdminUnits = 0
adminUnitId = -1
currentLandkreisAgs = ""
previousLandkreisAgs = "01001000"

def isRowWithLandkreis(row):
    return math.isnan(row["VB"])

def createInsertRows(adminUnitId, id, population_total, population_male, population_female, area, date):
    inserts = []
    inserts.append('  (E\'{0}\', E\'{1}\', {2}, E\'{3}\', E\'{4}\', {5}, E\'{6}\', {7})'.format("population_total", "Einwohnende (gesamt)", population_total, "Einwohnende", date, adminUnitId, "destatis", id))
    inserts.append('  (E\'{0}\', E\'{1}\', {2}, E\'{3}\', E\'{4}\', {5}, E\'{6}\', {7})'.format("population_male", "Einwohnende (männlich)", population_male, "Einwohnende", date, adminUnitId, "destatis", id + 1))
    inserts.append('  (E\'{0}\', E\'{1}\', {2}, E\'{3}\', E\'{4}\', {5}, E\'{6}\', {7})'.format("population_female", "Einwohnende (weiblich)", population_female, "Einwohnende", date, adminUnitId, "destatis", id + 2))
    inserts.append('  (E\'{0}\', E\'{1}\', {2}, E\'{3}\', E\'{4}\', {5}, E\'{6}\', {7})'.format("population_per_km2", "Einwohnerdichte", 0 if area == 0 else population_total/area, "Einwohnende/km²", date, adminUnitId, "destatis", id + 3))
    inserts.append('  (E\'{0}\', E\'{1}\', {2}, E\'{3}\', E\'{4}\', {5}, E\'{6}\', {7})'.format("area", "Fläche", area, "km²", date, adminUnitId, "destatis", id + 4))
    return inserts;

## filter all states and historic data (in red in the file)
df_all = df[df["Kreis"].notna()]
df_neg = df_all[(df_all["VB"].notna() & df_all["Gem"].isna())]
df_res = pd.merge(df_neg, df_all, on=["Land", "RB", "Kreis", "VB", "Gem"], how='outer', indicator=True).query("_merge != 'both'").drop(['_merge', 'Name_x', "Fläche_x", "pop_total_x", "pop_male_x", "pop_female_x", "pop_area_x", "PLZ_x", "lng_x", "lat_x",], axis=1).reset_index(drop=True)
df_res.columns = df_res.columns.str.replace('_y', '')

for index, row in df_res.iterrows():

    # only check rows with are a kreis or kreisfreie stadt
    if isRowWithLandkreis(row):
      currentLandkreisAgs = '{:02.0F}{:01.0F}{:02.0F}000'.format(row["Land"], row["RB"], row["Kreis"])
      print('Found new Landkreis with AGS \'{0}\' (old: {1})'.format(currentLandkreisAgs, previousLandkreisAgs))

      ## we found a new landkreis on the row, so we have to save the old one
      if currentLandkreisAgs != previousLandkreisAgs:

        print(row["Name"], previousLandkreisAgs)
        cur.execute('SELECT id FROM administration_unit a where a.gemeindeschluesselaufgefuellt = \'{0}\' and ade = 4'.format(previousLandkreisAgs));
        adminUnitId = cur.fetchone()[0]
        inserts += createInsertRows(adminUnitId, id, population_total, population_male, population_female, area, date);
        id += 5

        population_total = 0
        population_male = 0
        population_female = 0
        area = 0

      previousLandkreisAgs = currentLandkreisAgs

    else:
        
        ags = '{:02.0F}{:01.0F}{:02.0F}{:03.0F}'.format(row["Land"], row["RB"], row["Kreis"], row["Gem"])

        print('Found new Gemeinde with AGS \'{0}\' (old: {1})'.format(ags, previousLandkreisAgs))

        # Gemeinsames deutsch-luxemburgisches Hoheitsgebiet or Deutsch-luxemburgisches Hoheitsgebiet or Küstengewässer einschl. Anteil am Festlandsockel
        if ags != '07000999' and ags != '10042999' and ags != '13000999': 

            cur.execute('SELECT id FROM administration_unit a where a.gemeindeschluesselaufgefuellt = \'{0}\' and ade = 6'.format(ags))
            one = cur.fetchone() # return the id once for all inserts
            inserts += createInsertRows(one[0], id, row["pop_total"], row["pop_male"], row["pop_female"], row["Fläche"], date);
            id += 5

            # sum up values to have values for the complete landkreis
            population_total  += row["pop_total"];
            population_male   += row["pop_male"];
            population_female += row["pop_female"];
            area              += row["Fläche"];

cur.execute('SELECT id FROM administration_unit a where a.gemeindeschluesselaufgefuellt = \'{0}\' and ade = 4'.format(previousLandkreisAgs));
inserts += createInsertRows(cur.fetchone()[0], id, population_total, population_male, population_female, area, date);

print("writing sql file to the disk")
# change this line to run stuff outside docker (faster queries)
# f = open("../../src/main/resources/config/liquibase/data/germany_destatis_statistics_administrative_units.sql", "w")
f = open("/app/src/main/resources/config/liquibase/data/germany_destatis_statistics_administrative_units.sql", "w")
f.write('INSERT INTO statistic ("key","name","value","unit","date","administration_unit_id","source","id")\n')
f.write('VALUES \n')
f.write(",\n".join(inserts) + ";")
f.close()

# print("reading sql file to the disk to sql import")
# with open("/app/src/main/resources/config/liquibase/data/germany_destatis_statistics_administrative_units.sql") as f:
#     query = f.read()
#     cur.execute(query)
#     conn.commit()