import { MOBILITY_CHARTS_CAR_TYPE_QUERY } from './queries/mobility_type_car.js';

import got from 'got';
import * as fs from 'fs';

const GRAFANA_URL = "https://scarica.isea.rwth-aachen.de/iseaview/api/ds/query";

const queries = [
  {
    query: MOBILITY_CHARTS_CAR_TYPE_QUERY,
    filename : "rwth_mobility_car_type.ts",
    constantName: "MOBILITY_CHARTS_CAR_TYPE"
  }
]

const date = new Date();
date.setFullYear( date.getFullYear() - 5 )
const millis = date.getTime();

const state = process.argv.slice(2)[0];
if ( ['Baden-Württemberg', 'Bayern', 'Berlin', 'Brandenburg', 'Bremen',
  'Hamburg', 'Hessen', 'Mecklenburg-Vorpommern', 'Niedersachsen', 'Nordrhein-Westfalen',
  'Rheinland-Pfalz', 'Saarland', 'Sachsen-Anhalt' , 'Sachsen', 'Schleswig-Holstein', 'Thüringen' ].indexOf(state) === 0 ) {

  console.log("State not supported: '" + state + "'");
  process.exit()
}

for (const query of queries) {
  for ( let q of query.query.queries ) q.rawSql = q.rawSql.replaceAll("BUNDESLAND", state);
  const data = await got.post(GRAFANA_URL, { json: query.query }).json();

  if ( query.constantName === "MOBILITY_CHARTS_CAR_TYPE" ){
  }
  else { /* no need to do anything */ console.log("else") };

  const output = `export const ${query.constantName} = ${JSON.stringify(data, null, 2)};`
  fs.writeFile(`/app/data/rwth_aachen/${query.filename}`, output, function (err,data) {
    if (err) { return console.log(err); }
  });
}


